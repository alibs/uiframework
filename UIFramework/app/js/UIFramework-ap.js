
ALiBS.gv.sy.so="immediate-console";
//ALiBS.gv.sy.so="print";


// *************************************************************************
var P = new ALiBS.ap();
P.gv = {}; // P global values 
// *************************************************************************
P.ap.re = function() // onPageRender
{
	//ALiBS.sy.gl = 7; // system global level
	//
	P.ap.le = 1; // local log status
	ALiBS.gv.ag.le = 1;
	ALiBS.gv.ap.le = 1;
	//
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.re",le:P.ap.le});
	//
	// Load Required Modules
	//
	P.ap._mod[P.ap._mod.length] = "ALiBS.ui"; // only if REQUIRED
	P.ap._mod[P.ap._mod.length] = "JQueryMobile.ui"; // only if REQUIRED
	//
	//P.ap._mod[P.ap._mod.length] = "ALiBS.b0"; // only if REQUIRED
	//
	//P.ap._mod[P.ap._mod.length] = "ALiBS.hp"; // only if REQUIRED
	//
	//P.ap._mod[P.ap._mod.length] = "ALiBS.it"; // only if REQUIRED
	P.ap._mod[P.ap._mod.length] = "ALiBS.ev"; // only if REQUIRED
	P.ap._mod[P.ap._mod.length] = "ALiBS.lg"; // only if REQUIRED
	//
	P.ap._mod[P.ap._mod.length] = "ALiBS.to"; // only if REQUIRED
	//
	// Set Language files
	//
	//ALiBS.gv.sy.lc["ALiBS.it.pt_br_ap"] = "./i18n/it-pt_br_ap.js"; // application custom words - should be replace in P.ap.re
	//ALiBS.gv.sy.lc["ALiBS.it.en_us_ap"] = "./i18n/it-en_us_ap.js"; // application custom words - should be replace in P.ap.re
	//ALiBS.gv.sy.lc["ALiBS.it.es_es_ap"] = "./i18n/it-es_es_ap.js"; // application custom words - should be replace in P.ap.re
};
// *************************************************************************
P.ap.ol = function() // onPageLoad
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.ol",le:P.ap.le});
	//
	//ALiBS.el.ge("iLgCtDiv").innerHTML = P.lg.lg();
	//
	ALiBS.gv.ag.le = 1; // local log status
	//ALiBS.gv.it.le = 1; // local log status
	//
	// Mapping of Application Modules
	//ALiBS.gv.sy.mc["f0"] = {f00:"./apf0.js"};
	//ALiBS.gv.sy.mc["f1"] = {f00:"./apf1.js"};
	//ALiBS.gv.sy.mc["f2"] = {f00:"./apf2.js"};
	//
/*	if(!P.ap.f0){ ALiBS.ag.lm("f0"); }
	var z = new ALiBS.ag.tc(100,100);
	z.cb = function()
	{
		P.ap.f0();
	};
	z.ex();
	//
	//
	if(!P.ap.f1){ ALiBS.ag.lm("f1"); }
	var y = new ALiBS.ag.tc(100,100);
	y.cb = function()
	{
		P.ap.f1();
	};
	y.ex()
	//
	//
	if(!P.ap.f2){ ALiBS.ag.lm("f2"); }
	var x = new ALiBS.ag.tc(100,100);
	x.cb = function()
	{
		P.ap.f2();
	};
	x.ex();
*/	//
	//ALiBS.gv.lg.img = ALiBS.gv.sy.sp + "lg01.gif";
	//
	// Mapping of Application Modules
	//ALiBS.gv.sy.mc["jquery"] = {f00:"../jquery.mobile.custom/jquery.1.11.1.min.js"};
	//ALiBS.ag.lm("jquery");
	//
	P.ap.p0_up2();
	//
	// Mapping of Application Modules
	//ALiBS.gv.sy.mc["jquerymobile"] = {f00:"../jquery.mobile.custom/jquery.mobile.custom.min.js"}; // reload to show "body" again
	//ALiBS.ag.lm("jquerymobile");
};
// *************************************************************************
P.ap.cl = function() // changePageLanguage
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.cl",le:P.ap.le});
	//
	//P.ap._itr("iItSel"); // i18n refresh combo of languages
	//
	ALiBS.sy.la(5+0,"Refreshing Language",{o:"P.ap.cl",le:P.ap.le});
	//
	// refresh words in current language (these are the tokens, not the words)
/*	//
	ALiBS.el.si("div3",ALiBS.gv.it.cla["security"]);
	ALiBS.el.si("div4",ALiBS.gv.it.cla["password"]);
	ALiBS.el.si("div5",ALiBS.gv.it.cla["veryWeakPassword"]);
	ALiBS.el.si("div6",ALiBS.gv.it.cla["weakPassword"]);
	//
	ALiBS.el.si("div7",ALiBS.gv.it.cla["I"]);
	ALiBS.el.si("div8",ALiBS.gv.it.cla["you"]);
	ALiBS.el.si("div9",ALiBS.gv.it.cla["he"]);
	ALiBS.el.si("div10",ALiBS.gv.it.cla["we"]);
	//
	ALiBS.el.si("div11",ALiBS.gv.it.cla["i_lglg_maximize"]);
*/	//
	ALiBS.sy.la(5+0,"Language Refreshed",{o:"P.ap.cl",le:P.ap.le});
};
// *************************************************************************
P.ap.al = function() // after P.ap.ol and after P.ap.cl and after P.ap._rm
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.al",le:P.ap.le});
	//
	ALiBS.gv.ag.nd = 0; // next id number to delete (tag)
	//
	// pre load images 
	//
	//P.ap._img[P.ap._img.length] = new Array("iImg01","./i/headphone01.jpg"); // NOT using ALiBS.ag.ex() to load, use ALiBS.el.ss() instead
	//
	//
	// pre load modules
	//
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.rx"; // NOT Required immediately
	P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.dt"; // NOT Required immediately
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.ev"; // NOT Required immediately
	//
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.bn"; // NOT Required immediately
	//
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.b0"; // NOT Required immediately
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.b1"; // NOT Required immediately
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.b2"; // NOT Required immediately
	//
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.c3"; // NOT Required immediately
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.c4"; // NOT Required immediately
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.bn"; // NOT Required immediately
	//
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.c7"; // NOT Required immediately
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.c8"; // NOT Required immediately
	//
	//P.ap._pre_mod[P.ap._pre_mod.length] = "ALiBS.tp"; // NOT Required immediately
	//
	//P.ap._pre_img[P.ap._pre_img.length] = "./i/monitor01.jpg";
	//
	/*for (var i in ALiBS.gv.sy.mc) // log loaded modules here ??????????
	{
		ALiBS.sy.la(1,"ALiBS.gv.sy.mc["+i+"].f00= "+ALiBS.gv.sy.mc[i].f00,{o:"P.ap.al"});
	}*/
};
// *************************************************************************
// *************************************************************************
// *************************************************************************
// *************************************************************************
P.ap.span1 = function() // custom function
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.span01",le:P.ap.le});
	//
	var c = ALiBS.el.gi('iSpan01');
	ALiBS.sy.la(1,"c="+c,{o:"P.ap.span1"});
	//
	var d = ALiBS.el.gi('iSpan02');
	ALiBS.sy.la(1,"d="+d,{o:"P.ap.span1"});
	//
	if(c) ALiBS.el.si('iSpan02',c);
};
// *************************************************************************
P.ap.text1 = function() // custom function
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.text1",le:P.ap.le});
	//
	var i = ALiBS.ag.ex('texto.txt','f');
	//
	var x = new ALiBS.ag.tc(100,100);
	x.cb = function()
	{
		var c = ALiBS.el.gif(i);
		//
		if(c)
		{
			ALiBS.sy.la(1,"c(before)="+c,{o:"P.ap.text1"});
			c = ALiBS.sg.as(c.toString(),">");
			c = ALiBS.sg.bs(c,"<");
			c = ALiBS.sg.tr(c,null);
			ALiBS.sy.la(1,"c(after)="+c,{o:"P.ap.text1"});
			//
			ALiBS.el.si('iSpam02',c);
		}
		else xpto.xpto; // forçar erro
	};
	x.ex();
};
// *************************************************************************
P.ap.text2 = function() // custom function
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.text2",le:P.ap.le});
	//
	//var i = ALiBS.ag.ex('texto2.txt','f');
	//
	var a = new ALiBS.ax();
	a.cb = function(t,x,s) // callback - text, xml, status
	{
		ALiBS.sy.la(1,"callback text="+t,{o:"P.ap.text2"});
		ALiBS.sy.la(1,"callback xml="+x,{o:"P.ap.text2"});
		ALiBS.sy.la(1,"callback status="+s,{o:"P.ap.text2"});
		//
		var c = t;
		ALiBS.sy.la(1,"c(before)="+c,{o:"P.ap.text2"});
		c = ALiBS.sg.as(c.toString(),">");
		c = ALiBS.sg.bs(c,"<");
		c = ALiBS.sg.tr(c,null);
		ALiBS.sy.la(1,"c(after)="+c,{o:"P.ap.text2"});
		//
		ALiBS.el.si('iSpam02',c);
	};
	//
	a.ex('texto2.txt'); // execute
	//a.ex('texto2.txt',{t:false}); // execute
};
// *************************************************************************
P.ap.img1 = function() // custom function
{
	ALiBS.sy.la(6,"Method Execution Started...",{o:"P.ap.img1",le:P.ap.le});
	//
	var i = ALiBS.ag.ex('dm01_0000.gif','i');
	var i = ALiBS.ag.ex('dm02_0000.gif','i');
};
// *************************************************************************
P.ap.p0_up = function() // update page p0 HTML
{
	var a = new Array();
	//
	//a[a.length] = '<p><a href="#one" class="ui-btn ui-shadow ui-corner-all">Show page "one"</a></p>';
	//a[a.length] = '<p><a href="#two" class="ui-btn ui-shadow ui-corner-all">Show page "two"</a></p>';
	//a[a.length] = '<p><a href="#popup" class="ui-btn ui-shadow ui-corner-all" data-rel="dialog" data-transition="pop">Show page "popup" (as a dialog)</a></p>';
	//
	a[a.length] = '<p><a href="#i_uijmpg_div_page_page00" class="ui-btn ui-shadow ui-corner-all">Show page00</a></p>';
	a[a.length] = '<p><a href="#i_uijmpg_div_page_page01" class="ui-btn ui-shadow ui-corner-all">Show page01</a></p>';
	a[a.length] = '<p><a href="#i_uijmpg_div_page_dialog00" class="ui-btn ui-shadow ui-corner-all" data-rel="dialog" data-transition="pop">Show page "dialog00" (as a dialog)</a></p>';
	// 
	//a[a.length] = '<p><a href="#i_uijmpg_div_page_page00" class="ui-btn ui-shadow ui-corner-all" data-rel="dialog" data-transition="pop">Show page "page00" (as a dialog)</a></p>';
	//a[a.length] = '<p><a href="#i_uijmpg_div_page_page01" class="ui-btn ui-shadow ui-corner-all" data-rel="dialog" data-transition="pop">Show page "page01" (as a dialog)</a></p>';
	//
	return a.join("");
};
// *************************************************************************
P.ap.p0_up2 = function()	
{
/*
	T0 = new ALiBS.to.t();

	T0.nm.f00="campo0";
	T0.nm.f01="campo1";
	ALiBS.to.mn(T0);

	T0.ff.to.bcr = function(t,i){console.log("before create")}; 
	T0.ff.to.bup = function(t,i){console.log("before update")};
	T0.ff.to.bde = function(t,i){console.log("before delete")};
	//
	T0.ff.to.acr = function(t,i){console.log("after create")};
	T0.ff.to.aup = function(t,i){console.log("after update")};
	T0.ff.to.ade = function(t,i){console.log("after delete")};

	T0.new();
	T0.new();
	T0.new();

	T0.setf(0,{f00:"aaaa"});
	T0.set(0,{campo1:"bbbb"});

	T0.del(1);
	T0.del(2);

*/
	var z = {};
	//
	z.v_t0 = new ALiBS.to.t();
	//
	z.v_t0.ff.to.bcr = function(t,i){console.log("before create")}; 
	z.v_t0.ff.to.bup = function(t,i){console.log("before update")};
	z.v_t0.ff.to.bde = function(t,i){console.log("before delete")};
	//
	z.v_t0.ff.to.acr = function(t,i){console.log("after create")};
	z.v_t0.ff.to.aup = function(t,i){console.log("after update")};
	z.v_t0.ff.to.ade = function(t,i){console.log("after delete")};
	//
	z.v_t0.nm.f20 = "data-role"; // 
	z.v_t0.nm.f21 = "alibs.record.desc";
	z.v_t0.nm.f22 = "alibs.record.idx";
	z.v_t0.nm.f23 = "alibs.page.reference"; // reference
	z.v_t0.nm.f24 = "page-data-theme";

	z.v_t0.nm.f60 = "LPanel0-btn-text";
	z.v_t0.nm.f61 = "LPanel0-btn-data-icon";
	z.v_t0.nm.f62 = "LPanel0-data-display";


	z.v_t0.nm.f70 = "RPanel0-btn-text";
	z.v_t0.nm.f71 = "RPanel0-btn-data-icon";
	z.v_t0.nm.f72 = "RPanel0-data-display";

	z.v_t0.nm.f31 = "header-data-theme";
	z.v_t0.nm.f32 = "header-title";

	z.v_t0.nm.f41 = "main-data-theme"; // custom page id ????
	z.v_t0.nm.f42 = "main-title";

	z.v_t0.nm.f51 = "footer-data-theme";
	z.v_t0.nm.f52 = "footer-title";
	//

	//
	ALiBS.to.mn(z.v_t0); // create reverse fields
	//
	// *******************
	//
	z.v_rc = z.v_t0.new();
	//
	z.v_t0.set(z.v_rc,
		{
			"data-role":		"page", 
			"alibs.record.desc":	"my description",
			"alibs.record.idx":	"page00", 
			"alibs.page.reference":	"page00", 
			"page-data-theme":	"a", 

			//"LPanel0-btn-text":	"LPanel",
			"LPanel0-btn-data-icon":"arrow-l,bars,gear",
			"LPanel0-data-display":	"overlay",

			//"RPanel0-btn-text":	"RPanel",
			"RPanel0-btn-data-icon":"gear,bars,arrow-r",
			"RPanel0-data-display":	"overlay",

			"header-data-theme":	"a",
			"header-title":		"Header page00", 

			"main-data-theme":	"a",
			"main-title":		"Main page00", 

			"footer-data-theme":	"b", 
			"footer-title":		"Footer page00"
		});
	//
	z.v_rc = z.v_t0.new();
	//
	z.v_t0.setf(z.v_rc,
		{
			f20: "page", // data-role
			f21: "my description",
			f22: "page01", // index
			f23: "page01", // page reference
			f24: "a", // page theme

			f60: "LPanel",
			f61: "bars",
			f62: "reveal",

			f70: "RPanel",
			f71: "gear",
			f72: "reveal",

			f31: "a", // header theme
			f32: "Header page01", // <h1> header title/text

			f42: "Main page01", // <h2> main title/text

			f51: "b", // footer theme
			f52: "Footer page01" // <h4> footer title/text
		});
	//
	z.v_rc = z.v_t0.new();
	//
	z.v_t0.setf(z.v_rc,
		{
			f20: "page", // data-role
			f21: "my description",
			f22: "dialog00", // index
			f23: "dialog00", // page reference
			f24: "a", // page theme

			f60: "LPanel",
			f61: "bars",
			f62: "reveal",

			f70: "RPanel",
			f71: "gears",
			f72: "reveal",

			f31: "a", // header theme
			f32: "Header dialog00", // <h1> header title/text

			f42: "Main dialog00", // <h2> main title/text

			f51: "b", // footer theme
			f52: "Footer dialog00" // <h4> footer title/text
		});
	//
	z.v_s0 = ALiBS.ui.jmpg(z.v_t0);
	console.log("z.v_s0="+z.v_s0);
	//
	ALiBS.ui.jmsw("one","lpanel1","rpanel1");
	ALiBS.ui.jmsw("i_uijmpg_div_page_page00","i_uijmpg_div_lpanel0_page00","i_uijmpg_div_rpanel0_page00");
	ALiBS.ui.jmsw("i_uijmpg_div_page_page01","i_uijmpg_div_lpanel0_page01","i_uijmpg_div_rpanel0_page01");
	//
	ALiBS.el.ge("body").lastChild.insertAdjacentHTML("afterend",z.v_s0);
	//
	var a = P.ap.p0_up();
	ALiBS.el.si('i_uijmpg_div_main_page00',a);
	ALiBS.el.si('i_uijmpg_div_main_page01',a);
	ALiBS.el.si('i_uijmpg_div_main_dialog00',a);
	//
	//ALiBS.ui.jmsw("demo-page","left-panel","right-panel");
};
// *************************************************************************
// *************************************************************************
ALiBS.sy.la(0,"loaded module ap.js"+ALiBS.gv.sy.se,{o:"ap.js"});
//
P.ap.ex(); // execute page
// *************************************************************************

