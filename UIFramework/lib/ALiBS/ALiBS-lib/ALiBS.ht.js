/* *************************************************************************
// INFO:

----------------------------------------------------------------------------

Step 1:

ALiBS.el.si("iALiBS_k0",ALiBS.ht.t0("iALiBS_k0",{ty:1,oc:"val",ok:"sub"}));

function val(v)
{
	alert("val.Valor="+v);
};

function sub(v)
{
	alert("sub.Valor="+v);
};

----------------------------------------------------------------------------

Step 2:

<div id=iALiBS_teclado0>xxxx</div>

----------------------------------------------------------------------------

// *************************************************************************
// TODO

- t1: teclado alphanumérico
- t2: teclado igual ao do Santander  0|1 2|3 4|5 6|7 8|9
- t3: calculadora simples
- t4: calculadora avançada ????

*/
//
// *************************************************************************
/** 
	Class ALiBS.ht

		Description: 
			- Required Class for All Others;
			- Creates ALiBS Namespace;
			- Modules Name Cache;
			- Log Management;

	--------------------------------------------------------------------
*/
//
if(!ALiBS.gv.ht) ALiBS.gv.ht = {}; // global values
//
if(!ALiBS.gv.ht.le) ALiBS.gv.ht.le = 0; // local log status
//
ALiBS.gv.ht.nn = 0; // x times loaded
//
ALiBS.gv.ht.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ht.t = {}; // keyboards cache
//
// *************************************************************************
//
ALiBS.ht = // HTML Teclados / Keyboards
{
	sc: function(i,v) // print value in screen
	/** 
	Method <static> ALiBS.ht.sc(v)

		Description: Update Screen Value

		Parameters:
			i = (required)(String)element id
			v = (required)(String)value level 
	
		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{
		var v0 = ALiBS.el.gv(i);
		ALiBS.el.sv(i,v0+v);
	},
	// *****************************************************************
	cl: function(i) // clear screen
	/** 
	Method <static> ALiBS.ht.cl(v)

		Description: Clear Screen Value

		Parameters:
			i = (required)(String)element id
	
		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{
		var v0 = ALiBS.el.gv(i);
		ALiBS.el.sv(i,"");
	},
	// *****************************************************************
	oc: function(p) // onclick 
	{
		// should be empty
	},
	// *****************************************************************
	ok: function(p) // on OK
	{
		// should be empty
	},
	// *****************************************************************
	t0: function(n,p) // teclado numérico
	/** 
	Method <static> ALiBS.ht.t0(p)

		Description: Log Messages 

		Parameters:
			n = (required)(String)Keyboard Name
			p = (optional)(JSON)parameters {ty:1,oc:"func"} 
				ty = (String)type 
					1=text(Defaut)
					2=password
				oc = (String)onclick function name+parameters = func(this.value)
				ok = (String)on OK function name+parameters = func(this.value)
	
		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{
		ALiBS.gv.ht.t[n] = {};
		ALiBS.gv.ht.t[n].t = "t0"; // t0,t1,etc
		ALiBS.gv.ht.t[n].i0 = "i_htt0_input0_"+n;
		ALiBS.gv.ht.t[n].d0 = "i_htt0_div0_"+n;
		//
		var csc = "ALiBS.ht.sc('"+ALiBS.gv.ht.t[n].i0+"',this.value);"; // command sc
		var ccl = "ALiBS.ht.cl('"+ALiBS.gv.ht.t[n].i0+"');"; // command cl
		//
		var ty = "text"; // Default = 1 = text
		var oc = "ALiBS.ht.oc(this.value)";
		var ok = "ALiBS.ht.ok"+"(ALiBS.el.gv(ALiBS.gv.ht.t['"+n+"'].i0))";
		//
		if(p)
		{
			if(typeof(p.ty)=="number")
			{
				if(p.ty==2)
				{
					ty = "password";
				}
				else
				{
					ty = "text";
				}
			}
			//
			if(typeof(p.oc)=="string") oc = p.oc+"(this.value)";
			//
			if(typeof(p.ok)=="string") ok = p.ok+"(ALiBS.el.gv(ALiBS.gv.ht.t['"+n+"'].i0))";
		}
		//
		var a = new Array();
		//
		//a[a.length] = '<div id="iT0Div0" style="position:absolute;visibility:hidden;">';
		a[a.length] = '<div id="'+ALiBS.gv.ht.t[n].d0+'" >';
		a[a.length] = '<table width="100" border="0"><tbody>';
		//
		a[a.length] = '<tr><td colspan="3">';
		a[a.length] = '<input type="'+ty+'" id="'+ALiBS.gv.ht.t[n].i0+'" disabled="disabled" style="width:95%" />';
		a[a.length] = '</td></tr>';
		//
		a[a.length] = '<tr>';
		a[a.length] = '<td align="left"><input type="button" value="1" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td align="center"><input type="button" value="2" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td align="right"><input type="button" value="3" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<tr>';
		//
		a[a.length] = '<tr>';
		a[a.length] = '<td align="left"><input type="button" value="4" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td><input type="button" value="5" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td align="right"><input type="button" value="6" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '</tr>';
		//
		a[a.length] = '<tr>';
		a[a.length] = '<td align="left"><input type="button" value="7" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td><input type="button" value="8" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td align="right"><input type="button" value="9" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '</tr>';
		//
		a[a.length] = '<tr>';
		a[a.length] = '<td><input type="button" value="CL" title="CL" onclick="'+ccl+'" /></td>';
		a[a.length] = '<td><input type="button" value="0" onclick="'+csc+oc+'" /></td>';
		a[a.length] = '<td><input type="submit" value="OK" onclick="'+ok+'" title="OK" /></td>';
		a[a.length] = '</tr>';
		//
		a[a.length] = '</tbody></table>';
		a[a.length] = '</div>';
		//
		return a.join("");
	},
	// *****************************************************************
	t1: function(n,p) // teclado numérico
	/** 
	Method <static> ALiBS.ht.t0(p)

		Description: Log Messages 

		Parameters:
			n = (required)(String)Keyboard Name
			p = (optional)(JSON)parameters {ty:1,oc:"func"} 
				ty = (String)type 1=text(Defaut), 2=password
				oc = (String)onclick function name
	
		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{
		ALiBS.gv.ht.t[n] = {};
		ALiBS.gv.ht.t[n].t = "t1"; // t0,t1,etc
		ALiBS.gv.ht.t[n].i0 = "i_htt1_input0_"+n;
		ALiBS.gv.ht.t[n].d0 = "i_htt1_div0_"+n;
		//
		var csc = "ALiBS.ht.sc('"+ALiBS.gv.ht.t[n].i0+"',this.value);"; // command sc
		var ccl = "ALiBS.ht.cl('"+ALiBS.gv.ht.t[n].i0+"');"; // command cl
		//
		var ty = "text"; // Default = 1 = text
		var oc = "ALiBS.ht.oc(this.value)";
		var ok = "ALiBS.ht.ok"+"(ALiBS.el.gv(ALiBS.gv.ht.t['"+n+"'].i0))";
		//
		if(p)
		{
			if(typeof(p.ty)=="number")
			{
				if(p.ty==2)
				{
					ty = "password";
				}
				else
				{
					ty = "text";
				}
			}
			//
			if(typeof(p.oc)=="string") oc = p.oc+"(this.value)";
			//
			if(typeof(p.ok)=="string") ok = p.ok+"(ALiBS.el.gv(ALiBS.gv.ht.t['"+n+"'].i0))";
		}
		//
		var a = new Array();
		//
		//a[a.length] = '<div id="iT0Div0" style="position:absolute;visibility:hidden;">';
		//a[a.length] = '<div id="'+ALiBS.gv.ht.t[n].d0+'" style="position:absolute;">';
		a[a.length] = '<div id="'+ALiBS.gv.ht.t[n].d0+'" >';
		//
		a[a.length] = '<div>';
		a[a.length] = '<input type="'+ty+'" id="'+ALiBS.gv.ht.t[n].i0+'" disabled="disabled" size="13px" />';
		a[a.length] = '</div>';
		//
		a[a.length] = '<div>';
		a[a.length] = '<input type="button" value="1" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="button" value="2" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="button" value="3" onclick="'+csc+oc+'" />';
		//
		a[a.length] = '<div>';
		a[a.length] = '<input type="button" value="4" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="button" value="5" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="button" value="6" onclick="'+csc+oc+'" />';
		a[a.length] = '</div>';
		//
		a[a.length] = '<div>';
		a[a.length] = '<input type="button" value="7" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="button" value="8" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="button" value="9" onclick="'+csc+oc+'" />';
		a[a.length] = '</div>';
		//
		a[a.length] = '<div>';
		a[a.length] = '<input type="button" value="CL" title="CL" onclick="'+ccl+'" />';
		a[a.length] = '<input type="button" value="0" onclick="'+csc+oc+'" />';
		a[a.length] = '<input type="submit" value="OK" onclick="'+ok+'" title="OK" />';
		a[a.length] = '</div>';
		//
		a[a.length] = '</div>';
		//
		return a.join("");
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ht("+(++ALiBS.gv.ht.nn)+"x)",{o:"ALiBS.ht"});
ALiBS.sy.la(0+0,"ALiBS.gv.ht.le="+ALiBS.gv.ht.le+ALiBS.gv.sy.se,{o:"ALiBS.ht"});

