// *************************************************************************
//
//ALiBS.gv.sy.so="immediate-console";
//
if(!ALiBS.gv.TS0002) ALiBS.gv.TS0002 = {}; // global values
//
if(!ALiBS.gv.TS0002.le) ALiBS.gv.TS0002.le = 0; // local log status
//
ALiBS.gv.TS0002.rl = "15.0603.1455";
//
// *************************************************************************
//
var TS0002 = new ALiBS.to.t({tb_rf:"TS0002",tb_id:"_TS0002"}); 
//
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{

}
//
// *************************************************************************
//
else // Browser / Client
{
/*
	// TS0002P = Logs de Eventos do Sistema (tipo sentinel)
	// Number of Records: Many Records
	// Type of Content: Data


f33",f23:"Event ID",			f24:"",				f26:"15.0515"};
f34",f23:"Event ID 2",			f24:"",				f26:"15.0515"};
f35",f23:"Event ID 3",			f24:"",				f26:"15.0515"};
f36",f23:"Event ID 4",			f24:"",				f26:"15.0515"};

*/

	TS0002.tb.tg = "TS0002";
	TS0002.tb.ty = "fghij";
	//
	// *************************************************
	//
	TS0002.nm.f21 = "SourceSystem"; 
	TS0002.cp.f21 = "Source System";
	TS0002.tt.f21 = "Source System (Title)";
	TS0002.dc.f21 = "Source System (Description)";
	TS0002.dv.f21 = "[EMPTY]";
	TS0002.ob.f21 = "Source System (Obs)";
	TS0002.cd.f21 = "15.0701";
	//
	TS0002.nm.f22 = "SourceEnvironment"; 
	TS0002.cp.f22 = "Source Environment";
	TS0002.tt.f22 = "Source Environment (Title)";
	TS0002.dc.f22 = "Source Environment (Description)";
	TS0002.dv.f22 = "[EMPTY]";
	TS0002.ob.f22 = "Source Environment (Obs)";
	TS0002.cd.f22 = "15.0701";
	//
	TS0002.nm.f23 = "SourceEntity"; 
	TS0002.cp.f23 = "Source Entity";
	TS0002.tt.f23 = "Source Entity (Title)";
	TS0002.dc.f23 = "Source Entity (Description)";
	TS0002.dv.f23 = "[EMPTY]";
	TS0002.ob.f23 = "Source Entity (Obs)";
	TS0002.cd.f23 = "15.0701";
	//
	TS0002.nm.f24 = "ExecutantID"; 
	TS0002.cp.f24 = "Executant ID";
	TS0002.tt.f24 = "Executant ID (Title)";
	TS0002.dc.f24 = "Executant ID (Description)";
	TS0002.dv.f24 = "[EMPTY]";
	TS0002.ob.f24 = "Executant ID (Obs)";
	TS0002.cd.f24 = "15.0701";
	//
	// ****************************************
	//
	TS0002.nm.g21 = "DestinationSystem"; 
	TS0002.cp.g21 = "Destination System";
	TS0002.tt.g21 = "Destination System (Title)";
	TS0002.dc.g21 = "Destination System (Description)";
	TS0002.dv.g21 = "[EMPTY]";
	TS0002.ob.g21 = "Destination System (Obs)";
	TS0002.cd.g21 = "15.0701";
	//
	TS0002.nm.g22 = "DestinationEnvironment"; 
	TS0002.cp.g22 = "Destination Environment";
	TS0002.tt.g22 = "Destination Environment (Title)";
	TS0002.dc.g22 = "Destination Environment (Description)";
	TS0002.dv.g22 = "[EMPTY]";
	TS0002.ob.g22 = "Destination Environment (Obs)";
	TS0002.cd.g22 = "15.0701";
	//
	TS0002.nm.g23 = "DestinationEntity"; 
	TS0002.cp.g23 = "Destination Entity";
	TS0002.tt.g23 = "Destination Entity (Title)";
	TS0002.dc.g23 = "Destination Entity (Description)";
	TS0002.dv.g23 = "[EMPTY]";
	TS0002.ob.g23 = "Destination Entity (Obs)";
	TS0002.cd.g23 = "15.0701";
	//
	TS0002.nm.g24 = "ReceiverID"; 
	TS0002.cp.g24 = "Receiver ID";
	TS0002.tt.g24 = "Receiver ID (Title)";
	TS0002.dc.g24 = "Receiver ID (Description)";
	TS0002.dv.g24 = "[EMPTY]";
	TS0002.ob.g24 = "Receiver ID (Obs)";
	TS0002.cd.g24 = "15.0701";
	//
	// ****************************************
	//
	TS0002.nm.h21 = "EventTimestamp"; 
	TS0002.cp.h21 = "Event Timestamp";
	TS0002.tt.h21 = "Event Timestamp (Title)";
	TS0002.dc.h21 = "Event Timestamp (Description)";
	TS0002.dv.h21 = "[EMPTY]";
	TS0002.ob.h21 = "Event Timestamp (Obs)";
	TS0002.cd.h21 = "15.0701";
	//
	TS0002.nm.h22 = "EventStatusCode"; 
	TS0002.cp.h22 = "Event Status Code (Caption)";
	TS0002.tt.h22 = "Event Status Code (Title)";
	TS0002.dc.h22 = "Event Status Code (Description)";
	TS0002.dv.h22 = "[EMPTY]";
	TS0002.ob.h22 = "Event Status Code - da tabela de código de erros ? (Obs)";
	TS0002.cd.h22 = "15.0701";
	//
	// ****************************************
	//
	TS0002.nm.h41 = "Action";  
	TS0002.cp.h41 = "Action (Caption)";
	TS0002.tt.h41 = "Action (Title)";
	TS0002.dc.h41 = "Action (Description)";
	TS0002.dv.h41 = "[EMPTY]";
	TS0002.ob.h41 = "Action - CRUDE (Obs)";
	TS0002.cd.h41 = "15.0701";
	//
	TS0002.nm.h42 = "Error Code";  
	TS0002.cp.h42 = "Error Code (Caption)";
	TS0002.tt.h42 = "Error Code (Title)";
	TS0002.dc.h42 = "Error Code (Description)";
	TS0002.dv.h42 = "[EMPTY]";
	TS0002.ob.h42 = "Error Code (Obs)";
	TS0002.cd.h42 = "15.0701";
	//
	TS0002.nm.h43 = "Message";  
	TS0002.cp.h43 = "Message (Caption)";
	TS0002.tt.h43 = "Message (Title)";
	TS0002.dc.h43 = "Message (Description)";
	TS0002.dv.h43 = "[EMPTY]";
	TS0002.ob.h43 = "Message - rather zipped (Obs)";
	TS0002.cd.h43 = "15.0701";
	//
	// *************************************************
	//
	TS0002.nm.i41 = "CustomID1"; 
	TS0002.cp.i41 = "Custom ID 1 (Caption)";
	TS0002.tt.i41 = "Custom ID 1 (Title)";
	TS0002.dc.i41 = "Custom ID 1 (Description)";
	TS0002.dv.i41 = "[EMPTY]";
	TS0002.ob.i41 = "Custom ID 1 (Obs)";
	TS0002.cd.i41 = "15.0701";
	//
	// *************************************************
	//
	TS0002.tb.hg.t = 1; // DEFAULT = null = NOT SET = field title control = (see also ALiBS.gv.hg.tt)
	//
	//
	TS0002.tb.hx = [{ty:"[HEADERHX]",tx:"[ TS0002 - Users ]",c:"white",bg:"red",al:"center"}]; // Header hx;
	//
	TS0002.tb.pg.css.hx = 1; // header hx style ON/OFF (0=OFF=NOT CHANGE, 1=ON=CHANGE)
	//
	TS0002.tb.pg.n = 13; // paging = number of records to show (0 = show all)
	//
	//TS0002.tb.nso = "a01"; // fields in grid NOT to be sorted
	//
	TS0002.tb.sh = ["f21","f22","f23","f23","f24","g21","g22","g23","g24","h21","h22","h41","h42","h43","i41"]; // fields to show
	//
	TS0002.tb.fa = "fm"; // active filter
	//
	// *************************************************
	//
	TS0002.tb.fk.f21 = [{ty:"[TABLEINFO]",tx:"Info",c:"red",bg:"white"}]; // table information button
	TS0002.tb.fk.f05 = [{ty:"[PAGING]",tx:"Pg",c:"red",bg:"white"}]; // Paging buttons = Navigation
	TS0002.tb.fk.f03 = [{ty:"[COMPLEXFILTER]",tx:"CF",c:"red",bg:"white",t:"[]",v:"fm;x"},
									{t:"fm:match",v:"fm;x"},
									{t:"fe:equals",v:"fe;x"},{t:"res>=1",v:"fc;(a00>=1)"},{t:"res>=3",v:"fc;(a00>=3)"}]; // complex filter
	//
	TS0002.tb.fk.f04 = [{ty:"[PAGELINES]",tx:"Lines",l:"4,5,6,7,8,9,10,11,12,13,20",c:"red",bg:"white"}]; // Page Lines
	TS0002.tb.fk.f01 = [{ty:"[FONTSIZE]",tx:"Fon",l:"7px,8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,30px",c:"red",bg:"white"}]; // Font size
	TS0002.tb.fk.f02 = [{ty:"[FONTFAMILY]",tx:"Fam",l:"Verdana,Arial",c:"red",bg:"white"}]; // Font family
	//
	TS0002.tb.fk.f00 = [{ty:"[FONTSTYLE]",tx:"Sty",l:"normal,italic,oblique",c:"red",bg:"white"}]; // Font family
	TS0002.tb.fk.f06 = [{ty:"[FONTWEIGHT]",tx:"Wei",l:"normal,bold,bolder,lighter,100,200,300,400,500,600,700,800,900",c:"red",bg:"white"}]; // Font family
	//
	// *************************************************
	TS0002.ff.hg.dg = function(t,n,p) // display GRID
	// t = ()table
	// n = ()sorted field
	// p = (optional)(JSON)parameter {}
	//	rv = (boolean)reverse sort (default=false)
	//	y = (String)field type [abcdefghijkrx] (default=f)
	{
		var z = p || {}; // [BEST-PRACTICE]
		//
		if(n==t.so.g) ALiBS.to.o(t,n,{rv:!t.so.rv,y:z.y}); // reverse
		else ALiBS.to.o(t,n,{y:z.y}); // normal sort
		//
		var a = ALiBS.hg.g0(t,"nome",{cs:"hg_css0F"}); // 4101, font-size: 16px; padding:2px;
		ALiBS.el.si("iContGrid02a",a);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
	};
	// *************************************************
	TS0002.ff.hg.af = function(t,p) // after load = atualizar datas da agenda
	// t = (ALiBS.to)
	// f02 = (String)data (para ordenar table)
	// f05 = (String)valor 
	// f11 = (String)saldo conta 
	{
		var z = {}; // [BEST-PRACTICE]
		//
		var r = 1;
		//
		// ********************************
		//
		ALiBS.to.o(t,21,{y:"f"});
		t.tb.fi["f21"] = "[]," + ALiBS.to.rd(t,{y:"f",g:21,ke:2}).join(",");
//console.log(t.tb.fi["f21"]);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	};
	// *************************************************
} // endif("Server")
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module TS0002",{o:"TS0002"});
ALiBS.sy.la(0+0,"ALiBS.gv.TS0002.rl="+ALiBS.gv.TS0002.rl,{o:"TS0002"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.TS0002.le="+ALiBS.gv.TS0002.le+ALiBS.gv.sy.se,{o:"TS0002"});

