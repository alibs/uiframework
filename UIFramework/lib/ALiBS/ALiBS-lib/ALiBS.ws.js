/* *************************************************************************
// INFO:

// Teste de parametros/objetos {}

// no iManager
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=100
//
// Memoria: 
// http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=1003

// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=1003
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=1004
//
// Token
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=2000    - PASS - b64
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=2001    - Token - b64
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=2002    - OTP - b64
//
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=2000&p1=admin.servers.sistema.bradesco&p2=N0v3ll
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=2001&p1=admin.servers.sistema.bradesco
// https://192.168.9.80:8443/nps/AP-Token/wsjs.jsp?cb=cb&cmd=2002&p1=admin.servers.sistema.bradesco&p2=0000
//
// *****************************************************************
//

// *************************************************************************
// TODO(2010-09-08):

TODO(2010-09-08): 

*/
//
// *************************************************************************
//
if(!ALiBS.gv.ws) ALiBS.gv.ws = {}; // global values
//
if(!ALiBS.gv.ws.le) ALiBS.gv.ws.le = 0; // local log status
//
ALiBS.gv.ws.nn = 0; // x times loaded
//
ALiBS.gv.ws.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ws.rl = "15.0617.1718";
//
// *************************************************************************
//
ALiBS.gv.ws.uc = {}; // url / cmd cache
//
// *************************************************************************
//
ALiBS.gv.ws.au = {}; // authentication cache
//
ALiBS.gv.ws.au["ALiBS"] = // ALiBS Authentication
{
	rn: "ALiBS", // reference name
	st: 0, // status = 0=disabled, 1=enabled
	f0: null // function to authentication
};
//
/*
ALiBS.gv.ws.au["iManager"] = // iManager Authentication
{
	rn: "iManager", // reference name
	st: 0 // status = 0=disabled, 1=enabled
};
//
ALiBS.gv.ws.au["PortalWF"] = // PortalWF Authentication
{
	rn: "PortalWF", // reference name
	st: 0 // status = 0=disabled, 1=enabled
};
*/
//
// *************************************************************************
//
ALiBS.ws = // Web Server = wsjs = OLD jsws
{
	// *****************************************************************
	js: function(cc,rq,rp,p) // run js
	// cc = (required)(session) Session // OLD cc = (required)(connection) Connection Control / Session (javax.naming.directory.DirContext)
	// rq = (required)(HttpServletRequest) page request
	// rp = (required)(HttpServletResponse) page response
	//
	//requestHttp TODO(2014-06-03): needed?
	//responseHttp TODO(2014-06-03): needed?
	//
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	// return = (string) return value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			// *************************************************
			//
			z.ec = -1000; 
			z.pa = ALiBS.ag.uz("?" + rq["queryString"]); // use "?" to indicate it's coming from client
			//
			z.ec = -1030; 
			r = z.pa.cmd + " NOT Registered in ALiBS.gv.ws.uc"; // [BEST-PRACTICE]
			//
			z.ec = -1050; 
			z.pa.za = z.za;
			z.pa.zb = z.zb;
			z.pa.zc = z.zc;
			//
			z.ec = -1100; 
			for(var i in z.pa)
			{
				ALiBS.sy.la(5+0,i+"="+z.pa[i],{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
			}
			//
			// ****************
			//
			z.ok = 1; // start value
			//
			z.ec = -1200; 
			for(var i in ALiBS.gv.ws.au) // i = rn
			{
				ALiBS.sy.la(5+0,"ALiBS.gv.ws.au[" + i + "][rn]=" + ALiBS.gv.ws.au[i]["rn"],{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
				ALiBS.sy.la(5+0,"ALiBS.gv.ws.au[" + i + "][st]=" + ALiBS.gv.ws.au[i]["st"],{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
				//
				z.ec = -1300; 
				if(ALiBS.gv.ws.au[i]["st"]==1 && typeof(ALiBS.gv.ws.au[i]["f0"])!="undefined") // Authentication active
				{
					z.ec = -1400; 
					z.ok = ALiBS.gv.ws.au[i]["f0"](cc,rq,rp,z.pa); // validate authentication // TODO:(2014-06-15):should return 0 / 1
					//
					if(z.ok==0) // failed authentication / validation
					{
						ALiBS.sy.la(5+0,"z.ok="+z.ok,{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
						//
						break; 
					}
				}
				else
				{
					//
				}
			}
			//
			// ****************
			//
			if(z.ok) // Authentication OK or Authentication disabled
			{
				z.ec = -2000; 
				for(var i in ALiBS.gv.ws.uc)
				{
					ALiBS.sy.la(7+0,"Registered cmd = ALiBS.gv.ws.uc["+i+"]",{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
					//
					z.ec = -2100; 
					if(z.pa.cmd.indexOf(i)>=0) // i inside cmd
					{
						z.ec = -2200; 
						r = ALiBS.gv.ws.uc[i](cc,rq,rp,z.pa); // unzipped z.pa instead of zipped p
						//
						break;
					}
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.ws.js",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			ALiBS.sy.la(0+0,"[Exception]Pr"+"inting Log...",{o:"ALiBS.ws.js",so:"pr"+"int"});
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
		//
		if(!z.pa.cb)
		{
			// r = r; 
			//
			// http://docs.oracle.com/javaee/6/api/javax/servlet/http/HttpServletResponse.html
			rp.setStatus(200);
			rp.setContentType("text/javascript"); // TODO(2015-06-17): REVER
		}
		else if(z.pa.cb == "html")
		{
			r = '<' + '!DOCTYPE HTML PUBLIC "' + '-' + '/' + '/W3C/' + '/DTD HTML 4.01/' + '/EN"' + 
				'"http:/' + '/www.w3.org/TR/html4/strict.dtd"' + '>' + 
				"<ht" + "ml>" + "<he" + "ad>" +
				'<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">' +
				"</he" + "ad><bo" + "dy>" + r + "</bo" + "dy></ht" + "ml>";
			//
			// http://docs.oracle.com/javaee/6/api/javax/servlet/http/HttpServletResponse.html
			rp.setStatus(200);
			rp.setContentType("text/html");
		}
		else
		{
			r = z.pa.cb + "(" + "'" + r + "'" + ");"; // == Return (trocado por causa do retorno de ALiBS.to)
			//
			// http://docs.oracle.com/javaee/6/api/javax/servlet/http/HttpServletResponse.html
			rp.setStatus(200);
			rp.setContentType("text/javascript");
		}
		//
		ALiBS.sy.la(5+0,"(cb)r.length="+r.length,{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
		//
		ALiBS.sy.la(5+0,"(cb)r="+r,{o:"ALiBS.ws.js",le:ALiBS.gv.ws.le});
		//
		ALiBS.sy.la(0+0,"Pr"+"inting Log...",{o:"ALiBS.ws.js",so:"pr"+"int"});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	wsc: function(cc,rq,rp,p) // ws cmd 
	// cc = (required)(session) Session // OLD cc = (required)(connection) Connection Control / Session (javax.naming.directory.DirContext)
	// rq = (required)(HttpServletRequest) page request
	// rp = (required)(HttpServletResponse) page response
	//
	//requestHttp TODO(2014-06-03): needed?
	//responseHttp TODO(2014-06-03): needed?
	//
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	// return = (string) return value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ws.wsc",le:ALiBS.gv.ws.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = z.cmd + " Registered but NOT found in ALiBS.ws.wsc()"; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1001; 
			z.cmd0 = z.cmd.replace("ALiBS.ws.",""); // remains p8, t8, o8, etc
			//
			z.ec = -1050; 
			if(z.cmd0 && ALiBS.ws[z.cmd0])
			{
				z.ec = -1070; 
				r = ALiBS.ws[z.cmd0](cc,rq,rp,p);
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("ServerTimestamp0")>=0)
			{
				z.ec = -1100; 
				r = "" + ((new Date())-1+1);
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("session.getAttribute")>=0)
			{
				z.ec = -1200; 
				if(z.p0=="test0")
				{
					z.ec = -1250; 
					r = cc.getAttribute("test0");
				}
				else
				{

				}
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("1100")>=0)
			{
				z.ec = -1300; 
				r = z.za + " , " + z.zb + " , " + z.zc;
			}
			//
			// *************************************************
			//
			else
			{
				//
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.ws.wsc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.ws.wsc",le:ALiBS.gv.ws.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	}
	// *****************************************************************
};
//
// *************************************************************************
//
ALiBS.gv.ws.uc["ALiBS.ws.wsc.if.ServerTimestamp0"] = ALiBS.ws.wsc; // 
//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.ServerTimestamp0
//
ALiBS.gv.ws.uc["ALiBS.ws.wsc.if.ServerTimestamp1"] = ALiBS.ws.wsc; // Error test for ALiBS.ws.wsc
//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.ServerTimestamp1
//cb('ALiBS.ws.wsc.if.ServerTimestamp1 Registered but NOT found in ALiBS.ws.wsc()');
//
//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.ServerTimestamp2
//cb('ServerTimestamp2 NOT Registered in ALiBS.gv.ws.uc');
//
ALiBS.gv.ws.uc["ALiBS.ws.wsc.if.1100"] = ALiBS.ws.wsc; // parameters in wsjs.jsp
//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.1100
//
ALiBS.gv.ws.uc["ALiBS.ws.wsc.if.session.getAttribute"] = ALiBS.ws.wsc; // test for session - 
//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.session.getAttribute&p0=test0
//
//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.session.getAttribute&p0=test1
//cb('ALiBS.ws.wsc.if.session.getAttribute Registered but NOT found in ALiBS.ws.wsc()');
//
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_ws_js")>=0)
		{
			ALiBS_ws_js = function(cc,rq,rp,p) // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_ws_js",le:ALiBS.gv.ws.le});
				//
				return ALiBS.ws.js(cc,rq,rp,p);
			};
		}
	}
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ws("+(++ALiBS.gv.ws.nn)+"x)",{o:"ALiBS.ws"});
ALiBS.sy.la(0+0,"ALiBS.gv.ws.rl="+ALiBS.gv.ws.rl,{o:"ALiBS.ws"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.ws.le="+ALiBS.gv.ws.le+ALiBS.gv.sy.se,{o:"ALiBS.ws"});
//
// *************************************************************************
//
//	SAMPLE OF FUNCTIONS TO BE OVERWRITTEN / EXTENDED
/*

*/
//
// *************************************************************************

