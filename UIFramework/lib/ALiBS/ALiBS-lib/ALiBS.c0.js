/* *************************************************************************
// INFO:

 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  AES implementation in JavaScript (c) Chris Veness 2005-2012                                   
   - see http://csrc.nist.gov/publications/PubsFIPS.html#197                                    
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  

// http://www.movable-type.co.uk/scripts/aes.html
//
// Javascript Cryptography Considered Harmful
// http://www.matasano.com/articles/javascript-cryptography/
//

Links Interessantes:

	http://www.chilkatforum.com/questions/4324/does-the-length-of-a-password-for-aes-encryption-make-it-more-secure


	http://stackoverflow.com/questions/5215413/what-length-password-equals-256bits-of-entropy


		If you're using only letters and numbers, then you've got a total of 26 × 2 + 10 = 62 possible values per character. 
			That's close to 64, so you have just under 6 bits of entropy per character.

		If you want 256 bits, then you need about 43 characters from your character set.

		You might do better to take a long pass-phrase and generate a 256-bit hash of that (SHA-256, perhaps). 
			Your pass-phrase might be a miniature essay - maybe 80-128 characters long; more would not hurt.

Entropy = Expoente

	base64 = 64 bytes = 2^6 = entropy = 6

	base256 = 256 bytes = 2^8 = entropy = 8


https://en.wikipedia.org/wiki/PBKDF2

	PBKDF2 é o mesmo que usar Super-Hash ? 
		Eu tenho SuperHash em MD5 e SHA-512

// *************************************************************************
// TODO:


*/
//
// *************************************************************************
//
if(!ALiBS.gv.c0) ALiBS.gv.c0 = {}; // global values
//
if(!ALiBS.gv.c0.le) ALiBS.gv.c0.le = 0; // local log status
//
ALiBS.gv.c0.nn = 0; // x times loaded
//
ALiBS.gv.c0.rl = "15.0731.1516";
//
// *************************************************************************
//
ALiBS.c0 = // AES Cryptography
{
	en: function(w0,ms,p) // Aes.Ctr.encrypt
	// Encrypt a text using AES encryption in Counter mode of operation - Unicode multi-byte character safe
	//
	/** 
	Method <static> <string> ALiBS.c0.en(w0,ms,p)

		Description: AES Encrypt

		Parameters:
			w0 = (required)(string) password 
			ms = (required)(string) message to be encrypted

			p = (optional)(JSON)parameter {}
				b0 = (number) bits 
					256 = DEFAULT (128,192,256)

		Observations:
	
		Return Value:
			<static> <string> Encrypted text

		Example(s):
			var ok = ALiBS.c0.en(w0,ms);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

		- Esta implementação de AES transforma qualquer password em uma key de 256 bits
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(w0)!="string") throw "ALiBS.c0.en.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(ms)!="string") throw "ALiBS.c0.en.Exception: Error: Invalid Parameter Type: (string) n0 passed as "+typeof(n0); // 
			//
			ALiBS.sy.la(5+0,"w0="+w0,{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"ms="+ms,{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_b0 = z.b0; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_b0)!="number" || (!(z.p_b0==128 || z.p_b0==192 || z.p_b0==256)) ) z.p_b0 = 256;  // standard allows 128/192/256 bit keys
			//
			ALiBS.sy.la(5+0,"z.p_b0="+z.p_b0,{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_bs = 16;  // block size fixed at 16 bytes / 128 bits (b4=4) for AES
			//ms = Utf8.encode(ms);
			//w0 = Utf8.encode(w0);
			w0 = ALiBS.c6.en(w0);
			//
//var t = new Date();  // timer
			//
			// use AES itself to encrypt password to get cipher key (using plain password as source for key 
			// expansion) - gives us well encrypted key (though hashed key might be preferred for prod'n use)
			//
			z.v_nby = z.p_b0/8;  // no bytes in key (16/24/32)
			//
			z.v_pwb = new Array(z.v_nby);
			//
			for(var i=0;i<z.v_nby;i++) // use 1st 16/24/32 chars of password for key
			{
				z.v_pwb[i] = isNaN(w0.charCodeAt(i)) ? 0 : w0.charCodeAt(i);
			}
			//
			z.v_k0 = ALiBS.c0._cip(z.v_pwb, ALiBS.c0._kex(z.v_pwb));  // gives us 16-byte key
			//
			z.v_k0 = z.v_k0.concat(z.v_k0.slice(0, z.v_nby-16));  // expand key to 16/24/32 bytes long
			//
			// initialise 1st 8 bytes of counter block with nonce (NIST SP800-38A §B.2): [0-1] = millisec, 
			// [2-3] = random, [4-7] = seconds, together giving full sub-millisec uniqueness up to Feb 2106
			z.v_cbk = new Array(z.v_bs);
			//
			z.v_nce = (new Date()).getTime();  // timestamp: milliseconds since 1-Jan-1970
			z.v_nms = z.v_nce%1000;
			z.v_nsc = Math.floor(z.v_nce/1000);
			z.v_nrd = Math.floor(Math.random()*0xffff);
			//
			for(var i=0;i<2;i++) z.v_cbk[i]   = (z.v_nms  >>> i*8) & 0xff;
			for(var i=0;i<2;i++) z.v_cbk[i+2] = (z.v_nrd >>> i*8) & 0xff;
			for(var i=0;i<4;i++) z.v_cbk[i+4] = (z.v_nsc >>> i*8) & 0xff;
			//
			// and convert it to a string to go on the front of the y0
			z.v_ctx = '';
			for(var i=0;i<8;i++) z.v_ctx += String.fromCharCode(z.v_cbk[i]);
			//
			// generate key schedule - an expansion of the key into distinct Key Rounds for each round
			z.v_kys = ALiBS.c0._kex(z.v_k0);
			//
			z.v_bkc = Math.ceil(ms.length/z.v_bs);
			z.v_cpt = new Array(z.v_bkc);  // y0 as array of strings
			//
			for(var b=0;b<z.v_bkc;b++)
			{
				// set counter (block #) in last 8 bytes of counter block (leaving nonce in 1st 8 bytes)
				// done in two stages for 32-bit ops: using two words allows us to go past 2^32 blocks (68GB)
				for(var c=0;c<4;c++) z.v_cbk[15-c] = (b >>> c*8) & 0xff;
				for(var c=0;c<4;c++) z.v_cbk[15-c-4] = (b/0x100000000 >>> c*8);
				//
				z.v_cpc = ALiBS.c0._cip(z.v_cbk, z.v_kys);  // -- encrypt counter block --
				//
				// block size is reduced on final block
				z.v_bkl = b<z.v_bkc-1 ? z.v_bs : (ms.length-1)%z.v_bs+1;
				z.v_cch = new Array(z.v_bkl);
				//
				for(var i=0;i<z.v_bkl;i++)
				{  // -- xor ms with ciphered counter char-by-char --
					z.v_cch[i] = z.v_cpc[i] ^ ms.charCodeAt(b*z.v_bs+i);
					z.v_cch[i] = String.fromCharCode(z.v_cch[i]);
				}
				//
				z.v_cpt[b] = z.v_cch.join(''); 
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_ctx + z.v_cpt.join(''); // Array.join is more efficient than repeated string concatenation in IE // [BEST-PRACTICE] 
//alert((new Date()) - t);
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0.en",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	de: function(w0,y0,p) // Aes.Ctr.decrypt - Decrypt a text encrypted by AES in counter mode of operation
	/** 
	Method <static> <String> ALiBS.c0.de(w0,y0,p)

		Description: AES Decrypt

		Parameters:
			w0 = (required)(string) password
			y0 = (required)(string) message to decrypt

			p = (optional)(JSON)parameter {}
				b0 = (number) bits 
					256 = DEFAULT (128,192,256)

		Observations:
	
		Return Value:
			<static> <String> Decrypted text

		Example(s):
			var ok = ALiBS.c0.de(w0,y0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0.de",le:ALiBS.gv.c0.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(w0)!="string") throw "ALiBS.c0.de.Exception: Error: Invalid Parameter Type: (string) w0 passed as "+typeof(w0); // 
			if(typeof(y0)!="string") throw "ALiBS.c0.de.Exception: Error: Invalid Parameter Type: (string) y0 passed as "+typeof(y0); // 
			//
			ALiBS.sy.la(5+0,"w0="+w0,{o:"ALiBS.c0.de",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"y0="+y0,{o:"ALiBS.c0.de",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_b0 = z.b0; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_b0)!="number" || (!(z.p_b0==128 || z.p_b0==192 || z.p_b0==256)) ) z.p_b0 = 256;  // standard allows 128/192/256 bit keys
			//
			ALiBS.sy.la(5+0,"z.p_b0="+z.p_b0,{o:"ALiBS.c0.en",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_bs = 16;  // block size fixed at 16 bytes / 128 bits (b4=4) for AES
			//y0 = Base64.decode(y0);
			//w0 = Utf8.encode(w0);
			w0 = ALiBS.c6.en(w0);
			//
//var t = new Date();  // timer
			//
			// use AES to encrypt password (mirroring encrypt routine)
			z.v_bk0 = z.p_b0/8;  // no bytes in key
			z.v_wb = new Array(z.v_bk0);
			//
			// ************************
			//
			for(var i=0;i<z.v_bk0;i++) 
			{
				z.v_wb[i] = isNaN(w0.charCodeAt(i)) ? 0 : w0.charCodeAt(i);
			}
			//
			z.v_k0 = ALiBS.c0._cip(z.v_wb, ALiBS.c0._kex(z.v_wb));
			z.v_k0 = z.v_k0.concat(z.v_k0.slice(0, z.v_bk0-16));  // expand key to 16/24/32 bytes long
			//
			// recover nonce from 1st 8 bytes of y0
			z.v_cbk = new Array(8);
			z.v_ctx = y0.slice(0, 8);
			for(var i=0;i<8;i++) z.v_cbk[i] = z.v_ctx.charCodeAt(i);
			//
			// generate key schedule
			z.v_kys = ALiBS.c0._kex(z.v_k0);
			//
			// separate y0 into blocks (skipping past initial 8 bytes)
			z.v_nbl = Math.ceil((y0.length-8) / z.v_bs);
			z.v_ct0 = new Array(z.v_nbl);
			//
			for(var b=0;b<z.v_nbl;b++) z.v_ct0[b] = y0.slice(8+b*z.v_bs, 8+b*z.v_bs+z.v_bs);
			y0 = z.v_ct0;  // y0 is now array of block-length strings
			//
			// m0 will get generated block-by-block into array of block-length strings
			z.v_ptx = new Array(y0.length);
			//
			for(var b=0;b<z.v_nbl;b++) 
			{
				// set counter (block #) in last 8 bytes of counter block (leaving nonce in 1st 8 bytes)
				for(var c=0;c<4;c++) z.v_cbk[15-c] = ((b) >>> c*8) & 0xff;
				for(var c=0;c<4;c++) z.v_cbk[15-c-4] = (((b+1)/0x100000000-1) >>> c*8) & 0xff;
				//
				z.v_cpc = ALiBS.c0._cip(z.v_cbk, z.v_kys);  // encrypt counter block
				//
				z.v_ptb = new Array(y0[b].length);
				for(var i=0;i<y0[b].length;i++) 
				{
					// -- xor z.v_ptx with ciphered counter byte-by-byte --
					z.v_ptb[i] = z.v_cpc[i] ^ y0[b].charCodeAt(i);
					z.v_ptb[i] = String.fromCharCode(z.v_ptb[i]);
				}
				//
				z.v_ptx[b] = z.v_ptb.join('');
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_ptx.join(''); // join array of blocks into single m0 string // [BEST-PRACTICE]
			//m0 = Utf8.decode(m0);  // decode from UTF8 back to Unicode multi-byte chars
//alert((new Date()) - t);
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0.de",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0.de",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0.de",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// sBox is pre-computed multiplicative inverse in GF(2^8) used in subBytes and keyExpansion [§5.1.1]
	// Aes.sBox
	_box:
	[
		0x63,0x7c,0x77,0x7b,0xf2,0x6b,0x6f,0xc5,0x30,0x01,0x67,0x2b,0xfe,0xd7,0xab,0x76,
		0xca,0x82,0xc9,0x7d,0xfa,0x59,0x47,0xf0,0xad,0xd4,0xa2,0xaf,0x9c,0xa4,0x72,0xc0,
		0xb7,0xfd,0x93,0x26,0x36,0x3f,0xf7,0xcc,0x34,0xa5,0xe5,0xf1,0x71,0xd8,0x31,0x15,
		0x04,0xc7,0x23,0xc3,0x18,0x96,0x05,0x9a,0x07,0x12,0x80,0xe2,0xeb,0x27,0xb2,0x75,
		0x09,0x83,0x2c,0x1a,0x1b,0x6e,0x5a,0xa0,0x52,0x3b,0xd6,0xb3,0x29,0xe3,0x2f,0x84,
		0x53,0xd1,0x00,0xed,0x20,0xfc,0xb1,0x5b,0x6a,0xcb,0xbe,0x39,0x4a,0x4c,0x58,0xcf,
		0xd0,0xef,0xaa,0xfb,0x43,0x4d,0x33,0x85,0x45,0xf9,0x02,0x7f,0x50,0x3c,0x9f,0xa8,
		0x51,0xa3,0x40,0x8f,0x92,0x9d,0x38,0xf5,0xbc,0xb6,0xda,0x21,0x10,0xff,0xf3,0xd2,
		0xcd,0x0c,0x13,0xec,0x5f,0x97,0x44,0x17,0xc4,0xa7,0x7e,0x3d,0x64,0x5d,0x19,0x73,
		0x60,0x81,0x4f,0xdc,0x22,0x2a,0x90,0x88,0x46,0xee,0xb8,0x14,0xde,0x5e,0x0b,0xdb,
		0xe0,0x32,0x3a,0x0a,0x49,0x06,0x24,0x5c,0xc2,0xd3,0xac,0x62,0x91,0x95,0xe4,0x79,
		0xe7,0xc8,0x37,0x6d,0x8d,0xd5,0x4e,0xa9,0x6c,0x56,0xf4,0xea,0x65,0x7a,0xae,0x08,
		0xba,0x78,0x25,0x2e,0x1c,0xa6,0xb4,0xc6,0xe8,0xdd,0x74,0x1f,0x4b,0xbd,0x8b,0x8a,
		0x70,0x3e,0xb5,0x66,0x48,0x03,0xf6,0x0e,0x61,0x35,0x57,0xb9,0x86,0xc1,0x1d,0x9e,
		0xe1,0xf8,0x98,0x11,0x69,0xd9,0x8e,0x94,0x9b,0x1e,0x87,0xe9,0xce,0x55,0x28,0xdf,
		0x8c,0xa1,0x89,0x0d,0xbf,0xe6,0x42,0x68,0x41,0x99,0x2d,0x0f,0xb0,0x54,0xbb,0x16
	],
	// *****************************************************************
	// rCon is Round Constant used for the Key Expansion [1st col is 2^(r-1) in GF(2^8)] [§5.2]
	// Aes.rCon
	_rco:
	[
		[0x00, 0x00, 0x00, 0x00],
		[0x01, 0x00, 0x00, 0x00],
		[0x02, 0x00, 0x00, 0x00],
		[0x04, 0x00, 0x00, 0x00],
		[0x08, 0x00, 0x00, 0x00],
		[0x10, 0x00, 0x00, 0x00],
		[0x20, 0x00, 0x00, 0x00],
		[0x40, 0x00, 0x00, 0x00],
		[0x80, 0x00, 0x00, 0x00],
		[0x1b, 0x00, 0x00, 0x00],
		[0x36, 0x00, 0x00, 0x00]
	],
	// *****************************************************************
	_cip: function(i0,wk) // Aes.cipher - main Cipher function [§5.1]
	// AES Cipher function: encrypt 'i0' state with Rijndael algorithm
	//   applies Nr rounds (10/12/14) using key schedule wk for 'add round key' stage
	//
	// @param {Number[]} i0 16-byte (128-bit) input state array
	// @param {Number[][]} wk   Key schedule as 2D byte-array (Nr+1 x b4 bytes)
	// @returns {Number[]}     Encrypted output state array
	//
	/** 
	Method <static> <number> ALiBS.c0._cip(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._cip();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._cip",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(i0)!="object-Array") throw "ALiBS.c0._cip.Exception: Error: Invalid Parameter Type: (object-Array) i0 passed as "+ALiBS.sy.ty(i0); // 
			if(ALiBS.sy.ty(wk)!="object-Array") throw "ALiBS.c0._cip.Exception: Error: Invalid Parameter Type: (object-Array) wk passed as "+ALiBS.sy.ty(wk); // 
			//
			ALiBS.sy.la(5+0,"i0="+i0,{o:"ALiBS.c0._cip",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"wk="+wk,{o:"ALiBS.c0._cip",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_b4 = 4;               // block size (in words): no of columns in state (fixed at 4 for AES)
			z.v_nr0 = wk.length/z.v_b4 - 1; // no of rounds: 10/12/14 for 128/192/256-bit keys
			z.v_st = [[],[],[],[]];  // initialise 4 x z.v_b4 byte-array 'state' with input [§3.4]
			//
			// ************************
			//
			for(var i=0;i<4*z.v_b4;i++) z.v_st[i%4][Math.floor(i/4)] = i0[i];
			//
			z.v_st = ALiBS.c0._add(z.v_st,wk,0,z.v_b4);
			//
			for(var j=1;j<z.v_nr0;j++) 
			{
				z.v_st = ALiBS.c0._sub(z.v_st,z.v_b4);
				z.v_st = ALiBS.c0._shr(z.v_st,z.v_b4);
				z.v_st = ALiBS.c0._mix(z.v_st,z.v_b4);
				z.v_st = ALiBS.c0._add(z.v_st,wk,j,z.v_b4);
			}
			//
			z.v_st = ALiBS.c0._sub(z.v_st,z.v_b4);
			z.v_st = ALiBS.c0._shr(z.v_st,z.v_b4);
			z.v_st = ALiBS.c0._add(z.v_st,wk,z.v_nr0,z.v_b4);
			//
			z.v_ou = new Array(4*z.v_b4);  // convert state to 1-d array before returning [§3.4]
			//
			for(var i=0;i<4*z.v_b4;i++) z.v_ou[i] = z.v_st[i%4][Math.floor(i/4)];
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_ou; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._cip",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._cip",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._cip",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_kex: function(kx) // Aes.keyExpansion
	// Perform Key Expansion to generate a Key Schedule
	//
	// @param {Number[]} kx Key as 16/24/32-byte array
	// @returns {Number[][]} Expanded key schedule as 2D byte-array (Nr+1 x b4 bytes)
	//
	// generate Key Schedule (byte-array Nr+1 x b4) from Key [§5.2]
	/** 
	Method <static> <number> ALiBS.c0._kex(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._kex();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._kex",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(kx)!="object-Array") throw "ALiBS.c0._kex.Exception: Error: Invalid Parameter Type: (object-Array) kx passed as "+ALiBS.sy.ty(kx); // 
			//
			ALiBS.sy.la(5+0,"kx="+kx,{o:"ALiBS.c0._kex",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_b4 = 4;            // block size (in words): no of columns in state (fixed at 4 for AES)
			z.v_ks = kx.length/4;  // key length (in words): 4/6/8 for 128/192/256-bit keys
			z.v_rn = z.v_ks + 6;       // no of rounds: 10/12/14 for 128/192/256-bit keys
			//
			z.v_w0 = new Array(z.v_b4*(z.v_rn+1));
			z.v_tmp = new Array(4);
			//
			// ************************
			//
			z.ec = -2000; // 
			for(var i=0;i<z.v_ks;i++) 
			{
				z.v_r0 = [kx[4*i], kx[4*i+1], kx[4*i+2], kx[4*i+3]];
				z.v_w0[i] = z.v_r0;
			}
			//
			for(var i=z.v_ks;i<(z.v_b4*(z.v_rn+1));i++) 
			{
				z.v_w0[i] = new Array(4);
				for(var t=0;t<4;t++) z.v_tmp[t] = z.v_w0[i-1][t];
				//
				if(i%z.v_ks==0) 
				{
					z.v_tmp = ALiBS.c0._swd(ALiBS.c0._rwd(z.v_tmp));
					for(var t=0;t<4;t++) z.v_tmp[t] ^= ALiBS.c0._rco[i/z.v_ks][t];
				} 
				else if(z.v_ks>6 && i%z.v_ks==4) 
				{
					z.v_tmp = ALiBS.c0._swd(z.v_tmp);
				}
				//
				for(var t=0;t<4;t++) z.v_w0[i][t] = z.v_w0[i-z.v_ks][t] ^ z.v_tmp[t];
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r =  z.v_w0; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._kex",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._kex",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._kex",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_sub: function(sb,b4) // Aes.subBytes - apply SBox to state sb [§5.1.1]
	/** 
	Method <static> <number> ALiBS.c0._sub(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._sub();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._sub",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(sb)!="object-Array") throw "ALiBS.c0._sub.Exception: Error: Invalid Parameter Type: (object-Array) sb passed as "+ALiBS.sy.ty(sb); // 
			if(typeof(b4)!="number") throw "ALiBS.c0._sub.Exception: Error: Invalid Parameter Type: (number) b4 passed as "+typeof(b4); // 
			//
			ALiBS.sy.la(5+0,"sb="+sb,{o:"ALiBS.c0._sub",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"b4="+b4,{o:"ALiBS.c0._sub",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			z.ec = -1200; // 
			for(var i=0;i<4;i++) 
			{
				for(var c=0;c<b4;c++) sb[i][c] = ALiBS.c0._box[sb[i][c]];
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = sb; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._sub",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._sub",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._sub",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_shr: function(sr,b4) // Aes.shiftRows - shift row r of state S left by r bytes [§5.1.2]
	/** 
	Method <static> <number> ALiBS.c0._shr(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._shr();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._shr",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(sr)!="object-Array") throw "ALiBS.c0._shr.Exception: Error: Invalid Parameter Type: (object-Array) sr passed as "+ALiBS.sy.ty(sr); // 
			if(typeof(b4)!="number") throw "ALiBS.c0._shr.Exception: Error: Invalid Parameter Type: (number) b4 passed as "+typeof(b4); // 
			//
			ALiBS.sy.la(5+0,"sr="+sr,{o:"ALiBS.c0._shr",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"b4="+b4,{o:"ALiBS.c0._shr",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_t0 = new Array(4);
			//
			// ************************
			//
			for(var i=1;i<4;i++) 
			{
				for(var c=0;c<4;c++) z.v_t0[c] = sr[i][(c+i)%b4];  // shift into temp copy
				for(var c=0;c<4;c++) sr[i][c] = z.v_t0[c];         // and copy back
				//
			}       // note that this will work for b4=4,5,6, but not 7,8 (always 4 for AES):
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = sr;  // see asmaes.sourceforge.net/rijndael/rijndaelImplementation.pdf // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._shr",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._shr",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._shr",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_mix: function(mx,b4) // Aes.mixColumns - combine bytes of each col of state S [§5.1.3]
	/** 
	Method <static> <number> ALiBS.c0._mix(p)

		Description: aaaa

		Parameters:
			dd = (required)() aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._mix();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._mix",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(mx)!="object-Array") throw "ALiBS.c0._mix.Exception: Error: Invalid Parameter Type: (object-Array) mx passed as "+ALiBS.sy.ty(mx); // 
			if(typeof(b4)!="number") throw "ALiBS.c0._mix.Exception: Error: Invalid Parameter Type: (number) b4 passed as "+typeof(b4); // 
			//
			ALiBS.sy.la(5+0,"mx="+mx,{o:"ALiBS.c0._mix",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"b4="+b4,{o:"ALiBS.c0._mix",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			for(var c=0;c<4;c++) 
			{
				z.v_a0 = new Array(4);  // 'a' is a copy of the current column from 'mx'
				z.v_b0 = new Array(4);  // 'b' is a•{02} in GF(2^8)
				//
				for(var i=0;i<4;i++) 
				{
					z.v_a0[i] = mx[i][c];
					z.v_b0[i] = mx[i][c]&0x80 ? mx[i][c]<<1 ^ 0x011b : mx[i][c]<<1;
				}
				//
				// z.v_a0[n] ^ b[n] is z.v_a0•{03} in GF(2^8)
				mx[0][c] = z.v_b0[0] ^ z.v_a0[1] ^ z.v_b0[1] ^ z.v_a0[2] ^ z.v_a0[3]; // 2*a0 + 3*a1 + a2 + a3
				mx[1][c] = z.v_a0[0] ^ z.v_b0[1] ^ z.v_a0[2] ^ z.v_b0[2] ^ z.v_a0[3]; // a0 * 2*a1 + 3*a2 + a3
				mx[2][c] = z.v_a0[0] ^ z.v_a0[1] ^ z.v_b0[2] ^ z.v_a0[3] ^ z.v_b0[3]; // a0 + a1 + 2*a2 + 3*a3
				mx[3][c] = z.v_a0[0] ^ z.v_b0[0] ^ z.v_a0[1] ^ z.v_a0[2] ^ z.v_b0[3]; // 3*a0 + a1 + a2 + 2*a3
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = mx; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._mix",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._mix",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._mix",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_add: function(st,wk,rk,b4) // Aes.addRoundKey - xor Round Key into state S [§5.1.4]
	/** 
	Method <static> <number> ALiBS.c0._add(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._add();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(st)!="object-Array") throw "ALiBS.c0._add.Exception: Error: Invalid Parameter Type: (object-Array) st passed as "+ALiBS.sy.ty(st); // 
			if(ALiBS.sy.ty(wk)!="object-Array") throw "ALiBS.c0._add.Exception: Error: Invalid Parameter Type: (object-Array) wk passed as "+ALiBS.sy.ty(wk); // 
			if(typeof(rk)!="number") throw "ALiBS.c0._add.Exception: Error: Invalid Parameter Type: (number) rk passed as "+typeof(rk); // 
			if(typeof(b4)!="number") throw "ALiBS.c0._add.Exception: Error: Invalid Parameter Type: (number) b4 passed as "+typeof(b4); // 
			//
			ALiBS.sy.la(5+0,"st="+st,{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"wk="+wk,{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"rk="+rk,{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
			ALiBS.sy.la(5+0,"b4="+b4,{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			z.ec = -1200; // 
			for(var i=0;i<4;i++)
			{
				for(var c=0;c<b4;c++)
				{
					st[i][c] ^= wk[rk*4+c][i];
				}
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = st; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._add",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._add",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_swd: function(sw) // Aes.subWord - apply SBox to 4-byte word w
	/** 
	Method <static> <number> ALiBS.c0._swd(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._swd();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._swd",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(sw)!="object-Array") throw "ALiBS.c0._swd.Exception: Error: Invalid Parameter Type: (object-Array) sw passed as "+ALiBS.sy.ty(sw); // 
			//
			ALiBS.sy.la(5+0,"sw="+sw,{o:"ALiBS.c0._swd",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			z.ec = -1200; // 
			for(var i=0;i<4;i++) sw[i] = ALiBS.c0._box[sw[i]];
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = sw; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._swd",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._swd",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._swd",le:ALiBS.gv.c0.le});
		//
		return r; 
	},
	// *****************************************************************
	_rwd: function(rw) // Aes.rotWord - rotate 4-byte word w left by one byte
	/** 
	Method <static> <number> ALiBS.c0._rwd(p)

		Description: aaaa

		Parameters:
			rw = (required)(????) aaaa

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

		Example(s):
			var ok = ALiBS.c0._rwd();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c0._rwd",le:ALiBS.gv.c0.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(rw)!="object-Array") throw "ALiBS.c0._rwd.Exception: Error: Invalid Parameter Type: (object-Array) rw passed as "+ALiBS.sy.ty(rw); // 
			//
			ALiBS.sy.la(5+0,"rw="+rw,{o:"ALiBS.c0._rwd",le:ALiBS.gv.c0.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_t0 = rw[0];
			//
			// ************************
			//
			z.ec = -1200; // 
			for(var i=0;i<3;i++) rw[i] = rw[i+1];
			//
			z.ec = -1300; // 
			rw[3] = z.v_t0;
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = rw; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c0._rwd",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c0._rwd",le:ALiBS.gv.c0.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c0._rwd",le:ALiBS.gv.c0.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c0("+(++ALiBS.gv.c0.nn)+"x)",{o:"ALiBS.c0"});
ALiBS.sy.la(0+0,"ALiBS.gv.c0.rl="+ALiBS.gv.c0.rl,{o:"ALiBS.c0"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.c0.le="+ALiBS.gv.c0.le+ALiBS.gv.sy.se,{o:"ALiBS.c0"});

