/* *************************************************************************
// INFO:

// *************************************************************************
// TODO

*/
//
// *************************************************************************
/** 
	Class ALiBS.hb

		Description: 
			- Required Class for All Others;
			- Creates ALiBS Namespace;
			- Modules Name Cache;
			- Log Management;

	--------------------------------------------------------------------
*/
//
if(!ALiBS.gv.hb) ALiBS.gv.hb = {}; // global values
//
if(!ALiBS.gv.hb.le) ALiBS.gv.hb.le = 0; // local log status
//
ALiBS.gv.hb.nn = 0; // x times loaded
//
ALiBS.gv.hb.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.hb = // HTML buttons
{
	gl: 1, // 0 = required = default - static and global log level
	/**
	Global Variable ALiBS.hb.gl

		Description: Global Log Level.

		Values:
			-1 = none = OFF
			0 = required (0)
			1 = error + (required) (0,1)

	--------------------------------------------------------------------
	*/
	// *****************************************************************
	la: function(l,m,p) // log to array
	/** 
	Method <static> ALiBS.hb.la(p)

		Description: Log Messages 

		Parameters:
			l = (required)(number) level 
				0 = required, ALWAYS logged even if Local Log (le=false) and/or Global Log (ALiBS.hb.gl==0) 
				1 = error, ALWAYS logged even if Local Log (le=false) and/or Global Log (ALiBS.hb.gl==0) 
			m = (required)(string/array) message
			p = (optional)(JSON) parameters {d:"y",o:"ALiBS.el",le:true,so:print} 
				d = (String)y/n (y=yes=default= use date time stamp)(n=no)
	
		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{

	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.hb("+(++ALiBS.gv.hb.nn)+"x)",{o:"ALiBS.hb"});
ALiBS.sy.la(0+0,"ALiBS.gv.hb.le="+ALiBS.gv.hb.le+ALiBS.gv.sy.se,{o:"ALiBS.hb"});

