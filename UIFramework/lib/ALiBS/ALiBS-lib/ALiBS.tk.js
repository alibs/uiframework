/* *************************************************************************
// INFO:

HTML IDs
	iALiBS_tk_user0
	iALiBS_tk_otp0
	iALiBS_tk_user0label
	iALiBS_tk_user0div

	iALiBS_tk_pass0
	iALiBS_tk_pass0label

	iALiBS_tk_divError

	iALiBS_tk_button0 = submit / login
	iALiBS_tk_button1 = cancel

	iALiBS_tk_divButton1

// *************************************************************************
TODO(2014-05-27): 


*/
//
// *************************************************************************
//
if(!ALiBS.gv.tk) ALiBS.gv.tk = {}; // global values
//
if(!ALiBS.gv.tk.le) ALiBS.gv.tk.le = 0; // local log status
//
ALiBS.gv.tk.nn = 0; // x times loaded
//
ALiBS.gv.tk.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.tk.rl = "14.0718.0001";
//
// *************************************************************************
//
ALiBS.gv.tk.agp = "ALiBS.wk.wsc.if.p0"; // password algorithm
	// ALiBS.wk.wsc.if.p0  = check password - Algorithm = local test
	// ALiBS.wk.wsc.if.p1 = check password - Algorithm = [Extended] Validation Functions
//
ALiBS.gv.tk.agt = "ALiBS.wk.wsc.if.t0"; // token number algorithm
	// ALiBS.wk.wsc.if.t0  = check token number - Algorithm = local test
	// ALiBS.wk.wsc.if.t1 = check token number - Algorithm = [Extended] Validation Functions TODO(2014-06-03):implementar
//
ALiBS.gv.tk.ago = "ALiBS.wk.wsc.if.o0"; // otp algorithm
	// ALiBS.wk.wsc.if.o0  = check otp - Algorithm = local test
	// ALiBS.wk.wsc.if.o1 = check otp - Algorithm = [Extended] Validation Functions TODO(2014-06-03):implementar
//
ALiBS.gv.tk.apa = ""; // application (html,authentication,token)
/*
	Página		t0-iManager	(Simula tela login iManager)
			t0-PortalWF	(Simula tela login PortalWF)
			t1
			iManager
			PortalWF

	Autenticação	[A:local]
			[A:VM.192.168.9.10]
			[A:VM.192.168.9.80]
			[A:P1.10.195.124.33]
			[A:PD.10.222.17.58]
			[A:PD.10.222.17.5]

	Token		[T:local]	(ALiBS.wk)
			[T:VM]		(using ALiBS.wk.t8 || ALiBS.wk.o8 but with fixed messages return for z.r0)
			[T:TU.10.193.103.17]
			[T:TI.10.192.72.72]
			[T:HO.10.193.65.140]
			[T:PD.bradesco.com.br]

*/
//
// *************************************************************************
//
ALiBS.gv.tk.bhv = {}; // behavior cache control
	// ALiBS.gv.tk.bhv["otp"] = 1; 
	//	0 = DEFAULT = do NOT keep token/otp page
	//	1 = keep token/otp page

ALiBS.gv.tk.r2m = {}; // msg cache control = use return message
	//ALiBS.gv.tk.r2m["UnhandledTokenMessage"] = 1; // use return message
	//ALiBS.gv.tk.r2m["UnhandledOtpMessage"] = 1 // use return message
//
// *************************************************************************
//
ALiBS.gv.tk.msg = // msg cache
{
	BackButtonPressed: "BackButtonPressed",
	//
	OTPinpuMsg:"OTP - One Time Password ", // keep space at the end - used in ALiBS.tk.st (app.js)
	//
	InvalidEmptyUser: "Invalid Empty User",
	InvalidEmptyPass: "Invalid Empty Pass",
	InvalidEmptyUserPass: "Invalid Empty User Pass", // old Invalid OTP1 - user input
	InvalidEmptyOtp: "Invalid Empty Otp", // old Invalid OTP2 - user input
	//
	InvalidUserPass: "Invalid User or Password",
	InvalidUserPassP: "Invalid User or [P]assword",
	InvalidUserPassU: "Invalid [U]ser or Password",
	//
	InvalidTokenNumber: "Invalid Token Number",
	//
	InvalidOtpLength: "Invalid Otp Length",
	InvalidOtp: "Invalid OTP",
	//
	WebSystemOffAppOutOfService: "Web System Off - Application Out Of Service for Maintenance",
	WebSystemOffNoAppInfo: "Web System Off - No Application Info",
	WebSystemOffUnhandledMessage: "Web System Off - Unhandled Message",
	//
	InvalidOUConnection: "Invalid OU Connection",
	InvalidOUAccess: "Invalid OU Access",
	InvalidOUInfo: "Invalid OU Info",
	//
	InvalidUserConnection: "Invalid User Connection",
	InvalidUserAccess: "Invalid User Access",
	InvalidUserInfo: "Invalid User Info",
	//
	EmptyPasswordMessageReturned: "Empty Password Message Returned",
	EmptyTokenMessageReturned: "Empty Token Message Returned",
	EmptyOtpMessageReturned: "Empty Otp Message Returned",
	//
	UnhandledPasswordErrorMessage: "Unhandled Password Error Message",
	UnhandledTokenErrorMessage: "Unhandled Token Error Message",
	UnhandledOtpErrorMessage: "Unhandled Otp Error Message",
	//
	UnhandledPasswordMessage: "Unhandled Password Message",
	UnhandledTokenMessage: "Unhandled Token Message",
	UnhandledOtpMessage: "Unhandled Otp Message", // old Invalid OTP3
	//
	UnhandledMessage: "Unhandled Message" // TODO(2014-06-05): 1 ocorrência
};
//
// *************************************************************************
//
ALiBS.tk = // (client) token
{
	ser: function(er,p) // set error - TO BE OVERWRITTEN / EXTENDED
	// er = (required)(string) error message
	//
	// p = (optional)(JSON)parameter 
	//	xx = (number) ALiBS.tk.ser(s,{xx:1}); 
	//		case ALiBS.gv.tk.r2m["UnhandledOtpMessage"] = 1
	//
	// return = <undefined>
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.ser",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.tk.ser",le:ALiBS.gv.tk.le});
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1100; // 
			if(er && typeof(z.xx)!="number") // clear values if z.xx == undefined (90%)
			{
				ALiBS.sy.la(5+0,"if(er && typeof(z.xx)!=number)",{o:"ALiBS.tk.ser",le:ALiBS.gv.tk.le});
				//
				z.ec = -1200; // 
				ALiBS.tk.shw(20);
				z.ec = -1300; // 
				ALiBS.tk.shw(11);
			} 
			else // clear otp value if z.xx==1
			{
				ALiBS.sy.la(5+0,"else",{o:"ALiBS.tk.ser",le:ALiBS.gv.tk.le});
				//
				z.ec = -1400; // 
				ALiBS.el.ge("iALiBS_tk_otp0").value = "";
			} 
			//
			z.ec = -1600; // 
			if(!ALiBS.tk.se0) // Custom set error function
			{
				z.ec = -1610; // 
				ALiBS.el.ge("iALiBS_tk_divError").innerHTML = "<span>" + er + "</span>";
			}
			else
			{
				z.ec = -1620; // 
				ALiBS.tk.se0(er,z);
			}
			//
			z.ec = -1700; // 
			if(ALiBS.tk.se1) // Always runs if exists
			{
				z.ec = -1710; // 
				ALiBS.tk.se1(er,z);
			}
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.ser",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	bak: function(p) // back button in OTP 
	// p = (optional)(JSON)parameter 
	//	
	// return = <undefined>
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.bak",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = false; // [BEST-PRACTICE] = bak() runs onclick() and needs "return false;" for MSIE works properly
		//
		try
		{
			z.ec = -1000; // ec = start
			ALiBS.tk.but(0); 
			z.ec = -1100; // 
			ALiBS.tk.ser(ALiBS.gv.tk.msg["BackButtonPressed"]);
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.bak",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	rpa: function(p) // password validation
	// p = (optional)(JSON)parameter 
	//	
	// return = <undefined>
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			z.ec = -1100; // 
			z.us = ALiBS.gv.tk.user0 = ALiBS.el.ge("iALiBS_tk_user0").value;
			ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
			z.ec = -1200; // 
			z.pa = ALiBS.gv.tk.pass0 = ALiBS.el.ge("iALiBS_tk_pass0").value;
			ALiBS.sy.la(5+0,"z.pa="+z.pa,{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
			//
			//alert("\n(1)user="+user+"\npass="+pass); // TODO(2014-04-10):Comentar esta linha, usada somente para testes
			//
			z.ec = -1300; // 
			if(!z.us || !z.pa || !ALiBS.sg.tr(z.us) || !ALiBS.sg.tr(z.pa))
			{
				ALiBS.sy.la(6+0,"if(!z.us || !z.pa || !ALiBS.sg.tr(z.us) || !ALiBS.sg.tr(z.pa))",{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
				//
				z.ec = -1310; // 
				ALiBS.tk.but(0); 
				z.ec = -1320; // 
				//ALiBS.tk.shw(11); // show user/password/tree/login
				z.ec = -1330; // 
				//ALiBS.tk.shw(20); // hide otp/login
				//
				z.ec = -1340; // 
				     if(!z.us || !ALiBS.sg.tr(z.us)) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyUser"]);
				else if(!z.pa || !ALiBS.sg.tr(z.pa)) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyPass"]);
				else 					ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyUserPass"]);
			}
			else
			{
				ALiBS.sy.la(6+0,"else",{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
				//
				z.ec = -1400; // 
				z.u0 = new Array();
				z.ec = -1410; // 
				z.u0[z.u0.length] = ALiBS.gv.tk.u0;
				z.ec = -1420; // 
				z.u0[z.u0.length] = "?cmd=" + ALiBS.gv.tk.agp; 
				z.ec = -1430; // 
				z.u0[z.u0.length] = "&cb=ALiBS.tk.cpa"; 
				z.ec = -1440; // 
				z.u0[z.u0.length] = "&p1=" + z.us; 
				z.ec = -1450; // 
				z.u0[z.u0.length] = "&p2=" + z.pa; 
				z.ec = -1460; // 
				z.u0[z.u0.length] = "&apa=" + ALiBS.gv.tk.apa; 
				//
				z.ec = -1470; // 
				ALiBS.ag.ex(z.u0.join(""),"s",{zpf:"cmd=",ut:true}); // compacta a partir do parâmetro p1
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.rpa",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
		//
		return r;
	},
	// *****************************************************************
	cpa: function(s,p) // password validation call back
	// s = (required)(string)call back response
	//
	// p = (optional)(JSON)parameter 
	//	
	// return = <undefined>
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.cpa",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r= ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.user0="+ALiBS.gv.tk.user0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.pass0="+ALiBS.gv.tk.pass0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.otp0="+ALiBS.gv.tk.otp0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			//
			ALiBS.sy.la(5+0,"ALiBS.el.ge(iALiBS_tk_user0).value="+ALiBS.el.ge("iALiBS_tk_user0").value,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.el.ge(iALiBS_tk_pass0).value="+ALiBS.el.ge("iALiBS_tk_pass0").value,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.el.ge(iALiBS_tk_otp0).value="+ALiBS.el.ge("iALiBS_tk_otp0").value,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			//
			z.ec = -1000; // 
			z.us = ALiBS.el.ge("iALiBS_tk_user0").value;
			//
			z.ec = -1100; // 
			if(!s || !ALiBS.sg.tr(s) || s.indexOf("[TOKEN-O")<0) // password failed
			{
				z.ec = -1200; // 
				if(!s || !ALiBS.sg.tr(s))			ALiBS.tk.ser(ALiBS.gv.tk.msg["EmptyPasswordMessageReturned"]);
				else if(s.indexOf("[SYSTEM-OFF][NO-APP-INFO]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["WebSystemOffNoAppInfo"]);
				else if(s.indexOf("[SYSTEM-OFF][APP-OUT-OF-SERVICE]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["WebSystemOffAppOutOfService"]);
				else if(s.indexOf("[SYSTEM-OFF]")>=0) 		ALiBS.tk.ser(ALiBS.gv.tk.msg["WebSystemOffUnhandledMessage"]);
				//
				else if(s.indexOf("[ERROR][CONNECTION][OU]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOUConnection"]);
				else if(s.indexOf("[ERROR][NO-ACCESS][OU]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOUAccess"]);
				else if(s.indexOf("[ERROR][NO-INFO][OU]")>=0) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOUInfo"]);
				else if(s.indexOf("[ERROR][CONNECTION][USER]")>=0)ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserConnection"]);
				else if(s.indexOf("[ERROR][NO-ACCESS][USER]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserAccess"]);
				else if(s.indexOf("[ERROR][NO-INFO][USER]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserInfo"]);
				//
				else if(s.indexOf("[ERROR][PASS][P]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserPassP"]);
				else if(s.indexOf("[ERROR][PASS][U]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserPassU"]);
				else if(s.indexOf("[ERROR][PASS]")>=0)		ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserPass"]);
				//
				else if(s.indexOf("[ERROR]")>=0)
				{
					z.ec = -1250; // 
					if(ALiBS.tk.se2) // Always runs if exists
					{
						z.ec = -1270; // 
						z.r0 = ALiBS.tk.se2(s);
						//
						if(!z.r0) ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledPasswordErrorMessage"]);
					}
					else
					{
						ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledPasswordErrorMessage"]);
					}
				}
				else // does NOT contain [ERROR] in string
				{
					ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledPasswordMessage"]); // other error
				}
				//
				z.ec = -1300; // 
				ALiBS.tk.but(0); 
				z.ec = -1400; // 
				ALiBS.tk.shw(20); // hide otp/login
				//
				z.ec = -1500; // 
				if(s.indexOf("[SYSTEM-OFF]")>=0) // if [SYSTEM-OFF] hide user/password
				{
					z.ec = -1510; // 
					ALiBS.tk.shw(10); // hide user/password/tree/login
				}
				else // starts again
				{
					z.ec = -1520; // 
					ALiBS.tk.shw(11); // show user/password/tree/login
				}
			}
			else // [TOKEN-OFF]    [TOKEN-ON]
			{
				z.ec = -1600; // 
				if(s.indexOf("[TOKEN-OFF]")>=0) // password OK + token NOT required // [TOKEN-OFF][U][H]    [TOKEN-OFF][U][A]    [TOKEN-OFF][S]
				{
					z.ec = -1610; // 
					ALiBS.tk.but(9); // submit()
				}
				else if(s.indexOf("[TOKEN-ON]")>=0) // [TOKEN-ON]== password OK + token required = "cmd=2000" = test
				{
					z.ec = -1700; // 
					z.u0 = new Array();
					z.ec = -1710; // 
					z.u0[z.u0.length] = ALiBS.gv.tk.u0;
					z.ec = -1720; // 
					z.u0[z.u0.length] = "?cmd=" + ALiBS.gv.tk.agt; 
					z.ec = -1730; // 
					z.u0[z.u0.length] = "&cb=ALiBS.tk.ctk"; 
					z.ec = -1740; // 
					z.u0[z.u0.length] = "&p1=" + z.us; 
					z.ec = -1750; // 
					//z.u0[z.u0.length] = "&p2=" + z.pa; // NOT REQUIRED 
					z.u0[z.u0.length] = "&apa=" + ALiBS.gv.tk.apa; 
					//
					z.ec = -1800; // 
					ALiBS.ag.ex(z.u0.join(""),"s",{zpf:"cmd=",ut:true}); // compacta a partir do parâmetro p1
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.cpa",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.tk.cpa",le:ALiBS.gv.tk.le});
		//
		return r; 
	},
	// *****************************************************************
	ctk: function(s,p) // token number validation call back
	// s = (required)(string)call back response
	//
	// p = (optional)(JSON)parameter 
	//	
	// return = <undefined>
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.ctk",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			if(!s || !ALiBS.sg.tr(s) || s.indexOf("[OK][TOKEN-")<0) // token number failed
			{
				z.ec = -1100; // 
				     if(!s || !ALiBS.sg.tr(s))		ALiBS.tk.ser(ALiBS.gv.tk.msg["EmptyTokenMessageReturned"]);
				else if(s.indexOf("[ERROR][TOKEN]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidTokenNumber"]);
				else if(s.indexOf("[ERROR]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledTokenErrorMessage"]);
				else
				{
					z.ec = -1200; // 
					if(!ALiBS.gv.tk.r2m["UnhandledTokenMessage"])		ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledTokenMessage"]); // token number failed
					else if(ALiBS.gv.tk.r2m["UnhandledTokenMessage"]==1)	ALiBS.tk.ser(s); 
					else							ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledTokenMessage"]); // token number failed
				}
				//
				z.ec = -1300; // 
				ALiBS.tk.but(0); 
				//
				z.ec = -1400; // 
				ALiBS.tk.shw(11); // show user/password/tree/login
				//
				z.ec = -1500; // 
				ALiBS.tk.shw(20); // hide otp/login
			}
			else if(s.indexOf("[OK][TOKEN-OFF]")>=0) // password OK + token NOT required // [OK][TOKEN-OFF]
			{
				z.ec = -1550; // 
				ALiBS.tk.but(9); // submit()
			}
			else // if(s.indexOf("[OK][TOKEN-ON]")>=0) // token number OK //[OK][TOKEN-ON][000-0]    [OK][TOKEN-ON][007-3]
			{
				z.ec = -1600; // 
				ALiBS.tk.ser("");	
				//
				z.ec = -1610; // 
				ALiBS.tk.but(7); //document.forms[0].onsubmit=ALiBS.tk. sok;
				//
				z.ec = -1620; // 
				z.v0 = ALiBS.sg.bs(  ALiBS.sg.as(s,"[OK][TOKEN-ON][") , "-V_0-"  );
				ALiBS.sy.la(5+0,"z.v0="+z.v0,{o:"ALiBS.tk.ctk",le:ALiBS.gv.tk.le});
				//
				z.ec = -1630; // 
				ALiBS.tk.shw(10);
				//
				z.ec = -1640; // 
				ALiBS.tk.shw(21,{tn:z.v0}); // show otp/login
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.ctk",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.tk.ctk",le:ALiBS.gv.tk.le});
		//
		return r; 
	},
	// *****************************************************************
	rob: function(p) // otp request - button otptxt
	// p = (optional)(JSON)parameter 
	//	
	// return = <undefined>
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.user0="+ALiBS.gv.tk.user0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.pass0="+ALiBS.gv.tk.pass0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.otp0="+ALiBS.gv.tk.otp0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			//
			ALiBS.sy.la(5+0,"ALiBS.el.ge(iALiBS_tk_user0).value="+ALiBS.el.ge("iALiBS_tk_user0").value,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.el.ge(iALiBS_tk_pass0).value="+ALiBS.el.ge("iALiBS_tk_pass0").value,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"ALiBS.el.ge(iALiBS_tk_otp0).value="+ALiBS.el.ge("iALiBS_tk_otp0").value,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			//
			z.ec = -1000; // 
			//z.us = ALiBS.el.ge("iALiBS_tk_user0").value;
			z.us = ALiBS.gv.tk.user0;
			ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			//
			z.ec = -1100; // 
			//z.ot = ALiBS.el.ge("iALiBS_tk_otp0").value;
			//z.ot = ALiBS.gv.tk.otp0;
			ALiBS.gv.tk.otp0 = ALiBS.el.ge("iALiBS_tk_pass0").value;
			z.ot = ALiBS.gv.tk.otp0;
			ALiBS.sy.la(5+0,"ALiBS.gv.tk.otp0="+ALiBS.gv.tk.otp0,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			ALiBS.sy.la(5+0,"z.ot="+z.ot,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
			//
			z.ec = -1200; // 
			if(!z.ot || !ALiBS.sg.tr(z.ot))
			{
				z.ec = -1300; // 
				ALiBS.tk.but(0); 
				z.ec = -1400; // 
				ALiBS.tk.shw(11); // show user/password/tree/login
				z.ec = -1500; // 
				ALiBS.tk.shw(20); // hide otp/login
				//
				ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyOtp"]);	
			}
			else
			{
				z.ec = -2000; // 
				z.u0 = new Array();
				z.ec = -2100; // 
				z.u0[z.u0.length] = ALiBS.gv.tk.u0;
				z.ec = -2200; // 
				z.u0[z.u0.length] = "?cmd=" + ALiBS.gv.tk.ago; 
				z.ec = -2300; // 
				z.u0[z.u0.length] = "&cb=ALiBS.tk.cot"; 
				z.ec = -2400; // 
				z.u0[z.u0.length] = "&p1=" + z.us; 
				z.ec = -2500; // 
				z.u0[z.u0.length] = "&p2=" + z.ot; 
				z.ec = -2600; // 
				z.u0[z.u0.length] = "&apa=" + ALiBS.gv.tk.apa; 
				//
				z.ec = -2700; // 
				ALiBS.ag.ex(z.u0.join(""),"s",{zpf:"cmd=",ut:true}); // compacta a partir do parâmetro p1
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.rob",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
		//
		return r; 
	},
	// *****************************************************************
	cot: function(s,p) // otp validation call back
	// s = (required)(string)call back response
	//
	// p = (optional)(JSON)parameter 
	//	
	// return = <undefined>
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			if(!s || !ALiBS.sg.tr(s) || s.indexOf("[OK][OTP]")<0) // otp failed
			{
				z.ec = -1100; // 
				     if(!s || !ALiBS.sg.tr(s))		ALiBS.tk.ser(ALiBS.gv.tk.msg["EmptyOtpMessageReturned"]); // otp failed
				else if(s.indexOf("[ERROR][OTP][O]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"]);
				else if(s.indexOf("[ERROR][OTP][U]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"]);
				else if(s.indexOf("[ERROR][OTP]")>=0)
				{
					ALiBS.sy.la(5+0,"[ERROR][OTP]bhv[otp]="+ALiBS.gv.tk.bhv["otp"],{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
					//
					if(!ALiBS.gv.tk.bhv["otp"]) // ALiBS.gv.tk.bhv["otp"] = 0 / undefined
					{
						ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"]);
					}
					else // ALiBS.gv.tk.bhv["otp"] = 1
					{
						ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"],{xx:1});
					}
				}
				else if(s.indexOf("[ERROR]")>=0)
				{
					ALiBS.sy.la(5+0,"[ERROR]bhv[otp]="+ALiBS.gv.tk.bhv["otp"],{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
					//
					if(!ALiBS.gv.tk.bhv["otp"]) // ALiBS.gv.tk.bhv["otp"] = 0 / undefined
					{
						ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledOtpErrorMessage"]);
					}
					else // ALiBS.gv.tk.bhv["otp"] = 1
					{
						ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledOtpErrorMessage"],{xx:1});
					}
				}
				else
				{
					z.ec = -1200; // 
					if(!ALiBS.gv.tk.r2m["UnhandledOtpMessage"])		ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledOtpMessage"]); // otp failed
					else							ALiBS.tk.ser(s,{xx:1}); // ALiBS.gv.tk.r2m["UnhandledOtpMessage"] = 1
				}
				//
				z.ec = -1300; // 
				if(!ALiBS.gv.tk.bhv["otp"]) // ALiBS.gv.tk.bhv["otp"] = 0 / undefined
				{
					ALiBS.sy.la(5+0,"(if(!)bhv[otp]="+ALiBS.gv.tk.bhv["otp"],{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
					//
					z.ec = -1400; // 
					ALiBS.tk.but(0); //document.forms[0].onsubmit=ALiBS.tk. sof;
					z.ec = -1500; // 
					ALiBS.tk.shw(11); // show user/password/tree/login
					z.ec = -1600; // 
					ALiBS.tk.shw(20); // hide otp/login
				}
				else // ALiBS.gv.tk.bhv["otp"] = 1 = keep token/otp page
				{
					ALiBS.sy.la(5+0,"(else)bhv[otp]="+ALiBS.gv.tk.bhv["otp"],{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
					//
					ALiBS.el.ge("iALiBS_tk_pass0").value = "";
					//
					ALiBS.el.sf("iALiBS_tk_pass0");
				}
			}
			else // if(s.indexOf("[OK][OTP]")>=0) // otp OK 
			{
				z.ec = -2000; // 
				ALiBS.tk.but(9); // submit()
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.cot",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
		//
		return r; 
	}
	// *****************************************************************
};
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.tk("+(++ALiBS.gv.tk.nn)+"x)",{o:"ALiBS.tk"});
ALiBS.sy.la(0+0,"ALiBS.gv.tk.rl="+ALiBS.gv.tk.rl,{o:"ALiBS.tk"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.tk.le="+ALiBS.gv.tk.le+ALiBS.gv.sy.se,{o:"ALiBS.tk"});
//
// *************************************************************************
//
//	SAMPLE OF FUNCTIONS TO BE OVERWRITTEN / EXTENDED
/*

*/
//
// *************************************************************************

