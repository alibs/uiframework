/* *************************************************************************
// INFO:

UTF-8(7)                                             Linux Programmer's Manual                                            UTF-8(7)



NAME
       UTF-8 - an ASCII compatible multibyte Unicode encoding

DESCRIPTION
       The Unicode 3.0 character set occupies a 16-bit code space.  The most obvious Unicode encoding (known as UCS-2) consists of
       a sequence of 16-bit words.  Such strings can contain as parts of many 16-bit characters bytes like '\0' or '/' which  have
       a  special  meaning  in  filenames and other C library function arguments.  In addition, the majority of Unix tools expects
       ASCII files and can't read 16-bit words as characters without major modifications.  For these reasons, UCS-2 is not a suit-
       able  external encoding of Unicode in filenames, text files, environment variables, etc.  The ISO 10646 Universal Character
       Set (UCS), a superset of Unicode, occupies even a 31-bit code space and the obvious UCS-4 encoding for it  (a  sequence  of
       32-bit words) has the same problems.

       The  UTF-8 encoding of Unicode and UCS does not have these problems and is the common way in which Unicode is used on Unix-
       style operating systems.

   Properties
       The UTF-8 encoding has the following nice properties:

       * UCS characters 0x00000000 to 0x0000007f (the classic US-ASCII characters) are encoded simply as bytes 0x00 to 0x7f (ASCII
         compatibility).  This means that files and strings which contain only 7-bit ASCII characters have the same encoding under
         both ASCII and UTF-8.

       * All UCS characters greater than 0x7f are encoded as a multibyte sequence consisting only of bytes in the  range  0x80  to
         0xfd, so no ASCII byte can appear as part of another character and there are no problems with, for example,  '\0' or '/'.

       * The lexicographic sorting order of UCS-4 strings is preserved.

       * All possible 2^31 UCS codes can be encoded using UTF-8.

       * The bytes 0xfe and 0xff are never used in the UTF-8 encoding.

       * The first byte of a multibyte sequence which represents a single non-ASCII UCS character is always in the range  0xc0  to
         0xfd  and indicates how long this multibyte sequence is.  All further bytes in a multibyte sequence are in the range 0x80
         to 0xbf.  This allows easy resynchronization and makes the encoding stateless and robust against missing bytes.

       * UTF-8 encoded UCS characters may be up to six bytes long, however the Unicode  standard  specifies  no  characters  above
         0x10ffff, so Unicode characters can only be up to four bytes long in UTF-8.

   Encoding
       The  following byte sequences are used to represent a character.  The sequence to be used depends on the UCS code number of
       the character:

       0x00000000 - 0x0000007F:
           0xxxxxxx

       0x00000080 - 0x000007FF:
           110xxxxx 10xxxxxx

       0x00000800 - 0x0000FFFF:
           1110xxxx 10xxxxxx 10xxxxxx

       0x00010000 - 0x001FFFFF:
           11110xxx 10xxxxxx 10xxxxxx 10xxxxxx

       0x00200000 - 0x03FFFFFF:
           111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx

       0x04000000 - 0x7FFFFFFF:
           1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx

       The xxx bit positions are filled with the bits of the character code number in binary representation.   Only  the  shortest
       possible multibyte sequence which can represent the code number of the character can be used.

       The UCS code values 0xd800–0xdfff (UTF-16 surrogates) as well as 0xfffe and 0xffff (UCS noncharacters) should not appear in
       conforming UTF-8 streams.

   Example
       The Unicode character 0xa9 = 1010 1001 (the copyright sign) is encoded in UTF-8 as

              11000010 10101001 = 0xc2 0xa9

       and character 0x2260 = 0010 0010 0110 0000 (the "not equal" symbol) is encoded as:

              11100010 10001001 10100000 = 0xe2 0x89 0xa0

   Application Notes
       Users have to select a UTF-8 locale, for example with

              export LANG=en_GB.UTF-8

       in order to activate the UTF-8 support in applications.

       Application software that has to be aware of the used character encoding should always set the locale with for example

              setlocale(LC_CTYPE, "")

       and programmers can then test the expression

              strcmp(nl_langinfo(CODESET), "UTF-8") == 0

      to determine whether a UTF-8 locale has been selected and whether therefore all plaintext standard input and output, termi-
       nal communication, plaintext file content, filenames and environment variables are encoded in UTF-8.

       Programmers  accustomed to single-byte encodings such as US-ASCII or ISO 8859 have to be aware that two assumptions made so
       far are no longer valid in UTF-8 locales.  Firstly, a single byte does not necessarily correspond  any  more  to  a  single
       character.  Secondly, since modern terminal emulators in UTF-8 mode also support Chinese, Japanese, and Korean double-width
       characters as well as nonspacing combining characters, outputting a single character does not necessarily advance the  cur-
       sor  by  one  position  as it did in ASCII.  Library functions such as mbsrtowcs(3) and wcswidth(3) should be used today to
       count characters and cursor positions.

       The official ESC sequence to switch from an ISO 2022 encoding scheme (as used for instance by VT100 terminals) to UTF-8  is
       ESC  %  G  ("\x1b%G").   The  corresponding  return  sequence from UTF-8 to ISO 2022 is ESC % @ ("\x1b%@").  Other ISO 2022
       sequences (such as for switching the G0 and G1 sets) are not applicable in UTF-8 mode.

       It can be hoped that in the foreseeable future, UTF-8 will replace ASCII and ISO 8859 at all levels as the common character
       encoding on POSIX systems, leading to a significantly richer environment for handling plain text.

   Security
       The  Unicode and UCS standards require that producers of UTF-8 shall use the shortest form possible, for example, producing
       a two-byte sequence with first byte 0xc0 is nonconforming.  Unicode 3.1 has added the requirement that conforming  programs
       must  not  accept  non-shortest  forms in their input.  This is for security reasons: if user input is checked for possible
       security violations, a program might check only for the ASCII version of "/../" or ";" or NUL and overlook that  there  are
       many non-ASCII ways to represent these things in a non-shortest UTF-8 encoding.

   Standards
       ISO/IEC 10646-1:2000, Unicode 3.1, RFC 2279, Plan 9.

SEE ALSO
       nl_langinfo(3), setlocale(3), charsets(7), unicode(7)

COLOPHON
       This  page  is  part  of  release 3.25 of the Linux man-pages project.  A description of the project, and information about
       reporting bugs, can be found at http://www.kernel.org/doc/man-pages/.



GNU                                                         2001-05-11                                                    UTF-8(7)
 Manual page utf8(7) line 80/131 (END)

// *************************************************************************

UTF-8 and Unicode FAQ for Unix/Linux

http://www.cl.cam.ac.uk/~mgk25/unicode.html


// *************************************************************************

Para testar

ALiBS.gv.c6.le = 1
ALiBS.c6.de(ALiBS.c6.en("áéíóúḉç"))
ALiBS_sy_la(0,"teste",{so:"print"})

// *************************************************************************

IMPORTANTE:
-----------

No imanager, em iManager-cmd.jsp tive que rodar ALiBS.c6.de() duas vezes seguidas
para recuperar as letras acentuadas corretamente.

String p3 = request.getParameter("p3"); // Motivo bloqueio/desbloqueio
//
System.out.println("(1)p3="+p3);
//
Object[] args = {p3};
p3 = "" + shell.callGlobalFunction("ALiBS_c6_de",args);
System.out.println("(2)p3="+p3);
//
Object[] args2 = {p3};
p3 = "" + shell.callGlobalFunction("ALiBS_c6_de",args2);
System.out.println("(3)p3="+p3);
//
shell.callGlobalFunction("Bradesco_printLog",null);

// *************************************************************************
// TODO:

*/
// TODO(2011-02-14): UTF-16
/*
 * Encode a string as utf-16
 */
/*
function str2rstr_utf16le(input)
{
	var output = "";
	for(var i = 0; i < input.length; i++)
		output += String.fromCharCode( input.charCodeAt(i)        & 0xFF,
				          (input.charCodeAt(i) >>> 8) & 0xFF);
	return output;
};

function str2rstr_utf16be(input)
{
	var output = "";
	for(var i = 0; i < input.length; i++)
		output += String.fromCharCode((input.charCodeAt(i) >>> 8) & 0xFF,
			                   input.charCodeAt(i)        & 0xFF);
	return output;
};
*/
//
// *************************************************************************
//
if(!ALiBS.gv.c6) ALiBS.gv.c6 = {}; // global values
//
if(!ALiBS.gv.c6.le) ALiBS.gv.c6.le = 0; // local log status
//
ALiBS.gv.c6.nn = 0; // x times loaded
//
ALiBS.gv.c6.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.c6.rl = "15.0629.1007";
//
// *************************************************************************
//
ALiBS.c6 = // utf8 encode/decode
{
	en: function(s0,p) 
	/** 
	Method <static> <string> ALiBS.c6.en(p)

		Description: UTF-8 encoding

		Parameters:
			s0 = (required) (string) string

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> UTF-8 string

		Example(s):
			var ok = ALiBS.c6.en();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "ALiBS.c6.en.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//if(typeof(z.us)!="string") throw "ALiBS.c6.en.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			//if(typeof(z.pw)!="string") throw "ALiBS.c6.en.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//if(typeof(z.ab)!="string" || z.ab=="*") z.ab = "*";
			//
			/*if(typeof(z.fd)!="string" || z.fd=="*")
			{
				z.fd = ALiBS.to.fdn(t0); // get ALL defined field names = DEFAULT = "*"
			}
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}*/
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			z.u0 = new Array();
	 		//
			// ************************
			//
			for(var i=0;i<s0.length;i++) 
			{
	 			z.c0 = s0.charCodeAt(i);
	//console.log("s0.charCodeAt("+n+")="+c);

	 			//
				//ALiBS.sy.la(5+0,"[in]["+s0.charAt(n)+"]["+c+"]",{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
				//
				z.ec = -1200; // 
				if(z.c0<128) 
				{
	//console.log("<128");
					z.ec = -1210; // 
					z.u0[z.u0.length] = String.fromCharCode(z.c0);
				}
				else if((z.c0>127) && (z.c0<2048)) 
				{
	//console.log(">128");
					z.ec = -1220; // 
					z.u0[z.u0.length] = String.fromCharCode((z.c0 >> 6) | 192);
					z.u0[z.u0.length] = String.fromCharCode((z.c0 & 63) | 128);
				}
				else 
				{
	//console.log("else");
					z.ec = -1230; // 
					z.u0[z.u0.length] = String.fromCharCode((z.c0 >> 12) | 224);
					z.u0[z.u0.length] = String.fromCharCode(((z.c0 >> 6) & 63) | 128);
					z.u0[z.u0.length] = String.fromCharCode((z.c0 & 63) | 128);
				}
				//
				z.ec = -2000; // 
				ALiBS.sy.la(7+0,"[in]["+s0.charAt(i)+"]["+z.c0+"]",{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
				ALiBS.sy.la(7+0,"[out]["+z.u0[i].charAt(z.u0.length-1)+"]["+z.u0[i].charCodeAt(z.u0.length-1)+"]",{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
			}
			//
			// ************************
			//
			z.ec = -9000; // -9902, -9903, etc
			//if(i>1000) throw "ALiBS.c6.en.Exception: Error: Infinite-Loop (i="+i+")"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			// ************************
			//
			z.ec = -9800; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.c6.en.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			r = z.u0.join(""); // [BEST-PRACTICE]
			//
			//r = "" + z.r; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c6.en",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
		//
		return r; 
	},
	// *****************************************************************
	de: function(u0,p) // 
	/** 

	Method <static> <string> ALiBS.c6.en(p)

		Description: UTF-8 Decoding

		Parameters:
			u0 = (required)(string) UTF-8 encoded string

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> string

		Example(s):
			var ok = ALiBS.c6.de();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "ALiBS.c6.de.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//if(typeof(z.us)!="string") throw "ALiBS.c6.de.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			//if(typeof(z.pw)!="string") throw "ALiBS.c6.de.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//if(typeof(z.ab)!="string" || z.ab=="*") z.ab = "*";
			//
			/*if(typeof(z.fd)!="string" || z.fd=="*")
			{
				z.fd = ALiBS.to.fdn(t0); // get ALL defined field names = DEFAULT = "*"
			}
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}*/
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			z.s0 = new Array();
			z.i = 0;
			z.c0 = 0;
			z.c1 = 0;
			z.c2 = 0;
			//
			// ************************
			//
			z.ec = -1200; // 
			while(z.i<u0.length) 
			{
				z.ec = -1300; // 
				z.c0 = u0.charCodeAt(z.i);
				// 
				//ALiBS.sy.la(5+0,"[in]["+u0.charAt(i)+"]["+z.c0+"]",{o:"ALiBS.c6.en",le:ALiBS.gv.c6.le});
				//
				z.ec = -1400; // 
				if(z.c0<128) 
				{
					z.ec = -1410; // 
					z.s0[z.s0.length] = String.fromCharCode(z.c0);
					z.i++;
				}
				else if((z.c0>191) && (z.c0<224)) 
				{
					z.ec = -1510; // 
					z.c2 = u0.charCodeAt(z.i+1);
					z.ec = -1520; // 
					z.s0[z.s0.length] = String.fromCharCode(((z.c0 & 31) << 6) | (z.c2 & 63));
					z.i += 2;
				}
				else 
				{
					z.ec = -1610; // 
					z.c2 = u0.charCodeAt(z.i+1);
					z.ec = -1620; // 
					z.c3 = u0.charCodeAt(z.i+2);
					z.ec = -1630; // 
					z.s0[z.s0.length] = String.fromCharCode(((z.c0 & 15) << 12) | ((z.c2 & 63) << 6) | (z.c3 & 63));
					z.i += 3;
				}
				//
				z.ec = -2000; // 
				//ALiBS.sy.la(7+0,"[in]["+u0.charAt(z.i)+"]["+z.c0+"]",{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
				//ALiBS.sy.la(7+0,"[out]["+z.s0[z.s0.length-1]+"]["+z.s0[z.i].charCodeAt(z.s0.length-1)+"]",{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
			}
			// 
			// ************************
			//
			z.ec = -9000; // -9902, -9903, etc
			//if(i>1000) throw "ALiBS.c6.de.Exception: Error: Infinite-Loop (i="+i+")"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			// ************************
			//
			z.ec = -9800; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.c6.de.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			r = z.s0.join(""); // [BEST-PRACTICE]
			//
			//r = "" + z.r; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c6.de",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c6.de",le:ALiBS.gv.c6.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
var ALiBS_c6_en = ALiBS.c6.en; // pointer
var ALiBS_c6_de = ALiBS.c6.de; // pointer
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c6("+(++ALiBS.gv.c6.nn)+"x)",{o:"ALiBS.c6"});
ALiBS.sy.la(0+0,"ALiBS.gv.c6.le="+ALiBS.gv.c6.le+ALiBS.gv.sy.se,{o:"ALiBS.c6"});

