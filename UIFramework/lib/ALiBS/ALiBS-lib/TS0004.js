// *************************************************************************
//
//ALiBS.gv.sy.so="immediate-console";
//
if(!ALiBS.gv.TS0004) ALiBS.gv.TS0004 = {}; // global values
//
if(!ALiBS.gv.TS0004.le) ALiBS.gv.TS0004.le = 0; // local log status
//
ALiBS.gv.TS0004.rl = "15.0603.1455";
//
// *************************************************************************
//
var TS0004 = new ALiBS.to.t({tb_rf:"TS0004",tb_id:"_TS0004"}); 
//
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{

}
//
// *************************************************************************
//
else // Browser / Client
{
	TS0004.tb.tg = "TS0004";
	TS0004.tb.ty = "fghij";
	//
	// *************************************************
	//
	TS0004.nm.f21 = "FullName"; 
	TS0004.cp.f21 = "Full Name";
	TS0004.tt.f21 = "Full Name (Title)";
	TS0004.dc.f21 = "Full Name (Description)";
	TS0004.dv.f21 = "[EMPTY]";
	TS0004.ob.f21 = "Full Name (Obs)";
	TS0004.cd.f21 = "15.0701";
	//
	TS0004.nm.f22 = "NickName"; 
	TS0004.cp.f22 = "Nick Name";
	TS0004.tt.f22 = "Nick Name (Title)";
	TS0004.dc.f22 = "Nick Name (Description)";
	TS0004.dv.f22 = "[EMPTY]";
	TS0004.ob.f22 = "Nick Name (Obs)";
	TS0004.cd.f22 = "15.0701";
	//
	TS0004.nm.f23 = "AppNickName"; 
	TS0004.cp.f23 = "App Nick Name";
	TS0004.tt.f23 = "App Nick Name (Title)";
	TS0004.dc.f23 = "App Nick Name (Description)";
	TS0004.dv.f23 = "[EMPTY]";
	TS0004.ob.f23 = "App Nick Name (Obs)";
	TS0004.cd.f23 = "15.0701";
	//
	TS0004.nm.f24 = "Birthday"; 
	TS0004.cp.f24 = "Birthday (Caption)";
	TS0004.tt.f24 = "Birthday (Title)";
	TS0004.dc.f24 = "Birthday (Description)";
	TS0004.dv.f24 = "[EMPTY]";
	TS0004.ob.f24 = "Birthday (Obs)";
	TS0004.cd.f24 = "15.0701";
	//
	// *************************************************
	//
	TS0004.nm.i21 = "RG"; 
	TS0004.cp.i21 = "RG (Caption)";
	TS0004.tt.i21 = "RG (Title)";
	TS0004.dc.i21 = "RG (Description)";
	TS0004.dv.i21 = "[EMPTY]";
	TS0004.ob.i21 = "RG (Obs)";
	TS0004.cd.i21 = "15.0701";
	//
	TS0004.nm.i22 = "CPF"; 
	TS0004.cp.i22 = "CPF (Caption)";
	TS0004.tt.i22 = "CPF (Title)";
	TS0004.dc.i22 = "CPF (Description)";
	TS0004.dv.i22 = "[EMPTY]";
	TS0004.ob.i22 = "CPF (Obs)";
	TS0004.cd.i22 = "15.0701";
	//
	TS0004.nm.i23 = "Passporte"; 
	TS0004.cp.i23 = "Passporte (Caption)";
	TS0004.tt.i23 = "Passporte (Title)";
	TS0004.dc.i23 = "Passporte (Description)";
	TS0004.dv.i23 = "[EMPTY]";
	TS0004.ob.i23 = "Passporte (Obs)";
	TS0004.cd.i23 = "15.0701";
	//
	TS0004.nm.i24 = "SocialSecurity"; 
	TS0004.cp.i24 = "SocialSecurity (Caption)";
	TS0004.tt.i24 = "SocialSecurity (Title)";
	TS0004.dc.i24 = "SocialSecurity (Description)";
	TS0004.dv.i24 = "[EMPTY]";
	TS0004.ob.i24 = "SocialSecurity (Obs)";
	TS0004.cd.i24 = "15.0701";
	//
	TS0004.nm.i25 = "Habilitação"; 
	TS0004.cp.i25 = "Habilitação (Caption)";
	TS0004.tt.i25 = "Habilitação (Title)";
	TS0004.dc.i25 = "Habilitação (Description)";
	TS0004.dv.i25 = "[EMPTY]";
	TS0004.ob.i25 = "Habilitação (Obs)";
	TS0004.cd.i25 = "15.0701";
	//
	TS0004.nm.i41 = "CustomID1"; 
	TS0004.cp.i41 = "Custom ID 1 (Caption)";
	TS0004.tt.i41 = "Custom ID 1 (Title)";
	TS0004.dc.i41 = "Custom ID 1 (Description)";
	TS0004.dv.i41 = "[EMPTY]";
	TS0004.ob.i41 = "Custom ID 1 (Obs)";
	TS0004.cd.i41 = "15.0701";
	//
	TS0004.nm.i42 = "CustomID2"; 
	TS0004.cp.i42 = "Custom ID 2 (Caption)";
	TS0004.tt.i42 = "Custom ID 2 (Title)";
	TS0004.dc.i42 = "Custom ID 2 (Description)";
	TS0004.dv.i42 = "[EMPTY]";
	TS0004.ob.i42 = "Custom ID 2 (Obs)";
	TS0004.cd.i42 = "15.0701";
	//
	// *************************************************
	//
	TS0004.nm.u21 = "PrimaryKey"; 
	TS0004.cp.u21 = "Primary Key (Caption)";
	TS0004.tt.u21 = "Primary Key (Title)";
	TS0004.dc.u21 = "Primary Key (Description)";
	TS0004.dv.u21 = "[EMPTY]";
	TS0004.ob.u21 = "Could be another field, case it's third part table, but should be copied to u00 for compatibility (Obs)";
	TS0004.cd.u21 = "15.0701";
	//
	TS0004.nm.u22 = "Dataset"; 
	TS0004.cp.u22 = "Dataset (Caption)";
	TS0004.tt.u22 = "Dataset (Title)";
	TS0004.dc.u22 = "Dataset (Description)";
	TS0004.dv.u22 = "[EMPTY]";
	TS0004.ob.u22 = "Data Segregation for multi-user, multi-accounts, multi-projects, multi-companies (Obs)";
	TS0004.cd.u22 = "15.0701";
	//
		//1 = belongs to user 1150 (user Alfredo)
		//2 = belongs to user 2342 (company ALiBS.org)
		//3 = belongs to user 1000 (company ALiBS.net)
		//- users can be granted access to other/multi datasets
	//
	// *************************************************
	//
	TS0004.nm.u31 = "CreationTimestamp"; 
	TS0004.cp.u31 = "Creation Timestamp (Caption)";
	TS0004.tt.u31 = "Creation Timestamp (Title)";
	TS0004.dc.u31 = "Creation Timestamp (Description)";
	TS0004.dv.u31 = "Creation Timestamp";
	TS0004.ob.u31 = "Creation Timestamp (Obs)";
	TS0004.cd.u31 = "15.0701";
	//
	TS0004.nm.u32 = "ModificationTimestamp"; 
	TS0004.cp.u32 = "Modification Timestamp (Caption)";
	TS0004.tt.u32 = "Modification Timestamp (Title)";
	TS0004.dc.u32 = "Modification Timestamp (Description)";
	TS0004.dv.u32 = "Creation Timestamp";
	TS0004.ob.u32 = "Modification Timestamp (Obs)";
	TS0004.cd.u32 = "15.0701";
	//
	TS0004.nm.u33 = "LogicalDeletionTimestamp"; 
	TS0004.cp.u33 = "Logical Deletion Timestamp (Caption)";
	TS0004.tt.u33 = "Logical Deletion Timestamp (Title)";
	TS0004.dc.u33 = "Logical Deletion Timestamp (Description)";
	TS0004.dv.u33 = "[EMPTY]";
	TS0004.ob.u33 = "Filters should show/hide this record. Works like blocking/suspending the record (Obs)";
	TS0004.cd.u33 = "15.0701";
	//
	TS0004.nm.u34 = "PhysicalDeletionTimestamp"; 
	TS0004.cp.u34 = "Physical Deletion Timestamp (Caption)";
	TS0004.tt.u34 = "Physical Deletion Timestamp (Title)";
	TS0004.dc.u34 = "Physical Deletion Timestamp (Description)";
	TS0004.dv.u34 = "[EMPTY]";
	TS0004.ob.u34 = "Flag to mean the record is allowed to be physically deleted (Obs)";
	TS0004.cd.u34 = "15.0701";
	//
/*
	uxx = Synchronization Timestamp (needed ?)
	u32 = Physical Deletion Timestamp ()
*/
	// *************************************************
	//
	TS0004.nm.u41 = "CreatorsID"; 
	TS0004.cp.u41 = "Creator's ID (Caption)";
	TS0004.tt.u41 = "Creator's ID (Title)";
	TS0004.dc.u41 = "Creator's ID (Description)";
	TS0004.dv.u41 = "Creator's ID";
	TS0004.ob.u41 = "Creator's ID (Obs)";
	TS0004.cd.u41 = "15.0701";
	//
	TS0004.nm.u42 = "LastModifiersID"; 
	TS0004.cp.u42 = "Last Modifier's ID (Caption)";
	TS0004.tt.u42 = "Last Modifier's ID (Title)";
	TS0004.dc.u42 = "Last Modifier's ID (Description)";
	TS0004.dv.u42 = "Last Modifier's ID";
	TS0004.ob.u42 = "Last Modifier's ID (Obs)";
	TS0004.cd.u42 = "15.0701";
	//
/*
	u4x = DeletorsID = precisa ?

	u4x = "OwnersID"; precisa? 
				= acho que nao pois vou usar dataset

	uxx = Last Modified Fields (needed ?)
		- ALL fields work with v99 = History
*/
	// *************************************************
	//
	TS0004.nm.u51 = "CreatorsInstalation"; 
	TS0004.cp.u51 = "Creator's Instalation (Caption)";
	TS0004.tt.u51 = "Creator's Instalation (Title)";
	TS0004.dc.u51 = "Creator's Instalation (Description)";
	TS0004.dv.u51 = "Creator's Instalation";
	TS0004.ob.u51 = "Mobile1,Mobile2,Portal (where modification happend) (Obs)";
	TS0004.cd.u51 = "15.0701";
	//
	TS0004.nm.u52 = "Last ModifiersInstalation"; 
	TS0004.cp.u52 = "Last Modifier's Instalation (Caption)";
	TS0004.tt.u52 = "Last Modifier's Instalation (Title)";
	TS0004.dc.u52 = "Last Modifier's Instalation (Description)";
	TS0004.dv.u52 = "Last Modifier's Instalation";
	TS0004.ob.u52 = "Mobile1,Mobile2,Portal (where modification happend) (Obs)";
	TS0004.cd.u52 = "15.0701";
	//
	// *************************************************
	//
	TS0004.tb.hg.tt = 2; // DEFAULT = null = NOT SET = field title control = (see also ALiBS.gv.hg.tt)
	//
	TS0004.tb.hx = [{ty:"[HEADERHX]",tx:"[ TS0004 - Users ]",c:"white",bg:"red",al:"center"}]; // Header hx;
	//
	TS0004.tb.pg.css.hx = 1; // header hx style ON/OFF (0=OFF=NOT CHANGE, 1=ON=CHANGE)
	//
	TS0004.tb.pg.n = 13; // paging = number of records to show (0 = show all)
	//
	TS0004.tb.nso = "a01"; // fields in grid NOT to be sorted
	//
	TS0004.tb.sh = "f21,f22,f23,f24,i21,i22,i23,i24,i25,i41,i42,u21,u22,u31,u32,u33,u34,u41,u42,u51,u52,a00".split(","); // fields to show
	//
	TS0004.tb.fa = "fm"; // active filter
	//
	TS0004.tb.pg.css.sz = "1900px";
	//
	// *************************************************
	//
	TS0004.tb.fk.f21 = [{ty:"[TABLEINFO]",tx:"Info",c:"red",bg:"white"}]; // table information button
	TS0004.tb.fk.f05 = [{ty:"[PAGING]",tx:"Pg",c:"red",bg:"white"}]; // Paging buttons = Navigation
	TS0004.tb.fk.f03 = [{ty:"[COMPLEXFILTER]",tx:"CF",c:"red",bg:"white",t:"[]",v:"fm;x"},
									{t:"fm:match",v:"fm;x"},
									{t:"fe:equals",v:"fe;x"},{t:"res>=1",v:"fc;(a00>=1)"},{t:"res>=3",v:"fc;(a00>=3)"}]; // complex filter
	//
	TS0004.tb.fk.f04 = [{ty:"[PAGELINES]",tx:"Lines",l:"4,5,6,7,8,9,10,11,12,13,20",c:"red",bg:"white"}]; // Page Lines
	TS0004.tb.fk.f01 = [{ty:"[FONTSIZE]",tx:"Fon",l:"7px,8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,30px",c:"red",bg:"white"}]; // Font size
	TS0004.tb.fk.f02 = [{ty:"[FONTFAMILY]",tx:"Fam",l:"Verdana,Arial",c:"red",bg:"white"}]; // Font family
	//
	TS0004.tb.fk.f00 = [{ty:"[FONTSTYLE]",tx:"Sty",l:"normal,italic,oblique",c:"red",bg:"white"}]; // Font family
	TS0004.tb.fk.f06 = [{ty:"[FONTWEIGHT]",tx:"Wei",l:"normal,bold,bolder,lighter,100,200,300,400,500,600,700,800,900",c:"red",bg:"white"}]; // Font family
	//
	// *************************************************
	TS0004.ff.hg.dg = function(t,n,p) // display GRID
	// t = ()table
	// n = ()sorted field
	// p = (optional)(JSON)parameter {}
	//	rv = (boolean)reverse sort (default=false)
	//	y = (String)field type [abcdefghijkrx] (default=f)
	{
		var z = p || {}; // [BEST-PRACTICE]
		//
		if(n==t.so.g) ALiBS.to.o(t,n,{rv:!t.so.rv,y:z.y}); // reverse
		else ALiBS.to.o(t,n,{y:z.y}); // normal sort
		//
		var a = ALiBS.hg.g0(t,"nome",{cs:"hg_css0F"}); // 4101, font-size: 16px; padding:2px;
		ALiBS.el.si("iContGrid02a",a);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
	};
	// *************************************************
	TS0004.ff.hg.af = function(t,p) // after load 
	// t = (ALiBS.to)
	// f02 = (String)data (para ordenar table)
	// f05 = (String)valor 
	// f11 = (String)saldo conta 
	{
		var z = {}; // [BEST-PRACTICE]
		//
		var r = 1;
		//
		// ********************************
		//
		ALiBS.to.o(t,21,{y:"f"});
		t.tb.fi["f21"] = "[]," + ALiBS.to.rd(t,{y:"f",g:21,ke:2}).join(",");
//console.log(t.tb.fi["f21"]);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	};
	// *************************************************
} // endif("Server")
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module TS0004",{o:"TS0004"});
ALiBS.sy.la(0+0,"ALiBS.gv.TS0004.rl="+ALiBS.gv.TS0004.rl,{o:"TS0004"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.TS0004.le="+ALiBS.gv.TS0004.le+ALiBS.gv.sy.se,{o:"TS0004"});

