// *************************************************************************
// Alfredo - 2014-04-29 14:30
// *************************************************************************
// Log Settings Before Loading ALiBS
//
var ALiBS = {gv:{sy:{},wi:{}}}; // PRE definitions for global values
//
ALiBS.gv.sy.sygl = -1; // global log level (==ALiBS.sy.gl)
//ALiBS.gv.sy.syli = "ALiBS.cp.bc12,ALiBS.cp.bc13"; // method list (==ALiBS.sy.li)
//
ALiBS.gv.sy.le = 1;
//ALiBS.gv.to.le=0;
ALiBS.gv.wi.le=1;
//ALiBS.gv.wl.le=1;
//
//ALiBS.gv.sy.gla = 0; // 0 = Auto Print / Clean , 1 = Auto Reduce
//ALiBS.gv.sy.glx=500; // max = trigger
//ALiBS.gv.sy.glu=100; // errors + glu
//
// *************************************************************************
// Log File Path / Name
//
ALiBS.gv.sy.logFile = ""; // log to Native log file
//
//ALiBS.gv.sy.logFile = "/var/log/idm_drivers/Relatorios-debug.log"; // VM - Alfredo - TODO(): Alterar também /var/Templates/html2pdf.sh
//
//ALiBS.gv.sy.logFile = "/var/log/idm_drivers/Relatorios-debug.log"; // Pretec1 - TODO(): Alterar também /var/Templates/html2pdf.sh
//
//ALiBS.gv.sy.logFile = "/var/log/Relatorios-debug.log"; // Producao - TODO(): Alterar também /var/Templates/html2pdf.sh
//
// *************************************************************************
//
// Import Java Classes and Packages
//ALiBS.gv.sy.importJava = "java.io","Packages.com.novell.ldap","Packages.javax.naming.directory";
//
/*
ALiBS.gv.sy.reference = "ALiBS_sy_la,ALiBS_sy_no,ALiBS_sy_la_print" + // reference without "."
	"ALiBSsNV_n1_x0" + // ALiBSsNV_n1_x0 = ALiBSsNV.n1.x0 -->> reference without "."
	"ALiBS.n1"+ // ALiBS.n1 = ALiBSsNV.n1 --> compatibility reference
	"Novell"+ // var Novell = ALiBSsNV -->> reference to namespace
	"Bradesco"; // var Bradesco = ALiBSzBD -->> reference to namespace
*/
//
// *************************************************************************

