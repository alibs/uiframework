/* *************************************************************************
// INFO:

Used by:

Rhino command line
jswebjetty
jswebserver
novell drivers

// *************************************************************************
// TODO(2010-09-08):

TODO(2010-09-08): 

H2-JAVA
SQLITE-JAVA
SQLITE-ANDROID
SQLITE-CHROME (CHROME USA SQLITE PARA WEBDATABASE / WEB API ????)
SQLITE-FIREFOX ???? WEB API
SQLITE-OPERA ???? WEB API
*/
//
// *************************************************************************
//
if(!ALiBS.gv.wd) ALiBS.gv.wd = {}; // global values
//
if(!ALiBS.gv.wd.le) ALiBS.gv.wd.le = 0; // local log status
//
ALiBS.gv.wd.nn = 0; // x times loaded
//
ALiBS.gv.wd.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wd.cc = {}; // connection object 
//
ALiBS.gv.wd.dl = {}; // drivers/packages loaded (H2,SQLITE,MYSQL,ORACLE)
//
ALiBS.gv.wd.pt==""; // ROOT/Absolute path to databases
//
// *************************************************************************
//
ALiBS.wd = // server database h2/sqlite
{
	co: function(n,y,h,o,u,w,p) // connection object
	// n = (required)(String)Connection Name/Reference
	// y = (required)(String)DBase Type (H2,SQLITE,MYSQL,ORACLE)
	// h = (required)(String)host OR file path+name
	// o = (required)(String)port
	// u = (required)(String)user
	// w = (required)(String)password
	// p = (optional)(JSON)parameters {}
	//	?
	// return error code
	//	 0: no error
	//	-1: name is not string
	//	-2: type is not string
	//	-3: host/path is not string
	//
	//	-4: port is not string
	//	-5: user is not string
	//	-6: pass is not string
	//
	//	-21: exception loading org.sqlite.JDBC
	//	-22: exception in DriverManager.getConnection()
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.co",le:ALiBS.gv.wd.le});
		//
		if(typeof(n)!="string")
		{
			return -1; 
		}
		//
		if(typeof(y)!="string")
		{
			return -2; 
		}
		//
		if(typeof(h)!="string")
		{
			return -3; 
		}
		//
		if(typeof(ALiBS.gv.wd.cc[n])=="undefined" || !ALiBS.gv.wd.cc[n]) 
		{
			ALiBS.gv.wd.cc[n] = {};
		}
		//
		ALiBS.gv.wd.cc[n]["y"] = y.toUpperCase(); // DBase Type (H2,SQLITE,MYSQL,ORACLE)
		ALiBS.gv.wd.cc[n]["h"] = h; // host OR file path+name
		//
		// ************************
		//
		if(ALiBS.gv.wd.cc[n]["y"]=="H2")
		{
			//if(typeof(o)!="string")
			//{
			//	return -4; 
			//}
			//
			if(typeof(u)!="string")
			{
				return -5; 
			}
			//		
			if(typeof(w)!="string")
			{
				return -6; 
			}
			//
			ALiBS.gv.wd.cc[n]["o"] = o; // port
			ALiBS.gv.wd.cc[n]["u"] = u; // user
			ALiBS.gv.wd.cc[n]["w"] = w; // pass
			//
			if(typeof(ALiBS.gv.wd.dl["H2"])=="undefined" || !ALiBS.gv.wd.dl["H2"]) 
			{
				try
				{
					ALiBS.sy.la(0+0,"Loading H2 Packages...",{o:"ALiBS.wd"});
					//
					Packages.org.h2.jdbcx.JdbcConnectionPool; // driver
					//
					ALiBS.gv.wd.dl["H2"] = true;
				} 
				catch(e)
				{
					ALiBS.sy.la(1+0,"ALiBS.wd.co Exception(-21):"+e,{o:"ALiBS.wd"});
					return -21;
				} 
			}
			//
			try
			{
				if(typeof(ALiBS.gv.wd.cc[n]["cp"])=="undefined" || !ALiBS.gv.wd.cc[n]["cp"] || ALiBS.gv.wd.cc[n]["cp"].getActiveConnections()==0) 
				{
					ALiBS.gv.wd.cc[n]["cp"] = Packages.org.h2.jdbcx.JdbcConnectionPool.create("jdbc:h2:split:30:"+h,u,w); // connection pool
					//
					ALiBS.gv.wd.cc[n]["ts-cp-date"] = new Date;
				}
			} 
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.co Exception(-23):"+e,{o:"ALiBS.wd"});
				return -23;
			} 
			//
			try
			{
				ALiBS.gv.wd.cc[n]["c"] = ALiBS.gv.wd.cc[n]["cp"].getConnection(); // is faster to use cp.getConnection() than DriverManager.getConnection()
				//
				ALiBS.gv.wd.cc[n]["ts-c-date"] = new Date();
			} 
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.co Exception(-22):"+e,{o:"ALiBS.wd"});
				return -22;
			} 
		}
		//
		// ************************
		//
		else if(ALiBS.gv.wd.cc[n]["y"]=="SQLITE")
		{
			if(typeof(ALiBS.gv.wd.dl["SQLITE"])=="undefined" || !ALiBS.gv.wd.dl["SQLITE"]) 
			{
				try
				{
					ALiBS.sy.la(0+0,"Loading SQLITE Packages...",{o:"ALiBS.wd"});
					//
					java.lang.Class.forName("org.sqlite.JDBC"); // driver = msg = No suitable driver found 
					//
					ALiBS.gv.wd.dl["SQLITE"] = true;
				} 
				catch(e)
				{
					ALiBS.sy.la(1+0,"ALiBS.wd.co Exception(-21):"+e,{o:"ALiBS.wd"});
					return -21;
				} 
			}
			//
			ALiBS.gv.wd.cc[n]["ts-cp-date"] = new Date(); // no connection pool
			//
			try
			{
				ALiBS.gv.wd.cc[n]["c"] = Packages.java.sql.DriverManager.getConnection("jdbc:sqlite:"+h);
				//
				ALiBS.gv.wd.cc[n]["ts-c-date"] = new Date();
			} 
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.co Exception(-22):"+e,{o:"ALiBS.wd"});
				return -22;
			} 
		}
		//
		//ALiBS.wd.scc(n);
		//
		return 0; // no error
	},
	// *****************************************************************
	scc: function(n) // show values stored in ALiBS.gv.wd.cc
	// n = (required)(String)Connection Name/Reference
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.scc",le:ALiBS.gv.wd.le});
		//
		var t = new ALiBS.to.t();
		//
		t.rs[t.rs.length] = 
			{
				f00:"ALiBS.gv.wd.cc["+n+"][ts-cp]=",
				f01:(new Date)-ALiBS.gv.wd.cc[n]["ts-cp-date"]+" milisec"
			};
		t.rs[t.rs.length] = 
			{
				f00:"ALiBS.gv.wd.cc["+n+"][ts-c]=",
				f01:(new Date)-ALiBS.gv.wd.cc[n]["ts-c-date"]+" milisec"
			};
		//
		for(var i in ALiBS.gv.wd.cc[n])
		{
			if(i=="ts-cp-date" || i=="ts-c-date")
			{
				t.rs[t.rs.length] = 
					{
						f00:"ALiBS.gv.wd.cc["+n+"]["+i+"]=",
						f01:ALiBS.dt.ts({d:ALiBS.gv.wd.cc[n][i],s0:"-",s1:" ",s2:":",s3:"."})
					};
			}
			else
			{
				t.rs[t.rs.length] = 
					{
						f00:"ALiBS.gv.wd.cc["+n+"]["+i+"]=",
						f01:ALiBS.gv.wd.cc[n][i]
					};
			}
		}
		//
		t.rs[t.rs.length] = 
			{
				f00:"(JdbcConnectionPool)getMaxConnections()=",
				f01:ALiBS.gv.wd.cc[n]["cp"].getMaxConnections()
			};
		t.rs[t.rs.length] = 
			{
				f00:"(JdbcConnectionPool)getActiveConnections()=",
				f01:ALiBS.gv.wd.cc[n]["cp"].getActiveConnections()
			};
		t.rs[t.rs.length] = 
			{
				f00:"(JdbcConnectionPool)getLoginTimeout()=",
				f01:ALiBS.gv.wd.cc[n]["cp"].getLoginTimeout()+" sec"
			};
		//
		t.rs[t.rs.length] = 
			{
				f00:"(Connection)getCatalog()=",
				f01:ALiBS.gv.wd.cc[n]["c"].getCatalog()
			};
		t.rs[t.rs.length] = 
			{
				f00:"(Connection)getAutoCommit()=",
				f01:ALiBS.gv.wd.cc[n]["c"].getAutoCommit()
			};
		//
		var m = ALiBS.gv.wd.cc[n]["c"].getMetaData();
		//
		t.rs[t.rs.length] = 
			{
				f00:"(MetaData)getURL()=",
				f01:m.getURL()
			};
		t.rs[t.rs.length] = 
			{
				f00:"(MetaData)getUserName()=",
				f01:m.getUserName()
			};
		//
		ALiBS.to.o(t,0);
		//
		for(var i=0;i<t.rs.length;i++)
		{
			ALiBS.sy.la(5+0,""+t.rs[i].f00+t.rs[i].f01,{o:"ALiBS.wd.scc",le:ALiBS.gv.wd.le});
		}
	},
	// *****************************************************************
	cx: function(n) // close conection
	// n = (required)(String)Connection Name/Reference
	//
	// return error code
	//	 0: no error
	//	-1: invalid connection name
	//	-21: invalid SQLITE connection to close
	//	-22: invalid H2 connection to close
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.cx",le:ALiBS.gv.wd.le});
		//
		if(typeof(ALiBS.gv.wd.cc[n])=="undefined" || !ALiBS.gv.wd.cc[n]) 
		{
			return -1;
		}
		//
		// ************************
		//
		if(ALiBS.gv.wd.cc[n]["y"]=="H2")
		{
			try
			{
				ALiBS.gv.wd.cc[n]["c"].close();
/*
close() throws SQLException
Closes this connection. All open statements, prepared statements and result sets that where created by this connection become invalid after calling this method. If there is an uncommitted transaction, it will be rolled back.
*/

				ALiBS.gv.wd.cc[n]["cp"].dispose();

/*
close() throws SQLException
Closes this connection. All open statements, prepared statements and result sets that where created by this connection become invalid after calling this method. If there is an 
uncommitted transaction, it will be rolled back.
*/

			} 
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.cx Exception(-22):"+e,{o:"ALiBS.wd"});
				return -22;
			} 
		}
		//
		// ************************
		//
		else if(ALiBS.gv.wd.cc[n]["y"]=="SQLITE")
		{
			try
			{
				ALiBS.gv.wd.cc[n]["c"].close();
			} 
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.cx Exception(-21):"+e,{o:"ALiBS.wd"});
				return -21;
			} 
		}
		//
		return 0; // no error
	},
	// *****************************************************************
	cm: function(n,cm,p) // command select, insert, update, delete
	// n = (required)(String)Connection Name/Reference
	// cm = (required)(String)command
	// p = (optional)(JSON)parameters {} 
	//	rt = (String)return type
	//		"" = [EMPTY] = AUTOMATIC DETECTION = DEFAULT
	//			s1 = String = rs.getString(1)
	//			to = ALiBS.to
	//		rs = FORCE data result set 
	//		s1 = FORCE String = rs.getString(1)
	//		to = FORCE ALiBS.to
	//
	// return error code
	//	rs: data result set
	//	s1: String
	//	to: ALiBS.to.ts (string)
	//	td: ALiBS.to.t (table object)
	//
	//	-1: invalid connection name
	//	-2: invalid connection to close
	//
	//	-11: exception in DriverManager.getConnection()
	//	-12: exception in executeQuery()
	//	-21: exception in cp.getConnection()
	//	-22: exception in execute()
	//
	//	-100: no rs to return
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.cm",le:ALiBS.gv.wd.le});
		//
		if(typeof(ALiBS.gv.wd.cc[n])=="undefined" || !ALiBS.gv.wd.cc[n])
		{
			return -1;
		}
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(!z.rt || typeof(z.rt)!="string")
		{
			if(    cm.indexOf(" from ")>0    &&    (cm.indexOf("select *")==0 || cm.indexOf("select f")==0 || cm.indexOf("select g")==0)    )
			{
				z.rt = "to"; // ALiBS.to
			}
			else
			{
				z.rt = "s1"; // String
			}
		}
		//
		// local varibles [BEST-PRACTICE]
		// z.rs;
		// z.st;
		z.ar = new Array();
		//
		// ************************
		//
		if(ALiBS.gv.wd.cc[n]["y"]=="H2")
		{
			if(ALiBS.gv.wd.cc[n]["c"].isClosed())
			{
				try
				{
					ALiBS.gv.wd.cc[n]["c"] = ALiBS.gv.wd.cc[n]["cp"].getConnection(); // is faster to use cp.getConnection() than DriverManager.getConnection()
				} 
				catch(e)
				{
					ALiBS.sy.la(1+0,"ALiBS.wd.cm Exception(-21):"+e,{o:"ALiBS.wd"});
					return -21;
				} 
			}
			//
			z.st = ALiBS.gv.wd.cc[n]["c"].createStatement();
			//
			try
			{
				if(cm.toLowerCase().indexOf("select")>=0) 
				{
					z.rs = z.st.executeQuery(cm); // returns [ResultSet] - (select)
				}
				else 
				{
					z.rs = z.st.executeUpdate(cm); // returns [int] update count - insert, update, delete, create, drop
					//
					if(cm.toLowerCase().indexOf("insert")>=0) 
					{
						z.rs = ALiBS.wd.gs1(z.st.getGeneratedKeys());
						ALiBS.sy.la(5+0,"getGeneratedKeys()="+z.rs,{o:"ALiBS.wd.cm",le:ALiBS.gv.wd.le});
					}
				}
			}
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.cm Exception(-22:"+e,{o:"ALiBS.wd"});
				return -22;
			}
			//
			// ALiBS.gv.wd.cc[n]["c"].close(); TODO(2011-06-06): should close connection pool????
			//z.st.close(); TODO(2011-06-06): should close statement????
		}
		//
		// ************************
		//
		else if(ALiBS.gv.wd.cc[n]["y"]=="SQLITE")
		{
			if(ALiBS.gv.wd.cc[n]["c"].isClosed())
			{
				try
				{
					ALiBS.gv.wd.cc[n]["c"] = Packages.java.sql.DriverManager.getConnection("jdbc:sqlite:"+ALiBS.gv.wd.cc[n]["h"]);
				} 
				catch(e)
				{
					ALiBS.sy.la(1+0,"ALiBS.wd.cm Exception(-11):"+e,{o:"ALiBS.wd"});
					return -11;
				} 
			}
			//
			z.st = ALiBS.gv.wd.cc[n]["c"].createStatement();
			//
			try
			{
				z.rs = z.st.executeQuery(cm);
			}
			catch(e)
			{
				ALiBS.sy.la(1+0,"ALiBS.wd.cm Exception(-12):"+e,{o:"ALiBS.wd"});
				return -12;
			}
			//
			//z.st.close(); TODO(2011-06-06): should close statement????
		}
		//
		var r; // [BEST-PRACTICE]
		//
		if(!z.rs)
		{
			r = -100;
		}
		else if(typeof(z.rs)=="string")
		{
			r = z.rs;
		}
		else if(z.rt=="to" || z.rt=="td") // TODO
		{
			// http://docs.oracle.com/javase/1.4.2/docs/api/java/sql/ResultSet.html
			// http://docs.oracle.com/javase/1.4.2/docs/api/java/sql/ResultSetMetaData.html
			//
			z.md = z.rs.getMetaData();
			z.nc = z.md.getColumnCount(); // number of columns
			//
			z.to = new ALiBS.to.t();
			//
			for(var i=1;i<=z.nc;i++) // field names
			{
				z.cn = z.md.getColumnName(i).toLowerCase();
				//
				z.to.nm[z.cn] = z.to.cp[z.cn] = z.cn; // field names + field captions
			}
			//
			while(z.rs.next()) // for each record
			{
				z.len = z.to.rs.length;
				z.to.rs[z.len] = {};
				//
				for(var i=1;i<=z.nc;i++)
				{
					z.cn = z.md.getColumnName(i).toLowerCase(); // column name
					//
					z.va = z.rs.getString(z.cn);
					//
					if(!z.va) z.to.rs[z.len][z.cn] = "";
					else z.to.rs[z.len][z.cn] = z.va;
				}
			}
			//
			ALiBS.to.un(z.to);
			//
			if(z.rt=="to") // string
			{
				r = ALiBS.to.ts(z.to); 
			}
			else // z.rt=="td" = table object
			{
				r = z.to;
			}
		}
		else if(z.rt=="s1")
		{
			r = ALiBS.wd.gs1(z.rs);
		}
		else // rt == rs
		{
			r = z.rs;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	gs1: function(rs,p) // getString(1)
	// rs = (required)(String)result set
	// p = (optional)(JSON)parameters {} 
	//	se = (String)separator
	//		"" = [EMPTY] = DEFAULT
	{
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.se)!="string") z.se = ""; // DEFAULT 
		//
		var r = ""; // [BEST-PRACTICE]
		//
		if(rs && rs.next)
		{
			while(rs.next()) 
			{
				//r += rs.getArray(1) + z.se;
				r += rs.getString(1) + z.se;
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	d: function(n,t,w) // get delete command
	// n = (required)(String)Connection Name/Reference
	// t = (required)(String)table name
	// w = (required)(String)where clause
	//
	// return error code
	//	 s: command string
	//	-1: invalid connection name
	//	-2: invalid table name
	//	-3: invalid where clause
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.d",le:ALiBS.gv.wd.le});
		//
		if(typeof(ALiBS.gv.wd.cc[n])=="undefined" || !ALiBS.gv.wd.cc[n]) 
		{
			return -1;
		}
		//
		if(typeof(t)!="string") 
		{
			return -2;
		}
		//
		if(typeof(w)!="string") 
		{
			return -3;
		}
		//
		var s = new Array();
		//
		// ************************
		//
		if(ALiBS.gv.wd.cc[n]["y"]=="H2" || ALiBS.gv.wd.cc[n]["y"]=="SQLITE")
		{
			s[s.length] = 'DELETE FROM ' + t + ' WHERE ' + w + ";"; //DELETE FROM main.log WHERE f30!="x"
		}
		else
		{
			//
		}
		//
		return s.join("");
	},
	// *****************************************************************
	i: function(n,t,p) // get insert command
	// n = (required)(String)Connection Name/Reference
	// t = (required)(String)table name
	// y = (optional)(JSON)field {f00:"abcd",f01:"1234"}

	//
	// return error code
	//	 s: command string
	//	-1: invalid connection name
	//	-2: invalid table name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.i",le:ALiBS.gv.wd.le});
		//
		//ALiBS.wd.scc(n);
		//
		if(typeof(ALiBS.gv.wd.cc[n])=="undefined" || !ALiBS.gv.wd.cc[n]) 
		{
			return -1;
		}
		//
		if(typeof(t)!="string") 
		{
			return -2;
		}
		//
		var s = new Array();
		var n0 = 0;
		//
		// ************************
		//
		if(ALiBS.gv.wd.cc[n]["y"]=="H2")
		{
			s[s.length] = 'INSERT INTO ' + t + ' ('; // 'INSERT INTO main.log ('
			//
			n0 = 0;
			//			
			for(var i in p) // get field names
			{
				if(n0==0)
				{
					s[s.length] = i;
					n0++;
				}
				else s[s.length] = "," + i;
			}
  			//
			s[s.length] = ') VALUES (';
			//
			n0 = 0;
			//			
			for(var i in p) // get field values
			{
				if(n0==0)
				{
					s[s.length] = "'" + p[i] + "'";
					n0++;
				}
				else s[s.length] = ",'" + p[i] + "'";
			}
  			//
			s[s.length] = ");";
		}
		//
		// ************************
		//
		else if(ALiBS.gv.wd.cc[n]["y"]=="SQLITE")
		{
			s[s.length] = 'INSERT INTO ' + t + ' ('; // 'INSERT INTO main.log ('
			//
			n0 = 0;
			//			
			for(var i in p) // get field names
			{
				if(n0==0)
				{
					s[s.length] = "'" + i + "'";
					n0++;
				}
				else s[s.length] = ",'" + i + "'";
			}
  			//
			s[s.length] = ') VALUES (';
			//
			n0 = 0;
			//			
			for(var i in p) // get field values
			{
				if(n0==0)
				{
					s[s.length] = "'" + p[i] + "'";
					n0++;
				}
				else s[s.length] = ",'" + p[i] + "'";
			}
  			//
			s[s.length] = ");";
		}
		else
		{
			//
		}
		//
		var r = s.join("").replace("('null',","(null,");
		return r;
		//return s.join("");
	},
	// *****************************************************************
/*
	TODO(2011-03-04): criar as funcoes abaixo

	select("tabela.campo,tabela2.campo2",where)

	update(tabela,where,{f00:"",f01:""})

	delete(tabela,where)
*/
	// *****************************************************************
	xxxx_sqlite_x: function(conn) // sqlite
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wd.xx",le:ALiBS.gv.wd.le});
		//
		//Use the connection to create a Statement object
		var stat = conn.createStatement();
		stat.executeUpdate("drop table if exists people ;");
		//returns: 0

		stat.executeUpdate("create table people (name,occupation);");
		//returns: 0

		//Create a prepared statement object
		var prep = conn.prepareStatement("insert into people values (?, ?);");
		prep.setString(1, "Gandhi");
		prep.setString(2, "politics");
		prep.addBatch();
		prep.setString(1, "Turing");
		prep.setString(2, "Computers");
		prep.addBatch();
		conn.setAutoCommit(false);
		//Execute the prepared statements
		prep.executeBatch();
		//returns: [I@6e1408

		conn.setAutoCommit(true);
		var resultSet = stat.executeQuery("select * from people;");
		while(resultSet.next())
		{
			ALiBS.sy.la(7+0,""+resultSet.getString("name") + " – " + resultSet.getString("occupation"),{o:"ALiBS.wd.xx",le:ALiBS.gv.wd.le});
		}
		//returns
		// Gandhi - politics
		// Turing – Computers
		//cleanup
		//While SQLite really isn’t the tool to use if
		//data needs to be shared across multiple applications
		// or users, be sure to close resultSet otherwise the
		//db will stay locked

		resultSet.close();
		stat.close();
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wd("+(++ALiBS.gv.wd.nn)+"x)",{o:"ALiBS.wd"});
ALiBS.sy.la(0+0,"ALiBS.gv.wd.le="+ALiBS.gv.wd.le+ALiBS.gv.sy.se,{o:"ALiBS.wd"});

