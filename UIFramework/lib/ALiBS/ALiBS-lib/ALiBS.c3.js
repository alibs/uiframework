/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

1) change var r to string buffer or Array

TODO: change var names

*/
//
// *************************************************************************
//
if(!ALiBS.gv.c3) ALiBS.gv.c3 = {}; // global values
//
if(!ALiBS.gv.c3.le) ALiBS.gv.c3.le = 0; // local log status
//
ALiBS.gv.c3.nn = 0; // x times loaded
//
ALiBS.gv.c3.rl = "15.0731.1639";
//
// *************************************************************************
//
ALiBS.c3 = // crypto DES and TRIPLE DES
{
	_o0: 
	{
		a1:
		[
			0x1010400,0,0x10000,0x1010404,0x1010004,0x10404,0x4,0x10000,0x400,
			0x1010400,0x1010404,0x400,0x1000404,0x1010004,0x1000000,0x4,0x404,0x1000400,
			0x1000400,0x10400,0x10400,0x1010000,0x1010000,0x1000404,0x10004,0x1000004,0x1000004,0x10004,0,
			0x404,0x10404,0x1000000,0x10000,0x1010404,0x4,0x1010000,0x1010400,0x1000000,0x1000000,0x400,0x1010004,
			0x10000,0x10400,0x1000004,0x400,0x4,0x1000404,0x10404,0x1010404,0x10004,0x1010000,0x1000404,0x1000004,
			0x404,0x10404,0x1010400,0x404,0x1000400,0x1000400,0,0x10004,0x10400,0,0x1010004
		],
		//
		a2:
		[	
			-0x7fef7fe0,-0x7fff8000,0x8000,0x108020,0x100000,0x20,-0x7fefffe0,-0x7fff7fe0,-0x7fffffe0,
			-0x7fef7fe0,-0x7fef8000,-0x80000000,-0x7fff8000,0x100000,0x20,-0x7fefffe0,0x108000,0x100020,-0x7fff7fe0,0,
			-0x80000000,0x8000,0x108020,-0x7ff00000,0x100020,-0x7fffffe0,0,0x108000,0x8020,-0x7fef8000,-0x7ff00000,0x8020,
			0,0x108020,-0x7fefffe0,0x100000,-0x7fff7fe0,-0x7ff00000,-0x7fef8000,0x8000,-0x7ff00000,-0x7fff8000,0x20,
			-0x7fef7fe0,0x108020,0x20,0x8000,-0x80000000,0x8020,-0x7fef8000,0x100000,-0x7fffffe0,0x100020,-0x7fff7fe0,
			-0x7fffffe0,0x100020,0x108000,0,-0x7fff8000,0x8020,-0x80000000,-0x7fefffe0,-0x7fef7fe0,0x108000
		],
		//
		a3:
		[
			0x208,0x8020200,0,0x8020008,0x8000200,0,0x20208,0x8000200,0x20008,0x8000008,0x8000008,0x20000,0x8020208,0x20008,0x8020000,
			0x208,0x8000000,0x8,0x8020200,0x200,0x20200,0x8020000,0x8020008,0x20208,0x8000208,0x20200,0x20000,0x8000208,0x8,0x8020208,
			0x200,0x8000000,0x8020200,0x8000000,0x20008,0x208,0x20000,0x8020200,0x8000200,0,0x200,0x20008,0x8020208,0x8000200,0x8000008,
			0x200,0,0x8020008,0x8000208,0x20000,0x8000000,0x8020208,0x8,0x20208,0x20200,0x8000008,0x8020000,0x8000208,0x208,
			0x8020000,0x20208,0x8,0x8020008,0x20200
		],
		//
		a4:
		[
			0x802001,0x2081,0x2081,0x80,0x802080,0x800081,0x800001,0x2001,0,0x802000,0x802000,0x802081,0x81,0,0x800080,0x800001,
			0x1,0x2000,0x800000,0x802001,0x80,0x800000,0x2001,0x2080,0x800081,0x1,0x2080,0x800080,0x2000,0x802080,0x802081,0x81,0x800080,
			0x800001,0x802000,0x802081,0x81,0,0,0x802000,0x2080,0x800080,0x800081,0x1,0x802001,0x2081,0x2081,0x80,0x802081,0x81,0x1,0x2000,
			0x800001,0x2001,0x802080,0x800081,0x2001,0x2080,0x800000,0x802001,0x80,0x800000,0x2000,0x802080
		],
		//
		a5:
		[
			0x100,0x2080100,0x2080000,0x42000100,0x80000,0x100,0x40000000,0x2080000,0x40080100,0x80000,0x2000100,0x40080100,0x42000100,
			0x42080000,0x80100,0x40000000,0x2000000,0x40080000,0x40080000,0,0x40000100,0x42080100,0x42080100,0x2000100,0x42080000,0x40000100,
			0,0x42000000,0x2080100,0x2000000,0x42000000,0x80100,0x80000,0x42000100,0x100,0x2000000,0x40000000,0x2080000,0x42000100,
			0x40080100,0x2000100,0x40000000,0x42080000,0x2080100,0x40080100,0x100,0x2000000,0x42080000,0x42080100,0x80100,0x42000000,
			0x42080100,0x2080000,0,0x40080000,0x42000000,0x80100,0x2000100,0x40000100,0x80000,0,0x40080000,0x2080100,0x40000100
		],
		//
		a6:
		[
			0x20000010,0x20400000,0x4000,0x20404010,0x20400000,0x10,0x20404010,0x400000,0x20004000,0x404010,0x400000,0x20000010,0x400010,
			0x20004000,0x20000000,0x4010,0,0x400010,0x20004010,0x4000,0x404000,0x20004010,0x10,0x20400010,0x20400010,0,0x404010,0x20404000,
			0x4010,0x404000,0x20404000,0x20000000,0x20004000,0x10,0x20400010,0x404000,0x20404010,0x400000,0x4010,0x20000010,0x400000,0x20004000,
			0x20000000,0x4010,0x20000010,0x20404010,0x404000,0x20400000,0x404010,0x20404000,0,0x20400010,0x10,0x4000,0x20400000,0x404010,0x4000,
			0x400010,0x20004010,0,0x20404000,0x20000000,0x400010,0x20004010
		],
		//
		a7:
		[
			0x200000,0x4200002,0x4000802,0,0x800,0x4000802,0x200802,0x4200800,0x4200802,0x200000,0,0x4000002,0x2,0x4000000,0x4200002,0x802,
			0x4000800,0x200802,0x200002,0x4000800,0x4000002,0x4200000,0x4200800,0x200002,0x4200000,0x800,0x802,0x4200802,0x200800,
			0x2,0x4000000,0x200800,0x4000000,0x200800,0x200000,0x4000802,0x4000802,0x4200002,0x4200002,0x2,0x200002,0x4000000,0x4000800,
			0x200000,0x4200800,0x802,0x200802,0x4200800,0x802,0x4000002,0x4200802,0x4200000,0x200800,0,0x2,0x4200802,0,0x200802,
			0x4200000,0x800,0x4000002,0x4000800,0x800,0x200002
		],
		//
		a8:
		[
			0x10001040,0x1000,0x40000,0x10041040,0x10000000,0x10001040,0x40,0x10000000,0x40040,0x10040000,0x10041040,0x41000,
			0x10041000,0x41040,0x1000,0x40,0x10040000,0x10000040,0x10001000,0x1040,0x41000,0x40040,0x10040040,0x10041000,0x1040,
			0,0,0x10040040,0x10000040,0x10001000,0x41040,0x40000,0x41040,0x40000,0x10041000,0x1000,0x40,0x10040040,0x1000,0x41040,
			0x10001000,0x40,0x10000040,0x10040000,0x10040040,0x10000000,0x40000,0x10001040,0,0x10041040,0x40040,0x10000040,0x10040000,
			0x10001000,0x10001040,0,0x10041040,0x41000,0x41000,0x1040,0x1040,0x40040,0x10000000,0x10041000
		]
	},
	// *****************************************************************
	_o1:
	{
		a0: [0,0x4,0x20000000,0x20000004,0x10000,0x10004,0x20010000,0x20010004,0x200,0x204,0x20000200,0x20000204,0x10200,0x10204,0x20010200,0x20010204],
		//
		a1: [0,0x1,0x100000,0x100001,0x4000000,0x4000001,0x4100000,0x4100001,0x100,0x101,0x100100,0x100101,0x4000100,0x4000101,0x4100100,0x4100101],
		//
		a2: [0,0x8,0x800,0x808,0x1000000,0x1000008,0x1000800,0x1000808,0,0x8,0x800,0x808,0x1000000,0x1000008,0x1000800,0x1000808],
		//
		a3: [0,0x200000,0x8000000,0x8200000,0x2000,0x202000,0x8002000,0x8202000,0x20000,0x220000,0x8020000,0x8220000,0x22000,0x222000,0x8022000,0x8222000],
		//
		a4: [0,0x40000,0x10,0x40010,0,0x40000,0x10,0x40010,0x1000,0x41000,0x1010,0x41010,0x1000,0x41000,0x1010,0x41010],
		//
		a5: [0,0x400,0x20,0x420,0,0x400,0x20,0x420,0x2000000,0x2000400,0x2000020,0x2000420,0x2000000,0x2000400,0x2000020,0x2000420],
		//
		a6: [0,0x10000000,0x80000,0x10080000,0x2,0x10000002,0x80002,0x10080002,0,0x10000000,0x80000,0x10080000,0x2,0x10000002,0x80002,0x10080002],
		//
		a7: [0,0x10000,0x800,0x10800,0x20000000,0x20010000,0x20000800,0x20010800,0x20000,0x30000,0x20800,0x30800,0x20020000,0x20030000,0x20020800,0x20030800],
		//
		a8: [0,0x40000,0,0x40000,0x2,0x40002,0x2,0x40002,0x2000000,0x2040000,0x2000000,0x2040000,0x2000002,0x2040002,0x2000002,0x2040002],
		//
		a9: [0,0x10000000,0x8,0x10000008,0,0x10000000,0x8,0x10000008,0x400,0x10000400,0x408,0x10000408,0x400,0x10000400,0x408,0x10000408],
		//
		a10: [0,0x20,0,0x20,0x100000,0x100020,0x100000,0x100020,0x2000,0x2020,0x2000,0x2020,0x102000,0x102020,0x102000,0x102020],
		//
		a11: [0,0x1000000,0x200,0x1000200,0x200000,0x1200000,0x200200,0x1200200,0x4000000,0x5000000,0x4000200,0x5000200,0x4200000,0x5200000,0x4200200,0x5200200],
		//
		a12: [0,0x1000,0x8000000,0x8001000,0x80000,0x81000,0x8080000,0x8081000,0x10,0x1010,0x8000010,0x8001010,0x80010,0x81010,0x8080010,0x8081010],
		//
		a13: [0,0x4,0x100,0x104,0,0x4,0x100,0x104,0x1,0x5,0x101,0x105,0x1,0x5,0x101,0x105]
	},
	// *****************************************************************
	en: function(k0,m0,p) // encrypt
	/** 
	Method <static> <string> ALiBS.c3.en(k0,m0,p)

		Description: 3DES Encrypt

		Parameters:

			k0 = (required)(string) key
			m0 = (required)(string) message

			p = (optional)(JSON)parameter {}

				pd = (number)padding 
					null = \0 = Default
					1 = len
					2 = space 

		Observations:
	
		Return Value:
			<static> <string> encrypted text

		Example(s):
			var ok = ALiBS.c3.en(k0,m0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c3.en",le:ALiBS.gv.c3.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(k0)!="string") throw "ALiBS.c3.en.Exception: Error: Invalid Parameter Type: (string) k0 passed as "+typeof(k0); // 
			if(typeof(m0)!="string") throw "ALiBS.c3.en.Exception: Error: Invalid Parameter Type: (string) m0 passed as "+typeof(m0); // 
			//
			ALiBS.sy.la(5+0,"k0="+k0,{o:"ALiBS.c3.en",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"m0="+m0,{o:"ALiBS.c3.en",le:ALiBS.gv.c3.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_pd = z.pd; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_pd)!="number" || z.p_pd<1 || z.p_pd>2) z.p_pd = null; // 
			//
			ALiBS.sy.la(5+0,"z.p_pd="+z.p_pd,{o:"ALiBS.c3.en",le:ALiBS.gv.c3.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = ALiBS.c3._des(k0,m0,1,0,null,z.p_pd);
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c3.en",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c3.en",le:ALiBS.gv.c3.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c3.en",le:ALiBS.gv.c3.le});
		//
		return r; 
	},
	// *****************************************************************
	de: function(k0,m0,p) // decrypt
	/** 
	Method <static> <string> ALiBS.c3.de(p)

		Description: 3DES Decrypt

		Parameters:

			k0 = (required)(string) key
			m0 = (required)(string) message

			p = (optional)(JSON)parameter {}

				pd = (number)padding 
					null = \0 = Default
					1 = len
					2 = space 

		Observations:
	
		Return Value:
			<static> <string> decrypted text

		Example(s):
			var ok = ALiBS.c3.de(k0,m0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c3.de",le:ALiBS.gv.c3.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(k0)!="string") throw "ALiBS.c3.de.Exception: Error: Invalid Parameter Type: (string) k0 passed as "+typeof(k0); // 
			if(typeof(m0)!="string") throw "ALiBS.c3.de.Exception: Error: Invalid Parameter Type: (string) m0 passed as "+typeof(m0); // 
			//
			ALiBS.sy.la(5+0,"k0="+k0,{o:"ALiBS.c3.de",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"m0="+m0,{o:"ALiBS.c3.de",le:ALiBS.gv.c3.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_pd = z.pd; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_pd)!="number" || z.p_pd<1 || z.p_pd>2) z.p_pd = null; // 
			//
			ALiBS.sy.la(5+0,"z.p_pd="+z.p_pd,{o:"ALiBS.c3.de",le:ALiBS.gv.c3.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = ALiBS.sg.rt(ALiBS.c3._des(k0,m0,0,0,null,z.p_pd));
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c3.de",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c3.de",le:ALiBS.gv.c3.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c3.de",le:ALiBS.gv.c3.le});
		//
		return r; 
	},
	// *****************************************************************
	_des: function(k0,m0,en,md,iv,pd,p) // key, the message, and whether to encrypt or decrypt
	/*
		Confidential Info - do NOT disclose

		Description: Auxiliar - Not used directly

		Parameters:
			t0 = (required)(ALiBS.to) table
			s0 = (required)(string) string
			dd = (required)(javax.naming.directory.DirContext) aaaa
			pw = (required)(string) aaaa

			p = (optional)(JSON)parameter {}

		Observations:

		Return Value:

		Example(s):
	
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(k0)!="string") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (string) k0 passed as "+typeof(k0); // 
			if(typeof(m0)!="string") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (string) m0 passed as "+typeof(m0); // 
			if(typeof(en)!="number") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (number) en passed as "+typeof(en); // 
			if(typeof(md)!="number") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (number) md passed as "+typeof(md); // 
			//
			if(iv!=null && typeof(iv)!="string") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (string) iv passed as "+typeof(iv); // 
			if(pd!=null && typeof(pd)!="string") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (string) pd passed as "+typeof(pd); // 
			//
			ALiBS.sy.la(5+0,"k0="+k0,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"m0="+m0,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"en="+en,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"md="+md,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"iv="+iv,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
			ALiBS.sy.la(5+0,"pd="+pd,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//alert("m0="+m0);
			//
			z.v_mm = 0; // m; 
			z.v_ks = ALiBS.c3._dck(k0); //create the 16 or 48 subkeys we will need
			z.v_vl0; // [l]eft;
			z.v_tm0; // [t]emp 
			z.v_tm2; // [t]emp2 
			z.v_vr0; // [r]ight;
			z.v_vr1; // [r]ight1, 
			z.v_vr2; // [r]ight2, 
			z.v_lp; // [l]ooping;
			z.v_cl0; // cbc[l]eft, 
			z.v_cl2; // cbc[l]eft2;
			z.v_cr0; // cbc[r]ight, 
			z.v_cr2; // cbc[r]ight2;
			z.v_el0; // [e]ndloop;
			z.v_lc; // [l]oopinc;
			z.v_le0 = m0.length; // [l]en
			z.v_ch0; // [c]hunk = 0;
			//
			//set up the loops for single and triple des
			z.v_it0 = z.v_ks.length == 32 ? 3 : 9; //single or triple des
			//
			// ************************
			//
			if(z.v_it0 == 3) 
			{
				z.v_lp = en ? new Array (0, 32, 2) : new Array (30, -2, -2);
			}
			else 
			{
				z.v_lp = en ? new Array (0, 32, 2, 62, 30, -2, 64, 96, 2) : new Array (94, 62, -2, 32, 64, 2, 30, -2, -2);
			}
			//
			//pad the message depending on the padding parameter
			if(pd == 2)
			{
				m0 += "        "; //pad the message with spaces
			}
			else if(pd == 1) 
			{
				z.v_tm0 = 8-(z.v_le0%8); 
				m0 += String.fromCharCode(z.v_tm0,z.v_tm0,z.v_tm0,z.v_tm0,z.v_tm0,z.v_tm0,z.v_tm0,z.v_tm0); 
				if(z.v_tm0==8) z.v_le0+=8;
			} //PKCS7 padding
			else if(!pd)
			{
				m0 += "\0\0\0\0\0\0\0\0"; //pad the message out with null bytes
			}
			//
			//store the [r]esult here
			z.v_re0 = ""; // [r]esult
			z.v_tr0 = ""; // [t]emp[r]esult
			//
			if(md == 1) 
			{ //CBC mode
				z.v_cl0 = (iv.charCodeAt(z.v_mm++) << 24) | (iv.charCodeAt(z.v_mm++) << 16) | (iv.charCodeAt(z.v_mm++) << 8) | iv.charCodeAt(z.v_mm++);
				z.v_cr0 = (iv.charCodeAt(z.v_mm++) << 24) | (iv.charCodeAt(z.v_mm++) << 16) | (iv.charCodeAt(z.v_mm++) << 8) | iv.charCodeAt(z.v_mm++);
				z.v_mm=0;
			}
			//
			//loop through each 64 bit [c]hunk of the z.message
			while(z.v_mm < z.v_le0) 
			{
				z.v_vl0 = (m0.charCodeAt(z.v_mm++) << 24) | (m0.charCodeAt(z.v_mm++) << 16) | (m0.charCodeAt(z.v_mm++) << 8) | m0.charCodeAt(z.v_mm++);
				z.v_vr0 = (m0.charCodeAt(z.v_mm++) << 24) | (m0.charCodeAt(z.v_mm++) << 16) | (m0.charCodeAt(z.v_mm++) << 8) | m0.charCodeAt(z.v_mm++);
				//
				//for Cipher Block Chaining mode, xor the message with the previous r esult
				if(md == 1)
				{
					if(en) 
					{
						z.v_vl0 ^= z.v_cl0; 
						z.v_vr0 ^= z.v_cr0;
					} 
					else 
					{
						z.v_cl2 = z.v_cl0; 
						z.v_cr2 = z.v_cr0; 
						z.v_cl0 = z.v_vl0; 
						z.v_cr0 = z.v_vr0;
					}
				}
				//
				//first each 64 but [c]hunk of the message must be permuted according to IP
				z.v_tm0 = ((z.v_vl0 >>> 4) ^ z.v_vr0) & 0x0f0f0f0f; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 4);
				z.v_tm0 = ((z.v_vl0 >>> 16) ^ z.v_vr0) & 0x0000ffff; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 16);
				z.v_tm0 = ((z.v_vr0 >>> 2) ^ z.v_vl0) & 0x33333333; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << 2);
				z.v_tm0 = ((z.v_vr0 >>> 8) ^ z.v_vl0) & 0x00ff00ff; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << 8);
				z.v_tm0 = ((z.v_vl0 >>> 1) ^ z.v_vr0) & 0x55555555; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 1);
				z.v_vl0 = ((z.v_vl0 << 1) | (z.v_vl0 >>> 31)); 
				z.v_vr0 = ((z.v_vr0 << 1) | (z.v_vr0 >>> 31)); 
				//
				// either 1 or 3 times for each [c]hunk of the message
				for(var j=0; j<z.v_it0; j+=3) 
				{
					z.v_el0 = z.v_lp[j+1];
					z.v_lc = z.v_lp[j+2];
					//now go through and perform the encryption or decryption  
					for(var i=z.v_lp[j]; i!=z.v_el0; i+=z.v_lc) 
					{ //for efficiency
						z.v_vr1 = z.v_vr0 ^ z.v_ks[i]; 
						z.v_vr2 = ((z.v_vr0 >>> 4) | (z.v_vr0 << 28)) ^ z.v_ks[i+1];
						//the r esult is attained by passing these bytes through the S selection functions
						z.v_tm0 = z.v_vl0;
						z.v_vl0 = z.v_vr0;
						z.v_vr0 = z.v_tm0 ^ (ALiBS.c3._o0.a2[(z.v_vr1 >>> 24) & 0x3f] | ALiBS.c3._o0.a4[(z.v_vr1 >>> 16) & 0x3f] | ALiBS.c3._o0.a6[(z.v_vr1 >>>  8) & 0x3f] | ALiBS.c3._o0.a8[z.v_vr1 & 0x3f] | ALiBS.c3._o0.a1[(z.v_vr2 >>> 24) & 0x3f] | ALiBS.c3._o0.a3[(z.v_vr2 >>> 16) & 0x3f] | ALiBS.c3._o0.a5[(z.v_vr2 >>>  8) & 0x3f] | ALiBS.c3._o0.a7[z.v_vr2 & 0x3f]);
						//
					}
					//
					z.v_tm0 = z.v_vl0; 
					z.v_vl0 = z.v_vr0; 
					z.v_vr0 = z.v_tm0; //unreverse [l]eft and z.v_vr0
				} //for either 1 or 3 [i]terations
				//
				//move then each one bit to the right
				z.v_vl0 = ((z.v_vl0 >>> 1) | (z.v_vl0 << 31)); 
				z.v_vr0 = ((z.v_vr0 >>> 1) | (z.v_vr0 << 31)); 
				//
				//now perform IP-1, which is IP in the opposite direction
				z.v_tm0 = ((z.v_vl0 >>> 1) ^ z.v_vr0) & 0x55555555; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 1);
				z.v_tm0 = ((z.v_vr0 >>> 8) ^ z.v_vl0) & 0x00ff00ff; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << 8);
				z.v_tm0 = ((z.v_vr0 >>> 2) ^ z.v_vl0) & 0x33333333; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << 2);
				z.v_tm0 = ((z.v_vl0 >>> 16) ^ z.v_vr0) & 0x0000ffff; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 16);
				z.v_tm0 = ((z.v_vl0 >>> 4) ^ z.v_vr0) & 0x0f0f0f0f; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 4);
				//
				//for Cipher Block Chaining mode, xor the message with the previous r esult
				if(md == 1) 
				{
					if(en) 
					{
						z.v_cl0 = z.v_vl0; 
						z.v_cr0 = z.v_vr0;
					} 
					else 
					{
						z.v_vl0 ^= z.v_cl2; 
						z.v_vr0 ^= z.v_cr2;
					}
				}
				//
				z.v_tr0 += String.fromCharCode((z.v_vl0>>>24),((z.v_vl0>>>16) & 0xff),((z.v_vl0>>>8) & 0xff),(z.v_vl0 & 0xff),(z.v_vr0>>>24),((z.v_vr0>>>16) & 0xff),((z.v_vr0>>>8) & 0xff),(z.v_vr0 & 0xff));
				//
				z.v_ch0 += 8;
				if(z.v_ch0 == 512) 
				{
					z.v_re0 += z.v_tr0; 
					z.v_tr0 = ""; 
					z.v_ch0 = 0;
				}
			} //for every 8 characters, or 64 bits in the message
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r =  z.v_re0 + z.v_tr0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c3._des",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c3._des",le:ALiBS.gv.c3.le});
		//
		return r; 
	},
	// *****************************************************************
	_dck: function(k0,p) // create keys // 64 bit key (even though only 56 bits are used) //as an array of 2 integers, and returns 16 48 bit keys
	/*
		Confidential Info - do NOT disclose

		Description: Auxiliar - Not used directly

		Parameters:

			t0 = (required)(ALiBS.to) table

			p = (optional)(JSON)parameter {}

		Observations:

		Return Value:

		Example(s):

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c3._dck",le:ALiBS.gv.c3.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(k0)!="string") throw "ALiBS.c3._des.Exception: Error: Invalid Parameter Type: (string) k0 passed as "+typeof(k0); // 
			//
			ALiBS.sy.la(5+0,"k0="+k0,{o:"ALiBS.c3._dck",le:ALiBS.gv.c3.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			//how many it erations (1 for des, 3 for triple des)
			z.v_it0 = k0.length > 8 ? 3 : 1; // it erations // changed by Paul 16/6/2007 to use Triple DES for 9+ byte keys
//console.log("k0.length="+k0.length);
//console.log("k0=["+k0+"] - z.v_it0="+z.v_it0);
			//
			//stores the return keys
			z.v_ks = new Array (32 * z.v_it0);
			//now define the [l]eft [s]hifts which need to be done
			//
			z.v_sh0 = new Array (0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0); // [s]hifts
			//other variables
			z.v_mm = 0; // m 
			z.v_nn = 0; // n
			z.v_vl0; // [l]eft;
			z.v_tm0; // [t]emp
			z.v_lt0; // [l]eft[t]emp 
			z.v_rt0; // [r]ight[t]emp 
			//
			// ************************
			//
			z.ec = -2000;
			for(var j=0; j<z.v_it0; j++) //either 1 or 3 [i]terations
			{ 
				z.v_vl0 = (k0.charCodeAt(z.v_mm++) << 24) | (k0.charCodeAt(z.v_mm++) << 16) | (k0.charCodeAt(z.v_mm++) << 8) | k0.charCodeAt(z.v_mm++);
				z.v_vr0 = (k0.charCodeAt(z.v_mm++) << 24) | (k0.charCodeAt(z.v_mm++) << 16) | (k0.charCodeAt(z.v_mm++) << 8) | k0.charCodeAt(z.v_mm++);
				//
				z.v_tm0 = ((z.v_vl0 >>> 4) ^ z.v_vr0) & 0x0f0f0f0f; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 4);
				z.v_tm0 = ((z.v_vr0 >>> -16) ^ z.v_vl0) & 0x0000ffff; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << -16);
				z.v_tm0 = ((z.v_vl0 >>> 2) ^ z.v_vr0) & 0x33333333; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 2);
				z.v_tm0 = ((z.v_vr0 >>> -16) ^ z.v_vl0) & 0x0000ffff; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << -16);
				z.v_tm0 = ((z.v_vl0 >>> 1) ^ z.v_vr0) & 0x55555555; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 1);
				z.v_tm0 = ((z.v_vr0 >>> 8) ^ z.v_vl0) & 0x00ff00ff; 
				z.v_vl0 ^= z.v_tm0; 
				z.v_vr0 ^= (z.v_tm0 << 8);
				z.v_tm0 = ((z.v_vl0 >>> 1) ^ z.v_vr0) & 0x55555555; 
				z.v_vr0 ^= z.v_tm0; 
				z.v_vl0 ^= (z.v_tm0 << 1);
				//
				z.ec = -3000;
				//the right side needs to be shifted and to get the last four bits of the [l]eft side
				z.v_tm0 = (z.v_vl0 << 8) | ((z.v_vr0 >>> 20) & 0x000000f0);
				//[l]eft needs to be put upside down
				z.v_vl0 = (z.v_vr0 << 24) | ((z.v_vr0 << 8) & 0xff0000) | ((z.v_vr0 >>> 8) & 0xff00) | ((z.v_vr0 >>> 24) & 0xf0);
				z.v_vr0 = z.v_tm0;
				//
				z.ec = -3100;
				//now go through and perform these [s]hifts on the [l]eft and [r]ight keys
				for(var i=0; i < z.v_sh0.length; i++) 
				{
					//shift the keys either one or two bits to the [l]eft
					if(z.v_sh0[i]) 
					{
						z.v_vl0 = (z.v_vl0 << 2) | (z.v_vl0 >>> 26); 
						z.v_vr0 = (z.v_vr0 << 2) | (z.v_vr0 >>> 26);
					}
					else 
					{
						z.v_vl0 = (z.v_vl0 << 1) | (z.v_vl0 >>> 27); 
						z.v_vr0 = (z.v_vr0 << 1) | (z.v_vr0 >>> 27);
					}
					//
					z.v_vl0 &= -0xf; 
					z.v_vr0 &= -0xf;
					//
					//now apply PC-2, in such a way that E is easier when encrypting or decrypting
					//this conversion will look like PC-2 except only the last 6 bits of each byte are used
					//rather than 48 consecutive bits and the order of lines will be according to 
					//how the S selection functions will be applied: S2, S4, S6, S8, S1, S3, S5, S7
					z.ec = -3200;
					z.v_lt0 = ALiBS.c3._o1.a0[z.v_vl0 >>> 28] | ALiBS.c3._o1.a1[(z.v_vl0 >>> 24) & 0xf] | ALiBS.c3._o1.a2[(z.v_vl0 >>> 20) & 0xf] | ALiBS.c3._o1.a3[(z.v_vl0 >>> 16) & 0xf] | ALiBS.c3._o1.a4[(z.v_vl0 >>> 12) & 0xf] | ALiBS.c3._o1.a5[(z.v_vl0 >>> 8) & 0xf] | ALiBS.c3._o1.a6[(z.v_vl0 >>> 4) & 0xf];
					//
					z.ec = -3300;
					z.v_rt0 = ALiBS.c3._o1.a7[z.v_vr0 >>> 28] | ALiBS.c3._o1.a8[(z.v_vr0 >>> 24) & 0xf] | ALiBS.c3._o1.a9[(z.v_vr0 >>> 20) & 0xf] | ALiBS.c3._o1.a10[(z.v_vr0 >>> 16) & 0xf] | ALiBS.c3._o1.a11[(z.v_vr0 >>> 12) & 0xf] | ALiBS.c3._o1.a12[(z.v_vr0 >>> 8) & 0xf] | ALiBS.c3._o1.a13[(z.v_vr0 >>> 4) & 0xf];
					//
					z.ec = -3400;
					z.v_tm0 = ((z.v_rt0 >>> 16) ^ z.v_lt0) & 0x0000ffff; 
					z.v_ks[z.v_nn++] = z.v_lt0 ^ z.v_tm0; 
					z.v_ks[z.v_nn++] = z.v_rt0 ^ (z.v_tm0 << 16);
				}

			} //for each [i]terations
//console.log(keys);
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_ks;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c3._dck",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c3._dck",le:ALiBS.gv.c3.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c3._dck",le:ALiBS.gv.c3.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c3("+(++ALiBS.gv.c3.nn)+"x)",{o:"ALiBS.c3"});
ALiBS.sy.la(0+0,"ALiBS.gv.c3.rl="+ALiBS.gv.c3.rl,{o:"ALiBS.c3"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.c3.le="+ALiBS.gv.c3.le+ALiBS.gv.sy.se,{o:"ALiBS.c3"});

