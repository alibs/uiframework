/* *************************************************************************
// INFO:

----------------------------------------------------------------------------

Step 1:

<script type="text/javascript" charset="utf-8" src="/nps/portal/modules/custom/skins/default/devices/default/ALiBS/ALiBS-c/ALiBS.sy-c.js"></script>
<script type="text/javascript" charset="utf-8" src="/nps/portal/modules/custom/skins/default/devices/default/ALiBS/ALiBS-c/ALiBS.hp-c.js"></script>

----------------------------------------------------------------------------

Step 2:

<div id="iALiBS_page0"></div>

----------------------------------------------------------------------------

Step 3: (if desired)

ALiBS.gv.lg.img0 = ALiBS.gv.sy.sp + "lg01.gif";

- Copy diamond img to path;

----------------------------------------------------------------------------

// *************************************************************************
// TODO(2011-09-26): 

TODO(2011-09-26): 

application

- Padrao de IDs
	- no HTML (iALiBS_page0)
	- na classe (i_hpp0_div)
	- adicionar "nome" do componente no ID para poder criar varios em uma mesma pagina (ht.js)

- Teclas atalho
	- ALiBS.gv.sy.sq[ALiBS.gv.lg.sq+0] = 

- Multi-languages - i18n - internacionalization

- Multi-skins
	- ALiBS.gv.lg.img0 = ALiBS.gv.sy.sp + "lg01.gif";
	- ALiBS.gv.lg.img1 = ALiBS.gv.sy.sp + "ALiBS-t-pwby-b0.png";

- Multi-components on same page


TODO(2011-09-26): 

- Definir padrao de nome dos arquivos de imagens
- Definir padrao de nome dos arquivos css

*/
//
// *************************************************************************
//
if(!ALiBS.gv.hp) ALiBS.gv.hp = {}; // global values
//
if(!ALiBS.gv.hp.le) ALiBS.gv.hp.le = 1; // local log status
//
ALiBS.gv.hp.nn = 0; // x times loaded
//
ALiBS.gv.hp.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.hp.img0 = ALiBS.gv.sy.sp + "lg01.gif";
//
ALiBS.gv.hp.img1 = ALiBS.gv.sy.sp + "ALiBS-t-pwby-b0.png";
//
// *************************************************************************
//
ALiBS.hp = // HTML page - Main Application Page
{
	p0: function(p) // html page 0 
	// p = (optional)(JSON)parameters {} 
	//	xx = (String)????
	// TODO: id deve ter string custom no final para diferenciar os componentes, caso tenha 2+ componentes na mesma página
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hp.p0",le:ALiBS.gv.hp.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.pgwd = 1100; // width
		z.pghg = 1100; // height
		//
		// DEFAULTS [BEST-PRACTICE]
		//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
		//
		// local varibles [BEST-PRACTICE]
		z.a = new Array();
		//
		z.a[z.a.length] = "<div id='iALiBS_log0'></div>";
		//
		z.a[z.a.length] = "<div class='c_hpp0_div_pg' id='i_hpp0_div_pg'>";
		//
		//z.a[z.a.length] = "<div class='c_hpp0_div_pg' id='i_hpp0_div_pg' ";
		//
		//z.a[z.a.length] = 'width="'+z.pgwd+'px" height="'+z.pghg+'px">';
		//
		// Log
		//
		//z.a[z.a.length] = "<div id='iALiBS_log0'></div>";
		//
		// Header
		//
		z.a[z.a.length] = "<div class='c_hpp0_div_h0' id='i_hpp0_div_h0'>Header 0</div>";
		z.a[z.a.length] = "<div class='c_hpp0_div_h1' id='i_hpp0_div_h1'>Header 1</div>";
		z.a[z.a.length] = "<div class='c_hpp0_div_h2' id='i_hpp0_div_h2'>Header 2</div>";
		z.a[z.a.length] = "<div class='c_hpp0_div_h3' id='i_hpp0_div_h3'>Header 3</div>";
		//
		// Menu
		//
		z.a[z.a.length] = "<div class='c_hpp0_div_m0' id='i_hpp0_div_m0'>Menu 0<br/><br/>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_m0a' id='i_hpp0_div_m0a'>Menu 0a<br/><br/></div>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_m0b' id='i_hpp0_div_m0b'>Menu 0b<br/><br/></div>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_m0c' id='i_hpp0_div_m0c'>Menu 0c<br/><br/></div>";
		z.a[z.a.length] = "</div>";
		//
		// Content 0
		//
		z.a[z.a.length] = "<div class='c_hpp0_div_c0' id='i_hpp0_div_c0'>Content 0";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_c0a' id='i_hpp0_div_c0a'>Content 0a<br/></div>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_c0b' id='i_hpp0_div_c0b'>Content 0b<br/></div>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_c0c' id='i_hpp0_div_c0c'>Content 0c<br/></div>";
		z.a[z.a.length] = "</div>";
		//
		// Content1
		//
		z.a[z.a.length] = "<div class='c_hpp0_div_c1' id='i_hpp0_div_c1'>Content 1<br/><br/>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_c1a' id='i_hpp0_div_c1a'>Content 1a<br/><br/></div>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_c1b' id='i_hpp0_div_c1b'>Content 1b<br/><br/></div>";
		z.a[z.a.length] = 	"<div class='c_hpp0_div_c1c' id='i_hpp0_div_c1c'>Content 1c<br/><br/></div>";
		z.a[z.a.length] = "</div>";
		//
		// Footer
		//
		z.a[z.a.length] = "<div class='c_hpp0_div_f0' id='i_hpp0_div_f0'>Footer 0</div>";
		z.a[z.a.length] = "<div class='c_hpp0_div_f1' id='i_hpp0_div_f1'>Footer 1</div>";
		z.a[z.a.length] = "<div class='c_hpp0_div_f2' id='i_hpp0_div_f2'>Footer 2</div>";
		z.a[z.a.length] = "<div class='c_hpp0_div_f3' id='i_hpp0_div_f3'>Footer 3</div>";
		//
		z.a[z.a.length] = "</div>";
		//
		var r = z.a.join(""); // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.hp.p0",le:ALiBS.gv.hp.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	_si: false, // false=show(default), true=hide
	// *****************************************************************
	si: function() // show log img
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hp.si",le:ALiBS.gv.hp.le});
		//
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	t0: function(t0,p) // html tab 0
	// t0 = (required)(ALiBS.to)table with tab information
	// p = (optional)(JSON)parameters {} 
	//	ci = (string)custom id (to have 2+ tabs in the same table)
	//		"" = DEFAULT
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hp.t0",le:ALiBS.gv.hp.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		//z.pgwd = 1100; // width
		//z.pghg = 1100; // height
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.ci)!="string") z.ci = ""; // DEFAULT
		//
		// local varibles [BEST-PRACTICE]
		z.a = new Array();
		//
		// table variables [BEST-PRACTICE]
		T0.tb.f00=T0.tb.id+"_tabSet";
		//
		z.a[z.a.length] = '<div id=i_hpt0_div_ts' + t0.tb.id;
		z.a[z.a.length] = ' style="display:block;visibility:visible">'; // tab set
		//
		// ************************
		//
		z.a[z.a.length] = '<div id="i_hpt0_div_th' + t0.tb.id + '">'; // tab opt
		//
		for(var i=0;i<t0.rs.length;i++)
		{
			if(t0.rs[i].f00!="opt") continue;
			//
			z.a[z.a.length] = '<div id="i_hpt0_div_th_' + t0.rs[i].f01 + t0.tb.id + '"';
			z.a[z.a.length] = ' onclick="ALiBS.st.ts(';
			z.a[z.a.length] = "'" + T0.tb.f00 + "','i_hpt0_div_wd_" + t0.rs[i].f01 + t0.tb.id + "'" + ');"';
			z.a[z.a.length] = '">';
			z.a[z.a.length] = '<a id="i_hpt0_a_th_' + t0.rs[i].f01 + t0.tb.id + '">' + t0.rs[i].f02 + '</a>';
			z.a[z.a.length] = '</div>';
		}
		//
		z.a[z.a.length] = '</div>'; // tab opt
		//
		// ************************
		//
		z.a[z.a.length] = '<div id="i_hpt0_div_wd' + t0.tb.id + '">'; // tab window
		//
		for(var i=0;i<t0.rs.length;i++)
		{
			if(t0.rs[i].f00!="wdo") continue;
			//
			//
			if(t0.rs[i].f03=="1")
			{
				z.a[z.a.length] = '<div id="i_hpt0_div_wd_' + t0.rs[i].f01 + t0.tb.id + '" style="display:block;visibility:visible">'; // tab window
			}
			else
			{
				z.a[z.a.length] = '<div id="i_hpt0_div_wd_' + t0.rs[i].f01 + t0.tb.id + '" style="display:none;visibility:hidden">'; // tab window
			}
			//
			z.a[z.a.length] = '<div>' + t0.rs[i].f02 + '</div>';
			//
			z.a[z.a.length] = '</div>'; // tab window
		}
		//
		z.a[z.a.length] = '</div>'; // tab window
		//
		// ************************
		//


/*
		<div>
			TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 
			TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 
			TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 
			TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 TabArea0 
			<div><textarea id="ta01" name="ta01" cols=80 rows=7></textarea></div>
		</div>
*/









		z.a[z.a.length] = '</div>'; // tab set
		//
		r = z.a.join("");
		//
		return r;
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.hp("+(++ALiBS.gv.hp.nn)+"x)",{o:"ALiBS.hp"});
ALiBS.sy.la(0+0,"ALiBS.gv.hp.le="+ALiBS.gv.hp.le+ALiBS.gv.sy.se,{o:"ALiBS.hp"});



