/* *************************************************************************
// INFO:

----------------------------------------------------------------------------

Step 1:

TODO: REVER

ALiBS.el.si("iALiBS_f0",ALiBS.hf.f0("iALiBS_k0",{ty:1,oc:"val",ok:"sub"}));


----------------------------------------------------------------------------

Step 2:

TODO: REVER

<div id=iALiBS_teclado0>xxxx</div>

----------------------------------------------------------------------------

// *************************************************************************
// TODO

*/
//
// *************************************************************************
//
if(!ALiBS.gv.hf) ALiBS.gv.hf = {}; // global values
//
if(!ALiBS.gv.hf.le) ALiBS.gv.hf.le = 0; // local log status
//
ALiBS.gv.hf.nn = 0; // x times loaded
//
ALiBS.gv.hf.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.hf.imgy0 = ALiBS.gv.sy.sp + "f0y1.png"; // Yes/Valid image 13x13
//
//ALiBS.gv.hf.imge0 = ALiBS.gv.sy.sp + "f0e1.png"; // Error image 13x13
ALiBS.gv.hf.imge0 = ALiBS.gv.sy.sp + "/ALiBS/hg.g0.imgx1.png"; // Error image 13x13
//
ALiBS.gv.hf.imgb0 = ALiBS.gv.sy.sp + "blank13x13.png"; // Blank image 13x13
//
// *************************************************************************
//
ALiBS.gv.hf.ci = {}; // Component Unique IDs cache
//
ALiBS.gv.hf.fn = {}; // Forms Unique IDs cache
//
// *************************************************************************
//
if(typeof(ALiBS.it)!="undefined")
{
	ALiBS.gv.it.fcl["ALiBS.hf.fcl"] = function(p) 
	/** 
	Method <static> ALiBS.hf.fcl(p)

		Description: Updates language, messages and ENABLE/DISABLE button

		Parameters:
			p = (optional)(JSON)parameters {} 
				u = (number)update
					"*" = ALL = Default = "*" = Updates language AND/OR messages of ALL components
					1 = Messages = Updates language AND/OR messages of Components with Valid/Error Messages

				fo = (String)component 
					"*" = ALL = Default = "*"
					"hff0" = Updates hff0
					"hff1" = Updates hff1

		Return Value:
			None

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.fcl",le:ALiBS.gv.hf.le});
		//
		var u = "*"; // Default = 0 = Updates language AND/OR messages of ALL components
		var fo = "*"; // Default = "*" = Updates hff0, hff1, hff2, etc
		//
		if(p)
		{
			if(typeof(p.u)=="number") u = p.u;
			//
			//alert("(hf)p.u="+p.u+"\n");
		}
		//
		var v0;
		var e0;
		var vd; // valid state
		var bi; // button ID
		//
		// Updates button OK
		//
		for(var f in ALiBS.gv.hf.fn)
		{
			v0 = 0; // reset
			bi = ""; // reset
			//
			for(var i in ALiBS.gv.hf.ci)
			{
				//
				if(ALiBS.gv.hf.ci[i].bi!="" && ALiBS.gv.hf.ci[i].bi.indexOf(f)>0) // bi!="" means this component has a button to ENABLE/DISABLE
				{
					vd = ALiBS.gv.hf.ci[i].vd;
					bi = ALiBS.gv.hf.ci[i].bi;
					//
					if(vd == -1)
					{
						v0 = -1;		
						ALiBS.st.st(bi,"disabled"); // basta um componente não valido
						break;				
					}
				}
			}
			//
			if(v0==0) ALiBS.st.st(bi,"enabled");
		}
		//
		// Updates hff0
		//
		if(fo=="*" || fo=="hff0")
		{
			if(u=="*" || u==1) // updates language AND/OR messages of ALL components OR Updates language AND/OR messages of Components with Valid/Error Messages
			{
				for(var i in ALiBS.gv.hf.ci)
				{
					vd = ALiBS.gv.hf.ci[i].vd;
					//
					e0 = ALiBS.el.ge("i_hff0_img0_"+i); // <img> title
					if(e0)
					{
						if(vd==0)
						{
							v0 = ALiBS.gv.it.cla["i_hff0_img0_"+i+"_i_title"];
							if(v0) e0.title = v0;
						}
						else if(vd==-1)
						{
							v0 = ALiBS.gv.it.cla["i_hff0_img0_"+i+"_e_title"];
							if(v0) e0.title = v0;
							//
							e0.src = ALiBS.gv.hf.imge0; // Error img
						}
						else if(vd==1)
						{
							v0 = ALiBS.gv.it.cla["i_hff0_img0_"+i+"_v_title"];
							if(v0) e0.title = v0;
							//
							e0.src = ALiBS.gv.hf.imgy0; // OK img
						}
					}
					//
					e0 = ALiBS.el.ge("i_hff0_a0_"+i); // <a> innerHTML
					if(e0)
					{
						if(vd==0)
						{
							v0 = ALiBS.gv.it.cla["i_hff0_a0_"+i+"_i_innerHTML"];
							if(v0) e0.innerHTML = v0;
						}
						else if(vd==-1)
						{
							v0 = ALiBS.gv.it.cla["i_hff0_a0_"+i+"_e_innerHTML"];
							if(v0) e0.innerHTML = v0;
						}
						else if(vd==1)
						{
							v0 = ALiBS.gv.it.cla["i_hff0_a0_"+i+"_v_innerHTML"];
							if(v0) e0.innerHTML = v0;
						}
					}
				}
			}
			//
			if(u=="*") // update ONLY languages of Components WITHOUT Valid/Error Messages
			{
				for(var i in ALiBS.gv.hf.ci)
				{
					vd = ALiBS.gv.hf.ci[i].vd;
					//
					e0 = ALiBS.el.ge("i_hff0_input0_"+i); // <input> title
					if(e0)
					{
						v0 = ALiBS.gv.it.cla["i_hff0_input0_"+i+"_title"];
						if(v0) e0.title = v0;
					}
					//
					e0 = ALiBS.el.ge("i_hff0_select0_"+i); // <select> title
					if(e0)
					{
						v0 = ALiBS.gv.it.cla["i_hff0_select0_"+i+"_title"];
						if(v0) e0.title = v0;
					}
					//
					e0 = ALiBS.el.ge("i_hff0_button0_"+i); // <input button> value + title
					if(e0)
					{
						v0 = ALiBS.gv.it.cla["i_hff0_button0_"+i+"_value"];
						if(v0) e0.value = v0;
						//
						v0 = ALiBS.gv.it.cla["i_hff0_button0_"+i+"_title"];
						if(v0) e0.title = v0;
					}
				}
			}
		}
		//
		// Updates hff1
		//
		if(fo=="*" || fo=="hff1")
		{
			//alert("Dentro de hff1");
		}
	};
}
//
// *************************************************************************
//
ALiBS.hf = // HTML Forms
{
	oc: function(p) // onchange
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.oc",le:ALiBS.gv.hf.le});
		//
		//alert('vazio');
		// should be empty
	},
	// *****************************************************************
	ok: function(p) // onclick
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.ok",le:ALiBS.gv.hf.le});
		//
		//alert('vazio');
		// should be empty
	},
	// *****************************************************************
	f0: function(c,n,p) // HTML component
	/** 
	Method <static> ALiBS.hf.f0(c,n,p)

		Description: Log Messages 

		Parameters:
			c = (required)(number)component type 
				[1]input text
				[2]input password
				[3]select
				[4]checkbox
				[5]radio
				[6]input button

			n = (required)(String)component unique ID (input0,select0)

			p = (optional)(JSON)parameters {} 
				oc = (String)onchange function name (func)

				ok = (String)onclick function name (func)

				im = (number)image
					0 = ????
					1 = Show image imge0 = Error = Default
					2 = Show image imgy0 = Yes/Valid 
					3 = Show image imgb0 = Blank 
					4 = Show image imgt0 = Transparent TODO(2012-03-28):Definir qual será o arquivo (gif,png,etc)

				swi = (number)show image - Default = 3
					0 = Do not show image
					1 = Show image at top
					2 = Show image at left
					3 = Show image at right

				swm = (number)show message - Default = 3
					0 = Do not show message
					1 = Show message at top
					2 = Show message at left
					3 = Show message at right

				bi: (String)button ID to ENABLE/DISABLE (form0ok0)
					"" = Default = No button to disable

				ssz = (number)select size - Default = 1

				stw = (number)style:width - Default = 'style="width:310px"'
					0 = Do Not use 

				mxl = (number)maxlength - Default = 50
					0 = Do Not use 

				dv0 = (number)div before - Default = 1
					0 = Do Not use 
					1 = Use <div>

				dv1 = (number)div after - Default = 1
					0 = Do Not use 
					1 = Use <div>

				dsb = (number)disabled - Default = 1 = Disabled
					0 = Enabled

				vd = (number)validation required
					0 = validation not required - Default = 0
					1 = valid
					-1 = invalid (force validation)
		Return Value:
			(String) HTML Component

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.f0",le:ALiBS.gv.hf.le});
		//
		var a = new Array();
		//
		var vd = 0; // validation not required // Default = 0
		//
		var bi = ""; // Default = "" = No button to disable
		//
		if(p)
		{
			if(typeof(p.vd)=="number") vd = p.vd;
			//
			if(typeof(p.bi)=="string" && p.bi!="") bi = "i_hff0_button0_"+p.bi;
		}
		//
		//alert("(hf)n="+n+"\np.bi="+p.bi+"\nbi="+bi);
		//
		ALiBS.gv.hf.ci[n] = // component unique IDs cache
		{
			vd:vd, // -1=invalid, 0=initial, 1=valid
			bi:bi 
		};
		//
		var f = n.indexOf(".");
		f = n.substring(0,f); 
		//
		ALiBS.gv.hf.fn[f] = // Form Name
		{
			a:true
		};
		//
		if(c==6) // input button
		{
			var ok = "ALiBS.hf.ok()"; // onclick
			var dv0 = 1; // Default = 1
			var dv1 = 1; // Default = 1
			var dsb = 'disabled'; // Default = 1 = Disabled
			//
			if(p)
			{
				if(typeof(p.ok)=="string") ok = p.ok+"()";
				//
				if(typeof(p.dv0)=="number") dv0 = p.dv0;
				//
				if(typeof(p.dv1)=="number") dv1 = p.dv1;
				//
				if(typeof(p.dsb)=="number" && p.dsb==0) dsb = ''; // enabled
			}
			//
			if(dv0==1) a[a.length] = '<div>'; 
			//
			a[a.length] = '<input type="button" '+dsb+' id="'+'i_hff0_button0_'+n+'" ';

			a[a.length] = 'value="OK" title="OK" onclick="'+ok+'" />'; 
			//
			if(dv1==1) a[a.length] = '</div>'; 
		}
		else
		{
			var oc = "ALiBS.hf.oc()"; // onchange
			var swi = 3; // Default = at right
			var swm = 3; // Default = at right
			var im = ALiBS.gv.hf.imge0; // Default = Error
			var ssz = 1; // Default = 1
			var stw = 'style="width:310px"'; // Default = 'style="width:310px"'
			var mxl = 'maxlength="50"'; // Default = 'maxlength="50"'
			//
			if(p)
			{
				if(typeof(p.oc)=="string") oc = p.oc+"('"+n+"',this.value)";
				//
				if(typeof(p.swi)=="number") swi = p.swi;
				//
				if(typeof(p.swm)=="number") swm = p.swm;
				//
				if(typeof(p.im)=="number")
				{
					if(p.im==2)
					{
						im = ALiBS.gv.hf.imgy0; // Yes/Valid
					}
					else if(p.im==3)
					{
						im = ALiBS.gv.hf.imgb0; // Blank
					}
				}
				//
				if(typeof(p.ssz)=="number") ssz = p.ssz;
				//
				if(typeof(p.stw)=="number")
				{
					if(p.stw==0) stw = ''; // Do Not use 
					else stw = 'style="width:'+p.stw+'px"';
				}
				//
				if(typeof(p.mxl)=="number")
				{
					if(p.mxl==0) mxl = ''; // Do Not use 
					else mxl = 'maxlength="'+p.mxl+'"';
				}
			}
			//
			if(swi==0) // Do not show image
			{
				if(swm==1) // message at top
				{
					a[a.length] = '<div>'; 
					//
					a[a.length] = '<a id="'+'i_hff0_a0_'+n+'">Fill in the blank</a>';
					//
					a[a.length] = '</div>	'; // DO NOT REMOVE FINAL TAB
				}
			}
			else if(swi==1) // image at top
			{
				a[a.length] = '<div>';
				//
				a[a.length] = '<img src="'+im+'" id="'+'i_hff0_img0_'+n+'" title="Error"></img>	'; // DO NOT REMOVE FINAL TAB
				//
				if(swm==1) // message at top
				{
					a[a.length] = '<a id="'+'i_hff0_a0_'+n+'">Fill in the blank</a>';
				}
				//
				a[a.length] = '</div>	'; // DO NOT REMOVE FINAL TAB
			}
			//
			a[a.length] = '<div>';
			//a[a.length] = '<div id="'+'i_hff0_div0_'+n+'">';
			//
			if(swi==2) // image at left
			{
				a[a.length] = '<img src="'+im+'" id="'+'i_hff0_img0_'+n+'" title="Error"></img>	'; // DO NOT REMOVE FINAL TAB
			}
			//
			if(swm==2) // message at left
			{
				a[a.length] = '<a id="'+'i_hff0_a0_'+n+'">Fill in the blank</a>';
			}
			//
			// ***************************
			//
			if(c==1 || c==2)
			{
				if(c==1) a[a.length] = '<input type="text" id="'+'i_hff0_input0_'+n+'" ';
				else if(c==2) a[a.length] = '<input type="password" id="'+'i_hff0_input0_'+n+'" ';
				//
				a[a.length] = mxl+' '+stw+' title="Fill in the blank" onchange="'+oc+'" />	'; // DO NOT REMOVE FINAL TAB
			}
			//
			if(c==3)
			{
				a[a.length] = '<select id="'+'i_hff0_select0_'+n+'" size="'+ssz+'" title="Select" '+stw+' onchange="'+oc+'"></select>	'; // DO NOT REMOVE FINAL TAB';
			}
			// ***************************
			//
			if(swi==3) // image at rigth
			{
				a[a.length] = '<img src="'+im+'" id="'+'i_hff0_img0_'+n+'" title="Error"></img>	'; // DO NOT REMOVE FINAL TAB
			}
			//
			if(swm==3) // message at rigth
			{
				a[a.length] = '<a id="'+'i_hff0_a0_'+n+'">Fill in the blank</a>';
			}
			//
			a[a.length] = '</div>';
			//
		}
		//
		return a.join("");
	},
	// *****************************************************************
	f1: function(n,p) // 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.f1",le:ALiBS.gv.hf.le});
		//
	},
	// *****************************************************************
	ao: function(s,p) // add select/combo options
	// s = (required)(String)option text
	// p = (optional)(JSON)parameters {s:true} 
	//	s = (optional)(boolean)selected
	//	v = (String)option
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.ao",le:ALiBS.gv.hf.le});
		//
		var a = "";
		var v = s;
		//
		if(p)
		{
			if(typeof(p.s)!="undefined" && p.s==true) a = " selected ";
			//
			if(typeof(p.v)!="undefined") v = p.v;
		}
		//
		return "<option "+a+" value="+v+">"+s+"</option>";
	},
	// *****************************************************************
	aod: function(p) // add date options 
	/** 
	Method <static> ALiBS.hf.aod(p)

		Description: Add Date Options 

		Parameters:
			p = (optional)(JSON)parameters {} 

				d = (Date)Date to start
					"" = Default = new Date() = Today

				n = (number)days to show = number of options

				sp = (String)separator
					"" = Default = "-"
					"/" = "/"
					"." = "."

				fm = (number)output date format
					"" = Default = "yyyy-mm-dd"
					"dmy" = "dd-mm-yyyy"
					"dMy" = "dd-Jan-yyyy" TODO

			TODO(2012-04-02): 
				- Opção para nao considerar sabado, domingo e feriados

		Return Value:
			(String) HTML Date <option> 

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.aod",le:ALiBS.gv.hf.le});
		//
		var opts = new Array();
		//
		var d;	
		var n = 30; // number of days = options = Default = 30 days
		var fm = ""; // Default = "yyyy-mm-dd"
		var sp = "-"; // Default = "-"
		//
		if(p)
		{
			if(typeof(p.d)=="obj"+"ect") d = p.d;
			else d = new Date(); // Default = Today
			//
			if(typeof(p.n)=="number") n = p.n;
			//
			if(typeof(p.fm)=="string") fm = p.fm;
			//
			if(typeof(p.sp)=="string") sp = p.sp;
		}
		//
		var j = ALiBS.dt.gj({d:d,s:true});
		var d0 = "";
		//
		opts[opts.length] = ALiBS.hf.ao("Selecionar",{s:true});
		//
		var dh = j.d; // day of month
		var j2;
		//
		for(var i=0;i<n;i++) 
		{

			j2 = ALiBS.dt.sd2(i,{d:d});
			//
			if(j2.d<10) d0 = "0"+j2.d; 
			else d0 = j2.d;
			//
			if(fm=="") // Text = yyyy mm dd
			{
				opts[opts.length] = ALiBS.hf.ao(j2.y+sp+j2.sm+sp+d0,{v:j2.y+"-"+j2.sm+"-"+d0}); // value/separator always yyyy-mm-dd 
			}
			else if(fm=="dmy") // Text = dd mm yyyy
			{
				opts[opts.length] = ALiBS.hf.ao(d0+sp+j2.sm+sp+j2.y,{v:j2.y+"-"+j2.sm+"-"+d0}); // value/separator always yyyy-mm-dd
			}
		}
		//
		return opts.join("");
	},
	// *****************************************************************
	ck: function(e,p)
	/** 
	Method <static> ALiBS.hf.ck(p)

		Description: Get Count, Get Checked(s) Options, Run Actions

		Parameters:
			e = (required)(String)element ID 
				without counter number = if radio or checkbox
				complete ID = if select

			p = (optional)(JSON)parameters {} 

				mn = (number)initial count
					0 = Default

				mx = (number)max count
					10 = Default

				a = (number)action
					0 = [checkbox,radio,SINGLE-select,MULTI-select] Get Count and Get Checked(s)= Default
					1 = [checkbox,MULTI-select] Switch All
					2 = [checkbox,MULTI-select] Check All
					3 = [checkbox,MULTI-select] Uncheck All
					-

		Return Value:
			(JSON) result

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hf.ck",le:ALiBS.gv.hf.le});
		//
		var r = 
		{
			tc:0, // [MULTI] = total checked (for checkbox and select MULTI)
			tu:0, // [MULTI] = total unchecked (for checkbox and select MULTI)
			id:null, // [SINGLE] = ID of option checked (for radio and select SINGLE)
			vl:null, // [SINGLE] = value of option checked (for radio and select SINGLE)
			ix:null //  [SINGLE] = index of option checked (for select)
		};
		//

			/* dynamic added values [SINGLE] and [MULTI]
			"ID": // options 
			{
				ck:true, // radio,checkbox,select checked
				vl:null, // radio,checkbox,select value
				tx:null, // select text
				ix:null // selected index
			}
			*/
		//
		var mn = 0; // Default
		var mx = 10; // Default
		var a = 0; // Default = Get Count and Get Checked(s)
		//
		var ob = null;
		//
		if(p)
		{
			if(typeof(p.mn)=="number") mn = p.mn;
			//
			if(typeof(p.mx)=="number") mx = p.mx;
			//
			if(typeof(p.a)=="number") a = p.a;
		}
		//
		var vl;
		var tx;
		var le;
		var ac = new Array(); // values checked
		var au = new Array(); // values unchecked
		//
		if(ALiBS.el.ge(e)==null) // checkbox or radio = ID is missing counter
		{
			for(var j=mn;j<=mx;j++) 
			{
				ob = ALiBS.el.ge(e+j);
				//
				if(ob)
				{
					if(a==1) ob.checked = !ob.checked; // Switch All
					else if(a==2) ob.checked = true; // Check All
					else if(a==3) ob.checked = false; // Uncheck All
					//
					if(ob.checked==true)
					{
						r.tc++;
						//
						r[e+j] = // ID
						{
							ck:true,
							vl:ob.value
						};
						//
						ac[ac.length] = ob.value;
						//
						r.id=e+j;
						r.vl=ob.value;
					}
					else
					{
						r.tu++;
						//
						r[e+j] = // ID
						{
							ck:false,
							vl:ob.value
						};
						//
						au[au.length] = ob.value;
					}
				}
			}
		}
		else // select = "e" exists and ID is complete
		{
			//if() //  SINGLE
			// else // MULTI
			var ix = ALiBS.el.ge(e).selectedIndex;
			var le = ALiBS.el.ge(e).length;
			//
			for(var j=0;j<le;j++) 
			{
				vl = ALiBS.el.ge(e).options[j].value;
				tx = ALiBS.el.ge(e).options[j].text;
				//
				if(ix>=0 && j==ix)
				{
					r.tc++;
					//
					r[j] = // ix = index
					{
						ck:true, // selected
						ix:j, // selected index
						vl:vl, // value
						tx:tx // text
					};
					//
					ac[ac.length] = vl;
					//
					r.ix=ix;
					r.vl=vl;
					r.tx=tx;
				}
				else
				{
					r.tu++;
					//
					r[j] = // ix = index
					{
						ck:false, // selected
						ix:j, // index
						vl:vl, // value
						tx:tx // text
					};
					//
					au[au.length] = vl;
				}
			}
			//
		}
		//
		//
		r.ac = ac.join(",");
		r.au = au.join(",");
		//
		return r; // JSON
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.hf("+(++ALiBS.gv.hf.nn)+"x)",{o:"ALiBS.hf"});
ALiBS.sy.la(0+0,"ALiBS.gv.hf.le="+ALiBS.gv.hf.le+ALiBS.gv.sy.se,{o:"ALiBS.hf"});

