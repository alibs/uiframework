/* *************************************************************************
// INFO:

USAGE:

var ix = new ALiBS.ix();
t.cb = function()
{
	if(!ALiBS.gv.it.pd || ALiBS.gv.it.pd==0) 
	{
		ALiBS.gv.it.pd = 1; // language changed/processed on page
		//
		if(a.cl) a.cl();
	}
	else xxxx(); // generate error;
};
t.ex();

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.ix) ALiBS.gv.ix = {}; // global values
//
if(!ALiBS.gv.ix.le) ALiBS.gv.ix.le = 0; // local log status
//
ALiBS.gv.ix.nn = 0; // x times loaded
//
ALiBS.gv.ix.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.ix = function(p)
// p = (optional) (JSON) parameters {m:10}
//	m = (number)max levels
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ix.ix",le:ALiBS.gv.ix.le});
	//
	var a = {};
	// *****************************************************************
	a.a = new Array(); // one position for each level
	// *****************************************************************
	a.m = (p && p.m) || 10; // default max levels
	// *****************************************************************
	a.cl = 0; // current level
	// *****************************************************************
	a.r = function(p) // reset
	// p = (optional) (JSON) parameters {i:2}
	//	i = (number)initial position to reset
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ix.a.r",le:ALiBS.gv.ix.le});
		//
		var i = (p && p.i) || 0; // default position to start reset
		//
		for(var j=i;j<a.m;j++)
		{
			a.a[j] = 0; // save number
		}
	};
	// *****************************************************************
	a.c = function(p) // current
	// p = (optional) (JSON) parameters {s:"."}
	//	s = (String)separator 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ix.a.c",le:ALiBS.gv.ix.le});
		//
		var s = (p && p.s) || "";
		var b = new Array();
		//
		for(var i=0;i<=a.cl;i++)
		{
			b[b.length] = a.a[i];
		}
		//
		return b.join(s);
	};
	// *****************************************************************
	a.n = function(l,p) // next level
	// l = (required)(number)level to increment
	// p = (optional) (JSON) parameters {s:"."}
	//	s = (String)separator 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ix.a.n",le:ALiBS.gv.ix.le});
		//
		var s = (p && p.s) || "";
		var b = new Array();
		//
		a.cl += l;
		a.a[a.cl]++;
		a.r({i:a.cl+1}); 
		//
		for(var i=0;i<=a.cl;i++)
		{
			b[b.length] = a.a[i];
		}
		//
		return b.join(s);
	};
	// *****************************************************************
	a.s = function(l,p) // set level
	// l = (required)(number)level to increment
	// p = (optional) (JSON) parameters {s:"."}
	//	s = (String)separator 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ix.a.n",le:ALiBS.gv.ix.le});
		//
		var s = (p && p.s) || "";
		var b = new Array();
		//
		a.cl = l;
		a.a[a.cl]++;
		a.r({i:a.cl+1}); 
		//
		for(var i=0;i<=a.cl;i++)
		{
			b[b.length] = a.a[i];
		}
		//
		return b.join(s);
	};
	// *****************************************************************
	a.r(); // reset to start
	// *****************************************************************
	return a;
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ix("+(++ALiBS.gv.ix.nn)+"x)",{o:"ALiBS.ix"});
ALiBS.sy.la(0+0,"ALiBS.gv.ix.le="+ALiBS.gv.ix.le+ALiBS.gv.sy.se,{o:"ALiBS.ix"});

