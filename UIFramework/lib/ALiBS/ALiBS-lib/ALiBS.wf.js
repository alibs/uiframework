/* *************************************************************************
// INFO:

// http://docs.oracle.com/javase/6/docs/api/java/io/File.html#renameTo%28java.io.File%29
An abstract representation of file and directory pathnames.


Used by:

Rhino command line
jswebjetty
jswebserver
novell drivers

// *************************************************************************
// TODO(2010-09-08):

TODO(2010-09-08): 

*/
//
// *************************************************************************
//
if(!ALiBS.gv.wf) ALiBS.gv.wf = {}; // global values
//
if(!ALiBS.gv.wf.le) ALiBS.gv.wf.le = 0; // local log status
//
ALiBS.gv.wf.nn = 0; // x times loaded
//
ALiBS.gv.wf.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wf.ff = {}; // custom functions
//
ALiBS.gv.wf.fl = ""; // file list to process
//
ALiBS.gv.wf.pt==""; // ROOT/Absolute path to files
//
// *************************************************************************
//
ALiBS.wf = // server file
{
	// *****************************************************************
	rl: function(f,d,p) // read line(s) from text file(s)
	// f = (required)(String)file list (separated by semicolon (;))
	// d = (required)(JSON)driver functions (handle: (ty)type,(rc)record)
	// p = (optional)(JSON)parameters {} 
	//	pr = (number)print line (1,2)
	//	my = (number)max lines to read to find type
	// return ????
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
		//
		var ret = "";
		//
		var se = "$pIpE$";
		//
		try 
		{
			var fs; // file names Array
			//
			if(f==null || typeof(f)!="string")
			{
				return null; // TODO(2011-02-03): acertar retorno
			}
			else
			{
				fs = f.split(";");
			}
			//
			var l = ""; // line (text)
			var n = 1; // line number/counter
			var h; // file handle
			//
			var pr = 0; // (default) do not print line // TODO(2011-06-29):usar ALiBS.gv.sy.so
			var my = 10; // (default) max lines to read to find type
			var a = new Array(); // Array for lines to find type
			var ty = ""; // file type ID (ex: idm-log-driver-jdbc)
			//
			if(p)
			{
				if(typeof(p.pr)=="number") pr = p.pr;
				if(typeof(p.my)=="number" && my>0 && my<100) my = p.my;
			}
			//
			for(var i=0;i<fs.length;i++)
			{
				if(pr>0)
				{
					ALiBS.sy.la(7+0,"\nprocessing file: "+fs[i]+"\n",{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
				}
				//
				// ***************************************** read (my) lines
				//
				n = 1; // reset line number before next file
				a = new Array();
				//
				h = new java.io.BufferedReader(new java.io.FileReader(fs[i])); 
				//
				while((l=h.readLine())!=null) // read lines to find file type
				{
					a[a.length] = l;
					//
					if(pr==1)
					{
						ALiBS.sy.la(7+0,"(read (my))processing line number "+n+":"+l,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
					}
					//
					if(pr==2)
					{
						ALiBS.sy.la(7+0,"(read (my))processing line number "+n,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
					}
					//
					if(n==my) break; // max lines to read to find type
					//
					n++;
				}
				//
				n--;
				//
				if(pr>0)
				{
					ALiBS.sy.la(7+0,"\n(read (my))total processed lines: "+n,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
				}
				//
				try
				{ 
					h.close(); 
				}
				catch(e)
				{ 
					ALiBS.sy.la(1+0,"(read (my))ALiBS.wf.rl(h.close)("+fs[i]+")(exception):"+e,{o:"ALiBS.wf.rl"});
					//
					ret += "(read (my))ALiBS.wf.rl(h.close)("+fs[i]+")(exception):"+e + se;
				}
				//
				// ***************************************** type
				//
				ty = "";
				//
				for(var j in d) // driver functions
				{
					print("j:"+j);
				}
				//
				for(var j in d) // driver functions
				{
					if(pr==1)
					{
						ALiBS.sy.la(7+0,"\nprocessing driver:"+j,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
					}
					//
					ty = d[j].ty(a);
					//
					if(ty=="")
					{
						if(pr==1)
						{
							ALiBS.sy.la(7+0,"VAZIO",{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
						}
					}
					else 
					{
						if(pr==1)
						{
							ALiBS.sy.la(7+0,"ty="+ty,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
						}
						break;
					}
				}
				//
				if(pr==1)
				{
					ALiBS.sy.la(5+0,"\n",{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
				}
				//
				if(ty=="") break; // did not find a driver
				//
				// ***************************************** process
				//
				n = 1; // reset line number before processing
				//
				h = new java.io.BufferedReader(new java.io.FileReader(fs[i])); 
				//
				while((l=h.readLine())!=null) // read lines to process
				{
					d[ty].rc(fs[i],ty,n,l,p);
					//
					//
					if(pr==1)
					{
						ALiBS.sy.la(7+0,"processing line number "+n+":"+l,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
					}
					//
					if(pr==2)
					{
						ALiBS.sy.la(7+0,"processing line number "+n,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
					}
					n++;
				}
				//
				d[ty].rc(fs[i],ty,-1,"",p); // WARNING of EOF
				//
				n--;
				//
				if(pr>0)
				{
					ALiBS.sy.la(7+0,"\ntotal processed lines: "+n,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
				}
				//
				ret += "total processed lines: " + n + se;
				//
				try
				{ 
					h.close(); 
				}
				catch(e)
				{ 
					ALiBS.sy.la(7+0,"ALiBS.wf.rl(h.close)(exception):"+e,{o:"ALiBS.wf.rl",le:ALiBS.gv.wf.le});
					ret += "ALiBS.wf.rl(h.close)(exception):" + e + se;
				}
				//
				//try{ d[ty].cl(); } // custom closing function
				//catch(e){ print("ALiBS.wf.rl(h.close)(exception):"+e); }
				//
				// *****************************************
			}
		}
		catch(e)
		{ 
			ALiBS.sy.la(1+0,"ALiBS.wf.rl(main)(exception):"+e,{o:"ALiBS.wf.rl"});
			ret += "ALiBS.wf.rl(main)(exception):" + e + se;
		}
		//
		// ************************
		//
		try
		{ 
			h.close(); 
		} // redundance
		catch(e)
		{ 
			ALiBS.sy.la(1+0,"ALiBS.wf.rl(h.close)(exception):"+e,{o:"ALiBS.wf.rl"});
			ret += "ALiBS.wf.rl(h.close)(exception):" + e + se;
		}
		//
		// ************************
		//
		try
		{ 
			d[ty].cl(); 
		} // custom closing function
		catch(e)
		{ 
			ALiBS.sy.la(1+0,"ALiBS.wf.rl(d[ty].cl())(exception):"+e,{o:"ALiBS.wf.rl"});
			ret += "ALiBS.wf.rl(d[ty].cl())(exception):" + e + se;
		}
		//
		return ret;
	},
	// *****************************************************************
	td: function(nm) // text driver
	// nm = (required)(String)name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.td",le:ALiBS.gv.wf.le});
		//
		var t = {}; // text driver
		//
		t.oo = {}; // output object = new version
/*

dd.oo = {};

dd.oo["linhas_vazias"] =
{
	n: "[OUTPUT][BATI]-Linhas-Vazias.txt",
	a: new Array(),
	f: file handle
};

dd.oo["erro_crude"] =
{
	n: "[OUTPUT][BATI]-Erro-Crude.txt",
	a: new Array()	
};

*/
		//
		t.op = new Array(); // old version
		//
		t.ok = 0; // should output data
		t.dn = "";
		t.fl = "";
		//
		// *********************************************************
		t.it = function(nm,fa,fo) // overwrite init function
		// nm = (required)(String)txtd name (ex: "LDIFGEN")
		// fa = (required)(Array)input files
		// fa = (required)(Array)output files
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.td.it",le:ALiBS.gv.wf.le});
			//
			var dd = ALiBS.gv.wf.ff[nm];
			//
			dd.op.length = 0;
			//
			// ****************
			//
/*			for(var i in dd.oo)
			{
				dd.op[dd.op.length] = fo[i];
			}
*/			//
			// ****************
			//
			for(var i=0;i<fo.length;i++) // output files
			{
				dd.op[dd.op.length] = fo[i];
			}
			//
			for(var i=0;i<dd.op.length;i++) // create Array()s
			{
				var ii = (i<10)?"0"+i:""+i;
				//
				dd["a"+ii] = new Array(); // a00 = new Array();
			}
			//
			// ****************
			//
			var pi = "input/";  // input path
			var po = "output/";  // output path
			//
			var fi = new Array(); // input files
			//
			for(var i=0;i<fa.length;i++) // input files
			{
				fi[fi.length] = ALiBS.gv.wf.pt + pi + fa[i];
			}
			//
			dd.fl = fi.join(";"); // SET input file list (could be APPEND (+=))
			//ALiBS.gv.wf.fl = fi.join(";"); // SET input file list (could be APPEND (+=))
			//
			try // output file
			{
				//
				// ****************
				//
				for(var i=0;i<dd.op.length;i++)
				{
					var ii = (i<10)?"0"+i:""+i;
					//
					dd["fh"+ii] = new java.io.BufferedWriter(new java.io.FileWriter(ALiBS.gv.wf.pt + po + "op" + ii + "-" + dd.op[i]));
				}
			}
			catch(e)
			{
				print("found:"+e);
			}
		};
		// *********************************************************
		t.cl = function() // overwrite closing function
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.td.cl",le:ALiBS.gv.wf.le});
			//
			for(var j in ALiBS.gv.wf.ff)
			{
				var dd = ALiBS.gv.wf.ff[j];
				//
				try
				{ 
					if(dd.ok==2) // save data
					{
						for(var i=0;i<dd.op.length;i++)
						{
							var ii = (i<10)?"0"+i:""+i;
							//
							if(dd["a"+ii].length) dd["fh"+ii].write(dd["a"+ii].join(""));
						}
					}	
					//
					for(var i=0;i<dd.op.length;i++)
					{
						var ii = (i<10)?"0"+i:""+i;
						//
						dd["fh"+ii].close(); 
					}
				} 
				catch(e)
				{
					print("found:"+e); 
				}
			}
		};
		// *********************************************************
		t.ty = function(a) // find file type
		// a = (required)(Array)lines
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.td.ty",le:ALiBS.gv.wf.le});
			//
			var l = a.length;
			//
			var e0 = "";
			var e1 = "";
			//
			for(var i=0;i<l;i++)
			{
				/*
				if(a[i].indexOf("[")==0) e0 = "cn=TesteUnitario,cn=Recursos,cn=Level10";
				//
				if(a[i].indexOf("<nds ")==0) e1 = "OK";
				//
				if(e0=="OK" && e1=="OK") return "LDIFGEN";
				*/
				return "ABCD";
			}
			//
			return "";
		};
		// *********************************************************
		t.rc = function(f,y,n,l,p) // process record
		// f = (required)(String)file name
		// y = (required)(String)file type
		// n = (required)(number)line number
		// l = (required)(String)line (text)
		// p = (optional)(JSON)parameters {} 
		//	pr = (number)print line (1,2)
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.td.rc",le:ALiBS.gv.wf.le});
			//
		};
		// *********************************************************
		//
		return t;
	},
	// *****************************************************************
	dw: function(u) // download file from URL
	// u = (required)(String)url (http://www.google.com)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.dw",le:ALiBS.gv.wf.le});
		//
		var l;
		var o;
		var d;
		var s;
		var a = new Array();
		//
		try 
		{
			l = new java.net.URL(u);
			//
			o = l.openStream();         
			//
			d = new java.io.DataInputStream(new java.io.BufferedInputStream(o));
			//
			while((s = d.readLine()) != null) 
			{
				a[a.length] = s;
			}
		} 
		catch(e)
		{
			ALiBS.sy.la(1+0,"ALiBS.wf Exception(e):"+e,{o:"ALiBS.wf"});
		} 
		//
		try 
		{ 
			o.close(); 
		} 
		catch(e)
		{
			ALiBS.sy.la(1+0,"ALiBS.wf Exception(e):"+e,{o:"ALiBS.wf"});
		} 
		//
		return a.join("");
	},
	// *****************************************************************
	hs: function(f) // file exists / has file
	// f = (required)(String)path+file name
	// return
	//	true = SUCCESS - TODO:(2013-03-13) should return 1
	//	false = FAILED - TODO:(2013-03-13) should return -1
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.hs",le:ALiBS.gv.wf.le});
		//
		var r = new java.io.File(f).exists();
		//
		return r;
	},
	// *****************************************************************
	mv: function(f,d) // renameTo
	// f = (required)(String)from path+file name
	// d = (required)(String)dest path+file name
	// return
	//	true = SUCCESS - TODO:(2013-03-13) should return 1
	//	false = FAILED - TODO:(2013-03-13) should return -1
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.mv",le:ALiBS.gv.wf.le});
		//
		var r = new java.io.File(f).renameTo(java.io.File(d));
		//
		return r;
	},
	// *****************************************************************
	de: function(f) // delete
	// f = (required)(String)path+file name
	// return
	//	true = SUCCESS - TODO:(2013-03-13) should return 1
	//	false = FAILED - TODO:(2013-03-13) should return -1
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.de",le:ALiBS.gv.wf.le});
		//
		var r = new java.io.File(f)["delete"](); // did NOT worked with new java.io.File(f).delete();
		//
		return r;
	},
	// *****************************************************************
	zp: function(f,o) // zip
	// f = (required)(String)input path+file name
	// o = (required)(String)output path+file name
	// return
	//	true = SUCCESS - TODO:(2013-03-13) should return 1
	//	false = FAILED - TODO:(2013-03-13) should return -1
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.zp",le:ALiBS.gv.wf.le});
		//
		var fos = new java.io.FileOutputStream(o);
		//
    		var zos = new java.util.zip.ZipOutputStream(fos);
		//
    		var ze = new java.util.zip.ZipEntry("xxxx");
		//
    		zos.putNextEntry(ze);
		//
    		var inp = new java.io.FileInputStream(f);
 		//
		var buffer = new Uint8Array(1024); // 8 bits x 1024 bytes TODO(2013-04-02): Drivers IDM não reconhecem, somente browsers mais novos
		//var buffer = new Uint8Array(1024); // 8 bits x 1024 bytes
		//var buffer = new Uint16Array(1024); // 16 bits x 1024 bytes
		//var buffer = new Uint32Array(1024); // 32 bits x 1024 bytes
		//
    		var len;
		//
    		while((len = inp.read(buffer)) > 0) 
		{
			zos.write(buffer, 0, len);
    		}
		//
    		inp.close();
		//
    		zos.closeEntry();
 		//
    		zos.close();
		//
		return 1;
	},
	// *****************************************************************
	ls: function(d,p) // list files
	// d = (required)(String)input path (dir name)
	// p = (optional)(JSON)parameters {} 
	//	f = (String)filter ({f:".txt"})
	//
	// return
	//	Array of String of files names (without DN path)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.ls",le:ALiBS.gv.wf.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ff = null;
		//
		if(typeof(z.f)=="string") z.ff = z.f;
		//
		var r = new java.io.File(d).list(z.ff);
		//
//ALiBS.sy.la(1,"files="+r.join(","),{le:1});
		//
		return r;
	},
	// *****************************************************************
	lm: function(f) // last modified
	// f = (required)(String)path+file name
	// return
	//	(number) number milliseconds since the epoch (00:00:00 GMT, January 1, 1970)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wf.lm",le:ALiBS.gv.wf.le});
		//
		var r = new java.io.File(f).lastModified(); 
		//
//ALiBS.sy.la(1,"ts="+r,{le:1});
		//
		return r;
	}
	// *****************************************************************
	// http://docs.oracle.com/javase/6/docs/api/java/io/File.html#renameTo%28java.io.File%29
	// delete, mkdir, filelist, etc
};
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_wf_xx")>=0)
		{
			ALiBS_wf_xx = function(f,p0,p1,p2,p3,p4) // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_wf_xx",le:ALiBS.gv.wf.le});
				//
				if(f=="rl") return ALiBS.wf.rl(p0);
				else if(f=="cx") return ALiBS.wf.cx(p0);
				else if(f=="cm") return ALiBS.wf.cm(p0,p1);
			};
		}
	}
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wf("+(++ALiBS.gv.wf.nn)+"x)",{o:"ALiBS.wf"});
ALiBS.sy.la(0+0,"ALiBS.gv.wf.le="+ALiBS.gv.wf.le+ALiBS.gv.sy.se,{o:"ALiBS.wf"});

