/* *************************************************************************
// INFO:

// *************************************************************************

JQuery Mobile

http://www.w3schools.com/jquerymobile/
http://www.w3schools.com/jquerymobile/jquerymobile_icons.asp




// ***************************

	Navigation - http://demos.jquerymobile.com/1.4.5/navigation/

		location.hash = "i_uijmpg_div_page_page0";
		location.hash = "page0";
		location.hash = "one";
		location.hash = "two";
		location.hash = "i_uijmpg_div_page_page0";
		location.hash = "i_uijmpg_div_page_popup0";


		$.mobile.navigate( "#one" );

		window.history.back();


		alterContent( data.state.url );  // como usar ????

// ***************************

	Transitions - http://demos.jquerymobile.com/1.4.5/transitions/

		fade
		pop
		flip
		turn
		flow
		slidefade
		slide
		slideup
		slidedown
		none


		To view all transition types, you must be on a browser that supports 3D transforms

		$.mobile.defaultPageTransition
		$.mobile.defaultDialogTransition


		<a href="index.html" data-transition="pop">I'll pop</a>
		When the Back button is pressed, the framework will automatically apply the reverse version of the transition that was used to show the page. 
		To specify that the reverse version of a transition should be used, add the data-direction="reverse" attribute to a link.


		by default we specify "fade" as the fallback. 

		For example, this will set the fallback transition for the slideup transition to "none":

			$.mobile.transitionFallbacks.slideup = "none"


		Max scroll for transitions

			By default, transitions are disabled (set to "none") when you're either coming FROM or going TO a page 
			where the scroll position is 3x the height of the device's screen

			$.mobile.getMaxScrollForTransition()
			2820


		Max width for transitions

			By default, transitions can be disabled (set to "none") when the window width is greater than a certain pixel width. 

			$.mobile.maxTransitionWidth
				defaults to false
				accepts any number representing a pixel width or false value
				If it's not false, the handler will use a "none" transition when the window is wider than the specified value.


// ***************************

// *************************************************************************
// TODO:

	- incluir em jmpg
		- Panel with swipe
		- i18n
		OK - incluir outros botões no header para acesso a outra page


	- como controlar swipe down e up ? (para atualizar a pagina por exemplo, como no facebook e yahoo noticias)

	- como fazer o menu aparecer quando for desktop, igual ao site do jquery.mobile
*/
//
// *************************************************************************
//
if(!ALiBS.gv.ui["JQueryMobile.ui.le"]) ALiBS.gv.ui["JQueryMobile.ui.le"] = 0; // local log status
//
ALiBS.gv.ui["JQueryMobile.ui.nn"] = 0; // x times loaded
//
ALiBS.gv.ui["JQueryMobile.ui.rl"] = "15.0904.1403";
//
// *************************************************************************
//
// JQuery Mobile User Interface 
//
// *************************************************************************
//
ALiBS.ui.jmpg = function(t0,p) // JQuery Mobile Page
/** 
Method <static> <string> ALiBS.ui.jmpg(t0,p)

	Description: Encode, zip, cryto, b4 on text for HTTP / HTTPS transmition

	Parameters:

		t0 = (required)(ALiBS.to)table

		p = (optional)(JSON)parameter {} 

			ro = (number) append div data-role="page"
				1 = DEFAULT = append
				0 = do NOT append

	Observations:

	Return Value:
		<static> <string> HTML JQuery Page

	Example(s):
		var en = ALiBS.ui.jmpg(t0);

--------------------------------------------------------------------
*/
/*
	Confidential Info - do NOT disclose
*/
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ui.jmpg",le:ALiBS.gv.ui["JQueryMobile.ui.le"]});
	//
	var z = p || {}; // [BEST-PRACTICE]
	//
	z.ec = -1; // ec = start
	//
	var r; // [BEST-PRACTICE]
	//
	try
	{
		z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
		//
		// ************************
		//
		z.ec = -950; // [BEST-PRACTICE] parameters validation
		if(ALiBS.sy.ty(t0)!="object-ALiBS.to") throw "ALiBS.ui.jmpg.Exception: Error: Invalid Parameter Type: (object-ALiBS.to) a0 passed as "+ALiBS.sy.ty(t0); // 
		//
		//ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.ui.jmpg",le:ALiBS.gv.ui["JQueryMobile.ui.le"]});
		//
		// ************************
		//
		z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
		z.p_ro   = z.ro; // Parameter Pattern and/or OLD Parameter Compatibility
		//
		if(typeof(z.p_ro)!="number") z.p_ro = 1; // DEFAULT
		//
		ALiBS.sy.la(5+0,"z.p_ro="+z.p_ro,{o:"ALiBS.ui.jmpg",le:ALiBS.gv.ui["JQueryMobile.ui.le"]});
		//
		// ************************
		//
		z.ec = -1050; // [BEST-PRACTICE] table variables
		//
		// ************************
		//
		z.ec = -1100; // [BEST-PRACTICE] local variables
		z.v_a0 = new Array(); // 
		//
		// ************************
		//
		z.ec = -1200; // 
		//
		for(var i =0;i<t0.rs.length;i++)
		{
			// **************** Page
			//
			if(z.p_ro) // TODO(2015-09-11): por que este if existe ?
			{
				z.v_a0[z.v_a0.length] = '<div data-role="page" ';
				z.v_a0[z.v_a0.length] = 'id="i_uijmpg_div_page_' + (t0.get(i,"alibs.page.reference")) + '" ';
				z.v_a0[z.v_a0.length] = 'data-theme="' + (t0.get(i,"page-data-theme")||ALiBS.gv.ui.dth) + '">';
			}
			//
			// **************** Panel
			//
			if(t0.get(i,"LPanel0-btn-data-icon")||t0.get(i,"LPanel0-btn-text"))
			{
				z.v_a0[z.v_a0.length] = '<div data-role="panel" ';
				z.v_a0[z.v_a0.length] = 'id="i_uijmpg_div_lpanel0_' + (t0.get(i,"alibs.page.reference")) + '" ';
				z.v_a0[z.v_a0.length] = 'data-position="left" ';
				z.v_a0[z.v_a0.length] = 'data-display="' + (t0.get(i,"LPanel0-data-display")) + '" ';
				z.v_a0[z.v_a0.length] = 'data-theme="' + (t0.get(i,"page-data-theme")||ALiBS.gv.ui.dth) + '"></div>';
			}
			//
			if(t0.get(i,"RPanel0-btn-data-icon")||t0.get(i,"RPanel0-btn-text"))
			{
				z.v_a0[z.v_a0.length] = '<div data-role="panel" ';
				z.v_a0[z.v_a0.length] = 'id="i_uijmpg_div_rpanel0_' + (t0.get(i,"alibs.page.reference")) + '" data-position="right" ';
				z.v_a0[z.v_a0.length] = 'data-display="' + (t0.get(i,"RPanel0-data-display")) + '" ';
				z.v_a0[z.v_a0.length] = 'data-theme="' + (t0.get(i,"page-data-theme")||ALiBS.gv.ui.dth) + '"></div>';
			}
			//
			// **************** Page Header
			//
			z.v_a0[z.v_a0.length] = '<div data-role="header" ';
			z.v_a0[z.v_a0.length] = 'data-theme="' + (t0.get(i,"header-data-theme")||t0.get(i,"page-data-theme")||ALiBS.gv.ui.dth) + '">'; // header
			z.v_a0[z.v_a0.length] = '<h1>' + (t0.get(i,"header-title")) + '</h1>';
			//
			// **************** Panel Buttons
			//
			if(t0.get(i,"LPanel0-btn-text")) // left panel button
			{
				z.v_a0[z.v_a0.length] = '<a href="#i_uijmpg_div_lpanel0_' + (t0.get(i,"alibs.page.reference")) + '" ';
				z.v_a0[z.v_a0.length] = 'class="ui-btn ui-icon-'+(t0.get(i,"LPanel0-btn-data-icon"))+' ui-btn-icon-left ui-shadow-icon" ';
				z.v_a0[z.v_a0.length] = 'data-icon="'+(t0.get(i,"LPanel0-btn-data-icon"))+'" ';
				z.v_a0[z.v_a0.length] = 'data-iconpos="notext">';
				z.v_a0[z.v_a0.length] = t0.get(i,"LPanel0-btn-text");
				z.v_a0[z.v_a0.length] = '</a>';
			}
			//
			else if(t0.get(i,"LPanel0-btn-data-icon")) // left panel button
			{
				z.v_di0 = t0.get(i,"LPanel0-btn-data-icon").split(",");
				//
				z.v_a0[z.v_a0.length] = '<div class="ui-btn-left ui-nodisc-icon">';
				//
				for(var j=0;j<z.v_di0.length;j+=1)
				{
					z.v_a0[z.v_a0.length] = '<a href="#i_uijmpg_div_lpanel0_' + (t0.get(i,"alibs.page.reference")) + '" ';
					z.v_a0[z.v_a0.length] = 'class="ui-btn-inline ui-nodisc-icon ui-link ui-btn ui-btn-icon-notext ui-corner-all ';
					z.v_a0[z.v_a0.length] = 'ui-icon-'+(z.v_di0[j])+'" ';
					z.v_a0[z.v_a0.length] = 'data-icon="'+(z.v_di0[j])+'" ';
					z.v_a0[z.v_a0.length] = 'data-iconpos="notext" data-shadow="false" data-iconshadow="false" data-role="button" role="button">'; 
					z.v_a0[z.v_a0.length] = '</a>';
				}
				//
				z.v_a0[z.v_a0.length] = '</div>';
			}
			//
			// *********
			//
			if(t0.get(i,"RPanel0-btn-text")) // right panel button
			{
				z.v_a0[z.v_a0.length] = '<a href="#i_uijmpg_div_rpanel0_' + (t0.get(i,"alibs.page.reference")) + '" ';
				z.v_a0[z.v_a0.length] = 'class="ui-btn ui-icon-'+(t0.get(i,"RPanel0-btn-data-icon"))+' ui-btn-icon-right ui-shadow-icon" ';
				z.v_a0[z.v_a0.length] = 'data-icon="'+(t0.get(i,"RPanel0-btn-data-icon"))+'" ';
				z.v_a0[z.v_a0.length] = 'data-iconpos="notext">';
				z.v_a0[z.v_a0.length] = t0.get(i,"RPanel0-btn-text");
				z.v_a0[z.v_a0.length] = '</a>';
			}
			//
			// *********
			//
			else if(t0.get(i,"RPanel0-btn-data-icon")) // right panel button
			{
				z.v_di0 = t0.get(i,"RPanel0-btn-data-icon").split(",");
				//
				z.v_a0[z.v_a0.length] = '<div class="ui-btn-right ui-nodisc-icon">';
				//
				for(var j=0;j<z.v_di0.length;j+=1)
				{
					z.v_a0[z.v_a0.length] = '<a href="#i_uijmpg_div_rpanel0_' + (t0.get(i,"alibs.page.reference")) + '" ';
					z.v_a0[z.v_a0.length] = 'class="ui-btn-inline ui-nodisc-icon ui-link ui-btn ui-btn-icon-notext ui-corner-all ';
					z.v_a0[z.v_a0.length] = 'ui-icon-'+(z.v_di0[j])+'" ';
					z.v_a0[z.v_a0.length] = 'data-icon="'+(z.v_di0[j])+'" ';
					z.v_a0[z.v_a0.length] = 'data-iconpos="notext" data-shadow="false" data-iconshadow="false" data-role="button" role="button">'; 
					z.v_a0[z.v_a0.length] = '</a>';
				}
				//
				z.v_a0[z.v_a0.length] = '</div>';
			}
			//
			// **************** Page Header End
			//
			z.v_a0[z.v_a0.length] = '</div>'; // header
			//
			// **************** Page Main
			//
			z.v_a0[z.v_a0.length] = '<div role="main" ';
			z.v_a0[z.v_a0.length] = 'class="ui-content">'; // main
			z.v_a0[z.v_a0.length] = '<h2>' + (t0.get(i,"main-title")) + '</h2>';
			z.v_a0[z.v_a0.length] = '<div id="i_uijmpg_div_main_' + (t0.get(i,"alibs.page.reference")) + '"></div>';
			z.v_a0[z.v_a0.length] = '</div>'; // main
			//
			// **************** Page Footer
			//
			z.v_a0[z.v_a0.length] = '<div data-role="footer" ';
			z.v_a0[z.v_a0.length] = 'data-theme="' + (t0.get(i,"footer-data-theme")||t0.get(i,"page-data-theme")||ALiBS.gv.ui.dth) + '">'; // footer
			z.v_a0[z.v_a0.length] = '<h4>' + (t0.get(i,"footer-title")) + '</h4>';
			z.v_a0[z.v_a0.length] = '</div>'; // footer
			//
			if(z.p_ro) z.v_a0[z.v_a0.length] = '</div>'; // page
			//
			z.v_a0[z.v_a0.length] = ALiBS.gv.sy.se; // \n = ONLY TO friendly view genereated HTML
		}
		//
					/*
						http://demos.jquerymobile.com/1.4.5/pages/

						<div data-role="page" id="one">

							<div data-role="header">
								<h1>Multi-page</h1>
							</div>

							<div role="main" class="ui-content">
								<h2>One</h2>

								<p>Page One</p>

								<h3>Show internal pages:</h3>
								<p><a href="#two" class="ui-btn ui-shadow ui-corner-all">Show page "two"</a></p>
								<p><a href="#popup" class="ui-btn ui-shadow ui-corner-all" data-rel="dialog" data-transition="pop">Show page "popup" (as a dialog)</a></p>

							</div>

							<div data-role="footer" data-theme="b">
								<h4>Page Footer</h4>
							</div>
						</div>

						<div data-role="page" id="popup">

							<div data-role="header" data-theme="b">
								<h1>Dialog</h1>
							</div>

							<div role="main" class="ui-content">
								<h2>Popup</h2>
								<p>Page popup</p>
								<p><a href="#one" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back to page "one"</a></p>
							</div><!-- /content -->

							<div data-role="footer">
								<h4>Page Footer</h4>
							</div><!-- /footer -->
						</div>

					*/
		//
		// ************************
		//
		z.ec = -9900; // [BEST-PRACTICE] assign return value
		r = "" + z.v_a0.join(""); 
		//
		// ************************
		//
		z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
	}
	catch(e)
	{
		switch(z.ec)
		{
			case -1:z.m0="Generic Error";z.m1="";break;
			case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
			default:z.m0="Generic Default Error";z.m1="";break;
		}
		//
		r = ALiBS.sy.em(z.ec,"ALiBS.ui.jmpg",{m0:z.m0,m1:z.m1,m2:e}).m2;
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		throw r;
	}
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
	//
	ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.ui.jmpg",le:ALiBS.gv.ui["JQueryMobile.ui.le"]});
	//
	ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.ui.jmpg",le:ALiBS.gv.ui["JQueryMobile.ui.le"]});
	//ALiBS.gv.sy.se
	return r; 
};
// *************************************************************************
//
ALiBS.ui.jmsw = function(pg,lp,rp,p) // JQuery Mobile Page Swipe
// pg = (string)page name
{
	$( document ).on( "pagecreate", "#"+pg, 
		function()
		{
			$( document ).on( "swipeleft swiperight", "#"+pg, 
				function( e ) 
				{
					// We check if there is no open panel on the page because otherwise
					// a swipe to close the left panel would also open the right panel (and v.v.).
					// We do this by checking the data that the framework stores on the page element (panel: open).
					if ( $( ".ui-page-active" ).jqmData( "panel" ) !== "open" ) 
					{
						if ( e.type === "swipeleft" ) 
						{
							$( "#"+rp ).panel( "open" );
						}
						else if ( e.type === "swiperight" )
						{
							$( "#"+lp ).panel( "open" );
						}
					}
				}
			);
		}
	);
};
// *************************************************************************
//
ALiBS["JQueryMobile.ui"] = ALiBS.ui; // reference - required for loading module correctly
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module JQueryMobile.ui("+(++ALiBS.gv.ui["JQueryMobile.ui.nn"])+"x)",{o:"JQueryMobile.ui"});
ALiBS.sy.la(0+0,"ALiBS.gv.ui[JQueryMobile.ui.rl]="+ALiBS.gv.ui["JQueryMobile.ui.rl"],{o:"JQueryMobile.ui"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.ui[JQueryMobile.ui.le]="+ALiBS.gv.ui["JQueryMobile.ui.le"]+ALiBS.gv.sy.se,{o:"JQueryMobile.ui"});

