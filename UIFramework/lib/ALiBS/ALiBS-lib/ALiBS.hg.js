/* *************************************************************************
// INFO:


// *************************************************************************
// TODO:
//

TODO:

	- Criar	

		- (? dias) Criar combo com o nome dos campos visiveis e ordenaveis, como outra opção 
			para ordenar campos (2014-05-08)
			

		- (1dia) botão "Clear" para limpar todos os filtros

		- td() - funcao ff0 para executar<br/>(gravar arquivo, formatar, remover registro, etc)(como em wl.js)
		- ts() - funcao ff0 para executar<br/>(gravar arquivo, formatar, remover registro, etc)(como em wl.js)
		- t2j() - funcao ff0 para executar<br/>(gravar arquivo, formatar, remover registro, etc)(como em wl.js)
		- t2v() - funcao ff0 para executar<br/>(gravar arquivo, formatar, remover registro, etc)(como em wl.js)
		- t2x() - funcao ff0 para executar<br/>(gravar arquivo, formatar, remover registro, etc)(como em wl.js)

		- t2j() - passar nome da tabela para criar T0.rs[0].f00 (tb_rf e/ou tb_id)
		- t2v() - Opções: com/sem aspas, indicar separador ; | etc


TODO: i18n 
	- quais são os defaults se não carregar i18n ? (INFO,Pagina,Total,etc)

TODO: acertar IDs

TODO: Paginação
	- carregar arquivo e/ou registros a cada 100/1000 registros paginados ou quando chegar ao último registro
		- criar funcao ff0 para executar para fazer o carregamento (como em wl.js)
	- [OK] number of records to display in grid
	- [OK] first record
	- [OK] last record
	- [OK] botoes/links [last]/[next]
	- [OK] ajustar grid para mostrar corretamete número de registros menor que o número setado para ser mostrado na página

TODO: Outros controles
	- campo para selecionar = radio
	- campo para deletar registro = X

TODO: CSS/img
	- [OK] hg.img0.ng
	- usar img como fundo do header

TODO: Campo com Tree/Arvore como em http://www.treegrid.com/treegrid/www/

TODO: Header Controles
	- [OK] criar entrada no css para novos headers
	- [OK] indices AND dos campos da tabela (como planilha)
	- [OK] combo para selecionar indices complexos que nao sao simplesmente campos da tabela
	- combo para selecionar campos a serem mostrados
	- input para pesquisa nos registros/campos
	- pagina de configuração de paginacao

TODO: Edição de campos 
	- div/page para pegar novo valor digitado pelo usuário

TODO: controle de registros para ser reenviado ao server
	- registros novos
	- modificados 
	- deletados - ver ALiBS.to

TODO: ver anotações no caderno no carro

TODO: exemplo de grid: http://www.treegrid.com/treegrid/www/

*/
//
// *************************************************************************
//
if(!ALiBS.gv.hg) ALiBS.gv.hg = {}; // global values
//
if(!ALiBS.gv.hg.le) ALiBS.gv.hg.le = 0; // local log status
//
ALiBS.gv.hg.nn = 0; // x times loaded
//
ALiBS.gv.hg.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.hg.rl = "15.0716.1059";
//
// *************************************************************************
//
ALiBS.gv.hg.tt = 0; // field title control
	// 0 = DEFAULT = do NOT add (title+fieldCode) automatically
	// 1 = add (title+fieldCode) to the end
	// 2 = add (fieldCode+title) to the beginning
//
//ALiBS.gv.hg.g0_imga = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imga0.png"; // ascending arrow image 12x7
//ALiBS.gv.hg.g0_imgd = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imgd0.png"; // descending arrow image 12x7
//
ALiBS.gv.hg.g0_imga = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imga1.png"; // ascending arrow image 9x10
ALiBS.gv.hg.g0_imgd = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imgd1.png"; // descending arrow image 9x10
//
//ALiBS.gv.hg.g0_imgx = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imgx0.png"; // X delete image 16x16
ALiBS.gv.hg.g0_imgx = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imgx1.png"; // X delete image 13x13
//
ALiBS.gv.hg.g0_imge = ALiBS.gv.sy.sp + "ALiBS/hg.g0.imge1.png"; // X edit image 20x16
//
// *************************************************************************
//
ALiBS.hg = // HTML Grid
{
	g0: function(t,n,p) // grid 0
	// t = (required)(ALiBS.to)table
	// n = (required)(String)grid name TODO(2012-08-31): TO BE IMPLEMENTED
	// p = (optional)(JSON)parameters {} 
	//	cs = (String)class for table

	//	hd0  = header div 0 = custom
	//
	//

	//	hx = (number) show "Grid Name / Text" header
	//		0 = do NOT show this header
	//		1 = show this header = DEFAULT

	//	h0 = (number) show "field names" header
	//		0 = do NOT show this header
	//		1 = show this header = DEFAULT

	//	h1 = (number) show "filter by field" header 
	//		0 = do NOT show this header
	//		1 = show this header = DEFAULT

	//	h2 = (number) show "control fields" header - complex filters, select fields to show TODO(2012-08-24): TO BE IMPLEMENTED
	//		0 = do NOT show this header
	//		1 = show this header = DEFAULT

	//	h3 = (number) show control header 3 (paging at top) TODO(2012-08-24): TO BE IMPLEMENTED
	//		0 = do NOT show this header
	//		1 = show this header = DEFAULT

	//	h4 = (number) show control header 4 (paging at top) TODO(2012-08-24): TO BE IMPLEMENTED
	//		0 = do NOT show this header
	//		1 = show this header = DEFAULT

	//	ph = (number)reprint header each "ph" lines 
	//		-1 = DEFAULT = (ph<0) do not print header again

	//	ph0 = (number)number of lines of the first page
	//		-1 = DEFAULT

	//	aj = (number)ajust after "aj" pages
	//		0 = DEFAULT

	//	el = (number)empty lines at the end
	//

	//	ff0 = (function)function(t0,ri) ALWAYS runs for EVERY record in filter

	// return = (Array) HTML table <table><tbody><tr><td></td></tbody></table>
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg.g0",le:ALiBS.gv.hg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
		z.c0 = z.cs + "r0"; // DEFAULT 
		z.c1 = z.cs + "r1"; // DEFAULT 
		z.c2 = z.cs + "r2"; // DEFAULT 
		//
		if(typeof(z.hx)!="number") z.hx = 1; // show header = DEFAULT
		//
		if(typeof(z.h0)!="number") z.h0 = 1; // show header = DEFAULT
		if(typeof(z.h1)!="number") z.h1 = 1; // show header = DEFAULT
		if(typeof(z.h2)!="number") z.h2 = 1; // show header = DEFAULT
		if(typeof(z.h3)!="number") z.h3 = 1; // show header = DEFAULT
		if(typeof(z.h4)!="number") z.h4 = 1; // show header = DEFAULT
		//
		if(typeof(z.ph)!="number")
		{
			z.ph = -1; // DEFAULT 
			z.ph0 = -1; // DEFAULT 
			z.phx = -1; // DEFAULT - NEXT line(j) to break
		}
		else
		{
			if(typeof(z.ph0)!="number") // NOT valid
			{
				z.phx = z.ph; // NEXT line(j) to break 
			}
			else
			{
				z.phx = z.ph0; // NEXT line(j) to break 
			}
		}
		//
		if(typeof(z.aj)!="number") z.aj = 0; // DEFAULT 
		//
		if(typeof(z.el)!="number") z.el = 0; // DEFAULT 
		//
		// table varibles
		if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc_; // pointer = DEFAULT - "field filter" header click
		if(typeof(t.ff.hg.ff)=="undefined") t.ff.hg.ff = ALiBS.hg._hgff_; // pointer = DEFAULT - field click
		//
		// ****************
		//
		//t.tb.hg.pg = t.tb.pg; // OLD Compatibility - replaced "tb.pg" by "tb.hg.pg"
		//
		t.tb.hg.pg.hx = (t.tb.hg.pg.hx||t.tb.hx); // OLD Compatibility - added "hg.pg"
		t.tb.hg.pg.sh = (t.tb.hg.pg.sh||t.tb.sh); // OLD Compatibility - added "hg.pg"
		t.tb.hg.pg.fk = (t.tb.hg.pg.fk||t.tb.fk); // OLD Compatibility - added "hg.pg"
		t.tb.hg.pg.fi = (t.tb.hg.pg.fi||t.tb.fi); // OLD Compatibility - added "hg.pg"
		t.tb.hg.pg.fv = (t.tb.hg.pg.fv||t.tb.fv); // OLD Compatibility - added "hg.pg"
		t.tb.hg.pg.nso = (t.tb.hg.pg.nso||t.tb.nso); // OLD Compatibility - added "hg.pg"
		//
		// ****************
		//
		// local varibles [BEST-PRACTICE]
		z.a = new Array();
		//
		z.fd = t.tb.hg.pg.sh || ["f00","f01","f02","f03","f04","f05","f06","f07","f08","f09"]; // DEFAULT
		//
		z.rc = 0; // record count
		z.n0;
		z.cc = true; // record color control
		//
		z.r0; // return of t.ft[fields] filter function
		z.k0; // field values to create <select>
		z.k1; // (Array) fields to create <select>
		z.k2; // (String) fields to create <select>
		//
		z.ajc = 1; // COUNTER = init count on page = Default = 1 - works with z.aj
		z.aja = 1; // (if) pixels
		z.ajb = 0; // (else) pixels
		//
		// ************************
		//
		if(t.tb.hg.pg.n>0) // paging ON
		{
			if(t.tb.hg.pg.c==null)
			{
				t.tb.hg.pg.c = new Array(); // create cache
				//
				t.tb.hg.pg.t = 0; // number of filtered records to show
				z.rc = 0; // record count
				//
				for(var j=0;j<t.rs.length;j++) // records
				{
					if(t.tb.fa!="" && t.ft[t.tb.fa]) // filter
					{
						z.r0 = t.ft[t.tb.fa](t,j);
						//
						if(z.r0>0) z.r0; // {} // OK
						else if(z.r0==0) continue; // ALiBS.to.f0 and ALiBS.to.f1
						else if(z.r0==-1) continue; // field filter
						else if(z.r0==-2) break;
					}
					//
					if(z.ff0) // ff0 = (function)function(t0,ri) ALWAYS runs for EVERY record in filter
					{
						z.ff0(t,j); // table,record
					}
					//
					t.tb.hg.pg.t++; // number of filtered records to show
					z.rc++;
					//
					if(z.rc==1) // first record
					{
						t.tb.hg.pg.c[t.tb.hg.pg.c.length] = {};
						// 
						t.tb.hg.pg.c[t.tb.hg.pg.c.length-1]["v0"] = j; // first record = position in t.rs[j]
					}
					//
					t.tb.hg.pg.c[t.tb.hg.pg.c.length-1]["v1"] = j; // last record = position in t.rs[j] (always updated)
					t.tb.hg.pg.c[t.tb.hg.pg.c.length-1]["n"] = z.rc; // number of records in this paging
					//
					if(z.rc==t.tb.hg.pg.n)
					{
						z.rc = 0; // reset counter
					}
				}
				//
				t.tb.hg.pg.p = 0; // first position in cache
				//
				if(typeof(t.tb.hg.pg.c[0])=="undefined") // no record in filter
				{
					t.tb.hg.pg.c[0] = {};
					// 
					t.tb.hg.pg.c[0]["v0"] = -1;
					t.tb.hg.pg.c[0]["v1"] = -1;
					t.tb.hg.pg.c[0]["n"] = 0;
				}
//console.log("1:new cache");
			}
			else
			{
				if(t.tb.hg.pg.p<0) t.tb.hg.pg.p = 0; // first position
				else if(t.tb.hg.pg.p>=t.tb.hg.pg.c.length) t.tb.hg.pg.p = t.tb.hg.pg.c.length-1; // last position
//console.log("2:old cache");
			}
		}
		else // paging OFF
		{
			t.tb.hg.pg.c = null; // should always be null if no paging is selected
		}
		//
/*
if(t.tb.hg.pg.c!=null) // paging ON
{
console.log("number of records in table: "+t.rs.length);
console.log("number of records in filter: "+t.tb.hg.pg.t);
console.log("max records per page: "+t.tb.hg.pg.n);
//
console.log("number of pages in filter: "+t.tb.hg.pg.c.length);
console.log("this page is number: "+(t.tb.hg.pg.p)); // (t.tb.hg.pg.p+1));
console.log("number of records in this page: "+t.tb.hg.pg.c[t.tb.hg.pg.p].n);
}
*/
		//
		// ************************
		//
/*
z.a[z.a.length] = '<select '; 
z.a[z.a.length] = 'onchange="alert('; 
z.a[z.a.length] = 'this.selectedIndex'; 
//z.a[z.a.length] = 'this.value'; 
z.a[z.a.length] = ')">'; 
//
z.a[z.a.length] = '<option value="?">?</option>';
z.a[z.a.length] = '<option value="a0">a</option>';
z.a[z.a.length] = '<option value="b0">b</option>';
z.a[z.a.length] = '<option value="c0">c</option>';
//
z.a[z.a.length] = '</select>';
*/
//console.log("t.tb.rf="+t.tb.rf)
//
		if(t.tb.hd0) // header div 0 = custom
		{
			//t.tb.hd0 = [{ty:"[HEADERHD0]",id0:"hd0",tx0:t.tb.iHeader+t.tb.iInfo}]; // footer
			//
			for(var i=0;i<t.tb.hd0.length;i++) // many custom headers hd0
			{
				z.a[z.a.length] = '<div id="'+t.tb.hd0[i].id0+'" class="" >'; 
				z.a[z.a.length] = t.tb.hd0[i].tx0; 
				z.a[z.a.length] = '</div>'; 
			}
		}
		//
		if(t.tb.hg.pg.hx) // show "Grid Name / Text" header
		{
			z.a[z.a.length] = '<table id="i_hgg0_tbhx_'+t.tb.rf+t.tb.id+'" class="'+z.cs+'hx" width="'+t.tb.hg.pg.css.sz+'" style="'; 
			z.a[z.a.length] = 'font-size:'+t.tb.hg.pg.css.fs; 
			z.a[z.a.length] = ';font-family:'+t.tb.hg.pg.css.fa; 
			//z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
			//z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
			z.a[z.a.length] = ';">'; 
				z.a[z.a.length] = '<thead id="i_hgg0_theadhx_'+t.tb.rf+t.tb.id+'">'; // TODO(2012-08-24):Corrigir ID - ADICIONAR INSTANCIA = NOME do grid
					z.a[z.a.length] = '<tr class="'+z.cs+'hx'+'">';
						//				
						z.a[z.a.length] = '<th id="i_hgg0_thhx_'+t.tb.rf+t.tb.id+'" style="'; 
						z.a[z.a.length] = 'text-align:' + t.tb.hg.pg.hx[0].al; 
						z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
						//
						z.a[z.a.length] = '"><em id="i_hggo_emhx_'+t.tb.rf+t.tb.id+'" style="';
						z.a[z.a.length] = 'color:'+t.tb.hg.pg.hx[0].c;
						z.a[z.a.length] = ';background:'+t.tb.hg.pg.hx[0].bg; 
						z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
						z.a[z.a.length] = ';">'+t.tb.hg.pg.hx[0].tx+'</em>'; 
						z.a[z.a.length] = "</th>"; 
						//
						//z.a[z.a.length] = '<th>'+t.tb.hg.pg.hx+'</th>';
					z.a[z.a.length] = '</tr>';
				z.a[z.a.length] = '</thead>';
			z.a[z.a.length] = '</table>';
		}
		//
		// ************************
		//
		z.a[z.a.length] = '<table id="i_hgg0_tb01_'+t.tb.rf+t.tb.id+'" class="'+z.cs+'" width="'+t.tb.hg.pg.css.sz+'" style="'; 
		z.a[z.a.length] = 'font-size:'+t.tb.hg.pg.css.fs; 
		z.a[z.a.length] = ';font-family:'+t.tb.hg.pg.css.fa; 
		z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
		z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
		z.a[z.a.length] = ';">'; 
		//
		// TODO(2012-08-24):Corrigir ID - este ID é usado internamente neste arquivo
		// precisar se parametro para funcionar 2+ grids na mesma tela
		//
		// ************************
		//
		if(z.h0==1 || z.h1==1 || z.h2==1 || z.h3==1 || z.h4==1) // show "field names" header = DEFAULT
		{
			z.a[z.a.length] = '<thead id="i_hgg0_thead01_'+t.tb.rf+t.tb.id+'">'; // TODO(2012-08-24):Corrigir ID
			//
			if(z.h4==1) // TODO if needed
			{
			}
			//
			if(z.h3==1) // TODO if needed
			{
			}
			//
			if(z.h2==1) // "control fields"
			{
				z.a[z.a.length] = '<tr class="'+z.cs+'h2'+'">';
				//
				z.k1 = new Array(); // fields to create <select>
				//
				for(var k in t.tb.hg.pg.fk)
				{
//console.log("k: "+k);
					z.k1[z.k1.length] = k; // f00,f01,etc 
				}
				//
				if(z.k1.length>0) // show header with filter fields
				{
					z.k2 = z.k1.join(",");
//console.log("z.k1.length: "+z.k1.length);
//console.log("z.k2: "+z.k2);
//console.log("z.fd.length: "+z.fd.length);
					//
					for(var n=0;n<z.fd.length;n++) // walk through fields to display
					{
						if(z.k2.indexOf(z.fd[n])>=0) // show fields with filter
						{
							z.fkn = t.tb.hg.pg.fk[z.fd[n]];
							//
							// **** NEW parameters pattern (ty="[XPTO]")
							//
							if(z.fkn[0] && z.fkn[0].ty=="[FONTSIZE]") // Setting Control for font size in css
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgfs_(t,z.fkn) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[FONTFAMILY]") // Setting Control for font family in css
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgfa_(t,z.fkn) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[FONTSTYLE]") // Setting Control for font family in css
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgfy_(t,z.fkn) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[FONTWEIGHT]") // Setting Control for font family in css
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgfw_(t,z.fkn) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[PAGELINES]") // Setting Control for number of records per page
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgpl_(t,z.fkn) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[PAGING]") // Setting Control for navigation = Paging buttons
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgpg_(t) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[TABLEINFO]") // table information button
							{
								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgti_(t) + "</th>"; 
							}
							else if(z.fkn[0] && z.fkn[0].ty=="[COMPLEXFILTER]") // Setting Control for complex filter
							{
								//z.a[z.a.length] = "<th>" + ALiBS.hg._hgcf_(t,z.fkn) + "</th>"; 

								z.a[z.a.length] = "<th>" + '<em style="color:'+z.fkn[0].c;
								z.a[z.a.length] = ';background:'+z.fkn[0].bg+'">'+z.fkn[0].tx+' </em>'; 
								z.a[z.a.length] = ALiBS.hg._hgcf_(t,z.fkn) + "</th>"; 
							}
							//
							// **** OLD parameters pattern - compatibility
							//
							else if(z.fkn=="[PAGING]") // paging button
							{
								z.a[z.a.length] = "<th>" + ALiBS.hg._hgpg_(t) + "</th>"; 
							}
							else if(z.fkn=="[TABLEINFO]") // table information button
							{
								z.a[z.a.length] = "<th>" + ALiBS.hg._hgti_(t) + "</th>"; 
							}
							//
							// **** OLD parameters pattern - compatibility
							//
							else if(z.fkn[0] && z.fkn[0].t) // complex filter
							{
								z.a[z.a.length] = "<th>" + ALiBS.hg._hgcf_(t,z.fkn) + "</th>"; 
							}
							//
							// ****
							//
							else 
							{
								z.a[z.a.length] = '<th></th>'; // show fields without filter
							}
							//z.a[z.a.length] = "<th>" + t.tb.hg.pg.fk[z.fd[n]] + "</th>";
						}
						else
						{
							z.a[z.a.length] = '<th></th>'; // show fields without filter
						}
					}
				}
				//
				z.a[z.a.length] = '</tr>';
			}
			//
//console.log("z.h1: "+z.h1)
			if(z.h1==1) // "fields filter"
			{
				z.a[z.a.length] = '<tr class="'+z.cs+'h1'+'">';
				//
				z.k1 = new Array(); // fields to create <select>
				//
				for(var k in t.tb.hg.pg.fi)
				{
					z.k1[z.k1.length] = k; // f00,f01,etc 
				}
				//
				if(z.k1.length>0) // show header with filter fields
				{
					z.k2 = z.k1.join(",");
					//
					for(var n=0;n<z.fd.length;n++) // walk through fields to display
					{
						if(z.k2.indexOf(z.fd[n])>=0) // show fields with filter
						{
							z.k0 = t.tb.hg.pg.fi[z.fd[n]].split(","); // all, opt1, opt2, etc
							//
							z.a[z.a.length] = '<th>';
							//
							z.a[z.a.length] = '<select ';
							z.a[z.a.length] = 'onchange="'+t.tb.rf+'.ff.hg.fc(';
							z.a[z.a.length] = t.tb.rf;
							z.a[z.a.length] = ",'"+z.fd[n]+"'";
							z.a[z.a.length] = ',this.selectedIndex';
							z.a[z.a.length] = ');">';
							//
							for(var l=0;l<z.k0.length;l++) // loop values
							{
								if(typeof(t.tb.hg.pg.fv[z.fd[n]])!="undefined" && t.tb.hg.pg.fv[z.fd[n]]==l)
								{
//console.log("(1)"+z.fd[n]+"="+t.tb.hg.pg.fv[z.fd[n]])
//console.log("(2)l="+l)
									 z.a[z.a.length] = '<option selected value="'+z.k0[l]+'">'+z.k0[l]+'</option>';
								}
								else z.a[z.a.length] = '<option>'+z.k0[l]+'</option>';
							}
							//
							z.a[z.a.length] = '</select>';
							z.a[z.a.length] = '</th>';
						}
						else
						{
							z.a[z.a.length] = '<th></th>'; // show fields without filter
						}
					}
				}
				//
				z.a[z.a.length] = '</tr>';
			}
			//
			if(z.h0==1) // "field names"
			{
				//
				ALiBS.hg._g0h0_(t,z);
			}
			//
			z.a[z.a.length] = '</thead>';
		}
		//
		// ************************
		//
		z.a[z.a.length] = '<tbody id="i_hgg0_Tbdy_'+t.tb.rf+t.tb.id+'">'; // TODO(2012-08-24):Corrigir ID
		//
		for(var j=0;j<t.rs.length;j++) // records
		{
			if(t.tb.fa!="" && t.ft[t.tb.fa]) // filter
			{
				z.r0 = t.ft[t.tb.fa](t,j);
				//
				if(z.r0>0) z.r0; // {} // OK
				else if(z.r0==0) continue; // ALiBS.to.f0 and ALiBS.to.f1
				else if(z.r0==-1) continue; // field filter
				else if(z.r0==-2) break;
			}
			//
			if(t.tb.hg.pg.c!=null) // paging ON
			{
				if(j<t.tb.hg.pg.c[t.tb.hg.pg.p].v0) continue;
				else if(j>t.tb.hg.pg.c[t.tb.hg.pg.p].v1) break;
				//
//console.log("p:"+t.tb.hg.pg.p+",  n:"+t.tb.hg.pg.n+", v0:"+t.tb.hg.pg.c[t.tb.hg.pg.p].v0+",  v1:"+t.tb.hg.pg.c[t.tb.hg.pg.p].v1+",  j:"+j);
			}
			//
//console.log("(antes)z.h0="+z.h0);
			if(z.h0==1) // show field header = DEFAULT
			{
//console.log("(depois)z.h0="+z.h0);
//console.log("\n(depois)j="+j);
//console.log("(depois)z.ph="+z.ph);
//console.log("(depois)z.phx="+z.phx);
//console.log("(depois)z.ajc="+z.ajc);
//console.log("(depois)z.aj="+z.aj);

				// reprint header
				//if(z.ph>0 && j>z.ph-1 && j%(z.ph)==0)
				if(j==z.phx) // z.phx = NEXT line(j) to break
				{
//console.log("(depois)z.ph="+z.ph);
//console.log("(depois)z.phx="+z.phx);
					z.phx = j+z.ph; // NEXT line(j) to break
					//
					z.a[z.a.length] = '</tbody></table>';
					//
					if(t.tb.fp) // show "page/total" footer
					{
						ALiBS.hg._g0fp_(t,z,j);
					}
					//
					if(t.tb.fp1) // fp1 footer
					{
						ALiBS.hg._g0fp1_(t,z,j);
					}
					//
					z.ajm = Math.abs(z.aj);
					//
					if( z.ajc>=z.ajm ) // prints for each "aj" records
					//if( (j/(z.ph))%z.aj==0  ) // prints for each "aj" records
					{
						if(z.aj>0)
						{
							z.phx += 1; // NEXT line(j) to break
						}
						else if(z.aj<0)
						{
							z.phx -= 1; // NEXT line(j) to break
						}
						else // ==0
						{
							// nothing
						}
						//
						z.ajc = 0;
						//
						//if(z.aja>=0) z.a[z.a.length] = '<p style="width:10px;height:'+z.aja+'px;background-color:red"></p>';
//console.log((j/(z.ph))%z.aj+":div"); // aj = 1 cai sempre aqui
					}
					else // prints if NOT aj (most records)
					{
						z.ajc++;
						//
						//if(z.ajb>=0) z.a[z.a.length] = '<p style="width:10px;height:'+z.ajb+'px;background-color:yellow"></p>';
//console.log((j/(z.ph))%z.aj+":br+div"); // aj = 0 cai sempre aqui
					}
					//
					//z.a[z.a.length] = '<p style="page-break-after: always;width:10px;height:'+z.aja+'px;background-color:red"></p>';
					z.a[z.a.length] = '<p style="page-break-after: always;width:10px;height:'+z.aja+'px;background-color:white"></p>';
					//
					// *****************
					//
					if(t.tb.hd0) // header div 0 = custom
					{
						//t.tb.hd0 = [{ty:"[HEADERHD0]",id0:"hd0",tx0:t.tb.iHeader+t.tb.iInfo}]; // footer
						//
						for(var i=0;i<t.tb.hd0.length;i++) // many custom headers hd0
						{
							z.a[z.a.length] = '<div id="'+t.tb.hd0[i].id0+'" class="" >'; 
							z.a[z.a.length] = t.tb.hd0[i].tx0; 
							z.a[z.a.length] = '</div>'; 
						}
					}
					//
					// *****************
					//
					z.a[z.a.length] = '<table id="i_hgg0_tb01_'+t.tb.rf+t.tb.id+'" class="'+z.cs+'" width="'+t.tb.hg.pg.css.sz+'" style="'; 
					z.a[z.a.length] = 'font-size:'+t.tb.hg.pg.css.fs; 
					z.a[z.a.length] = ';font-family:'+t.tb.hg.pg.css.fa; 
					z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
					z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
					z.a[z.a.length] = ';">'; 
					//
					z.a[z.a.length] = '<thead id="i_hgg0_Tbdy_'+t.tb.rf+t.tb.id+'">';
					//
					ALiBS.hg._g0h0_(t,z);
					//
					z.a[z.a.length] = '</thead>';
					//
					z.a[z.a.length] = '<tbody id="i_hgg0_Tbdy_'+t.tb.rf+t.tb.id+'">'; // TODO(2012-08-24):Corrigir ID
				}
			}
			//
			if(t.rs[j]["ck00"])
			{
				z.a[z.a.length] = '<tr class="'+z.cs+t.rs[j]["ck00"]+'">';
			}
			else if(z.cc)
			{
				z.a[z.a.length] = '<tr class="'+z.c0+'">';
			}
			else
			{
				z.a[z.a.length] = '<tr class="'+z.c1+'">';
			}
			//
			z.cc = !z.cc;
			//
			for(var i=0;i<z.fd.length;i++) // show fields 
			{
				z.y0 = z.fd[i].charAt(0);
				z.g0 = parseInt(z.fd[i].substring(1,3),10);
				//
//console.log(z.fd[i])
//console.log(z.g0)
				//
				if(!t.tb.hg.ty[z.fd[i]]) // if NOT exists ty.f00, ty.f01,
				{
					z.hfxx = t.rs[j]["h"+z.fd[i]]; // USE value in hf00, hf01,
				}
				else if(!t.tb.hg.ty[z.fd[i]].ty) // if NOT exists ty.f00.ty, ty.f01.ty,
				{
					z.hfxx = t.rs[j]["h"+z.fd[i]]; // USE value in hf00, hf01,
				}
				//
				// ********
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1000) // NOT checked (does NOT read/save field value)(only records in screen)
				{
					z.hfxx = '<input type="checkbox" />'; 
				}
				else if(t.tb.hg.ty[z.fd[i]].ty==1001) // CHECKED (does NOT read/save field value)(only records in screen)
				{
					z.hfxx = '<input type="checkbox" checked/>'; 
				}
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1050) // READ value from field
				{
					if(!t.rs[j][z.fd[i]]) // NO value
					{
						z.hfxx = '<input type="checkbox"/>'; 
					}
					else // ANY value
					{
						z.hfxx = '<input type="checkbox" checked/>'; 
					}
				}
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1059) // CUSTOM READ from field
				{
					z.hfxx = ''; // TODO:(2013-01-16)
				}
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1099) // CUSTOM checkbox
				{
					z.hfxx = ''; // TODO:(2013-01-16) 
				}
				//
				// ********
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1100) // DELETE (x) image
				{
					z.hfxx = '<img src="'+ALiBS.gv.hg.g0_imgx+'" />'; 
				}
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1101) // EDIT (x) image
				{
					z.hfxx = '<img src="'+ALiBS.gv.hg.g0_imge+'" />'; 
				}
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==1199) // CUSTOM image
				{
					z.hfxx = '<img src="'+t.tb.hg.ty[z.fd[i]].img+'" />'; 
				}
				//
				// ********
				//
				else if(t.tb.hg.ty[z.fd[i]].ty==9999) // CUSTOM html
				{
					z.hfxx = ''; // TODO:(2013-01-16) 
				}
				//
				// ********
				//
				else // DEFAULT
				{
					z.hfxx = t.rs[j]["h"+z.fd[i]]; // DEFAULT = USE value in hf00, hf01,
				}
				//
				//
				//z.hfxx = t.rs[j]["h"+z.fd[i]];
				//
				if(!z.hfxx) // hf00 is undefined
				{
					z.cfxx = t.rs[j]["c"+z.fd[i]];
					//
					z.n0 = t.rs[j][z.fd[i]];
					if(!z.n0 && z.n0!=0) z.n0 = "";
					//
					if(!z.cfxx) // cf00 is undefined
					{
						z.a[z.a.length] = '<td onclick="'+t.tb.rf+'.ff.hg.ff('+t.tb.rf+','+j+','+z.g0+",'"+z.y0+"'"+');">'+z.n0+'</td>';
					}
					else // cf00 is OK
					{
						z.a[z.a.length] = '<td onclick="'+t.tb.rf+'.ff.hg.ff('+t.tb.rf+','+j+','+z.g0+",'"+z.y0+"'"+');">'+
									'<em style="background:'+z.cfxx+';">'+z.n0+'</em></td>';
					}
				}
				else // hf00 is OK
				{
					z.a[z.a.length] = '<td onclick="'+t.tb.rf+'.ff.hg.ff('+t.tb.rf+','+j+','+z.g0+",'"+z.y0+"'"+');">'+z.hfxx+'</td>';
				}
			}
			//
			z.a[z.a.length] = '</tr>';
		}
		//
		// ************************
		//
		for(var j=0;j<z.el;j++) // empty lines at the end
		{
			z.a[z.a.length] = '<tr class="'+z.c2+'">';
			//
			//for(var i=0;i<t.tb.nf;i++) // fields
			for(var i=0;i<z.fd.length;i++) // show fields 
			{
				z.a[z.a.length] = '<td>'+'.'+'</td>'; // TODO(2012-08-01): usar . ou espaço?
			}
			//
			z.a[z.a.length] = '</tr>';
		}
		//
		z.a[z.a.length] = '</tbody></table>';
		//
		if(t.tb.fp) // show "page/total" footer
		{
			ALiBS.hg._g0fp_(t,z,t.rs.length);
		}
		//
		if(t.tb.fp1) // fp1 footer
		{
			ALiBS.hg._g0fp1_(t,z,j);
		}
		//
		var r = z.a.join(""); // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_g0h0_: function(t,z) // g0 header
	// t = (required)(ALiBS.to)table
	// z = (required)(JSON)z variables
	//
	// return = None
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._g0h0_",le:ALiBS.gv.hg.le});
		//
		z.a[z.a.length] = '<tr class="'+z.cs+'h0'+'">';
		//
		// inverter classes
		z.m = z.c0;
		z.c0 = z.c1;
		z.c1 = z.m;
		//
		for(var i=0;i<z.fd.length;i++) // fields TODO(mostrar os campos abcdefghijkrx):
		{
//console.log("1")
			if(typeof(t.ax[z.fd[i]])=="undefined") // normal index = field itself
			{
				z.y0 = z.fd[i].charAt(0); // letter of "f00"
				z.g0 = parseInt(z.fd[i].substring(1,3),10); // number of "f00"
			}
			else // auxiliar index = x00,x01,etc
			{
//console.log("2")
				z.y0 = t.ax[z.fd[i]].charAt(0); // letter of "f00"
				z.g0 = parseInt(t.ax[z.fd[i]].substring(1,3),10); // number of "f00"
			}
			//
//console.log(z.fd[i])
//console.log(z.y0)
//console.log(z.g0)
			//
			z.n0 = t.cp[z.fd[i]];
			if(!z.n0) z.n0 = t.nm[z.fd[i]];
			if(!z.n0) z.n0 = z.fd[i];
			//
//console.log("i="+i);
//console.log("z.fd["+i+"]="+z.fd[i]);
			//
			// ****************
			//
			if(typeof(t.tb.hg.tt)=="number") // value is set on table
			{
				if(t.tb.hg.tt==0) // 0 = do NOT add title+fieldCode automatically
				{
					z.tt = t.tt[z.fd[i]] || ""; // field title 
				}
				else if(t.tb.hg.tt==1) // 1 = add title+fieldCode to the end
				{
					if(!t.tt[z.fd[i]])
					{
						z.tt = "[" + z.fd[i] + "]";
					}
					else
					{
						z.tt = t.tt[z.fd[i]] + " [" + z.fd[i] + "]"; // field title 
					}
				}
				else if(t.tb.hg.tt==2) // 2 = add fieldCode+title to the beginning 
				{
					if(!t.tt[z.fd[i]])
					{
						z.tt = "[" + z.fd[i] + "]";
					}
					else
					{
						z.tt = "[" + z.fd[i] + "] " + t.tt[z.fd[i]]; // field title 
					}
				}
			}
			else // // value is NOT set on table = use DEFAULT value
			{
				if(!ALiBS.gv.hg.tt) // 0 = do NOT add title+fieldCode automatically
				{
					z.tt = t.tt[z.fd[i]] || ""; // field title 
				}
				else if(ALiBS.gv.hg.tt==1) // 1 = add title+fieldCode to the end
				{
					if(!t.tt[z.fd[i]])
					{
						z.tt = "[" + z.fd[i] + "]";
					}
					else
					{
						z.tt = t.tt[z.fd[i]] + " [" + z.fd[i] + "]"; // field title 
					}
				}
				else if(ALiBS.gv.hg.tt==2) // 2 = add fieldCode+title to the beginning 
				{
					if(!t.tt[z.fd[i]])
					{
						z.tt = "[" + z.fd[i] + "]";
					}
					else
					{
						z.tt = "[" + z.fd[i] + "] " + t.tt[z.fd[i]]; // field title 
					}
				}
			}
			//
			// ****************
			//
			if(t.tb.hg.pg.nso.indexOf(z.fd[i])>=0) // do not sort
			{
				z.a[z.a.length] = '<th class="" title="'+z.tt+'" style="background:gray">'+z.n0+'</th>';
			}
			else if(z.g0==t.so.g && z.y0==t.so.y) // sorted field
			{
				if(!t.so.rv) // NOT reverse sort = ascendent sort = increasing
				{
					if(ALiBS.gv.hg.g0_imga)
					{
						z.a[z.a.length] = '<th class="'+z.cs+'A" title="'+z.tt+'" ';
						z.a[z.a.length] = 'onclick="'+t.tb.rf+'.ff.hg.dg('+t.tb.rf+','+z.g0+",{y:'"+z.y0+"'"+'});">';
						z.a[z.a.length] = '<img src="'+ALiBS.gv.hg.g0_imga+'"></img>'+' '+z.n0+'</th>';
					}
					else
					{
						z.a[z.a.length] = '<th class="'+z.cs+'A" title="'+z.tt+'" ';
						z.a[z.a.length] = 'onclick="'+t.tb.rf+'.ff.hg.dg('+t.tb.rf+','+z.g0+",{y:'"+z.y0+"'"+'});">'+z.n0+'</th>';
					}
				}
				else // reverse = decreasing
				{
					if(ALiBS.gv.hg.g0_imgd)
					{
						z.a[z.a.length] = '<th class="'+z.cs+'D" title="'+z.tt+'" ';
						z.a[z.a.length] = 'onclick="'+t.tb.rf+'.ff.hg.dg('+t.tb.rf+','+z.g0+",{y:'"+z.y0+"'"+'});">';
						z.a[z.a.length] = '<img src="'+ALiBS.gv.hg.g0_imgd+'"></img>'+' '+z.n0+'</th>';
					}
					else
					{
						z.a[z.a.length] = '<th class="'+z.cs+'D" title="'+z.tt+'" ';
						z.a[z.a.length] = 'onclick="'+t.tb.rf+'.ff.hg.dg('+t.tb.rf+','+z.g0+",{y:'"+z.y0+"'"+'});">'+z.n0+'</th>';
					}
				}
			}
			else // no img = NOT sorted field
			{
				z.a[z.a.length] = '<th class="" title="'+z.tt+'" ';
				z.a[z.a.length] = 'onclick="'+t.tb.rf+'.ff.hg.dg('+t.tb.rf+','+z.g0+",{y:'"+z.y0+"'"+'});">'+z.n0+'</th>';
			}
		}
		//
		z.a[z.a.length] = '</tr>';
		//
		return;
	},
	// *****************************************************************
	_g0pn_: function(le,ph0,ph) // page number calculation
	// le = (number)t.rs.length
	// ph0 = (number)z.ph0 = first page number of records
	// ph = (number)z.ph = other pages number of records
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._g0pn_",le:ALiBS.gv.hg.le});
		//
		var r = 1; // le<=ph0
		//
		if(le>ph0)
		{
			r = 1;
			//
			le = le-ph0;
			//
			r += Math.floor((le)/(ph)); // cp mixes le/ph with RegExp
			if(le%ph>0) r++;
		}
		//
		return r;
	},
	// *****************************************************************
	_g0fp_: function(t,z,j) // fp footer page/total
	// t = (required)(ALiBS.to)table
	// z = (required)(JSON)z variables
	//
	// return = None
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._g0fp_",le:ALiBS.gv.hg.le});
		//
		if(!z.pga) // page number
		{
			z.pga=1;
			//
			if(z.ph0) 
			{
				z.pgt = ALiBS.hg._g0pn_(t.rs.length,z.ph0,z.ph); // page total
			}
			else
			{
				z.pgt = ALiBS.hg._g0pn_(t.rs.length,z.ph,z.ph); // page total
			}
		}
		else z.pga++;
		//
		z.a[z.a.length] = '<table id="i_hgg0_tbfp_'+t.tb.rf+t.tb.id+'" class="'+z.cs+'fp" width="'+t.tb.hg.pg.css.sz+'" style="'; 
		z.a[z.a.length] = 'font-size:'+t.tb.hg.pg.css.fs; 
		z.a[z.a.length] = ';font-family:'+t.tb.hg.pg.css.fa; 
		//z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
		//z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
		z.a[z.a.length] = ';">'; 
			z.a[z.a.length] = '<tfoot id="i_hgg0_theadfp_'+t.tb.rf+t.tb.id+'">'; // TODO(2012-08-24):Corrigir ID - ADICIONAR INSTANCIA = NOME do grid
				z.a[z.a.length] = '<tr class="'+z.cs+'fp'+'">';
					//				
					z.a[z.a.length] = '<th id="i_hgg0_thfp_'+t.tb.rf+t.tb.id+'" style="'; 
					z.a[z.a.length] = 'text-align:' + t.tb.fp[0].al; 
					z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
					//
					z.a[z.a.length] = ';"><em id="i_hggo_emfp_'+t.tb.rf+t.tb.id+'" style="';
					z.a[z.a.length] = 'color:'+t.tb.fp[0].c;
					z.a[z.a.length] = ';background:'+t.tb.fp[0].bg; 
					z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
					z.a[z.a.length] = ';">'; 
					z.a[z.a.length] = t.tb.fp[0].tx0+z.pga; 
					z.a[z.a.length] = t.tb.fp[0].tx1; //+"xpto2"; // only to change footer for tests
					z.a[z.a.length] = t.tb.fp[0].tx2+z.pgt; 
					z.a[z.a.length] = "</em></th>"; 
					//
				z.a[z.a.length] = '</tr>';
			z.a[z.a.length] = '</tfoot>';
		z.a[z.a.length] = '</table>';
		//
		return;
	},
	// *****************************************************************
	_g0fp1_: function(t,z,j) // fp1 footer 
	// t = (required)(ALiBS.to)table
	// z = (required)(JSON)z variables
	//
	// return = None
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._g0fp1_",le:ALiBS.gv.hg.le});
		//
		z.a[z.a.length] = '<table id="i_hgg0_tbfp1_'+t.tb.rf+t.tb.id+'" class="'+z.cs+'fp" width="'+t.tb.hg.pg.css.sz+'" style="'; 
		z.a[z.a.length] = 'font-size:'+t.tb.hg.pg.css.fs; 
		z.a[z.a.length] = ';font-family:'+t.tb.hg.pg.css.fa; 
		//z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
		//z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
		z.a[z.a.length] = ';">'; 
			z.a[z.a.length] = '<tfoot id="i_hgg0_theadfp1_'+t.tb.rf+t.tb.id+'">'; // TODO(2012-08-24):Corrigir ID - ADICIONAR INSTANCIA = NOME do grid
				z.a[z.a.length] = '<tr class="'+z.cs+'fp'+'">';
					//				
					z.a[z.a.length] = '<th id="i_hgg0_thfp1_'+t.tb.rf+t.tb.id+'" style="'; 
					z.a[z.a.length] = 'text-align:' + t.tb.fp1[0].al; 
					z.a[z.a.length] = ';background:'+t.tb.fp1[0].bg; 
					z.a[z.a.length] = ';font-weight:'+t.tb.hg.pg.css.fw; 
					//
					z.a[z.a.length] = ';"><em id="i_hggo_emfp1_'+t.tb.rf+t.tb.id+'" style="';
					z.a[z.a.length] = 'color:'+t.tb.fp1[0].c;
					z.a[z.a.length] = ';background:'+t.tb.fp1[0].bg; 
					z.a[z.a.length] = ';font-style:'+t.tb.hg.pg.css.fy; 
					z.a[z.a.length] = ';">'; 
					z.a[z.a.length] = t.tb.fp1[0].tx0; 
					//z.a[z.a.length] = t.tb.fp1[0].tx0+z.pga; 
					//z.a[z.a.length] = t.tb.fp1[0].tx1; //+"xpto2"; // only to change footer for tests
					//z.a[z.a.length] = t.tb.fp1[0].tx2+z.pgt; 
					z.a[z.a.length] = "</em></th>"; 
					//
				z.a[z.a.length] = '</tr>';
			z.a[z.a.length] = '</tfoot>';
		z.a[z.a.length] = '</table>';
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgti_: function(t) // table information button
	// t = (required)(ALiBS.to)table
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgti_",le:ALiBS.gv.hg.le});
		//
		var r = new Array();
		//
		r[r.length] = '<input type="button" value="[INFO]" onclick="alert(';
		r[r.length] = 'ALiBS.hg._tidt_(' + t.tb.rf + ')';
		r[r.length] = ')"/>';
		//		
		return r.join("");
	},
	// *****************************************************************
	_tidt_: function(t) // table information data component
	// t = (required)(ALiBS.to)table
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._tidt_",le:ALiBS.gv.hg.le});
		//
		var r = new Array();
		//
		// TODO: i18n
		r[r.length] = "number of records in table: "+t.rs.length;
		r[r.length] = "number of records in filter: "+t.tb.hg.pg.t;
		r[r.length] = "max records per page: "+t.tb.hg.pg.n;
		//
		r[r.length] = "\n";
		//
		r[r.length] = "number of pages in filter: "+t.tb.hg.pg.c.length;
		r[r.length] = "th"+"is page is number: "+(t.tb.hg.pg.p); // (t.tb.hg.pg.p+1)
		r[r.length] = "number of records in th"+"is page: "+t.tb.hg.pg.c[t.tb.hg.pg.p].n;
		//
		return r.join("\n");
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgpg_: function(t) // paging buttons component
	// t = (required)(ALiBS.to)table
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgpg_",le:ALiBS.gv.hg.le});
		//
		var r = new Array();
		//
		r[r.length] = '<input type="button" value="|<<" ';
		r[r.length] = 'onclick="ALiBS.hg._pgck_(';
		r[r.length] = t.tb.rf; // table reference
		r[r.length] = ',0);" ';
		r[r.length] = 'title="';
		r[r.length] = 0;
		r[r.length] = '" />';
		//
		r[r.length] = '<input type="button" value="|<" ';
		r[r.length] = 'onclick="ALiBS.hg._pgck_(';
		r[r.length] = t.tb.rf; // table reference
		r[r.length] = ',' + t.tb.rf+'.tb.hg.pg.p-1);" ';
		r[r.length] = 'title="';
		r[r.length] = (t.tb.hg.pg.p<=0)?0:(t.tb.hg.pg.p-1);
		r[r.length] = '" />';
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._pgck_('; 
		r[r.length] = t.tb.rf; 
		r[r.length] = ',' + t.tb.rf+'.tb.hg.pg.p+parseInt(this.value,10));" ';
		r[r.length] = ');">'; 
		//
		if(t.tb.hg.pg.c.length>=5000) r[r.length] = '<option value="-5000">-5000</option>';
		if(t.tb.hg.pg.c.length>=1000) r[r.length] = '<option value="-1000">-1000</option>';
		if(t.tb.hg.pg.c.length>=500) r[r.length] = '<option value="-500">-500</option>';
		if(t.tb.hg.pg.c.length>=100) r[r.length] = '<option value="-100">-100</option>';
		if(t.tb.hg.pg.c.length>=50) r[r.length] = '<option value="-50">-50</option>';
		if(t.tb.hg.pg.c.length>=10) r[r.length] = '<option value="-10">-10</option>';
		r[r.length] = '<option selected value="'+t.tb.hg.pg.p+'">'+t.tb.hg.pg.p+'</option>';
		if(t.tb.hg.pg.c.length>=10) r[r.length] = '<option value="+10">+10</option>';
		if(t.tb.hg.pg.c.length>=50) r[r.length] = '<option value="+50">+50</option>';
		if(t.tb.hg.pg.c.length>=100) r[r.length] = '<option value="+100">+100</option>';
		if(t.tb.hg.pg.c.length>=500) r[r.length] = '<option value="+500">+500</option>';
		if(t.tb.hg.pg.c.length>=1000) r[r.length] = '<option value="+1000">+1000</option>';
		if(t.tb.hg.pg.c.length>=5000) r[r.length] = '<option value="+5000">+5000</option>';
		//
		r[r.length] = '</select>';
		//
		r[r.length] = '<input type="button" value=">|" ';
		r[r.length] = 'onclick="ALiBS.hg._pgck_(';
		r[r.length] = t.tb.rf; // table reference
		r[r.length] = ',' + t.tb.rf+'.tb.hg.pg.p+1);" ';
		r[r.length] = 'title="';
		r[r.length] = (t.tb.hg.pg.p==t.tb.hg.pg.c.length-1)?t.tb.hg.pg.p:(t.tb.hg.pg.p+1);
		r[r.length] = '" />';
		//
		r[r.length] = '<input type="button" value=">>|" ';
		r[r.length] = 'onclick="ALiBS.hg._pgck_(';
		r[r.length] = t.tb.rf; // table reference
		r[r.length] = ',' + t.tb.rf+'.tb.hg.pg.c.length);" ';
		r[r.length] = 'title="';
		r[r.length] = (t.tb.hg.pg.c.length-1);
		r[r.length] = '" />';
		//		
		return r.join("");
	},
	// *****************************************************************
	_pgck_: function(t,v) // "complex filter" click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._pgck_",le:ALiBS.gv.hg.le});
		//
		t.tb.hg.pg.p = v; // set page
		//
		t.ff.hg.dg(t,-1);
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgcf_: function(t,o) // "complex filter" select component
	// t = (required)(ALiBS.to)table
	// o = (required)(Array)options [{t:"(a00>=1)",v:"fc;(a00>=1)"}]
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgcf_",le:ALiBS.gv.hg.le});
		//
		var r = new Array();
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._cfck_(';
		r[r.length] = t.tb.rf;
		r[r.length] = ",this.value";
		r[r.length] = ",this.selectedIndex";
		r[r.length] = ');">';
		//
		for(var i=0;i<o.length;i++)
		{
			if(i==t.tb.hg.pg.fv["cf"])
			{
				r[r.length] = '<option selected value="'+o[i].v+'">'+o[i].t+'</option>';
			}
			else
			{
				r[r.length] = '<option value="'+o[i].v+'">'+o[i].t+'</option>';
			}
		}
		//
		r[r.length] = '</select>';
		//
		return r.join("");
	},
	// *****************************************************************
	_cfck_: function(t,v,i) // "complex filter" click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option value
	// i = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._cfck_",le:ALiBS.gv.hg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.fv["cf"] = i; // (selectedIndex of "complex filter <select>")
		//
		z.vn = v.split(";");
		//
		t.tb.fa=z.vn[0];
		t.tb.fc=z.vn[1];
		//
		if(t.ff.hg._cfck_f0)
		{
//console.log("(ALiBS.hg)passou ff.hg._hgfc_f0");
			//
			t.ff.hg._cfck_f0(t,v,i);
		}
		//
		t.ff.hg.dg(t,-1);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgfc_: function(t,f,v) // "field filter" header click
	// t = (required)(ALiBS.to)table
	// f = (required)(String)field name (f00,f01,etc)
	// v = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgfc_",le:ALiBS.gv.hg.le});
		//
		t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.fv[f] = v;
		//
		if(t.ff.hg._hgfc_f0)
		{
//console.log("(ALiBS.hg)passou ff.hg._hgfc_f0");
			//
			t.ff.hg._hgfc_f0(t,f,v);
		}
		//
		t.ff.hg.dg(t,-1); 
		//
		return;
	},
	// *****************************************************************
	_hgff_: function(t,r,n,y) // field click
	// t = (required)(ALiBS.to)table
	// r = (required)(number)record number/position
	// n = (required)(number)field number (0-99)
	// y = (required)(String)field type (a,b,c,d,e,f,g,h,i,j,k,r,x)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgff_",le:ALiBS.gv.hg.le});
		//
		if(n<10) n = "0"+n;
		else n = ""+n;
		//
		alert(y+n+":"+t.rs[r][y+n]+"\n"+"record:("+r+")\nf00="+t.rs[r]["f00"]);
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgpl_: function(t,o) // page lines
	// t = (required)(ALiBS.to)table
	// o = (required)(Array)options [{t:"(a00>=1)",v:"fc;(a00>=1)"}]
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgpl_",le:ALiBS.gv.hg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		var r = new Array();
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._plck_(';
		r[r.length] = t.tb.rf;
		r[r.length] = ",this.value";
		r[r.length] = ",this.selectedIndex";
		r[r.length] = ');">';
		//
		z.vn = o[0].l.split(",");
		//
		for(var i=0;i<z.vn.length;i++)
		{

			if(z.vn[i]==t.tb.hg.pg.n)
			{
				r[r.length] = '<option selected value="'+t.tb.hg.pg.n+'">'+t.tb.hg.pg.n+'</option>';
			}
			else
			{
				r[r.length] = '<option value="'+z.vn[i]+'">'+z.vn[i]+'</option>';
			}
		}
		//
		r[r.length] = '</select>';
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r.join("");
	},
	// *****************************************************************
	_plck_: function(t,v,i) // page lines click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option value
	// i = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._plck_",le:ALiBS.gv.hg.le});
		//
		t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.n = v;
		//
		t.ff.hg.dg(t,-1);
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgfs_: function(t,o) // font size
	// t = (required)(ALiBS.to)table
	// o = (required)(Array)options [{t:"(a00>=1)",v:"fc;(a00>=1)"}]
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgfs_",le:ALiBS.gv.hg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		var r = new Array();
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._fsck_(';
		r[r.length] = t.tb.rf;
		r[r.length] = ",this.value";
		r[r.length] = ",this.selectedIndex";
		r[r.length] = ');">';
		//
		z.vn = o[0].l.split(",");
		//
		for(var i=0;i<z.vn.length;i++)
		{
			if(z.vn[i]==t.tb.hg.pg.css.fs)
			{
				r[r.length] = '<option selected value="'+t.tb.hg.pg.css.fs+'">'+t.tb.hg.pg.css.fs+'</option>';
			}
			else
			{
				r[r.length] = '<option value="'+z.vn[i]+'">'+z.vn[i]+'</option>';
			}
		}
		//
		r[r.length] = '</select>';
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r.join("");
	},
	// *****************************************************************
	_fsck_: function(t,v,i) // font size click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option value
	// i = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._fsck_",le:ALiBS.gv.hg.le});
		//
		//t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.css.fs = v;
		//
		var r = ALiBS.el.ge("i_hgg0_tb01_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
		ALiBS.st.st(r,"fontSize",v); // set style
		//
		if(t.tb.hg.pg.css.hx)
		{
			r = ALiBS.el.ge("i_hgg0_tbhx_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
			ALiBS.st.st(r,"fontSize",v); // set style
		}
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgfa_: function(t,o) // font family
	// t = (required)(ALiBS.to)table
	// o = (required)(Array)options [{t:"(a00>=1)",v:"fc;(a00>=1)"}]
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgfa_",le:ALiBS.gv.hg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		var r = new Array();
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._fack_(';
		r[r.length] = t.tb.rf;
		r[r.length] = ",this.value";
		r[r.length] = ",this.selectedIndex";
		r[r.length] = ');">';
		//
		z.vn = o[0].l.split(",");
		//
		for(var i=0;i<z.vn.length;i++)
		{
			if(z.vn[i]==t.tb.hg.pg.css.fa)
			{
				r[r.length] = '<option selected value="'+t.tb.hg.pg.css.fa+'">'+t.tb.hg.pg.css.fa+'</option>';
			}
			else
			{
				r[r.length] = '<option value="'+z.vn[i]+'">'+z.vn[i]+'</option>';
			}
		}
		//
		r[r.length] = '</select>';
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r.join("");
	},
	// *****************************************************************
	_fack_: function(t,v,i) // font family click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option value
	// i = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._fack_",le:ALiBS.gv.hg.le});
		//
		//t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.css.fa = v;
		//
		var r = ALiBS.el.ge("i_hgg0_tb01_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
		ALiBS.st.st(r,"fontFamily",v); // set style
		//
		if(t.tb.hg.pg.css.hx)
		{
			r = ALiBS.el.ge("i_hgg0_tbhx_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
			ALiBS.st.st(r,"fontFamily",v); // set style
		}
		//
		return;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgfy_: function(t,o) // font style
	// t = (required)(ALiBS.to)table
	// o = (required)(Array)options [{t:"(a00>=1)",v:"fc;(a00>=1)"}]
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgfy_",le:ALiBS.gv.hg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		var r = new Array();
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._fyck_(';
		r[r.length] = t.tb.rf;
		r[r.length] = ",this.value";
		r[r.length] = ",this.selectedIndex";
		r[r.length] = ');">';
		//
		z.vn = o[0].l.split(",");
		//
		for(var i=0;i<z.vn.length;i++)
		{
			if(z.vn[i]==t.tb.hg.pg.css.fy)
			{
				r[r.length] = '<option selected value="'+t.tb.hg.pg.css.fy+'">'+t.tb.hg.pg.css.fy+'</option>';
			}
			else
			{
				r[r.length] = '<option value="'+z.vn[i]+'">'+z.vn[i]+'</option>';
			}
		}
		//
		r[r.length] = '</select>';
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r.join("");
	},
	// *****************************************************************
	_fyck_: function(t,v,i) // font style click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option value
	// i = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._fyck_",le:ALiBS.gv.hg.le});
		//
		//t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.css.fy = v;
		//
		var r = ALiBS.el.ge("i_hgg0_tb01_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
		ALiBS.st.st(r,"fontStyle",v); // set style
		//
		if(t.tb.hg.pg.css.hx)
		{
			r = ALiBS.el.ge("i_hggo_emhx_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
			ALiBS.st.st(r,"fontStyle",v); // set style
		}
		//
		return;
	},
	// *****************************************************************	
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_hgfw_: function(t,o) // font weight
	// t = (required)(ALiBS.to)table
	// o = (required)(Array)options [{t:"(a00>=1)",v:"fc;(a00>=1)"}]
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._hgfw_",le:ALiBS.gv.hg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		var r = new Array();
		//
		r[r.length] = '<select '; 
		r[r.length] = 'onchange="ALiBS.hg._fwck_(';
		r[r.length] = t.tb.rf;
		r[r.length] = ",this.value";
		r[r.length] = ",this.selectedIndex";
		r[r.length] = ');">';
		//
		z.vn = o[0].l.split(",");
		//
		for(var i=0;i<z.vn.length;i++)
		{
			if(z.vn[i]==t.tb.hg.pg.css.fw)
			{
				r[r.length] = '<option selected value="'+t.tb.hg.pg.css.fw+'">'+t.tb.hg.pg.css.fw+'</option>';
			}
			else
			{
				r[r.length] = '<option value="'+z.vn[i]+'">'+z.vn[i]+'</option>';
			}
		}
		//
		r[r.length] = '</select>';
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r.join("");
	},
	// *****************************************************************
	_fwck_: function(t,v,i) // font weight click
	// t = (required)(ALiBS.to)table
	// v = (required)(number)option value
	// i = (required)(number)option index
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hg._fwck_",le:ALiBS.gv.hg.le});
		//
		//t.tb.hg.pg.c = null; // paging cache should be set [NULL] every time filter changes
		//
		t.tb.hg.pg.css.fw = v;
		//
		var r = ALiBS.el.ge("i_hgg0_tb01_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
		ALiBS.st.st(r,"fontWeight",v); // set style
		//
		if(t.tb.hg.pg.css.hx)
		{
			r = ALiBS.el.ge("i_hgg0_thhx_"+t.tb.rf+t.tb.id,{c:false}); // DO NOT reuse cache = element / table was recreated
			ALiBS.st.st(r,"fontWeight",v); // set style
		}
		//
		return;
	}
	// *****************************************************************
};
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.hg("+(++ALiBS.gv.hg.nn)+"x)",{o:"ALiBS.hg"});
ALiBS.sy.la(0+0,"ALiBS.gv.hg.rl="+ALiBS.gv.hg.rl,{o:"ALiBS.hg"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.hg.le="+ALiBS.gv.hg.le+ALiBS.gv.sy.se,{o:"ALiBS.hg"});

