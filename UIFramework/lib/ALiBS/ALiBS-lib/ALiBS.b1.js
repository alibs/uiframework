/* *************************************************************************
// INFO:

https://github.com/google/google-authenticator

https://tools.ietf.org/html/rfc4226

https://tools.ietf.org/html/rfc6238

https://support.google.com/accounts/answer/185839?hl=en&guide=1056283&rd=1


// *************************************************************************
// TODO:


*/
//
// *************************************************************************
//
if(!ALiBS.gv.b1) ALiBS.gv.b1 = {}; // global values
//
if(!ALiBS.gv.b1.le) ALiBS.gv.b1.le = 0; // local log status
//
ALiBS.gv.b1.nn = 0; // x times loaded
//
ALiBS.gv.b1.rl = "15.0803.1103";
//
// *************************************************************************
//
ALiBS.b1 = // OTP = One Time Password = Token
{
	g0: function(sk,p)
	/** 
	Method <static> <JSON> ALiBS.b1.g0(sk,p)

		Description: Get OTP 

		Parameters:

			sk = (required)(string) secret key

			p = (optional)(JSON)parameter {} 

				ts = (number) timestamp
					(new Date())-1+1 = DEFAULT = (new Date()).getTime()

				it = (number) interval (secs) for new OTP
					30 = DEFAULT

				lc = (number)return previous codes
					0 = DEFAULT = NO last codes
					[1-9] = last codes

				nc = (number)return previous codes
					0 = DEFAULT = NO next codes
					[1-9] = next codes

				rt = (number)return type
					0 = DEFAULT = include ts,t0,t1
					1 = only

		Observations:
	
		Return Value:
			<static> <JSON> OTP

		Example(s):
			var otp = ALiBS.b1.g0("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567");

			var otp = ALiBS.b1.g0("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",{it:20});

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

		sk = (String) secret key
			// usar secret key teste: 
				// ABCDEFGHIJKLMNOPQRSTUVWXYZ234567
				// 765432ZYXWVUTSRQPONMLKJIHGFEDCBA   reverse

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(sk)!="string") throw "ALiBS.b1.g0.Exception: Error: Invalid Parameter Type (<string> sk="+typeof(sk)+")"; // 
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_ts = z.ts; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_it = z.it; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_rt = z.rt; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_lc = z.lc; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_nc = z.nc; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_ts)!="number") z.p_ts = (new Date())-1+1;
			if(typeof(z.p_it)!="number") z.p_it = 30;
			if(typeof(z.p_rt)!="number") z.p_rt = 0;
			if(typeof(z.p_lc)!="number") z.p_lc = 0;
			if(typeof(z.p_nc)!="number") z.p_nc = 0;
			//
			ALiBS.sy.la(5+0,"z.p_ts="+z.p_ts,{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
			ALiBS.sy.la(5+0,"z.p_it="+z.p_it,{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
			ALiBS.sy.la(5+0,"z.p_rt="+z.p_rt,{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
			ALiBS.sy.la(5+0,"z.p_lc="+z.p_lc,{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
			ALiBS.sy.la(5+0,"z.p_nc="+z.p_nc,{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_t0 = Math.floor(z.p_ts/1000); // 
			z.v_t1 = Math.floor(z.v_t0/z.p_it); // 
			//
			// ************************
			//
			z.ec = -1600; // 
			z.v_r0 = {};
			z.v_r0.cd = "" + ALiBS.b1.__g0(sk,z.v_t1);
			//
			// ************************
			//
			z.ec = -1610; // 
			for(var i=1;i<=z.p_lc;i++)
			{
				z.v_r0["lc"+i] = "" + ALiBS.b1.__g0(sk,z.v_t1-i); // last OTPs
			}
			//
			z.ec = -1620; // 
			for(var i=1;i<=z.p_nc;i++)
			{
				z.v_r0["nc"+i] = "" + ALiBS.b1.__g0(sk,z.v_t1+i); // next OTPs
			}
			//
			// ************************
			//
			z.ec = -1630; // 
			if(z.p_rt==0)
			{
				z.v_r0.ts = z.p_ts;
				z.v_r0.t0 = z.v_t0;
				z.v_r0.t1 = z.v_t1;
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_r0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.b1.g0",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		//ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.b1.g0",le:ALiBS.gv.b1.le});
		//
		return r; 
	},
	// *****************************************************************
	__g0: function(sk,ts,p) // Given a secret key "K" and a timestamp "t" (in 30s units since the beginning of the epoch), return a TOTP code.
	/** 
	Method <static> <string> ALiBS.b1.__g0(sk,ts,p)

		Description: (Auxiliar) Generates OTP

		Parameters:

			sk = (required)(string) secret key

			ts = (required)(number) timestamp
				(new Date())-1+1 = DEFAULT = (new Date()).getTime()

			p = (optional)(JSON)parameter {} 

		Observations:
	
		Return Value:
			<static> <string> OTP

		Example(s):
			var otp = ALiBS.b1.__g0("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",(new Date()-1+1));

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.b1.__g0",le:ALiBS.gv.b1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(sk)!="string") throw "ALiBS.b1.__g0.Exception: Error: Invalid Parameter Type: (string) sk passed as "+typeof(sk); // 
			if(typeof(ts)!="number") throw "ALiBS.b1.__g0.Exception: Error: Invalid Parameter Type: (number) ts passed as "+typeof(ts); // 
			//
			ALiBS.sy.la(5+0,"sk="+sk,{o:"ALiBS.b1.__g0",le:ALiBS.gv.b1.le});
			ALiBS.sy.la(5+0,"ts="+ts,{o:"ALiBS.b1.__g0",le:ALiBS.gv.b1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_k0 = [];
			z.v_l0 = [];
			z.v_j0 = 0;
			z.v_c0 = 0;
			//
			// ************************
			//
			for(var i=0;i<sk.length;)
			{
				z.v_c0 =z.v_c0*32+'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'.indexOf(sk.charAt(i++).toUpperCase());
				//
				if((z.v_j0+=5)>31)
				{
					z.v_k0.push(Math.floor(z.v_c0/(1<<(z.v_j0-=32))));
					z.v_c0&=31;
				}
			}
			//
			z.v_j0&&z.v_k0.push(z.v_c0<<(32-z.v_j0));
			//
			for(var i=0;i<16;++i)
			{
				z.v_l0.push(0x6A6A6A6A^ ( z.v_k0[i]=z.v_k0[i]^0x5C5C5C5C ) );
			}
			//
			z.v_s0 = ALiBS.b1.sh(z.v_k0.concat(ALiBS.b1.sh(z.v_l0.concat([0,ts]))));
			z.v_o0 = z.v_s0[4]&0xF;
			//
			z.v_r0 = "" + (    (  z.v_s0[z.v_o0>>2]<<8*(z.v_o0&3) | (z.v_o0&3?z.v_s0[(z.v_o0>>2)+1]>>>8*(4-z.v_o0&3):0)  )&-1>>>1    )%1000000;
			//
			z.v_rr = new Array();
			//
			for(var i=z.v_r0.length;i<6;i++)
			{
				z.v_rr[z.v_rr.length] = "0";
			}
			//
			//if(z.v_r0.length<6 || z.v_r0.charAt(0)==" ") z.v_r0 = "0" + z.v_r0.replace(/\s/,"");
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_rr.join("") + z.v_r0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.b1.__g0",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.b1.__g0",le:ALiBS.gv.b1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.b1.__g0",le:ALiBS.gv.b1.le});
		//
		return r; 
	},
	// *****************************************************************
	sh: function(a0,p) // SHA1
	/** 
	Method <static> <array> ALiBS.b1.sh(a0,p)

		Description: SHA1

		Parameters:

			a0 = (required)(array) array

			p = (optional)(JSON)parameter {} 

		Observations:
	
		Return Value:
			<static> <array> OTP

		Example(s):
			var sh = ALiBS.b1.sh(a0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.b1.sh",le:ALiBS.gv.b1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(a0)!="object-Array") throw "ALiBS.b1.sh.Exception: Error: Invalid Parameter Type: (object-Array) a0 passed as "+ALiBS.sy.ty(a0); // 
			//
			ALiBS.sy.la(5+0,"a0="+a0,{o:"ALiBS.b1.sh",le:ALiBS.gv.b1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_d0 = a0.concat([1<<31]);
			z.v_vv = {};
			z.v_vv.v0 = 0x67452301;
			z.v_vv.w0 = 0x88888888;
			z.v_vv.y0 = 271733878;
			z.v_vv.x0 = z.v_vv.y0^z.v_vv.w0;
			z.v_vv.z0 = 0xC3D2E1F0;
			z.v_vv.w0 ^= z.v_vv.v0;
			//
			// ************************
			//
			do
			{
				z.v_d0.push(0);
			}
			while(z.v_d0.length+1&15);
			//
			z.v_d0.push(32*a0.length);
			//
			z.v_w0 = {};
			//
			while(z.v_d0.length)
			{
				z.v_w0.E = z.v_d0.splice(0,16);
				z.v_w0.a = z.v_vv.v0;
				z.v_w0.b = z.v_vv.w0;
				z.v_w0.c = z.v_vv.x0;
				z.v_w0.d = z.v_vv.y0;
				z.v_w0.e = z.v_vv.z0;
				z.v_w0.f;
				z.v_w0.k;
				//
				for(var i=12;++i<77;)
				{
					z.v_w0.E.push( ALiBS.b1.__g0b(z.v_w0.E[i]^z.v_w0.E[i-5]^z.v_w0.E[i-11]^z.v_w0.E[i-13],1) );
				}
				//
				z.v_w0.k = 0x5A827999;
				//
				for(var i=0;i<20;ALiBS.b1.__g0c(i++,z))
				{
					z.v_w0.f = z.v_w0.b&z.v_w0.c|~z.v_w0.b&z.v_w0.d;
				}
				//
				z.v_w0.k = 0x6ED9EBA1;
				//
				for(;i<40;ALiBS.b1.__g0c(i++,z))
				{
					z.v_w0.f = z.v_w0.b^z.v_w0.c^z.v_w0.d;
				}
				//
				z.v_w0.k = 0x8F1BBCDC;
				//
				for(;i<60;ALiBS.b1.__g0c(i++,z))
				{
					z.v_w0.f = z.v_w0.b&z.v_w0.c|z.v_w0.b&z.v_w0.d|z.v_w0.c&z.v_w0.d;
				}
				//
				z.v_w0.k = 0xCA62C1D6;
				//
				for(;i<80;ALiBS.b1.__g0c(i++,z))
				{
					z.v_w0.f = z.v_w0.b^z.v_w0.c^z.v_w0.d;
				}
				//
				z.v_vv.v0 += z.v_w0.a;
				z.v_vv.w0 += z.v_w0.b;
				z.v_vv.x0 += z.v_w0.c;
				z.v_vv.y0 += z.v_w0.d;
				z.v_vv.z0 += z.v_w0.e;
			}
			//
			z.v_r0 = [z.v_vv.v0,z.v_vv.w0,z.v_vv.x0,z.v_vv.y0,z.v_vv.z0];
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_r0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.b1.sh",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.b1.sh",le:ALiBS.gv.b1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.b1.sh",le:ALiBS.gv.b1.le});
		//
		return r; 
	},
	// *****************************************************************
	__g0b: function(x0,b0,p) // L(x,b)
	/** 
	Method <static> <string> ALiBS.b1.__g0(x0,b0,p)

		Description: (Auxiliar) 

		Parameters:

			x0 = (required)(number) 

			b0 = (required)(number)

			p = (optional)(JSON)parameter {} 

		Observations:
	
		Return Value:
			<static> <string> 

		Example(s):
			var otp = ALiBS.b1.__g0(x0,b0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.b1.__g0b",le:ALiBS.gv.b1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(x0)!="number") throw "ALiBS.b1.__g0b.Exception: Error: Invalid Parameter Type: (number) x0 passed as "+typeof(x0); // 
			if(typeof(b0)!="number") throw "ALiBS.b1.__g0b.Exception: Error: Invalid Parameter Type: (number) b0 passed as "+typeof(b0); // 
			//
			ALiBS.sy.la(5+0,"x0="+x0,{o:"ALiBS.b1.__g0b",le:ALiBS.gv.b1.le});
			ALiBS.sy.la(5+0,"b0="+b0,{o:"ALiBS.b1.__g0b",le:ALiBS.gv.b1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_v0 = x0<<b0|x0>>>32-b0;
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_v0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.b1.__g0b",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.b1.__g0b",le:ALiBS.gv.b1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.b1.__g0b",le:ALiBS.gv.b1.le});
		//
		return r; 
	},
	// *****************************************************************
	__g0c: function(x0,p) // I(x,z)
	/** 
	Method <static> <string> ALiBS.b1.__g0(x0,b0,p)

		Description: (Auxiliar) 

		Parameters:

			x0 = (required)(number) 

			p = (optional)(JSON)parameter {} 

		Observations:
	
		Return Value:
			<static> <string> 

		Example(s):
			var otp = ALiBS.b1.__g0(x0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.b1.__g0c",le:ALiBS.gv.b1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(x0)!="number") throw "ALiBS.b1.__g0c.Exception: Error: Invalid Parameter Type: (number) x0 passed as "+typeof(x0); // 
			//
			ALiBS.sy.la(5+0,"x0="+x0,{o:"ALiBS.b1.__g0c",le:ALiBS.gv.b1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_w0 = z.v_w0; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			z.p_w0.t0 = ALiBS.b1.__g0b(z.p_w0.a,5)+z.p_w0.f+z.p_w0.e+z.p_w0.k+z.p_w0.E[x0];
			z.p_w0.e = z.p_w0.d;
			z.p_w0.d = z.p_w0.c;
			z.p_w0.c = ALiBS.b1.__g0b(z.p_w0.b,30);
			z.p_w0.b = z.p_w0.a;
			z.p_w0.a = z.p_w0.t0;
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = ""; // z.v0; // undefined ? where is z.v0 ?
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.b1.__g0c",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.b1.__g0c",le:ALiBS.gv.b1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.b1.__g0c",le:ALiBS.gv.b1.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.b1("+(++ALiBS.gv.b1.nn)+"x)",{o:"ALiBS.b1"});
ALiBS.sy.la(0+0,"ALiBS.gv.b1.rl="+ALiBS.gv.b1.rl,{o:"ALiBS.b1"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.b1.le="+ALiBS.gv.b1.le+ALiBS.gv.sy.se,{o:"ALiBS.b1"});

