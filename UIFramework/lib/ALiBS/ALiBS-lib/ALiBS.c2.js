/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

1) change var r to string buffer or Array

*/
//
// *************************************************************************
//
if(!ALiBS.gv.c2) ALiBS.gv.c2 = {}; // global values
//
if(!ALiBS.gv.c2.le) ALiBS.gv.c2.le = 0; // local log status
//
ALiBS.gv.c2.nn = 0; // x times loaded
//
ALiBS.gv.c2.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.c2.rl = "15.0629.1007";
//
// *************************************************************************
//
ALiBS.c2 = // string to number array 
{
	sa: function(s0,p) // 
	/** 
	Method <static> <array> ALiBS.c2.sa(p)

		Description: String to Number Array

		Parameters:
			s0 = (required)(string) string

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number-array> Array

		Example(s):
			var ok = ALiBS.c2.sa(str);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c2.sa",le:ALiBS.gv.c2.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "ALiBS.c2.sa.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//if(typeof(z.us)!="string") throw "ALiBS.c2.sa.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			//if(typeof(z.pw)!="string") throw "ALiBS.c2.sa.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.c2.sa",le:ALiBS.gv.c2.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//if(typeof(z.ab)!="string" || z.ab=="*") z.ab = "*";
			//
			/*if(typeof(z.fd)!="string" || z.fd=="*")
			{
				z.fd = ALiBS.to.fdn(t0); // get ALL defined field names = DEFAULT = "*"
			}
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}*/
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.c2.sa",le:ALiBS.gv.c2.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			z.a0 = new Array();
			// 
			// ************************
			//
			z.ec = -1200; // 
			if(s0)
			{
				s0 = "" + s0;
				//
				z.ec = -1300; // 
				for(var i=0;i<s0.length;i++)
				{
					z.ec = -1310; // 
					z.a0[z.a0.length] = s0.charCodeAt(i);
					//
					ALiBS.sy.la(7+0,"z.a0[z.i]="+z.a0[i],{o:"ALiBS.c2.sa",le:ALiBS.gv.c2.le});
				}
			}
			//
			// ************************
			//
			z.ec = -9000; // -9902, -9903, etc
			//if(i>1000) throw "ALiBS.c2.sa.Exception: Error: Infinite-Loop (i="+i+")"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			// ************************
			//
			z.ec = -9800; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.c2.sa.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			r = z.a0; // [BEST-PRACTICE]
			//
			//r = "" + z.r; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c2.sa",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c2.sa",le:ALiBS.gv.c2.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c2.sa",le:ALiBS.gv.c2.le});
		//
		return r; 
	},
	// *****************************************************************
	as: function(a0,p) // decimal array to string
	// return = string ascii 
	/** 
	Method <static> <number-array> ALiBS.c2.as(p)

		Description: Number Array to String

		Parameters:
			a0 = (required)(number-array) srray

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number-array> Array

		Example(s):
			var ok = ALiBS.c2.as(a0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c2.as",le:ALiBS.gv.c2.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "ALiBS.c2.as.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//if(typeof(z.us)!="string") throw "ALiBS.c2.as.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			//if(typeof(z.pw)!="string") throw "ALiBS.c2.as.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.c2.as",le:ALiBS.gv.c2.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//if(typeof(z.ab)!="string" || z.ab=="*") z.ab = "*";
			//
			/*if(typeof(z.fd)!="string" || z.fd=="*")
			{
				z.fd = ALiBS.to.fdn(t0); // get ALL defined field names = DEFAULT = "*"
			}
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}*/
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.c2.as",le:ALiBS.gv.c2.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			z.r0 = new Array();
			//
			// ************************
			//
			z.ec = -1200; // 
			for(var i=0;i<a0.length;i++)
			{
				z.ec = -1210; // 
				z.r0[z.r0.length] = String.fromCharCode(a0[i]);
			}
			//
			// ************************
			//
			z.ec = -9000; // -9902, -9903, etc
			//if(i>1000) throw "ALiBS.c2.as.Exception: Error: Infinite-Loop (i="+i+")"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			// ************************
			//
			z.ec = -9800; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.c2.as.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			r = "" + z.r0.join(""); // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c2.as",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c2.as",le:ALiBS.gv.c2.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c2.as",le:ALiBS.gv.c2.le});
		//
		return r; 
	},
	// *****************************************************************
	ae: function(a0,a1,p) //
	/** 
	Method <static> <number> ALiBS.c2.ae(p)

		Description: Array equals Array

		Parameters:
			a0 = (required)(array) array 0
			a1 = (required)(array) array 1

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number> position where array is different
				or
			-1 case is equal

		Example(s):
			var ok = ALiBS.c2.ae(a0,a1);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c2.ae",le:ALiBS.gv.c2.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = -1; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "ALiBS.c2.ae.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.c2.ae",le:ALiBS.gv.c2.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//if(typeof(z.fd)!="string" || z.fd=="*") fd = 
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.c2.ae",le:ALiBS.gv.c2.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			//z.a0 = new Array();
			//
			// ************************
			//
			for(var i=0;i<a0.length;i++)
			{
				if(typeof(a1[i])=="undefined" || a1[i]==null)
				{
					r = i;
					//
					break;
				}
				else
				{
					if(a0[i]==a1[i])
					{
						// continue;
					}
					else // different
					{
						r = i;
						//
						break;
					}
				}
			}
			//
			// ************************
			//
			z.ec = -9000; // -9902, -9903, etc
			//if(i>1000) throw "ALiBS.c2.ae.Exception: Error: Infinite-Loop (i="+i+")"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			// ************************
			//
			z.ec = -9800; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.c2.ae.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			//r = z.a.join(""); // [BEST-PRACTICE]
			//
			//r = "" + z.r; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c2.ae",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c2.ae",le:ALiBS.gv.c2.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c2.ae",le:ALiBS.gv.c2.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c2("+(++ALiBS.gv.c2.nn)+"x)",{o:"ALiBS.c2"});
ALiBS.sy.la(0+0,"ALiBS.gv.c2.le="+ALiBS.gv.c2.le+ALiBS.gv.sy.se,{o:"ALiBS.c2"});

