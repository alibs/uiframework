// *************************************************************************
//
var TS0000c00 = new ALiBS.to.t({tb_rf:"TS0000c00"}); 
//
// *************************************************************************
//
TS0000c00.nm.f21 = "TableCode";
TS0000c00.cp.f21 = "TableCode";
TS0000c00.tt.f21 = "TableTitle";
TS0000c00.dc.f21 = "Table Code/Real Name";
TS0000c00.dv.f21 = "FIXED:TableTitle";
TS0000c00.ob.f21 = "Physical Table Name";
//
//TS0000c00.nm.f22 = "FieldCode";
//TS0000c00.cp.f22 = "FieldCode";
//
TS0000c00.nm.f23 = "Description";
TS0000c00.cp.f23 = "Description";
//
//TS0000c00.nm.f24 = "Sample";
//TS0000c00.cp.f24 = "Sample";
//
//TS0000c00.nm.f25 = "DEFINED"; // DEFINED = 1
//TS0000c00.cp.f25 = "DEFINED";
//
TS0000c00.nm.f26 = "Version"; // Field Version (Table Version = Field Version +Alto)
TS0000c00.cp.f26 = "Version";
//
TS0000c00.nm.f27 = "TableName";
TS0000c00.cp.f27 = "TableName";
//
TS0000c00.nm.f28 = "TableCaption";
TS0000c00.cp.f28 = "TableCaption";
//
TS0000c00.nm.f29 = "TableTitle";
TS0000c00.cp.f29 = "TableTitle";
//
TS0000c00.nm.f30 = "TableGroup"; // Agrupamento de tabelas
TS0000c00.cp.f30 = "TableGroup";
//
//TS0000c00.nm.f31 = "Defaultvalue"; // Default Value
//TS0000c00.cp.f31 = "Default Value";
//
TS0000c00.nm.f99 = "OBS"; // Obs
TS0000c00.cp.f99 = "OBS";
//
// *************************************************************************
//
TS0000c00.tb.pg.n = 31; // paging = number of records to show (0 = show all)
//
TS0000c00.tb.sh = ["f30","f21","f27","f28","f29","f23","f26"];
//
TS0000c00.tb.fk.f21 = "[TABLEINFO]"; // table information button
TS0000c00.tb.fk.f23 = [{t:"Todos",v:"x;x"},{t:"fm:match",v:"fm;x"},{t:"fe:equals",v:"fe;x"},{t:"res>=1",v:"fc;(a00>=1)"},{t:"res>=3",v:"fc;(a00>=3)"}]; // complex filter
TS0000c00.tb.fk.f24 = "[PAGING]"; // paging button
//
TS0000c00.tb.fc = '(a00>=1)'; // complex filter string
TS0000c00.tb.fa = "fm"; // active filter
//
ALiBS.gv.hg.tt = 1; // field title
//
// *************************************************************************
TS0000c00.ff.hg.dg = function(t,n,p) // display GRID
// t = ()table
// n = ()sorted field
// p = (optional)(JSON)parameter {}
//	rv = (boolean)reverse sort (default=false)
//	y = (String)field type [abcdefghijkrx] (default=f)
{
	var z = p || {}; // [BEST-PRACTICE]
	//
	if(n==t.so.g) ALiBS.to.o(t,n,{rv:!t.so.rv,y:z.y}); // reverse
	else ALiBS.to.o(t,n,{y:z.y}); // normal sort
	//
	var a = ALiBS.hg.g0(t,"nome"); // 4101, font-size: 16px; padding:2px;
	//
	//var a = ALiBS.hg.g0(t,"nome",{cs:""}); // 4101, font-size: 16px; padding:2px;
	//
	ALiBS.el.si("iContGrid02a",a);
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
};
// *************************************************************************
TS0000c00.ff.hg.af = function(t) // after load = 
// t = (ALiBS.to)
{
	var z = {}; // [BEST-PRACTICE]
	//
	// ********************************
	//
	t.ff.hg.rc(t); // insert own structure into table data
	//
	ALiBS.to.nf(t); // Normalise Fields
	//
	// ********************************
	//
	for(var i=0;i<t.rs.length;i++)
	{
		if(t.rs[i].f99)
		{
			t.rs[i]["hf23"] = '<em title="'+ t.rs[i].f99 +'" style="background:yellow;">'+ t.rs[i].f23 +'</em>'; // write obs = f99
		}
	}
	//
	// ********************************
	//
	ALiBS.to.o(t,21,{y:"f"});
	t.tb.fi["f21"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:21,ke:2}).join(",");
	//
	ALiBS.to.o(t,23,{y:"f"});
	t.tb.fi["f23"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:23,ke:2}).join(",");
	//
	//ALiBS.to.o(t,24,{y:"f"});
	//t.tb.fi["f24"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:24,ke:2}).join(",");
	//
	ALiBS.to.o(t,25,{y:"f"});
	t.tb.fi["f25"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:25,ke:2}).join(",");
	//
	ALiBS.to.o(t,26,{y:"f"});
	t.tb.fi["f26"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:26,ke:2}).join(",");
	//
	ALiBS.to.o(t,30,{y:"f"});
	t.tb.fi["f30"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:30,ke:2}).join(",");
	//
	// ********************************
	//
	for(var i=0;i<t.rs.length;i++) // auxiliar index
	{
		t.rs[i]["e01"] = t.rs[i].f21 + "-" + t.rs[i].f22; 
	}
	//
	t.ax.f21 = "e01";
	//
	ALiBS.to.o(t,1,{y:"e"});
	//
	// ********************************
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
	//
	//return r; 
};
// *************************************************************************
TS0000c00.ff.hg.rc = function(t) // record create = from table info
// t = (ALiBS.to)
{
	//var z = {}; // [BEST-PRACTICE]
	//
	// ********************************
	//
	for(var i in t.nm)
	{
		t.rs[t.rs.length] = 
				{
					f00:0,
					f21:t.tb.rf, // Table Code
					f22:i, // Field Code
					f23:t.dc[i], // Description
					f24:"", // Sample
					f25:"1", // DEFINED
					f26:"", // Version
					f27:t.nm[i], // Field Name
					f28:t.cp[i], // Field Caption
					f29:t.tt[i], // Field Title
					f30:"teste", // Table Group
					f31:t.dv[i], // Default Value
					f99:t.ob[i], // OBS
				};
	}
	//
	// ********************************
	//
	//z = null; // [BEST-PRACTICE]
	//delete z; // [BEST-PRACTICE]
	//
	//return r; 
};
// *************************************************************************

