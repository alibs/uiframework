/* *************************************************************************
// INFO:

- To insert new IDs/elements in cache (create a second ID for the same element)

	ALiBS.gv.el.ec["myID0"] = window.document.body;

	ALiBS.gv.el.ec["myID1"] = ALiBS.el.ge("otherID");

	ALiBS.gv.el.ec["myID2"] = document.AuthenticateForm.username;

	- to get the element from cache
		ALiBS.el.ge("myID0");

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.el) ALiBS.gv.el = {}; // global values
//
if(!ALiBS.gv.el.le) ALiBS.gv.el.le = 0; // local log status
//
ALiBS.gv.el.nn = 0; // x times loaded
//
ALiBS.gv.el.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.el.rl = "14.0911.0001";
//
// *************************************************************************
//
ALiBS.gv.el.ec = {}; // elements cache
//
ALiBS.gv.el.ecf = {}; // elements cache - function
//	ALiBS.gv.el.ecf["EID"] = function(){return document.getElementById("xpto");};
//
ALiBS.gv.el.rc = 1; // reset cache
//	0 = do NOT reset cache
//	1 = DEFAULT = reset cache if window.location changed
//
ALiBS.gv.el.wl = ""; // last window.location.href
//
// *************************************************************************
//
ALiBS.el = // HTML elements control
{
	rc: function() // reset cache
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.rc",le:ALiBS.gv.el.le});
		//
		var w = window.location.href;
		//
		if(ALiBS.gv.el.rc==1 && w!=ALiBS.gv.el.wl) // window.location.href changed
		{
			ALiBS.sy.la(5+0,"window.location.href Old=["+ALiBS.gv.el.wl+"]",{o:"ALiBS.el.rc",le:ALiBS.gv.el.le});
			ALiBS.sy.la(5+0,"window.location.href New=["+w+"]",{o:"ALiBS.el.rc",le:ALiBS.gv.el.le});
			ALiBS.sy.la(5+0,"ALiBS.gv.el.ec (Cache) Reset",{o:"ALiBS.el.rc",le:ALiBS.gv.el.le});
			//alert("Cache Reset");
			//
			ALiBS.gv.el.wl = w; // new window.location.href
			//
			ALiBS.gv.el.ec = null; // reset cache
			ALiBS.gv.el.ec = {};
			//
			ALiBS.gv.el.ecf = null;
			ALiBS.gv.el.ecf = {};
		}
		else
		{
			ALiBS.sy.la(5+0,"ALiBS.gv.el.ec (Cache) NOT Reset",{o:"ALiBS.el.rc",le:ALiBS.gv.el.le});
		}
	},
	// *****************************************************************
	ge: function(i,p) // get element 
	// i = element id
	// p = (optional)(JSON)parameter {c:false}
	//	c = (boolean)use cache (default=true) 
	// return = element (Only One Element)
	{
		// TODO(2011-07-29): MODIFICAR CACHE - vale a pena?
		//
		// ALiBS.gv.el.ec["i."+i] = ... para ids
		// ALiBS.gv.el.ec["t."+i] = ... para tags body,head,etc
		// ALiBS.gv.el.ec["n."+i] = ... para names = implementar busca getElementsByName ???? deveria ???? e se tiver names repetidos?
		// ALiBS.gv.el.ec["c."+i] = ... para class = implementar busca ALiBS.el.ga() ???? deveria ???? e se tiver classes repetidos?
		//
		//
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
		//
		ALiBS.el.rc(); // reset cache
		//
		var e;
		var c = true; // DEFAULT = use cache
		//
		if(p)
		{
			if(typeof(p.c)=="boolean") c = p.c;
		}
		//
		if(c)
		{
			if(ALiBS.gv.el.ecf[i])
			{
				e = ALiBS.gv.el.ecf[i](); // first = functions cache
			}
			//
			if(!e)
			{
				e = ALiBS.gv.el.ec[i]; // second = references cache
			}
		}
		//
		//if(c) e = ALiBS.gv.el.ec[i];
		//
		ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
		//
		if(e) // if id or tagName in cache
		{
			ALiBS.sy.la(5+0,"(if(e))e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
			//
//console.log("if(e)"); // using cache
			return e;
		}
		else if(i=="body") // reserved word, don't use as id
		{
			if(window.document.body)
			{
				e = ALiBS.gv.el.ec[i] = window.document.body;
				//	
				ALiBS.sy.la(5+0,"(window.document.body)e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
				//
				return e;
			}
			else if(window.document.getElementsByTagName)
			{
				e = ALiBS.gv.el.ec[i] = window.document.getElementsByTagName("body").item(0); // window.document.getElementsByTagName('body')[0]; ???
				//	
				ALiBS.sy.la(5+0,"(getElementsByTagName(body))e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
				//
				return e;
			}
			else if(window.document.all)
			{
				e = ALiBS.gv.el.ec[i] = window.document.all.tags("body");
				//	
				ALiBS.sy.la(5+0,"(window.document.all.tags(body))e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
				//
				return e;
			}
		}
		else if(i=="head") // reserved word, don't use as id
		{
			if(window.document.head)
			{
				e = ALiBS.gv.el.ec[i] = window.document.head;
				//	
				ALiBS.sy.la(5+0,"(window.document.head)e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
				//
				return e;
			}
			else if(window.document.getElementsByTagName)
			{
				e = ALiBS.gv.el.ec[i] = window.document.getElementsByTagName("head").item(0); // window.document.getElementsByTagName('head')[0]; ???
				//	
				ALiBS.sy.la(5+0,"(getElementsByTagName(head))e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
				//
				return e;
			}
			else if(window.document.all)
			{
				e = ALiBS.gv.el.ec[i] = window.document.all.tags("head");
				//	
				ALiBS.sy.la(5+0,"(window.document.all.tags(head))e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
				//
				return e;
			}
		}
		else if(window.document.getElementById)
		{
			e = ALiBS.gv.el.ec[i] = window.document.getElementById(i);
			//	
			ALiBS.sy.la(5+0,"(getElementById)e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
			//
//console.log("if(getElementById)"); // NOT using cache
			return e;
		}

/*		pego meta pelo id="iMeta01" ou qualquer outro id
		pego link pelo id="iLink01" ou qualquer outro id
*/
		else if(window.document.all)
		{
			e = ALiBS.gv.el.ec[i] = window.document.all[i];
			//	
			ALiBS.sy.la(5+0,"(window.document.all)e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
			//
			return e;
		}
		else if(window.document.layers)
		{
			e = ALiBS.gv.el.ec[i] = ALiBS.el.__ge(i);
			//	
			ALiBS.sy.la(5+0,"(window.document.layers)e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
			//
			return e;
		}
		else 
		{
			ALiBS.sy.la(5+0,"(else)e="+e,{o:"ALiBS.el.ge",le:ALiBS.gv.el.le});
			//
			return false;
		}
	},
	// *****************************************************************
	__ge: function(i) // window.document.getElementById() - args(id)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.__ge",le:ALiBS.gv.el.le});
		//
		e = window.document.layers[i];
		//
		if(e)
		{
			ALiBS.sy.la(5+0,"e="+e,{o:"ALiBS.el.__ge(if(e))",le:ALiBS.gv.el.le});
			//
			return e;
		}
		else
		{
			ALiBS.sy.la(5+0,"e="+e,{o:"ALiBS.el.__ge(else)",le:ALiBS.gv.el.le});
			//
			for(var x=0, y; !y && x < window.document.layers.length; x++)
			{
				return ALiBS.el.__ge(i,window.document.layers[x].window.document);
				//
				//return ALiBS.el.__ge(i,window.document.layers[x]); // nao seria esta linha? nao foi testado ainda em um layer browser
			}
		}
		//
		ALiBS.sy.la(5+0,"e="+e,{o:"ALiBS.el.__ge(false)",le:ALiBS.gv.el.le});
		//
		return false;
	},
	// *****************************************************************
	gn: function(c,i,p) // getElementsByTagName TODO(2011-08-01):SHOULD BE DEPRECATED - replaced by ga()
	// c = (required)(String)class name (could be substring, empty OR *)
	// i = (required)(String)element id (could be substring, empty OR *)
	// p = (optional)(JSON)parameter {c:false}
	//	d = (window.document)window.document OR iFrame element
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gn",le:ALiBS.gv.el.le});
		//
		var d = window.document;
		//
		if(p)
		{
			if(typeof(p.d)!="undefined") d = p.d;
		}
		//
		return ALiBS.el.ga({i:i,c:c,d:d});
	},
	// *****************************************************************
	ga: function(p) // get elements Array 
	// p = (optional)(JSON)parameter {}
	//	d = (window.document)window.document OR iFrame element
	//	i = (String)element id TODO(2014-03-27):(could be substring, empty OR *)
	//	t = (String)element tag TODO(2014-03-27):(could be substring, empty OR *)
	//	n = (String)element name TODO(2014-03-27):(could be substring, empty OR *)
	//	c = (String)element class TODO(2014-03-27):(could be substring, empty OR *)
	//
	//	TODO(2014-03-27): cc = (boolean)use cache (default=true) - this function uses cache ?
	//
	//	TODO:return = null | (Array)elements 
	{
/*
	criar novo metodo 
	mp: function(p) // elements map
		- mesmos parametros de ga
		- na primeira vez, cria o cache ecx, e retorna os valores solicitados nos paramentros
		- depois nao cria mais o map
		- parametro para resetar o map e recriar - (cuidado para nao ficar resetando e recriando toda vez)

			0 = reset and return null - to NOT need to pass other parameters
			1 = reset and return list

		- usar ALiBS.el.ga({t:"*"}) para capturar todos os elementos da pagina

	TODO(2014-03-27): novo cache
	//
	ecx = new Array(); // elements cache extended
	ecx[ecx.length] = 
	{
		id: "element-id",
		tn: "tag-name",
		nm: "element-name", 
		cn: "class-names", // "c0,c1,c2"
		va: reference =  value = documento.getElementById()
	};

*/
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		//
		//ALiBS.el.rc(); TODO(2011-08-01): Should reset cache here? 
		//TODO(2011-08-01): Should update cache inside this function, with its results?
		//TODO(2011-07-29): adicionar no cache os elementos encontrados????
		//
		var d = window.document;
		var id = ""; // Default = ALL
		var tn = ""; // Default = ALL
		var nm = ""; // Default = ALL
		var cn = ""; // Default = ALL
		//
		if(p)
		{
			if(typeof(p.d)!="undefined") d = p.d;
			//
			//
			if(typeof(p.i)=="string")
			{
				id = p.i;
				ALiBS.sy.la(5+0,"(p)id="+id,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
			//
			if(typeof(p.t)=="string")
			{
				tn= p.t;
				ALiBS.sy.la(5+0,"(p)tag="+tn,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
			//
			if(typeof(p.n)=="string")
			{
				nm = p.n;
				ALiBS.sy.la(5+0,"(p)name="+nm,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
			//
			if(typeof(p.c)=="string")
			{
				cn = p.c;
				ALiBS.sy.la(5+0,"(p)className="+cn,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
		}
		//
		ALiBS.sy.la(5+0,"id="+id,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		ALiBS.sy.la(5+0,"tn="+tn,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		ALiBS.sy.la(5+0,"nm="+nm,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		ALiBS.sy.la(5+0,"cn="+cn,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		//
		var es; // elements
		var an = null; // elements with name
		var ac = null; // elements with className
		//
		// *********************************************************
		//
		if(!id) // element id
		{
			ALiBS.sy.la(7+0,"!id",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			es = null;
		}
		else
		{
			ALiBS.sy.la(7+0,"id="+id,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			var e0 = ALiBS.el.ge(id); // Always the first element case HTML has 2+ equal element ids
			//
			if(e0)
			{
				es = new Array();
				//
				es[es.length] = e0;
			}
			else
			{
				ALiBS.sy.la(7+0,"(id)e0=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
				//
				return null;
			}
		}
		//  
		if(!es)
		{
			ALiBS.sy.la(7+0,"(after-id)es=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"(after-id)es NOT null - length="+es.length,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			for(var j=0;j<es.length;j++)
			{
				ALiBS.sy.la(7+0,"(after-id)es["+j+"]="+es[j][id],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
		}
		//
		// *********************************************************
		//
		if(!tn) // tag name
		{
			ALiBS.sy.la(7+0,"!tn",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"tn="+tn,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			if(!es)
			{
				es = d.getElementsByTagName(tn); // || d.all; // ALL elements in page
				//
				if(!es)
				{
					ALiBS.sy.la(7+0,"(tn)es=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
					//
					return null;
				}
			}
			else // es[0] only 1 element
			{
				if(es[0].localName!=tn)
				{
					ALiBS.sy.la(7+0,"(tn)es[0].localName!=tn",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
					//
					return null;
				}
			}
		}
		//
		if(!es)
		{
			ALiBS.sy.la(7+0,"(after-tn)es=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"(after-tn)es NOT null - length="+es.length,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			for(var j=0;j<es.length;j++)
			{
				ALiBS.sy.la(7+0,"(after-tn)es["+j+"]="+es[j].localName,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
		}
		//
		// *********************************************************
		//
		if(!nm)
		{
			ALiBS.sy.la(7+0,"!nm",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"nm="+nm,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			if(!es)
			{
				es = d.getElementsByName(nm); 
				//
				if(!es)
				{
					ALiBS.sy.la(7+0,"(nm)es=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
					//
					return null;
				}
			}
			else
			{
				an = new Array();
				//
				for(var l=0;l<es.length;l++) 
				{
					var e = es[l]; // element
					var li; // list of names of each element
					//
					ALiBS.sy.la(7+0,"(ALL-n)es["+l+"].getAttribute(name)="+es[l].getAttribute("name"),{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
					//
					if(e.getAttribute("name")!=null)
					{
						li = e.getAttribute("name").split(' ');
						//
						for(var j=0;j<li.length;j++) 
						{
							ALiBS.sy.la(7+0,"(ALL-nm)li["+j+"]="+li[j],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
							//
							if(li[j].indexOf(nm)>=0) 
							{
								ALiBS.sy.la(7+0,"(OK-nm)li["+j+"]="+li[j],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
								//
								an.push(e);
								break;
							}
							else 
							{
								ALiBS.sy.la(7+0,"(NOT-nm)li["+j+"]="+li[j],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
							}
						}
					}
					else
					{
						ALiBS.sy.la(7+0,"e[name]=null ai["+l+"]"+ai[l],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
					}
				}
				//
				if(!an)
				{
					ALiBS.sy.la(7+0,"(nm)an=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
					//
					return null;
				}
				else
				{
					es = an;
				}
			}
		}
		//  
		if(!es)
		{
			ALiBS.sy.la(7+0,"(after-nm)es=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"(after-nm)es NOT null - length="+es.length,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			for(var j=0;j<es.length;j++)
			{
				ALiBS.sy.la(7+0,"(after-nm)es["+j+"]="+es[j].getAttribute("name"),{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
		}
		//
		// *********************************************************
		//
		if(!cn)
		{
			ALiBS.sy.la(7+0,"!cn",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"cn="+cn,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			if(!es)
			{
				es = d.getElementsByTagName("*") || d.all; 
			}
			//
			ac = new Array();
			//
			for(var l=0;l<es.length;l++) 
			{
				var e = es[l]; // element
				var li; // list of classNames of each element
				//
				if(e["className"]!=null)
				{
					li = e["className"].split(' ');
					//
					for(var j=0;j<li.length;j++) 
					{
						ALiBS.sy.la(7+0,"(ALL-cn)li["+j+"]="+li[j],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
						//
						var cn2 = new RegExp(cn,"gi");
						//
						var cn2m = li[j].match(cn2);
						if( cn2m ) 
						//if( li[j].match(cn2) ) 
						//if( cn.test(li[j]) ) 
						//if(li[j].indexOf(cn)>=0) 
						{
							ALiBS.sy.la(7+0,"(OK-cn)li["+j+"]="+li[j],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
							//
							ac.push(e);
							break;
						}
						else 
						{
							ALiBS.sy.la(7+0,"(NOT-cn)li["+j+"]="+li[j],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
						}
					}
				}
			}
			//
			if(!ac)
			{
				ALiBS.sy.la(7+0,"(nm)ac=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
				//
				return null;
			}
			else
			{
				es = ac;
			}
		}
		//
		if(!es)
		{
			ALiBS.sy.la(7+0,"(after-cn)es=null",{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
		}
		else
		{
			ALiBS.sy.la(7+0,"(after-cn)es NOT null - length="+es.length,{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			//
			for(var j=0;j<es.length;j++)
			{
				ALiBS.sy.la(7+0,"(after-cn)es["+j+"]="+es[j]["className"],{o:"ALiBS.el.ga",le:ALiBS.gv.el.le});
			}
		}
		//
		return es;
	},
	// *****************************************************************
	oc: function(i,f) // onclick
	// i = element id
	// f = function
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.oc",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//
		ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.el.oc",le:ALiBS.gv.el.le});
		//
		if(e) e.onclick = f;
	},
	// *****************************************************************
	ov: function(i,f) // onmouseover
	// i = element id
	// f = function
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.ov",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//
		ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.el.ov",le:ALiBS.gv.el.le});
		//
		if(e) e.onmouseover = f;
	},
	// *****************************************************************
	ot: function(i,f) // onmouseout
	// i = element id
	// f = function
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.ot",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//
		ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.el.ot",le:ALiBS.gv.el.le});
		//
		if(e) e.onmouseout = f;
	},
	// *****************************************************************
	sv: function(i,v) // set value
	// i = element id
	// v = value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.sv",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//
		ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.el.sv",le:ALiBS.gv.el.le});
		ALiBS.sy.la(5+0,"v="+v,{o:"ALiBS.el.sv",le:ALiBS.gv.el.le});
		//
		if(e) e.value = v;
	},
	// *****************************************************************
	av: function(i,v) // add value - concat value
	// i = element id
	// v = value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.av",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		if(e) e.value = e.value + v; // concat 
	},
	// *****************************************************************
	arv: function(i,v) // add reverse value - concat reverse value
	// i = element id
	// v = value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.arv",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		if(e) e.value = v + e.value; // concat 
	},
	// *****************************************************************
	gv: function(i) // get value
	// i = element id
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gv",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		if(e) return e.value;
	},
	// *****************************************************************
	gi: function(i) // get innerHTML - <div>, <span>, <a>
	// i = element id
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gi",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		if(e) return e.innerHTML; 
	},
	// *****************************************************************
	si: function(i,v) // set innerHTML - <div>, <span>, <a>
	// i = element id
	// v = value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.si",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//
		ALiBS.sy.la(5+0,"e="+e,{o:"ALiBS.el.si",le:ALiBS.gv.el.le});
		ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.el.si",le:ALiBS.gv.el.le});
		ALiBS.sy.la(5+0,"v="+v,{o:"ALiBS.el.si",le:ALiBS.gv.el.le});
		//
		try
		{
			if(e) e.innerHTML = v;  
		}
		catch(e)
		{
			ALiBS.sy.la(5+0,"[HANDLED][EXCEPTION]:"+e,{o:"ALiBS.el.si",le:ALiBS.gv.el.le});
		}
	},
	// *****************************************************************
	ss: function(i,v) // set source - image source
	// i = element id
	// v = value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.ss",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//if(e) e.src = v;
		//
		if(e) e.src = v + "?_scF=el.ss&rl=" + ALiBS.gv.sy.rl; 
	},
	// *****************************************************************
	sf: function(i,p) // set focus
	// i = (required)(string) element id
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	// return = void
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.sf",le:ALiBS.gv.el.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			z.e0 = ALiBS.el.ge(i);
			//
			if(z.e0)
			{
				try // I.E. generates ERROR if element is hidden, NOT visible (display:none, visibility:hidden)
				{
					z.e0.focus();
				}
				catch(e)
				{
					ALiBS.sy.la(5+0,"[HANDLED][EXCEPTION]element.focus()="+e,{o:"ALiBS.el.sf",le:ALiBS.gv.el.le});
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.el.sf",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.el.sf",le:ALiBS.gv.el.le});
		//
		return r; 
	},
	// *****************************************************************
	//function getObjs(e,p)
	gsi: function(e,p) // get SELECT information
	// e = (required)(String)element id
	// p = (optional)(JSON)parameters {d:"l"} 
	//	d = (String)data type (l/s)(list/size)
	//	mx = (number)max initial size
	{
		// TODO:(2011-10-03) - adicionar retornar opcao selecionada (number)
		// TODO:(2011-10-03) - adicionar retornar opcao selecionada (value/string)
		//
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gsi",le:ALiBS.gv.el.le});
		//
		var d = "l"; // Default
		var mx = 5; // 1000; // Default
		//
		if(p)
		{
			if(typeof(p.d)=="string") d = p.d;
			//
			if(typeof(p.mx)=="number") d = p.mx;
		}
		//
		var obj = ALiBS.el.ge(e);
		//
		if(d=="l") // list
		{
			var a = new Array();
			//
			for(var i=0;i<mx;i++)
			{
				if(typeof(obj)=="undefined" || !obj || typeof(obj.options)=="undefined" || !obj.options || typeof(obj.options[i])=="undefined" || !obj.options[i] || typeof(obj.options[i].value)=="undefined" || !obj.options[i].value)
				{
					return a; // Array
				}
				else
				{
					var opt = obj.options[i].value;
					//alert("opt="+opt);
					//
					a[a.length] = opt;
				}
			}
		}
		else if(d=="s") // size
		{
			for(var i=0;i<mx;i++)
			{
				//var i2 = i;
				//
				if(typeof(obj)=="undefined" || !obj || typeof(obj.options)=="undefined" || !obj.options || typeof(obj.options[i])=="undefined" || !obj.options[i] || typeof(obj.options[i].value)=="undefined" || !obj.options[i].value)
				{
					//alert("i="+i);
					//
					return i;
				}
				//
				//alert("saiu do for="+i2);
			}
		}
		else
		{
			return "";
		}
	},
	// *****************************************************************
	gif: function(i) // get iFrame innerHTML
	// i = element id
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gif",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		if(e.contentwindow.document)
		{
			return e.contentwindow.document.getElementsByTagName('BODY')[0].innerHTML;
		}
		else if(e.contentWindow)
		{
			return e.contentWindow.window.document.body.innerHTML;
		}
		else return e;
	},
	// *****************************************************************
	fs: function(i) // form submit - executa validacao em onclick e depois executa submit
	// i = element id
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.fs",le:ALiBS.gv.el.le});
		//
		var e = ALiBS.el.ge(i);
		//
		//ZLiBS.sy.la(7,"passou1\n",{le:ALiBS.gv.el.le});
		if(typeof e.onsubmit == "func"+"tion") 
		{
			//ZLiBS.sy.la(7,"passou2\n",{le:ALiBS.gv.el.le});
			if(!e.onsubmit) 
			{
				//ZLiBS.sy.la(7,"passou3\n",{le:ALiBS.gv.el.le});

				return false;
			}
			else
			{
				//ZLiBS.sy.la(7,"passou4\n",{le:ALiBS.gv.el.le});

				var ok = e.onsubmit();
				//ZLiBS.sy.la(7,"ok="+ok+"\n",{le:ALiBS.gv.el.le});
				if(ok)
				{
					//ZLiBS.sy.la(7,"passou5\n",{le:ALiBS.gv.el.le});
					return e.submit();
				}
				else return false;
			}
		}
		else return e.submit();
	},
	// *****************************************************************
	ae: function(i,t,pi) // append to element
	// i = (required)(string)id of element to create
	// t = (required)(string)type of element to create ("div","span")
	// pi = (required)(string||element)id of parent ("id001","body","head","div001",etc)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.ae",le:ALiBS.gv.el.le});
		//
		var c = window.document.createElement(t);
		if(c)
		{
			c.setAttribute("id",i);
			c.setAttribute("name",i);
			//c.id = i;
			//c.name = i;
			//
			if(typeof(pi)=="string") // id of element TODO(2014-03-13): BEST-PRACTICE - USAR EM TODAS AS FUNCOES pois fica mais amplo (string||element)
			{
				var e = ALiBS.el.ge(pi);
				if(e) e.appendChild(c);
			}
			else
			{
				if(pi) pi.appendChild(c);
			}
		}
	},
	// *****************************************************************
	// http://www.w3schools.com/jsref/dom_obj_event.asp
	// http://www.w3schools.com/tags/ref_eventattributes.asp
	aon: "onabort,onafterp"+"rint,onbeforep"+"rint,"+
	"onbeforeunload,onblur,oncanplay,oncanplaythrough,onchange,onclick,oncontextmenu,ondblclick,ondrag,ondragend,ondragenter,ondragleave,ondragover,"+
	"ondragstart,ondrop,ondurationchange,onemptied,onended,onerror,onfocus,onformchange,onforminput,onhaschange,oninput,oninvalid,onkeydown,onkeypress,onkeyup,"+
	"onload,onloadeddata,onloadedmetadata,onloadstart,onmessage,onmousedown,onmouseenter,onmouseleave,onmousemove,onmouseout,onmouseover,onmouseup,onmousewheel,"+
	"onoffline,ononline,onpagehide,onpageshow,onpause,onplay,onplaying,onpopstate,onprogress,onratechange,onreadystatechange,onredo,onreset,onresize,onscroll,"+
	"onseeked,onseeking,onselect,onstalled,onstorage,onsubmit,onsuspend,ontimeupdate,onundo,onunload,onvolumechange,onwaiting",
	// *****************************************************************
	gon: function(p) // get "on" defined functions
	// p = (optional)(JSON)parameter 
	//	rt = (number)return type
	//		0 = DEFAULT = print to log
	//		1 = ALiBS.to.rs (f00:)
	//
	//	mx = (number)max initial size
	//
	// return = <undefined>
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			if(typeof(z.rt)!="number") z.rt = 0; // DEFAULT 
			//
			ALiBS.sy.la(5+0,"z.rt="+z.rt,{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
			//
			// **************** 
			//
			z.rs = new Array();
			//
			z.ec = -1000; // 
			z.ar = ALiBS.el.aon.split(",");
			//
			z.ec = -1100; // 
			for(var i=0;i<z.ar.length;i++)
			{
				z.ec = -1200; // 
				if(ALiBS.sy.ty(window[z.ar[i]])=="function")
				{
					if(z.rt==0) // log
					{
						ALiBS.sy.la(5+0,"window["+z.ar[i]+"]=[NOT-EMPTY]",{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
					}
					else if(z.rt==1)
					{
						z.rs[z.rs.length] = {
							f00: "window",
							f01: z.ar[i],
							f02: "[NOT-EMPTY]",
							f03: window[z.ar[i]]
						};
					}
					else
					{

					}
				}
			}
			//
			z.ec = -2100; // 
			for(var i=0;i<z.ar.length;i++)
			{
				z.ec = -2200; // 
				if(ALiBS.sy.ty(window.document[z.ar[i]])=="function")
				{
					if(z.rt==0) // log
					{
						ALiBS.sy.la(5+0,"window.document["+z.ar[i]+"]=[NOT-EMPTY]",{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
					}
					else if(z.rt==1)
					{
						z.rs[z.rs.length] = {
							f00: "window.document",
							f01: z.ar[i],
							f02: "[NOT-EMPTY]",
							f03: window.document[z.ar[i]]
						};
					}
					else
					{

					}
				}
			}
			//
			z.ec = -3100; // 
			z.tg = ALiBS.el.ga({t:"*"});
			//
			z.ec = -3110; // 
			for(var j=0;j<z.tg.length;j++)
			{
				for(var i=0;i<z.ar.length;i++)
				{
					z.ec = -3200; // 
					if(ALiBS.sy.ty(z.tg[j][z.ar[i]])=="function")
					{
						if(z.rt==0) // log
						{
							z.s0 = z.tg[j].tagName+"[id:"+z.tg[j].id+"][name:"+z.tg[j].name+"]["+z.ar[i]+"]=[NOT-EMPTY]";
							ALiBS.sy.la(5+0,z.s0,{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
						}
						else if(z.rt==1)
						{
							z.rs[z.rs.length] = {
								f00: z.tg[j].tagName,
								f01: z.ar[i],
								f02: "[NOT-EMPTY]",
								f03: z.tg[j][z.ar[i]],
								f04: "id:"+z.tg[j].id,
								f05: "name:"+z.tg[j].name,
								f06: "value:"+z.tg[j].value
							};
						}
						else
						{

						}
					}
				}
			}
			//
/*
			for(var i in window)
			{
				if(ALiBS.sy.ty(window[i])=="function") console.log(window[i]);
			}

			for(var i in window.document)
			{
				if(ALiBS.sy.ty(window.document[i])=="function") console.log(window.document[i]);
			}
*/
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			//z.ec = -1100; // 
			//
			z.ec = -9000; 
			r = z.rs;				
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.el.gon",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS.el.gon",le:ALiBS.gv.el.le});
		//
		return r; 
	},
	// *****************************************************************
	gd: function(n0,p) // get divs / span / other 
	/** 
	Method <static> <number> ALiBS.el.gd(p)

		Description: aaaa

		Parameters:
			n0 = (required)(string) Number of elements

			p = (optional)(JSON)parameter {}
				el = (string) Element (div,span,etc)
					div = DEFAULT
				px = (string) ID Prefix
					el = DEFAULT
				en = (number) set element name
					0 = DEFAULT = do NOT set
					1 = set

		Observations:
	
		Return Value:
			<static> <string> HTML Elements

		Example(s):
			var e0 = ALiBS.el.gd(10);
			var e0 = ALiBS.el.gd(10,{el:"span"});

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			z.ec = -950; // 
			if(typeof(n0)!="number" || n0<1) n0 = 1; // DEFAULT
			//
			ALiBS.sy.la(5+0,"n0="+n0,{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.el)!="string" || !z.el) z.el = "div"; // DEFAULT 
			if(typeof(z.px)!="string" || !z.px) z.px = z.el; // DEFAULT 
			if(typeof(z.en)!="number") z.en = 0; // DEFAULT 
			//
			ALiBS.sy.la(5+0,"z.el="+z.el,{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
			ALiBS.sy.la(5+0,"z.px="+z.px,{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
			ALiBS.sy.la(5+0,"z.en="+z.en,{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			z.a0 = new Array(); // para conter as strings <div>
			//
			z.ec = -1200; // 
			for(var i=0;i<n0;i++)
			{
				z.a0[z.a0.length] = '<'+z.el;
				//
				z.a0[z.a0.length] = ' id="'+z.px+i;
				//
				if(z.en==1)
				{
					z.a0[z.a0.length] = ' name="'+z.px+i;
				}
				//
				z.a0[z.a0.length] = '"></'+z.el+'>';
			}
			//
			r = z.a0.join("");
			//<div id="div00"></div><div id="div01"></div><div id="div02"></div><div id="div03"></div><div id="div04">;
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.el.gd",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.el.gd",le:ALiBS.gv.el.le});
		//
		return r; 
	},
	// *****************************************************************
	chs: function() // charset
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.el.chs",le:ALiBS.gv.el.le});
		//
		var c = null;
		//
		if(!c && window.document.charset) c = window.document.charset;
		//
		if(!c && window.document.characterSet) c = window.document.characterSet;
		//
		if(!c) return null;
		else return c.toLowerCase();
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.el("+(++ALiBS.gv.el.nn)+"x)",{o:"ALiBS.el"});
ALiBS.sy.la(0+0,"ALiBS.gv.el.le="+ALiBS.gv.el.le,{o:"ALiBS.el"});
//
ALiBS.sy.la(0+0,"charset="+ALiBS.el.chs()+ALiBS.gv.sy.se,{o:"ALiBS.el"});

