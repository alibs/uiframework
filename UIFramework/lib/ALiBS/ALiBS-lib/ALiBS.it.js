/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.it) ALiBS.gv.it = {}; // i18n = internationalization - use only for language
//
if(!ALiBS.gv.it.le) ALiBS.gv.it.le = 0; // local log status
//
ALiBS.gv.it.nn = 0; // x times loaded
//
ALiBS.gv.it.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.gv.it.cl = ""; // current language 
//
ALiBS.gv.it.cla = {}; // cache of words 
//
ALiBS.gv.it.fcl = {}; // on change language = function Array cache
//
ALiBS.gv.it.pd = 2; // 2 = not loaded, 0 = loaded but not processed, 1 = language changed/processed on page
//
ALiBS.gv.it.pd_ap = 2; // 2 = not loaded, 0 = loaded but not processed, 1 = language changed/processed on page
//
// *************************************************************************
//
ALiBS.gv.it.pt_br_ALiBS_nn = 0; // x times loaded // TODO: colocar no stress test, pode carregar mais de uma vez?
ALiBS.gv.it.en_us_ALiBS_nn = 0; // x times loaded
ALiBS.gv.it.es_es_ALiBS_nn = 0; // x times loaded
//
ALiBS.gv.it.pt_br_ap_nn = 0; // x times loaded // TODO: colocar no stress test, pode carregar mais de uma vez?
ALiBS.gv.it.en_us_ap_nn = 0; // x times loaded
ALiBS.gv.it.es_es_ap_nn = 0; // x times loaded
//
// *************************************************************************
//
ALiBS.it = // i18n = internationalization
{
	dl: function() // default language
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.it.dl",le:ALiBS.gv.it.le});
		//
		var a = ALiBS.ev.bl();
		//
		if(a) 
		{
			a.toLowerCase();
			//
			if(a.indexOf("en")>=0) return "ALiBS.it.en_us";
			else if(a.indexOf("es")>=0) return "ALiBS.it.es_es";
			else return "ALiBS.it.pt_br";
		}
		else return "ALiBS.it.pt_br";
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.it("+(++ALiBS.gv.it.nn)+"x)",{o:"ALiBS.it"});
ALiBS.sy.la(0+0,"ALiBS.gv.it.le="+ALiBS.gv.it.le+ALiBS.gv.sy.se,{o:"ALiBS.it"});

