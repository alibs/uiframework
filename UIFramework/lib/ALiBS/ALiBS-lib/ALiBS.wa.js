/* *************************************************************************
// INFO:

// *************************************************************************
// TODO():

*/
//
// *************************************************************************
//
if(!ALiBS.gv.wa) ALiBS.gv.wa = {}; // global values
//
if(!ALiBS.gv.wa.le) ALiBS.gv.wa.le = 0; // local log status
//
ALiBS.gv.wa.nn = 0; // x times loaded
//
ALiBS.gv.wa.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wa.rl = "14.0822.0001";
//
// *************************************************************************
//
ALiBS.wa = // Application (Server) Authentication
{
	// *****************************************************************
	ao: function(ho,us,pw,p) // Generic Authentication Object
	/** 
	Method <instance> <ALiBS.wa.ao> ALiBS.wa.ao(ho,us,pw,p)

		Description: Generic Authentication Object

		Parameters:

			ho = (required)(string) Host (url)

			us = (required)(string) UserID (principal)

			pw = (required)(string) Password (secret)

			p = (optional)(JSON)parameters {}

				uDN = (string) userDN (principal)
				uID = (string) userID (principal)

				cp = (number) Use Connection Pool 
					0 = No connection pool
					1 = DEFAULT = Use connection pool

				to = (number) Connection Timeout (milliseconds)
					10*1000 = DEFAULT = 10.000 milliseconds = 10 seconds

				mxrs = (number) Max Results
					10*1000 = DEFAULT

				ssl = (required)(number) SSL
					0 = DEFAULT = NO SSL
					1 = Use the SSL Socket Property = ( Context.SECURITY_PROTOCOL=ssl ) + ( URL=ldap://localhost:636/o=JNDITutorial )
					2 = Use the LDAPS URL = (URL=ldaps://localhost:636/o=JNDITutorial)

		Observations:
	
		Return Value:
			Authentication Object with 
				(cc) connection data (javax.naming.directory.DirContext)
				(cc) connection data (other)

		Example:
			var x = ALiBS.wa.ao(host,user,pass);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = {}; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 

			//TODO:
			// r.d0 = {}; // Data Object 0 (could create others manually)
			// r.v0 = {}; // Custom Variables 0 (could create others manually)

			r.sdc = {}; // authentication object / session data connection
			r.va = {}; // custom variables/values
			//
			z.ec = -950; // 
			if(typeof(ho)!="string") throw "alibs.wa.ao.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			if(typeof(us)!="string") throw "alibs.wa.ao.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			if(typeof(pw)!="string") throw "alibs.wa.ao.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(8+0,"z.pw="+z.pw,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			if(typeof(z.to)!="number") z.to = 10*1000; // 
			if(typeof(z.mxrs)!="number") z.mxrs = 10*1000; // 
			if(typeof(z.uID)!="string") z.uID = ""; // 
			if(typeof(z.uDN)!="string") z.uDN = ""; // 
			//
			ALiBS.sy.la(5+0,"z.cp="+z.cp,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(5+0,"z.ssl="+z.ssl,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(5+0,"z.to="+z.to,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(5+0,"z.mxrs="+z.mxrs,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(5+0,"z.uID="+z.uID,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			ALiBS.sy.la(5+0,"z.uDN="+z.uDN,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
			//
			//
			// local variables [BEST-PRACTICE]
			//z.a = new Array();
			//
			r.sdc.ho = ho;
			r.sdc.us = us;
			r.sdc.pw = pw;
			//
			r.sdc.cp = z.cp;
			r.sdc.ssl = z.ssl;
			r.sdc.to = z.to;
			r.sdc.mxrs = z.mxrs;
			//
			r.sdc.uID = z.uID;
			r.sdc.uDN = z.uDN;
			//
			r.sdc.cc0 = r.sdc.cc = null; // javax.naming.directory.DirContext
			r.sdc.cc1 = null; // connection information from application (iManager/Tomcat/javax/etc)
			r.sdc.cc2 = null; // connection information from application (iManager/Tomcat/javax/etc)
			r.sdc.cc3 = null; // connection information from application (iManager/Tomcat/javax/etc)
			r.sdc.cc4 = null; // connection information from application (iManager/Tomcat/javax/etc)
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wa.ao",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wa.ao",le:ALiBS.gv.wa.le});
		//
		return r; // Authentication Object
	},
	// *****************************************************************
	sao: function(ao,p) // Save/Set Authentication Object to session
	/** 
	Method <static> <number> ALiBS.wa.sao(ao,p)

		Description: Save/Set Authentication Object to session

		Parameters:

			ao = (required)(ALiBS.wa.ao) Authentication Object

			p = (optional)(JSON)parameters {}


		Observations:
	
		Return Value:
			Status:
				1 = Success
				-1 = Error

		Example:
			var x = ALiBS.wa.sao(ao);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wa.sao",le:ALiBS.gv.wa.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = -1; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			//
			z.ec = -950; // 
			//if(typeof(ho)!="string") throw "ALiBS.wa.sao.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wa.sao",le:ALiBS.gv.wa.le});
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			if(typeof(ao.sdc.uID)!="string") ao.sdc.uID = ""; // 
			//
			session.setAttribute("ALiBS.wa.ao.instance",ao); // user (HTTPServer) session
			//
			r = 1;
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wa.sao",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wa.sao",le:ALiBS.gv.wa.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wa.sao",le:ALiBS.gv.wa.le});
		//
		return r; // number
	},
	// *****************************************************************
	gao: function(p) // Save/Set Authentication Object to session
	/** 
	Method <instance> <ALiBS.wa.ao> ALiBS.wa.gao(p)

		Description: Save/Set Authentication Object to session

		Parameters:

			p = (optional)(JSON)parameters {}


		Observations:
	
		Return Value:
			Status:
				1 = Success
				-1 = Error

		Example:
			var x = ALiBS.wa.gao(ao);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wa.gao",le:ALiBS.gv.wa.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = null; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			//
			z.ec = -950; // 
			//if(typeof(ho)!="string") throw "ALiBS.wa.gao.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wa.gao",le:ALiBS.gv.wa.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//
			//ALiBS.sy.la(5+0,"z.cp="+z.cp,{o:"ALiBS.wa.gao",le:ALiBS.gv.wa.le});
			//
			// local variables [BEST-PRACTICE]
			//if(typeof(ao.sdc.uID)!="string") ao.sdc.uID = ""; // 
			//
			r = session.getAttribute("ALiBS.wa.ao.instance",ao); // user (HTTPServer) session
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wa.gao",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wa.gao",le:ALiBS.gv.wa.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wa.gao",le:ALiBS.gv.wa.le});
		//
		return r; // Authentication Object
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wa("+(++ALiBS.gv.wa.nn)+"x)",{o:"ALiBS.wa"});
ALiBS.sy.la(0+0,"ALiBS.gv.wa.rl="+ALiBS.gv.wa.rl,{o:"ALiBS.wa"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.wa.le="+ALiBS.gv.wa.le+ALiBS.gv.sy.se,{o:"ALiBS.wa"});

