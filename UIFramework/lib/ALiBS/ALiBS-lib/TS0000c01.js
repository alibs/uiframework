// *************************************************************************
//
//ALiBS.gv.sy.so="immediate-console";
//
if(!ALiBS.gv.TS0000c01) ALiBS.gv.TS0000c01 = {}; // global values
//
if(!ALiBS.gv.TS0000c01.le) ALiBS.gv.TS0000c01.le = 0; // local log status
//
ALiBS.gv.TS0000c01.rl = "15.0603.1455";
//
// *************************************************************************
//
var TS0000c01 = new ALiBS.to.t({tb_rf:"TS0000c01"}); 
//
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{

}
//
// *************************************************************************
//
else // Browser / Client
{

	TS0000c01.nm.f21 = "TableCode";
	TS0000c01.cp.f21 = "TableCaption";
	TS0000c01.tt.f21 = "TableTitle";
	TS0000c01.dc.f21 = "Table Code/Real Name";
	TS0000c01.dv.f21 = "FIXED:TableTitle";
	TS0000c01.ob.f21 = "Physical Table Name";
	//
	TS0000c01.nm.f22 = "FieldCode";
	TS0000c01.cp.f22 = "FieldCode";
	//
	TS0000c01.nm.f23 = "Description";
	TS0000c01.cp.f23 = "Description";
	//
	TS0000c01.nm.f24 = "Sample";
	TS0000c01.cp.f24 = "Sample";
	//
	TS0000c01.nm.f25 = "x"; 
	TS0000c01.cp.f25 = "x";
	//
	TS0000c01.nm.f26 = "CreationDate"; // Field Creation Date (Table Version = Field Version mais velho)
	TS0000c01.cp.f26 = "Creation Date";
	//
	TS0000c01.nm.f27 = "Name";
	TS0000c01.cp.f27 = "Name";
	//
	TS0000c01.nm.f28 = "Caption";
	TS0000c01.cp.f28 = "Caption";
	//
	TS0000c01.nm.f29 = "Title";
	TS0000c01.cp.f29 = "Title";
	//
	TS0000c01.nm.f30 = "TableGroup"; // Agrupamento de tabelas
	TS0000c01.cp.f30 = "TableGroup";
	//
	TS0000c01.nm.f31 = "Defaultvalue"; // Default Value
	TS0000c01.cp.f31 = "Default Value";
	//
	TS0000c01.nm.f99 = "OBS"; // Obs
	TS0000c01.cp.f99 = "OBS";
	//
	// *************************************************************************
	//
	TS0000c01.tb.pg.n = 31; // paging = number of records to show (0 = show all)
	//
	TS0000c01.tb.sh = ["f30","f21","f22","f27","f28","f29","f23","f24","f25","f26","f31"];
	//
	TS0000c01.tb.fk.f21 = "[TABLEINFO]"; // table information button
	TS0000c01.tb.fk.f23 = [{t:"Todos",v:"x;x"},{t:"fm:match",v:"fm;x"},{t:"fe:equals",v:"fe;x"},{t:"res>=1",v:"fc;(a00>=1)"},{t:"res>=3",v:"fc;(a00>=3)"}]; // complex filter
	TS0000c01.tb.fk.f24 = "[PAGING]"; // paging button
	//
	TS0000c01.tb.fi.f22 = "[],[EMPTY],[NOT-EMPTY],[abcde],[fghij],[klmno],[pqrst],[uvxwyz],a,b,c,d,e,f,g,h,i,j";
	//
	TS0000c01.tb.fc = '(a00>=1)'; // complex filter string
	TS0000c01.tb.fa = "fm"; // active filter
	//
	ALiBS.gv.hg.tt = 1; // field title
	//
	// *************************************************************************
	TS0000c01.ff.hg.dg = function(t,n,p) // display GRID
	// t = ()table
	// n = ()sorted field
	// p = (optional)(JSON)parameter {}
	//	rv = (boolean)reverse sort (default=false)
	//	y = (String)field type [abcdefghijkrx] (default=f)
	{
		var z = p || {}; // [BEST-PRACTICE]
		//
		if(n==t.so.g) ALiBS.to.o(t,n,{rv:!t.so.rv,y:z.y}); // reverse
		else ALiBS.to.o(t,n,{y:z.y}); // normal sort
		//
		var a = ALiBS.hg.g0(t,"nome"); // 4101, font-size: 16px; padding:2px;
		//
		//var a = ALiBS.hg.g0(t,"nome",{cs:""}); // 4101, font-size: 16px; padding:2px;
		//
		ALiBS.el.si("iContGrid02a",a);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
	};
	// *************************************************************************
	TS0000c01.ff.hg.af = function(t) // after load = 
	// t = (ALiBS.to)
	{
		var z = {}; // [BEST-PRACTICE]
		//
		// ********************************
		//
		t.ff.hg.rc(t); // insert TS0000c01 (own) structure into TS0000c01 data
		//
		t.ff.hg.rc(TS0002); // insert table structure into TS0000c01 data
		TS0002.rs = null; // Data is not needed = Garbage Collection
		//
		t.ff.hg.rc(TS0004); 
		TS0004.rs = null; 
		//
		t.ff.hg.rc(TM0000); 
		TM0000.rs = null; 
		//
		t.ff.hg.rc(TM2010); 
		TM2010.rs = null; 
		//
		ALiBS.to.nf(t); // Normalise Fields
		//
		// ********************************
		//
		for(var i=0;i<t.rs.length;i++)
		{
			if(t.rs[i].f99)
			{
				t.rs[i]["hf23"] = '<em title="'+ t.rs[i].f99 +'" style="background:yellow;">'+ t.rs[i].f23 +'</em>'; // write obs = f99
			}
		}
		//
		// ********************************
		//
		ALiBS.to.o(t,21,{y:"f"});
		t.tb.fi["f21"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:21,ke:2}).join(",");
		//
		ALiBS.to.o(t,23,{y:"f"});
		t.tb.fi["f23"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:23,ke:2}).join(",");
		//
		//ALiBS.to.o(t,24,{y:"f"});
		//t.tb.fi["f24"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:24,ke:2}).join(",");
		//
		ALiBS.to.o(t,25,{y:"f"});
		t.tb.fi["f25"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:25,ke:2}).join(",");
		//
		ALiBS.to.o(t,26,{y:"f"});
		t.tb.fi["f26"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:26,ke:2}).join(",");
		//
		//ALiBS.to.o(t,27,{y:"f"});
		//t.tb.fi["f27"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:27,ke:2}).join(",");
		t.tb.fi["f27"] = "[],[EMPTY],[NOT-EMPTY]";
		//
		ALiBS.to.o(t,30,{y:"f"});
		t.tb.fi["f30"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:30,ke:2}).join(",");
		//
		// ********************************
		//
		for(var i=0;i<t.rs.length;i++) // auxiliar index
		{
			t.rs[i]["e01"] = t.rs[i].f21 + "-" + t.rs[i].f22; 
		}
		//
		t.ax.f21 = "e01";
		//
		ALiBS.to.o(t,1,{y:"e"});
		//
		// ********************************
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		//return r; 
	};
	// *************************************************************************
	TS0000c01.ff.hg.rc = function(t) // record create = from table info
	// t = (ALiBS.to)
	{
		var z = {}; // [BEST-PRACTICE]
		//
		z.fd = ALiBS.to.fdn(t);
		//
		// ********************************
		//
		//for(var i in t.nm)
		for(var i=0;i<z.fd.length;i++)
		{
			TS0000c01.rs[TS0000c01.rs.length] = 
				{
					f00:0,
					f21:t.tb.rf, // Table Code
					f22:z.fd[i], // Field Code
					f23:t.dc[z.fd[i]], // Description
					f24:t.rs[0][z.fd[i]], // Sample
					//f25:"",
					f26:t.cd[z.fd[i]], // Creation Date
					f27:t.nm[z.fd[i]], // Field Name
					f28:t.cp[z.fd[i]], // Field Caption
					f29:t.tt[z.fd[i]], // Field Title
					f30:t.tb.tg, // Table Group
					f31:t.dv[z.fd[i]], // Default Value
					f99:t.ob[z.fd[i]], // OBS
				};
		}
		//
		// ********************************
		//
		//z = null; // [BEST-PRACTICE]
		//delete z; // [BEST-PRACTICE]
		//
		//return r; 
	};
	// *************************************************************************
	TS0000c01.ff.hg._hgfc_f0 = function(t,f,v) // when filter changes (field filter)
	// t = (ALiBS.to)
	{
		//var z = {}; // [BEST-PRACTICE]
		//
		console.log("(ff.hg._hgfc_f0)passou ff.hg._hgfc_f0");
		console.log("f="+f);
		console.log("v="+v);
		//
		console.log("f[v]="+t.tb.fi[f].split(",")[v]);
	};
	// *************************************************************************
	TS0000c01.ff.hg._cfck_f0 = function(t,v,i) // when filter changes (complex filter)
	// t = (ALiBS.to)
	{
		//var z = {}; // [BEST-PRACTICE]
		//
		console.log("(ff.hg._cfck_f0)passou ff.hg._cfck_f0");
		console.log("v="+v);
		console.log("i="+i);
		//
		//console.log("f[v]="+t.tb.fi[f].split(",")[v]);
	};
	// *************************************************************************
} // endif("Server")
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module TS0000c01",{o:"TS0000c01"});
ALiBS.sy.la(0+0,"ALiBS.gv.TS0000c01.rl="+ALiBS.gv.TS0000c01.rl,{o:"TS0000c01"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.TS0000c01.le="+ALiBS.gv.TS0000c01.le+ALiBS.gv.sy.se,{o:"TS0000c01"});

