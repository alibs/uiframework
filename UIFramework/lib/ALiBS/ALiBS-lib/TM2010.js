// *************************************************************************
//
var TM2010 = new ALiBS.to.t({tb_rf:"TM2010"}); 
//
// *************************************************************************
//
TM2010.nm.f00 = "Horas"; // 24,20,16,12,8,4,0 = Muito Alta, Alta, Média, Baixa, Muito Baixa
TM2010.cp.f00 = "Horas";
TM2010.tt.f00 = "\n24hs (Muito Alta)\n20hs (Alta)\n16hs (Média)\n8hs (Baixa)\n4hs (Muito Baixa)\n0hs";
//
TM2010.nm.f21 = "SourceSystem"; // Update/Action/Code Point = Assunto = Drivers, Linux, Sentinel, Arquitetura, WorkFlow, iManager, Relatórios
TM2010.cp.f21 = "Source<br/>System";
//
TM2010.nm.f22 = "DestinationSystem"; // IDM, AD, GSEG, AD, Sentinel
TM2010.cp.f22 = "Destination<br/>System";
//
TM2010.nm.f23 = "Assunto"; // CRUD de Objetos
TM2010.cp.f23 = "Assunto";
//
TM2010.nm.f24 = "DescFunc"; // 1 funcionalidade por registro
TM2010.cp.f24 = "Descrição<br/>Funcionalidade";
//
TM2010.nm.f25 = "DescFunc2"; // Descrição Funcionalidade = Inclusão,Inclusão - Regra p/ PD, Alteração, Exclusão, Leitura/Pesquisa/Matching
TM2010.cp.f25 = "Descrição 2"; // 1 funcionalidade por registro
//
TM2010.nm.f99 = "OBS"; // Obs
TM2010.cp.f99 = "OBS";
//
// ***************************
//
TM2010.nm.a00 = "SaldoHoras"; 
TM2010.cp.a00 = "Saldo<br/>Horas";
//
TM2010.nm.a01 = "PF"; 
TM2010.cp.a01 = "PF";
//
TM2010.nm.a02 = "SaldoPF"; 
TM2010.cp.a02 = "Saldo<br/>PF";
//
// ***************************
//
TM2010.nm.b00 = "Nr"; // Numero da funcionalidade
TM2010.cp.b00 = "Nr";
//
// *************************************************************************
//
TM2010.tb.pg.n = 31; // paging = number of records to show (0 = show all)
//
TM2010.tb.sh = ["b00","f21","f22","f24","f23","f25","f00","a00","a01","a02"];
//
TM2010.tb.fk.f21 = "[TABLEINFO]"; // table information button
TM2010.tb.fk.f23 = [{t:"Todos",v:"x;x"},{t:"fm:match",v:"fm;x"},{t:"fe:equals",v:"fe;x"},{t:"res>=1",v:"fc;(a00>=1)"},{t:"res>=3",v:"fc;(a00>=3)"}]; // complex filter
TM2010.tb.fk.f24 = "[PAGING]"; // paging button
//
TM2010.tb.fc = '(a00>=1)'; // complex filter string
TM2010.tb.fa = "fm"; // active filter
//
TM2010.tb.hx = [{ty:"[HEADERHX]",tx:"[ Esforço ]",c:"white",bg:"red",al:"center"}]; // Header hx;
//
ALiBS.gv.hg.tt = 2; // field title
//
// *************************************************************************
TM2010.ff.hg.dg = function(t,n,p) // display GRID
// t = ()table
// n = ()sorted field
// p = (optional)(JSON)parameter {}
//	rv = (boolean)reverse sort (default=false)
//	y = (String)field type [abcdefghijkrx] (default=f)
{
	var z = p || {}; // [BEST-PRACTICE]
	//
	if(n==t.so.g) ALiBS.to.o(t,n,{rv:!t.so.rv,y:z.y}); // reverse
	else ALiBS.to.o(t,n,{y:z.y}); // normal sort
	//
	TM2010.tb.s00=0; // saldo horas
	TM2010.tb.s01=0; // Nr funcionalidades
	TM2010.tb.s02=0; // saldo PF
	//
	var a = ALiBS.hg.g0(t,"nome",{cs:"hg_css0F",ff0:TM2010.ff.hg.ff0}); // 4101, font-size: 16px; padding:2px;
	//
	//var a = ALiBS.hg.g0(t,"nome",{cs:""}); // 4101, font-size: 16px; padding:2px;
	//
	ALiBS.el.si("iContGrid02a",a);
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
};
// *************************************************************************
TM2010.ff.hg.af = function(t) // after load = 
// t = (ALiBS.to)
{
	var z = {}; // [BEST-PRACTICE]
	//
	// ********************************
	//
	for(var i=0;i<t.rs.length;i++) // write obs = f99
	{
		if(t.rs[i].f99)
		{
			t.rs[i]["hf23"] = '<em title="'+ t.rs[i].f99 +'" style="background:yellow;">'+ t.rs[i].f23 +'</em>'; 
		}
	}
	//
	// ********************************
	//
	for(var i=0;i<t.rs.length;i++) // PF
	{
		t.rs[i]["a01"] = t.rs[i].f00*2.5/10; 
	}
	//
	// ********************************
	//
	ALiBS.to.o(t,0,{y:"f"});
	t.tb.fi["f00"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:0,ke:3}).join(",");
	//
	ALiBS.to.o(t,21,{y:"f"});
	t.tb.fi["f21"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:21,ke:2}).join(",");
	//
	ALiBS.to.o(t,22,{y:"f"});
	t.tb.fi["f22"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:22,ke:2}).join(",");
	//
	ALiBS.to.o(t,24,{y:"f"});
	t.tb.fi["f24"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:24,ke:2}).join(",");
	//
	ALiBS.to.o(t,25,{y:"f"});
	t.tb.fi["f25"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:25,ke:2}).join(",");
	//
	ALiBS.to.o(t,26,{y:"f"});
	t.tb.fi["f26"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:26,ke:2}).join(",");
	//
	ALiBS.to.o(t,30,{y:"f"});
	t.tb.fi["f30"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:30,ke:2}).join(",");
	//
	// ********************************
	//
	ALiBS.to.o(t,23,{y:"f"});
	//
	t.tb.fi["f23"] = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:23,ke:2}).join(",");
	//
	for(var i=0;i<t.rs.length;i++) // indice auxiliar regressivo baseado no f23
	{
		t.rs[i]["e01"] = (9999-i); 
	}
	//
	for(var i=0;i<t.rs.length;i++) // indice auxiliar
	{
		t.rs[i]["e00"] = ALiBS.sg.fi(t.rs[i].f21+"-0-","0",25,{d:"r"}) + "-" + t.rs[i].f22 + "-" + t.rs[i].f24 + "-" + t.rs[i].e01 + "-" + t.rs[i].f23; 
	}
	//
	t.ax["f21"] = "e00"; // alternative index (for GRID)
	//
	ALiBS.to.o(t,0,{y:"e"});
	//
	// ********************************
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
	//
	//return r; 
};
// *************************************************************************
TM2010.ff.hg.ff0 = function(t,n) // ff0 = calcular saldo
// t = ()table
// n = ()record
// p = (optional)(JSON)parameter {}
//	x = 
{
	t.rs[n]["a00"] = (+t.tb.s00) + (+t.rs[n]["f00"]);
	//
	t.tb.s00 = +t.rs[n]["a00"];
	//
	// ********************************
	//
	t.rs[n]["b00"] = (+t.tb.s01) + 1;
	//
	t.tb.s01 = +t.rs[n]["b00"];
	//
	// ********************************
	//
	t.rs[n]["a02"] = (+t.tb.s02) + (+t.rs[n]["a01"]);
	//
	t.tb.s02 = +t.rs[n]["a02"];
};
// *************************************************************************

