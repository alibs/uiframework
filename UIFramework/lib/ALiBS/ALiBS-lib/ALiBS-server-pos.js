// *************************************************************************
// Alfredo - 2014-05-12 14:30
// *************************************************************************
// Log Settings After Loading ALiBS
//
ALiBS.sy.gl = 5; // global log level
//ALiBS.sy.li = "ALiBS.cp.bc12,ALiBS.cp.bc13"; // method list
//
//ALiBS.gv.to.le=0;
//ALiBS.gv.wi.le=1;
//ALiBS.gv.wl.le=1;
//
//ALiBS.gv.sy.gla = 0; // 0 = Auto Print / Clean , 1 = Auto Reduce
ALiBS.gv.sy.glx=500; // max = trigger
ALiBS.gv.sy.glu=100; // errors + glu
//
// *************************************************************************
//
//ALiBS.gv.sy.lgrs(); // reset log file
//
// *************************************************************************
// Print Log After Loading ALiBS
//
ALiBS.sy.la(1+0,"",{so:"print"}); // after loading ALiBS
//
// *************************************************************************
// *************************************************************************
// *************************************************************************
// *************************************************************************

