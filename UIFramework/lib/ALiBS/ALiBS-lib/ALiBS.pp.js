/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

1 - somente tags
2 - properties
3 - CDATA

root.tag1 = abc1
root.tag1.@prop1 = abc2

root.tag2.#cdata = conteudo de cdata


// ***************************

myClassesA.io.propertiesUtil.java
myClassesX.stress.v2.Stress_PropertiesUtil.java

[null]
[empty]
[space]
[noTagValue]
[noAttrValue]
[cdata]

// ***************************

Substituir &lt; e &lt; por < e > dentro de CDATA 
Substituir [cdata()cdata] por <![CDATA[]]>

// ***************************

strArray[0]="nome0=valor0";
strArray[1]="nome1=valor1";

public static void arrayToProp(String strArray[], Properties prop) throws Exception

public static String[] propToArray(Properties prop) throws Exception

// ***************************

Talvez nao precise:
*******************
// retorna String[] de keyNames ordenada por keyName
public static String[] keyNames(Properties prop) throws Exception


Proximos:
*********

	// retorna String[] de values ordenada por keyName
	public static String[] keyValues(Properties prop) throws Exception

	// reset os valores de properties que come�am com prefix
	// type = equals, startsWith, resetAll
	public static void propReset(Properties prop, String prefix, String type) throws Exception

	// [null]
	public static String propToToken(Properties prop, String separador) throws Exception

	// [null] [empty]
	public static void tokenToProp(String strToken, Properties prop, String separador) throws Exception

	public static int countChars(String str, String charToCount) throws Exception

	// retorna os valores de properties que contenham prefix ou  os valores que n�o come�am com prefix, dependendo de isPrefix = true/false
	public static int propFilter(Properties prop, Properties propReturn, String prefix, boolean isPrefix) throws Exception

	// retorna os valores de properties que come�am com prefix ou  os valores que n�o come�am com prefix, dependendo de isPrefix = true/false
	public static int propFilter(Properties prop, Properties propReturn, String prefix, String prefixContains, boolean isPrefix) throws Exception

	// retorna os valores de properties que come�am com prefix ou  os valores que n�o come�am com prefix, dependendo de isPrefix = true/false
	public static int propFilter(Properties prop, Properties propReturn, String[] prefix, boolean isPrefix) throws Exception

	public static void propToPropForEach(Properties propFrom, Properties propTo, Properties propRules) throws Exception

	// incrementar vari�veis + inicializar vari�veis
	private static void propToPropAux3(Properties propRules, String propName, String propValue) throws Exception

	// substituir vari�veis
	private static String propToPropAux2(Properties propRules, String propToReplace) throws Exception

	// acertar mensagens de exception

// ***************************
	
	name  (tag to)  : [$propFrom], [$propRules], [$propTo], [$var$s] 
	value (tag from): [$propFrom], [$propRules], [$propTo], [$s]
	
	TO
[$n]=
[$s]=
[$var$s]=

FROM
=[noTagValue]
=[$nCount]
=[$sNome]
=[$sNome[$nIdx0]]

// ***************************

	private static void propToPropAux(Properties propTo, Properties propFrom, Properties propRules) throws Exception


	public static int addCounter(Properties prop, String counterName, int addValue) throws Exception

	public static int setCounter(Properties prop, String counterName, int setValue) throws Exception

	private static int setCounter(Properties prop, String counterName, int initialValue, int incrementValue, boolean reset) throws Exception

	public static int getCounter(Properties prop, String counterName) throws Exception

	public static int getInteger(Properties prop, String field, String outputAction) throws Exception

	// outputAction = NullEmptyBlankNotAllowed = gera VarNullEmptyBlankNotAllowedException
	// outputAction = ZeroNotAllowed = gera VarZeroNotAllowedException
	// outputAction = NegativeNotAllowed = gera VarNegativeNotAllowedException
	// outputAction = PositiveNotAllowed = gera VarPositiveNotAllowedException
	public static long getLong(Properties prop, String field, String outputAction) throws Exception

	// outputAction = NullEmptyBlankNotAllowed = gera VarNullEmptyBlankNotAllowedException
	public static boolean getBoolean(Properties prop, String field, String outputAction) throws Exception

	public static String getString(Properties prop, String field, String outputAction) throws Exception

	// outputAction = NullEmptyBlankNotAllowed = gera VarNullEmptyBlankNotAllowedException
	// outputAction = replaceToEmpty = gera empty
	// outputAction = replaceToNull = gera null
	// outputAction = replaceToFalse = gera false
	// outputAction = "" ou none = nada
	// qualquer outra string = seta valor da string
	public static void getStringBuffer(StringBuffer sbfValue, Properties prop, String field, String outputAction) throws Exception

	// outputAction = NullEmptyBlankNotAllowed = gera VarNullEmptyBlankNotAllowedException
	public static String getPrefix(Properties prop, String field, String outputAction) throws Exception


		classStatus.setProperty("class.bestPractices.level[0].@lastUpdate","2006-11-13"); // Método classStatus()
		classStatus.setProperty("class.bestPractices.level[1].@lastUpdate","2006-11-13"); // CTRL+SHIFT+O
		classStatus.setProperty("class.bestPractices.level[2].@lastUpdate","2006-11-13"); // Linhas (5-131)
		classStatus.setProperty("class.bestPractices.level[3].@lastUpdate","2006-11-13"); // Métodos STATIC (exemplo: ClasseUtil)
		classStatus.setProperty("class.bestPractices.level[4].@lastUpdate","2006-11-13"); // Métodos Não STATIC (exemplo: ClasseData)
		classStatus.setProperty("class.bestPractices.level[5].@lastUpdate","2006-11-13"); // Construtor para Classes com Métodos Não STATIC

// ***************************

*/
//
// *************************************************************************
//
if(!ALiBS.gv.pp) ALiBS.gv.pp = {}; // global values
//
if(!ALiBS.gv.pp.le) ALiBS.gv.pp.le = 0; // local log status
//
ALiBS.gv.pp.nn = 0; // x times loaded
//
ALiBS.gv.pp.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.pp = // properties
{
	px: function(j) // properties to xml
	// Remove tabs entre as tags e no inicio e fim dos values. Não remove no meio dos values.
	// aspas simples (') dentro dos values - tem que usar CDATA
	// maior (>), menor (<) dentro dos values da tags - tem que usar CDATA
	//
	// j = (required)(JSON)properties
	//
	// return (String) xml	
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.pp.px",le:ALiBS.gv.pp.le});
		//
		var x = "";
		//
		return x;
	},
	// *****************************************************************
	xp: function(s,p) // xml to properties
	// s = (required)(String)xml
	// p = (optional)(JSON)parameters {}
	//
	//	rt = (number)
	//		0 = DEFAULT = pp
	//		1 = ALiBS.to
	//
	// return (JSON)properties
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.pp.xp",le:ALiBS.gv.pp.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		if(typeof(z.rt)!="number") z.rt = 0; // DEFAULT
		//
		var a = s.split("<");
		var t = new Array(); // tree
		//
		var j;
		//
		if(z.rt==1) // ALiBS.to
		{
			j = new ALiBS.to.t(); // JSON to return
		}
		else // properties
		{
			j = {}; // JSON to return
		}
		//
		var b;
		var l; // length
		var l2; // length
		var c; // Array of properties name=value
		//
		var d;
		var x = false;
		//
		var c0 = 0;
		//
		l = a.length;
		//
		for(var i=0;i<l;i++)
		{
			if(a[i]==null || a[i]=="") continue; // generally the first occurance
			else if(a[i].charAt(0)=="/") // tag closing </tag>
			{
				t.length = t.length-1; // remove last tag from tree
			}
			else
			{
				b = a[i].split(">");
				//
				if(b[0]) // b[0] = tag name AND properties / b[1] = tag value
				{
					c = b[0].indexOf(" ");
					//
					if(c>0) // there are properties
					{
						c = b[0].substr(c+1); // String with properties c[0]=name / c[1]=value
						c = ALiBS.sg.tr(c);
						c = ALiBS.pp.st(c," ");
						c = ALiBS.pp.aa(c);
						//
						b[0] = b[0].substr(0,b[0].indexOf(" ")); // tag name
					}
					else
					{
						c = null;
					}
					//
					//if(b[0][(b[0].length-1)]=="/") // no value tag <tag/> only if tag has NO attributes
					if(b[0].charAt((b[0].length-1))=="/") // no value tag <tag/> only if tag has NO attributes
					{
						t[t.length] = b[0].substr(0,b[0].length-1); // add new tag to tree
						//
						if(z.rt==1) // ALiBS.to
						{
							j.rs[j.rs.length] = new Array();
							//
							j.rs[j.rs.length-1].f00 = ALiBS.sg.fi(""+(c0++),"0",7); // "[" + (c0++) + "]";
							j.rs[j.rs.length-1].f01 = t.join(".");
							j.rs[j.rs.length-1].f02 = "[noTagValue]";
						}
						else // properties
						{
							//j[t.join(".")] = "[noTagValue]";
							j["["+(c0++)+"]"+t.join(".")] = "[noTagValue]";
						}
						//
						t.length = t.length-1; // remove last tag from tree
					}
					else
					{
						t[t.length] = b[0]; // add new tag to tree
						//
						if(!b[1] || b[1]=="" || ALiBS.sg.tr(b[1])=="")
						{
							b[1] = "[noTagValue]";
						}
						//
						if(z.rt==1) // ALiBS.to
						{
							j.rs[j.rs.length] = new Array();
							//
							j.rs[j.rs.length-1].f00 = ALiBS.sg.fi(""+(c0++),"0",7); // "[" + (c0++) + "]";
							j.rs[j.rs.length-1].f01 = t.join(".");
							j.rs[j.rs.length-1].f02 = b[1];
						}
						else // properties
						{
							//j[t.join(".")] = b[1];
							j["["+(c0++)+"]"+t.join(".")] = b[1];
						}
						//
						if(c && c.length>0)
						{
							l2 = c.length;
							for(var k=0;k<l2;k++) 
							{
								d = c[k][1];
								if(typeof(d)=="undefined" || d=="") d = "[noAttrValue]";
								else if(d=="/")
								{
									x = true;
									d = "[noAttrValue]";
								}
								//else if(d[d.length-1]=="/")
								else if(d.charAt(d.length-1)=="/")
								{
									x = true;
									d = d.substr(0,d.length-1);
								}
								//
								if(z.rt==1) // ALiBS.to
								{
									j.rs[j.rs.length] = new Array();
									//
									j.rs[j.rs.length-1].f00 = ALiBS.sg.fi(""+(c0++),"0",7); // "[" + (c0++) + "]";
									j.rs[j.rs.length-1].f01 = t.join(".")+".@"+c[k][0];
									j.rs[j.rs.length-1].f02 = d;
								}
								else // properties
								{
									//j[t.join(".")+".@"+c[k][0]] = d;
									j["["+(c0++)+"]"+t.join(".")+".@"+c[k][0]] = d;
								}
							}
						}
						//
						if(x) t.length = t.length-1; // remove last tag from tree
					}
				}
			}
		}
		//
		return j;
	},
	// *****************************************************************
	ps: function(j) // properties to string
	// j = (required)(JSON)properties
	//
	// return (String) "name=value,name1=value1,name2=value2"
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.pp.ps",le:ALiBS.gv.pp.le});
		//
		var a = new Array();
		//
		for(var i in j)
		{
			a[a.length] = i+"="+j[i];	
		}
		//
		//a.sort();
		//
		return a.join();
	},
	// *****************************************************************
	st: function(s,t) // split BUT do not split inside strings "" OR ''
	// s = (required)(String)string to split
	// t = (required)(String)separator
	//
	// return (Array) a[i][0]=name / a[i][1]=value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.pp.st",le:ALiBS.gv.pp.le});
		//
		if(typeof(s)!="string" || !s) return null;
		if(typeof(t)!="string" || !t) return null;
		//
		var a = new Array(); // items
		var b = new Array(); // chars
		//
		var x; // start/end of string "\"" OR "'"
		//
		var l = s.length;
		for(var i=0;i<l;i++)
		{
			if(s.charAt(i)=="\"" || s.charAt(i)=="'") // start of string
			{
				x = s.charAt(i);
				i++; // do not use first "\"" OR "'"
				while(i<l) // security
				{
					if(s.charAt(i)==x) // end of string
					{
						break;
					}
					else
					{
						b[b.length] = s.charAt(i++);
					}
				}
			}
			else if(s.charAt(i)==t && s.charAt(i)==" ") 
			{
				if(s.charAt(i-1)=="\"" || s.charAt(i-1)=="'") // after " OR ' = end of property
				{ 
					while(s.charAt(i)==" ")
					{
						i++;
					}
					i--;
					a[a.length] = b.join("");
					b.length = 0;
				}
				else if(s.charAt(i-1)=="=") // after = / NOT end of property
				{ 
					while(s.charAt(i)==" ")
					{
						i++;
					}
				}
				else // after tag = end
				{
					while(s.charAt(i)==" ")
					{
						i++;
					}
					i--;
					a[a.length] = b.join("");
					b.length = 0;
				}
			}
			else if(s.charAt(i)==t)
			{
				a[a.length] = b.join("");
				b.length = 0;
			}
			else
			{
				b[b.length] = s.charAt(i);
			}
		}
		//
		a[a.length] = b.join("");
		b.length = 0;
		//
		return a;
	},
	// *****************************************************************
	aa: function(a) // Array to Array
	// a = (required)(Array)pair a[i]=  name="value"
	//
	// return = (Array) bidimensional b[i][0]=name / b[i][1]=value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.pp.aa",le:ALiBS.gv.pp.le});
		//
		var b = new Array();
		//
		var l = a.length;
		for(var i=0;i<l;i++)
		{
			b[b.length] = a[i].split("=");
		}
		//
		return b; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.pp("+(++ALiBS.gv.pp.nn)+"x)",{o:"ALiBS.pp"});
ALiBS.sy.la(0+0,"ALiBS.gv.pp.le="+ALiBS.gv.pp.le+ALiBS.gv.sy.se,{o:"ALiBS.pp"});

