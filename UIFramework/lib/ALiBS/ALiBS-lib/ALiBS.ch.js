/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

mudar nome de funcoes e variaveis

*/
//
// *************************************************************************
//
if(!ALiBS.gv.ch) ALiBS.gv.ch = {}; // global values
//
if(!ALiBS.gv.ch.le) ALiBS.gv.ch.le = 0; // local log status
//
ALiBS.gv.ch.nn = 0; // x times loaded
//
ALiBS.gv.ch.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.ch = 
{
	ascToInt: function(p) // str 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ch.ascToInt",le:ALiBS.gv.ch.le});
		//
		var w = p +'';
		var x = 0;
		var z = 36; // divisor
		var y = 0; // tmp
		var v = 0; // grandeza
		var e = 0; // expoente
		//
		for(var j=0;j<w.length;j++)
		{
			y = ALiBS.ch.intValue(w.charAt(j));
			//ALiBS.libhtm.Htm.dw("char("+j+")="+w.charAt(j)+"<br/>");
			//ALiBS.libhtm.Htm.dw("y="+y+"<br/>");
			//
			e = w.length-1-j;
			//ALiBS.libhtm.Htm.dw("e="+e+"<br/>");
			//
			v = Math.pow(z,e);
			//ALiBS.libhtm.Htm.dw("v="+v+"<br/>");
			//
			x = x + y*v;
		}
		//
		return x;
	},
	// *****************************************************************
	intToAsc: function(p) // int 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ch.intToAsc",le:ALiBS.gv.ch.le});
		//
		//ALiBS.libhtm.Htm.dw("p="+p+"<br/>");
		//
		var t = new Array(); 
		var y = p; // dividendo
		var z = 36; // divisor
		var x = 0; // resto
		var w = 0; // tmp
		var r = ""; // tmp
		//
		for(var j=0;;j++)
		{
			if(y<z)
			{
				t[t.length] = y;
				break;
			}
			else if(y==z)
			{
				t[t.length] = 0;
				t[t.length] = 1;
				break;
			}
			else // y>z
			{
				w = y; 
				//
				x=w%z; // resto
				t[t.length] = x;
				//ALiBS.libhtm.Htm.dw("resto(x)="+x+"<br/>");
				//
				y-=x; // total - resto
				y/=z; // quociente
				//ALiBS.libhtm.Htm.dw("quociente(y)="+y+"<br/>");
			}
			//			
			if(j>5) break;
		}
		//
		for(var j=0;j<t.length;j++)
		{
			r = r.concat(ALiBS.ch.ascValue(t[t.length-1-j]));
			//ALiBS.libhtm.Htm.dw("t["+j+"]="+t[j]+"<br/>");
		}
		//
		return r;
	},
	// *****************************************************************
	ascValue: function(p) // int 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ch.ascValue",le:ALiBS.gv.ch.le});
		//
		if(p>=0 && p<=9)
		{
			return p.toString(); // 0 ate 9 = 10
		}
		else
		{
			if(p==10) return "a";
			else if(p==11) return "b"; 	
			else if(p==12) return "c"; 	
			else if(p==13) return "d"; 	
			else if(p==14) return "e"; 	
			else if(p==15) return "f"; 	
			else if(p==16) return "g"; 	
			else if(p==17) return "h"; 	
			else if(p==18) return "i"; 	
			else if(p==19) return "j"; 	// a ate j = 10
			else if(p==20) return "k"; 	
			else if(p==21) return "l"; 	
			else if(p==22) return "m"; 	
			else if(p==23) return "n"; 	
			else if(p==24) return "o"; 	
			else if(p==25) return "p"; 	
			else if(p==26) return "q"; 	
			else if(p==27) return "r"; 	
			else if(p==28) return "s"; 	
			else if(p==29) return "t"; 	// k ate t = 10
			else if(p==30) return "u"; 	
			else if(p==31) return "v"; 	
			else if(p==32) return "w"; 	
			else if(p==33) return "x"; 	
			else if(p==34) return "y"; 	
			else if(p==35) return "z"; 	// u ate z = 6
			else return ""; // > 35
		}
	},
	// *****************************************************************
	intValue: function(p) // ch 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ch.intValue",le:ALiBS.gv.ch.le});
		//
		//var w = p+'';
		//ALiBS.libhtm.Htm.dw("w="+w+"<br/>");
		//
		if(p=='0') return 0;
		else if(p=='1') return 1; 	
		else if(p=='2') return 2; 	
		else if(p=='3') return 3; 	
		else if(p=='4') return 4; 	
		else if(p=='5') return 5; 	
		else if(p=='6') return 6; 	
		else if(p=='7') return 7; 	
		else if(p=='8') return 8; 	
		else if(p=='9') return 9; 	
		else if(p=='a') return 10; 	
		else if(p=='b') return 11; 	
		else if(p=='c') return 12; 	
		else if(p=='d') return 13; 	
		else if(p=='e') return 14; 	
		else if(p=='f') return 15; 	
		else if(p=='g') return 16; 	
		else if(p=='h') return 17; 	
		else if(p=='i') return 18; 	
		else if(p=='j') return 19; 	
		else if(p=='k') return 20; 	
		else if(p=='l') return 21; 	
		else if(p=='m') return 22; 	
		else if(p=='n') return 23; 	
		else if(p=='o') return 24; 	
		else if(p=='p') return 25; 	
		else if(p=='q') return 26; 	
		else if(p=='r') return 27; 	
		else if(p=='s') return 28; 	
		else if(p=='t') return 29; 	
		else if(p=='u') return 30; 	
		else if(p=='v') return 31; 	
		else if(p=='w') return 32; 	
		else if(p=='x') return 33; 	
		else if(p=='y') return 34; 	
		else if(p=='z') return 35; 	
		else return -1;
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ch("+(++ALiBS.gv.ch.nn)+"x)",{o:"ALiBS.ch"});
ALiBS.sy.la(0+0,"ALiBS.gv.ch.le="+ALiBS.gv.ch.le+ALiBS.gv.sy.se,{o:"ALiBS.ch"});

