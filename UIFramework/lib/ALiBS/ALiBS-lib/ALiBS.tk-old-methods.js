/* *************************************************************************
// INFO:


// *************************************************************************
TODO(2014-05-27): 


*/
//
// *************************************************************************
//
if(!ALiBS.gv.tk) ALiBS.gv.tk = {}; // global values
//
if(!ALiBS.gv.tk.le) ALiBS.gv.tk.le = 0; // local log status
//
ALiBS.gv.tk.nn = 0; // x times loaded
//
ALiBS.gv.tk.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.gv.tk.agp = "ALiBS.wk.wsc.if.p0"; // password algorithm
	// ALiBS.wk.wsc.if.p0  = check password - Algorithm = local test
	// ALiBS.wk.wsc.if.p1 = check password - Algorithm = [Extended] Validation Functions
//
ALiBS.gv.tk.agt = "ALiBS.wk.wsc.if.t0"; // token number algorithm
	// ALiBS.wk.wsc.if.t0  = check token number - Algorithm = local test
	// ALiBS.wk.wsc.if.t1 = check token number - Algorithm = [Extended] Validation Functions TODO(2014-06-03):implementar
//
ALiBS.gv.tk.ago = "ALiBS.wk.wsc.if.o0"; // otp algorithm
	// ALiBS.wk.wsc.if.o0  = check otp - Algorithm = local test
	// ALiBS.wk.wsc.if.o1 = check otp - Algorithm = [Extended] Validation Functions TODO(2014-06-03):implementar
//
ALiBS.gv.tk.apa = ""; // application (html,authentication,token)
/*
	Página		t0-iManager	(Simula tela login iManager)
			t0-PortalWF	(Simula tela login PortalWF)
			t1
			iManager
			PortalWF

	Autenticação	[A:local]
			[A:VM.192.168.9.10]
			[A:VM.192.168.9.80]
			[A:P1.10.195.124.33]
			[A:PD.10.222.17.58]
			[A:PD.10.222.17.5]

	Token		[T:local]	(ALiBS.wk)
			[T:VM]		(using ALiBS.wk.t8 || ALiBS.wk.o8 but with fixed messages return for z.r0)
			[T:TU.10.193.103.17]
			[T:TI.10.192.72.72]
			[T:HO.10.193.65.140]
			[T:PD.bradesco.com.br]

*/
//
// *************************************************************************
//
ALiBS.gv.tk.bhv = {}; // behavior cache control
	// ALiBS.gv.tk.bhv["otp"] = 1; // keep token/otp page

ALiBS.gv.tk.r2m = {}; // msg cache control = use return message
	//ALiBS.gv.tk.r2m["UnhandledTokenMessage"] = 1; // use return message
	//ALiBS.gv.tk.r2m["UnhandledOtpMessage"] = 1 // use return message
//
// *************************************************************************
//
ALiBS.gv.tk.msg = // msg cache
{
	BackButtonPressed: "BackButtonPressed",
	//
	OTPinpuMsg:"OTP - One Time Password ", // keep space at the end - used in ALiBS.tk.st (app.js)
	//
	InvalidEmptyUser: "Invalid Empty User",
	InvalidEmptyPass: "Invalid Empty Pass",
	InvalidEmptyUserPass: "Invalid Empty User Pass", // old Invalid OTP1 - user input
	InvalidEmptyOtp: "Invalid Empty Otp", // old Invalid OTP2 - user input
	//
	InvalidUserPass: "Invalid User or Password",
	InvalidUserPassP: "Invalid User or [P]assword",
	InvalidUserPassU: "Invalid [U]ser or Password",
	//
	InvalidTokenNumber: "Invalid Token Number",
	//
	InvalidOtpLength: "Invalid Otp Length",
	InvalidOtp: "Invalid OTP",
	//
	WebSystemOffAppOutOfService: "Web System Off - Application Out Of Service for Maintenance",
	WebSystemOffNoAppInfo: "Web System Off - No Application Info",
	WebSystemOffUnhandledMessage: "Web System Off - Unhandled Message",
	//
	InvalidOUConnection: "Invalid OU Connection",
	InvalidOUAccess: "Invalid OU Access",
	InvalidOUInfo: "Invalid OU Info",
	//
	InvalidUserConnection: "Invalid User Connection",
	InvalidUserAccess: "Invalid User Access",
	InvalidUserInfo: "Invalid User Info",
	//
	EmptyPasswordMessageReturned: "Empty Password Message Returned",
	EmptyTokenMessageReturned: "Empty Token Message Returned",
	EmptyOtpMessageReturned: "Empty Otp Message Returned",
	//
	UnhandledPasswordErrorMessage: "Unhandled Password Error Message",
	UnhandledTokenErrorMessage: "Unhandled Token Error Message",
	UnhandledOtpErrorMessage: "Unhandled Otp Error Message",
	//
	UnhandledPasswordMessage: "Unhandled Password Message",
	UnhandledTokenMessage: "Unhandled Token Message",
	UnhandledOtpMessage: "Unhandled Otp Message", // old Invalid OTP3
	//
	UnhandledMessage: "Unhandled Message" // TODO(2014-06-05): 1 ocorrência
};
//
// *************************************************************************
//
ALiBS.tk = // (client) token
{
	_f0: function(p) // empty / void function - TO BE ASSIGNED TO FUNCTIONS TO BE OVERWRITTEN
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk._f0",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // ec = start
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk._f0",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	ser: function(er,p) // set error - TO BE OVERWRITTEN / EXTENDED
	// er = (required)(string) error message
	//
	// p = 
	//
	// return = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.ser",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.tk.ser",le:ALiBS.gv.tk.le});
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1100; // 
			if(er && typeof(z.xx)!="number") // clear values
			{
				z.ec = -1200; // 
				ALiBS.tk.sht(0);
				z.ec = -1300; // 
				ALiBS.tk.shu(1);
			} 
			else // clear value
			{
				z.ec = -1400; // 
				ALiBS.tk.got().value = "";
				z.ec = -1500; // 
				//ALiBS.tk.got().focus(); //TODO(2014-06-27): IE: test before set focus (shoud NOT be display:none, visibility:hidden)
			} 
			//
			z.ec = -1600; // 
			ALiBS.tk.ger().innerHTML = "<span>" + er + "</span>";
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.ser",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	stg: function(p) // // set tag token <input> - TO BE OVERWRITTEN / EXTENDED ({der:z.v0[0],dus:z.v0[0],dot:z.v0[0]})
	// p = (optional) (JSON) parameters {} 
	//	der = (optional)(ElementID) error div
	//	dus = (optional)(ElementID) user div
	//	dot = (optional)(ElementID) otp div
	//
	// ****************************************
	//
	//	se0 =  (optional)(ElementID) error separator
	//		"" = DEFAULT
	//	se1 =  (optional)(ElementID) error separator
	//		"" = DEFAULT
	//
	// ****************************************
	//
	//	su0 =  (optional)(ElementID) user separator
	//		<br/> = DEFAULT
	//	su1 =  (optional)(ElementID) user separator
	//		<br/> = DEFAULT
	//	su2 =  (optional)(ElementID) user separator
	//		"" = DEFAULT
	//
	// ****************************************
	//
	//	spa =  (optional)(number) password active
	//		1 = DEFAULT
	//	sp0 =  (optional)(ElementID) pass separator
	//		<br/> = DEFAULT
	//	sp1 =  (optional)(ElementID) pass separator
	//		<br/> = DEFAULT
	//	sp2 =  (optional)(ElementID) pass separator
	//		"" = DEFAULT
	//
	// ****************************************
	//
	//	sl0 =  (optional)(ElementID) login button separator
	//		<br/> = DEFAULT
	//	sl1 =  (optional)(ElementID) login button separator
	//		"" = DEFAULT
	//
	// ****************************************
	//
	//	sta =  (optional)(number) tree active
	//		1 = DEFAULT
	//	stx =  (optional)(string) tree name / IP
	//		"" = DEFAULT
	//	st0 =  (optional)(ElementID) tree separator
	//		<br/> = DEFAULT
	//	st1 =  (optional)(ElementID) tree separator
	//		"<br/>" = DEFAULT
	//	st2 =  (optional)(ElementID) tree separator
	//		"" = DEFAULT
	//
	// ****************************************
	//
	//	so0 =  (optional)(ElementID) otp separator
	//		<br/> = DEFAULT
	//	so1 =  (optional)(ElementID) otp separator
	//		<br/> = DEFAULT
	//	so2 =  (optional)(ElementID) otp separator
	//		<br/> = DEFAULT
	//	so3 =  (optional)(ElementID) otp separator
	//		"" = DEFAULT
	//	so4 =  (optional)(ElementID) otp separator
	//		"" = DEFAULT
	// ****************************************
	//
	// return = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.stg",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // ec = start
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.se0)!="string") z.se0 = ""; // DEFAULT 
			if(typeof(z.se1)!="string") z.se1 = ""; // DEFAULT 
			//
			if(typeof(z.su0)!="string") z.su0 = "<br/>"; // DEFAULT 
			if(typeof(z.su1)!="string") z.su1 = "<br/>"; // DEFAULT 
			if(typeof(z.su2)!="string") z.su2 = ""; // DEFAULT 
			//
			if(typeof(z.spa)!="number") z.spa = 1; // DEFAULT 
			if(typeof(z.sp0)!="string") z.sp0 = "<br/><br/>"; // DEFAULT 
			if(typeof(z.sp1)!="string") z.sp1 = "<br/>"; // DEFAULT 
			if(typeof(z.sp2)!="string") z.sp2 = ""; // DEFAULT 
			//
			if(typeof(z.sta)!="number") z.sta = 0; // DEFAULT 
			if(typeof(z.stx)!="string") z.stx = ""; // DEFAULT 
			if(typeof(z.st0)!="string") z.st0 = "<br/><br/>"; // DEFAULT 
			if(typeof(z.st1)!="string") z.st1 = "<br/>"; // DEFAULT 
			if(typeof(z.st2)!="string") z.st2 = ""; // DEFAULT 
			//
			if(typeof(z.sl0)!="string") z.sl0 = "<br/>"; // DEFAULT 
			if(typeof(z.sl1)!="string") z.sl1 = ""; // DEFAULT 
			//
			if(typeof(z.so0)!="string") z.so0 = "<br/>"; // DEFAULT 
			if(typeof(z.so1)!="string") z.so1 = "<br/>"; // DEFAULT 
			if(typeof(z.so2)!="string") z.so2 = "<br/>"; // DEFAULT 
			if(typeof(z.so3)!="string") z.so3 = ""; // DEFAULT 
			if(typeof(z.so4)!="string") z.so4 = ""; // DEFAULT 
			//
			z.ec = -1100; // 
			if(z.der)
			{
				z.ec = -1200; // 
				z.h0 = 	z.se0+'<span></span>'+
					z.se1;
				//
				z.ec = -1300; // 
				ALiBS.el.ae("divErroEID","div",z.der); // append to element
				//
				z.ec = -1400; // 
				ALiBS.el.si("divErroEID",z.h0);
			}
			//
			// ***********
			//
			z.ec = -2000; // 
			if(z.dus)
			{
				z.ec = -2100; // 
				z.h0 = 	z.su0+'<span id="userTxtEID">Usuário:</span>'+
					z.su1+'<input type=text id="userEID"></input>'+
					z.su2+
					'';
				//
				z.ec = -2200; // 
				if(z.spa==1)
				{
					z.h0 += z.sp0+'<span id="passTxtEID">Senha:</span>'+
						z.sp1+'<input type=password id="passEID"></input>'+
						z.sp2;

				}
				//
				z.ec = -2300; // 
				if(z.sta==1)
				{
					z.h0 += z.st0+'<span id="treeTxtEID">Árvore:</span>'+
						z.st1+'<input type=text id="treeEID" value="'+z.stx+'"></input>'+
						z.st2;

				}
				//
				z.ec = -2400; // 
				z.h0 += z.sl0+'<input id="butEID" type=button value="login" onclick="login_x()">'+
					z.sl1;
				//
				z.ec = -2800; // 
				ALiBS.el.ae("divUserEID","div",z.dus); // append to element 
				//
				z.ec = -2900; // 
				ALiBS.el.si("divUserEID",z.h0); // element.innerHTML
			}
			//
			// ***********
			//
			z.ec = -3000; // 
			if(z.dot)
			{
				z.ec = -3100; // 
				z.h0 = 	z.so0+'<span id="otptxtEID">OTP:</span>'+
					z.so1+'<input type=password id="otpEID"></input>'+
					z.so2+'<input id="butOtpEID" type=button value="login" onclick="login()">'+
					z.so3+'<input id="butCancelEID" type=button value="Voltar" onclick="ALiBS.tk.bak()">'+
					z.so4;
				//
				z.ec = -3200; // 
				ALiBS.el.ae("divOtpEID","div",z.dot); // append to element
				//
				z.ec = -3300; // 
				ALiBS.el.si("divOtpEID",z.h0);
			}
			//
			ALiBS.tk.sht(0); // hide otp/login
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.stg",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	bak: function(p) // back button in OTP 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.bak",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // ec = start
			ALiBS.tk.sof(); 
			z.ec = -1100; // 
			ALiBS.tk.sht(0); // hide otp/login
			z.ec = -1200; // 
			ALiBS.tk.shu(1); // show user/password/tree/login
			//
			z.ec = -1300; // 
			ALiBS.tk.ser(ALiBS.gv.tk.msg["BackButtonPressed"]);
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.bak",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	shu: function(v,p) // show user / password <input>
	// v = (required)(number)
	//	0 = hide
	//	1 = show
	//
	// p = 
	//
	// return = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.shu",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.tk.shu",le:ALiBS.gv.tk.le});
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1100; // 
			if(v==0)
			{
				z.vx = "hide";
			}
			else
			{
				z.ec = -1200; // 
				ALiBS.tk.gus().value = ""; // clear user input
				z.ec = -1300; // 
				//ALiBS.tk.gus().focus(); //TODO(2014-06-27): IE: test before set focus (shoud NOT be display:none, visibility:hidden)
				z.ec = -1400; // 
				ALiBS.tk.gpa().value = ""; // clear pass input
				//
				z.vx = "show";
			}
			//
			z.ec = -1500; // 
			z.v2 = ALiBS.tk.gdu();
			//
			z.ec = -1600; // 
			ALiBS.st.st(z.v2,z.vx); // hide input fields
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.shu",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	sht: function(v,p) // show token otp <input>
	// v = (required)(number)
	//	0 = hide
	//	1 = show
	//
	// p = 
	//
	// return = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.sht",le:ALiBS.gv.tk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1000; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.tk.sht",le:ALiBS.gv.tk.le});
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1100; // 
			if(v==0)
			{
				z.vx = "hide";
			}
			else
			{
				z.ec = -1200; // 
				ALiBS.tk.got().value = ""; // clear user input
				z.ec = -1300; // 
				//ALiBS.tk.got().focus(); //TODO(2014-06-27): IE: test before set focus (shoud NOT be display:none, visibility:hidden)
				//
				z.vx = "show";
			}
			//
			//z.v1 = ALiBS.el.ge("otptxtEID"); deveria esconder tambem?
			//
			z.ec = -1500; // 
			z.v2 = ALiBS.tk.gdo();
			//
			z.ec = -1600; // 
			ALiBS.st.st(z.v2,z.vx); // hide input fields
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tk.sht",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	rpa: function() // password validation
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.rpa",le:ALiBS.gv.tk.le});
		//
		var z = {};
		//
		var r = "";
		//
		z.us = ALiBS.tk.gus().value;
		z.pa = ALiBS.tk.gpa().value;
		//
		//alert("\n(1)user="+user+"\npass="+pass); // TODO(2014-04-10):Comentar esta linha, usada somente para testes
		//
		if(!z.us || !z.pa || !ALiBS.sg.tr(z.us) || !ALiBS.sg.tr(z.pa))
		{
			ALiBS.tk.sof(); 
			ALiBS.tk.shu(1); // show user/password/tree/login
			ALiBS.tk.sht(0); // hide otp/login
			//
			     if(!z.us || !ALiBS.sg.tr(z.us)) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyUser"]);
			else if(!z.pa || !ALiBS.sg.tr(z.pa)) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyPass"]);
			else 					ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyUserPass"]);
		}
		else
		{
			z.u0 = new Array();
			z.u0[z.u0.length] = ALiBS.gv.tk.u0;
			z.u0[z.u0.length] = "?cmd=" + ALiBS.gv.tk.agp; 
			z.u0[z.u0.length] = "&cb=ALiBS.tk.cpa"; 
			z.u0[z.u0.length] = "&p1=" + z.us; 
			z.u0[z.u0.length] = "&p2=" + z.pa; 
			z.u0[z.u0.length] = "&apa=" + ALiBS.gv.tk.apa; 
			//
			ALiBS.ag.ex(z.u0.join(""),"s",{zpf:"cmd=",ut:true}); // compacta a partir do parâmetro p1
		}
		//
		return r;
	},
	// *****************************************************************
	cpa: function(s) // password validation call back
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.cpa",le:ALiBS.gv.tk.le});
		//
		var z = {};
		//
		var r = "";
		//
		z.us = ALiBS.tk.gus().value;
		//
		if(!s || !ALiBS.sg.tr(s) || s.indexOf("[TOKEN-O")<0) // password failed
		{
			if(!s || !ALiBS.sg.tr(s))			ALiBS.tk.ser(ALiBS.gv.tk.msg["EmptyPasswordMessageReturned"]);
			else if(s.indexOf("[SYSTEM-OFF][NO-APP-INFO]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["WebSystemOffNoAppInfo"]);
			else if(s.indexOf("[SYSTEM-OFF][APP-OUT-OF-SERVICE]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["WebSystemOffAppOutOfService"]);
			else if(s.indexOf("[SYSTEM-OFF]")>=0) 		ALiBS.tk.ser(ALiBS.gv.tk.msg["WebSystemOffUnhandledMessage"]);
			//
			else if(s.indexOf("[ERROR][CONNECTION][OU]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOUConnection"]);
			else if(s.indexOf("[ERROR][NO-ACCESS][OU]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOUAccess"]);
			else if(s.indexOf("[ERROR][NO-INFO][OU]")>=0) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOUInfo"]);
			else if(s.indexOf("[ERROR][CONNECTION][USER]")>=0)ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserConnection"]);
			else if(s.indexOf("[ERROR][NO-ACCESS][USER]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserAccess"]);
			else if(s.indexOf("[ERROR][NO-INFO][USER]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserInfo"]);
			//
			else if(s.indexOf("[ERROR][PASS][P]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserPassP"]);
			else if(s.indexOf("[ERROR][PASS][U]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserPassU"]);
			else if(s.indexOf("[ERROR][PASS]")>=0)		ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidUserPass"]);
			//
			else if(s.indexOf("[ERROR]")>=0)		ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledPasswordErrorMessage"]);
			else						ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledPasswordMessage"]); // other error
			//
			ALiBS.tk.sof(); 
			ALiBS.tk.sht(0); // hide otp/login
			//
			if(s.indexOf("[SYSTEM-OFF]")>=0) // if [SYSTEM-OFF] hide user/password
			{
				ALiBS.tk.shu(0); // hide user/password/tree/login
			}
			else // starts again
			{
				ALiBS.tk.shu(1); // show user/password/tree/login
			}
		}
		else // [TOKEN-OFF]    [TOKEN-ON]
		{
			if(s.indexOf("[TOKEN-OFF]")>=0) // password OK + token NOT required // [TOKEN-OFF][U][H]    [TOKEN-OFF][U][A]    [TOKEN-OFF][S]
			{
				//z.us = ALiBS.tk.gus().value;
				//z.pa = ALiBS.tk.gpa().value;
				//z.ot = ALiBS.tk.got().value;
				//z.otx = ALiBS.tk.gtx().innerHTML;
				//
				//alert("\n(2)user="+z.us+"\npass="+z.pa+"\notp="+z.ot+"\notptxt="+z.otx);
				//
				ALiBS.tk.sot(); //document.forms[0].onsubmit=ALiBS.tk.sot;
				ALiBS.tk.sub(); //document.forms[0].submit();
			}
			else if(s.indexOf("[TOKEN-ON]")>=0) // [TOKEN-ON]== password OK + token required = "cmd=2000" = test
			{
				z.u0 = new Array();
				z.u0[z.u0.length] = ALiBS.gv.tk.u0;
				z.u0[z.u0.length] = "?cmd=" + ALiBS.gv.tk.agt; 
				z.u0[z.u0.length] = "&cb=ALiBS.tk.ctk"; 
				z.u0[z.u0.length] = "&p1=" + z.us; 
				//z.u0[z.u0.length] = "&p2=" + z.pa; // NOT REQUIRED 
				z.u0[z.u0.length] = "&apa=" + ALiBS.gv.tk.apa; 
				//
				ALiBS.ag.ex(z.u0.join(""),"s",{zpf:"cmd=",ut:true}); // compacta a partir do parâmetro p1
			}
		}
		//
		return r;
	},
	// *****************************************************************
	ctk: function(s) // token number validation call back
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.ctk",le:ALiBS.gv.tk.le});
		//
		var z = {};
		//
		var r = "";
		//
		if(!s || !ALiBS.sg.tr(s) || s.indexOf("[OK][TOKEN]")<0) // token number failed
		{
			     if(!s || !ALiBS.sg.tr(s))		ALiBS.tk.ser(ALiBS.gv.tk.msg["EmptyTokenMessageReturned"]);
			else if(s.indexOf("[ERROR][TOKEN]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidTokenNumber"]);
			else if(s.indexOf("[ERROR]")>=0)	ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledTokenErrorMessage"]);
			else
			{
				if(!ALiBS.gv.tk.r2m["UnhandledTokenMessage"])		ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledTokenMessage"]); // token number failed
				else if(ALiBS.gv.tk.r2m["UnhandledTokenMessage"]==1)	ALiBS.tk.ser(s); 
				else							ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledTokenMessage"]); // token number failed
			}
			//
			ALiBS.tk.sof(); 
			ALiBS.tk.shu(1); // show user/password/tree/login
			ALiBS.tk.sht(0); // hide otp/login
		}
		else // if(s.indexOf("[OK][TOKEN]")>=0) // token number OK //[OK][TOKEN][000-0]    [OK][TOKEN][007-3]
		{
			ALiBS.tk.ser("");	
			//
			ALiBS.tk.sok(); //document.forms[0].onsubmit=ALiBS.tk.sok;
			//
			ALiBS.tk.shu(0); // hide user/password/tree/login
			//
			ALiBS.tk.sht(1); // show otp/login
			//
			z.v0 = ALiBS.sg.bs(  ALiBS.sg.as(s,"[OK][TOKEN][") , "]"  );
			//
			ALiBS.tk.stn(z.v0);		
			//
			ALiBS.tk.stf();	
		}
		//
		return r;
	},
	// *****************************************************************
	rob: function() // otp request - button otptxt
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.rob",le:ALiBS.gv.tk.le});
		//
		var z = {};
		//
		var r = "";
		//
		z.us = ALiBS.tk.gus().value;
		z.ot = ALiBS.tk.got().value;
		//
		//alert("\n(3)otpuser="+user+"\npass="+pass);
		//
		if(!z.ot || !ALiBS.sg.tr(z.ot))
		{
			ALiBS.tk.sof(); 
			ALiBS.tk.shu(1); // show user/password/tree/login
			ALiBS.tk.sht(0); // hide otp/login
			//
			ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidEmptyOtp"]);	
		}
		else
		{
			z.u0 = new Array();
			z.u0[z.u0.length] = ALiBS.gv.tk.u0;
			z.u0[z.u0.length] = "?cmd=" + ALiBS.gv.tk.ago; 
			z.u0[z.u0.length] = "&cb=ALiBS.tk.cot"; 
			z.u0[z.u0.length] = "&p1=" + z.us; 
			z.u0[z.u0.length] = "&p2=" + z.ot; 
			z.u0[z.u0.length] = "&apa=" + ALiBS.gv.tk.apa; 
			//
			ALiBS.ag.ex(z.u0.join(""),"s",{zpf:"cmd=",ut:true}); // compacta a partir do parâmetro p1
		}
		//
		return r;
	},
	// *****************************************************************
	cot: function(s) // otp validation call back
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tk.cot",le:ALiBS.gv.tk.le});
		//
		var z = {};
		//
		var r = "";
		//
		if(!s || !ALiBS.sg.tr(s) || s.indexOf("[OK][OTP]")<0) // otp failed
		{
			     if(!s || !ALiBS.sg.tr(s))		ALiBS.tk.ser(ALiBS.gv.tk.msg["EmptyOtpMessageReturned"]); // otp failed
			else if(s.indexOf("[ERROR][OTP][O]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"]);
			else if(s.indexOf("[ERROR][OTP][U]")>=0) ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"]);
			else if(s.indexOf("[ERROR][OTP]")>=0) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["InvalidOtp"]);
			else if(s.indexOf("[ERROR]")>=0) 	ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledOtpErrorMessage"]);
			else
			{
				if(!ALiBS.gv.tk.r2m["UnhandledOtpMessage"])		ALiBS.tk.ser(ALiBS.gv.tk.msg["UnhandledOtpMessage"]); // otp failed
				else							ALiBS.tk.ser(s,{xx:1}); // ALiBS.gv.tk.r2m["UnhandledOtpMessage"] = 1
			}
			//
			if(!ALiBS.gv.tk.bhv["otp"])
			{
				ALiBS.tk.sof(); //document.forms[0].onsubmit=ALiBS.tk.sof;
				ALiBS.tk.shu(1); // show user/password/tree/login
				ALiBS.tk.sht(0); // hide otp/login
			}
			else // !ALiBS.gv.tk.bhv["otp"] = 1
			{
				// keep token/otp page
			}
		}
		else // if(s.indexOf("[OK][OTP]")>=0) // otp OK 
		{
			z.us = ALiBS.tk.gus().value;
			z.pa = ALiBS.tk.gpa().value;
			z.ot = ALiBS.tk.got().value;
			z.otx = ALiBS.tk.gtx().innerHTML;
			//
			//alert("\n(4)user="+z.us+"\npass="+z.pa+"\notp="+z.ot+"\notptxt="+z.otx);
			//
			ALiBS.tk.sot(); //document.forms[0].onsubmit=ALiBS.tk.sot;
			ALiBS.tk.sub(); //document.forms[0].submit();
		}
		//
		return r;
	}
	// *****************************************************************
};
//
// *************************************************************************
//
ALiBS.tk.gus = function(){return ALiBS.el.ge("userEID");}; // get user element
ALiBS.tk.gpa = function(){return ALiBS.el.ge("passEID");}; // get pass element
ALiBS.tk.gdu = function(){return ALiBS.el.ge("divUserEID");}; // get div user element
//
ALiBS.tk.got = function(){return ALiBS.el.ge("otpEID");}; // get otp element
ALiBS.tk.gtx = function(){return ALiBS.el.ge("otptxtEID");}; // get otp txt lable element
ALiBS.tk.gdo = function(){return ALiBS.el.ge("divOtpEID");}; // get div otp element
//
ALiBS.tk.ger = function(){return ALiBS.el.ge("divErroEID");}; // get error element
//
ALiBS.tk.gbl = function(){return ALiBS.el.ge("butEID");}; // get button login() element
ALiBS.tk.gbo = function(){return ALiBS.el.ge("butOtpEID");}; // get button otplogin() element
//
ALiBS.tk.sok = ALiBS.tk._f0; // [r]of - set on submit = otp validation - ALiBS.tk.rob() + return false
//
// *************************************************************************
//
ALiBS.tk.stn = function(tn){ALiBS.tk.gtx().innerHTML = "<span>" + ALiBS.gv.tk.msg["OTPinpuMsg"] + tn + "</span>";}; // set token number
//
ALiBS.tk.stf = function(){ALiBS.tk.got().focus();}; // // set token focus
//
ALiBS.tk.sof = function(){ALiBS.tk.gbl().onclick = ALiBS.tk.gbo().onclick = function(){ALiBS.tk.rpa();return false;};}; // set onsubmit false
//
ALiBS.tk.sot = function(){ALiBS.tk.gbl().onclick = ALiBS.tk.gbo().onclick = function(){login()};}; // set onsubmit true
//
ALiBS.tk.sub = function(){ALiBS.tk.sht(0);ALiBS.tk.gbl().onclick();} // run submit()
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.tk("+(++ALiBS.gv.tk.nn)+"x)",{o:"ALiBS.tk"});
ALiBS.sy.la(0+0,"ALiBS.gv.tk.le="+ALiBS.gv.tk.le+ALiBS.gv.sy.se,{o:"ALiBS.tk"});
//
// *************************************************************************
//
//	SAMPLE OF FUNCTIONS TO BE OVERWRITTEN / EXTENDED
/*

*/
//
// *************************************************************************

