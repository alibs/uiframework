/* *************************************************************************
// INFO:

Used by:

Rhino command line
jswebjetty
jswebserver
novell drivers

// *************************************************************************
// TODO(2010-09-08):

TODO(2010-09-08): 

http://download.oracle.com/javase/1.4.2/docs/api/java/lang/System.html#getProperties()

file.separator	File separator ("/" on UNIX)
path.separator	Path separator (":" on UNIX)
line.separator	Line separator ("\n" on UNIX)

*/
//
// *************************************************************************
//
if(!ALiBS.gv.wi) ALiBS.gv.wi = {}; // global values
//
if(!ALiBS.gv.wi.le) ALiBS.gv.wi.le = 0; // local log status
//
ALiBS.gv.wi.nn = 0; // x times loaded
//
ALiBS.gv.wi.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
// *************************************************************************
//
ALiBS.wi = // server info 
{
	gi: function(p) // get Info = server info
	// p = (optional)(JSON)parameters {} 
	//	rq = (JSON)requestHttp
	//	se = (char)separator \n <br>
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.gi",le:ALiBS.gv.wi.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		try
		{
			z.ec = -1000; // 
			//  DEFAULTS [BEST-PRACTICE]
			if(typeof(z.se)=="undefined" || !z.se) z.se = ALiBS.gv.sy.se; // DEFAULT
			//
			//z.ec = -1002; // 
			//if(typeof(z.rq)!="undefined") // DO NOT VALIDATE HERE = ERROR if(typeof(z.rq)=="undefined") = very strange
			//
			// local variables [BEST-PRACTICE]
			z.ec = -1010; // 
			z.s = new Array();
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.wi.gi",le:ALiBS.gv.wk.le});
			//
			z.ec = -1100; // 
			z.gm = ALiBS.wi.gm();
			//
			z.ec = -1200; // 
			z.s[z.s.length] = z.se + z.se + "Number-of-processors = "+z.gm.pr;
			z.ec = -1300; // 
			z.s[z.s.length] = z.se + "JVM-max-memory = "+ALiBS.wi.fm(z.gm.jm)+" bytes";
			z.ec = -1400; // 
			z.s[z.s.length] = z.se + "Allocated-memory = "+ALiBS.wi.fm(z.gm.al)+" bytes";
			//
			z.ec = -1500; // 
			z.s[z.s.length] = z.se + "Allocated-free = "+ALiBS.wi.fm(z.gm.af)+" bytes ("+(""+Math.round(z.gm.pf)).substring(0,2)+"%)";
			z.ec = -1600; // 
			z.s[z.s.length] = z.se + "Allocated-used = "+ALiBS.wi.fm(z.gm.au)+" bytes ("+(""+Math.round(z.gm.pu)).substring(0,2)+"%)";
			//
			z.ec = -2000; // 
			z.gu = ALiBS.wi.gu();
			//
			z.s[z.s.length] = z.se + z.se + "user.name = "+z.gu.un;
			z.s[z.s.length] = z.se + "user.home = "+z.gu.uh;
			z.s[z.s.length] = z.se + "user.dir = "+z.gu.ud;
			//
			z.ec = -3000; // 
			z.go = ALiBS.wi.go();
			//
			z.s[z.s.length] = z.se + z.se + "os.name = "+z.go.on;
			z.s[z.s.length] = z.se + "os.arch = "+z.go.oa;
			z.s[z.s.length] = z.se + "os.version = "+z.go.ov;
			//
			z.ec = -4000; // 
			z.gp = ALiBS.wi.gp();
			//
			z.s[z.s.length] = z.se + z.se + "java.library.path = "+z.gp.lp;
			z.s[z.s.length] = z.se + "java.io.tmpdir = "+z.gp.td;
			z.s[z.s.length] = z.se + "java.ext.dirs = "+z.gp.ed;
			z.s[z.s.length] = z.se + "java.home = "+z.gp.ho;
			//
			z.s[z.s.length] = z.se + z.se + "java.class.path = "+z.gp.cp;
			//
			z.ec = -5000; // 
			z.gj = ALiBS.wi.gj();
			//
			z.s[z.s.length] = z.se + z.se + "java.version = " +z.gj.jvs;
			z.s[z.s.length] = z.se + "java.class.version = " +z.gj.jcv;
			z.s[z.s.length] = z.se + "java.compiler = " +z.gj.jc;
			//
			z.s[z.s.length] = z.se + "java.specification.version = " +z.gj.jsv;
			z.s[z.s.length] = z.se + "java.specification.vendor = " +z.gj.jsd;
			z.s[z.s.length] = z.se + "java.specification.name = " +z.gj.jsn;
			//
			z.s[z.s.length] = z.se + "java.vendor = " +z.gj.jvd;
			z.s[z.s.length] = z.se + "java.vendor.url = " +z.gj.jvu;
			//
			z.s[z.s.length] = z.se + "java.vm.version = " +z.gj.vmvs;
			z.s[z.s.length] = z.se + "java.vm.vendor = " +z.gj.vmvd;
			z.s[z.s.length] = z.se + "java.vm.name = " +z.gj.vmn;
			z.s[z.s.length] = z.se + "java.vm.specification.version = " +z.gj.vmsvs;
			z.s[z.s.length] = z.se + "java.vm.specification.vendor = " + z.gj.vmsvd;
			z.s[z.s.length] = z.se + "java.vm.specification.name = " +z.gj.vmsn;
			//
			z.ec = -6000; // 
			z.gs = ALiBS.wi.gs({rq:z.rq}); // new parameter name
			//
			z.ec = -6100; // 
			if(z.gs)
			{
				z.s[z.s.length] = z.se;
				//
				for(var i=0;i<z.gs.length;i++)
				{
					z.s[z.s.length] = z.se + "(" + z.gs[i].f02 + ")" + z.gs[i].f00 + "=" + z.gs[i].f01;
				}
			}
			else
			{
				z.ec = -6200; // 
				z.gs = ALiBS.wi.gs({rq:z.r}); // old parameter name = compatibility
				//
				z.ec = -6300; // 
				if(z.gs.si)
				{
					z.s[z.s.length] = z.se + z.se + "getServerInfo = " + z.gs.si;
					//z.s[z.s.length] = z.se + "getAttributeNames = "+z.gs.an;
				}
			}
			//
			z.ec = -7000; // 
			r = z.s.join("");
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wi.gi",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
		//
		/* TODO(2011-05-20): environment é criado por Envjs????
		for(var i in environment)
		{
			z.s[z.s.length] = "<br>"+ i +" = "+environment[i];
		}
		*/
		//return z.s.join("");
	},
	// *****************************************************************
	gm: function() // get JVM memory information
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.gm",le:ALiBS.gv.wi.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.rt = java.lang.Runtime.getRuntime();
		//
		var r = {};
		//
		r.pr = z.rt.availableProcessors(); // Number-of-processors
		r.jm = z.rt.maxMemory(); // JVM-max-memory
		r.al = z.rt.totalMemory(); // Allocated-memory
		r.af = z.rt.freeMemory(); // Allocated-free
		//
		r.au = r.al-r.af; // Allocated-used
		r.pf = r.af*100/r.al; // Allocated-free-percent - free*100/total
		r.pu = r.au*100/r.al; // Allocated-used-percent	- used*100/total		
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	gp: function() // get JVM path information
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.gp",le:ALiBS.gv.wi.le});
		//
		var r = {};
		//
		r.cp = java.lang.System.getProperty("java.class.path");
		r.lp = java.lang.System.getProperty("java.library.path");
		r.td = java.lang.System.getProperty("java.io.tmpdir");
		r.ed = java.lang.System.getProperty("java.ext.dirs");
		r.ho = java.lang.System.getProperty("java.home");
		//
		return r;
	},
	// *****************************************************************
	gu: function() // get JVM user information
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.gu",le:ALiBS.gv.wi.le});
		//
		var r = {};
		//
		r.un = java.lang.System.getProperty("user.name");
		r.uh = java.lang.System.getProperty("user.home");
		r.ud = java.lang.System.getProperty("user.dir");
		//
		return r;
	},
	// *****************************************************************
	go: function() // get JVM O.S. information
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.go",le:ALiBS.gv.wi.le});
		//
		var r = {};
		//
		r.on = java.lang.System.getProperty("os.name"); 
		r.oa = java.lang.System.getProperty("os.arch"); 
		r.ov = java.lang.System.getProperty("os.version"); 
		//
		return r;
	},
	// *****************************************************************
	gj: function(p) // get Java information
	// p = (optional)(JSON)parameters {} 
	//	r = (JSON)requestHttp
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.gj",le:ALiBS.gv.wi.le});
		//
		var r = {};
		//
		r.jvs = java.lang.System.getProperty("java.version");
		r.jcv = java.lang.System.getProperty("java.class.version");
		r.jc = java.lang.System.getProperty("java.compiler");
		//
		r.jsv = java.lang.System.getProperty("java.specification.version");
		r.jsd = java.lang.System.getProperty("java.specification.vendor");
		r.jsn = java.lang.System.getProperty("java.specification.name");
		//
		r.jvd = java.lang.System.getProperty("java.vendor");
		r.jvu = java.lang.System.getProperty("java.vendor.url");
		//
		r.vmvs = java.lang.System.getProperty("java.vm.version");
		r.vmvd = java.lang.System.getProperty("java.vm.vendor");
		r.vmn = java.lang.System.getProperty("java.vm.name");
		r.vmsvs = java.lang.System.getProperty("java.vm.specification.version");
		r.vmsvd = java.lang.System.getProperty("java.vm.specification.vendor");
		r.vmsn = java.lang.System.getProperty("java.vm.specification.name");
		//
		return r;
	},
	// *****************************************************************
	gs: function(p) // get JVM servlet information (Tomcat, Jetty, etc)
	// p = (optional)(JSON)parameters {} 
	//	rq = (JSON)requestHttp
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.gs",le:ALiBS.gv.wi.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = null; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		try
		{
			z.ec = -1000; // 
			//
			//  DEFAULTS [BEST-PRACTICE]
			if(typeof(z.se)=="undefined" || !z.se) z.se = "<br>";
			//if(typeof(z.rq)=="undefined") z.rq = null; // DO NOT VALIDATE HERE = ERROR
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.wi.gi",le:ALiBS.gv.wk.le});
			//
			z.ec = -1100; // 
			try
			{
				// http://docs.oracle.com/javaee/6/api/javax/servlet/ServletContext.html
				//
				z.x0 = z.rq.getSession(true).getServletContext(); 
				//
				if(!r)
				{
					r = [];
				}
				//
				r[r.length] = 
					{
						f00:"getServerInfo",
						f01:z.x0.getServerInfo(),
						f02:"ServletContext"
					};
				r[r.length] = 
					{
						f00:"getContextPath",
						f01:z.x0.getContextPath(),
						f02:"ServletContext"
					};
				r[r.length] = 
					{
						f00:"getServletContextName",
						f01:z.x0.getServletContextName(),
						f02:"ServletContext"
					};
			}
			catch(e)
			{
			}
			//
			z.ec = -1200; // 
			try
			{
				// http://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html
				//
				if(!r)
				{
					r = [];
				}
				//
				r[r.length] = 
					{
						f00:"method",
						f01:z.rq["method"],
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"servletPath",
						f01:z.rq["servletPath"],
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"requestURI",
						f01:z.rq["requestURI"],
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"serverPort",
						f01:z.rq["serverPort"],
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"serverName", 
						f01:z.rq["serverName"], // 127.0.0.1 OR www.meusite.com
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"requestURL",
						f01:z.rq["requestURL"], // http://127.0.0.1:7777/uri2 OR http://www.meusite.com:7777/uri2
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"headers-Host",
						f01:z.rq["headers"]["Host"],
						f02:"ServletRequest js"
					};
				r[r.length] = 
					{
						f00:"headers-Origin",
						f01:z.rq["headers"]["Origin"],
						f02:"ServletRequest js"
					};
					//
					// *********************************
					//
				r[r.length] = 
					{
						f00:"getServerInfo",
						f01:z.rq.getServerInfo(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getServerName",
						f01:z.rq.getServerName(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getServerPort",
						f01:z.rq.getServerPort(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getCharacterEncoding",
						f01:z.rq.getCharacterEncoding(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getContentType",
						f01:z.rq.getContentType(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getLocalAddr",
						f01:z.rq.getLocalAddr(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getLocalName",
						f01:z.rq.getLocalName(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getProtocol",
						f01:z.rq.getProtocol(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRemoteAddr",
						f01:z.rq.getRemoteAddr(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRemoteHost",
						f01:z.rq.getRemoteHost(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRemotePort",
						f01:z.rq.getRemotePort(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"getScheme",
						f01:z.rq.getScheme(),
						f02:"ServletRequest"
					};
				r[r.length] = 
					{
						f00:"isSecure",
						f01:z.rq.isSecure(),
						f02:"ServletRequest"
					};
			}
			catch(e)
			{
			}
			//
			z.ec = -1300; // 
			try
			{
				//http://docs.oracle.com/javaee/6/api/javax/servlet/http/HttpServletRequest.html
				//
				if(!r)
				{
					r = [];
				}
				//
				r[r.length] = 
					{
						f00:"getAuthType",
						f01:z.rq.getAuthType(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getContextPath",
						f01:z.rq.getContextPath(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getMethod",
						f01:z.rq.getMethod(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getPathInfo",
						f01:z.rq.getPathInfo(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getPathTranslated",
						f01:z.rq.getPathTranslated(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getQueryString",
						f01:z.rq.getQueryString(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRemoteUser",
						f01:z.rq.getRemoteUser(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRequestedSessionId",
						f01:z.rq.getRequestedSessionId(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRequestURI",
						f01:z.rq.getRequestURI(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getRequestURL",
						f01:z.rq.getRequestURL(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getServletPath",
						f01:z.rq.getServletPath(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"getUserPrincipal",
						f01:z.rq.getUserPrincipal(),
						f02:"HttpServletRequest"
					};
				r[r.length] = 
					{
						f00:"isRequestedSessionIdValid",
						f01:z.rq.isRequestedSessionIdValid(),
						f02:"HttpServletRequest"
					};
				//
				/* long	getDateHeader(java.lang.String name) 
					Returns the value of the specified request header as a long value that represents a Date object.

				 java.util.Enumeration<java.lang.String>	getHeaderNames() 
					  Returns an enumeration of all the header names this request contains.

				 java.util.Enumeration<java.lang.String>	getHeaders(java.lang.String name) 
					  Returns all the values of the specified request header as an Enumeration of String objects.

				 int	getIntHeader(java.lang.String name) 
					  Returns the value of the specified request header as an int.

				 HttpSession	getSession() 
					  Returns the current session associated with this request, or if the request does not have a session, creates one.
				 HttpSession	getSession(boolean create) 
					  Returns the current HttpSession associated with this request or, if there is no current session and create is true, returns a new session.

				 boolean	isUserInRole(java.lang.String role) 
					  Returns a boolean indicating whether the authenticated user is included in the specified logical "role".

				*/
			}
			catch(e)
			{
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wi.gs",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		//ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wi.gs",le:ALiBS.gv.wk.le});
		//
		return r; 
	},
	// *****************************************************************
	/* TODO(2011-05-20): environment é criado por Envjs????
	ge: function(p) // get js.jar environment
	// p = (optional)(JSON)parameters {} 
	//	r = (JSON)requestHttp
	{
		var r = {};
		//
		if(p)
		{
			if(p.r)
			{
				var x = p.r.getSession(true).getServletContext();
				r.si = x.getServerInfo(); 
				//r.an = x.getAttributeNames(); 
			}
		}
		//
		return r;
	},*/
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	fm: function(s) // TODO(2011-05-16): pra que serve?
	// s = (required)(String)s
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.fm",le:ALiBS.gv.wi.le});
		//
		if(!s) return s;
		if(typeof(s)=="number") s = ""+s;
		if(typeof(s)!="string") return s;
		if(s.length<4) return s;
		//
		var l = s.length;
		var b = new Array();
		var c = new Array();
		var n = 0;
		//
		for(var i=l-1;i>=0;i--)
		{
			b[b.length] = s[i];
			n++;
			//
			if(n%3==0 && i!=0) b[b.length] = ".";
		}
		//
		l = b.length;
		//
		for(var i=l-1;i>=0;i--)
		{
			c[c.length] = b[i];
		}
		//
		return c.join("");		
	},
	// *****************************************************************
	wsc: function(cc,rq,rp,p) // ws cmd - JVMInfo 
	// cc = (required)(connection) Connection Control / Session (javax.naming.directory.DirContext)
	// rq = (required)(HttpServletRequest) page request
	// rp = (required)(HttpServletResponse) page response
	//
	//requestHttp TODO(2014-06-03): needed?
	//responseHttp TODO(2014-06-03): needed?
	//
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	// return = (string) return value
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wi.wsc",le:ALiBS.gv.ws.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = z.cmd + " Registered but NOT found in ALiBS.wi.wsc()"; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -800; // 
			//
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -900; 
			z.gi = ALiBS.wi.gi({rq:rq,se:ALiBS.gv.sy.se}); // 1003
			//
			z.ec = -1001; 
			z.cmd0 = z.cmd.replace("ALiBS.wi.",""); // remains p8, t8, o8, etc
			//
			z.ec = -1050; 
			if(z.cmd0 && ALiBS.wi[z.cmd0])
			{
				z.ec = -1070; 
				r = ALiBS.wi[z.cmd0](cc,rq,rp,p);
			}
			else if(z.cmd.indexOf("1003")>=0 || z.cmd.indexOf("JVMInfo")>=0)
			{
				z.ec = -1100; 
				r = "(ALiBS.ws.js)JVM Memory Information: " + z.gi; // 1003
			}
			else if(z.cmd.indexOf("1004")>=0)
			{
				z.ec = -1200; 
				r = ALiBS.sg.as( ALiBS.sg.bs(z.gi,"user.name") ,"Number-of-processors");
			}
			else
			{
				//
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wi.wsc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wi.wsc",le:ALiBS.gv.ws.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	}
};
// *************************************************************************
//
if(ALiBS.gv.ws) // add urls to ws cache
{
	ALiBS.gv.ws.uc["ALiBS.wi.wsc.if.JVMInfo"] = ALiBS.wi.wsc; // JVMInfo
	//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wi.wsc.if.JVMInfo
	//
	ALiBS.gv.ws.uc["ALiBS.wi.wsc.if.1003"] = ALiBS.wi.wsc; // JVMInfo
	ALiBS.gv.ws.uc["1003"] = ALiBS.wi.wsc; // OLD compatibility
	//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wi.wsc.if.1003
	//
	ALiBS.gv.ws.uc["ALiBS.wi.wsc.if.1004"] = ALiBS.wi.wsc; // processors + memory
	ALiBS.gv.ws.uc["1004"] = ALiBS.wi.wsc; // OLD compatibility
	//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wi.wsc.if.1004
	//
	ALiBS.gv.ws.uc["ALiBS.wi.wsc.if.1101"] = ALiBS.wi.wsc; // Error test for ALiBS.wi.wsc
	//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wi.wsc.if.1101
	//
	//
	//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wi.wsc.if.1102
	//cb('1102 NOT Registered in ALiBS.gv.ws.uc');
}
//
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_wi_gi")>=0)
		{
			ALiBS_wi_gi = function() // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_wi_gi",le:ALiBS.gv.wi.le});
				//
				var gi = ALiBS.wi.gi({se:ALiBS.gv.sy.se});
				//
				ALiBS.sy.la(5+0,"Info-Java-Memory:\n"+gi,{o:"ALiBS_wi_gi",le:ALiBS.gv.wi.le});
				//
				ALiBS.sy.la(1+0,"",{so:"pr"+"int"});
				//
				return gi;
			};
		}
	}
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wi("+(++ALiBS.gv.wi.nn)+"x)",{o:"ALiBS.wi"});
ALiBS.sy.la(0+0,"ALiBS.gv.wi.le="+ALiBS.gv.wi.le+ALiBS.gv.sy.se,{o:"ALiBS.wi"});

