/* *************************************************************************
// INFO:

// *************************************************************************
// TODO

*/
//
// *************************************************************************
/** 
	Class ALiBS.n0

		Description: 
			- Required Class for All Others;
			- Creates ALiBS Namespace;
			- Modules Name Cache;
			- Log Management;

	--------------------------------------------------------------------
*/
//
if(!ALiBS.gv.n0) ALiBS.gv.n0 = {}; // global values
//
if(!ALiBS.gv.n0.le) ALiBS.gv.n0.le = 0; // local log status
//
ALiBS.gv.n0.nn = 0; // x times loaded
//
ALiBS.gv.n0.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.n0 = // Novell - Edir
{
	ch0: function(ty,dn,ts,us,de,ro,p) // XML nrfChildRoles
	/** 
	Method <static> ALiBS.n0.ch(p)

		Description: Create XML nrfChildRoles

		Parameters:
			ty = (required)(String)type (0)
			dn = (required)(String)dn
			ts = (required)(String)timestamp
			us = (required)(String)user
			de = (required)(String)description (added by iManager)
			ro = (required)(String)role
			p = (optional)(JSON) parameters {d:""} 
				d = (String)

		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.n0.ga",le:ALiBS.gv.n0.le});
		//
		var x = new Array();
		//
		x[x.length] = "<value>";
		//
		x[x.length] = "<type>" + ty + "</type>";
		x[x.length] = "<volume>" + dn + "</volume>";
		//
		x[x.length] = "<path>";
		x[x.length] = "<assignment>";
		//
		x[x.length] = "<start_tm>" + ts + "</start_tm>";
		x[x.length] = "<req>" + ALiBS.wl.df(us,",",{oou:"Segmentos,Dependencias,Departamentos,Sistema,Servers,IDM,Grupos"}) + "</req>";
		x[x.length] = "<req_desc>" + de + "</req_desc>";
		x[x.length] = "<cause><role>" + ALiBS.wl.df(ro,",",{oou:"Segmentos,Dependencias,Departamentos,Sistema,Servers,IDM,Grupos"}) + "</role></cause>";
		//
		x[x.length] = "</assignment>";
		x[x.length] = "</path>";
		//
		x[x.length] = "</value>";
		//
		return x.join("");
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.n0("+(++ALiBS.gv.n0.nn)+"x)",{o:"ALiBS.n0"});
ALiBS.sy.la(0+0,"ALiBS.gv.n0.le="+ALiBS.gv.n0.le+ALiBS.gv.sy.se,{o:"ALiBS.n0"});

