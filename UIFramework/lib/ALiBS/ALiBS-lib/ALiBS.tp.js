/* *************************************************************************
// INFO:

- Padronização de ALiBS.to para salvar em DBase

******************************

- Definição:

	- Vou usar o campo (qualquer campo) para pesquisa ?
		- caso NAO seja usado para pesquisa, então o campo pode ser:
			- criptografado (password)
			- base64
			- zipado
			- CLOB (tabela de modificações do registro, zipada, <ALiBS.t2s>)
			- tabela <ALiBS.t2s>		(OBS: se não estiver zipado/criptografado/base64, então também pode ser pesquisado)
			- json (en:"xxx",pt:"xxx")	(OBS: se não estiver zipado/criptografado/base64, então também pode ser pesquisado)

// *************************************************************************

- TODO(2013-07-08): URGENTE


- TODO(2013-07-08): NÃO URGENTE



// *************************************************************************


// *************************************************************************

*/
//
// *************************************************************************
/** 
	Class ALiBS.tp

		Description: 
			- Dinamyc Table DBase Pattern;

	--------------------------------------------------------------------
*/
// *************************************************************************
//
if(!ALiBS.gv.tp) ALiBS.gv.tp = {}; // global values
//
if(!ALiBS.gv.tp.le) ALiBS.gv.tp.le = 0; // local log status
//
ALiBS.gv.tp.nn = 0; // x times loaded
//
ALiBS.gv.tp.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.tp.rl = "15.0508.1049";
//
// *************************************************************************
//
ALiBS.tp = // table pattern
{
	cc: function(p) // creation table SQL command
	/** 
	Method <static> <string:ALiBS.tp.cc> ALiBS.tp.cc(p)

		Description: Creates a new <instance> of Dinamyc Table Object <ALiBS.tp.cc>

		Parameters:
			p = (optional)(JSON) parameters {d:"y",o:"ALiBS.el",le:true,so:print} 

				fz = (string)List of fields to include in table 
					f
					fg
					fgh
					fghi
					fghij = DEFAULT
					OBS: uvxwyz will ALWAYS be included

				dp = (number)Drop table if exists
					0 = do NOT drop table = DEFAULT
					1 = DROP table
		Observations:
	
		Return Value:
			<static> <string:ALiBS.tp.cc> Creation Table SQL Command

		Example:
			var cc = ALiBS.tp.cc(); 

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - NOT to be disclosed

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.tp.cc",le:ALiBS.gv.tp.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "alibs.tp.cc.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.tp.cc",le:ALiBS.gv.tp.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.fz)!="string")
			{
				 z.fz = "fghij"+"uvxwyz"; // DEFAULT
			}
			else
			{
				 z.fz = z.fz+"uvxwyz";
			}
			//
			if(typeof(z.dp)!="number") z.dp = 0; // DEFAULT
			//
			ALiBS.sy.la(5+0,"z.yf="+z.yf,{o:"ALiBS.tp.cc",le:ALiBS.gv.tp.le});
			ALiBS.sy.la(5+0,"z.dp="+z.dp,{o:"ALiBS.tp.cc",le:ALiBS.gv.tp.le});
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			z.cc0 = new Array();
			z.cc1 = new Array();
			//
			// ************************
			//
			for(var j=0;j<z.fz.length;j++) // from f to z
			{
				z.f0 = z.fz.charAt(j);
				//
				z.ec = -1200; // 
				for(var i=0;i<=19;i++) // 20 DECIMAL	= f00 a f19 (20)	SEM PRECISAO FIXADA
				{
				
					if(i<10) z.ii = "0"+i;
					else	 z.ii = ""+i;
					//
					z.cc0[z.cc0.length] = z.f0+z.ii+" DECIMAL";
				}
				//
				z.ec = -1300; // 
				for(var i=20;i<=20;i++) // 01 CLOB	= f20 (01)
				{
					z.cc0[z.cc0.length] = z.f0+i+" CLOB";
				}
				//
				z.ec = -1400; // 
				for(var i=21;i<=99;i++) // 79 VARCHAR	= f21 a f99 (79)	SEM TAMANHO FIXADO
				{
					z.cc0[z.cc0.length] = z.f0+i+" VARCHAR";
				}
			}
			//
			// ************************
			//
			z.ec = -1500; // 
			if(z.dp==1) // DROP TABLE
			{
				z.cc1[z.cc1.length] = "DROP TABLE IF EXISTS f00_f99;";
			}
			//
			z.ec = -1501; // 
			z.cc1[z.cc1.length] = "CREATE TABLE PUBLIC.f00_f99 (";
			//
			z.ec = -1502; // 
			z.cc1[z.cc1.length] = z.cc0.join(","); // [BEST-PRACTICE]
			//
			z.ec = -1503; // 
			z.cc1[z.cc1.length] = ");";
			//
			// ************************
			//
			z.ec = -2000; // 
			r = z.cc1.join(""); // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9900; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.tp.cc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.tp.cc",le:ALiBS.gv.tp.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.tp.cc",le:ALiBS.gv.tp.le});
		//
		return r; 
	}
	// *****************************************************************
	// *****************************************************************
};
// *************************************************************************
//
//ALiBS.tp.td = ALiBS.tp.s2t; // pointer - OLD compatibility (do NOT use var)
//
if(ALiBS.gv.sy.ev=="Server")
{
	//ALiBS_tp_tp = ... ; // pointer (do NOT use var)
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.tp("+(++ALiBS.gv.tp.nn)+"x)",{o:"ALiBS.tp"});
ALiBS.sy.la(0+0,"ALiBS.gv.tp.rl="+ALiBS.gv.tp.rl,{o:"ALiBS.tp"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.tp.le="+ALiBS.gv.tp.le+ALiBS.gv.sy.se,{o:"ALiBS.tp"});
 
