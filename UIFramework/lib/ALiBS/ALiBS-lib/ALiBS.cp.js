/* *************************************************************************
// INFO:

// *************************************************************************
// TODO(2012-06-18):

****************************************************************************
// INFO:

// *************************************************************************
// TODO:

-3.js
3a) Parâmetros para documentação (@param, @return)

4d) Implementar leitura de strings de i18n 
4e) Implementar user preferences para cores e outros usando mesma ideia de i18n
	criar skins: s0 = default (js), s1 = outro skin (js), u1 = user custom preferences (DBase)

// ***************************

-5.js 
5b) Nas aplicações, remover scripts inseridos com ajax (head.removeChild(script)) (in callback function, immediately after loading)
Removing the <script> tag does not undefine functions or variables that were defined by it, so you are done with it the moment it is done loading. 

// ***************************

Layout engine:
http://en.wikipedia.org/wiki/List_of_web_browsers
http://en.wikipedia.org/wiki/Comparison_of_web_browsers
http://en.wikipedia.org/wiki/Comparison_of_layout_engines
http://en.wikipedia.org/wiki/Comparison_of_layout_engines_(ECMAScript)
Javascript engine:
http://en.wikipedia.org/wiki/List_of_ECMAScript_engines
http://en.wikipedia.org/wiki/List_of_JavaScript_engines

// ***************************

-8.js 
8a) Implementar namespace amigavel????

ALiBS.t1 = {};
ALiBS.t1.t2 = ALiBS.aa;
// chamar funcao ALiBS.aa.aa() = ALiBS.t1.t2.aa()
//
//
ALiBS.pack = {};
ALiBS.pack.classx = {};
ALiBS.pack.classx.meth = ALiBS.aa.ac;
// chamar funcao ALiBS.aa.ac() = ALiBS.pack.classx.meth()

// ***************************

-9.js
9a) Qual tipo de licença utilizar? ou não usar nenhuma?
9b) Versão liberada para ser compactada e publicada 

// ***************************

-c.js
cs000) Versão compactada e publicada na versao/release 0000
Não é necessário controle de versão nos numeros de 0 a 9 pois não são publicos

// ***************************

classStatus.setProperty("class.bestPractices.level[2].@lastUpdate","2006-11-13"); // Linhas (5-131)
classStatus.setProperty("class.bestPractices.level[3].@lastUpdate","2006-11-13"); // Métodos STATIC (exemplo: ClasseUtil)
classStatus.setProperty("class.bestPractices.level[4].@lastUpdate","2006-11-13"); // Métodos Não STATIC (exemplo: ClasseData)
classStatus.setProperty("class.bestPractices.level[5].@lastUpdate","2006-11-13"); // Construtor para Classes com Métodos Não STATIC


ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("nasp","Implementar: Namespace");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("todo","Implementar: Lista de Pendências");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("7777","Implementar: **** até a posição 77");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("csst","Implementar: Class Status method in ALiBS class");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lela","Implementar: le and la methods");
//05
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("load","Implementar: Mensagem 'loaded module...'");

MONITORACAO
- tamanho de ALiBS.gv.sy.gl
- ALiBS.gv.xx.nn ==1 (nao pode carregar mais de uma vez)

// *************************************************************************

Implementar em todos os módulos que geram HTML

- ID

- imgs file
- imgs path

- css file
- css path
- CLASS

- i18n


- Padrao de IDs (Multi-components on same page)
	- na aplicacao (iALiBS_page0, iALiBS_div0, iALiBS_componente0)
		iALiBS_tk_user0
		iALiBS_tk_pass0
		iALiBS_tk_otp0

	- no modulo (i_hpp0_div0_+n) n = name
		- adicionar "nome" do componente no ID para poder criar varios em uma mesma pagina (exemplo: ht.js)

- Nos modulos que usam ALiBS.to, passar por parâmetro o nome dos campos utilizados f00, f01, f02

- Padrao de teclas de atalho
	- ALiBS.gv.sy.sq[ALiBS.gv.lg.sq+0] = 

- Padrao de arquivos de imagens (Multi-images / Multi-skins)
	ALiBS.gv.hf.imgy0 = ALiBS.gv.sy.sp + "hf.f0y1.png"; // Yes/Valid image 13x13
	//
	ALiBS.gv.hf.imge0 = ALiBS.gv.sy.sp + "hf.f0e1.png"; // Error image 13x13
	//
	ALiBS.gv.hf.imgb0 = ALiBS.gv.sy.sp + "hf.blank13x13.png"; // Blank image 13x13

- Padrao de arquivos de css
	- usar {c0:"cssName"}

- Multi-languages - i18n - internacionalization

	1) no HTML preencher com texto genérico em Inglês

	2) Criar novas strings 
		- it-pt_br, it-es_es, it-en-us
		- ou dinamicamente na pagina HTML

	3) Registrar IDs dos diferentes componentes em cache

	4) Criar funçao de "update" (ver hf,lg)
		- ALiBS.gv.it.fcl["ALiBS.hf.fcl"] = function()

	5) Chamar funcao de "update"
		- if(typeof(ALiBS.it)!="undefined") ALiBS.gv.it.fcl["ALiBS.hf.fcl"](); // update language

- Padrao de nome de função auxiliar
	_gea: function() // AUXILIAR de ge:
	_m0h: function() // AUXILIAR de m0:
	__ab()

- Padrao de nome de função DEPRECATED = fora de uso
	_xxxx_a: function()

- Padrao de retorno de funcao = Objeto OR number ????
	- objeto va (exemplo: cp.js)

- deprecated: xxxx_ab()

// *************************************************************************

Padrao IDs

No HTML:
iALiBS_form0
iALiBS_teclado0
iALiBS_button0
iALiBS_log0


Nos modulos ALiBS
i_htt0_input0_"+n;

i_lglg_div0+<identificador-do-componente>

exemplo:
i_lglg_div_log (log geralmente vai ter somente uma instancia)
i_lglg_img_log

i_htt0_div_teclado0 (instancia 0)
i_htt0_div_teclado1 (instancia 1)

i_htt0_form_teclado0
i_htt0_form_teclado1

// *************************************************************************

TODO(2012-06-15): Este 'o modelo antigo????
Controle de erros: codigo e mensagems

Novo:
	igual ao usado em ALiBS.cp


Não será assim

Antigo: ????
todos os modulos devem usar o controle de Erros

ALiBS.gv.sy.em["ALiBS.cp.bk[-1]"] = "Error Unknown";

return ALiBS.sy.ec(-1,"cp.bk",{{m0:"abcd",l:5,le:true}}) // error code

print ALiBS.sy.em(-1,{}) // error message
	return 
	Erros acima de -101 = iguais aos de ALiBS.er
	se nao encontrado "Error Message Not Implemented"

// *************************************************************************

		if(revision>=1) // Exatidão Global
		{
			bestPractices.setProperty("best.bestPractices.level[0].@required","false");
			bestPractices.setProperty("best.bestPractices.level[0].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[0].description","Exatidão: Todos Métodos: Implementar Stress Test Básico para Processo Global (Testar Entrada e Saída do Sistema)");
		}
		//
		if(revision>=2) // Escrita Mínima
		{
			bestPractices.setProperty("best.bestPractices.level[1].@required","true");
			bestPractices.setProperty("best.bestPractices.level[1].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[1].description","Escrita: Classe: Implementar Best Practices - Método classStatus() e Remover Método version()");
			// 
			bestPractices.setProperty("best.bestPractices.level[2].@required","true");
			bestPractices.setProperty("best.bestPractices.level[2].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[2].description","Escrita: Classe: CTRL+SHIFT+O em todos os imports para acertar as classes/bibliotecas importadas");
			// 
			bestPractices.setProperty("best.bestPractices.level[3].@required","true");
			bestPractices.setProperty("best.bestPractices.level[3].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[3].description","Escrita: Todos Métodos: Separar as variaveis e os métodos com linhas (5-131)");
			//
			bestPractices.setProperty("best.bestPractices.level[4].@required","true");
			bestPractices.setProperty("best.bestPractices.level[4].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[4].description","Escrita: Todos Métodos: Acertar Identação, Inicio e Final de Classes, Métodos, IFs, Loops no Meu Padrão)");
		}
		//
		if(revision>=3) // Exatidão dos Métodos
		{
			bestPractices.setProperty("best.bestPractices.level[5].@required","true");
			bestPractices.setProperty("best.bestPractices.level[5].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[5].description","Exatidão: Todos Métodos: Implementar Stress Test Básico para Todos Parâmetros (String,boolean,int,long,double,float)(Exceto Properties)");
		}
		//
		if(revision>=4)
		{
			bestPractices.setProperty("best.bestPractices.level[6].@required","true");
			bestPractices.setProperty("best.bestPractices.level[6].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[6].description","Memória/Velocidade: Todos Métodos: Implementar Classe com Métodos STATIC (exemplo: ClasseUtil)");
			// 
			bestPractices.setProperty("best.bestPractices.level[7].@required","true");
			bestPractices.setProperty("best.bestPractices.level[7].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[7].description","Escrita: Todos Métodos: Implementar Classe com Métodos Não STATIC (exemplo: ClasseData)");
			// 
			bestPractices.setProperty("best.bestPractices.level[8].@required","true");
			bestPractices.setProperty("best.bestPractices.level[8].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[8].description","Escrita: Classe: Criar Construtor para Classes com Métodos Não STATIC");
		}
		//
		if(revision>=5)
		{
			bestPractices.setProperty("best.bestPractices.level[9].@required","true");
			bestPractices.setProperty("best.bestPractices.level[9].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[9].description","Exatidão: Todos Métodos: Verificar Parâmetros de Entrada e Implementar com.myClasses.exception.*");
			//
			bestPractices.setProperty("best.bestPractices.level[10].@required","true");
			bestPractices.setProperty("best.bestPractices.level[10].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[10].description","Exatidão: Todos Métodos: Nenhum método deve retornar null / Implementar VarNullNotAllowedException");
		}
		//
		if(revision>=6) 
		{
			bestPractices.setProperty("best.bestPractices.level[11].@required","true");
			bestPractices.setProperty("best.bestPractices.level[11].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[11].description","Escrita: Todos Métodos: Remover Variáveis Não Utilizadas na Classe = Project/Properties/Java Compiler/ (Erros/Warnings) /Unnecessary code");
			// 
			bestPractices.setProperty("best.bestPractices.level[12].@required","true");
			bestPractices.setProperty("best.bestPractices.level[12].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[12].description","Escrita: Todos Métodos: Remover Linhas de Código Comentadas que Não Estejam Mais Em Uso");
		}
		//
		if(revision>=7)
		{
			bestPractices.setProperty("best.bestPractices.level[13].@required","true");
			bestPractices.setProperty("best.bestPractices.level[13].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[13].description","Escrita: Todos Métodos: Colocar os métodos em ordem alfabética dentro das classes");
			// 
			bestPractices.setProperty("best.bestPractices.level[14].@required","true");
			bestPractices.setProperty("best.bestPractices.level[14].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[14].description","Escrita: Todos Métodos: Implementar classe LogUtil e Remover Todos System.out.printl()");
		}
		//
		if(revision>=8) 
		{
			// 
			bestPractices.setProperty("best.bestPractices.level[15].@required","true");
			bestPractices.setProperty("best.bestPractices.level[15].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[15].description","Memória/Velocidade: Todos Métodos: Usar a classe StringBuffer ao invés da classe String no Código dos Métodos");
			// 
			bestPractices.setProperty("best.bestPractices.level[16].@required","true");
			bestPractices.setProperty("best.bestPractices.level[16].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[16].description","Memória/Velocidade: Todos Métodos: Usar a classe StringBuffer[] ao invés da classe String[] no Código dos Métodos");
			// 
			bestPractices.setProperty("best.bestPractices.level[17].@required","true");
			bestPractices.setProperty("best.bestPractices.level[17].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[17].description","Memória: Todos Métodos: Usar a classe String nos Parâmetros e no Retorno dos Métodos");
			// 
			bestPractices.setProperty("best.bestPractices.level[18].@required","true");
			bestPractices.setProperty("best.bestPractices.level[18].@lastUpdateMin","2006-11-13");
			bestPractices.setProperty("best.bestPractices.level[18].description","Escrita: Todos Métodos: Em loops for() e while() utilizar contadores com nomes descritivos (nClass, nMethod, nTest, nTable, nRegister, nField)");
			// 
			// Gerar arquivos JAR com código bagunçado
			//
			// 
			//
			// Todos Métodos: SEMPRE QUE FOR POSSÍVEL
			// dados = RPF = Relacional Properties Format
			// return = result = XML format
			// parâmetros = properties
		}
		//
		if(revision>=9) 
		{
			bestPractices.setProperty("best.bestPractices.level[0].@required","true");
			bestPractices.setProperty("best.bestPractices.level[0].@lastUpdateMin","2006-10-21");
			bestPractices.setProperty("best.bestPractices.level[0].description","Exatidão: Todos Métodos: Implementar Stress Test Básico para Parâmetros Prop");
			//
			bestPractices.setProperty("best.bestPractices.level[0].@required","true");
			bestPractices.setProperty("best.bestPractices.level[0].@lastUpdateMin","2006-10-21");
			bestPractices.setProperty("best.bestPractices.level[0].description","Exatidão: Todos Métodos: Implementar Stress Test Avançado para Parâmetros Int,String,Boolean");
			//
			bestPractices.setProperty("best.bestPractices.level[0].@required","true");
			bestPractices.setProperty("best.bestPractices.level[0].@lastUpdateMin","2006-10-21");
			bestPractices.setProperty("best.bestPractices.level[0].description","Exatidão: Todos Métodos: Implementar Stress Test Avançado para Parâmetros Prop");
			//
			// Todos Métodos: os metodos de stress test auxiliares devem ficar no arquivo de stress_test e nao em XptoUtil
			//
			// Nos Métodos com Mais de Uma Assinatura, Colocar Primeiro os Parâmetros Obrigatórios e Depois os Não Obrigatórios
			//
			// Implementar variáveis returnStringValue, returnBooleanValue, returnLongValue, returnFloatValue
		}
		//
		int aInt = 2147483647; // Qual é o limite?  65.000?
		System.out.println(aInt);

	}

[Básico - str vazio]
[Básico - str null]
[Básico - str 1 espaço]

[Básico - boolean true]
[Básico - boolean false]

[Básico - int -300] 
[Básico - int 300]
[Básico - int -1]
[Básico - int 1]
[Básico - int 0]

[Básico - long -70.000L] 
[Básico - long 70.000L]
[Básico - long -1L]
[Básico - long 1L]
[Básico - long 0L]

******************************

[Avançado - str Setar limites (ex: 50/100/200 caracteres)] 
            str > int (256) para ver se dá erro com str.length(), for(int), while(int) Integer.parseInt(str)

[Avançado - int Setar limites (ex: horas = 0 a 24)(ex: minutos = 0 a 60)]

[Avançado - long Setar limites (ex: horas = 0 a 24)(ex: minutos = 0 a 60)]

******************************
Casos especiais

[Caso Especial - separator vazio]
[Caso Especial - separator null]
[Caso Especial - separator inválido] 
[Caso Especial - separator.length() != 1 ] separator.length() = 1

[Caso Especial - occur == 0 ] occur != 0 
[Caso Especial - occur > str.length() ] occur <= str.length()
[Caso Especial - occur = -1 ]
[Caso Especial - occur = 1 ]
[Caso Especial - occur = -2 ]
[Caso Especial - occur = 2 ]
[Caso Especial - occur = -999 ] ocorrência não existe em str
[Caso Especial - occur = 999 ] ocorrência não existe em str

[Caso Especial - str != after && str != before ] str = after || str = before

str pode ser somente espaço, não pode ser null ou vazio
searched pode ser somente espaço, não pode ser null ou vazio
subNew pode ser somente espaço ou vazio, não pode ser null


ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("erro","Implementar: Controle de retorno de erros nos módulos ALiBS.sy.er");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("glog","Implementar: Mensagens de logs Globais");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("cs00","Implementar: Class Status method in Stress Test");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("jpar","Implementar: JSON para parâmetros não obrigatórios");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("dpar","Implementar: Descrição dos parâmetros (required)(optional)(number)(JSON) e do retorno");
//
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("vvar","Implementar: Variaveis locais na função com var");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("json","Implementar: Classe e metodos/functions no formato Object Notation (JSON)");
//
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("v1le","Implementar: Nomes de variáveis locais com 1/2 letras (a,b,c1,c2,etc)");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("tryc","Implementar: Try/catch exception quando necessário");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("sts0","Implementar: Stress Test - Basic");

ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("stsa","Implementar: Stress Test - Advanced = using stress_array");

ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("ccod","Implementar: Comentários do código");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("stbf","Implementar: StringBuffer, caso necessário");
//
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("html","Implementar: Template, caso exista html");
//
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lff2","Implementar: Testado Linux Firefox2 (Stress Test????)");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lop3","Implementar: Testado Linux Opera3");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lgc2","Implementar: Testado Linux Google Chrome2");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lkq2","Implementar: Testado Linux Konqueror2");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lsm4","Implementar: Testado Linux SeaMonkey4 (Netscape)");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("lfl4","Implementar: Testado Linux Flock4");
//
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("wie7","Implementar: Testado Windows IE7");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("wsf3","Implementar: Testado Windows Safari3");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("wgc2","Implementar: Testado Windows Google Chrome2 (V8)");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("wff2","Implementar: Testado Windows Firefox2");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("wop3","Implementar: Testado Windows Opera3");
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("wmt3","Implementar: Testado Windows Maxthon3");
//
ALiBS.gv.cs.df[ALiBS.gv.cs.df.length] = new Array("stsc","Implementar: Stress Test after compressed");

// *************************************************************************
// TODO(2012-06-18):

- "function " (com espaco depois) 
	- function xpto(){}
		- esta fora do padrao
	- tem que ser
		- xx: function() 

- Deixar de usar ALiBS.cs e validar tudo em ALiBS.cp

- Verificar se tem:
- ALiBS = veriricar se tem :
	
	- closure = nested functions (and DOM event listeners)

	- se todos os metodos 
		- validam os seus parametros, exceto p
		- retornam r [BEST-PRACTICE]
		- usam z = {} [BEST-PRACTICE]

	- se esta usando o controle de Erros = retorno e mensagens
		- ERROR messages fora de padrao
	- usando string como array s[i] ao inves de s.charAt(i)
		- aqui em ALiBS.cp existe em bk:

- verificar se tem os [BEST-PRACTICES]
	- var z = {}; 
	- z = null;
	- delete z;

	- if(p)

	- documentacao /**


Vou usar assim?
var P = {};
ALiBSA = {}; // Application Data
ALiBSA.tc = {}; // Timeout control Data

******************************
// TODO(2011-07-04):

- Problemas ao compactar este proprio codigo
	- ALiBS.cp.idt(s);

- criar my_compress-c
	- compactar cp com my_compress
	- compactar cp com my_compress_c	
	- comparar os 2 arquivos = devem ser identicos

- criar stress teste que usa todos ALiBS-c
	- Criar funcao em Array()
	- criar outro array que substitui linhas comentadas, forçando erros

// *************************************************************************

*/
//
// *************************************************************************
//
if(!ALiBS.gv.cp) ALiBS.gv.cp = {}; // global values
//
if(!ALiBS.gv.cp.le) ALiBS.gv.cp.le = 0; // local log status
//
ALiBS.gv.cp.nn = 0; // x times loaded
//
ALiBS.gv.cp.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.cp.rl = "15.0805.1734";
//
// *************************************************************************
//
ALiBS.gv.cp.ff = {}; // custom functions
//
ALiBS.gv.cp.c0 = 0; // function count
ALiBS.gv.cp.c1 = 0; // trace(6+0) count
//
ALiBS.gv.cp.c2 = 0; // ALL functions count
ALiBS.gv.cp.c3 = 0; // return count
//
ALiBS.gv.cp.c4 = 0; // var z count
ALiBS.gv.cp.c5 = 0; // z = null count
//
ALiBS.gv.cp.c6 = 0; // switch(e) count
//
ALiBS.gv.cp.c7 = 0; // ALiBS.gv...rl count
ALiBS.gv.cp.c8 = 0; // ALiBS.sy.la(0+0, + ALiBS.gv.c5.rl= count
//
ALiBS.gv.cp.c9 = 0; // function parameters to validate
ALiBS.gv.cp.c10 = 0; // Invalid Parameter Type
//
ALiBS.gv.cp.er = new Array(); // error Array
//
ALiBS.gv.cp.lfp = ""; // last function parameters (to validate irregular variables)
ALiBS.gv.cp.lfp2 = ""; // last function parameters (to count parameters)
//
// *************************************************************************
//
/** 
	Class ALiBS.cp

		- Generates Compressed js files;
		- Generates ALiBS documentation;
		- Generates ALiBS source
		- Checks Browser Compatibilities

	--------------------------------------------------------------------
*/
ALiBS.cp = // compress text file
{
	cp: function(s,p) // compress text
	// s = (required)(String)text
	// p = (optional)(JSON)parameter {n:"100"}
	//	n = (required)(String)max chars in line
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cp",le:ALiBS.gv.cp.le});
		//
		var va = ALiBS.cp.va(s);
		//
		if(!(!va || !va.ec || !(va.ec<0))) return va.m0+va.m1;
		else
		{
			var sw = s.toLowerCase();
			//
			if(sw.indexOf("<"+"h"+"t"+"m"+"l"+">")>=0 && s.indexOf("<"+"h"+"e"+"a"+"d"+">")>=0 && s.indexOf("<"+"b"+"o"+"d"+"y")>=0)
			{ 
				s = ALiBS.cp.rh(s);
				s = ALiBS.cp.idt(s);
			}
			//
			s = ALiBS.cp.rsc(s);
			//
			s = ALiBS.cp.rmc(s);
			//
			s = ALiBS.cp.rs(s);
			//
			s = ALiBS.cp.bk(s,p);
		}
		//
		return s;
	},
	// *****************************************************************
	dc: function(s,p) // generate documentation
	// s = (required)(String)text
	// p = (optional)(JSON)parameter {}
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.dc",le:ALiBS.gv.cp.le});
		//
		var va = ALiBS.cp.va(s);
		//
		if(!(!va || !va.ec || !(va.ec<0))) return va.m0+va.m1;
		else
		{
			s = ALiBS.cp.gdc(s);
			//
			s = ALiBS.cp.rsc(s); 
		}
		//
		return s;
	},
	// *****************************************************************
	sr: function(s,p) // generate source with few comments
	// s = (required)(String)text
	// p = (optional)(JSON)parameter {}
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.sr",le:ALiBS.gv.cp.le});
		//
		var va = ALiBS.cp.va(s);
		//
		if(!(!va || !va.ec || !(va.ec<0))) return va.m0+va.m1;
		else
		{
			var sw = s.toLowerCase();
			//
			if(sw.indexOf("<"+"h"+"t"+"m"+"l"+">")>=0 && s.indexOf("<"+"h"+"e"+"a"+"d"+">")>=0 && s.indexOf("<"+"b"+"o"+"d"+"y")>=0)
			{ 
				s = ALiBS.cp.rh(s);
				s = ALiBS.cp.idt(s);
			}
			//
			s = ALiBS.cp.rsc(s,{r:true});
			//
			s = ALiBS.cp.rmc(s); 
			//
			s = ALiBS.cp.rnt(s); 
		}
		//
		return s;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	va: function(s) // validations
	// s = (required)(String)text
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.va",le:ALiBS.gv.cp.le});
		//
		var va;
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.a = new Array();
		// 
		z.a[z.a.length] = ALiBS.cp.rsc(s); // single comment
		//
		z.a[z.a.length] = ALiBS.cp.rmc(z.a[z.a.length-1]); // multiple comment
		//
		z.s = z.a[z.a.length-1];
		//
		for(var i=0;i<100;i++)
		{
			switch(i)
			{
				case 0: 
					va = ALiBS.cp.pr0(z.s); // many ; or , or print or console.log
					break;

				case 10: 
					va = ALiBS.cp.bc09(z.s); // }; Closing
					break;
				case 11:
					va = ALiBS.cp.bc10(z.s); // }; Closing
					break;
				case 12:
					va = ALiBS.cp.bc12(z.s); // p a r s e I n t
					break;
				case 13:
					va = ALiBS.cp.bc13(z.s); // s u b s t r
					break;
				case 14:
					va = ALiBS.cp.bc14(z.s); // ,}
					break;





				case 24:
					va = ALiBS.cp.cf09(z.s); // Code Formating 09 = for (, while (, if (
					break;
				case 25:
					va = ALiBS.cp.cf10(s); // (USE s instead z.s) // Code Formating 10 = wrong comment 
					break;
				case 26:
					va = ALiBS.cp.cf11(z.s); // Code Formating 11 = if{} else{} for(){} 
					break;
				case 27:
					va = ALiBS.cp.cf12(z.s); // Code Formating 12 = "= {" or ": {"
					break;
				case 28:
					va = ALiBS.cp.cf14(z.s); // (USE s instead z.s) // ALiBS.sy.la
					break;
	

				case 30:
					va = ALiBS.cp.al0(z.s); // (USE s instead z.s) 
					break;

				case 31:
					z = null; // [BEST-PRACTICE]
					delete z; // [BEST-PRACTICE]
					//
					return 1;
			}
			//
			//if(i>=30) break;
			//
			if(va)
			{
				if(!(!va || !va.ec || !(va.ec<0))) ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = va.m0+va.m1;
				if(va<0) ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = "va=" + va;
			}
			//
			va = null; // to not enter if(va) to create new records in Array
		}
	},
	// *****************************************************************
	gdc: function(s) // get doc/comments
	// s = (required)(String)text
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.gdc",le:ALiBS.gv.cp.le});
		//
		var a = new Array();
		var i = 0;
		var f;
		//
		// comentários do tipo / * *              * /
		//w hile(true)
		while(i>=0)
		{
			i = s.indexOf("/"+"*"+"*");
			if(i>=0)
			{
				f = s.indexOf("*"+"/",i+3);
				a[a.length] = s.substring(i+3,f);
				s = s.substring(f+2,s.length);
			}
			else break;
		}
		//
		return a.join("");
	},
	// *****************************************************************
	rsc: function(s,p) // remove single line comment 
	// s = (required)(String)text
	// p = (optional)(JSON)parameter {r:true}
	//	r = (boolean)do not remove / / ! = DEFAULT = REMOVE
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.rsc",le:ALiBS.gv.cp.le});
		//
		var r;
		//
		if(  ALiBS.sy.ec(  r=ALiBS.sy.ev(s,"ALiBS.cp.rsc",{ty:"s"})  )  ) return r; // [BEST-PRACTICE] parameter validation
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.la = s.split("\n"); // lines
		z.l = z.la.length; // [BEST-PRACTICE]
		z.s = new Array(); // [BEST-PRACTICE]
		//
		if(p)
		{
			if(typeof(p.r)=="boolean" && p.r==true) z.r = true;
		}
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.r)=="undefined" || !z.r) z.r = false; // DEFAULT = REMOVE / / !
		//
		for(var i=0;i<z.l;i++) // each line
		{
			if((z.ix=z.la[i].indexOf("/"+"/"+"!"))>=0) // comentarios / / !
			{
				if(z.r==false) // false=REMOVE / / !
				{
					z.s[i] = z.la[i].substr(0,z.ix-0); 
				}
				else
				{
					z.s[i] = z.la[i];
				}
			}
			else if((z.ix=z.la[i].indexOf("/"+"/"))>=0) // comentarios / /
			{
				z.s[i] = z.la[i].substr(0,z.ix-0); 
			}
			else if((z.ix=z.la[i].indexOf(String.fromCharCode(35)))>=0) // comentarios #
			{
				z.s[i] = z.la[i].substr(0,z.ix-0); 
			}
			else
			{
				z.s[i] = z.la[i];
			}
		}
		//
		r = z.s.join("\n"); // necessario \n pois é usado tambem por dc 
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	rmc: function(s) // remove multiple lines comment
	// s = (required)(String)text
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.rmc",le:ALiBS.gv.cp.le});
		//
		var r;
		//
		if(  ALiBS.sy.ec(  r=ALiBS.sy.ev(s,"ALiBS.cp.rmc",{ty:"s"})  )  ) return r; // [BEST-PRACTICE] parameter validation
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.s = s;
		//
		// [BEST-PRACTICE]
/*		r = s.replace(/\<\!\-\-[\d\D\s\S\w\W]{0,}\-\-\>/gi,"") // comentários do tipo <!-- -->
			.replace(/&lt;\!\-\-[\d\D\s\S\w\W]{0,}\-\-&gt;/gi,"") // comentários do tipo &gt;!-- --&lt;		
			.replace(/\/\*[\d\D\s\S\w\W]{0,}\*\/[\ ]{0}/gi,""); // comentários do tipo / *   * /
*/		//
		//
		//z.s = z.s.replace(/\/\*(.|[\n\r\t\ ])*?\*\/{1}/gi,""); // comentários do tipo / *   * /
		//z.s = z.s.replace(/\<\!\-\-(.|[\n\r\t\ ])*?\-\-\>/gi,""); // comentários do tipo <!-- -->
		//z.s = z.s.replace(/[&]lt;\!\-\-(.|[\n\r\t\ ])*?\-\-[&]gt;/gi,""); // comentários do tipo <!-- -->
/*		//
		if(z.s.indexOf("/"+"*")>=0 || z.s.indexOf("*"+"/")>=0)
		{
			console.log("ainda tem / *");
		}
		//console.log(z.s);
*/

		//
		// comentários do tipo <!-- -->
		//w hile(true)
		z.i = 0;
		while(z.i>=0)
		{
			z.i = z.s.indexOf("&lt"+";!--");
			if(z.i>=0)
			{
				z.f = z.s.indexOf("--&gt"+";",z.i+7); // 7 = &lt;!--
				z.s = z.s.substring(0,z.i)+z.s.substr(z.f+6); // 6 = --&gt;
			}
			else break;
		}
		//
		// comentários do tipo <!-- -->
		//w hile(true)
		z.i = 0;
		while(z.i>=0)
		{
			z.i = z.s.indexOf("<"+"!--");
			if(z.i>=0)
			{
				z.f = z.s.indexOf("-->",z.i+4); // 4 = <!--
				z.s = z.s.substring(0,z.i)+z.s.substr(z.f+3); // 3 = -->
			}
			else break;
		}
		//
		// comentários do tipo / *   * /
		//w hile(true)
		z.i = 0;
		while(z.i>=0)
		{
			z.i = z.s.indexOf("/"+"*");
			if(z.i>=0)
			{
				z.f = z.s.indexOf("*"+"/",z.i+2); // 2 = / *
				z.s = z.s.substring(0,z.i)+z.s.substr(z.f+2); // 2 = * /
			}
			else break;
		}
		//
		r = "" +z.s; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	rnt: function(s) // remove \n\t\n and \n\n
	// s = (required)(String)text
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.rnt",le:ALiBS.gv.cp.le});
		//
		s = s.replace(/\n\t\n/g,"\n");
		//
		s = s.replace(/\n\n\n/g,"\n");
		//
		s = s.replace(/\n\n/g,"\n");
		//
		s = s.replace(/\n\n/g,"\n");
		// 
		return s;
	},
	// *****************************************************************
	rs: function(s) // remove spaces - many types
	// s = (required)(String)text
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.rs",le:ALiBS.gv.cp.le});
		//
		s = s.replace(/\s{0,}=/g,"=");
		s = s.replace(/=\s{0,}/g,"=");
		//
		s = s.replace(/\s{0,}\./g,".");
		s = s.replace(/\.\s{0,}/g,".");
		//
		s = s.replace(/\s{0,}\,/g,",");
		s = s.replace(/\,\s{0,}/g,",");
		//
		s = s.replace(/\s{0,};/g,";");
		s = s.replace(/;\s{0,}/g,";");
		//
		s = s.replace(/\s{0,}\:/g,":");
		s = s.replace(/\:\s{0,}/g,":");
		//
		s = s.replace(/\s{0,}\+/g,"+");
		s = s.replace(/\+\s{0,}/g,"+");
		//
		s = s.replace(/\s{0,}}/g,"}");
		s = s.replace(/}\s{0,}/g,"}");
		//
		s = s.replace(/\s{0,}{/g,"{");
		s = s.replace(/{\s{0,}/g,"{");
		//
		s = s.replace(/\s{0,}\(/g,"(");
		s = s.replace(/\(\s{0,}/g,"(");
		//
		s = s.replace(/\s{0,}\)/g,")");
		s = s.replace(/\)\s{0,}/g,")");
		//
		s = s.replace(/\s{0,}\|/g,"|");
		s = s.replace(/\|\s{0,}/g,"|");
		//
		s = s.replace(/\s{0,}\?/g,"?");
		s = s.replace(/\?\s{0,}/g,"?");
		//
		s = s.replace(/\s{0,}&/g,"&");
		s = s.replace(/&\s{0,}/g,"&");
		//
		s = s.replace(/\s{0,}\^/g,"^");
		s = s.replace(/\^\s{0,}/g,"^");
		//
		//
		s = s.replace(/^[\s]+/,""); // ltrim
		s = s.replace(/[\s]+$/,""); // rtrim
		// 
		return s;
	},
	// *****************************************************************
	bk: function(s,p) // break lines
	// s = (required)(String)text
	// p = (optional)(JSON)parameter {n:"100"}
	//	n = (required)(String)max chars in line
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		//
		//
		//
		var n = 1000; // default max chars in line
		//
		if(p)
		{
			if(typeof(p.n)== "string")
			{
				n = parseInt(p.n,10);
				//
				if(typeof(n)!="number") n = 1000; // default
			}
		}
		//
		var c = 0; // number of chars in current line
		var a = new Array();
		//
		var l = s.length;
		//
		for(var i=0;i<l;i++)
		{
			if(s.charAt(i)=='"') // start of string
			{
				//ALiBS.sy.la(7+0,"(0A)i="+i+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				ALiBS.sy.la(7+0,"(0A)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				//
				ok = true;
				//
				while(i<l)
				{
					a[a.length] = s.charAt(i);
					c++;
					i++;
					//
					if(s.charAt(i)=='"') // end of string
					{
						//ALiBS.sy.la(7+0,"(0B)i="+i+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
						ALiBS.sy.la(7+0,"(0B)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
						//
						a[a.length] = s.charAt(i);
						c++;
						break;
					}
				}
				//
				continue;
			}
			//
			else if(s.charAt(i)=="'") // start of string
			{
				//ALiBS.sy.la(7+0,"(1A)i="+i+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				ALiBS.sy.la(7+0,"(1A)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				//
				while(i<l)
				{
					a[a.length] = s.charAt(i);
					c++;
					i++;
					//
					if(s.charAt(i)=="'") // end of string
					{
						//ALiBS.sy.la(7+0,"(1B)i="+i+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
						ALiBS.sy.la(7+0,"(1B)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
						//
						a[a.length] = s.charAt(i);
						c++;
						break;
					}
				}
				//
				continue;
			}
			//
			//else if(s.charAt(i)=="/") // start of regex
			else if(s.charAt(i)=="/" && s.charAt(i-1)!=")" && s.charAt(i+1)!="(") // start of regex - NOT division - for division use (a)/(b)
			{
				//ALiBS.sy.la(7+0,"(2A)i="+i+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				ALiBS.sy.la(7+0,"(2A)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				//
				while(i<l)
				{
					a[a.length] = s.charAt(i);
					c++;
					i++;
					//
					if(s.charAt(i)=="/") // end of regex
					{
						//ALiBS.sy.la(7+0,"(2B)i="+i+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
						ALiBS.sy.la(7+0,"(2B)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
						//
						a[a.length] = s.charAt(i);
						c++;
						break;
					}
				}
				//
				continue;
			}
			//
			//
			//
			//
			else if(c<(0.95*n)) // continue
			{
				a[a.length] = s.charAt(i);
				c++;			
			}
			//
			//
			//
			//
			else if(s.charAt(i)==";" && s.charAt(i-3)=="&") // ; = 59
			{
				a[a.length] = s.charAt(i);
				c++;
			}
			else if(s.charAt(i)==",") // , = 44
			{
				a[a.length] = s.charAt(i);
				c++;
				//
				//a[a.length] = s.charAt(i);
				//a[a.length] = "\n";
				//
				//ALiBS.sy.la(7+0,"(;,)(new line)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				//
				//c = 0;
			}
			else if(s.charAt(i)==";") // ; = 59 
			{
				a[a.length] = s.charAt(i);
				a[a.length] = "\n";
				//
				ALiBS.sy.la(7+0,"(;,)(new line)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				//
				c = 0;
			}
			else if(s.charAt(i)=="{") // { = 123
			{
				a[a.length] = "\n";
				//
				ALiBS.sy.la(7+0,"({)(new line)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
				//		
				a[a.length] = s.charAt(i); 
				c = 1;
			}
			else if(s.charAt(i)=="}") // } = 125 
			{
				if(s.charAt(i+1)==";" || s.charAt(i+1)==",") // if( }; OR }, ) continue
				{
					a[a.length] = s.charAt(i);
					c++;			
				}
				else
				{
					a[a.length] = s.charAt(i); // } 
					a[a.length] = "\n";
					//
					ALiBS.sy.la(7+0,"(})(new line)c="+c+"s(i)="+s.charAt(i),{o:"ALiBS.cp.bk",le:ALiBS.gv.cp.le});
					//
					c = 0;
				}
			}
			else if(s.charAt(i)=="(") // continue
			{
				a[a.length] = s.charAt(i);
				c++;			
			}
			else if(s.charAt(i)==")") // continue
			{
				a[a.length] = s.charAt(i);
				c++;			
			}
			else // continue
			{
				a[a.length] = s.charAt(i);
				c++;			
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return a.join("");
	},
	// *****************************************************************
	rh: function(s) // replace html
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.rh",le:ALiBS.gv.cp.le});
		//
		var i = s.indexOf("<!DOC"+"TYPE");
		//
		if(i>=0)
		{
			i = s.indexOf("<ht"+"ml>");
			s = s.substr(i); // remove <!DOCTYPE>
			isHtml = true;
		}
		//
		s = s.replace(/\>/g,"&gt;");
		s = s.replace(/\</g,"&lt;");
		//
		s = s.replace(/[\s]&gt;/g,"&gt;");
		s = s.replace(/&gt;[\s]/g,"&gt;");
		//
		s = s.replace(/[\s]&lt;/g,"&lt;");
		s = s.replace(/&lt;[\s]/g,"&lt;");
		//
		return s;
	},
	// *****************************************************************
	idt: function(s) // include DOCTYPE
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.idt",le:ALiBS.gv.cp.le});
		//
		return "&lt;!DOCTYPE html PUBLIC " + String.fromCharCode(34) +  "-" + String.fromCharCode(47) + String.fromCharCode(47) +
			"W3C" + String.fromCharCode(47) + String.fromCharCode(47) + "DTD html 3.2" + String.fromCharCode(47) + String.fromCharCode(47) + 
			"EN" + String.fromCharCode(34) + "&gt" + String.fromCharCode(59) + "\n" + s;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	xxxx_remover: function(s,a,b)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.remover",le:ALiBS.gv.cp.le});
		//
		var z = {};
		//
		z.j = 0;
		while(z.j>=0)
		{
			z.j = s.indexOf(a); // find a
			if(z.j>=0)
			{
				z.f = s.indexOf(b,z.j+1); // find b
				//
				if(z.f>=0) s = s.substring(0,z.j)+s.substr(z.f+1);
				else break;
			}
			else break;
		}
		//
		z = null;
		delete z;
		//
		return s;
	},
	// *****************************************************************
	pr0: function(s) // many ; or ,
	// s = (required)(String)text without comments
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.pr0",le:ALiBS.gv.cp.le});
		//
		var z = {};
		//
		z.a = new Array();
		// 
		z.a[z.a.length] = s; 
		//
		z.d = z.a[z.a.length-1].split("\n");
		//
		//d = v.split("\n");
		//
		var a,b,c,d; // consegue pegar
		var x;; // consegue pegar
		x = "teste;;"; // nao pega
		x = "teste,,"; // nao pega
		x = "teste", y = "teste2" // v Is Not Empty
		x = "print";
		x = "console.log";
		//
		for(var i=0;i<z.d.length;i++)
		{
			if(ALiBS.sg.nox(z.d[i],";")>1)
			{
				if(z.d[i].indexOf("case")>0 || z.d[i].indexOf("default")>0) // switch
				{
					// nothing
				}
				else
				{
					z.va = ALiBS.sy.em(-1,"ALiBS.cp.pr0",{m0:"Found many ;",m1:ALiBS.sg.tr(z.d[i])}); 
					ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
				}
			}
			//
			if(ALiBS.sg.nox(z.d[i],",")>1) 
			{
				x2 = ALiBS.sg.rpc(z.d[i],
							[	["[\r\n\t\s ]{1,}",""], // space, tab, new line, car return
								["[-]{0,}0x[0-9A-Za-z]{1,}[,]{0,1}",""], // Array of Hexa
								["[0-9]{1,}[,]{0,1}",""] // Array of Numbers
							]);
/*if(z.d[i].indexOf("0x")>=0)
{
console.log("x2="+x2);
console.log("typeof(x2)="+typeof(x2));
}*/
				//
				if(x2=="[]," || x2=="[]" || !x2)
				{
					// OK
				}
				else
				{
					z.va = ALiBS.sy.em(-2,"ALiBS.cp.pr0",{m0:"Found many ,",m1:ALiBS.sg.tr(z.d[i])}); 
					ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
				}
			}
			//
			if(z.d[i].indexOf("consol"+"e.log")>=0)
			{
				z.va = ALiBS.sy.em(-3,"ALiBS.cp.pr0",{m0:"Found consol"+"e.log",m1:ALiBS.sg.tr(z.d[i])}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf('Invalid Parameter Type";z.m1="";break;')>=0) // Should be z.m1=""+e;break;
			{
				z.va = ALiBS.sy.em(-3,"ALiBS.cp.pr0",{m0:'Invalid Parameter Type";z.m1="";break;',m1:ALiBS.sg.tr(z.d[i])}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf('.cs = "nasp,')>=0) // Should be z.m1=""+e;break;
			{
				z.va = ALiBS.sy.em(-3,"ALiBS.cp.pr0",{m0:'.cs = "nasp,',m1:ALiBS.sg.tr(z.d[i])}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf("p"+"rint")>=0)
			{
				if(s.indexOf("ALiBS."+"sy =")>=0) 
				{
					// Allowed only in ALiBS.sy.la
				}
				else
				{
					z.va = ALiBS.sy.em(-4,"ALiBS.cp.pr0",{m0:"Found p"+"rint",m1:ALiBS.sg.tr(z.d[i])}); 
					ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
				}
			}
		}
		//
		if(s.indexOf("ALiBS.w")>0 && s.indexOf("wsc")<0)
		{
			z.va = ALiBS.sy.em(-3,"ALiBS.cp.pr0",{m0:'ALiBS.w WITHOUT wsc',m1:ALiBS.sg.tr(z.d[i])}); 
			ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return 1;
	},
	// *****************************************************************
	bc09: function(s) // Browser Compatibility 09 = typeof object | function
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc09",le:ALiBS.gv.cp.le});
		//
		var v;
		var d;
		var di;
		var e = [/[f]unction/,/[i]f\(/,/[e]lse/,/[f]or\(/,/[w]hile\(/];
		//
		v = s;
		//
		d = v.split("\n");
		//
		for(var i=0;i<d.length;i++)
		{
			di = ALiBS.sg.tr(d[i]);
			//
			if(di=="") continue;
			//
			else if(di.indexOf("type"+"of")>=0 && (di.indexOf("obj"+"ect")>=0 || di.indexOf("func"+"tion")>=0))
			{
				return ALiBS.sy.em(-1,"ALiBS.cp.bc09",{m0:"typ"+"eof"+"("+"func"+"tion || obj"+"ect)",m1:di}); 
			}
		}
		//
		return 1;
	},
	// *****************************************************************
	bc10: function(s) // Browser Compatibility 10 = function closing "};"
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		//
		// Gera problemas com Rhino/js.jar
		//
		//s.replace("a","")	// test = should generate error
		//s.replace(/\s{0,};/g,";")	// test = should generate error
		//a = "abcd" 		// test = should generate error
		//var x = ""		// test = should generate error
		//a: "abc",	// test = should generate error
		//b: "abc"	// test = should generate error
		//var a = /[a_z]{0,}/ 	// test = should generate error
		//

		// TODO(2011-07-23): nao esta funcionando
		/*
		var __ab = function() // test = should generate error
		{}
		//
		var __ab = 		// test = should generate error
		{
			__ef: function()
			{
				__gh: 0
			},
			__cd: "abcd"
		};
		*/
		//
		//
		//return 1; // TODO(2011-07-28): coloquei aqui para nao fazer a validacao

		var i0;
		var f = 0;
		var v; // 
		var d = new Array(); //
		var d0;
		var di; // 
		var di2;
		var d_1; //
		var dch;
		var dcd;
		var di0;
		var diL;
		//
		//var aOK = new Array(); // OK = removed
		//var aSV = new Array(); // Saved
		//
		v = s;
		//
		d0 = v.split("\n");
		//
		// *********************************************************
		// v = LINE BY LINE = NOT TEXT COMPLETE 
		// *********************************************************
		//

		var e9 = new Array();
		//
		var m;
		//
		//e9[e9.length] = /^[_a-zA-Z0-9\"\'][^\;\,\+]{0,}[\;\,\+]$/gi; // 
		//
/*
		e9[e9.length] = /^[_a-zA-Z\"\'][^\;]{0,}[\;]$/gi; // [a0_"']   [exceto ;] [somente ;]
		e9[e9.length] = /^[_a-zA-Z\"\'][^\,]{0,}[\,]$/gi; // [a0_"']   [exceto ,] [somente ,]
		//
		e9[e9.length] = /^([_a-zA-Z\"\'])([\'][\;]{0,}[\'][\;])$/gi; // 
		e9[e9.length] = /^([_a-zA-Z\"\'])([\"][\;]{0,}[\"][\;])$/gi; // 
		e9[e9.length] = /^([_a-zA-Z\"\'])([\/][\;]{0,}[\/][\;])$/gi; // 
		e9[e9.length] = /^([_a-zA-Z\"\'])([\(][\;]{0,}[\)][\;])$/gi; // 
*/
		//
		//e9[e9.length] = /^[_a-zA-Z\"\']*?[\;]$/gi; // [a0_"']   [qualquer coisa] [somente ;]
		//e9[e9.length] = /^[_a-zA-Z\"\']*?[\,]$/gi; // [a0_"']   [qualquer coisa] [somente ,]
		//
		//e9[e9.length] = /^[_a-zA-Z\"\']*?[\/][g][i][\;]$/gi; // 
		//e9[e9.length] = /^[_a-zA-Z\"\']*?[\/][g][i][\,]$/gi; // 



		for(var i=0;i<d0.length;i++)
		{
			di = ALiBS.sg.tr(d0[i]);
			//
			if(di!="")
			{
/*
				// TODO(2012-06-22): fazer replace por "" igual em e0 e e1
				//
				for(var k=0;k<4;k++)
				{
					for(var j=0;j<e9.length;j++)
					{
						m = di.match(e9[j]); 
						//
						if(m)
						{
							//ALiBS.sy.la(7+0,"k["+k+"]-e9["+j+"]-m = OK",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//
							for(var h=0;h<m.length;h++)
							{
								ALiBS.sy.la(7+0,"k["+k+"]-e9["+j+"]-m["+h+"] = "+m[h],{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
								//
								di = di.replace(m[h],"");
							}
							//
							//ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//ALiBS.sy.la(7+0,"SV = "+di+"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
						}
						else
						{
							//ALiBS.sy.la(7+0,"k["+k+"]-e9["+j+"]-m = null",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
						}
					}
				}
				//
				//ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				//ALiBS.sy.la(5+0,"SV = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				//ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				//
				// *************************************************
				//
//				d[d.length] = di; // starts new record if letter "" ''

*/


				di0 = di.charAt(0);
				diL = di.charAt(di.length-1);
				//
				//if(ALiBS.sg.co(diL,[/[\;\,\+]/])>=0 && ALiBS.sg.co(di0,[/[_a-zA-Z0-9\"\']/])>=0) // last char = ; , +  // first char = letters, "" ''
				//
				if(ALiBS.sg.co(di0,[/[_a-zA-Z\"\']/])>=0)
				{
					if(diL==";") 
					{
						ALiBS.sy.la(7+0,"Removed [;] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
					}
					else if(diL==",") 
					{
						ALiBS.sy.la(7+0,"Removed [,] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
					}
					else if(diL=="+") 
					{
						ALiBS.sy.la(7+0,"Removed [+] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
					}
					else if(diL==":")
					{
						if(di.indexOf("case")==0) 
						{
							ALiBS.sy.la(7+0,"Removed [case:] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
						}
						else
						{
							ALiBS.sy.la(7+0,"NOT-REMOVED [case:] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//
							d[d.length] = di; // 
						}
					}
					else if(diL==")")
					{
						if(ALiBS.sg.tr(d0[i+1])=="{") 
						{
							if(di.indexOf("else if(")==0)
							{
								ALiBS.sy.la(7+0,"Replaced [else if()] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); 
								//
								d[d.length] = "if()"; // 
							}
							else if(di.indexOf("if(")==0)
							{
								ALiBS.sy.la(7+0,"Replaced [if()] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); 
								//
								d[d.length] = "if()"; // 
							}
							else if(di.indexOf("for(")==0)
							{
								ALiBS.sy.la(7+0,"Replaced [for()] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); 
								//
								d[d.length] = "for()"; // 
							}
							else if(di.indexOf("while(")==0)
							{
								ALiBS.sy.la(7+0,"Replaced [while()] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); 
								//
								d[d.length] = "while()"; // 
							}
							else if(di.indexOf("return f"+"unction(")==0) // ALiBS.sy.gc
							{
								ALiBS.sy.la(7+0,"Replaced [return f"+"unction()] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); 
								//
								d[d.length] = "func = "; // 
							}
							else
							{
								ALiBS.sy.la(7+0,"NOT-REMOVED( [){] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
								//
								d[d.length] = di; // 
							}
						}
						else if(ALiBS.sg.tr(d0[i+1]).charAt(0)=="}" && di.indexOf("=")<0) 
						{
							ALiBS.sy.la(7+0,"Removed({ [)}] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
						}
						else
						{
							ALiBS.sy.la(7+0,"NOT-REMOVED( [)] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//
							d[d.length] = di; // 
						}
					}
					else if(ALiBS.sg.co(diL,[/[_a-zA-Z0-9\"\']/])==0)
					{
						if(ALiBS.sg.tr(d0[i+1]).charAt(0)=="}" && di.indexOf("=")<0) 
						{
							ALiBS.sy.la(7+0,"Removed [_a-zA-Z] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
						}
						else
						{
							ALiBS.sy.la(7+0,"d0[i+1] = ["+d0[i+1] + "]",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							ALiBS.sy.la(7+0,"NOT-REMOVED [_a-zA-Z] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//
							d[d.length] = di; // 
						}
					}
					else if(diL=="]")
					{
						if(ALiBS.sg.tr(d0[i+1]).charAt(0)=="}" && di.indexOf("=")<0) 
						{
							ALiBS.sy.la(7+0,"Removed[ []] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
						}
						else
						{
							ALiBS.sy.la(7+0,"d0[i+1] = ["+d0[i+1] + "]",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							ALiBS.sy.la(7+0,"NOT-REMOVED[ []] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
							//
							d[d.length] = di; // 
						}
					}
					else
					{
						ALiBS.sy.la(7+0,"NOT-REMOVED [] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
						//
						d[d.length] = di; // 
					}
				}

				/*else if(ALiBS.sg.co(di0,[/[_a-zA-Z\"\']/])>=0 && ALiBS.sg.co(diL,[/[\;\,\+]/])>=0) // first char = letters, "" ''  // last char = ; , +
				{
					ALiBS.sy.la(7+0,"Removed [;] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				}*/
				//			
				//if(di.replace(/[_a-zA-Z0-9\"\'][^\;\,\+]{0,}[\;\,\+]/,"")=="") // TODO(2012-06-22): não pode ter ;,+ no meio da linha
				//{
				//	ALiBS.sy.la(7+0,"Removed OK - di = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				//}
				//
				else
				{
					var x2 = ALiBS.sg.rpc(di,
								[	["[\r\n\t\s ]{1,}",""], // space, tab, new line, car return
									["[-]{0,}0x[0-9A-Za-z]{1,}[,]{0,1}",""], // Array of Hexa
									["[0-9]{1,}[,]{0,1}",""] // Array of Numbers

								]);
					//
/*if(di.indexOf("0x")>=0 && x2)
{
	console.log("x2="+x2);
	console.log("typeof(x2)="+typeof(x2));
}*/
					//
					if(x2=="[]," || x2=="[]" || !x2)
					{
						ALiBS.sy.la(7+0,"Removed[ [0x]] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le}); // not added to d[d.length]
					}
					else
					{
/*
						di = di.replace(/[a-zA-Z_]{0,}\.[s]ubstring\([^{}()]{0,}\)/gi,"V_A_R"); // substring() // TODO(2012-06-22): pegar antes xpto.substring()
						//
						//di = di.replace(/[S]tring\.[f]romCharCode\([^{}()]{0,}\)/gi,"V_A_R"); // String.fromCharCode()
						//
						di = di.replace(/[S]tring\.[f]romCharCode\([0-9]{0,}\)/gi,"V_A_R"); // String.fromCharCode()
						//
						di = di.replace(/[a-zA-Z_]{0,}\.[i]ndexOf\([^{}()]{0,}\)[\ ]{0,}[\>\<\=]{1,2}[0-9]{1,}/gi,"V_A_R"); // xpto.indexOf()
						//
						//di = di.replace(/[a-zA-Z_]{0,}\.[i]ndexOf\([^{}()]{0,}\)[\ ]{0,}[\>\<\=]{0,2}[^{}()]{0,}/gi,"V_A_R"); // xpto.indexOf()
						//
						//di = di.replace(/[a-zA-Z_]{0,}\.[i]ndexOf\([^{})]{0,}\)[\ ]{0,}[\>\<\=]{0,2}[^{}()]{0,}/gi,"V_A_R"); // xpto.indexOf()
						//
						di = di.replace(/[t]ypeof\([^{}()]{0,}\)/gi,"V_A_R"); // typeof()
						//
	*/
	/*
						di = di.replace(/['][^']{0,}[']/gi,"V_A_R"); // ' '
						//
						di = di.replace(/["][^"]{0,}["]/gi,"V_A_R"); // " "
						//
						di = di.replace(/[/][^/]{0,}[/]\/gi/gi,"V_A_R"); // / / 
						//
						di = di.replace(/\([^()]{0,}\)/gi,"()"); // ( )
						//
						//
						//
						//
						di = di.replace(/[s]ubstring\([^{}()]{0,}\)/gi,"V_A_R"); // substring() // TODO(2012-06-22): pegar antes xpto.substring()
						//
						di = ALiBS.sg.tr(di);
	*/					//
						ALiBS.sy.la(7+0,"NOT-REMOVED [none] = "+di,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
						//
						d[d.length] = di; 
					}
				}


			}
		}
		//
		for(var i=0;i<d.length;i++)
		{
			ALiBS.sy.la(7+0,"(NOT-REMOVED)d[i] = "+d[i],{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		}
		//
		v = d.join("");
		ALiBS.sy.la(7+0,"v = "+v,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		//
		//
		//
		//
		var e0 = new Array();
		var e1 = new Array();
		var e2 = new Array();
		//
/*		//v = aSV.join("");
		v = d.join("");
		//
		// *********************************************************
		// v = TEXT COMPLETE = NOT LINE BY LINE
		// *********************************************************
		//
		e0[e0.length] = /[v]ar{0,}[\ ]{0,}[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\=][\ ]{0,}[\{][^{}(),;]{0,}[\}][\,\;]/gi; // var ALiBS.cp ={...};
		//
		e0[e0.length] = /[v]ar{0,}[\ ]{0,}[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\=][\ ]{0,}[^{}(),;]{0,}[\,\;]/gi; // var ALiBS.cp = ... ;
		//
		e0[e0.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\=\:][\ ]{0,}\{[^{}()]{0,}\}[\,\;]/gi; // JSON ALiBS.cp ={abcd:{},};
		//
		e0[e0.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[^{}()\[\]]{0,}\][\ ]{0,}[\=\:]\{[^{}()]{0,}\}[\,\;]/gi; // JSON ALiBS.gv.sy.mc["ALiBS.aP"] ={};
		//
		e0[e0.length] = /\!\([^{};,)]{0,}\)\)/gi; // !(!va || !va.ec || !(va.ec<0))
*/		//
/*		e0[e0.length] = /V_A_R\{\}/gi; // V_A_R{}
		//
		e0[e0.length] = /V_A_R\(\)\<\=\0/gi; // V_A_R()<=0
		//
		e0[e0.length] = /V_A_R\(\)\>\=\0/gi; // V_A_R()>=0
		//
		e0[e0.length] = /V_A_R\(\)\>\0/gi; // V_A_R()>0
		//
		e0[e0.length] = /V_A_R\(\)\<\0/gi; // V_A_R()<0
		//
		e0[e0.length] = /V_A_R\(\)\=\0/gi; // V_A_R()>=0
*/		//
/*		for(var k=0;k<2;k++)
		{
			for(var j=0;j<e0.length;j++)
			{
				m = v.match(e0[j]); 
				//
				if(m)
				{
					ALiBS.sy.la(7+0,"k["+k+"]-e0["+j+"]-m = OK",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
					//
					for(var i=0;i<m.length;i++)
					{
						//aOK[aOK.length] =  m[i];
						ALiBS.sy.la(7+0,"k["+k+"]-e0["+j+"]-m["+i+"] = "+m[i],{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
						//
						v = v.replace(m[i],"");
					}
					//
					ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
					ALiBS.sy.la(7+0,"SV = "+v+"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
					ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				}
				else
				{
					ALiBS.sy.la(7+0,"k["+k+"]-e0["+j+"]-m = null",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				}
			}
		}
*/		//
		ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		ALiBS.sy.la(5+0,"SV = "+v,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		//
		// *********************************************************
		//
		/*
			USAR dentro [] (dentro nunca precisa escape \ ) ????
			{}()

			USAR fora
			{}:
			\( \) \.
		*/ 
		//
		// Secure
		//
		e1[e1.length] = /[t]ry{}[c]atch\([e]\){}/gi; // try{}catch(){}
		//
		e1[e1.length] = /[f]or\([^{}]{0,}\){}/gi; // for(){}
		//
		e1[e1.length] = /[w]hile\([^{}]{0,}\){}/gi; // while(){}
		//
		e1[e1.length] = /[d]o{}/gi; // do{}
		//
		// ********************************
		//
		//e1[e1.length] = /[c]ase\ [0-9]{0,3}:/gi; // case 0: (from switch)
		//
		e1[e1.length] = /[s]witch\([^{}()]{0,}\){}/gi; // switch(){}
		//
		e1[e1.length] = /[i]f\([^{}()]{0,}\){}[e]lse{}/gi; // if(){}else{}
		//
		e1[e1.length] = /[i]f\([^{}()]{0,}\){}/gi; // if(){}
		//
		e1[e1.length] = /[e]lse{}/gi; // else{}
		//

		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\ [a-zA-Z0-9_]{0,}\([^{}()]{0,}\){}\;/gi; // var xx.xx.xx.xx.xx.xx.xx[] = function(){};
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\ [a-zA-Z0-9_]{0,}\([^{}()]{0,}\){}\,/gi; // var xx.xx.xx.xx.xx.xx.xx[] = function(){},
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\ [a-zA-Z0-9_]{0,}\([^{}()]{0,}\){}/gi; // var xx.xx.xx.xx.xx.xx.xx[] = function(){}

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\ [a-zA-Z0-9_]{0,}\([^{}()]{0,}\){}\;/gi; // xx.xx.xx.xx.xx.xx.xx[] = function(){};
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\ [a-zA-Z0-9_]{0,}\([^{}()]{0,}\){}\,/gi; // xx.xx.xx.xx.xx.xx.xx[] = function(){},
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\ [a-zA-Z0-9_]{0,}\([^{}()]{0,}\){}/gi; // xx.xx.xx.xx.xx.xx.xx[] = function(){}


		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\([^{}()]{0,}\){}\;/gi; // var xx.xx.xx.xx.xx.xx.xx[] = function(){};
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\([^{}()]{0,}\){}\,/gi; // var xx.xx.xx.xx.xx.xx.xx[] = function(){},
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\([^{}()]{0,}\){}/gi; // var xx.xx.xx.xx.xx.xx.xx[] = function(){}

		e1[e1.length] = /[_a-zA-Z]{1,}[\._a-zA-Z0-9]{0,}[\ ]{0,}[\=][\ ]{0,}[f]unction\([^{}()]{0,}\){}\;/gi; // xx.xx.xx.xx.xx.xx.xx = function(){};
		//		
		e1[e1.length] = /[_a-zA-Z]{1,}[\._a-zA-Z0-9]{0,}[\ ]{0,}[\:][\ ]{0,}[f]unction\([^{}()]{0,}\){}\,/gi; // xx.xx.xx.xx.xx.xx.xx : function(){},
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[\._a-zA-Z0-9]{0,}[\ ]{0,}[\:][\ ]{0,}[f]unction\([^{}()]{0,}\){}/gi; // xx.xx.xx.xx.xx.xx.xx : function(){}
		//

		e1[e1.length] = /[f]unction\ [a-zA-Z0-9_]{1,}\([^{}()]{0,}\){}\;/gi; // function abcd(){};
		//
		e1[e1.length] = /[f]unction\ [a-zA-Z0-9_]{1,}\([^{}()]{0,}\){}\,/gi; // function abcd(){},
		//
		e1[e1.length] = /[f]unction\ [a-zA-Z0-9_]{1,}\([^{}()]{0,}\){}/gi; // function abcd(){}

		//

		e1[e1.length] = /[r]eturn\ t\.rs\.[s]ort\([f]unction\(a\,b\){}\)\;/gi; // return t.rs.sort(function(a,b){}); used in ALiBS.to
		//
		e1[e1.length] = /t\.rs\.[s]ort\([f]unction\(a\,b\){}\)\;/gi; // t.rs.sort(function(a,b){}); used in ALiBS.to

		e1[e1.length] = /[r]eturn\ {}\;/gi; // return {}; used in ALiBS.bn


		//e1[e1.length] = /[i]f\([^{}]{0,}\){}[f]or\([^{}]{0,}\){}/gi; // if(){}for(){}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\){}[w]hile\([^{}]{0,}\){}/gi; // if(){}while(){}
		//

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[_a-zA-Z0-9\'\"\.]{0,}\]\.[a-zA-Z]{0,}\ \=[\ ]{0,}{}\;/gi; // ALiBS.gv.sy.sq[i].p = {};


		//
		// Rever ALiBS
		//
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}{}\;/gi; // var xx.xx.xx.xx.xx.xx.xx = {};
		//
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}{}\,/gi; // var xx.xx.xx.xx.xx.xx.xx = {},
		//
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}{}/gi; // var xx.xx.xx.xx.xx.xx.xx = {}
		//

		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}{}\;/gi; // var xx.xx.xx.xx.xx.xx.xx[] = {};
		//
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}{}\,/gi; // var xx.xx.xx.xx.xx.xx.xx[] = {},
		//
		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}{}/gi; // var xx.xx.xx.xx.xx.xx.xx[] = {};
		//


		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}{}\;/gi; // xx.xx.xx.xx.xx.xx.xx = {};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}{}\,/gi; // xx.xx.xx.xx.xx.xx.xx = {},
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}{}/gi; // xx.xx.xx.xx.xx.xx.xx = {}
		//

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9_\.\+\-\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}{}\;/gi; // xx.xx.xx.xx.xx.xx.xx[] = {};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9_\.\+\-\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}{}\,/gi; // xx.xx.xx.xx.xx.xx.xx[] = {},
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9_\.\+\-\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}{}/gi; // xx.xx.xx.xx.xx.xx.xx[] = {}
		//

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\:[\ ]{0,}{}\;/gi; // xx.xx.xx.xx.xx.xx.xx : {};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\:[\ ]{0,}{}\,/gi; // xx.xx.xx.xx.xx.xx.xx : {},
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\:[\ ]{0,}{}/gi; // xx.xx.xx.xx.xx.xx.xx : {}
		//

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\:[\ ]{0,}\[\]\,/gi; // xx.xx.xx.xx.xx.xx.xx : [],
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\:[\ ]{0,}\[\]/gi; // xx.xx.xx.xx.xx.xx.xx : []
		//

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\([^{}()]{0,}\){}\;/gi; // xx.xx.xx.xx.xx.xx.xx[] = function(){};
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\([^{}()]{0,}\){}\,/gi; // xx.xx.xx.xx.xx.xx.xx[] = function(){},
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[a-zA-Z0-9\.\+\-_\'\"]{0,}\][\ ]{0,}\=[\ ]{0,}[f]unction\([^{}()]{0,}\){}/gi; // xx.xx.xx.xx.xx.xx.xx[] = function(){}

		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[_a-zA-Z0-9\'\"\.]{0,}\][\ ]{0,}\=[\ ]{0,}{}\;/gi; // ALiBS.gv.sy.mc["ALiBS.sy"] = {};

		e1[e1.length] = /[v]ar\ [_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}\=[\ ]{0,}[n]ew\ [A]rray\([^{}()]{0,}\)\;/gi; // var a = new Array();

/*
		e1[e1.length] = /[v]ar{0,}[\ ]{0,}[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\=][\ ]{0,}[^{}(),;]{0,}[\,\;]/gi; // var ALiBS.cp = ... ;
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\=\:][\ ]{0,}\{[^{}()]{0,}\}[\,\;]/gi; // JSON ALiBS.cp ={abcd:{},};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[^{}()\[\]]{0,}\][\ ]{0,}[\=\:]\{[^{}()]{0,}\}[\,\;]/gi; // JSON ALiBS.gv.sy.mc["ALiBS.aP"] ={};
		//
		//e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\:\=][\ ]{0,}\{[^{}(),;]{0,}\}[\,\;]/gi; // ALiBS.cp ={abcd:{},};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\:\=][\ ]{0,}\{\}[\,\;]/gi; // ALiBS.cp ={abcd:{},};
		//
		//e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[^{}()\[\]]{0,}\][\ ]{0,}[\=\:][\ ]{0,1}[\{][\}][\;]{0,1}/gi; // JSON ALiBS.gv.sy.mc["ALiBS.aP"] = {};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[^{}()\[\]]{0,}\][\ ]{0,}[\=\:][\ ]{0,1}\{\}[\;]{0,1}  /gi; // JSON ALiBS.gv.sy.mc["ALiBS.aP"] = {};
		//
		//e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[^{}()\[\]]{0,}\][\ ]{0,}[\=\:][\ ]{0,1}[\{]{0,1}[\}]{0,1}[\;]{0,1}/gi; // JSON ALiBS.gv.sy.mc["ALiBS.aP"] = {};
		//
		e1[e1.length] = /{([e]lse\ ){1,}([e]lse){0,1}}/gi; // {else }
		//



		//
		e1[e1.length] = /\!\([^{};,)]{0,}\)\)/gi; // !(!va || !va.ec || !(va.ec<0))


		//
		// Rever
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}[\ ]{0,}[\:\=][\ ]{0,}\{\}[\,\;]/gi; // ALiBS.cp ={abcd:{},};
		//
		e1[e1.length] = /[_a-zA-Z]{1,}[_a-zA-Z0-9\.]{0,}\[[^{}()\[\]]{0,}\][\ ]{0,}[\=\:][\ ]{0,1}\{\}[\;]{0,1}  /gi; // ALiBS.gv.sy.mc["ALiBS.aP"] = {};
		//


		
		//e1[e1.length] = /[_a-zA-Z][\._a-zA-Z0-9]{0,}[\ ]{0,2}[\:\=][\ ]{0,2}[f]unction\([^{}()]{0,}\)\{[^(){}]{0,}\}[\,\;]{0,1}/gi; // REPETIDO = para ALiBS.lg
		//		
		e1[e1.length] = /[_a-zA-Z][\._a-zA-Z0-9]{0,}[\ ]{0,2}[\:\=][\ ]{0,2}[f]unction\([^{}()]{0,}\)\{\}[\,\;]{0,1}/gi; // REPETIDO = para ALiBS.lg
		//		


		e1[e1.length] = /[_a-zA-Z][\._a-zA-Z0-9]{0,}[\ ]{0,2}[\:\=][\ ]{0,2}[f]unction\([^{}()]{0,}\)\{\}[\,\;]{0,1}/gi; // abcd :/= function(){},/;
		//		
		e1[e1.length] = /[_a-zA-Z][\._a-zA-Z0-9]{0,}[\ ]{0,2}[\:\=][\ ]{0,2}[f]unction\([^{}()]{0,}\)\{\}[\,\;]{0,1}/gi; // REPETIDO = para ALiBS.lg
		//		
		e1[e1.length] = /\([f]unction\([^{}()]{0,}\){}\)/gi; // (function(){})

*/
		//
/*

		e1[e1.length] = /[i]f\([^{}]{0,}\)\{\}/gi; // if(){}
		//
		e1[e1.length] = /[e]lse\{\}[\,\;]{0,}/gi; // else{}
		//

		e1[e1.length] = /{([e]lse\ ){1,}([e]lse){0,1}}/gi; // {else }
		//
*/		//
		// Unsecure
		//
		//e1[e1.length] = /[t]ry\{[^(){}]{0,}\}[c]atch\([e]\)\{[^(){}]{0,}\}/gi; // try{}catch(){}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)\{[^(){}]{0,}\}[\,\;]{0,}[e]lse\{[^(){}]{0,}\}[\,\;]{0,}/gi; // if(){}else{}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)\{\}[\,\;]{0,}[e]lse\{\}[\,\;]{0,}/gi; // if(){}else{}
		//
		//e1[e1.length] = /[e]lse[\ ]{0,}[i]f\([^{}]{0,}\)\{[^(){}]{0,}\}[\,\;]{0,}/gi; // else if(){}
		//
		//e1[e1.length] = /[e]lse[\ ]{0,}[i]f\([^{}]{0,}\)\{\}[\,\;]{0,}/gi; // else if(){}
		//
/*		e1[e1.length] = /[e]lse[\ ]{0,}[i]f\([^{}]{0,}\){}[\,\;]{0,}/gi; // else if(){}*/
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)\{[^(){}]{0,}\}[\,\;]{0,}[e]lse[\ ]/gi; // if(){}else[\ ]
		//
/*		e1[e1.length] = /[i]f\([^{}]{0,}\)\{\}[\,\;]{0,}[e]lse[\ ]/gi; // if(){}else[\ ] */
		//
		//e1[e1.length] = /[e]lse\{[^(){}]{0,}\}[\,\;]{0,}/gi; // else{}
/*		//
		e1[e1.length] = /[e]lse\{\}[\,\;]{0,}/gi; // else{}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)[\ ]{0,}[f]or\([^{}]{0,}\)\{[^(){}]{0,}\}[\,\;]{0,}/gi; // if(){} for(){}
		//
		e1[e1.length] = /[i]f\([^{}]{0,}\)[\ ]{0,}[f]or\([^{}]{0,}\)\{\}[\,\;]{0,}/gi; // if(){} for(){}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)[\ ]{0,}[w]hile\([^{}]{0,}\)\{[^(){}]{0,}\}[\,\;]{0,}/gi; // if(){} while(){}
		//
		e1[e1.length] = /[i]f\([^{}]{0,}\)[\ ]{0,}[w]hile\([^{}]{0,}\)\{\}[\,\;]{0,}/gi; // if(){} while(){}
		//
		e1[e1.length] = /\([f]unction\([^{}()]{0,}\){}\)/gi; // (function(){})
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)\{V_A_R\}/gi; // if(V_A_R){}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)\{\}/gi; // if(V_A_R){}
		//
		//e1[e1.length] = /[i]f\([^{}]{0,}\)\{[^(){}]{0,}\}/gi; // if(){}
		//
		e1[e1.length] = /[i]f\([^{}]{0,}\)\{\}/gi; // if(){}
		//
*/





		for(var k=0;k<10;k++) // 7 cycles
		{
			for(var j=0;j<e1.length;j++) // loop through e1
			{
				m = v.match(e1[j]); 
				//
				if(m)
				{
					ALiBS.sy.la(7+0,"k["+k+"]-e1["+j+"]-m = OK",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
					//
					for(var i=0;i<m.length;i++)
					{
						//aOK[aOK.length] =  m[i];
						ALiBS.sy.la(7+0,"k["+k+"]-e1["+j+"]-m["+i+"] = "+m[i],{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
						//
						v = v.replace(m[i],"");
					}
					//
					ALiBS.sy.la(7+0,"\nSV = "+v+"\n\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				}
				else
				{
					ALiBS.sy.la(7+0,"k["+k+"]-e1["+j+"]-m = null",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
				}
			}
		}
		//
		ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		ALiBS.sy.la(5+0,"SV = "+v,{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		ALiBS.sy.la(7+0,"\n",{o:"ALiBS.cp.bc10",le:ALiBS.gv.cp.le});
		//
		// *********************************************************
		//
		if(v!="")
		{
			return ALiBS.sy.em(-11,"ALiBS.cp.bc10",{m0:"Unknown][v Is Not Empty",m1:v}); 
		}
		//
		return 1;
	},
	// *****************************************************************
	bc11: function(s) // Browser Compatibility 11 = \n\r
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc11",le:ALiBS.gv.cp.le});
		//
		// TODO(2011-07-20):
		// \n OR \r // OUTROS
		// \r // MSIE6
	},
	// *****************************************************************
	bc12: function(s) // Browser Compatibility 12 = p a r s e I n t (numero,10)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc12",le:ALiBS.gv.cp.le});
		//
		//p a r s e I n t (numero,10)
		//If the radix parameter is omitted:
		//	16 if string begins with "0x"
		//	8 if string begins with "0"
		//	10 if string begins with any other value
		//
		var i = 0;
		var f = 0;
		var v; // virgula
		var d; // Dez  = 10
/*		var n = 0;
		//
		w hile(true && n<=10)
		{
			n++;
			//
			if(n>=10)
			{
				return ALiBS.sy.em(-99,"ALiBS.cp.bc12",{m0:"Infinite Looping",m1:n}); 
			}
*/			//


		while(i>=0)
		{
			i = s.indexOf("p"+"a"+"r"+"s"+"e"+"I"+"n"+"t",f);
			if(i>=0)
			{
				v = s.indexOf(",",i+8+1); // p a r s e I n t (  ,
				f = s.indexOf(String.fromCharCode(41)+String.fromCharCode(59),v); // ");" p a r s e I n t (  );
				//
				//f = s.indexOf(");",v); // p a r s e I n t (  );
				//
				if(v<0 || v>f)
				{
					return ALiBS.sy.em(-1,"ALiBS.cp.bc12",{m0:"p"+"arseInt Without Second Argument",m1:s.substring(i,f+1)}); 
				}
				else
				{
					d = s.substring(v,f+1);
					if(d.indexOf(",10)")<0 && d.indexOf(",10-")<0 && d.indexOf(",10+")<0)
					{
						return ALiBS.sy.em(-2,"ALiBS.cp.bc12",{m0:"p"+"arseInt Without '10' as Second Argument",m1:s.substring(i,f+1)+"]["+d+"]"}); 
					}
					else
					{
						// break; // ????
						// continue; // ????
					}
				}
			}
			else break;
		}
		//
		return 1;
	},
	// *****************************************************************
	bc13: function(s) // Browser Compatibility 13 = s u b s t r AND s u b s t r i n g
	/** 
	Method <static> ALiBS.cp.bc3(s)

		Description: Check Compatibility for s u b s t r
		// s.s u b s t r (-2) // OUTROS
		// s.s u b s t r (s.length-2) // MSIE6
		//
		// Shoud not exist
		//s.s u b s t r ( -1 ); // test
		//s.s u b s t r ( 1 , -1 ); // test
		//s.s u b s t r ( -1 , 1 ); // test
		//s.s u b s t r ( -1 , -1 ); // test

		Parameters:
			s = (required)(String)code
	
		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/	
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc13",le:ALiBS.gv.cp.le});
		//
		//s.s ubstr( -1 ); // test = should generate error 
		//s.s ubstr( 1 , -1 ); // test = should generate error 
		//s.s ubstr( -1 , 1 ); // test = should generate error 
		//s.s ubstr( -1 , -1 ); // test = should generate error 
		//
		var i = 0;
		var f = 0;
		var v; // sinal "-" ou virgula
		var d0; // 
		var d1; // first argument
		var d2; // second argument
		//
		var s0;
		var s1;
		//
/*		var n = 0;
		//
		w hile(true && n<=10)
		{
			n++;
			//
			if(n>=10)
			{
				return ALiBS.sy.em(-99,"ALiBS.cp.bc13",{m0:"Infinite Looping",m1:n}); 
			}
*/			//
//console.log(s)
		while(i>=0)
		{
			i = s.indexOf("s"+"u"+"b"+"s"+"t"+"r",f);
			//
//console.log("i="+i)
//console.log("s0="+s0)
			if(i>=0)
			{
				s1 = s.substr(i,9);
console.log(s1);
				//
				if(s1=="s"+"u"+"b"+"s"+"t"+"r"+"i"+"n"+"g")
				{
					s0 = 9; // substring
				}
				else
				{
					s0 = 6; // substr
				}
				//
				f = s.indexOf(")",i+s0+1); // s u b s t r (  )
				v = s.indexOf("-",i+s0+1); // s u b s t r (  -
				v1 = s.indexOf(",",i+s0+1); // s u b s t r (  ,
				//
				//if(v>0 && v<f) // there is signal "-"
				if(v1>f) // , outside ()
				{
					d0 = s.substring(i+s0+1,f); // only arguments
					v = d0.indexOf(","); 
					if(v<0) // no ","
					{
						var r = 
							{	
								ec:-1,
								m0:"[ERROR][-1][ALiBS.cp.bc13][s"+"ubstr Should Have 2 Arguments]",
								m1:"["+s.substring(i,f+1)+"]["+d0+"]"
							};
						//
						ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.bc13"});
						//
						return r;
					}
					else
					{
						v = d0.split(",");
						//
						d1 = v[0]; // first arg
						d2 = v[1]; // second arg
						//
						//
						d1 = ALiBS.sg.rs(d1,String.fromCharCode(35)+" ,0123456789",""); // #
						//
						v = d1.indexOf("-"); 
						if(v==0)
						{
							var r = 
								{	
									ec:-2,
									m0:"[ERROR][-2][ALiBS.cp.bc13][s"+"ubstr With '-']",
									m1:"["+s.substring(i,f+1)+"]["+d0+"]"
								};
							//
							ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.bc13"});
							//
							return r;
						}
						//
						//
						d2 = ALiBS.sg.rs(d2,String.fromCharCode(35)+" ,0123456789",""); // #
						//
						v = d2.indexOf("-"); 
						if(v==0)
						{
							var r = 
								{	
									ec:-3,
									m0:"[ERROR][-3][ALiBS.cp.bc13][s"+"ubstr With '-']",
									m1:"["+s.substring(i,f+1)+"]["+d0+"]"
								};
							//
							ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.bc13"});
							//
							return r;
						}
					}
				}
			}
			else break;
		}
		//
		return 1;
	},
	// *****************************************************************
	bc14: function(s) // Browser Compatibility 14 = ",}"
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc14",le:ALiBS.gv.cp.le});
		//
		var v;
		var d;
		var di;
		//
		//
		v = ALiBS.cp.rsc(s); // single comment
		//
		v = ALiBS.cp.rmc(v); // multiple comment
		//
		v = ALiBS.cp.rs(v); // spaces
		//
		d = v.split("\n");
		//
		for(var i=0;i<d.length;i++)
		{
			di = ALiBS.sg.tr(d[i]);
			//
			if(di=="") continue;
			//
			else
			{
				var ix = di.indexOf(","+"}");
				//
				if(ix>=0)
				{
					return ALiBS.sy.em(-1,"ALiBS.cp.bc14",{m0:'Found ",'+'}"',m1:di.substring(ix-10,ix+10)}); // ",}"
				}
				else
				{
					//
				}
			}
		}
		//
		return 1;
	},
	// *****************************************************************
	bc15: function(s) // Browser Compatibility 15 = Array / [] / charAt()
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.bc15",le:ALiBS.gv.cp.le});
		//
		// TODO(2011-07-20):
		//	- var a = new array  a[i]
		//	- var a = "abcd";    a[i]
		//
		// string[i] // OUTROS
		// string.charAt(i) // MSIE6
		//
		// [] somente funciona no MSIE se for Array, não funciona para indexar strings
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	cf09: function(s) // Code Formating 09 = "for (" , "while (" , "if (" , "for(i" , "function("
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf09",le:ALiBS.gv.cp.le});
		//
		//if (s){} // test = should generate error 
		//for (var i=0;i<1;i++){}  // test = should generate error 
		//while (i<0){i++}; // test = should generate error 
		/*var a = function ()
		{
			//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf09",le:ALiBS.gv.cp.le});
		};*/
		//
		var v;
		var d;
		var di;
		var e = [ /[i]f\ \(/ , /[f]or\ \(/ , /[w]hile\ \(/ , /[f]or\([^v] / , /[f]unction\ \(/ ];
		//
		v = s;
		//
		d = v.split("\n");
		//
		for(var i=0;i<d.length;i++)
		{
			di = ALiBS.sg.tr(d[i]);
			//
			if(di=="") continue;
			//
			else if(ALiBS.sg.co(di,e)>=0)
			{
				var r = 
					{	
						ec:-1,
						m0:"[ERROR][-1][ALiBS.cp.cf09][f"+"or "+"(,w"+"hile "+"(,i"+"f "+"(]", // [for (,while (,if (]
						m1:"["+di+"]"
					};
				//
				ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf09"});
				//
				return r;

				//z.va = ALiBS.sy.em(-4,"ALiBS.cp.cf09",{m0:"Found p"+"rint",m1:ALiBS.sg.tr(z.d[i])}); 
				//ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;

			}
		}
		//
		return 1;
	},
	// *****************************************************************
	cf10: function(s) // Code Formating 10 = wrong comment
	// s = (required)(String)text
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf10",le:ALiBS.gv.cp.le});
		//
		// ** * / // test = should generate error 
		// * // // test = should generate error 
		//
		var i0;
		//
		i0 = s.indexOf("/"+"/"+"*");
		//
		if(i0>=0)
		{
			var r = 
				{	
					ec:-1,
					m0:"[ERROR][-1][ALiBS.cp.cf10][TEXT HAS WRONG COMMENT "+"/"+"/"+"*]",
					m1:"[]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf10"});
			//
			return r;
		}
		//
		//
		i0 = s.indexOf("*"+"/"+"/");
		//
		if(i0>=0) 
		{
			var r = 
				{	
					ec:-2,
					m0:"[ERROR][-2][ALiBS.cp.cf10][TEXT HAS WRONG COMMENT "+"/"+"/"+"*]",
					m1:"[]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf10"});
			//
			return r;
		}
		//
		return 1;
	},
	// *****************************************************************
	cf11: function(s) // Code Formating 11 = if{} else{} for(){} at same line
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf11",le:ALiBS.gv.cp.le});
		//
		//if(s){} // test = should generate error 
		//else{}  // test = should generate error 
		//function a(){}; // test = should generate error 
		//
		var v;
		var d;
		var di;
		var e = [/[f]unction/,/[i]f\(/,/[e]lse/,/[f]or\(/,/[w]hile\(/];
		//
		v = s;
		//
		d = v.split("\n");
		//
		for(var i=0;i<d.length;i++)
		{
			di = ALiBS.sg.tr(d[i]);
			//
			if(di=="") continue;
			//
			else if(ALiBS.sg.co(di,e)>=0)
			{
				if(di.indexOf(String.fromCharCode(123))>=0 || di.indexOf(String.fromCharCode(125))>=0) // { = 123  } = 125
				{
					if(di.indexOf("ALi"+"BS")<0)
					{
						var r = 
							{	
								ec:-1,
								m0:"[ERROR][-1][ALiBS.cp.cf11][f"+"unction/if/e"+"lse/f"+"or/w"+"hile + {} at same line]",
								m1:"["+di+"]"
							};
						//
						ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf11"});
						//
						return r;
					}
				}
				else
				{
					//
				}
			}
		}
		//
		return 1;
	},
	// *****************************************************************
	cf12: function(s) // Code Formating 12 = "= {" or ": {"
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf12",le:ALiBS.gv.cp.le});
		//
		//var a = { // test = should generate error 
		//	}
		//
		//a : { // test = should generate error 
		//	}
		//
		var v;
		var d;
		var di;
		var l0;
		var l1;
		//
		v = s;
		//
		d = v.split("\n");
		//
		for(var i=0;i<d.length;i++)
		{
			di = ALiBS.sg.tr(d[i]);
			//
			if(di=="") continue;
			//
			else
			{
				l0 = "= " + String.fromCharCode(123); // = {
				l1 = ": " + String.fromCharCode(123); // : {
				//
				if(di.indexOf(l0)>=0 || di.indexOf(l1)>=0)
				{
					l0 = l0 + String.fromCharCode(125); // = {}
					l1 = l1 + String.fromCharCode(125); // : {}
					//
					if(di.indexOf(l0)<0 && di.indexOf(l1)<0) // not found
					{
						var r = 
							{	
								ec:-1,
								m0:"[ERROR][-1][ALiBS.cp.cf12][Wrong Format " + 
									String.fromCharCode(61) + String.fromCharCode(123) + "OR " + 
									String.fromCharCode(58) + String.fromCharCode(123) + "]",  // Wrong Format "= {" ": {"
								m1:"["+di+"]"
							};
						//
						ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf12"});
						//
						return r; 
					}
				}
				else
				{
					//
				}
			}
		}
		//
		return 1;
	},
	// *****************************************************************
	cf14: function(s) // Code Formating 14 = Log Control
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
		//
		// test = should generate error 
		//
		var z = {};
		//
		var v;
		var d;
		var di;
		//
		d = s.split("\n");
		//
		for(var i=0;i<d.length;i++) // find / / Z libs in comments
		{
			if(d[i].indexOf("Z"+"LiBS"+".sy.la(7,")>=0)
			{
				// OK
			}
			else if(d[i].indexOf("Z"+"LiBS")>=0)
			{
				var r = 
					{	
						ec:-8,
						m0:"[ERROR][-8][ALiBS.cp.cf14][Z"+"LiBS]", 
						m1:"["+d[i]+"]"
					};
				//
				ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
				//
				return r; 
			}
		}
		//
		v = ALiBS.cp.rsc(s); // single comment
		//
		v = ALiBS.cp.rmc(v); // multiple comment
		//
		d = v.split("\n");
		//
		a0 = new Array(); // array for c0 and c1
		a1 = new Array(); // array for c2 and c3
		a2 = new Array(); // array for c4 and c5
		//
		for(var i=0;i<d.length;i++)
		{
			di = ALiBS.sg.tr(d[i]);
			di = di.toLowerCase();
			//
			//
			// count f unction and ALiBS. sy.la
			//
			if(v.indexOf("AL"+"iBS.sy = ")>=0)// inside ALiBS.sy = ALIBS.sy.la = log to array
			{
				if(di.indexOf("f"+"unction ALiBS"+"_sy_la(l,m,p)")>=0)// ALIBS.sy.la = log to array
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count ALiBS.sy.la itself
				}
				else if(di.indexOf("la: f"+"unction(l,m,p)")>=0)// ALIBS.sy.la = log to array
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count ALiBS.sy.la itself
				}
				else if(di.indexOf("rl: f"+"unction(v,ts)")>=0) // ALiBS.sy.rl = reduce log ALiBS.gv.sy.gl
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count ALiBS.sy.la itself
				}
				else if(di.indexOf("yy: f"+"unction(o,l,d)")>=0) // ALiBS.sy.yy = log string formatted
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count ALiBS.sy.la itself
				}
				else if(di.indexOf("fm: f"+"unction(ac,sf,ts,p)")>=0) // ALiBS.sy.yy = log string formatted
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count ALiBS.sy.la itself
				}
				else if(di.indexOf("f"+"unction pr"+"int(a)")>=0) // function print = Server
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count print 
				}
				else if(di.indexOf("pr"+"int = f"+"unction(a)")>=0) // function print = Server
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count print 
				}
				else if(di.indexOf("alibs.gv.sy.out.rs = f"+"unction()")>=0) // function print = Server
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count print 
				}
				else if(di.indexOf("z.f0 = f"+"unction(")>=0 || di.indexOf("z.f0 = n"+"ew f"+"unction(")>=0) // function print = Server
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count print 
				}
				else if(di.indexOf("window.document.onkeydown = f"+"unction")>=0 || di.indexOf("window.document.onkeypress = f"+"unction")>=0 
					|| di.indexOf("window.document.onkeyup = f"+"unction")>=0) // 
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count print 
				}
				else if(di.indexOf("alibs.gv.sy.lgrs = f"+"unction")>=0 || di.indexOf("alibs.gv.sy.lgpt = f"+"unction")>=0) // 
				{
					ALiBS.sy.la(7+0,"Excluded Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// do not count print 
				}
				else
				{
					if(di.indexOf("f"+"unction")>=0)
					{
						ALiBS.sy.la(7+0,"Counted Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
						//
						ALiBS.gv.cp.c0++; // count functions
						//
						a0[a0.length] = di;
						//
						ALiBS.sy.la(7+0,"Method/Function Count = "+ALiBS.gv.cp.c0+ALiBS.gv.sy.se+ALiBS.gv.sy.se,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					}
				}
			}
			else // other classes/modules
			{
				if(di.indexOf("f"+"unction")>=0)
				{
					ALiBS.sy.la(7+0,"Counted Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					ALiBS.gv.cp.c0++; // count functions
					//
					a0[a0.length] = di;
					//
					ALiBS.sy.la(7+0,"Method/Function Count = "+ALiBS.gv.cp.c0+ALiBS.gv.sy.se+ALiBS.gv.sy.se,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				}
			}
			//
			// ****************
			//
			if(di.indexOf("alibs"+".sy.la(6"+"+0,"+'"method execution started..."')>=0)
			{
				ALiBS.sy.la(7+0,"Counted alibs.sy.la = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c1++; // count ALiBS.sy.la
				//
				a0[a0.length] = di;
				//
				ALiBS.sy.la(7+0,"alibs.sy.la Count = "+ALiBS.gv.cp.c1+ALiBS.gv.sy.se+ALiBS.gv.sy.se,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			// ****************
			//
			if(di.indexOf("f"+"unction")>=0) // count ALL Methods / Functions
			{
				if(v.indexOf("AL"+"iBS.sy = ")>=0 && di.indexOf("n"+"ew f"+"unction")>=0) // inside ALiBS.sy
				{
					// do NOT count = new Function in ALiBS.sy
				}
				else
				{
					ALiBS.sy.la(7+0,"Counted ALL Method/Function = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					ALiBS.gv.cp.c2++; // count ALL functions
					//
					a1[a1.length] = di;
					//
					ALiBS.sy.la(7+0,"ALL Method/Function Count = "+ALiBS.gv.cp.c2+ALiBS.gv.sy.se+ALiBS.gv.sy.se,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
					//
					// **************************
					//
					ALiBS.gv.cp.lfp2 = "" + di; // last function parameters
//console.log("ALiBS.gv.cp.lfp="+ALiBS.gv.cp.lfp2);
					//
					ex = /[(]{1}[\ ]{0,}[a-zA-Z0-9_,\ ]{1,}[)]{1}/; 
					ALiBS.gv.cp.lfp2 = ALiBS.gv.cp.lfp2.match(ex); // last function parameters
//console.log("ALiBS.gv.cp.lfp="+ALiBS.gv.cp.lfp2);
					//
					if(ALiBS.gv.cp.lfp2 && ALiBS.gv.cp.lfp2[0])
					{
						ALiBS.gv.cp.lfp2 = ALiBS.gv.cp.lfp2[0].replace("(p)","").replace("(","").replace(",p)","").replace(")","");
//console.log("ALiBS.gv.cp.lfp2="+ALiBS.gv.cp.lfp2);
					}
					//
					if(ALiBS.gv.cp.lfp2)
					{
						a0 = ALiBS.gv.cp.lfp2.split(","); // last function parameters
						//
//console.log("(antes)ALiBS.gv.cp.c9="+ALiBS.gv.cp.c9);
//console.log("(antes)a0.length="+a0.length);
						ALiBS.gv.cp.c9 += a0.length; // function parameters to validate
//console.log("(depois)ALiBS.gv.cp.c9="+ALiBS.gv.cp.c9);
					}
				}
			}
			//
			if(di.indexOf("r"+"eturn ")>=0 || di.indexOf("r"+"eturn;")>=0) // count ALL returns
			{
				ALiBS.sy.la(7+0,"Counted ALL Return = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c3++; // count ALL returns
				//
				a1[a1.length] = di;
				//
				ALiBS.sy.la(7+0,"ALL Return Count = "+ALiBS.gv.cp.c3+ALiBS.gv.sy.se+ALiBS.gv.sy.se,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			// ****************
			//
			if(di.indexOf("v"+"ar z =")>=0) // count ALL var z
			{
				ALiBS.sy.la(7+0,"Counted var z = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c4++; // count ALL functions
				//
				a2[a2.length] = di;
			}
			//
			if(di.indexOf("z = nu"+"ll")>=0) // count ALL z = null
			{
				ALiBS.sy.la(7+0,"Counted z = null = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c5++; // count ALL returns
				//
				a2[a2.length] = di;
			}
			//
			if(di.indexOf("swit"+"ch(z.ec)")>=0) // count ALL switch(e)
			{
				ALiBS.sy.la(7+0,"Counted switch(z.ec) = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c6++; // count switch(e)
				//
				a2[a2.length] = di;
			}
			//
			// ****************
			//
			if(di.indexOf("ali"+"bs.gv.")>=0 && di.indexOf(".rl = ")>=0) // ALiBS.gv...rl count
			{
				ALiBS.sy.la(7+0,"Counted ALiBS.gv..."+"rl = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c7++; // ALiBS.gv...rl count
				//
				a2[a2.length] = di;
				//
				// *************************
				//
				z.v_rl = ALiBS.sg.as(di,'= "');  
				z.v_ts = ALiBS.dt.ts({dt:5});
				if(z.v_rl<z.v_ts) // 
				{
					z.v_va = ALiBS.sy.em(-21,"ALiBS.cp.cf14",{m0:"Release < Timestamp",m1:ALiBS.sg.tr(di),mg:"WARNING"}); 
					ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.v_va.m0+z.v_va.m1;
				}
			}
			if(di.indexOf("ali"+"bs.sy.la(0+0,")>=0 && di.indexOf("ali"+"bs.gv.")>=0 && di.indexOf(".rl=")>=0) // ALiBS.sy.la(0+0, + ALiBS.gv.c5.rl= count
			{
				ALiBS.sy.la(7+0,"Counted ALiBS.sy.la(0+0,ALiBS.gv..."+"rl="+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c8++; // ALiBS.sy.la(0+0, + ALiBS.gv.c5.rl= count
				//
				a2[a2.length] = di;
			}
			//
			// ****************
			//
			if(di.indexOf("ex"+"ception: e"+"rror: inval"+"id para"+"meter t"+"ype")>=0) // count ALL Invalid Parameter Type
			{
				ALiBS.sy.la(7+0,"Counted Invalid Parameter Type = "+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				ALiBS.gv.cp.c10++; // Invalid Parameter Type
				//
				a2[a2.length] = di;
			}
			//
			// ****************
			//
			if(di.match(/[r][\ ]{0,4}[=][\ ]{0,4}[A-Za-z_]/gi) && di.indexOf("r = alibs.sy.em(z.ec")<0) // match r = [^(\"\]
			{
				ALiBS.sy.la(7+0,"Found Invalid Return (r = [A-Za-z_]) ="+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				z.v_va = ALiBS.sy.em(-21,"ALiBS.cp.cf14",{m0:"Found Invalid Return (r = [A-Za-z_])",m1:ALiBS.sg.tr(di),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.v_va.m0+z.v_va.m1;
			}
			//
			// ****************
			//
			if(di.match(/[+][=][\ ]{0,4}[A-Za-z_]/gi)) // match += [A-Za-z_]
			{
				ALiBS.sy.la(7+0,"Found += [A-Za-z_]) ="+di,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				z.v_va = ALiBS.sy.em(-21,"ALiBS.cp.cf14",{m0:"Found += [A-Za-z_])",m1:ALiBS.sg.tr(di),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.v_va.m0+z.v_va.m1;
			}
			//
			// ****************
			//
			if(di=="") continue;
			//
			else if(di.indexOf("alibs"+".sy.la(")>=0)
			{
				var ts = di.substring(di.indexOf("o:"),di.length-1);
				//
				ALiBS.sy.la(7+0,"ts = "+ts,{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
				//
				var tt = /o\:\"alibs[\._][a-z0-9]{1,2}[\._][_a-z0-9]{1,2}/;
				//
				if(!ts.match(tt)) tt = /o\:alibs[\._][a-z0-9]{1,2}[\._][_a-z0-9]{1,2}/; // sem " no inicio
				//
				if(!ts.match(tt)) // Do NOT have ALiBS.xx.xx in o:
				{
					if(di.indexOf("(0")<=0 && di.indexOf("(1")<=0) // level is = 2,3,4,5,6,7
					{
						var r = 
							{	
								ec:-22,
								m0:"[ERROR][-22][ALiBS.cp.cf14][ALiBS.xx.xx Not Set]", 
								m1:"["+d[i]+"]"
							};
						//
						ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
						//
						return r; 
					}
				}
				//
				if(di.indexOf("pas"+"sou")>=0)
				{
					var r = 
						{	
							ec:-7,
							m0:"[ERROR][-7][ALiBS.cp.cf14][pas"+"sou]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				//
				if(di.indexOf(","+"le:")<0) 
				{
					if(di.indexOf("0"+"+0")>0) 
					{
						continue;
					}
					else if(di.indexOf("1"+"+0")>0) 
					{
						continue;
					}
					else
					{
						var r = 
							{	
								ec:-1,
								m0:"[ERROR][-1][ALiBS.cp.cf14][le: Not Set]", 
								m1:"["+d[i]+"]"
							};
						//
						ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
						//
						return r; 
					}
				}
				//			
				//			
				if(di.indexOf(",le:alibs"+".gv")<0) // ALL Should be this way
				{
					var r = 
						{	
							ec:-2,
							m0:"[ERROR][-2][ALiBS.cp.cf14][le: Wrong Set]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				//			
				//			
				if(di.indexOf("(0"+",")>=0)
				{
					var r = 
						{	
							ec:-10,
							m0:"[ERROR][-10][ALiBS.cp.cf14][ALiBS"+".sy.la(0)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(1"+",")>=0)
				{
					var r = 
						{	
							ec:-11,
							m0:"[ERROR][-11][ALiBS.cp.cf14][ALiBS"+".sy.la(1)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(2"+",")>=0)
				{
					var r = 
						{	
							ec:-12,
							m0:"[ERROR][-12][ALiBS.cp.cf14][ALiBS"+".sy.la(2)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(3"+",")>=0)
				{
					var r = 
						{	
							ec:-13,
							m0:"[ERROR][-13][ALiBS.cp.cf14][ALiBS"+".sy.la(3)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(4"+",")>=0)
				{
					var r = 
						{	
							ec:-14,
							m0:"[ERROR][-14][ALiBS.cp.cf14][ALiBS"+".sy.la(4)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(5"+",")>=0)
				{
					var r = 
						{	
							ec:-15,
							m0:"[ERROR][-15][ALiBS.cp.cf14][ALiBS"+".sy.la(5)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(6"+",")>=0)
				{
					var r = 
						{	
							ec:-16,
							m0:"[ERROR][-16][ALiBS.cp.cf14][ALiBS"+".sy.la(6)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else if(di.indexOf("(7"+",")>=0)
				{
					var r = 
						{	
							ec:-17,
							m0:"[ERROR][-17][ALiBS.cp.cf14][ALiBS"+".sy.la(7)]", 
							m1:"["+d[i]+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
				else
				{
					if(di.indexOf("(0"+"+0"+",")>=0)
					{
						if(di.indexOf("le"+":")>=0)
						{
							var r = 
								{	
									ec:-5,
									m0:"[ERROR][-5][Level 0+0 With le:]", 
									m1:"["+d[i]+"]"
								};
							//
							ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
							//
							return r; 
						}
						else
						{
							continue;
						}
					}
					else if(di.indexOf("(1"+"+0"+",")>=0)
					{
						if(di.indexOf("le"+":")>=0)
						{
							var r = 
								{	
									ec:-6,
									m0:"[ERROR][-6][Level 1+0 With le:]", 
									m1:"["+d[i]+"]"
								};
							//
							ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
							//
							return r; 
						}
						else
						{
							continue;
						}
					}
					else if(di.indexOf("(5"+"+0"+",")>=0)
					{
						continue;
					}
					else if(di.indexOf("(6"+"+0"+",")>=0)
					{
						continue;
					}
					else if(di.indexOf("(7"+"+0"+",")>=0)
					{
						continue;
					}
					else if(di.indexOf("(8"+"+0"+",")>=0)
					{
						continue;
					}
					else
					{
						var r = 
							{	
								ec:-3,
								m0:"[ERROR][-3][ALiBS.cp.cf14][Wrong Level]", 
								m1:"["+d[i]+"]"
							};
						//
						ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
						//
						return r; 
					}
				}
			}
			//
			if(di.indexOf("alibs"+".sy.gl=")>=0 || di.indexOf("alibs"+".sy.gl =")>=0)
			{
				var r = 
					{	
						ec:-4,
						m0:"[ERROR][-4][ALiBS.cp.cf14][SET ALiBS"+".sy.gl]", 
						m1:"["+d[i]+"]"
					};
				//
				ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
				//
				return r; 
			}
			//
			if(di.indexOf("alibs"+".gv.")>=0 && (di.indexOf(".le=")>=0 || di.indexOf(".le =")>=0))
			{
				if(di.indexOf("true;")>=0 || di.indexOf("false;")>=0 || di.indexOf("1;")>=0 || di.indexOf("2;")>=0 || di.indexOf("3;")>=0)
				{
					var r = 
						{	
							ec:-9,
							m0:"[ERROR][-9][ALiBS.cp.cf14][SET ALiBS"+".sy.gl]", 
							m1:"["+d[i]+"]["+di+"]"
						};
					//
					ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
					//
					return r; 
				}
			}
			//
			if(di.indexOf("alibs"+".sy.li=")>=0 || di.indexOf("alibs"+".sy.li =")>=0)
			{
				var r = 
					{	
						ec:-22,
						m0:"[ERROR][-22][ALiBS.cp.cf14][SET ALiBS"+".sy.li]", 
						m1:"["+d[i]+"]"
					};
				//
				ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
				//
				return r; 
			}
			//
		}
		//
		if(ALiBS.gv.cp.c0!=ALiBS.gv.cp.c1)
		{
			var r = 
				{	
					ec:-21,
					m0:"[ERROR][-21][ALiBS.cp.cf14][Missing ALiBS"+".sy.la(6"+"+0) in func"+"tion]", 
					m1:"[Fun"+"ctions[c0]"+String.fromCharCode(61)+ALiBS.gv.cp.c0+"][ALi"+"BS.sy.la[c1]"+String.fromCharCode(61)+ALiBS.gv.cp.c1+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a0.length;i++)
			{
				ALiBS.sy.la(5+0,a0[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}
		//
		if(ALiBS.gv.cp.c2!=ALiBS.gv.cp.c3)
		{
			var r = 
				{	
					ec:-23,
					m0:"[ERROR][-23][ALiBS.cp.cf14][Function count DIFFERENT from Return count]", 
					m1:"[Fun"+"ctions[c2]"+String.fromCharCode(61)+ALiBS.gv.cp.c2+"][R"+"eturns[c3]"+String.fromCharCode(61)+ALiBS.gv.cp.c3+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a1.length;i++)
			{
				ALiBS.sy.la(5+0,a1[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}
		//
		if(ALiBS.gv.cp.c4!=ALiBS.gv.cp.c5 && 2*ALiBS.gv.cp.c4!=ALiBS.gv.cp.c5)
		{
			var r = 
				{	
					ec:-24,
					m0:"[ERROR][-24][ALiBS.cp.cf14][var z count DIFFERENT from z = null count]", 
					m1:"[v"+"ar z[c4]"+String.fromCharCode(61)+ALiBS.gv.cp.c4+"][z = nu"+"ll[c5]"+String.fromCharCode(61)+ALiBS.gv.cp.c5+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a1.length;i++)
			{
				ALiBS.sy.la(5+0,a1[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}
		//
		if(ALiBS.gv.cp.c4!=ALiBS.gv.cp.c6)
		{
			var r = 
				{	
					ec:-25,
					m0:"[ERROR][-25][ALiBS.cp.cf14][Function count DIFFERENT from switch(z.ec) count]", 
					m1:"[v"+"ar z[c4]"+String.fromCharCode(61)+ALiBS.gv.cp.c4+"][S"+"witch(z.ec)[c6]"+String.fromCharCode(61)+ALiBS.gv.cp.c6+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a1.length;i++)
			{
				ALiBS.sy.la(5+0,a1[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}
		//
		if(ALiBS.gv.cp.c7!=1)
		{
			var r = 
				{	
					ec:-26,
					m0:"[ERROR][-26][ALiBS.cp.cf14][ALiBS.gv..."+"rl count DIFFERENT from 1]", 
					m1:"[ALi"+"BS.gv..."+"rl[c7]"+String.fromCharCode(61)+ALiBS.gv.cp.c7+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a1.length;i++)
			{
				ALiBS.sy.la(5+0,a1[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}
		//
		if(ALiBS.gv.cp.c8!=1)
		{
			var r = 
				{	
					ec:-27,
					m0:"[ERROR][-27][ALiBS.cp.cf14][ALiBS.sy.la(0+0, + ALiBS.gv..."+"rl count DIFFERENT from 1]", 
					m1:"[ALi"+"BS.sy.la(0+0, + ALi"+"BS.gv.c5.rl[c8]"+String.fromCharCode(61)+ALiBS.gv.cp.c8+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a1.length;i++)
			{
				ALiBS.sy.la(5+0,a1[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}
		//
		if(ALiBS.gv.cp.c9!=ALiBS.gv.cp.c10)
		{
			var r = 
				{	
					ec:-25,
					m0:"[ERROR][-25][ALiBS.cp.cf14][Parametes count DIFFERENT from Invalid Parameter Type count]", 
					m1:"[p"+"arametes[c9]"+String.fromCharCode(61)+ALiBS.gv.cp.c9+"][I"+"nvalid Parameter Type[c10]"+String.fromCharCode(61)+ALiBS.gv.cp.c10+"]"
				};
			//
			ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.cf14"});
			//
			for(var i=0;i<a1.length;i++)
			{
				ALiBS.sy.la(5+0,a1[i],{o:"ALiBS.cp.cf14",le:ALiBS.gv.cp.le});
			}
			//
			return r; 
		}

//ALiBS.gv.cp.c9 += a0.length; // function parameters to validate



		//
		return 1;
	},
	// *****************************************************************
	cf15: function(s) // Code Formating 15 = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf15",le:ALiBS.gv.cp.le});
		//
	},
	// *****************************************************************
	cf16: function(s) // Code Formating 16 = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf16",le:ALiBS.gv.cp.le});
		//
	},
	// *****************************************************************
	cf17: function(s) // Code Formating 17 = Variables without var
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf17",le:ALiBS.gv.cp.le});
		//
		// TODO(2011-07-20):
	},
	// *****************************************************************
	cf18: function(s) // Code Formating 18 = tabs, spaces
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.cf18",le:ALiBS.gv.cp.le});
		//
		// TODO(2011-07-20):
	},
	// *****************************************************************
	al0: function(s) // pre-req for ALiBS
	// s = (required)(String)text without comments
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.cp.al0",le:ALiBS.gv.cp.le});
		//
		var z = {};
		//
		z.a = new Array();
		// 
		z.a[z.a.length] = s; 
		//
		z.d = z.a[z.a.length-1].split("\n");
		//
		//d = v.split("\n");
		//
/*		
		x = "eval";
		x = "self";
		x = "with";
		x = "this";
		x = "that";
		x = "substr(";
		//
		x = "aaaa: func";
		x = "aaaaa: func";
		//

*/		//
		for(var i=0;i<z.d.length;i++)
		{
			if(z.d[i].indexOf(".repl"+"ace(")>=0 && z.d[i].indexOf(").repl"+"ace(")<0) // TODO(2014-05-16): WHY ???? should not use replace ? use what instead ? 
			{
				z.va = ALiBS.sy.em(-101,"ALiBS.cp.al0",{m0:"Found .repl"+"ace(",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf("e"+"val")>=0)
			{
				z.va = ALiBS.sy.em(-5,"ALiBS.cp.al0",{m0:"Found e"+"val",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf("s"+"elf")>=0)
			{
				z.va = ALiBS.sy.em(-6,"ALiBS.cp.al0",{m0:"Found s"+"elf",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf("w"+"ith")>=0)
			{
				z.va = ALiBS.sy.em(-7,"ALiBS.cp.al0",{m0:"Found w"+"ith",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf("t"+"his")>=0)
			{
				z.va = ALiBS.sy.em(-8,"ALiBS.cp.al0",{m0:"Found t"+"his",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			if(z.d[i].indexOf("t"+"hat")>=0)
			{
				z.va = ALiBS.sy.em(-9,"ALiBS.cp.al0",{m0:"Found t"+"hat",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			//z.e = /[a-zA-Z0-9_]{1}[a-zA-Z0-9_]{4,}\:\ {0,}[f]unc/; // to NOT be caught by cf11 
			z.e = /[_]{0,}[a-zA-Z0-9]{4,}\:\ {0,}[f]unc/; // to NOT be caught by cf11 
			if(z.d[i].match(z.e))
			{
				z.va = ALiBS.sy.em(-10,"ALiBS.cp.al0",{m0:"Found f"+"unction name w"+"ith 4+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			// ******************************************
			//
			z.e = /[v]ar\ [^zr]{1,}/; // var\ [^z]{1}
			if(z.d[i].match(z.e))
			{
				if(z.d[i].indexOf("var ALiBS")<0 && z.d[i].indexOf("for(var ")<0)
				{
					z.va = ALiBS.sy.em(-10,"ALiBS.cp.al0",{m0:"Found v"+"ar [^z]",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
					ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
				}
			}
			//
			z.e = /[v]ar\ [zr]{1}[a-zA-Z0-9_-]{1,}/; 
			if(z.d[i].match(z.e))
			{
				if(z.d[i].indexOf("var ALiBS")<0 && z.d[i].indexOf("for(var ")<0)
				{
					z.va = ALiBS.sy.em(-10,"ALiBS.cp.al0",{m0:"Found v"+"ar [^z]",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
					ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
				}
			}
			//
			// ******************************************
			//
			if(z.d[i].indexOf("f"+"unction")>=0) // count ALL Methods / Functions
			{
				ALiBS.gv.cp.lfp = "" + z.d[i]; // last function parameters
//console.log("ALiBS.gv.cp.lfp="+ALiBS.gv.cp.lfp);
				//
				z.e = /[(]{1}[\ ]{0,}[a-zA-Z0-9_,\ ]{1,}[)]{1}/; 
				ALiBS.gv.cp.lfp = ALiBS.gv.cp.lfp.match(z.e); // last function parameters
//console.log("ALiBS.gv.cp.lfp="+ALiBS.gv.cp.lfp);
				//
				//ALiBS.gv.cp.lfp = ALiBS.sg.rpc(ALiBS.gv.cp.lfp,[["[(]",""],["p,[)]",""],["[)]",""]]);
				if(ALiBS.gv.cp.lfp && ALiBS.gv.cp.lfp[0])
				{
					ALiBS.gv.cp.lfp = ALiBS.gv.cp.lfp[0].replace("(","").replace(",p)","").replace(")","");
//console.log("ALiBS.gv.cp.lfp="+ALiBS.gv.cp.lfp);
				}
			}
			//
			// ******************************************
			//
			if(ALiBS.sg.co(z.d[i],["ALiBS.sy.la","z[.]la[(]z[.]lv[,]"])<0) // if NOT found these strings
			{
				z.v1 = ALiBS.sg.rpc(z.d[i],[["var\ z\ ",""],["var\ r\ ",""],["for[^a-z]{1}var\ [a-z]{1}",""],["[rz]{1}[\ ]{0,1}[=]{1}",""]]);
				//
				z.e = /[A-Za-z0-9_\.]{1,}[\ ]{0,}[=]{1}/; 
				z.v0 = z.v1.match(z.e);
				if(z.v0 && z.v0.length>0)
				{
					if(ALiBS.gv.cp.lfp)
					{
						z.a0 = ALiBS.gv.cp.lfp.split(","); // last function parameters
					}
					else
					{
						z.a0 = new Array();
					}
					//
					z.a0[z.a0.length] = /ALiBS[_\.]{1}/;
					//z.a0[z.a0.length] = /z[\.]/;
					z.a0[z.a0.length] = /z[.]p_/;
					z.a0[z.a0.length] = /z[.]v_/;
					z.a0[z.a0.length] = /z[.]ec/;
					z.a0[z.a0.length] = /z[.]m0/;
					//
	//console.log("z.a0="+z.a0);
	//console.log("ALiBS.gv.cp.lfp="+ALiBS.gv.cp.lfp);
					//
					for(var j=0;j<z.v0.length;j++)
					{
						if(ALiBS.sg.co(z.v0[j],z.a0)<0) // string contains any item of Array (match item = not necessarily exact item) = like indexOf() for many items
						{
							z.va = ALiBS.sy.em(-10,"ALiBS.cp.al0",{m0:"Found ir"+"regular va"+"riable",m1:ALiBS.sg.tr(z.v0[j]+"["+z.d[i]+"]"),mg:"WARNING"}); 
							ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
						}
					}
				}
			}
			//
			// ******************************************
			// Parameters Names
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 1 parameter
			{
				z.va = ALiBS.sy.em(-21,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 2 parameters
			{
				z.va = ALiBS.sy.em(-22,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 3 parameters
			{
				z.va = ALiBS.sy.em(-23,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 4 parameters
			{
				z.va = ALiBS.sy.em(-24,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 5 parameters
			{
				z.va = ALiBS.sy.em(-25,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 6 parameters
			{
				z.va = ALiBS.sy.em(-26,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			z.e = /ction\ {0,}[a-zA-Z0-9_]{0,}\([a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{1}\,[a-zA-Z0-9_]{3,}\,{0,1}/; 
			if(z.d[i].match(z.e)) // 7 parameters
			{
				z.va = ALiBS.sy.em(-27,"ALiBS.cp.al0",{m0:"Found f"+"unction parameter name w"+"ith 3+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			x = "f"+"unction(a)"; // nao deve pegar
			//
			x = "f"+"unction(a1)";
			x = "f"+"unction(a1,a)";
			//
			x = "f"+"unction(a,a2)";
			x = "f"+"unction(a,b,a3)";
			x = "f"+"unction(a,b,c,a4)";
			x = "f"+"unction(a,b,c,d,a5)";
			x = "f"+"unction(a,b,c,d,e,a6)";
			x = "f"+"unction(a,b,c,d,e,f,a7)";

			//
			var z0; // ok pega agora
			var r0;
			var zr;
			var rz;
			//
/*
			TODO(2012-07-04): var name com 4+
			z.e = /[a-zA-Z0-9_]{1}[a-zA-Z0-9_]{3,}\ {0,}[\=\:]/; 
			if(z.d[i].match(z.e))
			{
				z.va = ALiBS.sy.em(-10,"ALiBS.cp.al0",{m0:"Found v"+"ar name w"+"ith 4+ chars",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}
			//
			var abc = "";
			var abcd = "";
			def = "";
			fgh:"";
			fghi:"";
			//
*/
/*			//
			if(z.d[i].indexOf("s"+"ubstr(")>=0)
			{
				z.va = ALiBS.sy.em(-10,"ALiBS.cp.al0",{m0:"Found s"+"ubstr(",m1:ALiBS.sg.tr(z.d[i]),mg:"WARNING"}); 
				ALiBS.gv.cp.er[ALiBS.gv.cp.er.length] = z.va.m0+z.va.m1;
			}*/

		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return 1;
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.cp("+(++ALiBS.gv.cp.nn)+"x)",{o:"ALiBS.cp"});
ALiBS.sy.la(0+0,"ALiBS.gv.cp.rl="+ALiBS.gv.cp.rl,{o:"ALiBS.cp"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.cp.le="+ALiBS.gv.cp.le+ALiBS.gv.sy.se,{o:"ALiBS.cp"});
//
// *************************************************************************

