/* *************************************************************************
// INFO:

- ALiBS Reports (LDAP, DBase, LDIF, CSV, XML, etc)

******************************

- Usar para

	- Gerar relatórios PDF, CSV

	- Gerar LDIFs
	
- Dados de Entrada: LDAP, DBase, LDIF, CSV, XML, etc

- Dados de Saída: LDAP, DBase, LDIF, CSV, XML, etc

******************************

// *************************************************************************

- TODO(2015-04-29): URGENTE


- TODO(2015-04-29): NÃO URGENTE

	- passar parâmetros:

		rp.outfiles: 	
			"" = DEFAULT = pdf,csvz (js, html, csv deletados)
				pdf, 				pdfz (zipped RP1001.pdf.zip alone), 	pdfza (zipped in RP1001.zip with others) 
				csv, 				csvz (zipped RP1001.csv.zip alone), 	csvza (zipped in RP1001.zip with others)
				js (já é gerado, NÃO apagar), 	jsz  (zipped RP1001.js.zip alone), 	jsza  (zipped in RP1001.zip with others)
				xml, 				xmlz (zipped RP1001.xml.zip alone), 	xmlza (zipped in RP1001.zip with others)
				html,				htmlz (zipped RP1001.html.zip with js), htmlza (zipped in RP1001.zip with others)
									TODO(2015-04-29):incluir /Templates ????

// *************************************************************************

- ALiBS Queries and Reports - Padrão para ALiBS (código fonte reduced)
		ALiBS.gv.wq.ff.AQR0000

R = Relatórios Bradesco = Relatórios CUSTOM para clientes 
	TODO(2015-04-29):substituir 
		var ff = {}; // report functions
		ff.R0000
			por
		ALiBS.gv.wq.ff = {}; // report functions
		ALiBS.gv.wq.ff.R0000

	CR1000 = User - Usuários Migrados

	CR9000 = Relatórios Temporários, usados para gerar LDIFs uma única vez
			ALiBS.gv.wq.ff.RP0000


ALDQ = ALiBS LDAP Data Quality = Relatório de Inconsistências

	ALDQ1000 = Users with Multiples CNs
	ALDQ1001 = Users Outside of Users Container
	ALDQ1002 = Users x ACLs Granted Directly (should NOT exist)
	ALDQ1003 = Users x Alias

	//

	ALDQ2000 = Groups with Multiples CNs
	ALDQ2001 = Groups Outside of Groups Container
	ALDQ2002 = Groups x ACLs
	ALDQ2002 = Groups x Alias

	ALDQ2099 = Groups x Users (Groupmembership, securityEquals, member, equivalentTome)

	// 

	ALDQ3000 = Containers with Multiples OUs
	ALDQ3001 = 
	ALDQ3002 = Containers x ACLs Granted Directly



ALDI = ALiBS LDAP Data Information = Relatório de Informação

	ALDI1000 = User - Blocked Users (login disabled, lockedbyIntruder, gracelogins, )


// *************************************************************************

Preço: R$ 20.000,00
	- 50+ Relatórios Pré-Definidos

Suporte: R$ 
	- O que é o suporte?

Customização de Templates: de 20 horas x 100 horas x R$ 100 = R$ 10.000,00

Customização de Relatórios Exitentes: de 20 a 50 horas x R$ 100 = R$ 5.000,00 (Pode diminuir se a quantidade for média/grande)

Criação de Novos Relatórios LDAP: de 50 a 100 horas x R$ 100 = R$ 10.000,00 (Pode diminuir se a quantidade for média/grande)

Criação de Novos Relatórios Outras Fontes de Dados: de 100 a 200 horas x R$ 100 = R$ 20.000,00 (Pode diminuir se a quantidade for média/grande)

// *************************************************************************

*/
//
// *************************************************************************
/** 
	Class ALiBS.wq

		Description: 
			- Queries and Reports Engine;

	--------------------------------------------------------------------
*/
// *************************************************************************
//
if(!ALiBS.gv.wq) ALiBS.gv.wq = {}; // global values
//
if(!ALiBS.gv.wq.le) ALiBS.gv.wq.le = 0; // local log status
//
ALiBS.gv.wq.nn = 0; // x times loaded
//
ALiBS.gv.wq.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wq.rl = "14.0822.0001";
//
// *************************************************************************
//
ALiBS.wq = // table
{
	t: function(p) // table
	/** 
	Method <instance> <ALiBS.wq.t> ALiBS.wq.t(p)

		Description: Creates a new <instance> of Dinamyc Table Object <ALiBS.wq.t>

		Parameters:
			p = (optional)(JSON) parameters {d:"y",o:"ALiBS.el",le:true,so:print} 

				tb_rf = (string)table var T1 = new ALiBS.wq.t({tb_rf:"T1"});
				tb_id = (string)table id (used in grid elements id)

		Observations:
	
		Return Value:
			<instance> <ALiBS.wq.t> Dinamyc Table Object <ALiBS.wq.t>

		Example:
			var T9 = new ALiBS.wq.t({tb_rf:"T9",tb_id:"_t9"}); 

	--------------------------------------------------------------------
	*/
	// p = (optional)(JSON)parameters {} 
	//
	//	tb_ntb = (number)number of table fields (also can be initialized after using t.tb.ntb=4)
	//
	// return table object
	{
	},
	// *****************************************************************
	// *****************************************************************
};
// *************************************************************************
//
//ALiBS.wq.td = ALiBS.wq.s2t; // pointer - OLD compatibility (do NOT use var)
//
if(ALiBS.gv.sy.ev=="Server")
{
	//ALiBS_wq_xx = ... ; // pointer (do NOT use var)
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wq("+(++ALiBS.gv.wq.nn)+"x)",{o:"ALiBS.wq"});
ALiBS.sy.la(0+0,"ALiBS.gv.wq.rl="+ALiBS.gv.wq.rl,{o:"ALiBS.wq"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.wq.le="+ALiBS.gv.wq.le+ALiBS.gv.sy.se,{o:"ALiBS.wq"});
  
