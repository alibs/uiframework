/* *************************************************************************
// INFO:



// *************************************************************************
// TODO:


*/
//
// *************************************************************************
//
if(!ALiBS.gv.b2) ALiBS.gv.b2 = {}; // global values
//
if(!ALiBS.gv.b2.le) ALiBS.gv.b2.le = 0; // local log status
//
ALiBS.gv.b2.nn = 0; // x times loaded
//
ALiBS.gv.b2.rl = "15.0729.1137";
//
// *************************************************************************
//
ALiBS.b2 = // Signature Algorithm for Authentication
{
	sa: function(p)
	/** 
	Method <static> <string> ALiBS.b2.sa(sk,p)

		Description: Get Signature SA0

		Parameters:

			p = (optional)(JSON)parameter {} 

				sa = (number) Signature Algorithm
					0 = SA0 = DEFAULT

				id = (string) Identity
					"NO-USER-IDENTITY" = DEFAULT

				ku = (string) user password / key
					"NO-USER-PASSWORD" = DEFAULT

				ko = (string) OTP Key
					"NOT-OTP-KEY" = DEFAULT

				ot = (string) OTP

				it = (number) Interval (secs) for new OTP
					30 = DEFAULT = Same as for ALiBS.b1.g0

				ts = (number) Timestamp 
					(new Date())-1+1 = DEFAULT = (new Date()).getTime()

				n0 = (string) B64 Name 
					"sa" = DEFAULT

		Observations:
	
		Return Value:
			<static> <string> signature

		Example(s):
			var otp = ALiBS.b2.sa();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

		sk = (String) secret key
			// usar secret key teste: 
				// ABCDEFGHIJKLMNOPQRSTUVWXYZ234567
				// 765432ZYXWVUTSRQPONMLKJIHGFEDCBA   reverse

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			if(typeof(z.sa)!="number") z.sa = 0;
			if(typeof(z.id)!="string") z.id = "NO-USER-IDENTITY";
			if(typeof(z.ku)!="string") z.ku = "NO-USER-PASSWORD";
			if(typeof(z.ko)!="string") z.ko = "NOT-OTP-KEY";
			if(typeof(z.ot)!="string") z.ot = "NO-OTP";
			if(typeof(z.it)!="number") z.it = 30;
			if(typeof(z.ts)!="number") z.ts = (new Date())-1+1;
			if(typeof(z.n0)!="string") z.n0 = "sa";
			//
			ALiBS.sy.la(5+0,"z.sa="+z.sa,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.id="+z.id,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.ku="+z.ku,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.ko="+z.ko,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.ot="+z.ot,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.it="+z.it,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.ts="+z.ts,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			ALiBS.sy.la(5+0,"z.n0="+z.n0,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			if(z.sa==0) // SA0
			{
				z.t0 = Math.floor(z.ts/1000); // 
				z.t1 = Math.floor(z.t0/z.it); // 
				//
				z.m0 = z.ot+z.t1;
				z.m1 = z.m0.split("").reverse().join("");
				//
				z.v0 = z.id+z.ku;
				//
				z.r0 = ALiBS.c9.e4(ALiBS.c7.sh(z.it,z.v0,z.ko,z.m1),{n0:z.n0}); // Super Hash = HMAC-SHA512
				// n = (required)(number) times to run
				// s = (required)(String) salt
				// k = (required)(String) secret key = password
				// d = (required)(String) data = message
			}
			else // SA1
			{
				// To be defined
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.r0; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.b2.sa",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		//ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.b2.sa",le:ALiBS.gv.b2.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.b2("+(++ALiBS.gv.b2.nn)+"x)",{o:"ALiBS.b2"});
ALiBS.sy.la(0+0,"ALiBS.gv.b2.rl="+ALiBS.gv.b2.rl,{o:"ALiBS.b2"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.b2.le="+ALiBS.gv.b2.le+ALiBS.gv.sy.se,{o:"ALiBS.b2"});

