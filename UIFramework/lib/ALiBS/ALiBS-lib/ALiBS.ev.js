/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.ev) ALiBS.gv.ev = {}; // global values
//
if(!ALiBS.gv.ev.le) ALiBS.gv.ev.le = 0; // local log status
//
ALiBS.gv.ev.nn = 0; // x times loaded
//
ALiBS.gv.ev.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.ev = // Browser Environment
{
	ua: function(p) // userAgent
	// p = (optional)(JSON)parameters {u:userAgent} 
	//
	// return (String)user agent
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ev.ua",le:ALiBS.gv.ev.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.r = "" + window.navigator.userAgent;
		//
		var r = z.r; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	bn: function(p) // browser name
	// p = (optional)(JSON)parameters {u:userAgent} 
	// 	u = (String)user agent
	//
	// return (String)browser name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ev.bn",le:ALiBS.gv.ev.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.u)!="string") z.u = ALiBS.ev.ua(); // DEFAULT = [EMPTY]
		//
		if(!z.u)
		{
			z.r = null;
		}
		else if(z.u.indexOf("Opera")>=0)
		{
			z.r = "Opera";
		}
		else if(z.u.indexOf("MSIE")>=0)
		{
			z.r = "MSIE";
		}
		else if(z.u.indexOf("Konqueror")>=0)
		{
		 	z.r = "Konqueror";
		}
		else if(z.u.indexOf("Chrome")>=0)
		{
		 	z.r = "Chrome"; // before Safari and Mozilla (they are also included in Chrome user agent)
		}
		else if(z.u.indexOf("Safari")>=0)
		{
		 	z.r = "Safari";
		}
		else if(z.u.indexOf("SeaMonkey")>=0)
		{
		 	z.r = "SeaMonkey";
		}
		else if(z.u.indexOf("Flock")>=0)
		{
		 	z.r = "Flock";
		}
		else if(z.u.indexOf("Firefox")>=0)
		{
		 	z.r = "Firefox";
		}
		else if(z.u.indexOf("Mozilla")>=0)
		{
		 	z.r = "Mozilla";
		}
		else
		{
			z.r = z.u; 
		}
		//
		var r = z.r; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"User Agent="+r,{o:"ALiBS.ev.bn",le:ALiBS.gv.ev.le});
		//
		return r; 
	},
	// *****************************************************************
	bv: function(p) // browser version
	// p = (optional)(JSON)parameters {u:userAgent} 
	// 	u = (String)user agent
	//
	// return (String)browser version
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ev.bv",le:ALiBS.gv.ev.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.u)!="string") z.u = ALiBS.ev.ua(); // DEFAULT = [EMPTY]
		//
		if(!z.u)
		{
			z.r = null;
		}
		else if((z.i=z.u.indexOf("Opera"))>=0)
		{
		 	z.r = z.u.substring(z.i+5+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("MSIE"))>=0)
		{
			z.r = z.u.substring(z.i+4+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("Konqueror"))>=0)
		{
		 	z.r = z.u.substring(z.i+9+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("Chrome"))>=0)
		{
		 	z.r = z.u.substring(z.i+6+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("Safari"))>=0)
		{
		 	z.r = z.u.substring(z.i+6+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("SeaMonkey"))>=0)
		{
		 	z.r = z.u.substring(z.i+9+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("Flock"))>=0)
		{
		 	z.r = z.u.substring(z.i+5+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("Firefox"))>=0)
		{
		 	z.r = z.u.substring(z.i+7+1,z.u.length);
		}
		else if((z.i=z.u.indexOf("Mozilla"))>=0)
		{
		 	z.r = z.u.substring(z.i+8+1,z.u.length);
		}
		//
		//
		//
		if((z.i=z.r.indexOf(";"))>=0)
		{
			z.r = z.r.substring(0,z.i);
		}
		//
		if((z.i=z.r.indexOf("("))>=0)
		{
			z.r = z.r.substring(0,z.i);
		}
		//
		if((z.i=z.r.indexOf(")"))>=0)
		{
			z.r = z.r.substring(0,z.i);
		}
		//
		if((z.i=z.r.indexOf(" "))>=0)
		{
			z.r = z.r.substring(0,z.i);
		}
		//
		var r; // [BEST-PRACTICE]
		//
		if(z.r) r = z.r;
		else r = z.u;
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	bp: function(p) // browser platform
	// p = (optional)(JSON)parameters {u:userAgent} 
	// 	u = (String)user agent
	//
	// return (String)browser platform
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ev.bp",le:ALiBS.gv.ev.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.u)!="string") z.u = ALiBS.ev.ua(); // DEFAULT = [EMPTY]
		//
		if(!z.u)
		{
			z.r = null;
		}
		else
		{
			if(z.u.indexOf("Linux")>=0)
			{
				z.r = "Linux";
			}
			else if(z.u.indexOf("Windows")>=0)
			{
			 	z.r = "Windows";
			}
			else
			{
				z.r = "Other S.O."; // Mac, Unix ???? TODO(2013-05-20):Obter e acertar userAgents de Mac e Unix
			}
			//
			//
			//
			if(z.u.indexOf("x86_64")>=0)
			{
				z.r = z.r + " x86_64";
			}
			else
			{
				//z.r = z.r + " xUnknown";
			}
		}
		//
		var r = z.r; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	bl: function(p) // browser language
	// p = (optional)(JSON)parameters {u:userAgent} 
	// 	u = (String)user agent
	//
	// return (String)browser language
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ev.bl",le:ALiBS.gv.ev.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		if(typeof(z.u)=="string")
		{
			z.r = ALiBS.ev._bl(p.u); 
		}
		else if(typeof(window)!="undefined" && typeof(window.navigator)!="undefined")
		{
			if(typeof(window.navigator.language)!="undefined")
			{
				z.r = window.navigator.language;
			}
			else if(typeof(window.navigator.userLanguage)!="undefined")
			{
				z.r = window.navigator.userLanguage;
			}
			else
			{
				z.r = null;
			}
		}
		else
		{
			z.r = ALiBS.ev._bl(ALiBS.ev.ua());
		}
		//
		var r = z.r; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	_bl: function(u,p) // browser language
	// u = (String)user agent
	// p = (optional)(JSON)parameters {} 
	//
	// return (String)browser language

	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ev._bl",le:ALiBS.gv.ev.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.u = u.replace("(x86_64)","").replace("x86_64;","").replace("i686","").replace("X11;","");
		z.u = z.u.replace("Linux","").replace("Windows NT","").replace("Windows","");
		z.u = z.u.replace("compatible","").replace("Konqueror","").replace("MSIE","").replace("U;","");
		//
		if((z.i=z.u.indexOf("("))>=0)
		{
			z.u = z.u.substring(z.i+1,z.u.length); // TODO substring sem segundo parametro - cp nao pegou
		}
		//
		if((z.i=z.u.indexOf(")"))>=0)
		{
			z.u = z.u.substring(0,z.i);
		}
		//
		if((z.i=z.u.indexOf("rv:"))>=0)
		{
			z.u = z.u.substring(0,z.i);
		}
		//
		if((z.i=z.u.indexOf("SV"))>=0)
		{
		 	z.u = z.u.substring(0,z.i);
		}
		//
		z.u = ALiBS.sg.rs(z.u,";./0123456789","");
		z.u = ALiBS.sg.tr(z.u);
		//
		var r = z.u; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ev("+(++ALiBS.gv.ev.nn)+"x)",{o:"ALiBS.ev"});
ALiBS.sy.la(0+0,"ALiBS.gv.ev.le="+ALiBS.gv.ev.le,{o:"ALiBS.ev"});
//
ALiBS.sy.la(0+0,"UA="+ALiBS.ev.ua(),{o:"ALiBS.ev"});
if(ALiBS.sg) ALiBS.sy.la(0+0,"Browser Language="+ALiBS.ev.bl(),{o:"ALiBS.ev"});
ALiBS.sy.la(0+0,"Browser Name="+ALiBS.ev.bn(),{o:"ALiBS.ev"});
ALiBS.sy.la(0+0,"Browser Version="+ALiBS.ev.bv(),{o:"ALiBS.ev"});
ALiBS.sy.la(0+0,"Browser Plataform="+ALiBS.ev.bp()+ALiBS.gv.sy.se,{o:"ALiBS.ev"});


