// *************************************************************************
//
var TM0000 = new ALiBS.to.t({tb_rf:"TM0000",tb_id:"_TM0000"}); 
//
TM0000.ff.hg = {}; // hg grid functions
//
// *************************************************************************
//
TM0000.nm.f00 = "DNS/Domain"; // DNS 
TM0000.cp.f00 = "DNS/Domain";
//
TM0000.nm.f01 = "Dominio"; 
TM0000.cp.f01 = "Domínio";
//
TM0000.nm.f02 = "Fonte"; 
TM0000.cp.f02 = "Fonte Renda";
//
TM0000.nm.f03 = "Custo(A)"; 
TM0000.cp.f03 = "Custo(A)";
//
TM0000.nm.f04 = "Renda(A)"; 
TM0000.cp.f04 = "Renda(A)";
//
TM0000.nm.f05 = "Descricao"; 
TM0000.cp.f05 = "Descrição";
//
TM0000.nm.f06 = "level"; // dificuldade
TM0000.cp.f06 = "level"; // 
//
TM0000.nm.f07 = "Pr"; // prioridade
TM0000.cp.f07 = "Pr";
//
TM0000.nm.f08 = "xxxx8"; 
TM0000.cp.f08 = "xxxx8";
//
TM0000.nm.f09 = "VM"; // Hospedagem de VM
TM0000.cp.f09 = "Virtual Machine";
//
TM0000.nm.f10 = "VencDom"; // Vencimento Domínio
TM0000.cp.f10 = "Vencimento<br/>Domínio";
//
TM0000.nm.f11 = "VencHost"; // Vencimento Hospedagem
TM0000.cp.f11 = "Vencimento<br/>Hospedagem";
//
TM0000.tb.hg.tt = 1; // DEFAULT = null = NOT SET = field title control = (see also ALiBS.gv.hg.tt)
//
// *************************************************************************
//
TM0000.tb.hx = [{ty:"[HEADERHX]",tx:"[ Negócios ]",c:"white",bg:"red",al:"center"}]; // Header hx;
//
TM0000.tb.pg.css.hx = 1; // header hx style ON/OFF (0=OFF=NOT CHANGE, 1=ON=CHANGE)
//
TM0000.tb.pg.n = 13; // paging = number of records to show (0 = show all)
//
TM0000.tb.nso = "a03"; // fields in grid NOT to be sorted
//
TM0000.tb.sh = ["f00","f10","f09","f11","f01","f05","f02","f03","f04","f06","f07","f08"]; // fields to show
//
TM0000.tb.fk.f09 = [{ty:"[TABLEINFO]",tx:"Info",c:"red",bg:"white"}]; // table information button
TM0000.tb.fk.f05 = [{ty:"[PAGING]",tx:"Pg",c:"red",bg:"white"}]; // Paging buttons = Navigation
TM0000.tb.fk.f03 = [{ty:"[COMPLEXFILTER]",tx:"CF",c:"red",bg:"white",t:"[]",v:"fm;x"},
								{t:"fm:match",v:"fm;x"},
								{t:"fe:equals",v:"fe;x"},{t:"res>=1",v:"fc;(a00>=1)"},{t:"res>=3",v:"fc;(a00>=3)"}]; // complex filter
//
TM0000.tb.fk.f04 = [{ty:"[PAGELINES]",tx:"Lines",l:"4,5,6,7,8,9,10,11,12,13,20",c:"red",bg:"white"}]; // Page Lines
TM0000.tb.fk.f01 = [{ty:"[FONTSIZE]",tx:"Fon",l:"7px,8px,9px,10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,30px",c:"red",bg:"white"}]; // Font size
TM0000.tb.fk.f02 = [{ty:"[FONTFAMILY]",tx:"Fam",l:"Verdana,Arial",c:"red",bg:"white"}]; // Font family
//
TM0000.tb.fk.f00 = [{ty:"[FONTSTYLE]",tx:"Sty",l:"normal,italic,oblique",c:"red",bg:"white"}]; // Font family
TM0000.tb.fk.f06 = [{ty:"[FONTWEIGHT]",tx:"Wei",l:"normal,bold,bolder,lighter,100,200,300,400,500,600,700,800,900",c:"red",bg:"white"}]; // Font family
//
TM0000.tb.fa = "fm"; // active filter
//
// *************************************************************************
TM0000.ff.hg.dg = function(t,n,p) // display GRID
// t = ()table
// n = ()sorted field
// p = (optional)(JSON)parameter {}
//	rv = (boolean)reverse sort (default=false)
//	y = (String)field type [abcdefghijkrx] (default=f)
{
	var z = p || {}; // [BEST-PRACTICE]
	//
	if(n==t.so.g) ALiBS.to.o(t,n,{rv:!t.so.rv,y:z.y}); // reverse
	else ALiBS.to.o(t,n,{y:z.y}); // normal sort
	//
	var a = ALiBS.hg.g0(t,"nome",{cs:"hg_css0F"}); // 4101, font-size: 16px; padding:2px;
	ALiBS.el.si("iContGrid02a",a);
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
};
// *************************************************************************
TM0000.ff.hg.af = function(t,p) // after load = atualizar datas da agenda
// t = (ALiBS.to)
// f02 = (String)data (para ordenar table)
// f05 = (String)valor 
// f11 = (String)saldo conta 
{
	var z = {}; // [BEST-PRACTICE]
/*	//
	z.d0 = new Date();
	//
	z.x00 = "x00"; // auxiliar index for a01 (calc priority)
	z.x01 = "x01"; // auxiliar index for a01 (calc priority)
	//
	z.gj = ALiBS.dt.gj({s:true}); // today
	z.ho = z.gj.y+"-"+z.gj.sm+"-"+z.gj.sd; // today string
	//z.ho = "2013-01-10";
*/	//
	//
	for(var i=0;i<t.rs.length;i++)
	{
/*		z.f01v = t.rs[i][z.f01];
		//
		// **** SET STATUS to ALL
		//
		if(typeof(t.rs[i][z.f07])=="undefined" || t.rs[i][z.f07]=="") // Status
		{
			t.rs[i][z.f07] = "0"; 
		}
		//
		if(z.f01v.indexOf("HOJE")==0) 
		{
			t.rs[i][z.a00] = z.ho + t.rs[i][z.f02].substr(10,6);  
			//
			if(z.f01v.indexOf("HOJE0")==0) t.rs[i][z.a01] = "0"; 
			else if(z.f01v.indexOf("HOJE9")==0) t.rs[i][z.a01] = "9"; 
			else if(z.f01v.indexOf("HOJEC")==0) t.rs[i][z.a01] = t.rs[i][z.f08]; 
		}
		//
		// FIELD ******************
		//
		else if(z.f01v=="FIELD0") 
		{
			t.rs[i][z.a00] = t.rs[i][z.f02]; 
			t.rs[i][z.a01] = "0"; 
		}
*/	}
	//
	// ********************************
	//
	//
	// ********************************
	// f00 = DNS/VM
	ALiBS.to.o(t,0,{y:"f",rv:true});
	t.tb.fi.f00 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:0}).join(",");
	//
	// ********************************
	// f01 = Dominios
	ALiBS.to.o(t,1,{y:"f",rv:false});
	t.tb.fi.f01 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:1,ke:2}).join(",");
	//
	// ********************************
	// f02 = Fonte de Renda
	z.ar = new Array();
	for(var i=0;i<t.rs.length;i++)
	{
		z.ar[z.ar.length] = t.rs[i].f02.replace(/\<br\ \/\>/g,"").replace(/\s/g,"");
	}
	z.ar2 = z.ar.join(",").split(",");
	//console.log("z.ar2="+z.ar2.join(","));
	//
	z.ar2.sort();
	z.ar3 = ALiBS.to.rd(z.ar2);
	//console.log("z.ar3="+z.ar3.join(","));
	//
	t.tb.fi.f02 = "[]," + z.ar3.join(",");
	//
	// ********************************
	// f06 = level / dificuldade
	ALiBS.to.o(t,6,{y:"f",rv:true});
	t.tb.fi.f06 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:6}).join(",");
	//
	// f07 = Prioridade
	ALiBS.to.o(t,7,{y:"f",rv:true});
	t.tb.fi.f07 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:7}).join(",");
	//
	// ********************************
	// f09 = VM / Virtual Machine
	ALiBS.to.o(t,09,{y:"f",rv:false});
	t.tb.fi.f09 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:9,ke:2}).join(",");
	//
	// ********************************
	// f10 = Vencimento Dominio
	ALiBS.to.o(t,10,{y:"f",rv:false});
	t.tb.fi.f10 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:10,ke:2}).join(",");
	//
	// ********************************
	// f11 = Vencimento Hospedagem
	ALiBS.to.o(t,11,{y:"f",rv:false});
	t.tb.fi.f11 = "[]," + ALiBS.to.rd(t.rs,{y:"f",g:11,ke:2}).join(",");
	// ********************************
	//
	var r = 1;
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
	//
	return r; 
};
// *************************************************************************

