// *************************************************************************
// Alfredo - 2014-04-29 14:30
// *************************************************************************
// Log Settings Before Loading ALiBS
//
var ALiBS = {gv:{sy:{},wi:{}}}; // PRE definitions for global values
//
ALiBS.gv.sy.sygl = -1; // global log level (==ALiBS.sy.gl)
//ALiBS.gv.sy.syli = "ALiBS.cp.bc12,ALiBS.cp.bc13"; // method list (==ALiBS.sy.li)
//
ALiBS.gv.sy.le = 1;
//ALiBS.gv.to.le=0;
//
//ALiBS.gv.sy.gla = 0; // 0 = Auto Print / Clean , 1 = Auto Reduce
//ALiBS.gv.sy.glx=500; // max = trigger
//ALiBS.gv.sy.glu=100; // errors + glu
//
// *************************************************************************

