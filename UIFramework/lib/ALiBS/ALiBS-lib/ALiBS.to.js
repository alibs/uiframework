/* *************************************************************************
// INFO:

******************************

- Separadores em ALiBS - Level Sequence - Many/multiple tables

	-R_c- = Record_Field separator = First Table = should [r]eplace $pIpE$
	 |
	 |____	-R_i- = Record_Field separator = Internal Table = Internal Tables inside a Field = Serialized Tables inside a field 
		 |		
		 |____	-R_0- = Record_Field separator = Internal Table 
			 |
	 		 |____	-R_1- = Record_Field separator = Internal Table 


	- bradesco: brdLoginDisabledDescription 
		- to [r]eplace $pIpE$ and !pIpE! (not needed if brdLoginDisabledDescription is a <ALiBS.t2s> table)
			- brdLoginDisabledDescription should be a <ALiBS.t2s> table

	-M_v- = multivalued attrs = should [r]eplace !pIpE!
		- wl
		- to

- Outros separadores em ALiBS
		
	-R_c- = 
		[OK] - to
		[OK] - wl
		- wf

	-R_i- = Internal Table
		- Fields with i18n = Internationalization (many languages translation table)
		- Atribute brdLoginDisabledDescription (migrar/desmigrar/demitir/reintegrar logging table)

	-T_b-  
	-T_i-  
	-T_0-  

	-F_d- = fields inside a table (SentinelEventSearch.java)

	-F_i- = Internal Fields inside a Field
	-F_0-

	-M_0- 

	-O_0- = Other uses = temp = ONLY inside functions 

	-O_1- = Other uses = temp = ONLY inside functions 

******************************

(3100 fields = 500+2600) Application / temp / calc - Específicos por aplicação
	abcde, A..Z, 
	x = Index (100 fields) TODO(2015-04-19): usar X00, X01 (maiúsculo)

		x00 = custom indexed field (real data content) ALiBS.to.ci() uses real data OR sequencial number

		ex: x00 = subst(f00,4,7)+f01
		ex: x01 = f00+f04

************

(4100 fields = 1500+2600) Queries (ONLY Sentinel uses so many fields)
	fghij, klmno, pqrst, A..Z, 

************

(1500 fields) Data Fields
	fghij, klmno, pqrst (campos na sequência)

************

(600 fields) Control Fields / Metadados / Dados Reais - Mesma funcionalidade para todas as tabelas
	uvwxyz


	TODO():REVER	u22 = subset = (controle de acesso) dentro de um dataset - 
		0 = ALL:CRUD = Public Record
		1 = Restricted Record = DEFAULT (Ator A0000=Admin da App) - (need to set Actor, for example, actor A1005 access subsets 0 + 1 + 2
		- users can be granted access to other/multi subsets

		Direitos:
			- Direitos subentendidos estão no código client/server (CRUD tabelas, CRUD de pastas/estrutura, etc)
			- Outros direitos são por Ator:
				CRUD+E+SA = Create, Read, Update, Delete, Execute, System, Admin

		Atores DEFAULT:
			S0000 = System
			A0000 = Admin
			ALL = Public

		subset = ACL (vai ser assim ????) deveria se chamar ACL ?
			ALL:R		(Direitos de Read)
			S0000:S		(Direitos de System)
			A0000:A		(Direitos de Admin)
			GCIA:D		(Direitos de Delete)

		- Exemplo:

			- u21 = 1 = dataset = (Contas de ALiBS.net)

				- u22 = subset = (dataset = 1) (Contas de ALiBS.net)
					0 = Public = (everybody can see) Despesas 
					1 = Restrict = DEFAULT 
						(Ator A0000=Admin da App can see ALL Restricted) TODO(2015-04-27):which field will be ACL ?
					1000 = Restrict = Receitas (sample: granted to A0150 = Aprovador App)
					2000 = Restrict = Contas a Pagar
					3000 = Restrict = Contas a Receber
					3001 = Restrict = Contas a Receber - Registros de contas a receber confidenciais


	TODO():REVER	u23 = loadset = (controle de carregamento de dados) dentro de um subset - carregar somente os loadsets necessários
				(ordem de carregamento p/ enviar p/ cliente)

				0 = DEFAULT = sempre carrega
				1 = carrega quando solicitado
				2 = carrega quando solicitado

	****************

	u41 = (Timestamp) = BLOCKED = Timestamp of last calc on blocked values OR empty if no blocked

	u41 = (Timestamp) = Blocked by Admin
	u42 = (Code) = Reason Blocked by Admin
			0001 = Admin Blocked
			0002 = INTRUDER = Tentativa de intrusão
			1001 = FIRED = Demitido
			1002 = FRAUD = Análise/Tentativa de Fraude
			
	****************

	v00 = status do registro TODO(2010-09-08): 

		s = syncronized OR read from DBASE
		k = do NOT sync (probably READ-ONLY)

		n = new record in table, not syncronized
		m = modified record in table, not syncronized (which fields?)	
	
		d = deleted record in table, not syncronized, NOT deleted in DBASE yet
		r = deleted record in table, syncronized, deleted in DBASE, recoverable
		x = deleted record in table, syncronized, deleted in DBASE, NOT recoverable (real delete = is it possible?)(==recover space????) TODO(2010-09-08):


		Possible situations (cenarios):

		[s->m->s] s (read to table) --> m (modified in table) --> [AJAX-MODIFY] --> s (syncronized/updated to DBASE)

		[n->s] n (new in table) --> [AJAX-INSERT] --> s (syncronized/created in DBASE)
	
		[s->d->r] s (read to table) --> d (deleted in table) --> [AJAX-DELETE] --> r (syncronized/deleted in DBASE, recoverable)

		[r->s] d (deleted in table AND DBASE) --> [AJAX-UNDELETE]--> s (syncronized/undeleted in DBASE)
		[r->x] r (deleted in table AND DBASE) --> [AJAX-REAL-DELETE]--> x (syncronized/real deleted in DBASE)

		[x->n->s] x (deleted in table AND DBASE, not recoberable) --> n (new in table) --> [AJAX-INSERT] --> s (syncronized/created in DBASE)

	v01 = []; // TODO(2010-09-08): cache for modified field names ????
		v01[v01.length] = "f00"; // modified field
		v01[v01.length] = "f01"; // modified field

	v01 = "f00,f01"; // BEST 

	Other samples
		v01 = selected records - selection 1 (sample)
		v02 = selected records - selection 2 (sample)

************

	v99

	u19 = 10; // number of records in Record History, per type (which field is type ?) (this table should be equal to TS0002-Logs ?)
	u20 = <ALiBS.t2s> Record History on CRUD (limite: 10 last actions per type, 10 adds, 10 modifies, 10 blocks, etc) user, ator, timestamp, info, etc 
									(funciona como brdLoginDisabledDescription)

************

(many fields) HTML Display Fields (ALiBS.hg = grid)

	hf00,hf01,hf02,(...) = campos com formatação HTML
	cf00,cf01,cf02,(...) = cor dos campos para campos coloridos
	ck00 (somente) = registros coloridos = cor dos registros

************

(600 fields) Future Reserved Fields
	000 to 999 = Future Reserved (what for?) (1000 fields)
	_00 to _99 = Future Reserved (what for?) (100 fields)
	$00 to $99 = Future Reserved (what for?) (100 fields)	Currency ?
	@00 to @99 = Future Reserved (what for?) (100 fields)	
	#00 to #99 = Future Reserved (what for?) (100 fields)	Number ?
	!00 to !99 = Future Reserved (what for?) (100 fields)	Text ?

******************************

TODO(2013-07-19)

- un() deve usar fdn()

- usar fdn() onde for possível

- usar fne() e fnd() onde for possível (z.v_ii , z.v_oo, etc)

- nao serializar por DEFAULT as letras abaixo
		- a,b,c,d,e
		- k,q,r,x,
		- y,z
	- somente serializar por DEFAULT fghij,lmnop,stuvw , CASO EXISTAM
	- passar as letras por parametro

- [OK] para usar t00,t01,etc é necessário alterar t.tb.nt para t.tb.ntb

- [OK] alterar t.tb.na para t.tb.nn.a (para todas as letras)

- [OK] nomes de campos da table t00,t01,etc não podem ficar em nm{}
	criar t.tb.nm = {} table field names


// ***************************

Atualizacao de numero de registros e reordenacao:

	1) (auto) qualquer metodo, ao chamar ALiBS.to.l(t) 
	2) (manual) ao chamar ALiBS.to.l(t) 

// ***************************

Atualizacao de numero de campos:

	1) (manual) setar como parâmetro de entrada
	2) (manual) setar após criar a table
	3) (manual) ao chamar ALiBS.un(t) a qualquer momento
		ALiBS.un() somente atualiza o numero de campos se o novo numero de campos 
		for maior que o anterior. Isto porque os campos tipo a,c,i,x, 
		não necessariamente vão ter nomes e o numero de campos seria sempre 0.
		Neste caso, será necessário setar o numero de campos manualmente.
	4) (auto) ao ordenar por campo, ao chamar ALiBS.to.o() --> ALiBS.to.un(t)

// *************************************************************************

- TODO(2013-07-08): URGENTE

	- menu (popdown/combo list) inserir remover campos do grid (visualmente, ou seja alterar T0.tb.sh )
		- listar todos os campos em t0.tb.sh (vem no inicio da lista)
			- adicionar os outros campos disponíveis depois (no final da lista) em ordem alfabética 
			(a00 - [CALCULATED][CURRENCY] Saldo,f00 - [DATA] Nome ,hf00 - [CALCULATED][HTML] Nome, f01 - [DATA] Endereço, etc)
				TODO: TX99 = Estrutura dos campos 
					T0 / a00 = [CALCULATED][CURRENCY] Saldo
					T0 / f00 = [DATA] Nome 
					T0 / hf00 = [CALCULATED][HTML] Nome
					T0 / f01 = [DATA] Endereço

	- places where f00 exists in code (generates problems if rd: uses resultset instead of table)
		l:
		f:
		th:

- TODO(2013-07-08): NÃO URGENTE

	- controle de acessos 

		table: insert
		registros/objetos: select/find  + insert + delete
		campos: read/update
		atributos: read/update/delete

		buttons: disabled/enabled
		menus: read-link/click-link


// *************************************************************************

- LocalStorage

	- OPÇÃO de serializar somente header e/ou dados da tabela (para usar no journaling)
		- REQUIRED: record must have PK = Primary Key
			- on save: save only modified records/fields
			- on load: load records/fields to according PK
				- option to load at the end of the table (probably if PK NOT FOUND)
				- option to modify only fields in loaded record	
					OR to reset data fields in record before loading

	t2l: function(s,p) // Table to localStorage
	- sjn() save journaling
		ty = 0 saves entire table to localStorage
			- should use empty filter?
		ty = 1 saves filtered table to localStorage
			- should use filter instead?   "((f00>=1)&&(f01==abcd))"

		- creates a pair "name=value" to be used in localStorage or other
			user.app.T0.201401131009080001 = "xxxxxxx" (encrypted,zipped,b64,b0())

	l2t: function(s,p) // localStorage to Table 
	- ljn() load journaling
		ty = 0 loads entire table from localStorage
		ty = 1 loads filtered table from localStorage

		- loads entire table to a TEMPORATY TABLE
		- then load journaling records to a TEMPORATY TABLE (data from table T0 but loaded to TEMPORARY TABLE)
			user.app.T0.201401131009080001 = "xxxxxxx" (encrypted,zipped,b64,b0())
			user.app.tableRealName.timestamp

		- then updates app table with TEMPORARY TABLE using ALiBS.to.ct() AND k00 OR r00 (primary key)

	- s2t()
		- [OK] return a table object to a TEMPORARY TABLE (NOT dependent of s2t(), could be any table name)


	- t2s()
		- should use filter for records
		- should use filter for fields
		- returns a string with records and fields of filter

	- SHOULD USE t2j() ????
	- SHOULD USE j2t() ????

	- ct()
		- should accept empty fields and update all fields from TEMPORARY to T0
			- do NOT update empty fields "" to fields with data?
				- how to force? write [EMPTY] in field ?
				- or differ from null and ""
					- null = do NOT update
					- "" = update

					if(f00==null)
					{
						// do NOT update
					}
					else // "", 0, other data
					{
						T0.rs[i].f00 = TEMPORARY.rs[j].f00;
					}

- Sync

	TO BE REVISITED

	- mobile: sends modified records to site

	- site: updates records received from mobile
	- site: update sync timestamp (on table tb.syn.ts = this table field has to be created)
		- should have many timestamps controls for many mobile devices ?

	- site: sends modified records to mobile
	- saves data to database

	- mobile: updates records received from site
	- mobile: update sync timestamp (on table tb.syn.ts = this table field has to be created)
	- saves data to localstore

// *************************************************************************

*/
//
// *************************************************************************
/** 
	Class ALiBS.to

		Description: 
			- Dinamyc Table Object Module;

	--------------------------------------------------------------------
*/
// *************************************************************************
//
if(!ALiBS.gv.to) ALiBS.gv.to = {}; // global values
//
if(!ALiBS.gv.to.le) ALiBS.gv.to.le = 0; // local log status
//
ALiBS.gv.to.nn = 0; // x times loaded
//
ALiBS.gv.to.rl = "15.0731.1131";
//
// *************************************************************************
//
ALiBS.gv.to.oc = 0; // sorted function calls
ALiBS.gv.to.ol = 0; // sorted loop compares
//
ALiBS.gv.to.ec = 0; // sequencial function calls
ALiBS.gv.to.el = 0; // sequencial loop compares
//
// *************************************************************************
//
ALiBS.to = // table
{
	t: function(p) // table
	/** 
	Method <reference> <ALiBS.to.t> ALiBS.to.t(p)

		Description: Creates a new <reference> of Dinamyc Table Object <ALiBS.to.t>

		Parameters:
			p = (optional)(JSON) parameters {d:"y",o:"ALiBS.el",le:true,so:print} 

				tb_rf = (string)table var T1 = new ALiBS.to.t({tb_rf:"T1"});
				tb_id = (string)table id (used in grid elements id)

		Observations:
	
		Return Value:
			<reference> <ALiBS.to.t> Dinamyc Table Object <ALiBS.to.t>

		Example:
			var T9 = new ALiBS.to.t({tb_rf:"T9",tb_id:"_t9"}); 

	--------------------------------------------------------------------
	*/
	// p = (optional)(JSON)parameters {} 
	//
	//	tb_ntb = (number)number of table fields (also can be initialized after using t.tb.ntb=4)
	//
	// 	nm_a00 ... nm_a99 = (string) name of field 
	//		valid for ALL fields [a-z][00-99] + [0-9][00-99] + [_][00-99]
	//
	// 	cp_a00 ... cp_a99 = (string) caption of data field
	//		valid for ALL fields [a-z][00-99] + [0-9][00-99] + [_][00-99]
	//
	//	tb_nn_a = (number)number of application fields (also can be initialized after using t.tb.nn.a=4)
	//		valid for ALL fields [a-z][00-99] + [0-9][00-99] + [_][00-99]
	//
	//	tb_tn = (string)table name
	//	tb_cp = (string)table caption
	//	tb_rf = (string)table var T1 = new ALiBS.to.t({tb_rf:"T1"});
	//	tb_id = (string)table id (used in grid elements id)
	//
	// 	tb_t00 ... tb_t99 = (any) value of table field
	// 	tb_nm_t00 ... tb_nm_t99 = (string) name of table field
	// 	tb_cp_t00 ... tb_cp_t99 = (string) caption of table field
	//
	//	rs = (boolean)read field values from parameters 
	//		false = Default 
	//
	//	rsn = (number)max records to create/process 
	//		10*1000 = Default
	//
	//	rs_0_a00 = (any) value of rs[0].a00
	//		valid for ALL fields [a-z][00-99] + [0-9][00-99] + [_][00-99]
	//
	// return table object
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t",le:ALiBS.gv.to.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		// ************************
		//
		z.v_t = {}; // table object
		//
		z.v_t.rs = new Array(); // array of field values
		//
		z.v_t.rsx = {}; // index = record references (see ALiBS-update.js) (T6.rsx.f03["ALiBS.b0"])
			/*	T2.rsx = {}; 
				//
				T2.rsx["a10"] = {};
				T2.rsx.a10 = {};
				//
				for(var i=0;i<T2.rs.length;i++)
				{
					T2.rsx.a10[T2.rs[i].a10] = T2.rs[i]; // set record reference
				}
				//
				T6.rsx.f03[z.v_v1].f04 = z.v_v2; // set field value to record reference = T6.rsx.f03["ALiBS.b0"].f04
			*/
		//
		z.v_t.so = {}; // sort info
		z.v_t.tb = {}; // table control fields
		//
		// ************************
		// Fields Info
		//
		z.v_t.nm = {}; // field names  (T0.nm.f00 = "abcd";)
		z.v_t.mn = {}; // reverse field names = to create, use ALiBS.to.mn(t,p) // update reverse field names (T0.mn.abcd = "f00";)
		z.v_t.cp = {}; // field captions
		z.v_t.tt = {}; // field titles = on mouse over TODO(2013-05-22): should send when serializing? serialize shoud send only data?
		z.v_t.dc = {}; // field description
		z.v_t.dv = {}; // field default value
		z.v_t.ob = {}; // field observation = field title on field description on ALiBS Schema TS0000C01
		z.v_t.cd = {}; // field creation data / version
		//
		z.v_t.ff = {}; // custom functions
		z.v_t.ft = {}; // filter functions
		z.v_t.ax = {}; // alternative index cache (used by ALiBS.hg = Grid) - TODO(2012-08-29): colocar no Stress Test
		//
		// ************************
		//
		z.v_t.ff.to = {}; // to functions // TODO(2015-06-03): should'n it be "db / tp" ?
		//
		z.v_t.ff.to.ocr = function(t,i) // on create record = to be extended
		{
			/*if(ALiBS.hg)
			{
				// TA2010P.ff.hg.af = after load
			}
			//
			if(ALiBS.server)
			{

			}*/
		};
		z.v_t.ff.to.oup = function(t,i){}; // on update record = to be extended
		z.v_t.ff.to.ode = function(t,i){}; // on delete record = to be extended
		//
		z.v_t.ff.to.bcr = function(t,i){}; // before create record = to be extended
		z.v_t.ff.to.bup = function(t,i){}; // before update record = to be extended
		z.v_t.ff.to.bde = function(t,i){}; // before delete record = to be extended
		//
		z.v_t.ff.to.acr = function(t,i){}; // after create record = to be extended
		z.v_t.ff.to.aup = function(t,i){}; // after update record = to be extended
		z.v_t.ff.to.ade = function(t,i){}; // after delete record = to be extended
		//
		z.v_t.ff.to.acf = function(t,i){}; // after change filter
		z.v_t.ff.to.ald = function(t,i){}; // after load data
		//
		// ************************
		//
		z.v_t["new"] = function(p) // get field using reverse name
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.new",le:ALiBS.gv.to.le});
			//
			var len = this.rs.length;
			//
			this.ff.to.bcr(this,len); 
			//
			this.rs[len] = {};
			//
			this.ff.to.acr(this,len);
			//
			return len;
		};
		//
		z.v_t["del"] = function(i0,p) // get field using reverse name
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.del",le:ALiBS.gv.to.le});
			//
			this.ff.to.bde(this,i0); 
			//
			this.rs[i0] = null;
			//
			this.ff.to.ade(this,i0);
			//
			return i0;
		};
		//
		// ************************
		//
		z.v_t["get"] = function(i0,f0,p) // get field using reverse name
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.get",le:ALiBS.gv.to.le});
			//
			return this.rs[i0][this.mn[f0]];
		};
		//
		z.v_t["getf"] = function(i0,f0,p) // get field using field id (f00)
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.getf",le:ALiBS.gv.to.le});
			//
			return this.rs[i0].f0;
		};
		//
		// ************************
		//
		z.v_t["set"] = function(i0,f0v0,p) // set field using reverse name
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.set",le:ALiBS.gv.to.le});
			//
			this.ff.to.bup(this,i);
			//
			for(var i in f0v0)
			{
				this.rs[i0][this.mn[i]] = f0v0[i];
			}
			//
			this.ff.to.aup(this,i);
			//
			return 1;
		};
		//
		z.v_t["setf"] = function(i0,f0v0,p) // set field using field id (f00)
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.setf",le:ALiBS.gv.to.le});
			//
			this.ff.to.bup(this,i);
			//
			for(var i in f0v0)
			{
				this.rs[i0][i] = f0v0[i];
			}
			//
			this.ff.to.aup(this,i);
			//
			return 1;
		};
		//
		// ************************
		//
		z.v_t.ft["fx"] = function(t,i) // "extended" filter = fc AND fm
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.ft.fx",le:ALiBS.gv.to.le});
			//
			var r = t.ft["fc"](t,i);
			//
			if(r!=1)
			{
				r = 0;
			}
			else
			{
				r = t.ft["fm"](t,i);
				//
				if(r!=1)
				{
					r = 0;
				}
			}
			//
			return r;
		};
		//
		// ************************
		//
		z.v_t.ft["fc"] = function(t,i) // "complex" filter
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.ft.fc",le:ALiBS.gv.to.le});
			//
			return ALiBS.to.f1(t,i,t.tb.fc);
		};
		//
		// ************************
		//
		z.v_t.ft["fe"] = function(t,i) // AND "fields equal" filter = (values == item of select)
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.ft.ff",le:ALiBS.gv.to.le});
			//
			var r = 1; // [BEST-PRACTICE]
			//
			for(var n in t.tb.fi) // filter AND for all fields
			{
				if(t.tb.fv[n]>0) // filter ON for this field
				{
					if(t.rs[i][n]!=t.tb.fi[n].split(",")[t.tb.fv[n]])
					{
						r = -1; // AND FAILED = continue
						break;
					}
				}
			}
			//
			return r; // AND OK
		};
		//
		// ************************
		//
		z.v_t.ft["fm"] = function(t,i) // AND "fields match" filter = (values match item of select)
		{
//console.log("fm")
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t.ft.ff",le:ALiBS.gv.to.le});
			//
			var r = 1; // [BEST-PRACTICE]
			//
			for(var n in t.tb.fi) // filter AND for all fields
			{
				if(t.tb.fv[n]>0) // filter ON for this field
				{
					if(typeof(t.rs[i][n])=="number") 
					{
						if(t.tb.fi[n].split(",")[t.tb.fv[n]]=="[EMPTY]") // filter Empty
						{
							r = -1; // FAILED = number is NOT [EMPTY]
							break;
						}
						else if( t.tb.fi[n].split(",")[t.tb.fv[n]]=="[NOT-EMPTY]" )
						{
							//r = 1; OK = number is [NOT-EMPTY]
						}
						else if(t.rs[i][n]!=t.tb.fi[n].split(",")[t.tb.fv[n]])
						{
							r = -1; // AND FAILED = continue
							break;
						}
					}
					else
					{
						if(!t.rs[i][n]) // field Empty
						{
							if(t.tb.fi[n].split(",")[t.tb.fv[n]]!="[EMPTY]") // filter NOT Empty
							{
								r = -1;
								break;
							}
							else if( t.tb.fi[n].split(",")[t.tb.fv[n]]=="[NOT-EMPTY]" )
							{
								//r = -1; // AND FAILED = continue
								//break;
							}
						}
						else if( t.tb.fi[n].split(",")[t.tb.fv[n]]=="[NOT-EMPTY]" )
						{
							//r = -1; // AND FAILED = continue
							//break;
						}
						else if( t.tb.fi[n].split(",")[t.tb.fv[n]]=="[EMPTY]" || !t.rs[i][n].match(new RegExp( t.tb.fi[n].split(",")[t.tb.fv[n]],"")) )
						{
							r = -1; // AND FAILED = continue
							break;
						}
					}
				}
			}
			//
			return r; // AND OK
		};
		//
		// ************************
		//
		z.v_t.tb.fa = ""; // active complex filter DEFAULT=""=No-Filter (used by ALiBS.hg = Grid) - TODO(2012-08-29): colocar no Stress Test
				//fa,fm,fc
		//
		z.v_t.tb.fc = ""; // complex filter string (f00>="abcd") (used by ALiBS.hg = Grid) - TODO(2012-08-29): colocar no Stress Test
		//
		// ************************
		//
		//TODO: ALiBS.hg should be loaded BEFORE ALiBS.to
		//
		if(ALiBS.hg) // TODO(2012-09-07): only if ALiBS.hg is going to be used 
		{
			z.v_t.tb.hg = {}; // hg controls (used by ALiBS.hg = Grid) - TODO(2012-08-28): colocar no Stress Test
			z.v_t.tb.hg.ty = {}; // type of fields (checkbox, image) - TODO(2012-08-28): to be created = colocar no Stress Test
				// z.v_t.tb.hg.ty.f00 = {ty:1000} = simple checkbox
			//
			z.v_t.tb.hg.tt = null; // DEFAULT = null = NOT SET = field title control = (see also ALiBS.gv.hg.tt)
			//
			// ****************
			//
			z.v_t.tb.hg.pg = {}; // paging controls (used by ALiBS.hg = Grid) - TODO(2012-08-28): colocar no Stress Test
			z.v_t.tb.hg.pg.c = null; // cache of positions first and last  (used by ALiBS.hg = Grid) - TODO(2012-08-28): colocar no Stress Test
			z.v_t.tb.hg.pg.p = -1; // -1 DO NOT CHANGE = paging = current position/page in cache
			z.v_t.tb.hg.pg.t = 0; // number of filtered records to show
			//
			z.v_t.tb.hg.pg.n = 13; // paging = number of records to show (0 = show all)(0 = paging disabled/off)(Default = 13 records per page)
			//
			z.v_t.tb.hg.pg.css = {}; // css atributes control
			z.v_t.tb.hg.pg.css.fs = "1em"; // old = "13px"; // <table> css font size
			z.v_t.tb.hg.pg.css.fa = "Arial"; // <table> css font family
			z.v_t.tb.hg.pg.css.fy = "normal"; // <table> css font style
			z.v_t.tb.hg.pg.css.fw = "normal"; // <table> css font weight
			z.v_t.tb.hg.pg.css.sz = "131em"; // old = "1300px"; // <table> width
			//
			z.v_t.tb.hg.pg.css.hx = 0; // header hx style ON/OFF (0=OFF=NOT CHANGE, 1=ON=CHANGE)
			//
			// ****************
			//
			z.v_t.tb.hg.pg.hx; // Grid Control for Name / Text (used by ALiBS.hg = Grid) - TODO(2012-03-21): colocar no Stress Test
			z.v_t.tb.hg.pg.sh = "" ; // (Array) fields to show (used by ALiBS.hg = Grid) - TODO(2012-08-28): colocar no Stress Test
			z.v_t.tb.hg.pg.fk = {}; // fields with HTML for controls (used by ALiBS.hg = Grid) - TODO(2012-08-28): colocar no Stress Test
			z.v_t.tb.hg.pg.fi = {}; // "field filter" <select> options for each field (used by ALiBS.hg = Grid) - TODO(2012-08-28): colocar no Stress Test
					// ["f00"] = "Todos,0,1,2,3,4,5,6,7,11,12,13,14,15";
			z.v_t.tb.hg.pg.fv = {}; // "field filter" <select> selectedIndex for each field (f00,f01,etc) - TODO(2010-09-08): colocar no Stress Test
					// ["f00"] = 1; (selectedIndex)
					// ["cf"] = 0; (selectedIndex of "complex filter <select>")
			//
			z.v_t.tb.hg.pg.nso = ""; // fields in grid NOT to be sorted - TODO(2013-03-22): colocar no Stress Test
			//
			// ****************
			//
			z.v_t.ff.hg = {}; // hg grid functions
			//
			//z.v_t.ff.hg.dg = function(t,n,p) // display GRID
			//z.v_t.ff.hg.af = function(t) // after load
			//z.v_t.ff.hg.ff0 = function(t,n) // ff0 = calcular saldo
			//
			//z.v_t.ff.hg._hgfc_f0 = function(t,f,v) // when filter changes (field filter)
			//z.v_t.ff.hg._cfck_f0 = function(t,v,i) // when filter changes (complex filter)
			//
			//z.v_t.ff.hg.rc = function(t) // record create = from table info (Used in TS0000c01)
			//
			//
			// ****************
			//
			z.v_t.tb.pg = z.v_t.tb.hg.pg; // OLD Compatibility - replaced "tb.pg" by "tb.hg.pg"
			//
			z.v_t.tb.hx = z.v_t.tb.hg.pg.hx; // OLD Compatibility - added "hg.pg"
			z.v_t.tb.sh = z.v_t.tb.hg.pg.sh; // OLD Compatibility - added "hg.pg"
			z.v_t.tb.fk = z.v_t.tb.hg.pg.fk; // OLD Compatibility - added "hg.pg"
			z.v_t.tb.fi = z.v_t.tb.hg.pg.fi; // OLD Compatibility - added "hg.pg"
			z.v_t.tb.fv = z.v_t.tb.hg.pg.fv; // OLD Compatibility - added "hg.pg"
			z.v_t.tb.nso = z.v_t.tb.hg.pg.nso; // OLD Compatibility - added "hg.pg"
			//
			// ****************
		}
		//
		// ************************
		//
		//TODO: ALiBS.hs should be loaded BEFORE ALiBS.to
		//
		if(ALiBS.hs)
		{
			//z.v_t.tb.hs = {}; // hs controls
		}
		//
		// ************************
		//
		z.v_t.so.g = -1; // sorted field number
		z.v_t.so.y = ""; // field type abcdefghijkrx
		z.v_t.so.rv = false; // reverse sorted 
		z.v_t.so.sl = -1; // sorted length = number of records when sorted
		//
		// ************************
		// Table Info
		//
		z.v_t.tb.tn = ""; // table name 
		z.v_t.tb.cp = ""; // table caption 
		z.v_t.tb.rf = ""; // tableCode = var reference = var T1 = new ALiBS.to.t({tb_rf:"T1"});) - (used by ALiBS.hg / ALiBS.hs) - TODO(2010-09-08): colocar no Stress Test
		z.v_t.tb.id = ""; // table id id - (used by ALiBS.hg / ALiBS.hs) - TODO(2013-03-22): colocar no Stress Test
		//
		z.v_t.tb.tg = ""; // table group 
		z.v_t.tb.ty = ""; // table type (f,fg,fgh,fghi,fghij)
		//
		z.v_t.tb.tt = {}; // table title = on mouse over TODO(2013-05-22): should send when serializing? serialize shoud send only data?
		z.v_t.tb.dc = {}; // table description
		z.v_t.tb.ob = {}; // table observation = field title on table description on ALiBS Schema TS0000C00
		//
		z.v_t.tb.ntb = 0;  // number of Table fields 
		//
		z.v_t.tb.nm = {}; // name of Table fields tb.nm[t00]="xpto"
		//
		z.v_t.tb.nn = {}; // number of fields tb.nn[a]=1 
		//
		z.v_t.tb.zz = {}; // temporary variables - should be cleaned up after use
		//
		z.v_t.tb.uv = null; // temp area for update values = var to receive ajax data as text - should be cleaned up after use // TODO(2010-09-08): Stress Test
		//
		z.v_t.tb.us = {}; // update status // TODO(2010-09-08): Stress Test
		//if(!D.to["dados"].tb.us["page0,1"]) D.to["dados"].tb.us["page0,1"] = {};
		//D.to["dados"].tb.us["page0,1"].f00 = 1; // 1=loaded
		//D.to["dados"].tb.us["page0,1"].f01 = "2010-09-01:10:10:10"; // (timestamp/seconds) 
		//
		// ************************
		//
		//
		//t.va = {}; // custom variables TODO(2010-09-08): usar para variaveis, criar somente se for usar?
		//
		// TODO(2010-09-08): como representar quando for enviar os dados da table em join() OR tostring() ????
		//t.rs2 = []; // LDAP: multivalue attributes AND DBASE: child tables 
		// f00 = dn OR primary key
		// f01 = attribute name OR field name
		// f02 = atribute value OR field value 
		//
		//
		//t.vn = table name = TODO(2010-09-08): DEPRECATED in 2010-07-01
		//t.fn = field number = TODO(2010-09-08): DEPRECATED in 2010-07-01
		//t.ld = {}; // cache to control ajax loaded data/modules/pages/options = t.ld["loadedPage0,0"] = true; // TODO(2010-09-08): DEPRECATED in 2010-07-01 
		//t.lu = 0; // last update TODO(2010-09-08): DEPRECATED in 2010-07-01 (template01????)
		//t.uv = null; // TODO(2010-09-08): DEPRECATED in 2010-07-01 
		//t.tp // TODO(2010-09-08): DEPRECATED in 2010-07-01 
		//
/*for(var i in p)
{
console.log("p["+i+"]="+p[i]);
}*/
		//
		if(p) // set optional parameters to table
		{
			z.v_afd0 = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,_"; // all possible fields/letters/chars 
			//
			z.v_afd2 = z.v_afd0.replace(",",""); // abcdefghijklmnopqrstuvwxyz0123456789_
			//
			z.v_afd3 = z.v_afd0.split(","); // ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
							// "0","1","2","3","4","5","6","7","8","9","_"]
			//
			z.v_atb = ["tn","cp","rf","id","ntb"]; // all possible tb
			//
			for(var i=0;i<z.v_atb.length;i++)
			{
				if(p["tb_"+z.v_atb[i]]) z.v_t.tb[z.v_atb[i]] = p["tb_"+z.v_atb[i]]; // z.v_t.tb.id = p.tb_id;
			}
			//
			for(var i=0;i<z.v_afd2.length;i++) // 
			{
				z.v_ch = z.v_afd2.charAt(i);
				//
				if(p["tb_nn_"+z.v_ch]) z.v_t.tb.nn[z.v_ch] = p["tb_nn_"+z.v_ch]; // nn = number of fields - z.v_t.tb.nn[a] = p[tb_nn_a];
				//
				for(var j=0;j<=99;j++) // 
				{
					z.v_fne = z.v_ch+(j>9? j : "0"+j);
					//
					if(p["nm_"+z.v_fne])
					{
						z.v_t.nm[z.v_fne] = p["nm_"+z.v_fne]; // nm=names - z.v_t.nm[a00] = p[nm_a00];
						//
						if(p["cp_"+z.v_fne]) z.v_t.cp[z.v_fne] = p["cp_"+z.v_fne]; // cp=captions - z.v_t.cp[a00] = p[cp_a00];
					}
				}
			}
			//
			for(var j=0;j<=99;j++) // 
			{
				z.v_fne = (j>9? j : "0"+j);
				//
				if(p["tb_t"+z.v_fne])
				{
					z.v_t.tb["t"+z.v_fne] = p["tb_t"+z.v_fne]; // tb_t00 ... tb_t99 = value Table fields - z.v_t.tb[t00] = p[tb_t00];
					//
					if(p["tb_nm_t"+z.v_fne]) z.v_t.tb.nm["t"+z.v_fne] = p["tb_nm_t"+z.v_fne]; // tb_nm_t00 ... tb_nm_t99 = name of Table fields
				}
			}
			//
			// ****************
			//
			if(typeof(p.rs)=="boolean" && p.rs==true)
			{
				for(var i in p) // FOR sem restrição: Não necessita existir f00
				{
					if(i.indexOf("rs_")==0) // rs_0_f00, rs_0,f01, etc
					{
						z.x0 = i.split("_");
						//
//console.log(i+"="+p[i]);
//console.log("z.x0[1]="+z.x0[1]);
//console.log("z.x0[2]="+z.x0[2]);
						//
						if(!z.v_t["rs"][+z.x0[1]])
						{
							z.v_t["rs"][+z.x0[1]] = {};
						}
						//
						z.v_t["rs"][+z.x0[1]][z.x0[2]] = p[i];
					}
				}
/*				//
				z.n = 10*1000; // max records to create // testing with 100 // TODO(2014-04-25): TO IMPLEMENT
				//
				if(typeof(p.rsn)=="number" && p.rsn>0) z.n = p.rsn; // extend max records to create
				//
				z.a = z.v_afd3;
				//
				z.l = z.a.length;
				//
				// Final Result === z.v_t.rs[0].f00 = p.rs_0_f00
				//
				for(var k=0;k<z.l;k++) // a,b,c,d,e,f,g,h,i,j,k,r,x
				{
					if(typeof(p[("rs_0_"+z.a[k]+"00")])!="undefined") // TODO(2013-07-24): Dependent of [a-z]00 - field with "00"
					{
						for(var j=0;j<z.n;j++) // records
						{
							if(typeof(p[("rs_"+j+"_"+z.a[k]+"00")])!="undefined")
							{
								for(var i=0;i<=99;i++) // fields 
								{
									z.v_oo = "0"+i;
									z.v_oo = z.v_oo.substr(z.v_oo.length-2,2); 
									//
									if(typeof(p[("rs_"+j+"_"+z.a[k]+z.v_oo)])!="undefined")
									{
										if(!z.v_t["rs"][j])
										{
											z.v_t["rs"][j] = {};
										}
										//
										z.v_t["rs"][j][(z.a[k]+z.v_oo)] = p[("rs_"+j+"_"+z.a[k]+z.v_oo)];
									}
								}
							}
						}
					}
				}
*/
			}
		}
		//
		var r = z.v_t; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; // table
	},
	// *****************************************************************
	s2t: function(s,p) // String To Table = Table Deserialize
	/** 
	Method <static> <ALiBS.to.t> ALiBS.to.s2t(s,p)

		Description: String to Table = Table Deserialize

		Parameters:
			s = (required)(string)formated <ALiBS.to.s2t> string to be deserialized
			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object

		Example:
			var x = ALiBS.to.s2t(z.ret);

	--------------------------------------------------------------------
	*/
	/*
		Input:
			-R_c-rs_0_f00:1
			-R_c-rs_0_f01:"1"
			-R_c-rs_0_f02:"2400-12-31.99"
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.s2t",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		z.v_sp = s.substr(0,5); // [AUTO] -R_c- OR -R_i- OR -T_b- OR any other
//console.log("z.v_sp="+z.v_sp)
		z.v_ll; // lines
		z.v_pa = new Array(); // pair
		z.v_ob = {}; // object
		z.ix = 0;
		//
		z.v_ll = s.split(z.v_sp); // -R_c-
		//
		z.v_le = z.v_ll.length;
		//
		for(var i=0;i<z.v_le;i++)
		{
			ALiBS.sy.la(7+0,"z.v_ll["+i+"]="+z.v_ll[i],{o:"ALiBS.to.s2t",le:ALiBS.gv.to.le});
			//
			z.ix = z.v_ll[i].indexOf(":");
			//
			z.v_pa[0] = z.v_ll[i].substring(0,z.ix);
			z.v_pa[1] = z.v_ll[i].substring(z.ix+1,z.v_ll[i].length);
			//
//console.log(z.v_pa[0]+"="+z.v_pa[1]);
			//
			ALiBS.sy.la(7+0,"z.v_pa[0]="+z.v_pa[0],{o:"ALiBS.to.s2t",le:ALiBS.gv.to.le});
			ALiBS.sy.la(7+0,"z.v_pa[1]="+z.v_pa[1],{o:"ALiBS.to.s2t",le:ALiBS.gv.to.le});
			//
			if(z.v_pa[1]==null)
			{
				z.v_ob[z.v_pa[0]] = null;
			}
			else if(z.v_pa[1]=="" || z.v_pa[1]=='""' || z.v_pa[1]=="''")
			{
				z.v_ob[z.v_pa[0]] = "";
			}
			else if(z.v_pa[1].toLowerCase()=="true")
			{
				z.v_ob[z.v_pa[0]] = true;
			}
			else if(z.v_pa[1].toLowerCase()=="false")
			{
				z.v_ob[z.v_pa[0]] = false;
			}
			//else if(ALiBS.sg.is(z.v_pa[1])) // isSpecial
			else if(z.v_pa[1].match(/^[0-9-+]/)) // isDigit
			{
//console.log("passou is");
				z.v_ob[z.v_pa[0]] = parseInt(z.v_pa[1],10);
			}
			else // string
			{
//console.log("passou string");
				if(z.v_pa[1].charAt(0)=='"')
				{
					z.v_ob[z.v_pa[0]] = z.v_pa[1].substring(1,z.v_pa[1].length-1);
				}
				else if(z.v_pa[1].charAt(0)=="'")
				{
					z.v_ob[z.v_pa[0]] = z.v_pa[1].substring(1,z.v_pa[1].length-1);
				}
				else
				{
					z.v_ob[z.v_pa[0]] = z.v_pa[1]; //z.v_pa[1].substring(1,z.v_pa[1].length-1);
				}
//console.log("z.v_ob["+z.v_pa[0]+"]="+z.v_ob[z.v_pa[0]]);
			}
		}
		//
//console.log("z.v_ob="+z.v_ob);
//x0 = z.v_ob;
		//
		var r = ALiBS.to.t(z.v_ob); // [BEST-PRACTICE] 
		//
//console.log("r.rs.length="+r.rs.length);
		ALiBS.to.un(r);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	}, 
	// *****************************************************************
	t2s: function(t,p) // Table to String = Table Serialize
	/** 
	Method <static> <ALiBS.to.t2s> ALiBS.to.t2s(t,p)

		Description: To String = Table to String = Table Serialize

		Parameters:
			t = (required)(ALiBS.to)table object (ALiBS.to.t)
			p = (optional)(JSON)parameter {}
				sp = (number)separator
					0 = DEFAULT = -R_c- = records inside a table (to [r]eplace $pI pE$)
					1 = -T_b- = tables inside a string with many/multiple tables TODO:(do NOT use -T_b-)
					2 = TODO(2014-10-23) -R_i- table inside field (do NOT use -T_b-)

				rn = (number)max records to create/process - Default = t.rs.length

				fc = (string)complex filter
					* = DEFAULT

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t2s> table serialized (string)

		Example:
			var x = ALiBS.to.t2s(z.ret);

	--------------------------------------------------------------------
	*/
	/*
		Output:
			-R_c-rs_0_f00:1
			-R_c-rs_0_f01:"1"
			-R_c-rs_0_f02:"2400-12-31.99"
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t2s",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.rn)!="number") z.rn = t.rs.length; // Default = t.rs.length
		//
		if(typeof(z.sp)!="number") z.sp = "-R_c-"; // DEFAULT 
		else if(z.sp==1) z.sp = "-T_b-"; // 
		else z.sp = "-R_c-"; // DEFAULT 
		//
		if(typeof(z.fc)!="string")
		{
			//t.tb.fc = "(f00:=*)"; // DEFAULT = *
			z.v_ALL = 1; // faster
		}
		else
		{
			t.tb.fc = z.fc; 
		}
		//
		ALiBS.sy.la(5+0,"t.tb.fc="+t.tb.fc,{o:"ALiBS.to.t2s",le:ALiBS.gv.to.le});
		//
		//
		//z.v_s0 = ["|",";",",","/","?","!","@","%","*","(",")","{","}","[","]"]; // do NOT use "&" ":" "_" "$" "=" "+" "-" "#"
		//
		z.v_rnc = 0; // count for z.rn = processed records
		z.v_a0 = new Array();
		z.v_a1 = new Array();
		//
		ALiBS.to.un(t);
		//
		z.v_a0[z.v_a0.length] = 'rs:true'; // to create records in rs[]
		//
		z.v_a0[z.v_a0.length] = 'tb_tn:"' + t.tb.tn + '"'; // table name 
		z.v_a0[z.v_a0.length] = 'tb_cp:"' + t.tb.cp + '"'; // table caption 
		z.v_a0[z.v_a0.length] = 'tb_rf:"' + t.tb.rf + '"'; // table var  
		z.v_a0[z.v_a0.length] = 'tb_id:"' + t.tb.id + '"'; // table id 
		//
		if(t.tb.ntb) z.v_a0[z.v_a0.length] = 'tb_ntb:' + t.tb.ntb; // number of table fields
		//
		// ************************
		//
		if(t.tb.nn.a) z.v_a0[z.v_a0.length] = 'tb_nn_a:' + t.tb.nn.a; // number of application fields 
		if(t.tb.nn.b) z.v_a0[z.v_a0.length] = 'tb_nn_b:' + t.tb.nn.b; // number of application fields 
		if(t.tb.nn.c) z.v_a0[z.v_a0.length] = 'tb_nn_c:' + t.tb.nn.c; // number of application fields 
		if(t.tb.nn.d) z.v_a0[z.v_a0.length] = 'tb_nn_d:' + t.tb.nn.d; // number of application fields 
		if(t.tb.nn.e) z.v_a0[z.v_a0.length] = 'tb_nn_e:' + t.tb.nn.e; // number of application fields 
		//
		if(t.tb.nn.f) z.v_a0[z.v_a0.length] = 'tb_nn_f:' + t.tb.nn.f; // number of data fields 
		if(t.tb.nn.g) z.v_a0[z.v_a0.length] = 'tb_nn_g:' + t.tb.nn.g; // number of application fields 
		if(t.tb.nn.h) z.v_a0[z.v_a0.length] = 'tb_nn_h:' + t.tb.nn.h; // number of application fields 
		if(t.tb.nn.i) z.v_a0[z.v_a0.length] = 'tb_nn_i:' + t.tb.nn.i; // number of application fields 
		if(t.tb.nn.j) z.v_a0[z.v_a0.length] = 'tb_nn_j:' + t.tb.nn.j; // number of application fields 
		//
		if(t.tb.nn.k) z.v_a0[z.v_a0.length] = 'tb_nn_k:' + t.tb.nn.k; // number of control fields 
		if(t.tb.nn.r) z.v_a0[z.v_a0.length] = 'tb_nn_r:' + t.tb.nn.r; // number of reserved fields 
		if(t.tb.nn.x) z.v_a0[z.v_a0.length] = 'tb_nn_x:' + t.tb.nn.x; // number of index fields 
		//
		// ************************
		//
		z.v_a1[z.v_a1.length] = new Array("t","tb_t","tb"); // tb_t00 ... tb_t99
		//
		z.v_a1[z.v_a1.length] = new Array("t","nm_t","nm"); // nm_t00 ... nm_t99 = name of Table fields
		//
		z.v_a1[z.v_a1.length] = new Array("a","nm_a","nm");
		z.v_a1[z.v_a1.length] = new Array("b","nm_b","nm");
		z.v_a1[z.v_a1.length] = new Array("c","nm_c","nm");
		z.v_a1[z.v_a1.length] = new Array("d","nm_d","nm");
		z.v_a1[z.v_a1.length] = new Array("e","nm_e","nm");
		//
		z.v_a1[z.v_a1.length] = new Array("f","nm_f","nm"); // nm_f00 ... nm_f99
		z.v_a1[z.v_a1.length] = new Array("g","nm_g","nm");
		z.v_a1[z.v_a1.length] = new Array("h","nm_h","nm");
		z.v_a1[z.v_a1.length] = new Array("i","nm_i","nm");
		z.v_a1[z.v_a1.length] = new Array("j","nm_j","nm");
		//
		z.v_a1[z.v_a1.length] = new Array("k","nm_k","nm");
		z.v_a1[z.v_a1.length] = new Array("r","nm_r","nm");
		z.v_a1[z.v_a1.length] = new Array("x","nm_x","nm");
		//
		z.v_a1[z.v_a1.length] = new Array("t","cp_t","cp"); // cp_t00 ... cp_t99 = caption of Table fields
		//
		z.v_a1[z.v_a1.length] = new Array("a","cp_a","cp");
		z.v_a1[z.v_a1.length] = new Array("b","cp_b","cp");
		z.v_a1[z.v_a1.length] = new Array("c","cp_c","cp");
		z.v_a1[z.v_a1.length] = new Array("d","cp_d","cp");
		z.v_a1[z.v_a1.length] = new Array("e","cp_e","cp");
		//
		z.v_a1[z.v_a1.length] = new Array("f","cp_f","cp"); // cp_f00 ... cp_f99
		z.v_a1[z.v_a1.length] = new Array("g","cp_g","cp"); // cp_f00 ... cp_f99
		z.v_a1[z.v_a1.length] = new Array("h","cp_h","cp"); // cp_f00 ... cp_f99
		z.v_a1[z.v_a1.length] = new Array("i","cp_i","cp"); // cp_f00 ... cp_f99
		z.v_a1[z.v_a1.length] = new Array("j","cp_j","cp"); // cp_f00 ... cp_f99
		//
		z.v_a1[z.v_a1.length] = new Array("k","cp_k","cp");
		z.v_a1[z.v_a1.length] = new Array("r","cp_r","cp");
		z.v_a1[z.v_a1.length] = new Array("x","cp_x","cp");
		//
		// ************************
		//
		z.v_l0 = z.v_a1.length;
		//
		for(var k=0;k<z.v_l0;k++) // length of Array z.v_a1
		{
			for(var i=0;i<=99;i++) // fields r.f00,r.f01,etc
			{
				z.v_oo = "0"+i;
				z.v_oo = z.v_oo.substr(z.v_oo.length-2,2); 
				//
				if(typeof(t[z.v_a1[k][2]][z.v_a1[k][0]+z.v_oo])!="undefined")
				{
					z.v_a0[z.v_a0.length] = z.v_a1[k][1]+z.v_oo+':"'+t[z.v_a1[k][2]][z.v_a1[k][0]+z.v_oo]+'"'; // tb_t00 = t.tb.t00
				}
			}
		}
		//
		// ************************
		//
		if(t.rs && t.rs[0])
		{
			z.v_a1 = ["a","b","c","d","e","f","g","h","i","j","k","r","x"];
			//
			z.v_l0 = z.v_a1.length;
			//
			// Final Result === t.rs[0].f00 = p.rs_0_f00
			//
			z.v_rn2 = t.rs.length; // table record number
			//
			for(var j=0;j<z.v_rn2;j++) // records
			{
				z.v_ok = z.v_ALL || t.ft.fc(t,j); // "complex" filter
				//
				if(z.v_ok) // record OK
				{
					for(var k=0;k<z.v_l0;k++) // f,g,h,i
					{
						for(var i=0;i<=99;i++) // fields 
						{
							z.v_oo = "0"+i;
							z.v_oo = z.v_oo.substr(z.v_oo.length-2,2); 
							//
							z.v_v0 = t["rs"][j][z.v_a1[k]+z.v_oo];
							z.v_v1 = typeof(z.v_v0);
							//

							if(z.v_v1!="undefined")
							{
								if(z.v_v1=="number" || z.v_v1=="boolean")
								{
									z.v_a0[z.v_a0.length] = "rs_"+z.v_rnc+"_"+z.v_a1[k]+z.v_oo+':' + t["rs"][j][z.v_a1[k]+z.v_oo] + ''; // rs_0_f00:"t.rs[0].f00"
									//z.v_a0[z.v_a0.length] = "rs_"+j+"_"+z.v_a1[k]+z.v_oo+':' + t["rs"][j][z.v_a1[k]+z.v_oo] + ''; // rs_0_f00:"t.rs[0].f00"
								}
								else // string
								{
									z.v_a0[z.v_a0.length] = "rs_"+z.v_rnc+"_"+z.v_a1[k]+z.v_oo+':"' + t["rs"][j][z.v_a1[k]+z.v_oo] + '"'; // rs_0_f00:"t.rs[0].f00"
									//z.v_a0[z.v_a0.length] = "rs_"+j+"_"+z.v_a1[k]+z.v_oo+':"' + t["rs"][j][z.v_a1[k]+z.v_oo] + '"'; // rs_0_f00:"t.rs[0].f00"
								}
							}


	/*						if(typeof(t["rs"][j][z.v_a1[k]+z.v_oo])!="undefined")
							{
								z.v_a0[z.v_a0.length] = "rs_"+j+"_"+z.v_a1[k]+z.v_oo+':"' + t["rs"][j][z.v_a1[k]+z.v_oo] + '"'; // rs_0_f00:"t.rs[0].f00"
							}
	*/
						}
					}
					//
					z.v_rnc++; // records processed
					//
					if(z.v_rnc>=z.rn)
					{
						break;
					}
				}
			}
		}
		//
		// ************************
		//
		var r = z.sp + z.v_a0.join(z.sp); // (string) // [BEST-PRACTICE]
		//
/*
		var r = z.v_a0.join(z.sp); // (string) // [BEST-PRACTICE]
		//
		for(var i=0;i<z.v_s0.length;i++)
		{
			if(r.indexOf(z.v_s0[i])<0) // Not Found = Good to be a separator
			{
				z.sp = z.v_s0[i];
				break;
			}			
		}
		//
		if(z.sp!="$pI pE$") // New separator Found
		{
			r = z.sp + r.replace(/\$pI pE\$/gi,z.sp); // add separator as first byte
		}
		else // using "$pI pE$" as separator = DEFAULT case all letters exist in text
		{
			r = "$pI pE$" + r; // $ as first byte = means "$pI pE$" 
		}
		//
*/
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	}, 
	// *****************************************************************
	c2t: function(t,tx,p) // CSV to Table - (,;|) OR -R_c- -F_d-
	/** 
	Method <static> <ALiBS.to.t> ALiBS.to.c2t(s,p)

		Description: CSV to Table 

		Parameters:
			t = (required)(ALiBS.to)table object (ALiBS.to.t)
			tx = (required)(string)CSV text
			p = (optional)(JSON)parameter {}
				fsp = (string)field separator ( ","    ";"    "-F_d-" )
					"," = DEFAULT 
				rsp = (string)record separator ( "\n"    "-R_c-" )
					"\n" = DEFAULT

				ff0 = (function)field names resolution

		Observations:
	
		Return Value:
			NONE

		Example:
			var x = ALiBS.to.c2t(t,csv);

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.fsp)!="string") z.fsp = ","; // DEFAULT ","
			if(typeof(z.rsp)!="string") z.rsp = "\n"; // DEFAULT ","
			//
			if(ALiBS.sy.ty(z.ff0)!="func"+"tion") z.ff0 = null; // function ff0
			//
			ALiBS.sy.la(5+0,"z.fsp="+z.fsp,{o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.rsp="+z.rsp,{o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
			//
			// ************
			//
			z.ec = -1000; // 
			z.v_ta = tx.split(z.rsp); // records
			//
//for(var i=0;i<z.v_ta.length;i++) // records
//{ console.log("["+i+"]= "+z.v_ta[i]); }
			//
			// ************
			//
			z.ec = -1100; // 
			t.rs = new Array();
			//
			z.ec = -1200; // 
			for(var j=1;j<z.v_ta.length;j++) // records
			{
				if(z.v_ta[j])
				{
//console.log("z.v_ta["+(j-1)+"]="+z.v_ta[j-1])
					//
					z.v_taN = z.v_ta[j].split(z.fsp); // fields
					//
					t.rs[j-1] = {};
					//
					for(var i=0;i<z.v_taN.length;i++)
					{
						if(i<10) z.v_ii = "0"+i;
						else z.v_ii = ""+i;
						//
						t.rs[j-1]["f"+z.v_ii] = z.v_taN[i];
					}
				}
			}
			//
//for(var j=0;j<t.rs.length;j++) // records
//{ console.log("t.rs["+(j)+"]="+t.rs[j].f03) }
			//
			// ************
			//
			z.ec = -1300; // 
			z.v_ta0 = z.v_ta[0].split(z.fsp); // first line with field names
			//
			ALiBS.sy.la(5+0,"(first-line)z.v_ta0[0]="+z.v_ta0,{ll:1000,o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
			//
			z.ec = -1400; // 
			for(var i=0;i<z.v_ta0.length;i++)
			{
				z.ec = -1500; // 
				if(i<10) z.v_ii = "0"+i;
				else z.v_ii = ""+i;
				//
				ALiBS.sy.la(7+0,"z.v_ii="+z.v_ii,{o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
				//
				z.ec = -1600; // 
				if(z.ff0)
				{
					z.ec = -1700; // 
					z.ff0(t,z.fsp,z.v_ta0[i],i);
					//
					ALiBS.sy.la(7+0,"t.nm[fii]="+t.nm["f"+z.v_ii],{o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
					ALiBS.sy.la(7+0,"t.cp[fii]="+t.cp["f"+z.v_ii],{o:"ALiBS.to.c2t",le:ALiBS.gv.to.le});
				}
				else
				{
					z.ec = -1800; // 
					z.v_nmx = z.v_ta0[i];
					//
					z.ec = -1900; // 
					t.nm["f"+z.v_ii] = z.v_nmx; // field names
					//
					z.ec = -2000; // 
					t.cp["f"+z.v_ii] = z.v_nmx; // field captions
				}
			}
			//
			z.ec = -9999; // 
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.to.c2t",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		r = ""; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	}, 
	// *****************************************************************
	t2c: function(s,p) // Table to CSV - (,;|) OR -R_c- -F_d-
	{
		// TODO(2013-07-29): ja foi feito no projeto Relatorios
		//
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t2c",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	x2t: function(s,p) // Table to XML
	{
		// TODO(2013-07-29):
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.x2t",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	t2x: function(s,p) // XML to Table
	{
		// TODO(2013-07-29):
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t2x",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	j2t: function(s,p) // JS to Table - NATIVO - NAO PRECISA
	{
		// TODO(2013-07-29):
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.j2t",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	t2j: function(s,p) // Table to JS
	/*
		Output:
			T0.rs[0]={f00:"a",f01:2};\n

	*/
	{
		// TODO(2013-07-29):
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t2j",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	l2t: function(s,p) // localStorage to Table 
	{
		// TODO(2013-08-05):
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.l2t",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	t2l: function(s,p) // Table to localStorage
	{
		// zipar ????

		// TODO(2013-08-05):
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.t2l",le:ALiBS.gv.to.le});
		//
		return; 
	},
	// *****************************************************************
	un: function(t,p) // update number of fields (tb_ntb,tb_nn_a,tb_nn_f,tb_nn_k,tb_nn_r,tb_nn_x) based on fieldNames
	/** 
	Method <static> <ALiBS.to.t> ALiBS.to.un(t,p)

		Description: update number of fields 

		Parameters:
			t = (required)(ALiBS.to)table
			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			NONE

		Example:
			ALiBS.to.un(T9);

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.un",le:ALiBS.gv.to.le});
		//
		if(!t || typeof(t.rs)=="undefined")
		{
			//return ALiBS.sy.er(null,"ALiBS.to.o",'t is NOT a table'); 
			//return null;
		}
		else
		{
			var z = p || {}; // [BEST-PRACTICE]
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// local variables [BEST-PRACTICE]
			z.v_s0 = "";
			//
			z.v_ty = "abcdefghijkrxt"; // types 
			z.v_l0 = z.v_ty.length;
			//
			z.v_j0 = 0; // counter
			//
			for(var k=0;k<z.v_l0;k++) // types
			{
				z.v_s0 = z.v_ty.charAt(k); // MSIE do not understand string[i], ONLY string.charAt(i)
				//
				z.v_j0 = 0; // counter
				//
				for(var i=0;i<=99;i++) // fields r.f00,r.f01,etc
				{
					z.v_oo = "0"+i;
					z.v_oo = z.v_oo.substr(z.v_oo.length-2,2); 
					//
					if(typeof(t.nm[(z.v_s0+z.v_oo)])!="undefined") // number of names of fields
					{
						z.v_j0++;
					} 
				}
				//
				if(z.v_s0=="t") // table fields
				{
					t.tb[("ntb")] = z.v_j0; // 
				}
				else
				{
					//t.tb[("n"+z.v_s0)] = z.v_j0; // 
					t.tb.nn[(z.v_s0)] = z.v_j0; // 
				}
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; 
	},
	// *****************************************************************
	fnd: function(fn,p) // field name decode to object
	// fn = (required)(string)field name
	// p = (optional)(JSON)parameter {}
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.fnd",le:ALiBS.gv.to.le});
		//
		//var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//
		var r = {}; // [BEST-PRACTICE]
		//
		r.s = fn.match(/[a-zA-Z]{1,}/g)[0];
		r.n = fn.match(/[0-9]{1,}/g)[0];
		//
		//z = null; // [BEST-PRACTICE]
		//delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	fne: function(s,n,p) // field name encode to string
	// f = (required)(string)field type (a,b,c,d,e,f,g,h,i,j,k,r,x)
	// n = (required)(number)field number (0-99)
	// p = (optional)(JSON)parameter {}
	//	d = (number) number of digits 
	//		2 = DEFAULT
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.fne",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.d)!="number") z.d = 2; // DEFAULT 
		//
		// local variables [BEST-PRACTICE]
		z.v_s0 = ("0000000000000"+n);
		//
		var r = s + z.v_s0.substring(z.v_s0.length-z.d,z.v_s0.length); // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	gn: function(f,n) // get field name = OLD = compatibility
	// f = (required)(string)field type (a,b,c,d,e,f,g,h,i,j,k,r,x)
	// n = (required)(number)field number (0-99)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.gn",le:ALiBS.gv.to.le});
		//
		return ALiBS.to.fne(f,n);
		//
/*		var r; // [BEST-PRACTICE]
		//
		if(n<10)
		{
			r = f+"0"+n;
		}
		else
		{
			r = f+""+n;
		}
		//
		return r; 
*/
	},
	// *****************************************************************
	ld0: function(t,a,p) // load data from Array() // TODO(2010-09-08): colocar em stress teste - usado em template1 e nos grids loteria, conta, agenda, etc
	// t = (required)(ALiBS.to)table
	// a = (required)(Array)data array to be loaded
	//	Array(
	//		{f00:"menuP0",f01:"javascript :F.init(0,0)",f02:"Home"},
	//		{f00:"menuP1",f01:"javascript :F.init(1,0)",f02:"Download"}    )
	// p = (optional)(JSON)parameter {r:"page0,0",f00:1}
	//	r = (string)resource loaded name
	//
	//	f00...f99 = see t.tb.us
	//
	// return = <undefined>
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.ld0",le:ALiBS.gv.to.le});
		//
		for(var i in a) // add new records to the end of ResultSet
		{
			t.rs[t.rs.length] = a[i];
		}
		//
		if(p && typeof(p.r)=="string") // set loading controls if needed
		{
			for(var i in p)
			{
				if(!t.tb.us[p.r])
				{
					t.tb.us[p.r] = {};
				}
				//
				t.tb.us[p.r][i] = p[i];
			}
		}
		//
		//ALiBS.to.un(t); // TODO(2010-09-08): em todas a funcoes de load data
		//
		return;
	},
	// *****************************************************************
	o: function(t,g,p) // Sort by Field
	/** 
	Method <static> <ALiBS.to.t> ALiBS.to.o(t,g,p)

		Description: Sort by Field

		Parameters:
			t = (required)(ALiBS.to)table
			g = (required)(number)field number [0-99]
			p = (optional)(JSON)parameter {}
				y = (string)field type [abcdefghijkrx] 
					f = Default
				rv = (boolean)reverse sort
					false = DEFAULT // TODO(2010-09-08): colocar o valor default em todas as descriçoes????
				sh = (boolean)shuffle
					false = DEFAULT

		Observations:
			May return unexpected results when sorting if:
				- Fields are NOT normalized (undefined/null/empty)
				- Fields are MIXED uppercase/lowercase

		Return Value:
			<static> <ALiBS.to.t> table object 

		Example:
			ALiBS.to.o(T9,0);

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.o",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(!t || typeof(t.rs)=="undefined")
		{
			//return ALiBS.sy.er(null,"ALiBS.to.o",'t is NOT a table'); // TODO(2010-09-08): validar parametros de entrada em todos os metodos
			r = null;
		}
		else if(typeof(g)!="number" || g<0 || g>99)
		{
			r = null; 
		}
		else
		{
			if(typeof(z.y)!="string" || "abcdefghijkrx".indexOf(z.y)<0) z.y = "f"; // field DEFAULT = f
			if(typeof(z.rv)!="boolean") z.rv = false; // reverse DEFAULT = false
			if(typeof(z.sh)!="boolean") z.sh = false; // shuffle DEFAULT = false
			//
			// table variables
			if(t.tb.pg) t.tb.pg.c = null; // paging cache should be set [NULL] every time filter changes
			t.so.g = g; // sorted field number
			t.so.y = z.y; 
			t.so.sl = t.rs.length; // number of records
			t.so.rv = z.rv;
			//
			// local variables [BEST-PRACTICE]
			z.v_oo = "0"+g;
			z.v_oo = z.v_oo.substr(z.v_oo.length-2,2); 
			//
			ALiBS.to.un(t); 
			// 
			if(z.sh) // shuffle
			{
				z.f0 = function(a,b)
				{
					ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.o.sh.sort",le:ALiBS.gv.to.le});
					//
					return ALiBS.sg.rd(0,2)-1;
				};
				//
				for(var i=0;i<4;i++) // 4x is enough???? TODO:(2013-06-11): passar por parametro
				{
					r = t.rs.sort(z.f0);
				}
			}
			else if(!z.rv) // NOT reverse sort = ascendent sort
			{
				z.f0 = function(a,b)
				{
					ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.o.!rv.sort",le:ALiBS.gv.to.le});
					//
					return a[(z.y+z.v_oo)]<b[(z.y+z.v_oo)]?-1:(a[(z.y+z.v_oo)]>b[(z.y+z.v_oo)]?1:0);
				};
				//
				r = t.rs.sort(z.f0);
			}
			else // reverse
			{
				z.f0 = function(a,b)
				{
					ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.o.rv.sort",le:ALiBS.gv.to.le});
					//
					return a[(z.y+z.v_oo)]<b[(z.y+z.v_oo)]?1:(a[(z.y+z.v_oo)]>b[(z.y+z.v_oo)]?-1:0);
				};
				//
				r = t.rs.sort(z.f0);
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	ci: function(t0,fd,p) // Create compound index from many fields. Always sort by new index
	/** 
	Method <static> <void> ALiBS.to.ci(t,g,p)

		Description: Create compound index from many fields. Always sort by new index.

		Parameters:
			t0 = (required)(ALiBS.to)table
			fd = (required)(string)fields in sort order and field length 
				DEFAULT length = 10
				ex: "f00,f05,f03"
				ex: "f00=13,f03=31"

			p = (optional)(JSON)parameter {}
				ix0 = (string)field name for index
					x00 = DEFAULT

				ix1 = (string)field name for category subtotal
					x01 = DEFAULT

				ix2 = (string)field name for filter (0=counting,1=subtotal)
					x02 = DEFAULT

				it0 = (string)table field name for total
					t00 = DEFAULT

				fw = (string)fill with
					"0" = DEFAULT

				di = (string) direction to fill 
					l = left = DEFAULT
					r = rigth

				rt = (number)type of index
					0 = DEFAULT = 0 ... n (sequencial number)
					1 = real values 

		Observations:
	
		Return Value:
			NONE

		Example:
			ALiBS.to.ci(T9,"f00,f07,f01",{ix:x03});

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		if(!fd)
		{
			//return ALiBS.sy.er(null,"ALiBS.to.o",'t is NOT a table'); 
			//return null;
		}
		else
		{
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.ix0)!="string") z.ix0 = "x00"; // DEFAULT 
			if(typeof(z.ix1)!="string") z.ix1 = "x01"; // DEFAULT 
			if(typeof(z.ix2)!="string") z.ix2 = "x02"; // DEFAULT 
			if(typeof(z.it0)!="string") z.it0 = "t00"; // DEFAULT 
			//
			if(typeof(z.fw)!="string") z.fw = "0"; // DEFAULT = 0
			if(typeof(z.di)!="string") z.di = "l"; // DEFAULT = 0
			if(typeof(z.rt)!="number") z.rt = 0; // DEFAULT = 0
			//
			// local variables [BEST-PRACTICE]
			z.v_fd2 = fd.split(","); // (f00=13,f03=31)
			//
			z.v_fda = new Array();
			z.v_lxa = new Array();
			//
			for(var i=0;i<z.v_fd2.length;i++) // fields in compound index
			{
				z.v_fdx = z.v_fd2[i].indexOf("=");
				//
				ALiBS.sy.la(5+0,"z.v_fdx="+z.v_fdx,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
				//
				if(z.v_fdx>0)
				{
					z.v_fda[i] = z.v_fd2[i].substr(0,z.v_fdx);
					z.v_lxa[i] = z.v_fd2[i].substr(z.v_fdx+1,z.v_fd2[i].length);
					//
					if(z.v_lxa[i]) z.v_lxa[i] = parseInt(z.v_lxa[i],10);
					else z.v_lxa[i] = 10;
				}
				else
				{
					z.v_fda[i] = z.v_fd2[i];
					z.v_lxa[i] = 10;
				}
				//
				ALiBS.sy.la(5+0,"z.v_fda["+i+"]="+z.v_fda[i],{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
				ALiBS.sy.la(5+0,"z.v_lxa["+i+"]="+z.v_lxa[i],{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
			}
			//
			z.v_ci0 = new Array();
			//
			z.v_len = t0.rs.length;
			//
			for(var i=0;i<z.v_len;i++) // records
			{
				for(var j=0;j<z.v_fda.length;j++) // fields in compound index
				{
					z.v_v0 = t0.rs[i][z.v_fda[j]] || ""; // "" if undefined
					//
					ALiBS.sy.la(7+0,"z.v_v0="+z.v_v0,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
					//
					z.v_v1 = ALiBS.sg.fi(""+z.v_v0,z.fw,z.v_lxa[j],{d:z.di}); // z.v_v0 must be string
					//
					ALiBS.sy.la(7+0,"z.v_v1="+z.v_v1,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
					//
					z.v_ci0[z.v_ci0.length] = z.v_v1; // 0000000000 + "abcdefg"
				}
				//
				t0.rs[i][z.ix0] = z.v_ci0.join("-"); // temp index
				//
				ALiBS.sy.la(7+0,"t0.rs["+i+"]["+z.ix0+"]="+t0.rs[i][z.ix0],{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
				//
				z.v_ci0.length = 0; // reset Array to create new record/index
			}
			//
			ALiBS.to.o(t0,parseInt(z.ix0.charAt(1)+z.ix0.charAt(2),10),{y:z.ix0.charAt(0)});
			//
			if(z.rt==0) // index = 0 ... n
			{
				z.v_len2 = (""+z.v_len).length;
				//
				for(var i=0;i<z.v_len;i++) // records
				{
					t0.rs[i][z.ix0] = ALiBS.sg.fi(""+i,"0",z.v_len2);
				}
			}
			else if(z.rt==1) // index = real values
			{
				// do nothing
			}
			else if(z.rt==31) // sum up by category - index = real values
			{
				if(t0.rs.length==0)
				{
					//t0.rs[i][z.ix1] = z.v_sb; // to NOT exist [EMPTY/UNDEFINED/NULL] ix1
					//
					//t0.rs[i][z.ix2] = "0"; // counting (to use as filter)
					//
					t0.tb[z.it0] = 0;
				}
				if(t0.rs.length==1)
				{
					t0.rs[i][z.ix1] = 1; // to NOT exist [EMPTY/UNDEFINED/NULL] ix1
					//
					t0.rs[i][z.ix2] = "1"; // subtotal (to use as filter)
					//
					t0.tb[z.it0] = 1;
				}
				else
				{
					z.v_sb = 0; // subtotal (init=1)(for tables rs.length==1)
					z.v_tt = 0; // total
					//
					for(var i=0;i<t0.rs.length;i++)
					{
						z.v_sb++; 
						//
						t0.rs[i][z.ix1] = z.v_sb; // count
						//
						if((i+1)<t0.rs.length && t0.rs[i][z.ix0]==t0.rs[i+1][z.ix0]) // next exists
						{
							t0.rs[i][z.ix2] = "0"; // counting (to use as filter)
						}
						else // next do NOT exist OR  current record/field != last record/field
						{
							t0.rs[i][z.ix2] = "1"; // subtotal (to use as filter)
							//
							z.v_sb = 0; 
						}
						//
						z.v_tt++;
					}
					//
					t0.tb[z.it0] = z.v_tt;
				}
			}
			//
			ALiBS.sy.la(5+0,"fd="+fd,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_fd2="+z.v_fd2,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_fda="+z.v_fda,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_lxa="+z.v_lxa,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.ix="+z.ix,{o:"ALiBS.to.ci",le:ALiBS.gv.to.le});
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; 
	},
	// *****************************************************************
	mn: function(t,p) // update reverse field names
	/** 
	Method <static> <ALiBS.to.t> ALiBS.to.mn(t,p)

		Description: update reverse field names

		Parameters:
			t = (required)(ALiBS.to)table
			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			NONE

		Example:
			ALiBS.to.mn(T9);

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.mn",le:ALiBS.gv.to.le});
		//
		if(!t || typeof(t.rs)=="undefined")
		{
			//return ALiBS.sy.er(null,"ALiBS.to.o",'t is NOT a table'); 
			//return null;
		}
		else
		{
			var z = p || {}; // [BEST-PRACTICE]
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// local variables [BEST-PRACTICE]
			//
			for(var i in t.nm) // types
			{
				t.mn[t.nm[i]] = i; // reverse field names
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; 
	},
	// *****************************************************************
	l: function(t,p) // length of Table OR resultSet OR Array
	// t = (required)(table||table.rs||Array||String)ALiBS.to||ALiBS.to.rs||Array||String // TODO(2010-09-08):atualizar esta descricao em todas funcoes que se aplicam
	// p = (optional)(JSON)parameters {} 
	//	se = (string)separator
	// return length
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.l",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.se)!="string") z.se = ""; // DEFAULT 
		//
		var r = 0; // [BEST-PRACTICE]
		//
		if(ALiBS.sy.ty(t)=="object-ALiBS.to")
		//if(typeof(t.rs)!="undefined" && typeof(t.rs[0])!="undefined" && typeof(t.rs[0]. f00)!="undefined") // table TODO(2014-04-25): USAR ALiBS.sy.ty(a)
		{
			r = t.rs.length;
			//
			if(t.so.sl!=r && t.so.g>-1) ALiBS.to.o(t,t.so.g,{y:t.so.y,rv:t.so.rv}); // table has new records and table is sorted
		}
		else if(typeof(t[0])!="undefined" && typeof(t[0].f00)!="undefined") // resultSet
		{
			r = t.length; 
		}
		else if(typeof(t)=="string") // String
		{
			if(z.se)
			{
				r = t.split(z.se).length; 
			}
			else
			{
				r = t.length; 
			}
		}
		else if(typeof(t)=="obj"+"ect") // Array
		{
			r = t.length; // Array OR String
		}
		else 
		{
			//return ALiBS.sy.er(-1,"ALiBS.to.l",'t is NOT a table/table.rs/Array/String'); // TODO(2010-09-08): rever retornos de erro em todos metodos
			r = -1;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	gv: function(t,i,f,n,p) // get field value
	// t = (required)(ALiBS.to)table
	// i = (required)(number)record number/position
	// f = (required)(string)field type (a,b,c,d,e,f,g,h,i,j,k,r,x)
	// n = (required)(number)field number (0-99)
	// p = (optional)(JSON)parameters {} 
	//	uv = (any type)value to return if r==undefined
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.gv",le:ALiBS.gv.to.le});
		//
		var r = t.rs[i][ALiBS.to.gn(f,n)];
		//
		if(typeof(r)!="undefined")
		{
			//r = r;
		}
		else 
		{
			r = p.uv;
		}
		//
		return r; 
	},
	// *****************************************************************
	f: function(t,i,p) // get field value
	// t = (required)(table||table.rs||Array||String)ALiBS.to||ALiBS.to.rs||Array||String 
	// i = (required)(number)record 
	// p = (optional)(JSON)parameters {y:"f",g:0} 
	//	y = (string)field type [abcdefghijkrx] (default=f) 
	// 	g = (number)field number [0-99] (default=0) // TODO(2010-09-08): colocar o valor default em todas as descriçoes de optional
	//	se = (string)separator
	//
	// return = field value
	//	null = NOT a valid object
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.f",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.g)!="number" || z.g<0 || z.g>99) z.g = 0; // DEFAULT field
		if(typeof(z.y)!="string" || "abcdefghijkrx".indexOf(z.y)<0) z.y = "f"; // DEFAULT field
		if(typeof(z.se)!="string") z.se = "";
		//
		// local variables [BEST-PRACTICE]
		z.v_oo = "0"+z.g;
		z.v_oo = z.v_oo.substr(z.v_oo.length-2,2); 
		//
		var r; // [BEST-PRACTICE]
		//
		if(ALiBS.sy.ty(t)=="object-ALiBS.to")
		//if(typeof(t.rs)!="undefined" && typeof(t.rs[0])!="undefined" && typeof(t.rs[0]. f00)!="undefined") // table TODO(2014-04-25): USAR ALiBS.sy.ty(a)
		{
			r = t.rs[i][(z.y+z.v_oo)];
		}
		else if(typeof(t[0])!="undefined" && typeof(t[0].f00)!="undefined") // resultSet
		{
			r = t[i][(z.y+z.v_oo)];
		}
		else if(typeof(t)=="string") // String
		{
			if(z.se)
			{
				r = (t.split(z.se))[i]; 
			}
			else
			{
				r = t.charAt(i); 
			}
		}
		else if(typeof(t)=="obj"+"ect") // Array
		{
			r = t[i]; 
		}
		else
		{
			//return ALiBS.sy.er(null,"ALiBS.to.f",'t is NOT a table/table.rs/Array/String'); // TODO(2010-09-08): rever retornos de erro em todos metodos
			r = null;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	of: function(a,e,p) // Sorted Find - search on SORTED field - find primary key OR first value, if value is duplicated 
	// TODO(2010-09-08): mudar "a" para "t" em todos os metodos que se aplicam
	// TODO(2012-08-13): of deveria respeitar/tratar filtro ????
	// a = (required)(Array||table||table.rs)ALiBS.to OR Array OR ResultSet - FIELD MUST BE SORTED (increasing sort, not reverse/decreasing)
	// e = (required)(Array element)element value to be found
	// p = (optional)(JSON)parameters {} 
	//
	// return = (number)element index [i]
	//	-1 = element under min
	//	-3 = element over max
	//	-2 = element not found
	//	-4 = Array.length = 0
	//	-5 = t is NOT a table/table.rs/Array/String
	//	-6 = table is NOT sorted
	/*
		if "a" is a table (ALiBS.to) then search runs on sorted field
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.of",le:ALiBS.gv.to.le});
		//
		ALiBS.gv.to.oc++;
		//
		var z = {}; // [BEST-PRACTICE]
		//
		if(p) // Old version had p.g, p.y
		{
			z.p = p; // should have no more in stress-test and any other place
//console.log("1-COMPATIBILITY-parameter-p-present"); 
		}
		else
		{
			if(ALiBS.sy.ty(a)=="object-ALiBS.to")
			//if(typeof(a.rs)!="undefined" && typeof(a.rs[0])!="undefined" && typeof(a.rs[0]. f00)!="undefined") // table TODO(2014-04-25): USAR ALiBS.sy.ty(a)
			{
				if(a.so.g==-1) // table is NOT sorted - it's sorted by insert order - may NOT work as expected
				{
//console.log("2-TABLE-NOT-sorted");
					// no information provided to sort
					z.p = null; // p is undefined - p parameter NOT passed
				}
				else // table is sorted
				{
//console.log("3-TABLE-SORTED"); // OK
					z.p = {};
					z.p.y = a.so.y;
					z.p.g = a.so.g;
				}
			}
			else // Array-String-ResultSet
			{
//console.log("4-Array-String-ResultSet"); // OK
				z.p = null; // p is undefined - p parameter NOT passed
			}	
		}
		//
		z.v_l0 = ALiBS.to.l(a); // length
		//
		var r = -2; // [BEST-PRACTICE]
		// 
		z.v_n0 = 0; // min
		z.x0 = z.v_l0-1; // max
		z.v_d0; // med
		z.v_ld; // last med
		//
		z.v_mn = ALiBS.to.f(a,z.v_n0,z.p); 
		z.v_mx = ALiBS.to.f(a,z.x0,z.p);  
		z.v_md; // med
		//
		if(z.v_l0==0)
		{
			// ALiBS.sy.er(-4,"ALiBS.to.of",'Array.length = 0'); // Array.length = 0
			//
			r = -4;
		}
		else if(z.v_l0==-1)
		{
			// ALiBS.sy.er(-5,"ALiBS.to.of",'t is NOT a table/table.rs/Array/String'); 
			//
			r = -5;
		}
		else if(z.v_mn==e) // found on first position
		{
			ALiBS.gv.to.ol++;
			r = z.v_n0; 
		}
		else if(z.v_mx==e) // found on last position = max
		{
			ALiBS.gv.to.ol++;
			//
			for(var j=z.x0;0<=j;j--)
			{
				ALiBS.gv.to.ol++;
				//
				z.v_md = ALiBS.to.f(a,j,z.p); 
				//
				if(z.v_md==e)
				{
					r = j;
					continue;
				}
				else break;
			}
		}
		else if(z.v_mn>e) // not found
		{
			ALiBS.gv.to.ol++;
			// ALiBS.sy.er(-1,"ALiBS.to.of",'value under min'); // elemento abaixo do min
			//
			r = -1;
		}
		else if(z.v_mx<e) // not found
		{
			ALiBS.gv.to.ol++;
			// ALiBS.sy.er(-3,"ALiBS.to.of",'value over max'); // elemento acima do max
			//
			r = -3;
		}
		else
		{
			ALiBS.gv.to.ol++;
			//
			z.v_d0 = Math.round((z.v_n0+z.x0)/2);
			z.v_ld = -1;
			//
			for(var i=0;i<=z.v_d0<=z.x0;i++)
			{
				ALiBS.gv.to.ol++;
				//
				if(z.v_ld==z.v_d0) break;
				z.v_md = ALiBS.to.f(a,z.v_d0,z.p); 
				//
				if(z.v_md==e)
				{
					for(var j=z.v_d0;0<=j;j--)
					{
						ALiBS.gv.to.ol++;
						//
						z.v_md = ALiBS.to.f(a,j,z.p); 
						if(z.v_md==e)
						{
							r = j;
							continue;
						}
						else break;
					}
					break;
				}
				else if(z.v_md>e)
				{
					z.x0 = z.v_d0; // set new max
				}
				else // menor
				{
					z.v_n0 = z.v_d0; // set new min
				}
				//
				z.v_ld = z.v_d0;
				z.v_d0 = Math.round((z.v_n0+z.x0)/2); // set new med
				//
				if(z.v_d0<=0 || z.v_d0>=z.v_l0) break;
			}
		}
		//
		if(r==-2)
		{ 
			// ALiBS.sy.er(-2,"ALiBS.to.of",'value not found'); // elemento acima do max
			//
			// -2;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	ef: function(a,e,p) // Sequential Find 
	// a = (required)(Array||table||table.rs)ALiBS.to OR Array OR ResultSet
	// e = (required)(Array element)element value to be found
	// p = (optional)(JSON)parameters {g:0,y:"f"} 
	//	y = (string)field type [abcdefghijkrx] (default=f) 
	// 	g = (number)field number [0-99]
	//
	// return = (number)element index [i]
	//	r =-1 element under min
	//	r =-3 element over max
	//	r =-2 element not found
	//	r =-4 Array.length = 0
	//	r =-5 t is NOT a table/table.rs/Array/String
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.ef",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
		//
		// table variables
		ALiBS.gv.to.ec++;
		//
		// local variables [BEST-PRACTICE]
		z.ai;
		z.v_l0 = ALiBS.to.l(a); // length
		//
		var r = -2; // [BEST-PRACTICE]
		//
		if(z.v_l0==0)
		{
			//return ALiBS.sy.er(-4,"ALiBS.to.ef",'Array.length = 0'); // Array.length = 0
			r = -4;
		}
		else if(z.v_l0==-1)
		{
			//return ALiBS.sy.er(-5,"ALiBS.to.ef",'t is NOT a table/table.rs/Array/String'); 
			r = -5;
		}
		else
		{
			z.v_mn = null;
			z.v_mx = null;
			//
			for(var i=0;i<z.v_l0;i++)
			{
				ALiBS.gv.to.el++;
				z.ai = ALiBS.to.f(a,i,p);
				//
				if(!z.v_mn || z.v_mn>z.ai) z.v_mn = z.ai;
				//
				if(!z.v_mx || z.v_mx<z.ai) z.v_mx = z.ai;
				//
				if(z.ai==e)
				{
					r = i;
					break;
				}
			}
		}
		//
		if(r==-2)
		{
			if(e<z.v_mn)
			{
				//return ALiBS.sy.er(-1,"ALiBS.to.ef",'value under min'); // elemento abaixo do min
				r = -1;
			}
			else if(e>z.v_mx)
			{
				//return ALiBS.sy.er(-3,"ALiBS.to.ef",'value over max'); // elemento acima do max
				r = -3;
			}
			else
			{
				//return ALiBS.sy.er(-2,"ALiBS.to.ef",'value not found'); // elemento acima do max
				r = -2;
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	ra: function(a,p) // ResultSet (1 field) to Array
	// a = (required)(ALiBS.to.rs)ALiBS.to.rs
	// p = (optional)(JSON)parameters {g:0,y:"f"} 
	//	y = (string)field type [abcdefghijkrx] (default=f) 
	// 	g = (number)field number [0-99]
	//
	// return = (Array)Array of 1 field
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.ra",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.v_l0 = ALiBS.to.l(a); // length
		//
		var r = new Array();
		//
		for(var i=0;i<z.v_l0;i++)
		{
			r[i] = ALiBS.to.f(a,i,p); // TODO(2015-04-30): add z.px (prefix) and z.sx (sufix)
							// r[i] = z.px + ALiBS.to.f(a,i,p) + z.sx; 
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	rd: function(a,p) // remove duplicated - "a" must be sorted to call ALiBS.to.of()
	// a = (required)(Array or ALiBS.to.rs)Array or ResultSet
	// p = (optional)(JSON)parameters {g:0,y:"f"} 
	//
	//	y = (string)field type [abcdefghijkrx] (default=f) 
	//
	// 	g = (number)field number [0-99]
	//
	//	ke = (number)keep/empty values
	//		1 = DEFAULT = Keep
	//		0 = do NOT keep
	//		2 = [r]eplace to [EMPTY]
	//		3 = [r]eplace to [EMPTY] + "0"
	//
	//	rd = (number)type of return
	//		0 = Default = (Array)Array without duplicated values
	//		1 = (Array)Array of duplicated values
	//
	// return = see rd
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.rd",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.rd)!="number") z.rd = 0; // DEFAULT
		if(typeof(z.ke)!="number") z.ke = 1; // DEFAULT = 1 = KEEP Empty values
		//
		// local variables [BEST-PRACTICE]
		z.v_r0 = new Array();
		z.v_r1 = new Array();
		//
		z.v_v0 = null;
		z.v_v1 = 0; // control for NOT duplucating [NOT-EMPTY] and/or "0"
		z.v_n0 = null;
		z.v_l0 = ALiBS.to.l(a); // length
//console.log("z.v_l0="+z.v_l0);
		//
		for(var i=0;i<z.v_l0;i++)
		{
			z.v_v0 = ALiBS.to.f(a,i,p); // get field value
//console.log("z.v_v0="+z.v_v0);
			//
			if(!z.v_v0) // "" , 0 , null
			{
				if(z.ke==0)
				{
					continue; // do NOT keep empty values
				}
				else if(z.ke==1)
				{
					// Keep empty values
				}
				else if(z.ke==2 || z.ke==3)
				{
					z.v_v0 = "[EMPTY]";
				}
			}
			//
			z.v_n0 = ALiBS.to.of(z.v_r0,z.v_v0); // search z.v_r0 = not duplicated
//console.log("z.v_n0="+z.v_n0);
			//
			if(z.v_n0<0) // NOT FOUND
			{
				z.v_r0[z.v_r0.length] = z.v_v0; // not duplicated
							// TODO(2015-04-30): add z.px (prefix) and z.sx (sufix)
							// z.v_r0[z.v_r0.length] = z.px + z.v_v0 + z.sx; // not duplicated
				//
				if(z.v_v0=="[EMPTY]" && !z.v_v1) // only add 1 time
				{
					z.v_v1 = 1;
					//
					z.v_r0[z.v_r0.length] = "[NOT-EMPTY]"; // also add [NOT-EMPTY] if [EMPTY] exists
					//
					if(z.ke==3)
					{
						z.v_r0[z.v_r0.length] = "0"; // add "0"
					}
				}
			}
			else // FOUND
			{
				z.v_r1[z.v_r1.length] = z.v_v0; // duplicated
							// TODO(2015-04-30): add z.px (prefix) and z.sx (sufix)
							// z.v_r0[z.v_r0.length] = z.px + z.v_v0 + z.sx; // duplicated
				//
				if(z.v_v0=="[EMPTY]" && !z.v_v1) // only add 1 time
				{
					z.v_v1 = 1;
					//
					z.v_r1[z.v_r1.length] = "[NOT-EMPTY]";  // also add [NOT-EMPTY] if [EMPTY] exists
					//
					if(z.ke==3)
					{
						z.v_r1[z.v_r1.length] = "0"; // add "0"
					}
				}
			}
		}
		//
		var r; // [BEST-PRACTICE]
		//
		if(z.rd==0) r = z.v_r0; // remove duplicated
		else r = z.v_r1; // duplicated
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	co: function(a,b,p) // A contains B = return true/false
	// a = (required)(Array)conjunto A must be sorted to use ALiBS.to.of()
	// b = (required)(Array)conjunto B (no need to be sorted)
	// p = (optional)(JSON)parameters {ag:0,ay:"f",bg:0,by:"f"}
	//	ay = (string)field type [abcdefghijkrx] (default=f) 
	// 	ag = (number)field number [0-99]
	//	by = (string)field type [abcdefghijkrx] (default=f) 
	// 	bg = (number)field number [0-99]
	//
	// return = (boolean)true/false 
	//		return false when find the first element not contained
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.co",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		z.v_l0 = ALiBS.to.l(b); // B length
		z.v_n0 = -1;
		z.v_v0 = null;
		//
/*
if(typeof(a.so)!="undefined")
{
	console.log("a.so.y="+a.so.y)
	console.log("a.so.g="+a.so.g)
}
*/
		//
		var r = true; // [BEST-PRACTICE]
		//
		for(var i=0;i<z.v_l0;i++)
		{
			z.v_v0 = ALiBS.to.f(b,i,{g:(p && p.bg),y:(p && p.by)});
			z.v_n0 = ALiBS.to.of(a,z.v_v0); // must be sorted to use of()
			//
			if(z.v_n0<0)
			{
				r = false;
				break;
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	an: function(a,b,p) // AND = Intersection A / B (faster if a<b)(slower if a>b)
	// a = (required)(Array)conjunto A must be sorted to use ALiBS.to.of()
	// b = (required)(Array)conjunto B (no need to be sorted)
	// p = (optional)(JSON)parameters {ag:0,ay:"f",bg:0,by:"f"} 
	//	ay = (string)field type [abcdefghijkrx] (default=f) 
	// 	ag = (number)field number [0-99]
	//	by = (string)field type [abcdefghijkrx] (default=f) 
	// 	bg = (number)field number [0-99]
	//
	// return = (Array)Array Difference
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.an",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		z.v_l0 = ALiBS.to.l(b); // B length
		z.v_n0 = -1;
		z.v_v0 = null;
		//
		var r = new Array(); // [BEST-PRACTICE]
		//
		for(var i=0;i<z.v_l0;i++) // through B
		{
			z.v_v0 = ALiBS.to.f(b,i,{g:(p && p.bg),y:(p && p.by)});
			z.v_n0 = ALiBS.to.of(a,z.v_v0); // must be sorted to use of()
			if(z.v_n0>=0)
			{
				r[r.length] = z.v_v0;
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	or: function(a,b,p) // OR = Sum A / B
	// a = (required)(Array OR table OR ALiBS.to.rs)conjunto A (no need to be sorted)ALiBS.gv.ag.nn = 0; 
	// b = (required)(Array OR table OR ALiBS.to.rs)conjunto B (no need to be sorted)
	// p = (optional)(JSON)parameters {ag:0,ay:"f",bg:0,by:"f"} 
	//	ay = (string)field type [abcdefghijkrx] (default=f) 
	// 	ag = (number)field number [0-99]
	//	by = (string)field type [abcdefghijkrx] (default=f) 
	// 	bg = (number)field number [0-99]
	//
	// return = (Array)Array Sum
	{		
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.or",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		z.v_la = ALiBS.to.l(a); // length
		z.lb = ALiBS.to.l(b); // length
		//
		var r = new Array(); // [BEST-PRACTICE]
		//
		for(var i=0;i<z.v_la;i++)
		{
			r[r.length] = ALiBS.to.f(a,i,{g:(p && p.ag),y:(p && p.ay)});
		}
		//
		for(var i=0;i<z.lb;i++)
		{
			r[r.length] = ALiBS.to.f(b,i,{g:(p && p.bg),y:(p && p.by)});
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	df: function(a,b,p) // Difference A / B (A - B)
	// a = (required)(Array)conjunto A (no need to be sorted)
	// b = (required)(Array)conjunto B must be sorted to use ALiBS.to.of()
	// p = (optional)(JSON)parameters {ag:0,ay:"f",bg:0,by:"f"} 
	//	ay = (string)field type [abcdefghijkrx] (default=f) 
	// 	ag = (number)field number [0-99]
	//	by = (string)field type [abcdefghijkrx] (default=f) 
	// 	bg = (number)field number [0-99]
	//
	// return = (Array)Array Difference
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.df",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		z.v_l0 = ALiBS.to.l(a); // A length
		z.v_n0 = -1;
		z.v_v0 = null;
		//
		var r = new Array(); // [BEST-PRACTICE]
		//
		for(var i=0;i<z.v_l0;i++)
		{
			z.v_v0 = ALiBS.to.f(a,i,{g:(p && p.ag),y:(p && p.ay)});
			z.v_n0 = ALiBS.to.of(b,z.v_v0); // must be sorted to use of()
			//
			if(z.v_n0<0)
			{
				r[r.length] = z.v_v0;
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	th: function(t,p) // tree hierarchy - create tree 
	// t = (required)(ALiBS.to)table menu
	// p = (optional)(JSON)parameters {} 
	//	f01 = (string)recordDN
	//	f05 = (string)parentID
	//	f06 = (string)recordIDX - order
	//	f07 = (string)recordID
	//	x00 = (string)index for parentID
	//
	//	lv = (number)level of recordID (number of "0" to add before each recordID) (DEFAULT = 4)
	//	li = (number)level of IDX (number of "0" to add before each IDX) (DEFAULT = lv)
	//	re = (number)recordID of "ERROR" container (DEFAULT = 0)
	// return = NONE
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.th",le:ALiBS.gv.to.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		if(p) // TODO(2013-06-11): REMOVER
		{
			if(typeof(p.f01)=="string") z.f01 = p.f01;
			if(typeof(p.f05)=="string") z.f05 = p.f05;
			if(typeof(p.f06)=="string") z.f06 = p.f06;
			if(typeof(p.f07)=="string") z.f07 = p.f07;
			if(typeof(p.x00)=="string") z.x00 = p.x00;
			//
			if(typeof(p.lv)=="number") z.lv = p.lv;
			if(typeof(p.li)=="number") z.li = p.li;
			if(typeof(p.re)=="number") z.re = p.re;
		}
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.f01)=="undefined") z.f01 = "f01"; // DEFAULT 
		if(typeof(z.f05)=="undefined") z.f05 = "f05"; // DEFAULT 
		if(typeof(z.f06)=="undefined") z.f06 = "f06"; // DEFAULT 
		if(typeof(z.f07)=="undefined") z.f07 = "f07"; // DEFAULT 
		if(typeof(z.x00)=="undefined") z.x00 = "x00"; // DEFAULT 
		//
		if(typeof(z.lv)=="undefined") z.lv = 4; // DEFAULT = 4
		if(typeof(z.li)=="undefined") z.li = z.lv; // DEFAULT = z.lv
		if(typeof(z.re)=="undefined") z.re = 0; // DEFAULT = 0
		//
		z.v_tp = new ALiBS.to.t(); // parentDN = aux to mount tree
		// f00: recordID of parentID
		// f01: recordDN of parentID
		// x00: index for f00:recordID-normalized
		//
		z.v_tp.rs[0] = 
		{
			f00:"0", // f00:recordID
			f01:ALiBS.sg.re("0",z.lv), // f01:recordDN
			x00:ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+"0" ,z.lv,{o:1}) // index for f00:recordID-normalized
		}; 
		//
		for(var i=0;i<t.rs.length;i++) // create index t.x00:parentID
		{
			z.v_pid = t.rs[i][z.f05];
			z.v_rid = t.rs[i][z.f07];
			//
			if(typeof(z.v_pid)=="undefined" || z.v_pid=="" || z.v_pid==null) // ERROR parentID == [EMPTY]
			{
				t.rs[i][z.x00] = ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+z.re ,z.lv,{o:1}); // index for parentID
			}
			else if(typeof(z.v_rid)=="undefined" || z.v_rid=="" || z.v_rid==null) // ERROR recordID == [EMPTY]
			{
				t.rs[i][z.x00] = ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+z.re ,z.lv,{o:1}); // index for parentID
			}
			else if(z.v_pid==t.rs[i][z.f07]) // parentID == recordID
			{
				t.rs[i][z.x00] = ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+z.re ,z.lv,{o:1}); // index for parentID
			}
			else // OK
			{
				t.rs[i][z.x00] = ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+t.rs[i][z.f05] ,z.lv,{o:1}); // index for parentID
			}
		}
		//
		ALiBS.to.o(t,0,{y:z.x00.charAt(0)}); // sort table on x00:parentID-normalized
		//
		ALiBS.to.o(z.v_tp,0,{y:"x"}); // sort parentDN table on x00:recordID-normalized
		//
		z.v_epDN = 1; // there is at least one empty parendID
		//
		while(z.v_epDN)
		{
			// TODO(2012-09-13): should sort here as in agenda
			// ALiBS.to.o(z.v_tp,0,{y:"x"}); // sort parentDN table on x00:recordID-normalized
			//
			z.v_epDN = 0;
			//
			for(var i=0;i<t.rs.length;i++)
			{
				z.v_j0 = ALiBS.to.of(z.v_tp,t.rs[i][z.x00]); // must be sorted by x00
				//
				if(z.v_j0>=0) // found t.rs[][z.x00] = z.v_tp.rs[].x00
				{
					z.v_pDN = z.v_tp.rs[z.v_j0].f01; // parentDN
					//
					z.v_rDN = z.v_pDN + "." + ALiBS.sg.cr( ALiBS.sg.re("0",z.li)+t.rs[i][z.f06] ,z.li) + "_" + ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+t.rs[i][z.f07],z.lv);
					//
					t.rs[i][z.f01] = z.v_rDN; // set recordDN
					//
					// f00:recordID, f01:recordDN, x00:index-recordID-normalized = set parentDN in parent table
					z.v_tp.rs[z.v_tp.rs.length] = 
					{
						f00:t.rs[i][z.f07], 
						f01:z.v_rDN, 
						x00:ALiBS.sg.cr( ALiBS.sg.re("0",z.lv)+t.rs[i][z.f07] ,z.lv) // create index z.v_tp[].x00:parentID
					}; 
					//
					ALiBS.sy.la(7+0," z.v_tp.rs[].f00="+z.v_tp.rs[z.v_tp.rs.length-1].f00,{o:"ALiBS.to.th",le:ALiBS.gv.to.le});
					ALiBS.sy.la(7+0," z.v_tp.rs[].f01="+z.v_tp.rs[z.v_tp.rs.length-1].f01,{o:"ALiBS.to.th",le:ALiBS.gv.to.le});
					ALiBS.sy.la(7+0," z.v_tp.rs[].x00="+z.v_tp.rs[z.v_tp.rs.length-1].x00,{o:"ALiBS.to.th",le:ALiBS.gv.to.le});
				}
				else
				{
					z.v_epDN++; // number of parentID NOT found in table parentDN
				}
			}
		}
		//
		var r = z.lv;
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	f1: function(t,i,f,p) // complex filter = string filter
	// t = (required)(ALiBS.to)table menu
	// i = (required)(number)record number/position
	// f = (required)(string)complex filter
	// p = (optional)(JSON)parameters {} 
	//	? = 
	// return = 
	//	0 = FALSE = filter FAILED
	//	1 = TRUE = filter OK
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.f1",le:ALiBS.gv.to.le});
		//
		/*
		AND
			if(f00!="abcd") return -1; // !=
			//
			if(f01!="xxxx") return -1; // !=
			//
			return 1; // ONLY if all ifs OK

		OR
			if(f00=="abcd") return 1;
			//
			if(f01=="xxxx") return 1;
			//
			return -1; // ONLY if all ifs FAILED
		*/
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		f = f.replace(" "+"||","||").replace("||"+" ","||").replace(" "+"&&","&&").replace("&&"+" ","&&");
		//
		//f = f.replace(" "+")",")").replace(")"+" ",")").replace(" "+"(","(").replace("("+" ","(");
		//
		z.ix = f.indexOf(")||("); // OR group operation
		//
		if(z.ix>0) // OR group operation
		{
			z.v_gs = f.split(")||("); // OR group operation
			z.v_go = "||"; // OR group operation
			z.v_io = "&&"; // AND item operation
		}
		else 
		{
			z.ix = f.indexOf(")&&("); // AND group operation 
			//
			if(z.ix>0) // AND group operation
			{
				z.v_gs = f.split(")&&("); // AND group operation 
				z.v_go = "&&"; // AND group operation
				z.v_io = "||"; // OR item operation
			}
			else // single group operation
			{
				z.ix = f.indexOf("&&"); // AND group operation
				//
				z.v_gs = new Array(f.substring(1,f.length-1)); // groups 
				z.v_go = "??"; // [NO] group operation
				//
				if(z.ix>0) // AND group operation
				{
					z.v_io = "&&"; // AND item operation
				}
				else
				{
					z.ix = f.indexOf("||"); // OR group operation
					//
					if(z.ix>0) // OR group operation
					{
						z.v_io = "||"; // OR item operation
					}
					else
					{
						z.v_io = "??"; // [NO] item operation
					}
				}
			}
		}
		//
		z.v_lgs = z.v_gs.length; // number of groups
		//
		ALiBS.sy.la(5+0,"z.v_gs="+z.v_gs,{o:"ALiBS.to.f1",le:ALiBS.gv.to.le});
		ALiBS.sy.la(5+0,"z.v_go="+z.v_go,{o:"ALiBS.to.f1",le:ALiBS.gv.to.le});
		ALiBS.sy.la(5+0,"z.v_io="+z.v_io,{o:"ALiBS.to.f1",le:ALiBS.gv.to.le});
		ALiBS.sy.la(5+0,"z.v_lgs="+z.v_lgs,{o:"ALiBS.to.f1",le:ALiBS.gv.to.le});
		//
		z.v_bk = 0; // break;
		//
		for(var k=0;k<z.v_lgs;k++) // for each group operation
		{
			z.v_gi = z.v_gs[k].split(z.v_io); 
			z.v_lgi = z.v_gi.length; // number of items
			//
			for(var j=0;j<z.v_lgi;j++)
			{
//if(D.ff.counter==null) D.ff.counter=0;
//else D.ff.counter++;
//console.log("temp:"+D.ff.counter)
				z.v_gij = z.v_gi[j].replace(/\(/g,""); 
				z.v_gij = z.v_gij.replace(/\)/g,""); 
				//
				z.fd = z.v_gij.substring(0,3);
				z.v_op = z.v_gij.substring(3,5);
				//
				// f00==xx
				//
				if(z.v_op.charAt(1)=="=" || z.v_op.charAt(1)==":") // == >= <= != := || :: !:
				{
					z.v_va = z.v_gij.substring(5,z.v_gij.length);
				}
				else // < >
				{
					z.v_op = z.v_gij.substring(3,4);
					z.v_va = z.v_gij.substring(4,z.v_gij.length);
				}
				//
//console.log("z.v_op="+z.v_op)
//console.log("z.v_va="+z.v_va)
				if(z.v_io=="??")
				{
					z.iv = ALiBS.to.f0(t.rs[i][z.fd],z.v_op,z.v_va);
					z.v_bk = 1; // break outter for
					break;
				}
				else if(z.v_io=="||")
				{
					if( ALiBS.to.f0(t.rs[i][z.fd],z.v_op,z.v_va) )
					{
						z.iv = 1; // item result
						break; // break if ANY result is TRUE for this group
					}
					else
					{
						z.iv = 0; // item result
					}
				}
				else // &&
				{
					if( ALiBS.to.f0(t.rs[i][z.fd],z.v_op,z.v_va) )
					{
						z.iv = 1; // item result
					}
					else
					{
						z.iv = 0; // item result
						break; // break if ANY result is FALSE for this group
					}
				}
			}
			//
			if(z.v_bk==1)
			{
				break;
			}
			else if(z.v_go=="||")
			{
				if(z.iv==1) break; // break if GROUP result is TRUE for ANY group
			}
			else if(z.v_go=="&&")
			{
				if(z.iv==0) break; // break if GROUP result is FALSE for ANY group
			}
			//else if(z.v_go=="??")
			//{
				// return above = do not need here
			//}
		}
		//
		var r = z.iv; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	f0:function(f,o,v,p) // simple filter
	// f = (required)(????)field value
	// o = (required)(string)operation == != >= <= > < 
	// v = (required)(string)value
	// p = (optional)(JSON)parameters {} 
	//	? = 
	// return = 
	//	0 = FALSE = filter FAILED
	//	1 = TRUE = filter OK
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.f0",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// local variables [BEST-PRACTICE]
		//z.v_v0;
		//z.v_v1;
		//z.v2;

	/*

	for(var i=0;i<T2.rs.length;i++) 
		if(    (ALiBS.to.f0(T2.rs[i]["f00"],"==","Lotofacil") && ALiBS.to.f0(T2.rs[i]["f01"],"==","0917"))  || (ALiBS.to.f0(T2.rs[i]["f00"],"==","Quina") && ALiBS.to.f0(T2.rs[i]["f01"],"==","3215"))  ) 
			console.log(T2.rs[i].f00 + " - " + T2.rs[i].f01);

	// ****

	for(var i=0;i<T2.rs.length;i++) 
		if(    (ALiBS.to.f0(T2.rs[i]["f00"],"==","Lotofacil") || ALiBS.to.f0(T2.rs[i]["f00"],"==","Quina"))  && (ALiBS.to.f0(T2.rs[i]["f01"],"==","0917") || ALiBS.to.f0(T2.rs[i]["f01"],"==","3215"))  ) 
			console.log(T2.rs[i].f00 + " - " + T2.rs[i].f01);

	*/

		//
		var r = 0; // FAILED = FALSE // [BEST-PRACTICE]
		//
		if(!f) f = ""; // for tables, it's all the same ("",null,undefined)
		//
		if(!v) v = ""; // for tables, it's all the same ("",null,undefined)
		//
		if(o=="==")
		{ 
			if(f==v) r = 1; // OK = TRUE
		} 
		else if(o=="::") // test
		{
//print("\nv="+v);
			z.v2 = v.replace(/^\*/,"").replace(/\*$/,"").replace(/\*/g,".*"); // remove "*" at begin and at end
			//z.v2 = v.replace(/\*/g,".*");
			//
			z.v_v0 = new RegExp(""+z.v2,"g");
			//
//print("\nz.v_v0="+z.v_v0);
			z.v_v1 = z.v_v0.test(f);
			//
			if(z.v_v1) r = 1;  // OK = TRUE
		}
		else if(o==":=" || o=="!:") // match OR NOT match
		{
//print("\nv="+v);
			z.v2 = v.replace(/^\*/,"").replace(/\*$/,"").replace(/\*/g,".*"); // remove "*" at begin and at end
			//z.v2 = v.replace(/\*/g,".*");
			//
			z.v_v0 = new RegExp(""+z.v2,"g");
			//
//print("\nz.v_v0="+z.v_v0);
			z.v_v1 = f.match(z.v_v0);
			//
			if(z.v_v1 && z.v_v1.length>0) r = 1;  // OK = TRUE
			//
//console.log("passou1")
//console.log("f="+f);
//console.log("o="+o);
//console.log("v="+v);
//console.log("z.v_v0="+z.v_v0);
			if(o=="!:")
			{
//console.log("passou2")
				r = (r==1? 0:1);
			}
		}
		else if(o=="!=")
		{ 
			if(f!=v) r = 1;  // OK = TRUE
		}
		else if(o==">=")
		{
			if(f>=v) r = 1; // OK = TRUE
		}
		else if(o=="<=")
		{
			if(f<=v) r = 1; // OK = TRUE
		}
		else if(o==">")
		{ 
			if(f>v) r = 1; // OK = TRUE
		}
		else if(o=="<")
		{
			if(f<v) r = 1; // OK = TRUE
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	at:function(t0,t1,p) // append table with second table records
	// t0 = (required)(ALiBS.to)table to receive new fields
	// t1 = (required)(ALiBS.to)table to provide fields
	// p = (optional)(JSON)parameters {} 
	//	? = 
	//
	// return = 
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.at",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//
		// table variables
		//
		// local variables [BEST-PRACTICE]
		//
		for(var i=0;i<t1.rs.length;i++)
		{
			t0.rs[t0.rs.length] = t1.rs[i]; // pointer to t1.rs
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; 
	},
	// *****************************************************************
	ct:function(t0,t1,rf,sf,p) // complete table with second table fields
	// t0 = (required)(ALiBS.to)table to receive new fields
	// t1 = (required)(ALiBS.to)table to provide fields
	// rf = (required)(string)relation fields (ex: f00==f01)
	// sf = (required)(string)set fields (f00=f01,f02=g00,g00=f03)
	// p = (optional)(JSON)parameters {} 
	//
	// 	fc = (string)complex filter
	//		* = DEFAULT
	//
	// return = 
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.v_ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}
			//
			ALiBS.sy.la(5+0,"t0.tb.fc="+t0.tb.fc,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			z.ec = -1101; // 
			if(rf.indexOf("==")>0)
			{
				z.ec = -1102; // 
				z.v_r2 = rf.split("=="); // f00==f01
			}
			else
			{
				z.ec = -1103; // 
				z.v_r2 = rf.split("="); // f00=f01
			}
			//
			ALiBS.sy.la(5+0,"z.v_r2="+z.v_r2,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			z.ec = -1201; // 
			z.v_r0 = z.v_r2[0]; // f00
			z.ec = -1202; // 
			z.v_r1 = z.v_r2[1]; // f01
			//
			ALiBS.sy.la(5+0,"z.v_r0="+z.v_r0,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_r1="+z.v_r1,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			z.ec = -1301; // 
			z.v_r0f = "" + z.v_r0.charAt(0); // [abcdefghijkrx]
			z.ec = -1302; // 
			z.v_r1f = "" + z.v_r1.charAt(0); // [abcdefghijkrx]
			//
			ALiBS.sy.la(5+0,"z.v_r0f="+z.v_r0f,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_r1f="+z.v_r1f,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			z.ec = -1401; // 
			z.v_r0n = parseInt(z.v_r0.substr(1,2),10); // [0-99]
			z.ec = -1402; // 
			z.v_r1n = parseInt(z.v_r1.substr(1,2),10); // [0-99]
			//
			ALiBS.sy.la(5+0,"z.v_r0n="+z.v_r0n,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_r1n="+z.v_r1n,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			z.ec = -1501; // 
			z.fd = new Array();
			z.ec = -1502; // 
			z.v_fo = new Array();
			//		
			z.ec = -1601; // 
			z.v_sf2 = sf.split(",");
			//
			ALiBS.sy.la(5+0,"z.v_sf2="+z.v_sf2,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			z.ec = -1701; // 
			for(var i=0;i<z.v_sf2.length;i++)
			{
				z.ec = -1702; // 
				z.sf3 = z.v_sf2[i].split("=");
				//
				z.ec = -1703; // 
				z.fd[z.fd.length] = z.sf3[0];
				z.ec = -1704; // 
				z.v_fo[z.v_fo.length] = z.sf3[1];
			}
			//
			ALiBS.sy.la(5+0,"z.fd="+z.fd,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.v_fo="+z.v_fo,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
			//
			z.v_v1 = ""; // old value
			z.v_n1 = -1000; // old value
			//
			//ALiBS.to.o(t0,z.v_r0n,{y:z.v_r0f}); // TODO(2013-06-18): sorting t0 will speed up search and [r]eplace (use old values z.v_v1) - should I sort here or outside?
			z.ec = -1801; // 
			ALiBS.to.o(t1,z.v_r1n,{y:z.v_r1f}); // MUST be sorted
			//
			z.ec = -1901; // 
			for(var i=0;i<t0.rs.length;i++) // records
			{
				ALiBS.sy.la(7+0,"i="+i,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
				//
				z.ec = -2001; // 
				z.v_ok = z.v_ALL || t0.ft.fc(t0,i); // "complex" filter
				//
				z.ec = -2002; // 
				if(z.v_ok) // record OK
				{
					z.ec = -2101; // 
					z.v_v0 = t0.rs[i][z.v_r0]; // 
					//
					if(z.v_v0 && z.v_v0==z.v_v1) // new == old --> use old value
					{
						z.ec = -2201; // 
						ALiBS.sy.la(7+0,"z.v_v1="+z.v_v1,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
						ALiBS.sy.la(7+0,"z.v_n1="+z.v_n1,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
						//
						z.ec = -2202; // 
						for(var j=0;j<z.fd.length;j++)
						{
							z.ec = -2203; // 
							if(z.v_n1>=0) t0.rs[i][z.fd[j]] = t1.rs[z.v_n1][z.v_fo[j]];
						}
					}
					else // new != old -->> find again
					{
						z.ec = -2301; // 
						z.v_n0 = ALiBS.to.of(t1,z.v_v0); // Sorted Find - search on SORTED field - find primary key OR first value, if value is duplicated 
						//
						z.ec = -2302; // 
						ALiBS.sy.la(7+0,"z.v_v0="+z.v_v0,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
						ALiBS.sy.la(7+0,"z.v_n0="+z.v_n0,{o:"ALiBS.to.ct",le:ALiBS.gv.to.le});
						//
						z.ec = -2303; // 
						if(z.v_n0>=0) // found
						{
							z.ec = -2401; // 
							for(var j=0;j<z.fd.length;j++)
							{
								z.ec = -2402; // 
								z.v_vf = t1.rs[z.v_n0][z.v_fo[j]];
								//
								if(!z.v_vf) // undefined / null / 0
								{
									if(typeof(z.v_vf)=="number") // 0
									{
										z.ec = -2403; // 
										t0.rs[i][z.fd[j]] = 0; 
									}
									else // undefined / null
									{
										z.ec = -2404; // 
										t0.rs[i][z.fd[j]] = ""; 
									}
								}
								else // exists
								{
									z.ec = -2405; // 
									t0.rs[i][z.fd[j]] = z.v_vf; 
								}
							}
							z.v_v1 = z.v_v0; // old value
							z.v_n1 = z.v_n0; // old value
						}
						else // NOT found = complete with ""
						{
							z.ec = -2501; // 
							for(var j=0;j<z.fd.length;j++)
							{
								t0.rs[i][z.fd[j]] = "";
							}
							z.v_v1 = ""; // old value
							z.v_n1 = -1000; // old value
						}
					}
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.to.ct",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; // r; 
	},
	// *****************************************************************
	dt:function(t0,p) // delete records from table
	// t0 = (required)(ALiBS.to)table to delete records
	//
	// p = (optional)(JSON)parameters {}
	//
	// 	fc = (string)complex filter
	//		* = DEFAULT
	//
	// 	fd = (string)fields list (f00,f01.f02) 
	//		* = DEFAULT
	//
	// return = none
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.dt",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.fc)!="string")
		{
			//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
			z.v_ALL = 1; // faster
		}
		else
		{
			t0.tb.fc = z.fc; 
		}
		//
		if(typeof(z.fd)!="string") // TODO(2013-07-31): acho que nao serve pra nada
		{
			z.v_fd2 = new Array();
		}
		else
		{
			z.v_fd2 = z.fd.split(",");
		}
		//
		ALiBS.sy.la(5+0,"z.v_fd2="+z.v_fd2,{o:"ALiBS.to.dt",le:ALiBS.gv.to.le});
		//
		// table variables
		//
		// local variables [BEST-PRACTICE]
		//
		z.v_nu = 0;
		//
		for(var i=0;i<t0.rs.length;i++) // records
		{
			z.v_ok = z.v_ALL || t0.ft.fc(t0,i); // "complex" filter
			//
			ALiBS.sy.la(7+0,"(rs["+i+"])z.v_ok="+z.v_ok,{o:"ALiBS.to.dt",le:ALiBS.gv.to.le});
			//
			if(z.v_ok) // record OK
			{
				t0.rs[i] = null;
				//
				z.v_nu++;
			}
		}
		//
		ALiBS.sy.la(5+0,"z.v_nu="+z.v_nu,{o:"ALiBS.to.dt",le:ALiBS.gv.to.le});
		//
		ALiBS.sy.la(5+0,"(a)rs.length="+t0.rs.length,{o:"ALiBS.to.dt",le:ALiBS.gv.to.le});
		//
		t0.rs.sort();
		//
		t0.rs.length = t0.rs.length - z.v_nu;
		//
		ALiBS.sy.la(5+0,"(b)rs.length="+t0.rs.length,{o:"ALiBS.to.dt",le:ALiBS.gv.to.le});
		//
		r = z.v_nu;
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	fdn:function(t0,p) // get ALL defined field names
	// t0 = (required)(ALiBS.to)table to get fields
	// p = (optional)(JSON)parameters {} 
	//	rs = (number)number of records to read fields
	//		1 = DEFAULT = 1 record = Record 0
	//		0 = NO records
	//		-1 = All records = t0.rs.length
	//
	// return = Array of field names (nm[fn] + cp[fn] + rs[i][fn])
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.fdn",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = new Array();
		//
		// ************************
		//
		// if !rs !! rs!=number		rs = 1
		// if rs=-1 			rs = t0.rs.length
		// if rs<0 			rs = 0
		// if rs>t0.rs.length		rs = t0.rs.length
		// else (0,1,2,etc)		rs = rs
		//
		z.v_rs = 1; // TODO:()
		//
		// ************************
		//
		for(var i=0; i<z.v_rs;i++) // get fields from Records
		{
			if(t0.rs[i]) // 
			{
				for(var j in t0.rs[i]) // get fields from record i
				{
					r[r.length] = j;
				}
			}
		}
		//
		// ************************
		//
		for(var i in t0.nm) // get fields from Names[]
		{
			r[r.length] = i;
		}
		//
		// ************************
		//
		for(var i in t0.cp) // get fields from Caption[]
		{
			r[r.length] = i;
		}
		//
		// ************************
		//
		for(var i in t0.mn) // get fields from Reverse[]
		{
			r[r.length] = t0.mn[i];
		}
		//
		// ************************
		//
		for(var i in t0.tb.sh) // get fields from Display t.tb.sh
		{
			r[r.length] = t0.tb.sh[i];
		}
		//
		// ************************
		//
		if(typeof(t0.tb.nso)=="string" && t0.tb.nso!="") r[r.length] = t0.tb.nso; // get fields from t.tb.nso
		//
		// ************************
		//
		r.sort();
		//
		r = ALiBS.to.rd(r); // remove duplicated - "a" must be sorted to call ALiBS.to.of()
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.to.fdn",le:ALiBS.gv.to.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	n1:function(t0,n1,p) // normal table to nx1 table
	// t0 = (required)(ALiBS.to)table to receive new fields
	// n1 = (required)(string)multivalue field to create nx1 table (f00,f01,etc)
	// p = (optional)(JSON)parameters {} 
	//
	//	ff0 = (function)function(t0,ri) runs before split
	//	ff1 = (function)function(t0,ri) runs after split
	//
	//	 //TODO(2014-10-23): should use ANY separator passed by parameter
	//
	// return = 
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.n1",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(ALiBS.sy.ty(z.ff0)!="func"+"tion") z.ff0 = null; // ff0 = before split
			if(ALiBS.sy.ty(z.ff1)!="func"+"tion") z.ff1 = null; // ff1 = after split
			//
			// table variables
			// 
			//
			// local variables [BEST-PRACTICE]
			z.ec = -1001; // 
			z.fd = new Array();
			//
			z.ec = -1002; // 
			z.v_fdn = ALiBS.to.fdn(t0);
			//
			z.ec = -1003; // 
			for(var i=0;i<z.v_fdn.length;i++) 
			{
				z.ec = -1001; // 
				if(z.v_fdn[i]!=n1) // do NOT set "nx1" field name (f00) to Array
				{
					z.ec = -1002; // 
					z.fd[z.fd.length] = z.v_fdn[i]; // field names, f00, f01, etc
				}
			}
			//
			ALiBS.sy.la(5+0,"n1="+n1,{o:"ALiBS.to.n1",le:ALiBS.gv.to.le});
			ALiBS.sy.la(5+0,"z.fd="+z.fd,{o:"ALiBS.to.n1",le:ALiBS.gv.to.le});
			//
			z.ec = -1201; // 
			z.v_t0rs = t0.rs; // save rs[] from normal table
			//
			z.ec = -1202; // 
			t0.rs = new Array(); // reset Array - table nx1
			//
			z.v_j0 = 0; // counter for table nx1
			//
			z.ec = -1203; // 
			for(var i=0;i<z.v_t0rs.length;i++) // for each t0rs[i]
			{
				z.ec = -1301; // 
				if(z.ff0) z.ff0(t0,i); // ff0 = before split
				//
				z.ec = -1302; // 
				z.v_v0 = z.v_t0rs[i][n1];
				//
				if(z.v_v0)
				{
					z.ec = -1401; // 
					z.v_v1 = z.v_v0.split("!pIpE!"); //TODO(2014-10-23): should use ANY separator passed by parameter
					//
					ALiBS.sy.la(7+0,"z.v_v0="+z.v_v0,{o:"ALiBS.to.n1",le:ALiBS.gv.to.le});
					ALiBS.sy.la(7+0,"z.v_v1="+z.v_v1,{o:"ALiBS.to.n1",le:ALiBS.gv.to.le});
					//
					z.ec = -1402; // 
					for(var j=0;j<z.v_v1.length;j++)
					{
						z.v_len = z.v_j0++;
						//
						z.ec = -1501; // 
						t0.rs[z.v_len] = {};
						//
						z.ec = -1502; // 
						t0.rs[z.v_len][n1] = z.v_v1[j]; // 
						//
						z.ec = -1503; // 
						for(var k=0;k<z.fd.length;k++) // not contains n1 field
						{
							z.ec = -1504; // 
							t0.rs[z.v_len][z.fd[k]] = z.v_t0rs[i][z.fd[k]]; 
						}
						//
						z.ec = -1505; // 
						if(z.ff1) z.ff1(t0,z.v_len); // ff1 = after split
					}
				}
				else
				{
					z.v_len = z.v_j0++;
					//
					z.ec = -1601; // 
					t0.rs[z.v_len] = {};
					//
					z.ec = -1602; // 
					t0.rs[z.v_len][n1] = ""; // z.v_v0 = "" null undefined
					//
					z.ec = -1603; // 
					for(var k=0;k<z.fd.length;k++) // not contains n1 field
					{
						z.ec = -1604; // 
						t0.rs[z.v_len][z.fd[k]] = z.v_t0rs[i][z.fd[k]]; 
					}
					//
					z.ec = -1605; // 
					if(z.ff1) z.ff1(t0,z.v_len); // ff1 = after split
				}
			}
			//
			z.ec = -1901; // 
			ALiBS.to.un(t0);
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.to.n1",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; // r; 
	},
	// *****************************************************************
	nf: function(t0,p) // // Normalize Fields = sort does NOT work OK with undefined/null/empty fields
	/** 
	Method <static> <void> ALiBS.to.nf(p)

		Description: Normalize Fields

		Parameters:

			t0 = (required)(ALiBS.to)table

			p = (optional)(JSON)parameter {}

			 	fd = (string)fields list (f00,f01.f02) 
					* / EMPTY = ALL fields = DEFAULT

		Observations:
	
		Return Value:
			<static> <void> void

		Example(s):
			ALiBS.to.nf("f00,f21,f22");

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - NOT to be disclosed
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			//if(typeof(z.ho)!="string") throw "ALiBS.to.nf.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.fd)!="string" || z.fd=="*")
			{
				z.fd = ALiBS.to.fdn(t0); // get ALL defined field names = DEFAULT = "*"
				//
				ALiBS.sy.la(5+0,"(*)z.fd="+z.fd,{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
				//console.log("(*)z.fd="+z.fd);
			}
			else
			{
				z.fd = z.fd.split(",");
				//
				ALiBS.sy.la(5+0,"(,)z.fd="+z.fd,{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
				//console.log("(,)z.fd="+z.fd);
			}
			//
			ALiBS.sy.la(5+0,"z.fd="+z.fd,{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			//z.v_a0 = new Array();
			//
			// ************************
			//
			for(var i=0;i<t0.rs.length;i++) // Normalize Fields = sort does NOT work OK with undefined/null/empty fields
			{
				for(var j=0;j<z.fd.length;j++)
				{
					if( !t0.rs[i][z.fd[j]] )
					{
						t0.rs[i][z.fd[j]] = '';
					}
				}
			}
			//
			// ************************
			//
			z.ec = -9800; // 
			//z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.to.nf.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			r = ""; // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.to.nf",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.to.nf",le:ALiBS.gv.to.le});
		//
		return r; 
	},
	// *****************************************************************
	lt:function(t0,p) // log table to ALiBS.sy.la
	// t0 = (required)(ALiBS.to)table to log
	//
	// p = (optional)(JSON)parameters {}
	//
	// 	lv = (number)level to log
	//		5 = DEFAULT
	//
	//	le = (number)log local le (0,1,2)
	//		ALiBS.gv.to.le = DEFAULT
	//
	//	o = (string)origin function ({o:"originFunction"})
	//		ALiBS.to.lt = DEFAULT
	//
	// 	fc = (string)complex filter
	//		* = DEFAULT
	//
	// 	fd = (string)fields list (f00,f01.f02) 
	//		* = DEFAULT
	//
	//	mx = (number)max records
	//		10 = DEFAULT
	//
	//	mvx = (number)max multivalue attrs
	//		50 = DEFAULT
	//
	//	ll = (number)length for z.m.substr - DEFAULT = ALiBS.gv.sy.gll
	//
	//	id = (string)id/msg to display before each line
	//		"" = DEFAULT
	//
	//	mvs = (Array)separators (plural) for multivalue fields ["f00=!pIpE!","f01=,","f02=;"]
	//
	// return = none
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.lt",le:ALiBS.gv.to.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.lv)!="number") z.lv = 5; // DEFAULT = 5
		if(typeof(z.le)!="number") z.le = ALiBS.gv.to.le; // DEFAULT = ALiBS.gv.to.le
		if(typeof(z.mx)!="number") z.mx = 10; // DEFAULT = 10
		if(typeof(z.mvx)!="number") z.mvx = 50; // DEFAULT = 50
		if(typeof(z.o)!="string") z.o = "ALiBS.to.lt"; // DEFAULT = ALiBS.to.lt
		if(typeof(z.id)!="string") z.id = ""; // DEFAULT = ""
		//
		if(typeof(z.fc)!="string")
		{
			//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
			z.v_ALL = 1; // faster
		}
		else
		{
			t0.tb.fc = z.fc; 
		}
		//
		if(typeof(z.fd)!="string")
		{
			z.v_fd2 = ALiBS.to.fdn(t0);
		}
		else
		{
			z.v_fd2 = z.fd.split(",");
		}
		//
		z.v_fd2.sort();
		//
		z.v_mv0 = new Array(); // separators = DEFAULT = Empty Array
		//
		if(ALiBS.sy.ty(z.mvs)=="obj"+"ect-Array")
		{
			for(var i=0;i<z.v_fd2.length;i++)
			{
				z.v_mv0[i] = ""; // DEFAULT = no separator
				//
				for(var j=0;j<z.mvs.length;j++)
				{
					if(z.mvs[j].indexOf(z.v_fd2[i])>=0)
					{
						z.v_mv0[i] = ALiBS.sg.as(z.mvs[j],"=");
						//
						break; // found
					}
				}
			}
		}
		//
		ALiBS.sy.la(5+0,"z.v_fd2="+z.v_fd2,{o:"ALiBS.to.lt",le:ALiBS.gv.to.le});
		ALiBS.sy.la(5+0,"z.mvs="+z.mvs,{o:"ALiBS.to.lt",le:ALiBS.gv.to.le});
		ALiBS.sy.la(5+0,"z.v_mv0="+z.v_mv0,{o:"ALiBS.to.lt",le:ALiBS.gv.to.le});
		//
		// table variables
		//
		// local variables [BEST-PRACTICE]
		//
		z.la = ALiBS.sy.la; // reference to fool ALiBS.cp
		//
		// ************************
		//
		z.v_nms = new Array();
		for(var i in t0.nm)
		{
			z.v_nms[z.v_nms.length] = i;
		}
		z.la(z.lv,"nm="+z.v_nms.sort().join(","),{o:z.o,le:z.le,ll:z.ll});
		//
		z.v_cps = new Array();
		for(var i in t0.cp)
		{
			z.v_cps[z.v_cps.length] = i;
		}
		z.la(z.lv,"cp="+z.v_cps.sort().join(","),{o:z.o,le:z.le,ll:z.ll});
		//
		// ************************
		//
		z.la(z.lv,z.id+"rs.length="+t0.rs.length,{o:z.o,le:z.le,ll:z.ll});
		//
		// ************************
		//
		for(var i=0;(i<z.mx && i<t0.rs.length);i++) // records
		{
			z.v_ok = z.v_ALL || t0.ft.fc(t0,i); // "complex" filter
			//
			if(z.v_ok) // record OK
			{
				for(var j=0;j<z.v_fd2.length;j++) // fields
				{
					if(z.v_mv0.length>0)
					{
						if(z.v_mv0[j]!="") // field j has sep j
						{
							z.v_fva = t0.rs[i][z.v_fd2[j]].split(z.v_mv0[j]);
							//
							for(var k=0;k<z.v_fva.length;k++)
							{
								if(k<z.mvx)
								{
									z.la(z.lv,z.id+"rs["+i+"]["+z.v_fd2[j]+"]["+k+"]=["+z.v_fva[k]+"]",{o:z.o,le:z.le,ll:z.ll});
								}
								else break; // max multivalue attr
							}
						}
						else
						{
							z.la(z.lv,z.id+"rs["+i+"]["+z.v_fd2[j]+"]=["+t0.rs[i][z.v_fd2[j]]+"]",{o:z.o,le:z.le,ll:z.ll});
						}
					}
					else
					{
						z.la(z.lv,z.id+"rs["+i+"]["+z.v_fd2[j]+"]=["+t0.rs[i][z.v_fd2[j]]+"]",{o:z.o,le:z.le,ll:z.ll});
					}
					//
					z.la(z.lv,ALiBS.gv.sy.se,{o:z.o,le:z.le});
				}
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return; 
	}
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
/*
	l00: function(a,p) // list records - falta adicionar filtro para funcionar como select
	// a =(required)(Array)ALiBS.to.rs
	// p = (optional)(JSON)parameters {t:0,s:"",l:""} 
	// 	t = (number)total options
	// 	s = (string)field separador
	// 	l = (string)line separador
	//
	// return = (Array) 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.l00",le:ALiBS.gv.to.le});
		//
		//TODO(2010-09-08):rever função
		//
		var r = new Array(); // return array
		var t = (p && p.t) || (1024*2);
		var s = (p && p.s) || ", ";
		var l = (p && p.l) || "<br>";
		//
		var n = a.length;
		//
		for(var i=0;i<n;i++)
		{
			if(ALiBS.op.co(t,0)) r[r.length]="f00="+a[i].f00+s;
			if(ALiBS.op.co(t,1)) r[r.length]="f01="+a[i].f01+s;
			if(ALiBS.op.co(t,2)) r[r.length]="f02="+a[i].f02+s;
			if(ALiBS.op.co(t,3)) r[r.length]="f03="+a[i].f03+s;
			if(ALiBS.op.co(t,4)) r[r.length]="f04="+a[i].f04+s;
			if(ALiBS.op.co(t,5)) r[r.length]="f05="+a[i].f05+s;
			if(ALiBS.op.co(t,6)) r[r.length]="f06="+a[i].f06+s;
			if(ALiBS.op.co(t,7)) r[r.length]="f07="+a[i].f07+s;
			if(ALiBS.op.co(t,8)) r[r.length]="f08="+a[i].f08+s;
			if(ALiBS.op.co(t,9)) r[r.length]="f09="+a[i].f09+s;
			if(ALiBS.op.co(t,10)) r[r.length]="f10="+a[i].f10+s;
			r[r.length]=l;
		}
		//
		return r; 
	},
	// *****************************************************************
	lc00: function(a,p) // list records - control fields
	// a =(required)(Array)ALiBS.to.rs
	// p = (optional)(JSON)parameters {t:0,s:"",l:""} 
	// 	t = (number)total options
	// 	s = (string)field separador
	// 	l = (string)line separador
	//
	// return = (Array) 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.to.lc00",le:ALiBS.gv.to.le});
		//
		//TODO(2010-09-08):rever função
		//
		var r = new Array(); // return array
		var t = (p && p.t) || (1024*2);
		var s = (p && p.s) || ", ";
		var l = (p && p.l) || "<br>";
		//
		var n = a.length;
		//
		for(var i=0;i<n;i++)
		{
			if(ALiBS.op.co(t,0)) r[r.length]="c00="+a[i].c00+s;
			if(ALiBS.op.co(t,1)) r[r.length]="c01="+a[i].c01+s;
			if(ALiBS.op.co(t,2)) r[r.length]="c02="+a[i].c02+s;
			if(ALiBS.op.co(t,3)) r[r.length]="c03="+a[i].c03+s;
			if(ALiBS.op.co(t,4)) r[r.length]="c04="+a[i].c04+s;
			if(ALiBS.op.co(t,5)) r[r.length]="c05="+a[i].c05+s;
			if(ALiBS.op.co(t,6)) r[r.length]="c06="+a[i].c06+s;
			if(ALiBS.op.co(t,7)) r[r.length]="c07="+a[i].c07+s;
			r[r.length]=l;
		}
		//
		return r; 
	}
*/
	// *****************************************************************
/*	nm: function(t,g) // get data field name
	// t = (required)(ALiBS.to)table
	// g = (required)(number)field number 
	// return = field value
	{
		//TODO(2010-09-08):Implementar f,c,a,i,x
		//
		var y = "f";
		if(p && typeof(p.y)=="string" && "abcdefghijkrx".indexOf(p.y)>=0) y = p.y; 
		//
		var oo = ("0"+g).substr(xxxx-2,2); 
		//
		return t.nm[(y+oo)];
	},
	// *****************************************************************
	cp: function(t,g) // get data field caption
	// t = (required)(ALiBS.to)table
	// g = (required)(number)field number 
	// return = field value
	{
		//TODO(2010-09-08):Implementar f,c,a,i,x
		//
		var y = "f";
		if(p && typeof(p.y)=="string" && "abcdefghijkrx".indexOf(p.y)>=0) y = p.y; 
		//
		var oo = ("0"+g).substr(xxxx-2,2); 
		//
		return t.cp[(y+oo)];
	},
	// *****************************************************************
	tf: function(t,g) // get table field
	// t = (required)(ALiBS.to)table
	// g = (required)(number)field number 
	// return = field value
	{
		var oo = ("0"+g).substr(xxxx-2,2); 
		//
		return t.tb[("t"+oo)];
	},*/
	// *****************************************************************
/*	r: function(p) // record
	// p = (optional)(JSON)parameters {a00:"",a01:"",f00:"",f01:"",k00:"",r00:"",x00:""} 
	//	[abcdefghijkrx][00] ... [abcdefghijkrx][99] = (any) value of field TODO(2010-09-08): colocar a descrição desta forma em todos os metodos
	//
	// return record object
	{
		var r = {}; // record
		//
		if(p)
		{
			var z = "";
			var oo;
			//
			var ty = "abcdefghijkrx"; // types 
			var l = ty.length;
			//
			for(var k=0;k<l;k++) // types
			{
				z = ty.charAt(k); 
				//
				for(var i=0;i<=99;i++) // r.f00,r.f01,etc
				{
					oo = "0"+i;
					oo = oo.substr(oo.length-2,2); 
					//
					if(typeof(p[(z+oo)])!="undefined") r[(z+oo)] = p[(z+oo)];
					else break;
				}
			}
		}t = (required)(ALiBS.to)table
			g = (required)(number)field number [0-99]
			p = (optional)(JSON)parameter {}
				y = (string)field type [abcdefghijkrx] (default=f) 
		//
		return r; // record
	},*/
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.to.td = ALiBS.to.s2t; // reference - OLD compatibility (do NOT use var)
ALiBS.to.ts = ALiBS.to.t2s; // reference - OLD compatibility (do NOT use var)
//
if(ALiBS.gv.sy.ev=="Server")
{
	//ALiBS_to_td = ... ; // pointer (do NOT use var)
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.to("+(++ALiBS.gv.to.nn)+"x)",{o:"ALiBS.to"});
ALiBS.sy.la(0+0,"ALiBS.gv.to.rl="+ALiBS.gv.to.rl,{o:"ALiBS.to"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.to.le="+ALiBS.gv.to.le+ALiBS.gv.sy.se,{o:"ALiBS.to"});

