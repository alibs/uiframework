/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.c4) ALiBS.gv.c4 = {}; // global values
//
if(!ALiBS.gv.c4.le) ALiBS.gv.c4.le = 0; // local log status
//
ALiBS.gv.c4.nn = 0; // x times loaded
//
ALiBS.gv.c4.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.c4 = // replace international characters
{
	ri: function(s,p) // replace international characters
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c4.ri",le:ALiBS.gv.c4.le});
		//
		return ALiBS.c4.r2(ALiBS.c4.r1(ALiBS.c4.r0(s)));
	},
	// *****************************************************************
	r0: function(s,p) // 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c4.r0",le:ALiBS.gv.c4.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.a = new Array("ç","Ç","á","Á","à","À","â","Â","ä","Ä","ã","Ã","é","É","è","È","ê","Ê","ë","Ë","ẽ","Ẽ","í","Í","ì","Ì","î","Î","ï","Ï","ĩ","Ĩ","ó","Ó","ò","Ò","ô","Ô","ö","Ö","õ","Õ","ú","Ú","ù","Ù","û","Û","ü","Ü","ũ","Ũ","ń","Ń","ǹ","Ǹ","ñ","Ñ");
		//
		z.b = new Array("c","C","a","A","a","A","a","A","a","A","a","A","e","E","e","E","e","E","e","E","e","E","i","I","i","I","i","I","i","I","i","I","o","O","o","O","o","O","o","O","o","O","u","U","u","U","u","U","u","U","u","U","n","N","n","N","n","N");
		//
		z.n = new Array();
		//	
		for(var i=0;i<s.length;i++)
		{
			z.n[i]=s.charAt(i);
			//
			for(var j=0;j<z.a.length;j++)
			{
				if(s.charAt(i)==z.a[j])
				{
					z.n[i]=z.b[j];
					break;
				}
			}
		}
		//
		var r = z.n.join(""); // [BEST-PRACTICE]
		// 
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	r1: function(s,p) //
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c4.r1",le:ALiBS.gv.c4.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.a = "çÇáÁàÀâÂäÄãÃéÉèÈêÊëËẽẼíÍìÌîÎïÏĩĨóÓòÒôÔöÖõÕúÚùÙûÛüÜũŨńŃǹǸñÑ";
		z.b = "cCaAaAaAaAaAeEeEeEeEeEiIiIiIiIiIoOoOoOoOoOuUuUuUuUuUnNnNnN";
		z.n = new Array();
		//	
		for(var i=0;i<s.length;i++)
		{
			z.n[i]=s.charAt(i);
			//
			for(var j=0;j<z.a.length;j++)
			{
				if(s.charCodeAt(i)==z.a.charCodeAt(j))
				{
					z.n[i]=z.b[j];
					break;
				}
			}
		}
		//
		var r = z.n.join(""); // [BEST-PRACTICE]
		// 
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	r2: function(s,p) //
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c4.r2",le:ALiBS.gv.c4.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.a = new Array(231,199,225,193,224,192,226,194,228,196,227,195,233,201,232,200,234,202,235,203,7869,7868,237,205,236,204,238,206,239,207,297,296,243,211,242,210,244,212,246,214,245,213,250,218,249,217,251,219,252,220,	361,360,324,323,505,504,241,209);
		//
		z.b = "cCaAaAaAaAaAeEeEeEeEeEiIiIiIiIiIoOoOoOoOoOuUuUuUuUuUnNnNnN";
		z.n = new Array();
		//	
		for(var i=0;i<s.length;i++)
		{
			z.n[i]=s.charAt(i);
			//
			for(var j=0;j<z.a.length;j++)
			{
				if(s.charCodeAt(i)==z.a[j])
				{
					z.n[i]=z.b[j];
					break;
				}
			}
		}
		//
		var r = z.n.join(""); // [BEST-PRACTICE]
		// 
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	s0: ["ABCDEFGHIJKLMNOPQRSTUVWXYZ","48CD3F6H1JKLMN09QR57UVWXY2"], 
	// *****************************************************************
	e0: function(s0,p) // change letter by numbers
	// t0 = (required)(string)text
	//
	// p = (optional)(JSON)parameters {}
	//
	// 	en = (number)encode
	//		0 = DEFAULT= encode
	//		1 = decode
	//
	// return (string)text converted
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c4.e0",le:ALiBS.gv.c4.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.en)!="number") z.en = 0; // DEFAULT 
		//
		if(z.en==0) // encode
		{
			z.a = ALiBS.c4.s0[0];
			z.b = ALiBS.c4.s0[1];
		}
		else // decode
		{
			z.b = ALiBS.c4.s0[0];
			z.a = ALiBS.c4.s0[1];
		}
		//
		z.s0 = ALiBS.c4.ri(s0.toUpperCase());
		//
		z.n = new Array();
		//	
		for(var i=0;i<z.s0.length;i++)
		{
			z.n[i]=z.s0.charAt(i);
			//
			for(var j=0;j<z.a.length;j++)
			{
				if(z.s0.charCodeAt(i)==z.a.charCodeAt(j))
				{
					z.n[i]=z.b[j];
					break;
				}
			}
		}
		//
		var r = z.n.join(""); // [BEST-PRACTICE]
		// 
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c4("+(++ALiBS.gv.c4.nn)+"x)",{o:"ALiBS.c4"});
ALiBS.sy.la(0+0,"ALiBS.gv.c4.le="+ALiBS.gv.c4.le+ALiBS.gv.sy.se,{o:"ALiBS.c4"});

