/*
	TODO(2014-05-15):ALiBSx = somente ponteiros ou CUSTOM functions ?
*/
// *************************************************************************
// Alfredo - 2014-05-22 16:00
// *************************************************************************
//ALiBS.gv.Funcoes = {}; // global values
//ALiBS.gv.Funcoes.nn = 0; // x times loaded
//ALiBS.gv.Funcoes.le = 1; // local log status
// *************************************************************************
var rso = {}; // remover senhas opção
// *************************************************************************
function resetRso()
{
	rso = {}; // re-assign empty object
	//
	rso.evt0 = new Array(); // event senhas / opção
	rso.inh = new Array(); // inherited
};
// *************************************************************************
function printRso()
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.printRso",le:ALiBS.gv.Funcoes.le});
	//
	ALiBS.sy.la(1+0,"\n",{o:"Funcoes.printRso",ll:5000});
	//
	for(var i=0;i<rso.evt0.length;i++)
	{
		ALiBS.sy.la(1+0,"rso.evt0["+i+"]="+rso.evt0[i],{o:"Funcoes.printRso",ll:5000});
	}
	//
	ALiBS.sy.la(1+0,"\n",{o:"Funcoes.printRso",ll:5000});
	//
	for(var i=0;i<rso.inh.length;i++)
	{
		ALiBS.sy.la(1+0,"rso.inh["+i+"]="+rso.inh[i],{o:"Funcoes.printRso",ll:5000});
	}
	//
	ALiBS.sy.la(1+0,"\n",{o:"Funcoes.printRso",ll:5000});
};
// *************************************************************************
function setEvtSnh(evt)
// evt = event 
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.setEvtSnh",le:ALiBS.gv.Funcoes.le});
	//
	if((""+evt).indexOf("\\Level10\\Legado\\SENHAS")>0)
	{
		rso.evt0[rso.evt0.length] = evt;
		ALiBS.sy.la(1+0,"evt="+evt,{o:"Funcoes.setEvtSnh",ll:5000});
	}
	//
	return "";
};
// *************************************************************************
function setInh(inh)
// inh = inherited value 
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.setInh",le:ALiBS.gv.Funcoes.le});
	//
	ALiBS.sy.la(1+0,"\n",{o:"Funcoes.setInh",ll:5000});
	//
	if((""+inh).indexOf("\\Level10\\Legado\\SENHAS")>0)
	{
		inh = ALiBS.sg.bs(""+inh,"<assignment>");
		rso.inh[rso.inh.length] = inh;
		ALiBS.sy.la(1+0,"inh="+inh,{o:"Funcoes.setInh",ll:5000});
	}
	//
	return "";
};
// *************************************************************************
function setAss(ass)
// ass = assigned value 
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.setAss",le:ALiBS.gv.Funcoes.le});
	//
	var z = {}; // [BEST-PRACTICE]
	//
	z.ec = -1; // ec = start
	//
	var r = ""; // [BEST-PRACTICE]
	//
	try
	{
		z.ec = -1001; // 
		ass = ""+ass;
		//
		z.ec = -1110; // 
		ALiBS.sy.la(1,"(0)ass="+ass,{o:"Funcoes.setAss",ll:5000});
		//
		z.ec = -1111; // 
		if(ass.indexOf("\\Level10\\Legado\\SENHAS")>=0)
		{
			z.ec = -1141; // 
			for(var j=0;j<rso.inh.length;j++)
			{
				z.ec = -1151; // 
				if(ass.indexOf(rso.inh[j].substr(1,1000))>=0)
				{
					z.ec = -1160; // 
					ALiBS.sy.la(1,"(OK)ass="+ass,{o:"Funcoes.setAss",ll:5000});
					//
					r = ass;
					break;
				}
				else
				{
					z.ec = -1170; // 
					ALiBS.sy.la(1,"(NOT-indexOf)ass="+ass,{o:"Funcoes.setAss",ll:5000});
				}
			}
		}
		else
		{
			z.ec = -1120; // 
			ALiBS.sy.la(1,"(NOT-L10)ass="+ass,{o:"Funcoes.setAss",ll:5000});
		}
	}
	catch(e)
	{
		switch(z.ec)
		{
			case -1:z.m0="Generic Error";z.m1="";break;
			//case -2:z.m0="Parameter x must be a number";z.m1="";break;
			default:z.m0="Generic Default Error";z.m1="";break;
		}
		//
		r = ALiBS.sy.em(z.ec,"Funcoes.setAss",{m0:z.m0,m1:z.m1,m2:e}).m2;
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		throw r;
	}
	//
	z = null; // [BEST-PRACTICE]
	delete z; // [BEST-PRACTICE]
	//
	return r; 
};
// *************************************************************************
function getNS(ass) // get Name Space
// ass = assigned value 
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.getNS",le:ALiBS.gv.Funcoes.le});
	//
	return (""+ass).charAt(0);
};
// *************************************************************************
function getVol(ass) // get Volume
// ass = assigned value 
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.getVol",le:ALiBS.gv.Funcoes.le});
	//
	return ALiBS.sg.bs(""+ass,"<assignment>").substr(1,1000);
};
// *************************************************************************
function getPath(ass) // get Path
// ass = assigned value 
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.getPath",le:ALiBS.gv.Funcoes.le});
	//
	return ALiBS.sg.as(""+ass,"<assignment>",{d:true});
};
// *************************************************************************
function isSENHAS() //
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"Funcoes.isSENHAS",le:ALiBS.gv.Funcoes.le});
	//
	if(rso.evt0.length>0) return "1";
	else return "0";
};
// *************************************************************************
function ALiBSx_listInside(s0,li) 
{
	var z = {};
	//
	z.ll = li.split(",");
	//
	z.ok = false; // NOT FOUND
	//
	var r;
	//
	for(var i=0;i<z.ll.length;i++)
	{
		if(z.ll[i] && s0.indexOf(z.ll[i])>=0)
		{
//console.log(z.ll);
			z.ok = true; // FOUND
			break;
		}
	}
	//
	r = z.ok;
	//
	z = null;
	delete z;
	//
	return r;
};
// *************************************************************************
function ALiBSx_getCNfromSourceDN(DN)
{
	var r = DN.substring(DN.lastIndexOf("\\")+1)
	//
	return r;
};
// *************************************************************************
function ALiBSx_contains(a,b)
{
	var r = false;
	//
	if(!a || !b)
	{
		r = false;	
	}
	else if(a.indexOf(b)>=0)
	{
		r = true;	
	}
	else
	{
		r = false;			
	}
	//
	return r;
};
// *************************************************************************
function ALiBSx_not_contains(a,b)
{
	return !ALiBSx_contains(a,b);
};
// *************************************************************************
function valid_dept(s)
// s = (string)departamento
{
	var r = false;
	//
	if(!s || (s.length!=4))
	{
		r = false;
	}
	else if(isDig(s))
	{
		r = true;
	}
	else
	{
		r = false;
	}
	//
	return r;
};
// *************************************************************************
function isDig(s)
{
	var r = false;
	//
	for(var i=0;i<4;i++)
	{
		var c = s.charAt(i);
		//
		if((c < "0") || (c > "9"))
		{
			r = false;
			//
			break;
		}
		else
		{
			r = true;
		}
	}
	//
	return r;
};
// *************************************************************************
function valid_secao(s)
// s = (string)departamento
{
	var r = false;
	//
	if(!s || (s.length!=3))
	{
		r = false;
	}
	else
	{
		r = true;
	}
	//
	return r;
};
// *************************************************************************
//
//ALiBS.sy.la(0+0,"loaded module Funcoes("+(++ALiBS.gv.Funcoes.nn)+"x)",{o:"Funcoes"});
//ALiBS.sy.la(0+0,"ALiBS.gv.Funcoes.le="+ALiBS.gv.Funcoes.le+ALiBS.gv.sy.se,{o:"Funcoes"});

 
