/* *************************************************************************
// INFO:

- Miguel

	1) PF-RC-10-20-30
		- Somente vai entrar depois do upgrade, não misturar com o pacote do upgrade

	2) Instalação do iManager
		- O Cezar fez as anotações necessárias ?

	3) Modificações do GA Corporativo devem entrar no upgrade, ja vai entrar no dia 25 no IDM normal

	4) IDM2AL1_01 ????

	5) Lista de drivers do DS1 = para que servem ?
		- Remover driver Entitlements
		- Copiar do Leandro o workspace com todos os drivers de producao


	- Escrever DRAFT do Afastamentos

	- Escrever DRAFT do PBCC

	- Refazer lista das funcionalidades do loopback quando altera dependencia
		- [BUG] está removendo Ator Compliance

	- Enviar email para Cezar com descrição dos 3 scripts 10-20-30

// *************************************************************************
// TODO:

- ALiBS.ls = local storage

	- nome da variavel

		v0 = ALiBS.c7.sh(13,"abcd","1234567","app0000.T0"); // Super Hash = HMAC-SHA512
		v0 = ALiBS.c8.sh(13,"abcd","1234567","app0000.T0"); // Super Hash = HMAC-MD5
		v1 = ALiBS.c9.e4(v0,{n:"uri"});

		- usar hash
			- app0001.T0 = Tabela de Dados Completa
			- app0001.T0h = Tabela de Dados = header
			- app0001.T0d = Tabela de Dados = Data

			- app0001.TC0 = Tabela de Configuração Completa
			- app0001.TR0 = Tabela de Resultados Completa

	- valor
		- usar ALiBS.b0 = zip + cripto

*/
//
// *************************************************************************
//
if(!ALiBS.gv.ls) ALiBS.gv.ls = {}; // global values
//
if(!ALiBS.gv.ls.le) ALiBS.gv.ls.le = 0; // local log status
//
ALiBS.gv.ls.nn = 0; // x times loaded
//
ALiBS.gv.ls.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ls.rl = "15.0416.0001";
//
// *************************************************************************
//
ALiBS.ls = // local storage for ALiBS.to
{
	// *****************************************************************
	s0: function(p) // save table to local storage
	/** 
	Method <static> <number> ALiBS.xx.xx(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa
			pw = (required)(string) aaaa

			p = (optional)(JSON)parameter {}
				y = (string)field type [abcdefghijkrx] 
					f = Default
				rv = (boolean)reverse sort
					false = DEFAULT // TODO(2010-09-08): colocar o valor default em todas as descriçoes
				sh = (boolean)shuffle
					false = DEFAULT

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

			0 = [NOK] Bad Connection / Bad Credentials
			1 = [OK] Using New Connection
			2 = [OK] Using Old Connection

		Example(s):
			var ok = ALiBS.xx.xx();
			var ok = ALiBS.xx.xx();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - NOT to be disclosed
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			z.ec = -950; // 
			if(typeof(z.ho)!="string") throw "alibs.xx.xx.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			if(typeof(z.us)!="string") throw "alibs.xx.xx.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			if(typeof(z.pw)!="string") throw "alibs.xx.xx.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			//z.a0 = new Array();
			//
			z.ec = -9901; // -9902, -9903, etc
			if(i>1000) throw "[INFINITE-LOOP]"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			z.ec = -1002; // 
			//
			//_FORCE_ERROR_; // FORCE ERROR
			//
			//r = z.a.join(""); // [BEST-PRACTICE]
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.xx.xx",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE		Confidential Info - NOT to be disclosed
]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
		//
		return r; 
	},
	// *****************************************************************
	l0: function(p) // load table from local storage
	/** 
	Method <static> <number> ALiBS.xx.xx(p)

		Description: aaaa

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) aaaa
			pw = (required)(string) aaaa

			p = (optional)(JSON)parameter {}
				y = (string)field type [abcdefghijkrx] 
					f = Default
				rv = (boolean)reverse sort
					false = DEFAULT // TODO(2010-09-08): colocar o valor default em todas as descriçoes????
				sh = (boolean)shuffle
					false = DEFAULT

		Observations:
	
		Return Value:
			<static> <ALiBS.to.t> table object 

			0 = [NOK] Bad Connection / Bad Credentials
			1 = [OK] Using New Connection
			2 = [OK] Using Old Connection

		Example(s):
			var ok = ALiBS.xx.xx();
			var ok = ALiBS.xx.xx();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - NOT to be disclosed
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			z.ec = -950; // 
			if(typeof(z.ho)!="string") throw "alibs.xx.xx.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			if(typeof(z.us)!="string") throw "alibs.xx.xx.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			if(typeof(z.pw)!="string") throw "alibs.xx.xx.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			//z.a0 = new Array();
			//
			z.ec = -9901; // -9902, -9903, etc
			if(i>1000) throw "[INFINITE-LOOP]"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			z.ec = -1002; // 
			//
			//_FORCE_ERROR_; // FORCE ERROR
			//
			//r = z.a.join(""); // [BEST-PRACTICE]
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.xx.xx",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.xx.xx",le:ALiBS.gv.xx.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ls("+(++ALiBS.gv.ls.nn)+"x)",{o:"ALiBS.ls"});
ALiBS.sy.la(0+0,"ALiBS.gv.ls.rl="+ALiBS.gv.ls.rl,{o:"ALiBS.ls"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.ls.le="+ALiBS.gv.ls.le+ALiBS.gv.sy.se,{o:"ALiBS.ls"});

