/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

Implementar 
if(e.style.display) e.style.display = "none";
if(e.display) e.display = "none";

// ***************************

div.style.width = "400px";
div.style.height = "300px";
div.style.overflow = "auto";

div.style.bottom = "0px";
div.style.right = "0px";

div.style.opacity = "0.5";
div.style.filter = "alpha(opacity=50)";
div.setAttribute("id", "SpryDebugWindow");

*/
//
// *************************************************************************
//
if(!ALiBS.gv.st) ALiBS.gv.st = {}; // global values
//
if(!ALiBS.gv.st.le) ALiBS.gv.st.le = 0; // local log status
//
ALiBS.gv.st.nn = 0; // x times loaded
//
ALiBS.gv.st.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.st.rl = "14.0717.0001";
//
// *************************************************************************
//
ALiBS.gv.st.tb = {}; // tabs control
//
// *************************************************************************
//
ALiBS.st = // HTML Style
{
	st: function(i,s,v,p) // set style
	// i = element id OR element object reference
	// s = style attribute
	// v = value
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	//	display = (string) value
	//		block/none/inline = (block = DEFAULT)
	//
	//	c = (boolean)use cache (true = DEFAULT) 
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.st",le:ALiBS.gv.st.le});
		//
		var z = p || {};
		//
		if(typeof i == "string")
		{
			var e = ALiBS.el.ge(i,{c:z.c}); // element
		}
		else // object = element reference
		{
			var e = i; // element reference
		}
		//
		if(!e || !s) 
		{
			// não faz nada
		}
		else if(s=="style")
		{
			e.style = v;
		}
		else if(s=="display")
		{
			e.style.display = v;
			e.display = v;
		}
		else if(s=="hide") // funciona no div
		{
			if(typeof(z.display)=="undefined") z.display = "none"; // DEFAULT
			//
			e.style.display = z.display; // "", none
			e.display = z.display;
			//
			e.style.visibility = "hidden";
			e.visibility = 'hidden';
			//
			//e.display = 'hidden';
		}
		else if(s=="show") // funciona no div
		{
			if(typeof(z.display)=="undefined") z.display = "block"; // DEFAULT
			//
			e.style.display = z.display; // "", inline, block
			e.display = z.display;
			//
			e.style.visibility="visible";
			e.visibility = 'visible';
			//
			//e.display = 'visible';
		}
		else if(s=="enabled") // nao funciona no div, executar direto no elemento
		{
			e.style.disabled=""; // false
			e.disabled=""; // false
		}
		else if(s=="disabled") // nao funciona no div, executar direto no elemento
		{
			e.style.disabled="true"; 
			e.disabled="true";
		}
		else if(s=="color")
		{
			e.style.color = v; // string "green", "blue", "white"
			e.color = v;
		}
		else if(s=="backgroundColor")
		{
			e.style.backgroundColor = v; // string "green", "blue", "white"
			e.backgroundColor = v;
		}
		else if(s=="left")
		{
			e.style.position = "absolute";
			e.position = "absolute";
			e.style.left = v; // number (130) or string ("130px")
			e.left = v;
		}
		else if(s=="top")
		{
			e.style.position = "absolute";
			e.position = "absolute";
			e.style.top = v; // number (130) or string ("130px")
			e.top = v;
		}
		else if(s=="border")
		{
			e.style.border = v; // solid 1px black
			e.border = v;
		}
		else if(s=="width")
		{
			e.style.width = v; // 700px 
			e.width = v;
		}
		else if(s=="height")
		{
			e.style.height = v; // 313px
			e.height = v;
		}
		else if(s=="textDecoration") 
		{
			e.style.textDecoration = v; // 4em
			e.textDecoration = v;
		}
		else if(s=="fontSize")
		{
			e.style.fontSize = v; // 12px
			e.fontSize = v;
			//
			e.style.fontsize = v; // 12px
			e.fontsize = v;
			//
			//e.style.cssText+="font-size:" + v;
//console.log("i="+i);
//console.log("fontSize v="+v);
		}
		else if(s=="fontFamily")
		{
			e.style.fontFamily = v; // arial, roman
			e.fontFamily = v;
		}
		else if(s=="fontStyle")
		{
			e.style.fontStyle = v; // arial, roman
			e.fontStyle = v;
		}
		else if(s=="fontWeight")
		{
			e.style.fontWeight = v; // arial, roman
			e.fontWeight = v;
		}
		else if(s=="class")
		{
			e.className = v; 
		}
		else if(s=="width")
		{
			e.style.width = v; // 13 px
			e.width = v;
		}
		else if(s=="height")
		{
			e.style.height = v; // 13px
			e.height = v;
		}
		else if(s=="cols")
		{
			e.style.cols = v; // 13
			e.cols = v;
		}
		else if(s=="rows")
		{
			e.style.rows = v; // 13
			e.rows = v;
		}
		else
		{
			// não faz nada
		}
	},
	// *****************************************************************
	ts: function(n,i) // show tabset
	// n = (required)(String)tabset name ("tabSet1")
	// i = (required)(String)tab id = tab to show ("div0")
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.ts",le:ALiBS.gv.st.le});
		//
		var e;
		//
		e = ALiBS.el.ge(ALiBS.gv.st.tb[n]+"h");
		if(e) e["className"]="";
		//		
		ALiBS.st.st(ALiBS.gv.st.tb[n],"hide"); // old active tab		
		ALiBS.st.st(i,"show");	// show tab "i"
		ALiBS.gv.st.tb[n]=i; // new active tab "i"
		//
		e = ALiBS.el.ge(ALiBS.gv.st.tb[n]+"h");
		if(e) e["className"]="li_selected";
	},
	// *****************************************************************
	gmp: function(e)  // get mouse position
	// e = (optional)(event)event = use to pass event for firefox, other browsers do NOT need this (DO NOT USE window.event. If so, Firefox stops working)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.gmp",le:ALiBS.gv.st.le});
		//
		var m = {};
		//
		if(!e) 
		{
			e = window.event; 
			if(!e)
			{
				//alert("no window.event");
				return 0;
			}
		}
		//
		if(e.pageX || e.pageY) 	
		{
			m.x = e.pageX;
			m.y = e.pageY;
		}
		else if(e.clientX || e.clientY)
		{
			m.x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
			m.y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
		}
		//
		return m;
	},
	// *****************************************************************
	sep: function(e,i,p) // ANIMATION = set element position relating to mouse position
	// e = (required)(event)event = use to pass event for firefox, other browsers do NOT need this (DO NOT USE window.event. If so, Firefox stops working)
	// i = (required)(String)element id = id of element to animate
	// p = (optional)(JSON) parameters {xna:140,xnp:0,xma:1100,xmp:1190,xf:1.2}
	//	xna = (number)min position allowed
	//	xnp = (number)min position to set
	//	xma = (number)max position allowed
	//	xmp = (number)max position to set
	//	xf = (number)multiplication/division factor
	// return = no return
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.sep",le:ALiBS.gv.st.le});
		//
		var m = ALiBS.st.gmp(e);
		//
		var xna = 0;
		var xnp = 0;
		var xma = 0;
		var xmp = 0;
		var xf = 1;
		//
		var e0;
		//
		if(p)
		{
			if(typeof(p.xna)=="number") xna = p.xna;
			if(typeof(p.xnp)=="number") xnp = p.xnp;
			if(typeof(p.xma)=="number") xma = p.xma;
			if(typeof(p.xmp)=="number") xmp = p.xmp;
			if(typeof(p.xf)=="number") xf = p.xf;
		}
		//
		//m.x = xf * m.x;
		//
		//m.x = Math.floor(m.x * 1.3 - ((window.innerWidth - 936) / 2 + 8)); 
		//m.y = m.y; // ???? corrigir
		//
		e0 = ALiBS.el.ge("z1");
		e0.innerHTML = "x=" + m.x;
		//
		if(m.x<=xna)
		{
			m.x = xnp; 
		} 
		else if(m.x>=xma) 
		{
			m.x = xmp; 
		}
		else
		{
			//m.x = Math.floor(m.x * 1.3 - ((window.innerWidth - 936) / 2 + 8)); 
			//m.x = xf * m.x;
			//
			m.x = m.x - xna;
			//
			if(m.x<0)
			{
				m.x = 0;
			}
			else
			{
				m.x = m.x * xf/(xma-xna);
			}
		}
		//
		e0 = ALiBS.el.ge(i);
		e0.style.marginLeft = "-" + m.x + "px";
		//
		e0 = ALiBS.el.ge("z2");
		e0.innerHTML = "x=" + m.x;
		//
		e0 = ALiBS.el.ge("z3");
		e0.innerHTML = "calc=" + xf/(xma-xna);
	},
	// *****************************************************************
	xxxx_ts: function(o,n,i) // show tabset
	// s = (required)(Object)object reference (A.tabs)
	// n = (required)(String)tabset name ("tabSet1")
	// i = (required)(String)tab id = tab to show ("div0")
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.xxxx_ts",le:ALiBS.gv.st.le});
		//
		ALiBS.st.st(o[n],"hide"); // old active tab		
		ALiBS.st.st(i,"show");	// show tab "i"
		o[n]=i; // new active tab "i"
	},
	// *****************************************************************
	// descontinuado
	xxxxss: function(i,s) // set style // TODO: Deprecated
	// i = element id
	// s = style
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.xxxxss",le:ALiBS.gv.st.le});
		//
		if(document.getElementById) // DOM3 = IE5, NS6
		{ 
			var e = ALiBS.el.ge(i);
			e.style.display = s;
		}
		else if(document.layers) // Netscape 4
		{ 
			var e = ALiBS.el.ge(i);
			e.display = s;
		}
		else // IE 4
		{ 
			var e = ALiBS.el.ge(i);
			document.all.e.style.display = s;
		}
	},
	// *****************************************************************
	// descontinuado
	xxxxsc: function(i,s) // set color // TODO: Deprecated
	// i = element id
	// s = style
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.st.xxxxsc",le:ALiBS.gv.st.le});
		//
		if(document.getElementById) // DOM3 = IE5, NS6
		{ 
			ALiBS.lg.wr(5,"opt=1\n");
			var e = ALiBS.el.ge(i);
			e.style.color = s;
		}
		else if(document.layers) // Netscape 4
		{ 
			ALiBS.lg.wr(5,"opt=2\n");
			var e = ALiBS.el.ge(i);
			e.color = s;
		}
		else // IE 4
		{ 
			ALiBS.lg.wr(5,"opt=3\n");
			var e = ALiBS.el.ge(i);
			document.all.e.style.color = s;
		}
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.st("+(++ALiBS.gv.st.nn)+"x)",{o:"ALiBS.st"});
ALiBS.sy.la(0+0,"ALiBS.gv.st.le="+ALiBS.gv.st.le+ALiBS.gv.sy.se,{o:"ALiBS.st"});

