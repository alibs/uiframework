/* *************************************************************************
// INFO:

setInterval()
   -> executes a function, over and over again, at specified time intervals  

setTimeout()
   -> executes a function, once, after waiting a specified number of milliseconds


https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Operators/Special_Operators/typeof_Operator

typeof returns one of the following strings:

    * number
    * string
    * boolean
    * undefined 

    * object (NOT cross-browser-compatible)
    * function (NOT cross-browser-compatible)

typeof(typeof(x)) is always string, no matter what x actually is.
IE seems to think that some functions are objects rather than functions: typeof(window.document.getElementById) returns object. 

// ***************************

	// p = (optional)(JSON)parameters {}
	//
	// vR = (number)value to return
	//	0 = DEFAULT / PREFERED = return primitive values (string,number,boolean,Array,object EXCEPT ALiBS.sy.em(0,"",{va:10}) )
	//	3 = (object)return ALiBS.sy.em(0,"",{va:10}) // [OK] (va: can be string,number,boolean,Array,object EXCEPT ALiBS.sy.em(0,"",{va:10}) )
	//
	// eR = (number)ERROR in function return control
	//	2 = DEFAULT (number) = return negative number
	//		if( !(r<0) ) do_func();
	//		else erro
	//	3 = (object)return ALiBS.sy.em() error code object
	//		if( !(va.ec<0) ) do_func();
	//		else erro
	//	4 = PREFERED (exception)throw exception  = throw ALiBS.sy.em(-1,"ALiBS.to.s",{m0:"parameter x is not-a-number",m1:"null"}).m2;
	//
	// pR = (number)ERROR in parameter validation return control
	//	0 = adequate value, do NOT return error if possible
	//		if NOT possible, throw exception (4)
	//	2 = DEFAULT (number) = return negative number
	//		if( !(r<0) ) do_func();
	//		else erro
	//	3 = (object)return ALiBS.sy.em() error code object
	//		if( !(va.ec<0) ) do_func();
	//		else erro
	//	4 = PREFERED (exception)throw exception  = throw ALiBS.sy.em(-1,"ALiBS.to.s",{m0:"parameter x is not-a-number",m1:"null"}).m2;
	//
	// if return is UNKNOWN
	//	if( !(!va || !va.ec || !(va.ec<0)) ) return va.m0+va.m1;
	//	else do_func();
	//		OR
	//	if( !ALiBS.sy.ec(va) ) do_func();
	//	else erro

// ***************************

IDEAL

	// p = (optional)(JSON)parameters {}
	//
	// vR = (number)value to return
	//	0 = DEFAULT / PREFERED = return primitive values (string,number,boolean,Array,object EXCEPT ALiBS.sy.em(0,"",{va:10}) )
	//
	// eR = (number)ERROR in function return control
	//	4 = PREFERED (exception)throw exception  = throw ALiBS.sy.em(-1,"ALiBS.to.s",{m0:"parameter x is not-a-number",m1:"null"}).m2;
	//
	// pR = (number)ERROR in parameter validation return control
	//	0 = adequate value, do NOT return error if possible
	//		if NOT possible, throw exception (4)
	//	4 = PREFERED (exception)throw exception  = throw ALiBS.sy.em(-1,"ALiBS.to.s",{m0:"parameter x is not-a-number",m1:"null"}).m2;

// ***************************

ERROR USAGE:

1) Do no return exceptions, handle every possible error.
2) If possible, return an adequate value (String, Array, Object, number, boolean, etc)
3) Return negative error codes for methods that normally 
	- return positive values (ex. ALiBS.to.f())
	- return String/Objects
	- return boolean ????
	- what for methods that normally return negative values ????
		- exemplos: ????

validation:
	if( !(r<0) ) 

- For ALiBS methods, only parameter validations return errors.
- Returns inside the method are not errors, they all are possible results (if negative OR null).
- These returns can be error for the caller, so CALLERs should test if error.

// ***************************

Valid Returns that are NOT errors (if not validating parameters)

- (null)
- ("") (empty)
- (-1,-2) (negative values)
- false (boolean)

// ***************************

Validating parameters (always use typeof())

var a = "abc"; // default
var b = 0; // default
//
if(p) // do NOT use so if p can be (0,"") and you want to overwrite (a="" )(b=0)
{
	if(typeof(p.a)=="string") a = p.a;
	//
	if(typeof(p.b)=="number" && p.b>=0 && p.b<=10) b = p.b;
}

// ***************************

Handling if(x) and if(!x)

if(x) is always false if x = (0,false,"",null,undefined)

if need to handle return where x = (0,"") are valid returns (ALiBS.to.f(),ALiBS.sg.tr())

	if(x!=null && x==0)
	if(x!=null && x=="")
	if(x!=null && x==false)
	if(x!=null && x!=true)

if(x!=null) means x != (null,undefined)

// ***************************

ALiBS.00.js (Stress test)

// I should know
// do not use if(x), preferably use
//
// if(!x) when x is defined (to test local variable)(NOT root var)(works with ALiBS.x)(does NOT work with ALiBS)
// if(typeof(x)=="undefined" || !x) (to test global variables)
//
// if(typeof(x)!="undefined" && x!=null && x==0) (to test 0)
// if(x!=null && x==0) (to test 0)
//
//	!x means
// 1) x==undefined
// 2) x==[not inicialized] 
// 3) x==null 
// 4) x==0 
// 5) x=="" 
// 6) x==false

// ***************************

LOG USAGE:

<script>
	var ALiBS = {}; 
	//
	ALiBS_gv_sy_le = 1;
	ALiBS_sy_gl = 7;
	ALiBS_sy_li = "ALiBS.cp.bc12,ALiBS.cp.bc13";
</script>

<script type="text/javascript" charset="utf-8" src="../../ALiBS/ALiBS-lib/ALiBS.sy.js"></script>

<script>
	var P = new ALiBS.ap();
	P.gv = {}; // global values 
	//
	P.ap.re = function() // custom onPageRender
	{
		P.ap._mod[P.ap._mod.length] = "ALiBS.lg"; // only if REQUIRED
	};
	//
	ALiBS.sy.la(0,"loaded module description_name_id.html"+ALiBS.gv.sy.se,{o:"ap.js"});
	//
	P.ap.ex(); // execute page
</script>

<div id="iALiBS_log0" class="log"></div>

// *************************************************************************

// Browser Cache

ALiBS.gv.sy[.][r]l = "0010"; // ALiBS release 

ALiBS.gv.sy.mc["ALiBS.sy"].f01:true // FORCE RELOAD used in ALiBS.ag.lm()

Configuration of Browsers Cache Control
---------------------------------------

MSIE = Tools / Internet Options / General / Settings / Check for newer versions of pages: Every visit to the page
Opera = Tools / Preferences / Advanced / History / Check for Documents: Always
Firefox, Chrome = No control
Konqueror = Settings / Configure Konqueror / Cache / Keep cache in sync

Opções

<%  
   response.setHeader("Cache-Control","no-cache"); //HTTP 1.1  
   response.setHeader("Pragma","no-cache"); //HTTP 1.0  
   response.setDateHeader ("Expires", 0); //prevents caching  
   response.setHeader("Cache-Control","no-store"); //HTTP 1.1  
%> 
 
Outra alternativa
 
html>
<head>
<title>Bradesco - Intranet </title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 

// *************************************************************************

- TODO(2013-07-08): URGENTE
	- 

- TODO(2013-07-08): Sem Urgência

	hl/hr = log to HTML element OR render HTML - browser
	cl = log to command line - ejs, v8, spidermonkey
	co = log to browser console

*/
//
// *************************************************************************
/** 
	Class ALiBS.sy

		Description: 
			- System Module;
			- Required Class for All Other Classes;
			- Creates ALiBS Namespace;
			- Modules Name Cache;
			- Log Management;

	--------------------------------------------------------------------
*/
// *************************************************************************
if(typeof(ALiBS)=="undefined" || !ALiBS) // ALiBS NOT defined
{
	var ALiBS = {}; //! ALiBS Namespace
	//
	// Browser always create 
	//	ALiBS 
	//	window.ALiBS
	//	windows.parent.ALiBS
}
// *************************************************************************
if(typeof(ALiBS.gv)=="undefined" || !ALiBS.gv) // should exist only here
{
	ALiBS.gv = {}; // global values
}
// *************************************************************************
//
if(!ALiBS.sy) // Avoid loading again
{
//
// *************************************************************************
//
ALiBS.gv.er = {}; // error global values // TODO(2011-07-24): remover depois de implementar er()
//
ALiBS.gv.er.em = ""; // last error message // TODO(2011-07-24): remover depois de implementar er()
//
// *************************************************************************
//
if(!ALiBS.gv.sy) ALiBS.gv.sy = {}; // global values
//
if(!ALiBS.gv.sy.le) ALiBS.gv.sy.le = 0; // local log status
//
// ALiBS release - SHOULD return to "0000" each time ALiBS version changes
// Used to FORCE URL reload (js files) when release changes
//
ALiBS.gv.sy.rl = "15.0731.1412"; // (Modified) ALiBS
//
//ALiBS.gv.sy.rl = "14.0411.0002"; // (Modified:2014-04-02)(GMUD-Bradesco:2014-04-11)
//ALiBS.gv.sy.rl = "14.0214.0001"; // (Modified:2014-02-02)(GMUD-Bradesco:2014-02-14)
//ALiBS.gv.sy.rl = "13.1122.0033"; // (Modified:2013-11-08)(GMUD-Bradesco:2013-11-08)
//ALiBS.gv.sy.rl = "13.1029.0032"; // (Modified:2013-10-29)(GMUD-Bradesco:2013-10-29)
//ALiBS.gv.sy.rl = "13.1025.0031"; // (Modified:2013-10-25)(GMUD-Bradesco:2013-10-25)
//ALiBS.gv.sy.rl = "13.0726.0032"; // ALiBS
//
// *************************************************************************
//
ALiBS.gv.sy.gl = new Array(); // global log array
//
ALiBS.gv.sy.gla = 0; // 0 = Auto Print / Clean , 1 = Auto Reduce
//
ALiBS.gv.sy.glx = 1000; // max records in log Array ALiBS.gv.sy.gl to start reducing (trigger)
//
ALiBS.gv.sy.glu = 200; // max messages to keep 
//
ALiBS.gv.sy.gle = 200; // max ERRORs to keep 
//
ALiBS.gv.sy.gll = 100; // max length of log string line
//
// *************************************************************************
//
ALiBS.gv.sy.se = "\n"; // (DEFAULT) separator for js.jar, firefox, chrome, opera (handle \n and \r )
//
//if(typeof(window)!="undefined" && typeof(window.navigator)!="undefined" && window.navigator.userAgent.indexOf("MSIE")>=0) ALiBS.gv.sy.se = "\r"; // MSIE do NOT handle \n, only \r
//
//
if(typeof(window)!="undefined" && typeof(window.navigator)!="undefined")
{
	ALiBS.gv.sy.ev = "Browser"; // HTML client
	//
	// *******************
	//
	if(window.navigator.userAgent.indexOf("MSIE")>=0)
	{
		ALiBS.gv.sy.se = "\r"; // MSIE do NOT handle \n, only \r
	}
}
else if(typeof(java)!="undefined")
{
	ALiBS.gv.sy.ev = "Server"; // JAVA js.jar
	//
	// *****************************************************************
	//
	// Load Java Modules (used by Rhino Command Line)
	//
	// Rhino command line = use importPackages
	// Rhino web (Shell) = DO NOT use importPackages = load all lib by default
	// Novell IDM Drivers = DO NOT use importPackages = load all lib by default
	//
	// ALiBS-pre - ONLY when using Rhino Command Line - ONLY the needed Java Packages
	//
	// ALiBS.gv.sy.importJava = "Packages.com.novell.ldap","Packages.javax.naming.directory";
	//
	//
	if(typeof(ALiBS.gv.sy.importJava)!="undefined")
	{
		try
		{
			if(typeof(importClass)!="undefined")
			{
				if(ALiBS.gv.sy.importJava.indexOf("java.lang.Thread")>=0) importClass(java.lang.Thread); // TODO(2012-03-05): testar Web+cmd
				//
				if(ALiBS.gv.sy.importJava.indexOf("java.io")>=0) importClass(java.io); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.BufferedReader")>=0) importClass(java.io.BufferedReader); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.FileReader")>=0) importClass(java.io.FileReader); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.BufferedWriter")>=0) importClass(java.io.BufferedWriter); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.FileWriter")>=0) importClass(java.io.FileWriter); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.InputStream")>=0) importClass(java.io.InputStream); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.File")>=0) importClass(java.io.File); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("java.io.DataInputStream")>=0) importClass(java.io.DataInputStream); // TODO(2012-03-05): testar Web+cmd
				//
				if(ALiBS.gv.sy.importJava.indexOf("java.net.URL")>=0) importClass(java.net.URL); // TODO(2012-03-05): testar Web+cmd
				//
				if(ALiBS.gv.sy.importJava.indexOf("java.lang.Runtime")>=0) importClass(java.lang.Runtime); // OK 2012-03-05
				if(ALiBS.gv.sy.importJava.indexOf("java.lang.System")>=0) importClass(java.lang.System); // 
			}
			//
			if(typeof(importPackage)!="undefined")
			{
				if(ALiBS.gv.sy.importJava.indexOf("Packages.javax.servlet.ServletConfig")>=0) importPackage(Packages.javax.servlet.ServletConfig);  // TODO(2012-03-05): testar Web+cmd
				//
				if(ALiBS.gv.sy.importJava.indexOf("Packages.java.sql")>=0) importPackage(Packages.java.sql); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("Packages.org.h2")>=0) importPackage(Packages.org.h2); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("Packages.org.sqlite")>=0) importPackage(Packages.org.sqlite); // TODO(2012-03-05): testar Web+cmd
				//
				if(ALiBS.gv.sy.importJava.indexOf("Packages.com.novell.ldap")>=0) importPackage(Packages.com.novell.ldap); // TODO(2012-03-05): testar Web+cmd
				if(ALiBS.gv.sy.importJava.indexOf("Packages.javax.naming.directory")>=0) importPackage(Packages.javax.naming.directory); // OK 2012-03-05
			}
			//
			ALiBS.sy.la(1+0,ALiBS.gv.sy.importJava + " loaded",{o:"ALiBS.sy"});
		} 
		catch(e)
		{
			ALiBS.sy.la(1+0,"ALiBS.sy.Exception:("+ALiBS.gv.sy.importJava+"):"+e,{o:"ALiBS.sy"});
		} 
	}
	//
	// *****************************************************************
	//
	ALiBS.gv.sy.lgrs = function() // redefine / reset log file
	{
		//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.gv.sy.lgrs",le:ALiBS.gv.sy.le}); // TODO(2014-06-27):EXCLUDE THIS in ALiBS.cp
		//
		if(ALiBS.gv.sy.logFile) // exists = NOT undefined = CUSTOM Log File
		{
			ALiBS.gv.sy.fileH = new java.io.PrintStream(new java.io.File(ALiBS.gv.sy.logFile),"UTF-8"); // funciona UTF-8
			//
			//ALiBS.gv.sy.outfile = new java.io.PrintStream(new java.io.File(ALiBS.gv.sy.logFile),"UTF-8"); // funciona UTF-8
			//ALiBS.gv.sy.outfile = new java.io.PrintStream(new java.io.FileOutputStream(ALiBS.gv.sy.logFile)); // funciona sem UTF-8
			//ALiBS.gv.sy.outfile = new java.io.PrintStream(new java.io.FileOutputStream(ALiBS.gv.sy.logFile),"UTF-8"); // nao funciona com UTF-8
			//
			java.lang.System.setOut(ALiBS.gv.sy.fileH); // file Handdler - set system out
			//
			return 1;
		}
		else // ALiBS.gv.sy.logFile NOT defined = DEFAULT = NATIVE Log File
		{
			return -1;
		}
	};
	//
	ALiBS.gv.sy.lgpt = function(a) // print to log file
	{
		//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.gv.sy.lgpt",le:ALiBS.gv.sy.le}); // TODO(2014-06-27):EXCLUDE THIS in ALiBS.cp
		//
		java.lang.System.out.println(""+a);
		//
		return 1;
	};
	//
	// *******************
	//
	if(typeof(ALiBS.gv.sy.logFile)=="undefined") // NATIVE Log File
	{
		// print to system.out (tomcat/catalina.log, jboss/server.log)
	}
	else // CUSTOM Log File
	{
		// print to CUSTOM file 
		// redefine / reset needed to run setOut()
		ALiBS.gv.sy.lgrs(); // reset log file
	}
	//
	// *******************
	//
	if(typeof(print)=="undefined" || !print) // native function print does NOT exist = NOT loaded claypool = (Novell drivers, others?)
	{
		print = ALiBS.gv.sy.lgpt; // print to log
	}
	//
	// *******************
	//
}
else // ????
{
	ALiBS.gv.sy.ev = "Other"; // Other ????
}
//
// *************************************************************************
//
//ALiBS.gv.sy.li = 0; // last index shown TODO: onde é usado? em stress-test? deprecated in 2011-08-08
//
ALiBS.gv.sy.so = ""; // standard output (js.jar=print) 
//	"" = DEFAULT = do NOT print/log
//	"immediate-console" = imediate log to console = console.log() = no cache
//	"immediate-print" = imediate print to file (Server) = no cache
//	"print" = FORCE print/log 
//
// *************************************************************************
//
ALiBS.gv.sy.cc = {}; // css name cache
//
//ALiBS.gv.sy.ccd = {}; // loaded css name cache
//
//ALiBS.gv.sy.hp = ((typeof A!="undefined") && A.gv.hp) || "/home/dados1/myDocsPs/_13_js_/"; // ALiBS skin home path
//
//ALiBS.gv.sy.sp = ((typeof A!="undefined") && A.gv.sp) || "ALiBS/ALiBS-s00/"; // ALiBS skin name path //TODO(2011-08-02): vai ser neste caminho ou em ap?
//
// Mapping of ALiBS CSS 
/*
ALiBS.gv.sy.cc["ALiBS.mn00"] = 
	{
		f00:"ALiBS.sk00-mn00.css",
		f01:0
	};
*/
//
//
//
ALiBS.gv.sy.sp = ((typeof A!="undefined") && A.gv.sp) || "/home/dados1/myDocsPs/_13_js_/ALiBS/ALiBS-s00/"; // ALiBS skin name path 
//
if(typeof(ALiBS_gv_sy_sp)!="undefined") ALiBS.gv.sy.sp = ALiBS_gv_sy_sp;
//
// Mapping of ALiBS CSS 
ALiBS.gv.sy.cc["ALiBS.mn00"] = 
	{
		f00: ALiBS.gv.sy.sp + "ALiBS.s00-mn00.css",
		f01:0
	};
//
// *************************************************************************
//
ALiBS.gv.sy.lc = {}; // languages name cache
//
ALiBS.gv.sy.lp = ((typeof A!="undefined") && A.gv.lp) || "/home/dados1/myDocsPs/_13_js_/ALiBS/ALiBS-i18n/"; // ALiBS languages path 
//
if(typeof(ALiBS_gv_sy_lp)!="undefined") ALiBS.gv.sy.lp = ALiBS_gv_sy_lp;
//
// Mapping of ALiBS Languages
ALiBS.gv.sy.lc["ALiBS.it.pt_br_ALiBS"] = ALiBS.gv.sy.lp + "ALiBS.it-pt_br_ALiBS.js";
ALiBS.gv.sy.lc["ALiBS.it.en_us_ALiBS"] = ALiBS.gv.sy.lp + "ALiBS.it-en_us_ALiBS.js";
ALiBS.gv.sy.lc["ALiBS.it.es_es_ALiBS"] = ALiBS.gv.sy.lp + "ALiBS.it-es_es_ALiBS.js"; 
//
ALiBS.gv.sy.lc["ALiBS.it.pt_br_ap"] = "./i18n/it-pt_br_ap.js"; // application custom words - should be replace in P.ap.re
ALiBS.gv.sy.lc["ALiBS.it.en_us_ap"] = "./i18n/it-en_us_ap.js"; // application custom words - should be replace in P.ap.re
ALiBS.gv.sy.lc["ALiBS.it.es_es_ap"] = "./i18n/it-es_es_ap.js"; // application custom words - should be replace in P.ap.re
//
ALiBS.gv.sy.ln = {}; // language names
//
ALiBS.gv.sy.ln["ALiBS.it.pt_br"] = "Portugu"+ String.fromCharCode(234) +"s"; // Português - String.fromCharCode(234) = ê
ALiBS.gv.sy.ln["ALiBS.it.en_us"] = "English";
ALiBS.gv.sy.ln["ALiBS.it.es_es"] = "Espa" + String.fromCharCode(241) + "ol"; // Español - String.fromCharCode(241) = ñ
//
// *************************************************************************
//
ALiBS.gv.sy.ALiBSvs = ALiBS.gv.sy.rl; // for compatibility
//
//ALiBS.gv.sy.ALiBSvs = /ALiBS\.[0-9]{2}\.[0-9]{1}[a-z]{0,1}/; // first occurance, sensitive (ALiBS.vs regular expression)
//
// *************************************************************************
//
// Mapping of ALiBS Modules
//
ALiBS.gv.sy.mc = {}; // modules name cache
//
ALiBS.gv.sy.mp = ((typeof A!="undefined") && A.gv.mp) || "/home/dados1/myDocsPs/_13_js_/ALiBS/ALiBS-lib/"; // ALiBS modules path
//
if(typeof(ALiBS_gv_sy_mp)!="undefined") ALiBS.gv.sy.mp = ALiBS_gv_sy_mp;
//
// for(var i in ALiBS.gv.sy.mc) console.log(ALiBS.gv.sy.mc[i].f00+" : "+ALiBS.gv.sy.mc[i].f04+" : "+ALiBS.gv.sy.mc[i].f03)
//
ALiBS.gv.sy.mc["ALiBS.sy"] = // CODE // Optional: dt,sg(log)		(sy.la)->(dt.ts)   (sy.ga)->(sg.ca)  
	{
		f00:"ALiBS.sy.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"", // dependencies
		f03:"(Client/Server) ALiBS System Required", // description
		f04:4 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed(html+java)
	};
//
ALiBS.gv.sy.mc["ALiBS.ag"] = // HTML // 
	{
		f00:"ALiBS.sy.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"", // dependencies
		f03:"(Client/Server) ALiBS System Required", // description
		f04:4 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};

//
ALiBS.gv.sy.mc["ALiBS.ap"] = //
	{
		f00:"ALiBS.sy.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"", // dependencies
		f03:"(Client/Server) ALiBS System Required", // description
		f04:4 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.ob"] = 
	{
		f00:"ALiBS.sy.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"", // dependencies
		f03:"(Client/Server) ALiBS System Required", // description
		f04:4 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.ax"] = // HTML // 
	{
		f00:"ALiBS.sy.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"", // dependencies
		f03:"(Client/Server) ALiBS System Required", // description
		f04:4 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
// *************************************************************************
//
ALiBS.gv.sy.mc["ALiBS.b0"] = 
	{
		f00:"ALiBS.b0.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.b1"] = 
	{
		f00:"ALiBS.b1.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.b2"] = 
	{
		f00:"ALiBS.b2.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,c9.e4,", // dependencies
		f03:"", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.bn"] = 
	{
		f00:"ALiBS.bn.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.c0"] = 
	{
		f00:"ALiBS.c0.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Cryptografy - AES", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.c1"] = 
	{
		f00:"ALiBS.c1.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) EBCDIC Conversion", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.c2"] = //      // Dep: none 
	{
		f00:"ALiBS.c2.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) String to Decimal Array ", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.c3"] = 
	{
		f00:"ALiBS.c3.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Cryptografy - DES and TRIPLE DES", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.c4"] = 
	{
		f00:"ALiBS.c4.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Replace International Characters", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.c6"] = 
	{
		f00:"ALiBS.c6.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) UTF-8 Encode/Decode", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.c7"] = 
	{
		f00:"ALiBS.c7.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Hash SHA-512", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.c8"] = 
	{
		f00:"ALiBS.c8.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Hash MD5", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.c9"] = 
	{
		f00:"ALiBS.c9.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,c6", // dependencies
		f03:"(Client/Server) Base Conversion", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.ch"] = 
	{
		f00:"ALiBS.ch.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"???? NOT USED ?", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.ck"] = 
	{
		f00:"ALiBS.ck.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,to", // dependencies
		f03:"(Client) Cookies", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.cp"] = 
	{
		f00:"ALiBS.cp.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) Html Compact/Verify ALiBS Modules", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};   
//
ALiBS.gv.sy.mc["ALiBS.dt"] = 
	{
		f00:"ALiBS.dt.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,sg.rs->dt.s2j;dt.df;dt.dw", // dependencies
		f03:"(Client/Server) Date", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.el"] = 
	{
		f00:"ALiBS.el.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) Html Elements", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.ev"] = 
	{
		f00:"ALiBS.ev.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) Browser Environment", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.hp"] = 
	{
		f00:"ALiBS.hp.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	}; 
//
ALiBS.gv.sy.mc["ALiBS.fs"] = 
	{
		f00:"ALiBS.fs.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};        
//
ALiBS.gv.sy.mc["ALiBS.gg"] = 
	{
		f00:"ALiBS.gg.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};        
//
ALiBS.gv.sy.mc["ALiBS.hg"] = // HTML // Dep: to
	{
		f00:"ALiBS.hg.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,el.ge,st.st", // dependencies
		f03:"(Client) Html Grid", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};        
//
ALiBS.gv.sy.mc["ALiBS.hm"] = // HTML //
	{
		f00:"ALiBS.hm.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) Html Tree Menu", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.it"] = // HTML // Dep: ev
	{
		f00:"ALiBS.it.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) i18n - Internationalization", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.ix"] = 
	{
		f00:"ALiBS.ix.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.lg"] = 
	{
		f00:"ALiBS.lg.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,sg,el,st", // dependencies
		f03:"(Client) Html ALiBS Log", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	}; 
//
ALiBS.gv.sy.mc["ALiBS.n0"] = 
	{
		f00:"ALiBS.n0.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"Novell 0", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.n1"] = 
	{
		f00:"ALiBS.n1.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"Novell 1", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.op"] = 
	{
		f00:"ALiBS.op.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Options", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.pp"] = 
	{
		f00:"ALiBS.pp.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Properties", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};  
//
ALiBS.gv.sy.mc["ALiBS.rx"] = 
	{
		f00:"ALiBS.rx.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Regular Expression", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.sb"] = 
	{
		f00:"ALiBS.sb.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) String Buffer", // description
		f04:0 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.sg"] = 
	{
		f00:"ALiBS.sg.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) String", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.st"] = // HTML //
	{
		f00:"ALiBS.st.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) Html Style", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.to"] = 
	{
		f00:"ALiBS.to.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,sg.id,sg.tr,sg.is", // dependencies
		f03:"(Client/Server) Table Object", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.tp"] = 
	{
		f00:"ALiBS.tp.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) Create Table SQL Command", // description
		f04:1 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.tk"] = 
	{
		f00:"ALiBS.tk.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) Html Token", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.ui"] = 
	{
		f00:"ALiBS.ui.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) HTML User Interface", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["JQueryMobile.ui"] = 
	{
		f00:"JQueryMobile.ui.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client) jQuery Mobile HTML User Interface", // description
		f04:2 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.wa"] = 
	{
		f00:"ALiBS.wa.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Server) Authentication", // description
		f04:3 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.wd"] = 
	{
		f00:"ALiBS.wd.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Server) Database", // description
		f04:3 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.wf"] = 
	{
		f00:"ALiBS.wf.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Server) File System", // description
		f04:3 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.wi"] = 
	{
		f00:"ALiBS.wi.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Server) Java Memory Information", // description
		f04:3 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.wl"] = // ldap
	{
		f00:"ALiBS.wl.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,sg.tr,to.t,to.un,to.ts", // dependencies
		f03:"(Client/Server) LDAP", // description
		f04:4 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
ALiBS.gv.sy.mc["ALiBS.wo"] = // soap
	{
		f00:"ALiBS.wo.js", // file name
		f01:false, // FORCE RELOAD used in ALiBS.ag.lm()
		f02:"sy,", // dependencies
		f03:"(Client/Server) SOAP", // description
		f04:3 // 0=Not-Set, 1=client-server-code, 2=client-html, 3=server-java, 4=client-server-mixed
	};
//
// *************************************************************************
//
ALiBS.gv.sy.kc = new Array(); // pressed keys Array
//
ALiBS.gv.sy.sq = new Array(); // functions Array
//
ALiBS.gv.sy.kcmx = 150; // 150 = DEFAULT max lenght of kc
//
// alt+ctrl+enter+esc+keys = show keys
ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length] = 
{
	nm:"keys[alt+ctrl+enter+esc+keys]",
	sq:"18,17,13,27,75,69,89,83",
	p: // just to test parameter passing
	{
		//run: 1, // 1 = keep running = DEFAULT
		//rkc: 1, // 1 = reset kc = DEFAULT
		p0:1,
		p1:2
	}, 
	oc:function(p)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:ALiBS.gv.sy.sq[p.ix].nm,le:ALiBS.gv.sy.le});
		//
		var r = prompt("OTP:({"+"p0:"+p.p0+"p1:"+p.p1+"})");
		if(r=="1347")
		{
			alert("Pressed Keys:\n"+ALiBS.gv.sy.kc.join());
		}
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:ALiBS.gv.sy.sq[p.ix].nm,le:ALiBS.gv.sy.le});
		//
		return false;
	}
}; 
// alt+ctrl+enter+esc+seqs = show seqs
ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length] = 
{
	nm:"seqs[alt+ctrl+enter+esc+seqs]",
	sq:"18,17,13,27,83,69,81,83",
	p: // just to test parameter passing
	{
		p0:3,
		p1:4
	}, 
	oc:function(p)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:ALiBS.gv.sy.sq[p.ix].nm,le:ALiBS.gv.sy.le});
		//
		var r = prompt("OTP:({"+"p0:"+p.p0+"p1:"+p.p1+"})");
		if(r=="1347")
		{
			var a = new Array();
			//
			for(var i=0;i<ALiBS.gv.sy.sq.length;i++)
			{
				a[a.length] = ALiBS.gv.sy.sq[i].nm;
			}
			//
			alert("Registered Sequences:\n"+a.join("\n"));
		}
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:ALiBS.gv.sy.sq[p.ix].nm,le:ALiBS.gv.sy.le});
		//
		return false;
	}
}; 
//
// to generate number sequence, use ALiBS.c2.sa("ALF")
//
// alt+ctrl+enter+esc+menu
//ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length] = {nm:"menu",sq:"18,17,13,27,77,69,78,85",oc:function(){alert("seq 0")}}; 
// ctrl+alt+esc
//ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length] = {nm:"teste",sq:"17,18,27",oc:function(){alert("seq 1")}}; 
// del
//ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length] = {nm:"del",sq:"46",oc:function(){alert("seq 2")}}; 
//
// *************************************************************************
//
ALiBS.sy = 
{
	gl: (!!ALiBS.gv.sy.sygl?ALiBS.gv.sy.sygl:1), // 0 = required/system = default - static and global log level
	/**
	Global Variable ALiBS.sy.gl

		Description: Static Global Log Level.

		Values:
			-1 = none = OFF
			0 = required/system (logs 0)
			1 = [DEFAULT] error (logs 0,1)
			2 = warning (logs 0,1,2)
			3 = success (logs 0,1,2,3)
			4 = info (logs 0,1,2,3,4)
			5 = debug (logs 0,1,2,3,4,5)
			6 = trace (logs 0,1,2,3,4,5,6)
			7 = superdebug (logs 0,1,2,3,4,5,6,7)
			8 = confidencial (logs 0,1,2,3,4,5,6,7,8)

		Example:
			ALiBS.sy.gl = 5;

	--------------------------------------------------------------------
	*/
	// *****************************************************************
	//gi: "", // static and global log id TODO: Deprecated in 2010/08/26
	// *****************************************************************
	li: (!!ALiBS.gv.sy.syli?ALiBS.gv.sy.syli:""), // ALiBS.sy.li is NOT necessary if(level == 0 | 1) OR if(level > 2 AND {le:1})
	/**
	Global Variable ALiBS.sy.li

		Description: Static Global Log List of Methods to (le:2)Allow / (le:3)Deny.

		Example:
			ALiBS.sy.li = "ALiBS.sy.ec,ALiBS.sy.er,ALiBS.sy.ec";

	--------------------------------------------------------------------
	*/
	// *****************************************************************
	la: function(l,m,p) // log to array
	/** 
	Method <static> <void> ALiBS.sy.la(l,m,p)

		Description: Log Messages to System Array.

		Parameters:
			l = (required)(number) level 
				0+0 = required/system
					- ALWAYS logged even if Local Log (le=0) and/or Global Log (ALiBS.sy.gl==0) 
					Use for:
					a) ALiBS.sy.la(0+0,"loaded module ALiBS.sg",{o:"ALiBS.sg"});

				1+0 = error
					- ALWAYS logged even if Local Log (le=0) and/or Global Log (ALiBS.sy.gl==0) 
					Use for:
					a) ec: ALiBS.sy.la(1+0,r.m0+r.m1,{o:"ALiBS.cp.bc10"});
					b) REAL ERRORS in try{}catch(e){} // Rhino web = load all lib by default
						ALiBS.sy.la(1+0,"ALiBS.ag Exception(java.lang.Thread):"+e,{o:"ALiBS.ag"});
					c) Connection errors

				2 = warning
					- Logged if (le==true) AND (ALiBS.sy.gl>=2) 

				3 = success
					- Logged if (le==true) AND (ALiBS.sy.gl>=3) 

				4 = info
					- Logged if (le==true) AND (ALiBS.sy.gl>=4)

				5 = debug
					- Logged if (le==true) AND (ALiBS.sy.gl>=5)
					Use for:
					a) ALiBS general debug messages (ALiBS.cp finds and generates error message)
					b) also can use Chrome console.log (ALiBS.cp finds and generates error message)

				5+0 = debug
					- Logged if (le==true) AND (ALiBS.sy.gl>=5)
					Use for:
					a) ALiBS general debug messages (permanent, DO NOT REMOVE from code)
					b) Inside loops for()/while() that displays information of Arrays/Objects
					c) HANDLED ERRORS in try{}catch(e){} 
						ALiBS.sy.la(5+0,"ALiBS.ag Exception(java.lang.Thread):"+e,{o:"ALiBS.ag"});

				6+0 = trace
					- Logged if (le==true) AND (ALiBS.sy.gl>=6)
					Use for:
					a) ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.xx.x",le:ALiBS.gv.xx.le});
					b) parameter values (request["parameters"])
	
				7+0 = superdebug
					- Logged if (le==true) AND (ALiBS.sy.gl>=7)
					Use for:
					a) Inside loops for()/while() 
						ALiBS.sy.la(7+1,"array[i]="+array[i],{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});

				8+0 = confidencial
					- Logged if (le==true) AND (ALiBS.sy.gl>=8)
					Use for:
					a) Show confidencial information (password, secrets, etc)

			m = (required)(string/array) message OR function to run

			p = (optional)(JSON) parameters {d:"y",o:"ALiBS.el",le:true,so:print} 
				d = (String)y/n (y=yes=default= use date time stamp)(n=no)
				o = (String)origin of message (class/module name)
					[?] = DEFAULT
				le = (number)local log enabled - default = false
					le:ALiBS.gv.lg.le (CORRECT USAGE)
					le:true (FORCE)(AVOID USING)
				so = (String)standard output (js.jar=print)
					"" = DEFAULT = do NOT print/console
					"print" = print/console using loop = even ERRORS are printed and Array is empty(ed)
					"printjoin" = print/console using join = even ERRORS are printed and Array is empty(ed)

				ii = (number)initial position for z.m.substr 
					0 = DEFAULT

				ll = (number)length for z.m.substr
					ALiBS.gv.sy.gll = DEFAULT

				nw = (number)new line before 
					0 = NO = DEFAULT
					[1-n] = number of lines

				p0 = (number)number of parameters
				p1 = (any type)parameter 1
				p2 = (any type)parameter 2
				p3 = (any type)parameter 3
				p4 = (any type)parameter 4

				s0 = (string) str to show before message
				s1 = (string) str to show after message


		Observations About ALiBS.gv.xx.le of each Class/Module:
			0 = Local Log Disabled = OFF (ALiBS.gv.xx.le = 0)
			1 = Local Log Enabled = All Methods of the Class are Logged (ALiBS.gv.xx.le = 1)
			2 = Local Log Enabled = Allow All Methods of the Class in List to be Logged (ALiBS.sy.li)
			3 = Local Log Enabled = Deny All Methods of the Class in List to be Logged (ALiBS.sy.li)
	
		Observations About "Printing to File": (Tomcat/Catalina.out, JBoss/Server.log, Drivers log, etc)
			Possibilities:
				1) log to NATIVE log
					ALiBS.gv.sy.logFile = ""; // DEFAULT

				2) log to NATIVE log (necessary to redefine / reset log)(AUTO = happens in ALiBS.sy)
					ALiBS.gv.sy.logFile = "/var/log/idm_drivers/Relatorios.log";

				3) log to CUSTOM log (necessary to redefine / reset log)(AUTO = happens in ALiBS.sy)
					ALiBS.gv.sy.logFile = "/var/log/idm_drivers/Relatorios-debug.log";

			Redefine / Reset Log:
				ALiBS.gv.sy.lgrs() = only works if ALiBS.gv.sy.logFile has been set

		Return Value:
			<void> = NO Return

		Example:
			ALiBS.sy.la(1+0,"Error="+err,{o:"ALiBS.dt.gs",le:ALiBS.gv.dt.le});
			ALiBS.sy.la(5+0,"var1="+var1,{o:"ALiBS.dt.gs",le:ALiBS.gv.dt.le});
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.gs",le:ALiBS.gv.dt.le});

	--------------------------------------------------------------------
	*/
	{
		//DO NOT USE ALiBS.sy.la(6+0) here!!!! To avoid looping!!!!
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = 0; // [BEST-PRACTICE]
		//
		z.ts = new Date();
		//
		if(ALiBS.sy.gl>=0) // CHAVE GERAL: configuração do nivel do log na pagina da aplicação
		{
			if(l<=ALiBS.sy.gl && l>=0) // Logs if allowed level (ex-ok:l=2,gl=3) (ex-not-ok:l=5,gl=1)
			{ 
				z.ok = false;
				//
				if(l==0 || l==1) // l=0 (required/system) OR l=1 (error)
				{
					z.ok = true;
				}
				else if(l>=2 && l<=8 && typeof(p.le)=="number")  // AND local log enabled (le=1,2,3)
				{
					if(p.le==1) // All Methods - does NOT check ALiBS.sy.li
					{
						z.ok = true;
					}
					else if(p.le==2) // Allow All Methods listed in ALiBS.sy.li
					{
						if(typeof(p.o)=="string" && ALiBS.sy.li.indexOf(p.o)>=0)
						{
							z.ok = true;
						}
					}
					else if(p.le==3) // Deny All Methods listed in ALiBS.sy.li
					{
						if(typeof(p.o)=="string" && ALiBS.sy.li.indexOf(p.o)<0)
						{
							z.ok = true;
						}
					}
				}
				//
				// ********
				//
				if(z.ok)
				{

					if(p)
					{
						if(typeof(p.so)=="string") z.so = p.so; // print
						if(typeof(p.o)=="string") z.o = "[" + p.o + "]"; // origin
						if(typeof(p.d)=="string" && p.d=="n") z.d = "[0]"; // timestamp
					}
					//  DEFAULTS [BEST-PRACTICE]
					if(typeof(z.so)=="undefined" || !z.so) z.so = ""; // DEFAULT 
					if(typeof(z.o)=="undefined" || !z.o) z.o = "[?]"; // DEFAULT 
					if(typeof(z.d)=="undefined" || !z.d) z.d = ""; // DEFAULT 
					if(typeof(z.p0)!="number") z.p0 = 0; // DEFAULT 
					if(typeof(z.ii)!="number") z.ii = 0; // DEFAULT 
					if(typeof(z.ll)!="number") z.ll = ALiBS.gv.sy.gll; // DEFAULT 
					if(typeof(z.s0)!="string") z.s0 = ""; // DEFAULT 
					if(typeof(z.s1)!="string") z.s1 = ""; // DEFAULT 
					if(typeof(z.nw)!="number") z.nw = 0; // DEFAULT 
					//
					// **************************
					//
					z.v = "";
					//
					for(var i=0;i<z.nw;i++) // 0 = DEFAULT = NO new line
					{
						z.v = z.v + ALiBS.gv.sy.se; 
					}
					//
					z.v = z.v +ALiBS.sy.yy(z.o,l,z.d);
					//
					// **************************
					//
					if(typeof(m)!="string")
					{
						if(z.p0==0)
						{
							z.m0 = m();
						}
						else if(z.p0==1)
						{
							z.m0 = m(z.p1);
						}
						else if(z.p0==2)
						{
							z.m0 = m(z.p1,z.p2);
						}
						else if(z.p0==3)
						{
							z.m0 = m(z.p1,z.p2,z.p3);
						}
						else if(z.p0==4)
						{
							z.m0 = m(z.p1,z.p2,z.p3,z.p4);
						}
						else
						{
							// TODO(2013-06-06): use more than 4 parameters
						}
						//
						z.m = z.s0 + (""+z.m0).substr(z.ii,z.ll) + z.s1;
					}
					else if(m.length<=z.ll)
					{
						z.m = m;
					}
					else
					{
						z.m = m.substr(z.ii,z.ll);
					}
					//
					// **************************
					//
					if(l<8)
					{
						ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = z.v + " " + z.m;
					}
					else // 8 = confidencial
					{
						// to test, in chrome console (NOT HERE in code) using stress test
							// ALiBS.sy.gl=8
							// ALiBS.sy.la(8,"teste",{so:"print",le:1})
							// ALiBS.c9=null , ALiBS.sg=null , ALiBS.c1=null , ALiBS.c2=null
						//
						if(ALiBS.c9)
						{
							ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = z.v + " " + "c9:" + ALiBS.c9.e4(z.m); // ALiBS.c9.d4("dGVzdGU=")
						}
						else if(ALiBS.sg)
						{
							ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = z.v + " " + "sg:" + ALiBS.sg.zp(z.m); // ALiBS.sg.uz([116,101,115,256])
						}
						else if(ALiBS.c1 && ALiBS.c2) 
						{
							ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = z.v + " " + "c1:" + ALiBS.c1.ae(ALiBS.c2.sa(z.m)); 
																// ALiBS.c2.as(ALiBS.c1.ea([163,133,162,163,133]))
						}
						else if(ALiBS.c2)
						{
							ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = z.v + " " + "c2:" + ALiBS.c2.sa(z.m); // ALiBS.c2.as([116,101,115,116,101])
						}
						else
						{
							ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = z.v + " " + z.m; // 
						}
					}
					//
					// **************************
					//
					z.x0 = "p"+"rint";
					z.x1 = "p"+"rintjoin";
					//
					if(ALiBS.gv.sy.ev=="Browser" && ALiBS.gv.sy.so=="immediate-console") // Browser
					{
						for(var i=0;i<ALiBS.gv.sy.gl.length;i++) // print log in array, (probably only this message)
						{
							console["log"](ALiBS.gv.sy.gl[i]); 
						}
						//
						ALiBS.gv.sy.gl.length = 0; // [BEST-PRACTICE] - if printED, then should CLEAR log
					}
					else if(ALiBS.gv.sy.ev=="Server" && ALiBS.gv.sy.so=="immediate-print") // Server
					{
						for(var i=0;i<ALiBS.gv.sy.gl.length;i++) // print log in array, (probably only this message)
						{
							print(ALiBS.gv.sy.gl[i]); // this is the only allowed print
						}
						//
						ALiBS.gv.sy.gl.length = 0; // [BEST-PRACTICE] - if printED, then should CLEAR log
					}
					else if(z.so==z.x0 || ALiBS.gv.sy.so==z.x0 || (ALiBS.gv.sy.gla==0 && ALiBS.gv.sy.gl.length>=ALiBS.gv.sy.glx) || z.so==z.x1 || ALiBS.gv.sy.so==z.x1)
					{
						z.v = ALiBS.sy.yy(z.o,5,z.d);
						//
						if(ALiBS.gv.sy.gla==0 && ALiBS.gv.sy.gl.length>=ALiBS.gv.sy.glx)
						{
							r = ALiBS.sy.fm(0,z.v,z.ts); // Auto
						}
						else
						{
							r = ALiBS.sy.fm(2,z.v,z.ts,{so:z.so}); // Custom 
						}
						//
						// *********
						//
						if(ALiBS.gv.sy.ev=="Browser") 
						{
							if(typeof(console)!="undefined" && typeof(console["log"])!="undefined")
							{
								if(ALiBS.gv.sy.so==z.x1 || z.so==z.x1) // printjoin = use join
								{
									console["log"](ALiBS.gv.sy.gl.join("\n")); 
								}
								else // printinloop = use loop
								{
									for(var i=0;i<ALiBS.gv.sy.gl.length;i++)
									{
										console["log"](ALiBS.gv.sy.gl[i]); 
									}
								}
							}
							else // console.log does NOT exist
							{
								ALiBS.sy["gl"] = -1; // turn OFF log
							}
						}
						else if(ALiBS.gv.sy.ev=="Server") 
						{
							if(ALiBS.gv.sy.so==z.x1 || z.so==z.x1)  // printjoin = use join
							{
								print(ALiBS.gv.sy.gl.join("\n")); // this is the only allowed print
							}
							else // printinloop = use loop
							{
								for(var i=0;i<ALiBS.gv.sy.gl.length;i++)
								{
									print(ALiBS.gv.sy.gl[i]); // this is the only allowed print
								}
							}
						}
						else // Other
						{
							ALiBS.sy["gl"] = -1; // turn OFF log
						}
						//
						ALiBS.gv.sy.gl.length = 0; // [BEST-PRACTICE] - if printED, then should CLEAR log
					}
				}
			}
		}
		//else
		//{
			// in this situation, there is no "z.v", so it's necessary to create it below
		//}
		//
		// ***********
		//
		if(ALiBS.gv.sy.gl.length>=ALiBS.gv.sy.glx) // REDUCE
		{
			if(!z.v) // not run yet - need to create strings
			{
				if(p)
				{
					if(typeof(p.o)=="string") z.o = "[" + p.o + "]"; // origin
					if(typeof(p.d)=="string" && p.d=="n") z.d = "[0]"; // timestamp
				}
				//  DEFAULTS [BEST-PRACTICE]
				if(typeof(z.o)=="undefined" || !z.o) z.o = "[?]"; // DEFAULT 
				if(typeof(z.d)=="undefined" || !z.d) z.d = ""; // DEFAULT 
				//
				z.v = ALiBS.sy.yy(z.o,5,z.d);
			}
			else // already run once - re-run with level 5
			{
				z.v = ALiBS.sy.yy(z.o,5,z.d);
			}
			//
			r = ALiBS.sy.rl(z.v,z.ts);
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	fm: function(ac,sf,ts,p) // log footer messages
	// ac = (required)(number)action
	//	0 = Auto Print = DEFAULT
	//	1 = Reduce
	//	2 = Custom Print
	// sf = (required)(string)log string formatted
	//
	// p = (optional) (JSON) parameters {} 
	//	so = (string)standard output (js.jar=print)
	//	de = (number)number of deleted lines - DEFAULT = 0
	//	ar = (number)auto reduced after - DEFAULT = 0
	//
	{
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.de)!="number") z.de = 0; // DEFAULT 
		if(typeof(z.ar)!="number") z.ar = 0; // DEFAULT 
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = "";
		//
		if(ac==0) // Auto
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " Auto Printed After="+(ALiBS.gv.sy.gl.length-1)+" [glx]";
		}
		else if(ac==1) // Reduce
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " Auto Reduced After="+(z.ar)+" [glx]";
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " deleted="+(z.de)+"";
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " kept="+(z.ar-z.de)+"";
		}
		else // 2 = Custom
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " Custom Printed After="+(ALiBS.gv.sy.gl.length-1)+"";
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.so=["+ALiBS.gv.sy.so+"]";
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " z.so=["+z.so+"]";
		}
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = "";
		//
		if(ALiBS.gv.sy.gla==0)
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gla="+ALiBS.gv.sy.gla+" [AUTO PRINT]";
		}
		else if(ALiBS.gv.sy.gla==1)
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gla="+ALiBS.gv.sy.gla+" [AUTO REDUCE]";
		}
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.glx="+ALiBS.gv.sy.glx+"";
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.glu="+ALiBS.gv.sy.glu+"";
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gle="+ALiBS.gv.sy.gle+"";
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gll="+ALiBS.gv.sy.gll+"";
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = "";
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.sy"+".gl="+ALiBS["sy"].gl+"";
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.sy"+".li=["+ALiBS["sy"].li+"]";
		//
		for(var i in ALiBS.gv)
		{
			if(ALiBS.gv[i].le) ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + ' ALiBS.gv["'+i+'"].le=' + ALiBS.gv[i].le;
		}
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = "";
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " Total Time=["+(new Date - ts)+"] millisecond(s)";
		//
		// ***********
		//
		var r = {}; // [BEST-PRACTICE]
		//
		if(ac==0) // Auto
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gl.length="+0+" [Log Auto Cleaned]";
			//
			r.len = 0; 
			r.sz = 0; 
		}
		else if(ac==1) // Reduce
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gl.length="+ALiBS.gv.sy.gl.length+" [Log Auto Reduced]";
			//
			r.len = ALiBS.gv.sy.gl.length+1; // +1 = add bellow
			r.sz = 0; // size = lines x length
			//
			for(var i=0;i<ALiBS.gv.sy.gl.length;i++)
			{
				if(ALiBS.gv.sy.gl[i]) // avoid null values
				{
					r.sz += ALiBS.gv.sy.gl[i].length;
				}
			}
		}
		else // 2 = Custom
		{
			ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gl.length="+0+" [Log Custom Cleaned]";
			//
			r.len = 0; 
			r.sz = 0; 
		}
		//
		// ***********
		//
		ALiBS.gv.sy.gl[ALiBS.gv.sy.gl.length] = sf + " ALiBS.gv.sy.gl(Total Size)="+r.sz+"";
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	yy: function(o,l,d) // log string formatted
	// o = (required)(String)origin of message (class/module name)
	// l = (required)(number)level 
	// d = (required)(String)y/n (y=yes=default= use date time stamp)(n=no)
	// return = log string formatted
	{
		//DO NOT USE ALiBS.sy.la(6+0) here!!!! To avoid looping!!!!
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.d = new Array();
		//
		z.d[z.d.length] = "["+ALiBS.gv.sy.ev+"]";
		//
		if(!d)
		{
			if(ALiBS.dt)
			{
				z.d[z.d.length] = "[" + (ALiBS.dt.ts({s0:"-",s1:" ",s2:":",s3:"."} )) + "]";
			}
			else
			{
				//z.d[z.d.length] = "[" + (""+new Date()).substring(0,24) + "]";
				//
				z.dt = new Date();
				//
				z.ts = new Array();
				//
				z.jy = z.dt.getFullYear();
				z.jm = z.dt.getMonth()+1; // [1-12]
				z.jd = z.dt.getDate();
				//
				z.jh = z.dt.getHours();
				z.jn = z.dt.getMinutes();
				z.js = z.dt.getSeconds();
				z.ji = z.dt.getMilliseconds();
				//
				z.ts[z.ts.length] = z.jy+"-";
				//
				if(z.jm<10) z.ts[z.ts.length] = "0"+z.jm+"-";
				else  z.ts[z.ts.length] = ""+z.jm+"-";
				//
				if(z.jd<10) z.ts[z.ts.length] = "0"+z.jd+" ";
				else  z.ts[z.ts.length] = ""+z.jd+" ";
				//
				if(z.jh<10) z.ts[z.ts.length] = "0"+z.jh+":";
				else  z.ts[z.ts.length] = ""+z.jh+":";
				//
				if(z.jn<10) z.ts[z.ts.length] = "0"+z.jn+":";
				else  z.ts[z.ts.length] = ""+z.jn+":";
				//
				if(z.js<10) z.ts[z.ts.length] = "0"+z.js+".";
				else  z.ts[z.ts.length] = ""+z.js+".";
				//
				if(z.ji<10) z.ts[z.ts.length] = "00"+z.ji;
				else if(z.ji<100) z.ts[z.ts.length] = "0"+z.ji;
				else  z.ts[z.ts.length] = ""+z.ji;
				//
				z.d[z.d.length] = "[" + z.ts.join("") + "]";
			}
		}
		else
		{
			z.d[z.d.length] = d; // [0]
		}
		//
		z.d[z.d.length] =  "["+ALiBS.sy.gl+":" + "REWSIDTXZ".substr(ALiBS.sy.gl,1) + "]"; // timestamp
		//
		z.d[z.d.length] = o + "["+l+":" + "REWSIDTXZ".substr(l,1) + "]"; // +"\n";
		//
		var r = z.d.join("");
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	rl: function(v,ts) // reduce log ALiBS.gv.sy.gl
	// v = (required)(string)log string formatted
	// ts = (required)(Date)start time
	// return = NONE
	{
		//DO NOT USE ALiBS.sy.la(6+0) here!!!! To avoid looping!!!!
		//
		var z = {}; // [BEST-PRACTICE]
		//
		//z.l = ALiBS.gv.sy.gl.length;
		//
		z.a = new Array();
		//
		z.nu = 0; // counter for null items
		//
		z.glu = ALiBS.gv.sy.glu; // counter for ALiBS.gv.sy.glu = messages (NOT Errors) to keep
		//
		z.gle = ALiBS.gv.sy.gle; // counter for ALiBS.gv.sy.gle = Errors to keep
		//
		for(var i=ALiBS.gv.sy.gl.length-1;i>=0;i--) // keep last ALiBS.gv.sy.glu records 
		{
			if( ALiBS.gv.sy.gl[i] ) // NOT null, undefined, empty
			{
				if((ALiBS.gv.sy.gl[i]+"").indexOf("[1:E] ")<0) // if NOT FOUND [1:E] then delete
				{
					if(z.glu<=0) // messages to keep ended then delete all
					{
						ALiBS.gv.sy.gl[i] = null;
						//
						z.nu++;
					}
					else // keep
					{
						//
						z.glu--;
					}
					//
					//z.glu--;
				}
				else // if FOUND [1:E]
				{
					if(z.gle<=0) // errors to keep ended then delete all
					{
						ALiBS.gv.sy.gl[i] = null;
						//
						z.nu++;
					}
					else // keep
					{
						//
						z.gle--;
					}
					//
					//z.gle--;
				}
			} 
			else // if null OR undefined OR empty
			{
				ALiBS.gv.sy.gl[i] = null;
				//
				z.nu++;
			}
		}
		//
		z.len = ALiBS.gv.sy.gl.length;
//console.log("z.len="+z.len)
//console.log("z.nu="+z.nu)
		//
		ALiBS.gv.sy.gl.sort(); // null items go to end position
		//
		ALiBS.gv.sy.gl.length = ALiBS.gv.sy.gl.length - z.nu; // + 4; // remove null items
		//
		var r = ALiBS.sy.fm(1,v,ts,{de:z.nu,ar:z.len}); // Reduce
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	ga: function(p) // get array
	// p = (optional) (JSON) parameters {} 
	// 	s = (String)separator
	//	r = (boolean)reverse Array
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.ga",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.s)!="string") z.s = ALiBS.gv.sy.se; // DEFAULT
		if(typeof(z.r)!="boolean") z.r = false; // DEFAULT
		//
		if(z.r) // reverse
		{
			z.b = ALiBS.sg.ca(ALiBS.gv.sy.gl,{r:true});
			//
			r = z.b.join(z.s);
		}
		else
		{
			r = ALiBS.gv.sy.gl.join(z.s);
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	hx: function() // reset array
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.hx",le:ALiBS.gv.sy.le});
		//
		ALiBS.gv.sy.gl.length = 0;
		//
		ALiBS.gv.sy.gl = new Array();
		//
		return;
	},
	// *****************************************************************
	kc: function(e) // key press control
	// e = (required)(Event)event
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
		//
		var l = ALiBS.gv.sy.sq.length;
		//
		var r;
		//
		if(l>0)
		{
			if(typeof(window)!="undefined" && typeof(window.event)!="undefined")
			{
				e = window.event;
				k = window.event.keyCode;
			}
			else if(e.which)
			{
				// e = e;
				k = e.which; // (legacy) NS/Firefox
			}
			//
			// ****************
			//
			if(e.target)
			{
				t = e.target;
			}
			else if(e.srcElement)
			{
				t = e.srcElement;
			}
			// 
			if(t.nodeType==3) // defeat Safari bug
			{
				t = t.parentNode;
			}
			//
			/*for(var i in t)
			{
				ALiBS.sy.la(5+0,"event target["+i+"]="+t[i],{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
			}*/
			//
			ALiBS.sy.la(5+0,"event target[tagName]="+t["tagName"],{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
			ALiBS.sy.la(5+0,"event target[nodeName]="+t["nodeName"],{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
			//
			ALiBS.sy.la(5+0,"event type="+e.type,{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
			//
			// ****************
			//
			ALiBS.gv.sy.kc[ALiBS.gv.sy.kc.length] = k;
			//
			var a = ALiBS.gv.sy.kc.join();
			//
			ALiBS.sy.la(5+0,"(cache)kc=["+a+"]",{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
			//
			for(var i=0;i<l;i++)
			{
				if(a.indexOf(ALiBS.gv.sy.sq[i].sq)>=0)
				{
					if(ALiBS.gv.sy.sq[i].p) // there are parameters defined
					{
						if(typeof(ALiBS.gv.sy.sq[i].p.rkc)!="number") // = from parameter passing in instance = ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length]
						{
							ALiBS.gv.sy.sq[i].p.rkc = 1; // 1 = reset kc = DEFAULT
						}
						//
						if(typeof(ALiBS.gv.sy.sq[i].p.run)!="number") // = from parameter passing in instance = ALiBS.gv.sy.sq[ALiBS.gv.sy.sq.length]
						{
							ALiBS.gv.sy.sq[i].p.run = 1; // 1 = keep running = DEFAULT
						}
						//
						ALiBS.gv.sy.sq[i].p.ix = i; // position in sq[] array
						ALiBS.gv.sy.sq[i].p.e = e; // event
					}
					else // no parameters defined
					{
						ALiBS.gv.sy.sq[i].p = 
						{
							rkc: 1, // 1 = reset kc = DEFAULT
							run: 1, // 1 = keep running = DEFAULT
							ix: i, // position in sq[] array
							e: e // event
						};
					}
					//
					// **************************
					//
					ALiBS.sy.la(5+0,"(running)ALiBS.gv.sy.sq["+i+"].nm="+ALiBS.gv.sy.sq[i].nm,{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
					//
					ALiBS.gv.sy.sq[i].oc(ALiBS.gv.sy.sq[i].p);
					//
					// **************************
					//
					if(!ALiBS.gv.sy.sq[i].p.rkc) // 0 = rkc = do NOT reset kc 
					{
						// 
					}
					else if(ALiBS.gv.sy.sq[i].p.rkc==1) // 1 = rkc = reset kc = DEFAULT
					{
						ALiBS.gv.sy.kc.length = 0; // reset array
					}
					else if(ALiBS.gv.sy.sq[i].p.rkc==2) // 2 = rkc = remove last key after execution
					{
						ALiBS.gv.sy.kc.length = ALiBS.gv.sy.kc.length-1; // 
					}
					//
					// **************************
					//
					if(ALiBS.gv.sy.sq[i].p.run==0) // 0 = run = do NOT keep running = BREAK
					{
						break;
					}
				}
			}
			//
			if(a.length>=ALiBS.gv.sy.kcmx) // DEFAULT = 150 = 50 key (2+1 chars)
			{
				ALiBS.gv.sy.kc.length = 0; // reset array
			}
		}
		//
		//alert(k);
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sy.kc",le:ALiBS.gv.sy.le});
		//
		return;
	},	
	// *****************************************************************
	ec: function(ec) // ERROR check
	// ec = (required)(object)error code object
	// return = (boolean)
	//	true = is error
	//	false = is NOT error
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.ec",le:ALiBS.gv.sy.le});
		//
		var r; // [BEST-PRACTICE]
		//
		if(!(!ec || !ec.ec || !(ec.ec<0)))
		{
			r = true;
		}
		else
		{
			r = false;
		}
		//
		return r;
	},
	// *****************************************************************
	ev: function(v,mn,p) // ERROR validation (parameter validation)
	// v = (required)value to be validated
	// mn = (required)(String)class/method name
	// p = (optional)(JSON) parameters
	//	ty = typeof
	//		x = DEFAULT = NO DEFINED TYPE
	//		s = string
	//		n = number
	//		b = boolean
	//		a = array
	//		j = json
	//		f = function
	//	lx = max string length OR max number value
	//		null = DEFAULT = NO DEFINED MAX
	//	ln = min string length OR min number value
	//		null = DEFAULT = NO DEFINED MIN
	// return = (object)error code object
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.ev",le:ALiBS.gv.sy.le});
		//
		var z = {}; // [BEST-PRACTICE] JSON object to hold all variables (to help garbage collection)
		var r = null; // [BEST-PRACTICE] unique variable outside JSON = return
		//
		if(p) // [BEST-PRACTICE]
		{
			if(typeof(p.ty)=="string") z.ty = p.ty; 
			if(typeof(p.lx)=="number") z.lx = p.lx; 
			if(typeof(p.ln)=="number") z.ln = p.ln; 
		}
		//
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.ty)=="undefined" || !z.ty) z.ty = "x"; // DEFAULT
		if(typeof(z.lx)=="undefined" || !z.lx) z.lx = null; // DEFAULT = NO DEFINED MAX
		if(typeof(z.ln)=="undefined" || !z.ln) z.ln = null; // DEFAULT = NO DEFINED MIN
		//
		if(z.ty=="s") // string
		{
			if(!r && typeof(v)!="string") r = ALiBS.sy.em(-1100,mn,{m0:"[STRING-REQUIRED]",m1:v}); // NOT a string
			//
			if(!r && z.ln!=null && v.length<z.ln) r = ALiBS.sy.em(-1001,mn,{m0:"[LENGTH<MIN]",m1:v}); 
			//
			if(!r && z.lx!=null && v.length>z.lx) r = ALiBS.sy.em(-1002,mn,{m0:"[LENGTH>MAX]",m1:v}); 
		}
		else if(z.ty=="n") // number
		{
			if(!r && typeof(v)!="number") r = ALiBS.sy.em(-1100,mn,{m0:"[NUMBER-REQUIRED]",m1:v}); // NOT a number
			//
			if(!r && z.ln!=null && v<z.ln) r = ALiBS.sy.em(-1001,mn,{m0:"[VALUE<MIN]",m1:v}); 
			//
			if(!r && z.lx!=null && v>z.lx) r = ALiBS.sy.em(-1002,mn,{m0:"[LENGTH>MAX]",m1:v}); 
		}
		else if(z.ty=="b") // boolean
		{
			if(!r && typeof(v)!="number") r = ALiBS.sy.em(-1100,mn,{m0:"[BOOLEAN-REQUIRED]",m1:v}); // NOT a boolean
		}
		else // including "x"
		{
			r = ALiBS.sy.em(-1000,mn,{m0:m0,m1:v}); // NO DEFINED TYPE
		}
		//
		z = null; // [BEST-PRACTICE] give garbage collection a hand
		delete z; // [BEST-PRACTICE] give garbage collection a hand
		//
		return r;
	},
	// *****************************************************************
	no: function() // new object 
			// TODO(2015-07-30): substituir esta funcao e sy.nw e sy.no por 
			// a = new Object(T4) -->> copia uma tabela inteira

	// arguments[0] = (string)names of properties/parameters
	// arguments[n] = values
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.no",le:ALiBS.gv.sy.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.n = arguments[0].split(","); 
		//
		var r = {};
		//
		for(var i=0;i<z.n.length;i++)
		{
			r[z.n[i]] = arguments[i+1];
		}
		//
		//r[n[0]] = v0;
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	nw: function(o,p) // new object - copy object
	// o = (object)obj to copy
	// p = (optional)(JSON) parameters
	//
	//	ac = (number)action
	//		1 = DEFAULT = console.log
	//		2 = typeof
	//		3 = Size
	//		4 = new/copy object
	//
	//	pa = (string)parent name
	//
	// return
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.nw",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = null; // [BEST-PRACTICE] unique variable outside JSON = return
		//
		// ***********
		//
		if(p) // [BEST-PRACTICE]
		{
			if(typeof(z.ac)!="number" || z.ac>2) z.ac = 1;  // DEFAULT
			if(typeof(z.pa)!="string") z.pa = "";  // DEFAULT
		}
		//
		// ***********
		//
		z.ty = ALiBS.sy.ty(o);
		//
		if(z.ty=="string")
		{
		
		}
		else if(z.ty.indexOf("object")<0)
		{
			if(z.ac==2)
			{
			}
			else if(z.ac==3)
			{

			}
			else // z.ac==1
			{
				if(!z.pa) z.pa = "root";
				//
				if(!i) i = "?";
				//
				if(z.ty=="string")
				{
					//
				}
				//
//console.log("[PRIMITIVE]["+z.ty+"]["+(z.pa)+"]["+i+"]="+o);
			}
		}
		else // if(z.ty.indexOf("object")>=0)
		{
			for(var i in o)
			{
				z.cc = 0;
				z.mx = 4;
				//
				if(z.cc>=z.mx)
				{
					break;
				}
				//
				z.ty = ALiBS.sy.ty(o);
				//
				if(z.ty.indexOf("object")>=0)
				{
					if(z.ac==2)
					{
					}
					else if(z.ac==3)
					{

					}
					else // z.ac==1
					{
//console.log("[OBJECT]["+z.ty+"]["+z.pa+"]["+i+"]="+o[i]);
						ALiBS.sy.nw(o[i],{ac:z.ac,pa:""+i});
					}
				}
			}
		}
		//
		// ***********
		//
		r = "";

/*
		z.f0 = function(o)
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.nw.f0",le:ALiBS.gv.sy.le});
			//
			return o;
		}; 
		//
		var r = new z.f0(o); // ATTENTION - already returns "new"
*/

		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
/*	__nw: function(o,p) // 
	// o = (object)obj to copy
	// p = (optional)(JSON) parameters
	//
	//	ac = (number)action
	//		1 = DEFAULT = console.log
	//		2 = typeof
	//		3 = Size
	//		4 = new/copy object
	//
	//	pa = (string)parent name
	//
	// return
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.nw",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = null; // [BEST-PRACTICE] unique variable outside JSON = return
		//
	},
*/	// *****************************************************************
	em: function(ec,mn,p) // ERROR message
	// ec = (required)(number)error code
	// mn = (required)(string)class/method name
	// p = (optional)(JSON)parameters {}
	//	mg = (string)message type
	//		ERROR = DEFAULT
	//		WARNING 
	//
	//	m0 = (string)message 0 (error descrition)
	//	m1 = (string)message 1 (error value)
	//	m2 = (string)exception message
	//	va = (any)real value
	//
	// return = (object)error code object
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.em",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE] JSON object to hold all variables (to help garbage collection)
		//
		var r; // [BEST-PRACTICE] unique local variable outside JSON = return
		//
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.m0)!="string") z.m0 = "m0=UNDEFINED"; // DEFAULT
		if(typeof(z.m1)!="string") z.m1 = "m1=UNDEFINED"; // DEFAULT
		if(typeof(z.mg)!="string") z.mg = "ERROR"; // DEFAULT
		//
		if(!ALiBS.gv.sy.shex) // do NOT show REAL/ROOT exception message = DEFAULT = false
		{
			z.mex = "";
		}
		else // 1 = show REAL/ROOT exception message
		{
			z.mex = z.m2; // z.m2 = e;
		}
		//
		// ***********
		//
		if(!ec) // NOT ERROR
		{
			r = 
			{
				ec:ec,
				m0:"[OK]",
				m1:"",
				m2:"",
				va:z.va
			};
		}
		else // ERROR
		{
			r = 
			{
				ec:ec,
				m0:"["+z.mg+"]["+ec+"]["+mn+"]["+z.m0+"]",
				m1:"["+z.m1+"]",
				m2:"["+z.mg+"]["+ec+"]["+mn+"]["+z.m0+"]" + "["+z.m1+"]["+z.mex+"]", // m0+m1+m2(mex)
				va:z.va
			};
		}
		//
		// ***********
		//
/*		if((""+z.m2).indexOf("ALiBS")<0 && ((""+z.m2).indexOf("Error:")>0)||((""+z.m2).indexOf("Exception:")>0) ) // TypeError:, ReferenceError:, InternalError:, etc
		{
			ALiBS.gv.sy.ecc++;
			//
			ALiBS.sy.la(1+0,"[ERROR][detected by ALiBS Error Control Engine]",{o:"ALiBS.sy.em"});
			//
			if(z.m2) ALiBS.sy.la(1+0,"[EXCEPTION]["+z.m2+"]",{o:"ALiBS.sy.em",ll:500}); // exception message
		}
*/		//
		if( (""+z.m2).indexOf("ALiBS")<0 ) // TypeError:, ReferenceError:, InternalError:, etc
		{
			if( (""+z.m2).indexOf("Error:")>0 || (""+z.m2).indexOf("Exception:")>0 ) // TypeError:, ReferenceError:, InternalError:, etc
			{
				ALiBS.gv.sy.ecc++;
				//
				ALiBS.sy.la(1+0,"[ERROR][detected by ALiBS Error Control Engine]",{o:"ALiBS.sy.em",nw:2});
				//
				if( z.m2 && (""+z.m2).indexOf("ALiBS")<0 ) ALiBS.sy.la(1+0,"[EXCEPTION]["+z.m2+"]",{o:"ALiBS.sy.em",ll:500}); // exception message
			}
		}
		//
		ALiBS.sy.la(1+0,r.m2,{o:"ALiBS.sy.em",ll:500});
		//
		// ***********
		//
		z = null; // [BEST-PRACTICE] give garbage collection a hand
		delete z; // [BEST-PRACTICE] give garbage collection a hand
		//
		return r;
	},
	// *****************************************************************
	ty: function(v,p) // type of variable
	// v = (required)(any-type)variable
	// p = (optional)(JSON)parameters {}
	//
	// return = (string)type of variable
	//	undefined (null || var a)
	//	number
	//	string ("" || '')
	//	boolean
	//	function
	//	object-Array
	//	object-RegExp
	//	object-Object
	//	object-HTML
	//	object-Self
	//	object-Other
	//	????
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.ty",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE] JSON object to hold all variables (to help garbage collection)
		//
		var r; // [BEST-PRACTICE] unique local variable outside JSON = return
		//
		// ***********
		//
		// DEFAULTS [BEST-PRACTICE]
		//if(typeof(z.rt)!="number" || z.rt>2) z.rt = 0;  // DEFAULT
		//
		// ***********
		//
		if(v==null) // v=null; or var a;
		{
			// chrome,mozzila,opera return "object" for typeof(null)
			// opera return "function" for typeof(a)  (var a)
			//
			r = "undefined"; // type
		}
		else if(typeof(v)=="undefined")
		{
			r = "undefined"; // type
		}
		else if(typeof(v)=="number")
		{
			r = "number"; // type
		}
		else if(typeof(v)=="string")
		{
			r = "string"; // type
		}
		else if(typeof(v)=="boolean")
		{
			r = "boolean"; // type
		}
		//
		// ***********
		//
		else if(typeof(v.apply)=="fun"+"ction" && typeof(v.call)=="fun"+"ction" && (""+v.valueOf()).indexOf("fun"+"ction")>=0)
		{
			r = "fun"+"ction"; // type
		}
		//
		// ***********
		//
		else if(typeof(v.join)=="fun"+"ction" && typeof(v.sort)=="fun"+"ction" && typeof(v.pop)=="fun"+"ction")
		{
			if(v[0] && ALiBS.sy.ty(v[0])=="obj"+"ect-Object") // ALiBS.to.rs
			{
				r = "obj"+"ect-ALiBS.to.rs"; // ResultSet
			}
			else // <0 = can NOT find type
			{
				r = "obj"+"ect-Array"; // []
			}
		}
		//
		// ***********
		//
		else if(typeof(v.test)=="fun"+"ction" && typeof(v.exec)=="fun"+"ction" && typeof(v.compile)=="fun"+"ction")
		{
			r = "obj"+"ect-RegExp"; // //
		}
		//
		// ***********
		//
		else if(typeof(v.hasOwnProperty)=="fun"+"ction" && (""+v.valueOf())=="[obj"+"ect Obj"+"ect]") // last because it does NOT have exclusive methods/properties
		{
			if(v.rs && v.nm && v.cp && v.tb) // ALiBS.to
			{
				r = "obj"+"ect-ALiBS.to"; // {}
			}
			else
			{
				r = "obj"+"ect-Object"; // {}
			}
		}
		//
		// ***********
		//
		else if(typeof(v)=="obj"+"ect")
		{
			if(v.setItem && v.getItem)
			{
				r = "obj"+"ect-Object"; // {}
			}
			else if(v.document && v.document.getElementById)
			{
				r = "obj"+"ect-HTML";
			}
			else if(v.getElementById)
			{
				r = "obj"+"ect-HTML";
			}
			else if(v.getElementsByTagName)
			{
				r = "obj"+"ect-HTML";
			}
			else if(v.innerHTML)
			{
				r = "obj"+"ect-HTML";
			}
			else if((v==self || v==this))
			{
				r = "obj"+"ect-Self";
			}
			else 
			{
				r = "obj"+"ect-Other";
			}
		}
		//
		// ***********
		//
		else
		{
			r = "????"; // else
		}
		//
		if(z.rt==1) // object {)
		{
			r.va = v; // value
		}
		//
		z = null; // [BEST-PRACTICE] give garbage collection a hand
		delete z; // [BEST-PRACTICE] give garbage collection a hand
		//
		return r;
	},
	// *****************************************************************
	sz: function(v,p) // size of variable
	// v = (required)(any-type)variable
	// p = (optional)(JSON)parameters {}
	//	vn = (optional)variable name
	//	rt = (number)return type
	//		0 = return size = DEFAULT
	//		1 = return object
	//	rt2 = (number)return type 2
	//		0 = return if f04>=0
	//		1 = return if f04>=1
	//		2 = return if f04>=2
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.sz",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.rt)!="number") z.rt = 0; // size = DEFAULT
		//
		// local varibles [BEST-PRACTICE]
		if(ALiBS.to)
		{
			z.t0 = new ALiBS.to.t();
			//
			z.t0.tb.fa = "fc"; // active filter
			//
			if(typeof(z.rt2)!="string") z.t0.tb.fc = "(f04>-1000)";
			else  z.t0.tb.fc = z.rt2;
		}
		else
		{
			z.t0 = {};
			z.t0.rs = new Array();
			//
			if(typeof(z.rt2)!="number") z.rt2 = 0; // size = DEFAULT
		}
		//
		z.nu = 0; // quantity of null
		//
		z.s0 = 16; // name=8bytes + address=8bytes
		//
		z.t0.rs[0] = 
		//z.t0.rs[z.t0.rs.length] = 
		{
			f00:z.vn, // variable name
			f01:v // pointer or value
		};
		//f02:z.ty, // type
		//f03:z.s0+0, // size
		//f04:0, // 0=container , 1=final leap , 2=leaf of container
		//
		for(var i=0;i<z.t0.rs.length;i++)
		{
			z.ty = ALiBS.sy.ty(z.t0.rs[i].f01); // pointer or value
			//
			z.t0.rs[i].f02 = z.ty; // type
			//
			if(z.ty=="undefined")
			{
				z.t0.rs[i].f03 = z.s0+0; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			else if(z.ty=="string")
			{
				z.t0.rs[i].f03 = z.s0+(""+z.t0.rs[i].f01).length; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			else if(z.ty=="number")
			{
				z.t0.rs[i].f03 = z.s0+(""+z.t0.rs[i].f01).length; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			else if(z.ty=="boolean")
			{
				z.t0.rs[i].f03 = z.s0+1; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			else if(z.ty=="func"+"tion")
			{
				z.t0.rs[i].f03 = z.s0+(""+z.t0.rs[i].f01).length; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			else if(z.ty=="obj"+"ect-RegExp")
			{
				z.t0.rs[i].f03 = z.s0+(""+z.t0.rs[i].f01).length; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			else if(z.ty=="obj"+"ect-HTML")
			{
				z.t0.rs[i].f03 = z.s0+(""+z.t0.rs[i].f01).length; // size
				z.t0.rs[i].f01 = "";
				z.t0.rs[i].f04 = 1;
			}
			//
			// ****************
			//
			else if(z.ty=="obj"+"ect-Object" || z.ty=="obj"+"ect-ALiBS.to.rs")
			{
//console.log("passou2");
				z.t0.rs[i].f03 = z.s0; // size
				//
				for(var j in z.t0.rs[i].f01) // pointer or value
				{
					z.t0.rs[z.t0.rs.length] = 
					{
						f00:""+z.t0.rs[i].f00+'.'+j, // variable name
						f01:z.t0.rs[i].f01[j] // pointer or value
					};
					//
					z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size
				}
				//
				z.t0.rs[i].f01 = "";
				//
				z.t0.rs[i].f04 = 0;
			}
			//
			// ****************
			//
			else if(z.ty=="obj"+"ect-Array")
			{
				z.t0.rs[i].f03 = z.s0; // size
				//
				z.ty = ALiBS.sy.ty(z.t0.rs[i].f01[0]); // pointer or value
				//
				if(z.ty=="undefined")
				{
					/*for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+0; // size
					}*/
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				else if(z.ty=="string")
				{
					for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size
					}
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				else if(z.ty=="number")
				{
					for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size  
					}
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				else if(z.ty=="boolean")
				{
					/*for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+1; // size
					}*/
					//
					z.t0.rs[i].f03 += z.s0+z.t0.rs[i].f01.length; // size
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				else if(z.ty=="func"+"tion")
				{
					for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size
					}
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				else if(z.ty=="obj"+"ect-RegExp")
				{
					for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size
					}
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				else if(z.ty=="obj"+"ect-HTML")
				{
					for(var j=0;j<z.t0.rs[i].f01.length;j++)
					{
						z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size
					}
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 2;
				}
				//
				// ***********
				//
				else if(z.ty=="obj"+"ect-Object" || z.ty=="obj"+"ect-ALiBS.to.rs")
				{
//console.log("passou1");
					z.t0.rs[i].f03 = z.s0; // size
					//
					for(var j in z.t0.rs[i].f01) // pointer or value
					{
						z.t0.rs[z.t0.rs.length] = 
						{
							f00:""+z.t0.rs[i].f00+'.'+j, // variable name
							f01:z.t0.rs[i].f01[j] // pointer or value
						};
						//
						z.t0.rs[i].f03 += z.s0+(""+z.t0.rs[i].f01[j]).length; // size
					}
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = 0;
				}
				//
				// ***********
				//
				else
				{
					// TODO(2013-06-10):WHAT?
					//
					z.t0.rs[i].f03 += z.s0+z.t0.rs[i].f01.length; // size
					//
					z.t0.rs[i].f01 = "";
					//
					z.t0.rs[i].f04 = -1; // else
				}
			}
			//
			// ****************
			//
			else // TODO(2013-06-10): what other types?
			{
				z.t0.rs[i].f03 = z.s0; // size
				//
				z.t0.rs[i].f01 = "";
				//
				z.t0.rs[i].f04 = -2; // else
			}
		}
		//
		// ************************
		//
		var r; // [BEST-PRACTICE]
		//
		if(z.rt==0) // return size
		{
			r = 0;
			//
			if(ALiBS.to)
			{
				for(var i=0;i<z.t0.rs.length;i++)
				{
					z.r0 = z.t0.ft[z.t0.tb.fa](z.t0,i);
					//
//console.log(z.r0)
					if(z.r0>0) // OK = sum
					{
						//z.r0; // {} // OK
						r += z.t0.rs[i].f03;
					}
					else // NOT OK = do NOT sum
					{
						//
					}
					//else if(z.r0==0) continue; // ALiBS.to.f0 and ALiBS.to.f1
					//else if(z.r0==-1) continue; // field filter
					//else if(z.r0==-2) break;
				}
			}
			else
			{
				for(var i=0;i<z.t0.rs.length;i++)
				{
					if(z.t0.rs[i].f04>=z.rt2) // return type 2
					{
						r += z.t0.rs[i].f03;
					}
				}
			}
		}
		else if(z.rt==1) // return object
		{
			if(ALiBS.to)
			{
//console.log("z.t0.tb.fa="+z.t0.tb.fa)
//console.log("z.t0.tb.fc="+z.t0.tb.fc)
				for(var i=0;i<z.t0.rs.length;i++)
				{
					z.r0 = z.t0.ft[z.t0.tb.fa](z.t0,i);
//console.log("z.r0="+z.r0)
//console.log("z.t0.rs[i].f04="+z.t0.rs[i].f04)
					//
					if(z.r0>0) // OK = keep
					{
						z.r0; // {} // OK
					}
					else // NOT OK = delete
					{
						z.t0.rs[i] = null; // entire record
						z.nu++;
					}
					//else if(z.r0==0) continue; // ALiBS.to.f0 and ALiBS.to.f1
					//else if(z.r0==-1) continue; // field filter
					//else if(z.r0==-2) break;
//console.log("passou")
				}
			}
			else
			{
				for(var i=0;i<z.t0.rs.length;i++)
				{
					if(z.t0.rs[i].f04<z.rt2) // return type 2
					{
						z.t0.rs[i] = null; // entire record
						z.nu++;
					}
				}
			}
			//
			if(z.nu>0) // records deleted
			{
				z.t0.rs.sort();
				z.t0.rs.length = z.t0.rs.length - z.nu;
			}
			//
			z.f0 = function()
				{
					return z.t0;
				}; // copy object
			//
			r = new z.f0();
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	sd: function() // show dependencies
	{
		/* Dependencies: 
		grep -i "ALiBS." ALiBS.sy.js | grep -v "ALiBS.sy" | grep -v "ALiBS.gv" | grep -v "ALiBS.ag" | grep -v "ALiBS.ap" | grep -v "ALiBS.ax" | uniq | sort

		grep -i "ALiBS." ALiBS.sg.js | grep -v "ALiBS.sg" | grep -v "ALiBS.gv" | uniq | sort

		grep -E "ALiBS\.[a-zA-Z0-9]{1,2}\.[a-zA-Z0-9]{1,3}\(" ALiBS.sy.js > dep.txt

		grep -E "ALiBS\.[a-zA-Z0-9]{1,2}\.[a-zA-Z0-9]{1,3}\(" ALiBS.el.js > dep.txt */
		//
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.sd",le:ALiBS.gv.sy.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.md = {};
		z.sp;
		//
		for(var i in ALiBS.gv.sy.mc)
		{
			if(ALiBS[i.replace("ALiBS\.","")])
			{
				ALiBS.sy.la(5+0,"ALiBS.gv.sy.mc["+i+"].f02="+ALiBS.gv.sy.mc[i].f02,{o:"ALiBS.sy.sd",le:ALiBS.gv.sy.le});
				//
				z.sp = ALiBS.gv.sy.mc[i].f02.split(",");
				//
				for(var j=0;j<z.sp.length;j++)
				{
					if(z.sp[j])
					{
						if(z.sp[j].length==2)
						{
							z.md[z.sp[j]] = 0;
						}
						else
						{
							z.md[z.sp[j].substr(0,2)] = 0;
						}
					}
				}
			}			
		}
		//
		ALiBS.sy.la(5+0,ALiBS.gv.sy.se,{o:"ALiBS.sy.sd",le:ALiBS.gv.sy.le});
		//
		for(var i in z.md)
		{
			if(!ALiBS[i])
			{
				ALiBS.sy.la(5+0,"NOT LOADED module ALiBS."+i,{o:"ALiBS.sy.sd",le:ALiBS.gv.sy.le});
			}
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return;
	},
	// *****************************************************************
	lrl: function(p) // list releases
	// p = (optional)(JSON)parameters {} 
	//
	// return 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.lrl",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			for(var i in ALiBS.gv)
			{
				z.ec = -1010; // 
				if(ALiBS.gv[i] && ALiBS.gv[i]["rl"])
				{
					z.ec = -1020; // 
					ALiBS.sy.la(5+0,"ALiBS.gv."+i+".rl="+ALiBS.gv[i]["rl"],{o:"ALiBS.sy.lrl",le:ALiBS.gv.sy.le});
				}
			}
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sy.lrl",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	fev: function(s0,p) // function evaluate string = fev() = eval() // DO NOT DOCUMENT
	// s0 = (required)(string)string to evaluate
	// p = (optional)(JSON)parameters {} 
	//
	// return (number)1=OK , -1=NOK
	/*
		s0 = "x0 = 100; x1 = 100";
		ALiBS.sy.fev(s0);
		// cria x0 e x1
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.fev",le:ALiBS.gv.sy.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			z.ec = -1102; // 
			z.f0 = new Function(s0);
			//
			z.ec = -1201; // 
			z.f0();
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sy.fev",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	}
	// *****************************************************************
};
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_sy_la")>=0) ALiBS_sy_la = ALiBS.sy.la; // pointer (do NOT use var)
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_sy_no")>=0) ALiBS_sy_no = ALiBS.sy.no; // pointer (do NOT use var)
		//
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_sy_la_print")>=0) // print log
		{
			ALiBS_sy_la_print = function() // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_sy_la_print",le:ALiBS.gv.sy.le});
				//
				ALiBS.sy["la"](1+0,"Printing Log...",{so:"print"}); // print log
				//
				return "";
			};
		}
	}
}
else if(ALiBS.gv.sy.ev=="Browser") // http://www.quirksmode.org/js/events_properties.html
{
	if(ALiBS.gv.sy.wdkd==1 || !ALiBS.gv.sy.wdkd) // DEFAULT
	{
		window.document.onkeydown = ALiBS.sy.kc; 
	}
	else if(ALiBS.gv.sy.wdkd==2) 
	{
		window.document.onkeydown = function(e)
		{
		}; 
	}
	else 
	{
		// NO ACTION
	}
	//
	// *******************
	//
	if(ALiBS.gv.sy.wdkp==1) 
	{
		window.document.onkeypress = ALiBS.sy.kc; // onkeypress não pega os eventos nas teclas ALT,CTRL,ALTGR,ESC
	}
	else if(ALiBS.gv.sy.wdkp==2) 
	{
		window.document.onkeypress = function(e)
		{
		}; 
	}
	else // DEFAULT
	{
		// NO ACTION
	}
	//
	// *******************
	//
	if(ALiBS.gv.sy.wdku==1) 
	{
		window.document.onkeyup = ALiBS.sy.kc; 
	}
	else if(ALiBS.gv.sy.wdku==2) 
	{
		window.document.onkeyup = function(e)
		{
		}; 
	}
	else // DEFAULT
	{
		// NO ACTION
	}
	//
	// [BEST-PRACTICE] Use only onkeydown and set onkeypress=onkeyup=null; or onkeypress=onkeyup=function(e){};
	//
	// do NOT use this way = duplicated executions
	// window.document.onkeypress = window.document.onkeydown = window.document.onkeyup = ALiBS.sy.kc; 
	//
	// to overwrite events on document object
	//window.document.onkeypress = function(e){}; 
	//window.document.onkeydown = function(e){};
	//window.document.onkeyup = function(e){}; 
	//
	// *******************
	//
	// this has been commented because Chrome says "captureEvents has been deprecated" (NOT needed)
/*	if(window.document.captureEvents && typeof(Event)!="undefined") 
	{
		window.document.captureEvents(Event.KEYDOWN|Event.KEYUP|Event.KEYPRESS|Event.keydown|Event.keyup|Event.keypress);
	}
*/
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.sy",{o:"ALiBS.sy"});
ALiBS.sy.la(0+0,"ALiBS.gv.sy.rl="+ALiBS.gv.sy.rl,{o:"ALiBS.sy"}); // Release
//
ALiBS.sy.la(0+0,"ALiBS.gv.sy.ev="+ALiBS.gv.sy.ev,{o:"ALiBS.sy"});
//
if(ALiBS.gv.sy.ev=="Server" && typeof(ALiBS.gv.sy.logFile)!="undefined")
{
	ALiBS.sy.la(0+0,"ALiBS.gv.sy.logFile="+ALiBS.gv.sy.logFile,{o:"ALiBS.sy"}); 
	ALiBS.sy.la(0+0,"ALiBS.gv.sy.logFile_keep="+ALiBS.gv.sy.logFile_keep,{o:"ALiBS.sy"}); // TODO(2015-08-14): keep is used anywhere ?
}
//
ALiBS.sy.la(0+0,"ALiBS.gv.sy.sygl="+ALiBS.gv.sy.sygl,{o:"ALiBS.sy"});
ALiBS.sy.la(0+0,"ALiBS.sy.gl="+ALiBS.sy.gl,{o:"ALiBS.sy"});
//
ALiBS.sy.la(0+0,"ALiBS.gv.sy.le="+ALiBS.gv.sy.le,{o:"ALiBS.sy"}); 
//
ALiBS.sy.la(0+0,"ALiBS.gv.sy.syli=["+ALiBS.gv.sy.syli+"]",{o:"ALiBS.sy"});
ALiBS.sy.la(0+0,"ALiBS.sy.li=["+ALiBS.sy.li+"]",{o:"ALiBS.sy"});
//
ALiBS.sy.la(0+0,"ALiBS.gv.sy.glx="+ALiBS.gv.sy.glx+"",{o:"ALiBS.sy"});
ALiBS.sy.la(0+0,"ALiBS.gv.sy.glu="+ALiBS.gv.sy.glu+"",{o:"ALiBS.sy"});
ALiBS.sy.la(0+0,"ALiBS.gv.sy.gle="+ALiBS.gv.sy.gle+"",{o:"ALiBS.sy"});
ALiBS.sy.la(0+0,"ALiBS.gv.sy.gll="+ALiBS.gv.sy.gll+""+ALiBS.gv.sy.se,{o:"ALiBS.sy"});
//
//
//
/* *************************************************************************
// INFO:

Opcao 1:  (ALIBS.ag)
j = {...}; // global data or function in loaded file
f1(); // callback function in loaded file - f1() uses j (data)

Opcao 2:  (ALIBS.ag)
f1(j = {...}); // callback function and local data in loaded file - like YAHOO callback function

Opcao 3:  (ALIBS.ag)
j = {...}; // global data or function in loaded file
var t = new ALiBS.ag.tc();
t.cb = f1(){}; // callback function called using ALiBS.ag.tc

Opcao 4:  (ALIBS.ax)
j = {...}; // local data or text in loaded file
var t = new ALiBS.ax();
t.cb = f1(t){}; // callback function called by ALiBS.ax() 

// ***************************

ALiBS.ag advantages over ALiBS.ax (XMLHttpRequest): 

1) Can request a file from anywhere on the net, not just the server your page was loaded from. 
2) Works in IE even when ActiveX is turned off (though not when Javascript is turned off). 
3) Works with a few older browsers that don't support XMLHttpRequest, like Opera 7 (though not Macintosh versions of IE). 

// ***************************

ALiBS.ag disadvantages over ALiBS.ax (XMLHttpRequest):

1) Returned data has to be formatted as Javascript code. 
   XMLHttpRequest() can be used to fetch data in any format, XML, JSON, plain text, or whatever. 
   (FALSE, ALiBS.ag can handle scripts, css, text and images)

2) Can only do GET requests, not POST requests. 
   (COULD NOT BE A PROBLEM, ALiBS.ag can run a HTTP request to JSP passing parameters AND JSP can do everything. DONE in iManager)

3) Functions and variables defined by loading a file by creating a new <script> tag are always created in the global context, 
   while those created with an eval() call are defined in the current context. 
   (FALSE, ALiBS.ag can load a script/function that updates a ALiBS.to object internally)

3) Whether the request is synchronous or asynchronous is pot luck, depending on the browser. XMLHttpRequest() can control this. 
   (NOT A PROBLEM, most requests should be asynchronous, and ALiBS.ag can control with ALiBS.cb OR callback() function)

4) When fetching JSON data from an untrusted source, there is no possiblity of checking the data before feeding it to the Javascript parser. 
   With XMLHttpRequest() you can parse the data with something like json2.js instead of eval() for secure parsing.
   (FALSE, ALiBS.ag can upload as TEXT (iframe)) 

// ***************************

dynamic <script> tags:
1) To load additional blocks of code 
2) To fetch JSON data from trusted source (trusted because may have some dificulties to secure source)

XMLHttpRequest():
1) To fetch JSON/XML/TEXT data from untrusted source (untrusted because data can be validated before use)

// ***************************

var t = new ALiBS.ag.tc(100,100);
t.cb = function()
{
	if(!ALiBS.gv.it.pd || ALiBS.gv.it.pd==0) 
	{
		ALiBS.gv.it.pd = 1; // language changed/processed on page
		//
		if(a.cl) a.cl();
	}
	else _FORCE_ERROR_origin(); // generate error;
};
t.ex();

// ***************************

// var t = new ALiBS.ag.tc(5,100);
// t do not need to be global, but its a good practice, to have control of name colision, once it (local var t) is kept until timeout is finished
// All variables called inside cb() or af() MUST be global or created inside object t (t.var = x;)

ALiBSA.tc.t0 = new ALiBS.ag.tc(5,100);
ALiBSA.tc.t0.ce = "[exception](t0)";
ALiBSA.tc.t0.cn = "[cb](t0)";
ALiBSA.tc.t0.ca = "[af](t0)";
ALiBSA.tc.t0.nn = 0; // custom counter
ALiBSA.tc.t0.cb = function()
{
	ALiBS.el.si("iDivTc_0","iDivTc_0["+ALiBSA.tc.t0.n+"]["+(ALiBSA.tc.t0.nn+=10)+"]");

	if(ALiBSA.tc.t0.n>=5) 
	{
		//ALiBS.el.si("iDivTc_0","iDivTc_0["+ALiBSA.tc.t0.n+"]["+(ALiBSA.tc.t0.nn+=10)+"]");
	}
	else _FORCE_ERROR_origin(); // generate error;
};
ALiBSA.tc.t0.af = function()
{
	P.st.writeResult("[tc]_[af](iDivTc_0["+ALiBSA.tc.t0.v+"])",ALiBSA.tc.t0.v,"");
};
ALiBSA.tc.t0.ex();

// *************************************************************************
// TODO:

Pendencias:
- funcionar no internet explorer

O que significa a linha abaixo? posso apagar?

ALiBS.gv.mc["ALiBS.ag"] = window.document.body;

*/
//
// *************************************************************************
//
ALiBS.gv.ag = {}; // global values 
//
ALiBS.gv.ag.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ag.le = 0; // local log status
//
if(typeof(ALiBS_gv_ag_le)!="undefined") ALiBS.gv.ag.le = ALiBS_gv_ag_le;
//
ALiBS.gv.ag.ol = new Array(); // onload functions cache
//
//TODO(2011-06-20):remover???? o certo é ag.ni e ag.nd????
//ALiBS.gv.sy.ni = 0; // script/css/text auto next id number
//ALiBS.gv.sy.nd = -10; // next id number to delete (tag)
//
ALiBS.gv.ag.ni = 0; // script/css/text auto next id number
ALiBS.gv.ag.nd = -10; // next id number to delete (tag)
//
// *************************************************************************
//
if(typeof(setTimeout)=="undefined" || typeof(clearTimeout)=="undefined")
{
        clearTimeout = function(th) // id
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.clearTimeout",le:ALiBS.gv.ag.le});
		//
		//ALiBS.sy.la(5+0,"th.isAlive()="+th.isAlive(),{o:"ALiBS.ag.clearTimeout",le:ALiBS.gv.ag.le});
		//ALiBS.sy.la(5+0,"th.activeCount()="+th.activeCount(),{o:"ALiBS.ag.clearTimeout",le:ALiBS.gv.ag.le});
		//ALiBS.sy.la(5+0,"th.isInterrupted()="+th.isInterrupted(),{o:"ALiBS.ag.clearTimeout",le:ALiBS.gv.ag.le});
		//
		ALiBS.sy.la(5+0,"(1)th.getName()="+th.getName(),{o:"ALiBS.ag.clearTimeout",le:ALiBS.gv.ag.le});
		//
		th.setName(th.getName()+"-[THREAD-DISABLED]");
		//
		ALiBS.sy.la(5+0,"(2)th.getName()="+th.getName(),{o:"ALiBS.ag.clearTimeout",le:ALiBS.gv.ag.le});
		//
		th = null;
		//
		//th.stop;
		//
		return;
        };
	//
        setTimeout = function(f,d) // function,timeout
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.setTimeout",le:ALiBS.gv.ag.le});
		//
                var f2 = 
		{
                        t:d, // timeout
                        s:false, // sleeping status
			c:null, // current thread 
			//               
                        run:function()
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.setTimeout.run",le:ALiBS.gv.ag.le});
				//
				this.c = java.lang.Thread.currentThread();
				//
				ALiBS.sy.la(5+0,"currentThread()="+this.c,{o:"ALiBS.ag.setTimeout.run",le:ALiBS.gv.ag.le});
				//
				ALiBS.sy.la(5+0,"getName()="+this.c.getName(),{o:"ALiBS.ag.setTimeout",le:ALiBS.gv.ag.le});
				//
				if(this.c.getName().indexOf("[THREAD-DISABLED]")<0) // NOT Disabled
				{
					ALiBS.sy.la(5+0,"Thread Enabled",{o:"ALiBS.ag.setTimeout",le:ALiBS.gv.ag.le});
					//
		                        if(!this.s)
					{
		                                this.s=true;
		                                java.lang.Thread.currentThread().sleep(this.t);
		                        }
		                        try
					{
						f(); 
					}
					catch(e)
					{
						ALiBS.sy.la(1+0,"ALiBS.ag Exception(java.lang.Thread):"+e,{o:"ALiBS.ag.setTimeout.run"});
					} 
				}
				else
				{
					ALiBS.sy.la(5+0,"Thread Disabled",{o:"ALiBS.ag.setTimeout",le:ALiBS.gv.ag.le});
				}
				//
				return;
                        }
                };
		//       
                var th = new java.lang.Thread(new java.lang.Runnable(f2));
		//
                th.start();
		//
		return th;
        };
}
// *************************************************************************
//
ALiBS.ag = // ajax get
{
	ex: function(u,t,p) // execute, run
	// u = (required)(String)url
	// t = (required)(String)type
	//	s = script (from Client)
	//	c = css (from Client)
	//	y = style (from Client)
	//	f = frame (from Client)
	//	i = image (from Client)
	//	v = from Server
	//	x = from ALiBS.ax.ex
	//
	// p = (optional)(JSON)parameters 
	//	i = (String)element id to be created
	//	ut = (boolean)use timestamp (Default = false)
	//	rl = (boolean)use release/subversion (Default = true)
	//	zpf = (String)zip all after parameter ("&p1") - CLIENT - ALWAYS USE & in this parameter
	//	fr = (number) FORCE return, used to TEST t = s,c,f,i 
	//		0 = Default = DO NOT force
	//		1 = FORCE return = url
	//		2 = FORCE return = id
	//	sty = (String)style ""
	//		ALiBS.ag.ex("","y",{i:"sty0",sty:"body {color:red;} textarea {color:blue;}"})
	//		ALiBS.ag.ex("","y",{i:"sty0",sty:".hg_css0ah0 th {background-color:red; color:blue;}"})
	//		ALiBS.ag.ex("","y",{i:"tagStyle0",sty:"textarea.novaclasse {color:green;}"})
	//		ALiBS.el.ge("tagStyle0").innerHTML="textarea.x2 {color:yellow}"
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		//
		if(typeof(u)=="string")
		{
			// none
		}
		else if(typeof(u)=="boolean" || typeof(u)=="number")
		{
			u = ""; // overwrite
		}
		else // object = should be an Array
		{
			for(var i=0;i<u.length;i++)
			{
				u[i] = ALiBS.ag.en(u[i]); // replace &
			}
			//
			u = "&" + u.join("&"); // & used to split
			//
			ALiBS.sy.la(5+0,"(object)u="+u,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		}
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.i)!="string" || z.i=="") z.i = "iAuto"+(ALiBS.gv.ag.ni++); // (String)element id to be created
		if(typeof(z.ut)!="boolean") z.ut = false; // (boolean)use timestamp (Default = false)
		if(typeof(z.rl)!="boolean") z.rl = true; // (boolean)use release/subversion (Default = true)
		if(typeof(z.zpf)!="string") z.zpf = ""; // (String)zip content from (Default = DO NOT ZIP on CLIENT)
		if(typeof(z.fr)!="number") z.fr = 0; // (number)FORCE return (Default = 0 = DO NOT force)
		//
		ALiBS.sy.la(5+0,"z.i="+z.i,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"z.ut="+z.ut,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"z.rl="+z.rl,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"z.zpf="+z.zpf,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		//
		// local varibles [BEST-PRACTICE]
		z.id = null; // id of body = shoud use this method to get element.id because ALiBS.el is not loaded yet
		//
		z.au = new Array(); // new URL
		//
		// *********************************************************
		//
		z.au[z.au.length] = u; // original URL
		//
		if(t=="v")
		{
			z.au[z.au.length] = "&_scF=server"; // sc = source - SERVER - FROM
		}
		else if(t=="x")
		{
			if(u.indexOf("?")>0) // exists
			{
				z.au[z.au.length] = "&_scF=ax.ex"; // sc = source - CLIENT - FROM
			}
			else // NOT exists
			{
				z.au[z.au.length] = "?_scF=ax.ex"; // sc = source - CLIENT - FROM
			}
		}
		else // ALiBS.ag.ex
		{
			if(u.indexOf("?")>0) // exists
			{
				z.au[z.au.length] = "&_scF=ag.ex"; // sc = source - CLIENT - FROM
			}
			else // NOT exists
			{
				z.au[z.au.length] = "?_scF=ag.ex"; // sc = source - CLIENT - FROM
			}
		}
		//
		// ************************
		//
		z.ok = 0; // DO NOT LOAD IMAGES
		//
		if(z.rl || u.indexOf("ALiBS")>=0 ) // add release (s,f,i,c) - release/subversion (Default = true) - ALWAYS for ALiBS
		{
			z.ok = 1; // NOT IMAGE + do NOT add HMAC
			//
			z.au[z.au.length] = "&_rlF=" + ALiBS.gv.sy.rl; // add release/subversion to FORCE download and to avoid brower cache - FROM
		}
		//
		// ************************
		//
		if(z.ut || t=="v" || t=="x" || t=="f") // ALWAYS add timestamp 
		{
			z.ok = 2; // NOT IMAGE + ADD HMAC TODO(2013-05-15): com "f" talvez nao funcione se ADD HMAC
			//
			z.au[z.au.length] = "&_tsF=" + (new Date()-1); // add timestamp to FORCE download and to avoid brower cache - FROM
		}
		else if(t=="s")
		{
			if(u.indexOf(".jsp")>0) // ALWAYS add timestamp to queries
			{
				z.ok = 2; // NOT IMAGE + ADD HMAC
				//
				z.au[z.au.length] = "&_tsF=" + (new Date()-1); // add timestamp to FORCE download and to avoid brower cache - FROM
			}
			else if(u.indexOf(".js")<0) // NOT js = ALWAYS add timestamp to queries
			{
				z.ok = 2; // NOT IMAGE + ADD HMAC
				//
				z.au[z.au.length] = "&_tsF=" + (new Date()-1); // add timestamp to FORCE download and to avoid brower cache - FROM
			}
			else // script.js OR ALiBS.xx.js
			{
				z.ok = 0; // do NOT ZIP + do NOT add HMAC
			}
		}
		else
		{
			z.ok = 0; // do NOT ZIP + do NOT add HMAC
		}
		//
		//
		// ************************
		//
		if(z.ok) // do NOT IMAGE ZIP IMAGE
		{
			if(z.zpf) 
			{
				if(ALiBS.b0)
				{
					z.u2 = z.au.join("");
					//
					z.nn = z.u2.indexOf(z.zpf);
					//
					if(z.nn>=0)
					{
						z.u0 = z.u2.substring(0,z.nn);
						//
						z.u1 = z.u2.substring(z.nn,z.u2.length);
						//
						z._lC0 = z.u1.length; // only the part to be zipped 
						//
						z.en = ALiBS.b0.en(z.u1,{zp:1,b64:1,b64n:"uri"});
						//
						z._lC1 = z.en.length; // only the zipped part 
						//
						// ******************
						//
						z.au.length = 0;
						//
						z.au[z.au.length] = z.u0;
						//
						if(z.u1.charAt(0)=="&") // &cmd
						{
							z.au[z.au.length] = "&zp=" + z.en;
						}
						else // cmd = zip did NOT start from "&" || "?" = Already in part NOT zipped
						{
							z.au[z.au.length] = "zp=" + z.en;
						}
						//
						ALiBS.sy.la(5+0,"z.u0="+z.u0,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
						ALiBS.sy.la(5+0,"z.u1="+z.u1,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
					}
					//
					z.au[z.au.length] = "&_lF0=" + z._lC0; // length 0 = original - FROM - TODO(2013-05-10):VALIDATE
					//
					z.au[z.au.length] = "&_lF1=" + z._lC1; // length 1 = after zip - FROM - TODO(2013-05-10):VALIDATE
					//
					z.au[z.au.length] = "&_lFp=" + Math.round((z._lC1)/(z._lC0)*1000)/10 + "p_Final"; // percentage - FROM - TODO(2013-05-10):VALIDATE
				}
				else
				{
					ALiBS.sy.la(5+0,"ALiBS.b0 NOT loaded",{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
				}
			}
			else
			{
				ALiBS.sy.la(5+0,"z.zpf = [EMPTY]",{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
			}
			//
			if(z.ok==2) // add HMAC
			{
				z.au[z.au.length] = "&_hmF=" + "0"; // HMAC = after zip - FROM - TODO(2013-05-10):VALIDATE
			}
		}
		//
		z.auj = z.au.join("");
		//
		ALiBS.sy.la(5+0,"z.au="+z.auj,{o:"ALiBS.ag.ex",le:ALiBS.gv.ag.le});
		//
		z.ok2 = 0; // came from ALiBS.ag.ex
		//
		// *********************************************************
		//
		if(z.fr) // FORCE return, used to TEST t = s,c,f,i
		{
			if(z.fr==1) // return url
			{
				z.i = z.auj;
			}
			else // ==2 return id
			{
				// z.i = z.i;
			}
		}
		else if(t=="v") // server
		{
			z.i = z.auj;
		}
		else if(t=="x") // ALiBS.ax.ex
		{
			z.i = z.auj;
		}
		else if(t=="s") // script = <script id="" type="text/javascript" charset="utf-8" src="P_loadModules.js"></script>
		{
			var e = window.document.createElement('script');
			e.id = z.i;
			//
			e.type="text/javascript";
			e.charset="utf-8";
			e.src = z.auj;
			//
			z.ok2 = 1;
		}
		else if(t=="c") // css = <link id="" type="text/css" rel="stylesheet" href="mn00.css">
		{
			var e = window.document.createElement('link');
			e.id = z.i;
			//
			e.type="text/css";
			e.rel = "stylesheet";
			e.href = z.auj;
			//
			z.ok2 = 1;
		}
		else if(t=="y") // style = <style id="" type="text/css">
		{
			var e = window.document.createElement('style');
			e.id = z.i;
			//
			e.type="text/css";
			e.innerText = z.sty; // chrome
			e.innerHTML = z.sty; // firefox
			//
			z.ok2 = 1;
		}
		else if(t=="f") // iframe (text) = <iframe  id="" style="position:absolute;visibility:hidden;" src="texto.txt">
		{
			var e = window.document.createElement('iframe');
			e.id = z.i;
			//
			//e.charset="utf-8"; // TODO:tentar consertar problema com acentos no firefox
			e.style.visibility="hidden";
			e.visibility = 'hidden';
			e.src = z.auj;
			//
			z.ok2 = 1;
		}
		//
		else if(t=="i") // img pre-load = <img  id="" style="position:absolute;visibility:hidden;" src="img.gif">
		{
			var e = window.document.createElement('img');
			e.id = z.i;
			//
			e.style.visibility="hidden";
			e.visibility = 'hidden';
			e.src = z.auj;
			//
			z.ok2 = 1;
		}
		//
		// *********************************************************
		//
		if(z.ok2==1)
		{
			if(!z.id)
			{
				if(t=="y") // style
				{
					if(window.document.head) z.id = window.document.head;
					else if(window.document.getElementsByTagName) z.id = window.document.getElementsByTagName("head").item(0);
					else if(window.document.all) z.id = window.document.all.tags("head");
				}
				else
				{
					if(window.document.body) z.id = window.document.body;
					else if(window.document.getElementsByTagName) z.id = window.document.getElementsByTagName("body").item(0);
					else if(window.document.all) z.id = window.document.all.tags("body");
					//
					else if(window.document.head) z.id = window.document.head;
					else if(window.document.getElementsByTagName) z.id = window.document.getElementsByTagName("head").item(0);
					else if(window.document.all) z.id = window.document.all.tags("head");
				}
			}
			//
			if(e) z.id.appendChild(e); 
		}
		//
		var r = z.i; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; // ID OR new url
	},
	// *****************************************************************
	en: function(u,p) // encapsulate & inside data from CLIENT and/or SERVER - & is used as separator
	// u = (required)(String)text
	// p = (optional)(JSON)parameters 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.en",le:ALiBS.gv.ag.le});
		//
		return u.replace(/&/gi,"[x12]");
	},
	// *****************************************************************
	de: function(u,p) // de-encapsulate & inside data from CLIENT and/or SERVER - & is used as separator
	// u = (required)(String)text
	// p = (optional)(JSON)parameters 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.de",le:ALiBS.gv.ag.le});
		//
		return u.replace(/\[x12\]/gi,"&");
	},
	// *****************************************************************
	uz: function(u,p) // unzip url
	// u = (required)(String)url
	// p = (optional)(JSON)parameters 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = {};
		//
		// ************************
		//
		r["_rlT"] = ALiBS.gv.sy.rl; // host - TO
		r["_tsT"] = (new Date()-1); // timestamp - TO
		r["_hmT"] = "0"; // HMAC  - TO
		//
		// ************************
		//
		z.nn = u.indexOf("?"); // when coming from CLIENT
		//
		if(z.nn<0) // when coming from SERVER 
		{
			ALiBS.sy.la(5+0,"coming from SERVER",{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
			//
			z.u0 = "server";
			z.u1 = u;
		}
		else // z.nn>=0 when coming from CLIENT
		{
			ALiBS.sy.la(5+0,"coming from CLIENT",{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
			//
			z.u0 = u.substring(0,z.nn); // url
			z.u1 = u.substring(z.nn+1,u.length);
			//
			if(!z.u0) z.u0 = "client";
		}
		//
		r["_hoT"] = z.u0; // host - SERVER
		//
		z.u2 = z.u1.split("&"); // ALWAYS use & as ALiBS.ag.ex ALWAYS uses & in new parameters (&_tsF,&_rlF), either for CLIENT or for SERVER
		//
		for(var i=0;i<z.u2.length;i++)
		{
			z.u2[i] = ALiBS.sg.tr(z.u2[i],{o:"?"});
			//
			z.nn = z.u2[i].indexOf("=");
			//
			if(z.nn>0)
			{
				r[z.u2[i].substring(0,z.nn)] = ALiBS.ag.de(z.u2[i].substring(z.nn+1,z.u2[i].length));
			}
		}
		//
		ALiBS.sy.la(5+0,"u="+u,{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"z.u0="+z.u0,{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"z.u1="+z.u1,{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
		//
		// ************************
		//
		if(r.zp)
		{
			if(ALiBS.b0)
			{
				z._lS0 = r.zp.length; // only the zipped part - TO
				//
				z.de = ALiBS.b0.de(r.zp,{zp:1,b64:1,b64n:"uri"});
				//
				z._lS1 = z.de.length; // only the zipped part - TO
				//
				z.u2 = z.de.split("&"); // ALWAYS use & as ALiBS.ag.ex ALWAYS uses & in new parameters (&_tsF,&_rlF), either for CLIENT or for SERVER
				//
				for(var i=0;i<z.u2.length;i++)
				{
					z.nn = z.u2[i].indexOf("=");
					//
					if(z.nn>0)
					{
						r[z.u2[i].substring(0,z.nn)] = ALiBS.ag.de(z.u2[i].substring(z.nn+1,z.u2[i].length));
					}
				}
				//
				r["_lT0"] = z._lS0; // length 0  - SERVER
				//
				r["_lT1"] = z._lS1; // length 1  - SERVER
			}
			else
			{
				// nothing
			}
		}
		//
		for(var i in r)
		{
			ALiBS.sy.la(5+0,i+"="+r[i],{o:"ALiBS.ag.uz",le:ALiBS.gv.ag.le});
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; // ID		
	},
	// *****************************************************************
	rm0: function() // remove (ALL) (up to now) scripts, css, text and img tags loaded from head
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
		//
		ALiBS.sy.la(5+0,"ALiBS.gv.ag.ni="+ALiBS.gv.ag.ni,{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"ALiBS.gv.ag.nd="+ALiBS.gv.ag.nd,{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
		//
		var e = null;
		var h = ALiBS.el.ge('head',{c:false});
		var b = ALiBS.el.ge('body',{c:false});
		//
		for(var i=0;i<=100;i++) // remove iScpt0...100 from head
		{
			e = ALiBS.el.ge('iScpt'+i,{c:false});
			//
			if(e)
			{
				try
				{
					b.removeChild(e);
					ALiBS.sy.la(7+0,"Removing body.iScpt"+i,{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
				}
				catch(e)
				{
					try
					{
						h.removeChild(e);
						ALiBS.sy.la(7+0,"Removing head.iScpt"+i,{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
					}
					catch(e)
					{
						ALiBS.sy.la(7+0,"Element iScpt"+i+" does NOT EXIT",{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le}); // 4 = info - NOT a real ERROR
					}
				}
			}
			else 
			{
				ALiBS.sy.la(7+0,"Element iScpt"+i+" NOT FOUND",{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le}); // 4 = info - NOT a real ERROR
				break;
			}
		}
		//
		for(var i=0;i<=100;i++) // remove iAuto0...100 from head 
		{
			e = ALiBS.el.ge('iAuto'+i,{c:false});
			//
			if(e)
			{
				if(!e.href) // NOT css
				{
					try
					{
						b.removeChild(e);
						ALiBS.sy.la(7+0,"Removing body.iAuto"+i,{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
					}
					catch(e)
					{
						try
						{
							h.removeChild(e);
							ALiBS.sy.la(7+0,"Removing head.iAuto"+i,{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le});
						}
						catch(e)
						{
							ALiBS.sy.la(7+0,"Element iAuto"+i+" does NOT EXIT",{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le}); // 4 = info - NOT a real ERROR
						}
					}
				}
				else
				{
					ALiBS.sy.la(7+0,"Element iAuto"+i+" is CSS and CAN NOT be removed",{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le}); // 4 = info - NOT a real ERROR
				}
			}
			else 
			{
				ALiBS.sy.la(7+0,"Element iAuto"+i+" NOT FOUND",{o:"ALiBS.ag.rm0",le:ALiBS.gv.ag.le}); // 4 = info - NOT a real ERROR
				break;
			}
		}
		//
		return;
	},
	// *****************************************************************
/*	rm1: function() // remove (1) script, css, text and img tag loaded from body
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"ALiBS.gv.ag.ni="+ALiBS.gv.ag.ni,{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"ALiBS.gv.ag.nd="+ALiBS.gv.ag.nd,{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
		//
		if(ALiBS.gv.ag.nd>=0 && ALiBS.gv.ag.nd<ALiBS.gv.ag.ni)
		{
			var e = null;
			var b = ALiBS.el.ge('body');
			//
			e = ALiBS.el.ge('iScpt'+ALiBS.gv.ag.nd);
			//
			if(e)
			{
				try
				{
					b.removeChild(e);
					ALiBS.sy.la(5+0,"removing iScpt"+ALiBS.gv.ag.nd,{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
				}
				catch(e)
				{
					ALiBS.sy.la(1+0,"ALiBS.ag.rm1()[0] exception(e):"+e,{o:"ALiBS.ag"});
				}
			}
			else 
			{
				ALiBS.sy.la(5+0,"no iScpt"+ALiBS.gv.ag.nd,{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
			}
			//
			//
			e = ALiBS.el.ge('iAuto'+ALiBS.gv.ag.nd);
			//
			if(e)
			{
				try
				{
					b.removeChild(e);
					ALiBS.sy.la(5+0,"removing iAuto"+ALiBS.gv.ag.nd,{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
				}
				catch(e)
				{
					ALiBS.sy.la(1+0,"ALiBS.ag.rm1[1] exception(e):"+e,{o:"ALiBS.ag"});
				}
			}
			else 
			{
				ALiBS.sy.la(5+0,"no iAuto"+ALiBS.gv.ag.nd,{o:"ALiBS.ag.rm1",le:ALiBS.gv.ag.le});
			}
		}
		//
		if(ALiBS.gv.ag.nd<ALiBS.gv.ag.ni) ALiBS.gv.ag.nd++; // 
	},*/
	// *****************************************************************
	lm: function(s) // load modules
	// s = (required)(string) module name OR file name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		//
		// TODO(2011-08-08): validar s e retornar erro
		if(typeof(s)!="string")
		{
			ALiBS.sy.la(5+0,"invalid module not loaded s="+s,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		}
		//
		ALiBS.sy.la(5+0,"s="+s,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"ALiBS.gv.sy.mc[s].f00="+ALiBS.gv.sy.mc[s].f00,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		//
		var s1 = s.replace("ALiBS.","");
		ALiBS.sy.la(5+0,"s1="+s1,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		//
		if(!ALiBS[s1] || ALiBS.gv.sy.mc[s].f01==true) // not (!) loaded     OR     FORCE RELOAD = true
		{
			var a = ALiBS.gv.sy.mc[s].f00;
			if(a) // try and load js in cache
			{
				ALiBS.sy.la(5+0,"ALiBS.gv.sy.mp="+ALiBS.gv.sy.mp,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
				ALiBS.sy.la(5+0,"a="+a,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
				//
				if(a.indexOf("/")>=0 || a.indexOf("\\")>=0) // NOT ALiBS
				{
					ALiBS.ag.ex(a,"s"); // do not add path
				}
				else 
				{
					//var mp = ALiBS.gv.sy.mp;
					var a0 = a;
					//
					if(ALiBS.gv.sy.mp.indexOf("-c")>0) // compressed modules in path "ALiBS-c"
					{
						a0 = a0.replace(".js","-c.js");
						//
						ALiBS.sy.la(5+0,"Loading Compressed Module "+a0,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
					}
					else
					{
						ALiBS.sy.la(5+0,"Loading Not Compressed Module "+a,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
					}
					//
					ALiBS.ag.ex(ALiBS.gv.sy.mp+a0,"s"); // add path
				}
			}
			else // try and load js file "s"
			{
				ALiBS.sy.la(5+0,"file="+s,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
				//
				ALiBS.ag.ex(s,"s"); // do not add path
			}
		}
		else 
		{
			ALiBS.sy.la(5+0,"module already loaded = "+s,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		}
		//
		ALiBS.sy.la(6+0,"Method Returned..."+ALiBS.gv.sy.se,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
		//
		return;
	},
	// *****************************************************************
	lc: function(s) // load css
	// s = (required)(string) css name OR file name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.lc",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"s="+s,{o:"ALiBS.ag.lc",le:ALiBS.gv.ag.le});
		//
		if(ss && !ALiBS.gv.ag.ccd[s]) // not null AND not empty AND not loaded
		{
			ALiBS.gv.ag.ccd[s] = true; // TODO: nao vai existir mais este cache
			//
			var a = ALiBS.gv.ag.cc[s];
			if(a) // try and load css in cache
			{
				ALiBS.sy.la(5+0,"a="+a,{o:"ALiBS.ag.lc",le:ALiBS.gv.ag.le});
				//
				if(a.indexOf("/")>=0 || a.indexOf("\\")>=0) ALiBS.ag.ex(a,"c"); // do not add path
				//else ALiBS.ag.ex(ALiBS.gv.sy.hp+ALiBS.sy.ag.sp+"_css/"+a,"c"); // add path
				else ALiBS.ag.ex(ALiBS.gv.sy.sp+a,"c"); // add path
			}
			else // try and load css file "s"
			{
				ALiBS.sy.la(5+0,"file="+s,{o:"ALiBS.ag.lm",le:ALiBS.gv.ag.le});
				//
				ALiBS.ag.ex(s,"c"); // do not add path
			}
		}
		else if(ALiBS.gv.ag.ccd[s])
		{
			ALiBS.sy.la(5+0,"css already loaded = "+s,{o:"ALiBS.ag.lc",le:ALiBS.gv.ag.le}); 
		}
		else
		{
			ALiBS.sy.la(5+0,"invalid css not loaded s="+s,{o:"ALiBS.ag.lc",le:ALiBS.gv.ag.le});
		}
		//
		return;
	},
	// *****************************************************************
	ll: function(s) // load languages
	// s = (required)(string) language name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.ll",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"cl="+ALiBS.gv.it.cl,{o:"ALiBS.ag.ll",le:ALiBS.gv.ag.le});
		ALiBS.sy.la(5+0,"s="+s,{o:"ALiBS.ag.ll",le:ALiBS.gv.ag.le});
		//
		if(s && s != ALiBS.gv.it.cl) // not null AND not empty AND not current language
		{
			var a = ALiBS.gv.sy.lc[s];
			if(a) // try and load language in cache
			{
				// Carregar aqui todas as linguages derivadas, setadas pela aplicação
				// ALiBS.it.sp_sp.conta , ALiBS.it.sp_sp.fundos
				// fazer um for()
				//
				if(a.indexOf("/")>=0 || a.indexOf("\\")>=0) ALiBS.ag.ex(a,"s"); // do not add path
				else ALiBS.ag.ex(ALiBS.gv.sy.lp+a,"s"); // add path
			}
			else // try and load language js file "s"
			{
				ALiBS.ag.ex(s,"s"); // do not add path
			}
		}
		else if(s == ALiBS.gv.it.cl)
		{
			ALiBS.sy.la(5+0,"language already loaded = "+s,{o:"ALiBS.ag.ll",le:ALiBS.gv.ag.le}); 
		}
		else
		{
			ALiBS.sy.la(5+0,"invalid language not loaded s="+s,{o:"ALiBS.ag.ll",le:ALiBS.gv.ag.le});
		}
		//
		return;
	},
	// *****************************************************************
	ol: function(p) // add onload events (body onload)
	// p = (optional)(JASON) {f:function(){};} 
	// 	f = (function) func 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.ol",le:ALiBS.gv.ag.le});
		//
		if(ALiBS.gv.ag.ol.length==0) // not initialized
		{
			if(typeof window.onload == 'func'+'tion') // cache old onload
			{
				var o = window.onload; // old onload
				ALiBS.gv.ag.ol[ALiBS.gv.ag.ol.length] = o;
			} 
		}
		//
		if(p && p.f) ALiBS.gv.ag.ol[ALiBS.gv.ag.ol.length] = p.f; // parameter passed function
		//
		window.onload = function() // new onload
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.ol.window.onload",le:ALiBS.gv.ag.le});
			//
			for(var i=0;i<ALiBS.gv.ag.ol.length;i++)
			{
				ALiBS.gv.ag.ol[i]();
			}
			//
			return;	
		};
		//
		return;
	},
	// *****************************************************************
	tc: function(t,m) // timeout control (OLD callback ALiBS.cb)
	// TODO: refazer e mudar chamada em todas as classes que utilizam ALiBS.cb()
	// TODO: criar atalho ALiBS.cb = ALiBS.ag.tc.cb; // Compatibilidade
	// TODO: fazer o mesmo com todas as classes que criam objetos

	// TODO: var t = new ALiBS.ag.tc() (t pode ser local????) Testar em stress test

	// t = (number)time (ms)
	// m = (number)max times
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.tc",le:ALiBS.gv.ag.le});
		//
		var a = {};
		// 
		a.t = 200; // default timeout
		a.m = 10; // default max times to test / loop
		//
		if(typeof(t)=="number" && t>0) a.t = t;
		if(typeof(m)=="number" && m>0) a.m = m;
		//
		a.n = 0; // current times loop has run
		a.c = null; // clear timeout
		//
		a.nt = 0; // times try passed
		a.nc = 0; // times catch passed
		//
		a.e = ""; // exception message
		a.ce = ""; // custom exception message
		a.cn = ""; // custom normal message
		a.ca = ""; // custom after message
		//
		// *********************************************************
		a.cb = function() // call back function = runs in every pooling
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.a.cb",le:ALiBS.gv.ag.le});
			//
			// custom, should be replaced
			//
			return;
		};
		// *********************************************************
		a.af = function() // run (1X) after max repeat exceeded OR after success
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.a.af",le:ALiBS.gv.ag.le});
			//
			// custom, should be replaced
			//
			return;
		};
		// *********************************************************
		a.af2 = function() // run (1X) after af - after max repeat exceeded OR after success
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.a.af2",le:ALiBS.gv.ag.le});
			//
			// custom, should be replaced
			//
			return;
		};
		// *********************************************************
		a.ex = function()
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
			//
			if(a.n<a.m)
			{
				ALiBS.sy.la(5+0,"a.m(if)="+a.m,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
				ALiBS.sy.la(5+0,"a.n(if)="+a.n,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
				//
				try // OK = ends execution
				{
					a.nt++;
					//
					a.cb(); // custom
					a.af(); // custom
					//
					ALiBS.sy.la(5+0,"a.n(try)="+a.n,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					ALiBS.sy.la(5+0,"cn="+a.cn+ALiBS.gv.sy.se,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					//
					if(a.c)
					{
						ALiBS.sy.la(5+0,"(try OK) cleared old a.c="+a.c,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
						//
						clearTimeout(a.c);
					}
					//
					a.af2(); // custom
				} 
				catch(e) // Error (FORCED or NOT) = continues execution
				{
					a.nc++;
					//
					ALiBS.sy.la(5+0,""+ALiBS.gv.sy.se,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le}); 
					//ALiBS.sy.la(5+0,"ALiBS.ag.tc()[0] exception(e):"+e,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le}); // NOT an ERROR
					//
					a.n++;
					a.e = e;
					//
					ALiBS.sy.la(5+0,"a.m(catch)="+a.m,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					ALiBS.sy.la(5+0,"a.n(catch)="+a.n,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					ALiBS.sy.la(5+0,"a.e="+a.e,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					ALiBS.sy.la(5+0,"a.ce="+a.ce,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					//
					try
					{
						ALiBS.sy.la(5+0,ALiBS.gv.sy.ev+" selected",{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
						//
						if(a.c==null) // not set first time
						{
							ALiBS.sy.la(5+0,"NOT cleared old a.c=null",{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
						}
						else
						{
							ALiBS.sy.la(5+0,"(catch ERR) cleared old a.c="+a.c,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
							//
							clearTimeout(a.c); // clear before set again
						}
						//
						ALiBS.sy.la(5+0,"setTimeout (catch(e))",{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le}); // NOT an ERROR
						//
						a.c = setTimeout(a.ex,a.t);
						//
						ALiBS.sy.la(5+0,"(ex) set new a.c="+a.c,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
						//
/*						if(ALiBS.gv.sy.ev=="Server") // java, command line, tomcat, driver
						{
							a.c = setTimeout(a.ex,a.t);
							//
							ALiBS.sy.la(5+0,"(Server)(ex) set new a.c="+a.c,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
						}
						else // Browser = Client
						{
							a.c = setTimeout(a.ex,a.t);
							//
							ALiBS.sy.la(5+0,"(Browser)(ex) set new a.c="+a.c,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
						}
*/
					}
					catch(e)
					{
						ALiBS.sy.la(1+0,"ALiBS.ag.tc()[1] exception(e):"+e,{o:"ALiBS.ag"}); // REAL ERROR
					}
					//
					//ALiBS.sy.la(5+0,""+ALiBS.gv.sy.se,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le,so:"print"});
					ALiBS.sy.la(5+0,""+ALiBS.gv.sy.se,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
				}
			}
			else // n >= m - do not execute any more
			{
				a.af(); // custom
				//
				ALiBS.sy.la(5+0,"a.m(af)="+a.m,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
				ALiBS.sy.la(5+0,"a.n(af)="+a.n,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
				ALiBS.sy.la(5+0,"ca="+a.ca+ALiBS.gv.sy.se,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
				//
				if(a.c)
				{
					ALiBS.sy.la(5+0,"(after af) cleared old a.c="+a.c,{o:"ALiBS.ag.a.ex",le:ALiBS.gv.ag.le});
					//
					clearTimeout(a.c);
				}
				//
				a.af2(); // custom
			}
			//
			return;
		};
		// *********************************************************
		return a;
	}
	// *****************************************************************
};
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_ag_uz0")>=0) // TODO(2014-06-03):keep for while (deprecated) - used by old jsws.jsp
		{
			ALiBS_ag_uz0 = function(r0,v0,p) // called by novell driver // claypool shell  (do NOT use var)
			// r0 = (required)(HttpServletRequest)page request
			// v0 = (string) values to return (cmd,cb)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_ag_uz0",le:ALiBS.gv.ag.le});
				//
				var z = p || {}; // [BEST-PRACTICE]
				//
				z.ec = -1; // ec = start
				//
				var r; // [BEST-PRACTICE]
				//
				try
				{
					z.ec = -2; // 
					//
					// DEFAULTS [BEST-PRACTICE]
					//
					//
					z.par = ALiBS.ag.uz("" + r0["queryString"]);
					//
					for(var i in z.par)
					{
						ALiBS.sy.la(7+0,i+"="+z.par[i],{o:"ALiBS_ag_uz0",le:ALiBS.gv.ag.le});
					}
					//
					z.va = v0.split(",");
					//
					z.ar = new Array();
					//
					for(var i=0;i<z.va.length;i++)
					{
						if(z.va[i])
						{
							z.ar[z.ar.length] = z.par[z.va[i]];
							//
							ALiBS.sy.la(7+0,"z.par["+z.va[i]+"]="+z.par[z.va[i]],{o:"ALiBS_ag_uz0",le:ALiBS.gv.ag.le});
						}
					}
					//
					r = z.ar.join(";");
				}
				catch(e)
				{
					switch(z.ec)
					{
						case -1:z.m0="Generic Error";z.m1="";break;
						//case -2:z.m0="Parameter x must be a number";z.m1="";break;
						default:z.m0="Generic Default Error";z.m1="";break;
					}
					//
					r = ALiBS.sy.em(z.ec,"ALiBS_ag_uz0",{m0:z.m0,m1:z.m1,m2:e}).m2;
					//
					z = null; // [BEST-PRACTICE]
					delete z; // [BEST-PRACTICE]
					//
					throw r;
				}
				//
				z = null; // [BEST-PRACTICE]
				delete z; // [BEST-PRACTICE]
				//
				ALiBS.sy.la(6+0,"r="+r,{o:"ALiBS_ag_uz0",le:ALiBS.gv.ag.le});
				//
				return r; 
			};
		}
	}
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ag",{o:"ALiBS.ag"});
ALiBS.sy.la(0+0,"ALiBS.gv.ag.le="+ALiBS.gv.ag.le+ALiBS.gv.sy.se,{o:"ALiBS.ag"});
//
//
//
//
/* *************************************************************************
// INFO:

- 4 ways of loading modules:

	- using ALiBS.ag.ex("teste1-ap.js","s");

		- generally in html, inside <script>
		- do NOT validate if module is already loaded


	- using P.ap._mod[P.ap._mod.length] - ONLY for ALiBS Modules

		- in P.ap.re = function() // onPageRender
 
			P.ap._mod[P.ap._mod.length] = "JQueryMobile.ui"; // only if REQUIRED

			- the line below should exist in module

				ALiBS["JQueryMobile.ui"] = ALiBS.ui; // reference - required for loading module correctly
		- uses ALiBS.ag.lm()
		- validates if module is already loaded
			if(!ALiBS[s1] || ALiBS.gv.sy.mc[s].f01==true) 


	- using  ALiBS.ag.lm("f1"); - for other modules / js

		- generally in P.ap.ol = function() // onPageLoad

			ALiBS.gv.sy.mc["f0"] = {f00:"./apf0.js"};
			//
			if(!P.ap.f1){ ALiBS.ag.lm("f1"); }

		- validates if module is already loaded
			if(!ALiBS[s1] || ALiBS.gv.sy.mc[s].f01==true) 


	- using P.ap.lm() - for ALiBS Modules and other modules / js

		- generally in P.ap.re = function() // onPageRender

			ALiBS.gv.sy.mc["Bradesco-token-app"] = {f00:"../AP-Token/Bradesco-token-app.js"};  
			//
			P.ap.lm("ALiBS.b0,ALiBS.c2,ALiBS.c3,ALiBS.c6,ALiBS.c9,ALiBS.tk,Bradesco-token-app");

		- validates if module is already loaded
			if(!ALiBS[s1] || ALiBS.gv.sy.mc[s].f01==true) 

// *************************************************************************

Usage:

var P = new ALiBS.ap();
P.gv = {}; // global values 
//
P.ap.re = function() // custom onPageRender
{
	ALiBS.gv.sy.mp = "../ALiBS/ALiBS-lib-c/";
	//ALiBS.gv.sy.sp = "/nps/portal/modules/custom/skins/default/devices/default/ALiBS/ALiBS_s00/";
	//
	ALiBS.gv.sy.mc["ALiBS-client-pos"] = {f00:"../AP-Token/ALiBS-client-pos.js"};  
	ALiBS.gv.sy.mc["Bradesco-token-app"] = {f00:"../AP-Token/Bradesco-token-app.js"};  
	//
	P.ap.lm("ALiBS.b0,ALiBS.c2,ALiBS.c3,ALiBS.c6,ALiBS.c9,ALiBS.tk,ALiBS-client-pos,Bradesco-token-app");
	//
	//
	//
	ATTENTION: needed for checking for loaded modules
	(in ALiBS-client-pos.js)
		ALiBS["ALiBS-client-pos"] = 1; // reference when loading with ap.js

	(in Bradesco-token-app.js)
		ALiBS["Bradesco-token-app"] = 1; // reference when loading with ap.js

};
//
P.ap.ol = function() // custom onPageLoad
{

};
//
P.ap.cl = function() // custom changePageLanguage
{

};
//
P.ap.al = function() // custom after P.ol and after P.cl
{

};
//
ALiBS.sy.la(0,"loaded module ap.js"+ALiBS.gv.sy.se,{o:"ap.js"});
//
P.ap.ex(); // execute page

// *************************************************************************
// TODO:

- ap
	- carrega usando ALiBS.ag
		mp = DEFAULT = /home/dados1/myDocsPs/_13_js_/ALiBS/ALiBS-lib/

*/
//
// *************************************************************************
//
ALiBS.gv.ap = {}; // global values
//
ALiBS.gv.ap.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ap.le = 0; // local log status
//
ALiBS.gv.ap.apple = 0; // local log status for application
//
if(typeof(ALiBS_gv_ap_le)!="undefined") ALiBS.gv.ap.le = ALiBS_gv_ap_le;
//
// *************************************************************************
//
ALiBS.ap = function() // page - application functions inherited
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ap.__",le:ALiBS.gv.ap.le});
	//
	var a = {};
	a.ap = {};
	// *****************************************************************
	a.ap._mod = new Array(); // Load Required Modules
	// *****************************************************************
	a.ap.lm = function(s) // load modules
	// s = (required)(String)modules ("ALiBS.lg,ALiBS.dt,ALiBS.cp")
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap.lm",le:ALiBS.gv.ap.le});
		//
		var s0 = s.split(",");
		//
		for(var i=0;i<s0.length;i++)
		{
			a.ap._mod[a.ap._mod.length] = s0[i];
		}
		//
		return;
	};
	// *****************************************************************
	a.ap.ex = function() // execute ALL
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap.ex",le:ALiBS.gv.ap.le});
		//
		ALiBS.ag.ol({f:a.ap._ol}); // register application page onPageLoad
		//
		// Load Required Modules = ONLY Always Required
		//
		a.ap._mod[a.ap._mod.length] = "ALiBS.c2"; // Always Required - (2012-03-16:Moved here to avoid loading error on iManager)
		//
		a.ap._mod[a.ap._mod.length] = "ALiBS.el"; // Always Required in HTML
		a.ap._mod[a.ap._mod.length] = "ALiBS.st"; // Always Required in HTML
		//
		a.ap._mod[a.ap._mod.length] = "ALiBS.sg"; // Always Required
		//
		a.ap._mod[a.ap._mod.length] = "ALiBS.sy"; // Always Required - Loaded AGAIN to add release (release is not added when loaded in HTML page HARD-CODED)
		//
		ALiBS.sy.la(5+0,"(before)a.ap._mod.join()="+a.ap._mod.join(","),{d:"y",o:"ALiBS.a.ap.ex",le:ALiBS.gv.ap.le});
		//
		if(a.ap.re) a.ap.re(); // custom onPageRender
		//
		ALiBS.sy.la(5+0,"(after)a.ap._mod.join()="+a.ap._mod.join(","),{d:"y",o:"ALiBS.a.ap.ex",le:ALiBS.gv.ap.le});
		//
/*		for(var i=0;i<a.ap._mod.length;i++) // load modules
		{
			ALiBS.ag.lm(a.ap._mod[i]); 
		}*/
		//
		return;
	};
	// *****************************************************************
	a.ap._ol = function() // onPageLoad
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
		//
		for(var i=0;i<a.ap._mod.length;i++) // load modules
		{
			ALiBS.ag.lm(a.ap._mod[i]); 
		}
		//
		ALiBS.sy.la(5+0,"a.ap._mod.join()="+a.ap._mod.join(","),{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
		//
		a.ap._ty = {};
		a.ap._ty.n = 0;
		a.ap._ty.cb = function()
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
			//
			ALiBS.sy.la(5+0,"a.ap._ty.n="+a.ap._ty.n,{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
			ALiBS.sy.la(5+0,"a.ap._mod.length="+a.ap._mod.length,{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
			//
			if(a.ap._ty.n<a.ap._mod.length) // check loaded Required Modules
			{
				ALiBS.sy.la(5+0,"a.ap._mod[a.ap._ty.n]="+a.ap._mod[a.ap._ty.n],{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
				//
				var s1 = a.ap._mod[a.ap._ty.n].replace("ALiBS.","");
				//
				ALiBS.sy.la(5+0,"s1="+s1,{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
				//
				if(ALiBS[s1]) 
				{
					ALiBS.sy.la(5+0,"a.ap._ty.n="+a.ap._ty.n+"="+a.ap._mod[a.ap._ty.n],{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
					a.ap._ty.n++;
					window.setTimeout(a.ap._ty.cb,4);
				}
				else window.setTimeout(a.ap._ty.cb,100);
			}
			else
			{
				if(a.ap.ol) a.ap.ol(); // custom onPageLoad
				//
				if(ALiBS.it)
				{
					var dl = ALiBS.it.dl();
					ALiBS.sy.la(5+0,"dl="+dl,{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
					//
					a.ap._cl({l:dl}); // changePageLanguage - set initial page language (Default/Current)
				}
				//
				if(ALiBS.ev) ALiBS.sy.la(5+0,"ua="+ALiBS.ev.ua(),{d:"y",o:"ALiBS.a.ap._ol",le:ALiBS.gv.ap.le});
				//
				if(a.ap.al) a.ap.al(); // custom after P.ol and after P.cl
				//
				a.ap._al(); // load and pre-load imgs and modules
			}
			//
			return;
		};
		a.ap._ty.cb();
		//
		return;
	};
	// *****************************************************************
	a.ap._cl = function(p) // changePageLanguage
	// p = (optional)(JSON)parameter {l:'ALiBS.it.en_us'}
	//	l = (String)language 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
		//
		var l = (p && p.l) || null;
		//
		ALiBS.sy.la(5+0,"(A)ALiBS.gv.it.pd="+ALiBS.gv.it.pd,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
		ALiBS.sy.la(5+0,"(A)ALiBS.gv.it.pd_ap="+ALiBS.gv.it.pd_ap,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
		//
		ALiBS.gv.it.pd=2; // ALiBS i18n
		ALiBS.gv.it.pd_ap=2; // application i18n
		//
		ALiBS.sy.la(5+0,"(B)ALiBS.gv.it.pd="+ALiBS.gv.it.pd,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
		ALiBS.sy.la(5+0,"(B)ALiBS.gv.it.pd_ap="+ALiBS.gv.it.pd_ap,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
		//
		ALiBS.ag.ll(l+"_ALiBS"); // language of ap module
		//
		var t = new ALiBS.ag.tc(100,100);
		//
		t.cb = function()
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap.t.cb",le:ALiBS.gv.ap.le});
			//
			ALiBS.sy.la(5+0,"(C)ALiBS.gv.it.pd="+ALiBS.gv.it.pd,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
			ALiBS.sy.la(5+0,"(C)ALiBS.gv.it.pd_ap="+ALiBS.gv.it.pd_ap,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
			//
			if(ALiBS.gv.it.pd==0) // loaded
			{
				ALiBS.gv.it.pd = 1; // language changed/processed on page
				//
				ALiBS.sy.la(5+0,"(D)ALiBS.gv.it.pd="+ALiBS.gv.it.pd,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
				//
				ALiBS.ag.ll(l+"_ap"); // language of ap module
				//
				_FORCE_ERROR_ALiBS_ap_a_ap_cl_pd0(); // generate error;
			}
			else if(ALiBS.gv.it.pd_ap==0) // loaded
			{
				ALiBS.gv.it.pd_ap = 1; // language changed/processed on page
				//
				ALiBS.sy.la(5+0,"(D)ALiBS.gv.it.pd_ap="+ALiBS.gv.it.pd_ap,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
				//
				if(a.ap.cl) a.ap.cl(); // custom changePageLanguage
				//
				for(var i in ALiBS.gv.it.fcl)
				{
					ALiBS.sy.la(7+0,"(it.fcl[i])"+i+"="+i,{o:"ALiBS.a.ap._cl",le:ALiBS.gv.ap.le});
					//
					ALiBS.gv.it.fcl[i]();
				}
			}
			else if(ALiBS.gv.it.pd!=1 || ALiBS.gv.it.pd_ap!=1) // not loaded yet
			{
				t.ce = "[WAITING] for loading of language";
				_FORCE_ERROR_ALiBS_ap_a_ap_cl_pd2(); // generate error;
			}
			//
			return;
		};
		t.ex();
		//
		return;
	};
	// *****************************************************************
	a.ap._itr = function(e) // i18n refresh combo of languages
	// e = (Required)(String)id of select/combo
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._itr",le:ALiBS.gv.ap.le});
		//
		var r = new Array();
		//
		r[r.length] = a.ap._ao(ALiBS.gv.sy.ln[ALiBS.gv.it.cl],ALiBS.gv.it.cl,{s:true});
		//
		for(var j in ALiBS.gv.sy.ln)
		{
			if(j!=ALiBS.gv.it.cl) r[r.length] = a.ap._ao(ALiBS.gv.sy.ln[j],j);
		}
		//
		ALiBS.sy.la(5+0,'r.join("")='+r.join(""),{o:"ALiBS.a.ap._ao",le:ALiBS.gv.ap.le});
		//
		ALiBS.el.si(e,r.join(""));
		//
		return;
	};
	// *****************************************************************
	a.ap._ao = function(t,v,p) // add select/combo options 
	// t = (required)(String)option text
	// v = (required)(String)option value
	// p = (optional)(JSON)parameters {s:true} 
	//	s = (optional)(boolean)selected
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._ao",le:ALiBS.gv.ap.le});
		//
		ALiBS.sy.la(5+0,"t="+t,{o:"ALiBS.a.ap._ao",le:ALiBS.gv.ap.le});
		ALiBS.sy.la(5+0,"v="+v,{o:"ALiBS.a.ap._ao",le:ALiBS.gv.ap.le});
		//
		var a = "";
		if(p && p.s) a = " selected";
		//
		//return "<option"+a+" title='dm01.gif'>"+s+"</option>";
		//return "<option"+a+"><spam><img src='./i/dm01.gif'>"+s+"</img></spam></option>";
		//return "<spam><img src='./i/dm01.gif'><option"+a+">"+s+"</option></img></spam>";
		//return "<img src='./i/dm01.gif'></img><option"+a+">"+s+"</option>";
		//return "<div style='backgroundImage:url(./i/dm01.gif)'></div><option"+a+">"+s+"</option>";

		//return "<span style='backgroundImage: url(./i/dm01.gif);'/><option"+a+" value='"+v+"'>"+t+"</option>";

		//var img = ALiBS.gv.sy.sp + "dm01.gif";

		//return "<option"+a+" value='"+v+"'><span style='backgroundImage: url("+img+");' />"+t+"</option>";

		//return "<option"+a+" value='"+v+"' style='backgroundImage: url("+img+");' />"+t+"</option>";

		return "<option"+a+" value='"+v+"'>"+t+"</option>";

		// http://www.ajaxload.info/
		// http://www.marghoobsuleman.com/jquery-image-dropdown
	};
	// *****************************************************************
	a.ap._itg = function(e) // i18n get options - called by <select>(combo)language
	// e = (Required)(String)id of select/combo
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._itg",le:ALiBS.gv.ap.le});
		//
		var x = ALiBS.el.ge(e).selectedIndex;
		var t = ALiBS.el.ge(e).options[x].text;
		var v = ALiBS.el.ge(e).options[x].value;
		//
		P.ap._cl({l:v});
		//
		return;
	};
	// *****************************************************************
	a.ap._rm = function() // remove tags in ALiBS.ag.rm
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._rm",le:ALiBS.gv.ap.le});
		//
		var x = new ALiBS.ag.tc(10*1000,100*1000);
		//
		x.cb = function()
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._rm.x.cb",le:ALiBS.gv.ap.le});
			//
			if(ALiBS.gv.ag.nd<ALiBS.gv.ag.ni)
			{
				//ALiBS.ag.rm1(); // remove next 1
				ALiBS.ag.rm0(); // remove next 1
			}
			else _FORCE_ERROR_ALiBS_ap_a_ap_rm(); // forçar erro - 100*1000
			//
			return;
		};
		x.ex();
		//
		return;
	};
	// *****************************************************************
	a.ap._img = new Array(); // load images with ALiBS.el.ss() 
	a.ap._pre_mod = new Array(); // pre load modules
	a.ap._pre_img = new Array(); // pre load images 
	// *****************************************************************
	a.ap._al = function() // load and pre-load imgs and modules
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
		//
		// pre load modules = load ALL modules not loaded in Load Required Modules
		//
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.c6"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.c9"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.b0"; // NOT Required immediately
		//
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.dt"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.ev"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.lg"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.rx"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.it"; // NOT Required immediately
		a.ap._pre_mod[a.ap._pre_mod.length] = "ALiBS.sb"; // NOT Required immediately
		//
		// ************************
		//
		for(var i=0;i<P.ap._pre_mod.length;i++) // 
		{
			ALiBS.sy.la(5+0,"(after)a.ap._pre_mod["+i+"]= "+a.ap._pre_mod[i],{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
		}
		//
		// ************************
		//
		for(var i=0;i<P.ap._img.length;i++) // 
		{
			ALiBS.sy.la(5+0,"(after)a.ap._img["+i+"]= "+a.ap._img[i],{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
		}
		//
		// ************************
		//
		for(var i=0;i<P.ap._pre_img.length;i++) // 
		{
			ALiBS.sy.la(5+0,"(after)a.ap._pre_img["+i+"]= "+a.ap._pre_img[i],{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
		}
		//
		var x = new ALiBS.ag.tc(100,100*(a.ap._img.length*a.ap._pre_mod.length*a.ap._pre_img.length)); // 
		x.nn = 0; // index for Array
		x.tt = "load_img"; // type
		x.cb = function()
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.a.ap._al.x.cb",le:ALiBS.gv.ap.le});
			//
			ALiBS.sy.la(5+0,"x.nn="+x.nn,{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
			//
			if(x.tt=="load_img")
			{
				ALiBS.sy.la(5+0,"trying load_img",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
				//
				if(x.nn<a.ap._img.length)
				{
					ALiBS.sy.la(5+0,"loading img: "+a.ap._img[x.nn][1],{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
					//
					ALiBS.el.ss(a.ap._img[x.nn][0],a.ap._img[x.nn][1]); // set source for img
					x.nn++;
				}
				else
				{
					ALiBS.sy.la(5+0,"moving to pre_load_mod",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
					//
					x.nn = 0;
					x.tt = "pre_load_mod";
				}
				//
				_FORCE_ERROR_ALiBS_ap_a_ap_al_load_mod(); // forçar erro 
			}
			else if(x.tt=="pre_load_mod")
			{
				ALiBS.sy.la(5+0,"trying pre_load_mod",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
				ALiBS.sy.la(5+0,"a.ap._pre_mod.length="+a.ap._pre_mod.length,{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
				//
				if(x.nn<a.ap._pre_mod.length)
				{
					ALiBS.sy.la(5+0,"pre-loading module: "+a.ap._pre_mod[x.nn],{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
					//
					ALiBS.ag.lm(a.ap._pre_mod[x.nn]); 
					x.nn++;
				}
				else
				{
					ALiBS.sy.la(5+0,"moving to pre_load_img",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
					//
					x.nn = 0;
					x.tt = "pre_load_img";
				}
				//
				_FORCE_ERROR_ALiBS_ap_a_ap_al_pre_load_mod(); // forçar erro 
			}
			else if(x.tt=="pre_load_img")
			{
				ALiBS.sy.la(5+0,"trying pre_load_img",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
				//
				if(x.nn<a.ap._pre_img.length)
				{
					ALiBS.sy.la(5+0,"pre-loading img: "+a.ap._pre_mod[x.nn],{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
					//
					ALiBS.ag.ex(a.ap._pre_img[x.nn],"i"); 
					x.nn++;
				}
				else
				{
					ALiBS.sy.la(5+0,"moving to remove_tags_script",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
					//
					x.nn = 0;
					x.tt = "remove_tags_script";
				}
				//
				_FORCE_ERROR_ALiBS_ap_a_ap_al_pre_load_img(); // forçar erro 
			}
			else if(x.tt=="remove_tags_script")
			{
				ALiBS.sy.la(5+0,"trying remove_tags_script",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
				//
				a.ap._rm(); // remove tags in ALiBS.ag.rm
				//
				ALiBS.sy.la(5+0,"moving out",{o:"ALiBS.a.ap._al",le:ALiBS.gv.ap.le});
			}
			//
			return;
		};
		x.ex();
		//
		return;
	};
	// *****************************************************************
	return a;
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ap",{o:"ALiBS.ap"});
ALiBS.sy.la(0+0,"ALiBS.gv.ap.le="+ALiBS.gv.ap.le+ALiBS.gv.sy.se,{o:"ALiBS.ap"});
//
//
//
//
/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
ALiBS.gv.ob = {}; // global values
//
ALiBS.gv.ob.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ob.le = 0; // local log status
//
if(typeof(ALiBS_gv_ob_le)!="undefined") ALiBS.gv.ob.le = ALiBS_gv_ob_le;
//
//
ALiBS.ob = 
{
	gc: function() // get constructor
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ob.gc",le:ALiBS.gv.ob.le});
		//
		return function() 
		{
			ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ob.gc.func"+"tion",le:ALiBS.gv.ob.le});
			//
			this.vr.apply(this, arguments); // variables.apply
			//
			return;
		};
	},
	// *****************************************************************
	ex: function(n,o) // extend
	// n = (required)(Object)new object
	// o = (required)(Object)old object
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ob.ex",le:ALiBS.gv.ob.le});
		//
		for(var i in o) 
		{
			n[i] = o[i]; // i = properties, functions and variables
		}
		//
		return n;
	}
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ob",{o:"ALiBS.ob"});
ALiBS.sy.la(0+0,"ALiBS.gv.ob.le="+ALiBS.gv.ob.le+ALiBS.gv.sy.se,{o:"ALiBS.ob"});
//
//
//
//
/* *************************************************************************
// INFO:

ATUALIZA = 
Define se o browser deve carregar a pagina de ação em todas as solicitações, 
ou pode simplesmente utilizar o seu cache se a pagina for solicitada uma segunda vez. 
true (default) = obriga que a página seja carregada sempre, 
false = permite a utilização do cache do browser.    

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
ALiBS.gv.ax = {}; // global values
//
ALiBS.gv.ax.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.ax.le = 0; // local log status
//
if(typeof(ALiBS_gv_ax_le)!="undefined") ALiBS.gv.ax.le = ALiBS_gv_ax_le;
//
//
ALiBS.ax = ALiBS.ob.gc();
ALiBS.ax.prototype = // TODO(2010-09-22): mudar para ALiBS.ag.ax() ????
{
	vr: function() 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ax.vr",le:ALiBS.gv.ax.le});
		//
		this.up = false; // updating
		//this.ax = null;
		//
		return;
	},
	// *****************************************************************
	cb: function() // callback function
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ax.cb",le:ALiBS.gv.ax.le});
		//
		// não faz nada, será sobrescrita
		//
		return;
	},
	// *****************************************************************
/*	rs: function() // ready state
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ax.rs",le:ALiBS.gv.ax.le});
		//
		var th = this;      
		//		
		if(th.ax) ALiBS.sy.la(7+0,"ax.readyState="+th.ax.readyState,{o:"ALiBS.ax.ab",le:ALiBS.gv.ax.le});
		if(th.ax && th.ax.readyState==4) 
		{             
			th.up = false;                
			th.cb(th.ax.responseText,th.ax.responseXML,th.ax.status);        
			th.ax = null;                                         
		}                   
		//
		return;                                  
	},*/
	// *****************************************************************
	ab: function() // abort
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ax.ab",le:ALiBS.gv.ax.le});
		//
		var th = this;      
		//		
		if(th.up) 
		{
			th.up = false;
			th.ax.ab();
			th.ax = null;
		}
		//
		return;
	},
	// *****************************************************************
	ex: function(u,p) // execute = run
	// u = (required) (sting) url
	// p = (optional) (JSON) parameters {t:false,m:"",d:""} 
	// 	t = (boolean) timestamp - include to url (defaul=true)
	//	m = (string) method (P/G)(get,post) (default=P)
	//	d = (string) data
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
		//
		var th = this;      
		//
		var r = false; // [BEST-PRACTICE]
		//	
		if(th.up)
		{ 
			r = false; 
		}
		else
		{
			th.ax = null;            
			//
			try 
			{ 
				th.ax = new XMLHttpRequest(); 
			} 
			catch(e)
			{
				ALiBS.sy.la(7+0,"ALiBS.ax.ex()[0] exception(e):"+e,{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
				ALiBS.sy.la(7+0,"ALiBS.ax.ex()[0] exception(e):Trying ActiveXObject",{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
				//
				var ms = ['Msxml2.XMLHTTP','Microsoft.XMLHTTP','Msxml2.XMLHTTP.6.0','Msxml2.XMLHTTP.4.0','Msxml2.XMLHTTP.3.0'];
				for(var i=0;i<ms.length;i++)
				{ 
					try
					{ 
						ALiBS.sy.la(7+0,"MSXMLHTTP["+i+"] = "+i,{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
						ALiBS.sy.la(7+0,"MSXMLHTTP[ms["+i+"]] = "+ms[i],{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
						//
						th.ax = new ActiveXObject(ms[i]);
						break;
					}
					catch(e)
					{
						ALiBS.sy.la(7+0,"ALiBS.ax.ex()[1] exception(e):"+e,{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
					}
				}
			}                                             
			//
			if(!th.ax) 
			{             
				ALiBS.sy.la(7+0,"XMLHttpRequest/ActiveXObject unsupported",{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
				//
				r = false; //O navegador não tem suporte
			} 
			else 
			{
				//th.ax.onreadystatechange = this.rs;
				//
				th.ax.onreadystatechange = function()
				{
					ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ax.ex.th.ax.onreadystatechange",le:ALiBS.gv.ax.le});
					//
					ALiBS.sy.la(7+0,"readyState="+th.ax.readyState,{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
					//
					if(th.ax.readyState==4) 
					{             
						th.up = false;                
						th.cb(th.ax.responseText,th.ax.responseXML,th.ax.status);        
						th.ax = null;                                         
					}
					//
					return;                                                
				};
				//
				th.up = new Date();
				//
				var d = "";
				var t = true; // default = use timestamp
				var m = "P"; // default = POST
				//
				if(p)
				{
					if(typeof(p.d)=="string")
					{
						//d = "&da="+p.d; // data passed
						d = p.d; // data passed
					}
					//
					if(typeof(p.t)=="boolean" && p.t==false) t = ""; // false - do not use timestamp - files will probably be loaded from browser cache
					//
					if(typeof(p.m)=="string" && p.m=="G") m = "G"; // GET
				}
				//
				if(t==true) t = "&ts="+(th.up.getTime())+"&rd="+Math.random(); 
				//
				var r; // = u + '?' + d + t;
				//
				// TODO(2011-04-12): como sao tratados os comandos PUT, DELETE, TRACK, HEAD, OPTIONS, TRACE, TRACK, DEBUG ???? Como testar ????
				//
				/* TODO(2011-04-12)
				HTTP is a stateless protocol. 
				A stateless protocol does not require the server to retain information or status about each user for the duration of multiple requests. 
				For example, when a web server is required to customize the content of a web page for a user, 
				the web application may have to track the user's progress from page to page. 

				A common solution is the use of HTTP cookies. 
				Other methods include server side sessions, 
				hidden variables (when the current page is a form), 
				and URL-rewriting using URI-encoded parameters, e.g., /index.php?session_id=some_unique_session_code.
				*/
				// Both client and server store js variables with values
				// client TO server = every request sends required(id,challenge,etc) information = send using POST = ALiBS.ax
				// server TO client = every response sends required(id,challenge,etc) information = reponse to POST = js variables OR text to be parsed/calculated 
				//
				if(m=="G") // GET
				{
					ALiBS.sy.la(7+0,"(GET)r="+r,{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
					//
					r = u + '?' + d + t;
					//
					th.ax.open("GET",r,true);
					// TODO(2011-04-13):set content length e outros
					//
					th.ax.setRequestHeader("Access-Control-Allow-Origin","*"); //TODO(2011-06-20): Fiz esta inclusao para testar baixar site do google - downloader.html
					th.ax.send(null);
				} 
				else // P = POST 
				{
					ALiBS.sy.la(7+0,"(POST)r="+r,{o:"ALiBS.ax.ex",le:ALiBS.gv.ax.le});
					//
					th.ax.open("POST",u,true);
					th.ax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					//
					th.ax.setRequestHeader("Access-Control-Allow-Origin","*"); //TODO(2011-06-20): Fiz esta inclusao para testar baixar site do google - downloader.html
					//
					r = d + t;
	//alert("r="+r);
					//
					th.ax.setRequestHeader("Content-Length",r.length);
					th.ax.send(r);
				}
				//           
				r = true;                                             
			}                                                                        
		}
		//
		return r;
	}
	// *****************************************************************
};
// *************************************************************************
//
Function.prototype.bm = function(d,n,a) // benchmark added to [ALL] functions
// d = (required)(String)test description
// n = (required)(number)times to run test
// a = (required)(Array)function original arguments [a,b,c]
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sy.bm",le:ALiBS.gv.sy.le});
	//
	var r = {};
	//
	r.d = d;
	r.s = new Date(); // ms = mili-seconds
	//
	if(a)
	{
		for(var i=0;i<n;i++) 
		{
			r.r = this.apply(this,a);
		}
	}
	else
	{
		for(var i=0;i<n;i++)
		{
			r.r = this.apply(this);
		}
	}
 	//
	r.s = new Date() - r.s;
 	//
	return r;
};
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ax",{o:"ALiBS.ax"});
ALiBS.sy.la(0+0,"ALiBS.gv.ax.le="+ALiBS.gv.ax.le+ALiBS.gv.sy.se,{o:"ALiBS.ax"});
//
// *************************************************************************
//
//
/*
if(typeof(ALiBS_gv_ap_P)!="undefined" && ALiBS_gv_ap_P!="") // Load P.js (instance of ALiBS.ap)
{
	ALiBS.ag.ex(ALiBS_gv_ap_P,"s"); // do not add path
}
*/
//
//
} // end of if(!ALiBS.sy) 
else
{
//
ALiBS.sy.la(0+0,"Module ALiBS.sy Already Loaded",{o:"ALiBS.sy"});
//
}
//
// *************************************************************************

