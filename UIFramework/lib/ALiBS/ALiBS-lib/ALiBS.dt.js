/* *************************************************************************
// INFO:

new Date(milissegundos)
new Date(string_de_data)
new Date(ano, mes, dia)
new Date(ano, mes, dia, hora, minuto, segundo, milissegundo)

Todos os parametros passados no construtor devem ser numericos. 
Com relacao ao parametro mes, o numero 0 representa Janeiro e o 11 Dezembro. 
Se nao for passada a hora, o objeto iniciara com a hora 00:00:00.

Com excecao da segunda opcao que pode ser instanciada passando uma string da seguinte maneira:
var data = new Date("Jan 1, 2000 00:00:00");

A data e hora no Javascript comecam em 00:00:00 h GMT de 01 de janeiro de 1970. 
Se voce tentar criar uma instancia com uma data anterior ira obter um resultado aleatório.

Metodos
*******

O objeto Date nao possui propriedades. 

- getDate() Devolve o dia do mes, um inteiro entre 1 e 31. Nao confundir com getDay que retorna o dia da semana.

- getDay() Devolve o dia da semana, inteiro entre 0 e 6 (0 para Domingo).

- getHours() Retorna a hora, inteiro entre 0 e 23.

- getMinutes() Devolve os minutos, inteiro entre 0 e 59.

- getSeconds() Devolve os segundos, inteiro entre 0 e 59.

- getMonth() Devolve o mes, um inteiro entre 0 e 11 (0 para Janeiro).

- getTime() Devolve os segundos transcorridos entre o dia 1 de janeiro de 1970 e a data correspondente ao objeto ao que se passa a mensagem.

- getYear() Obsoleto. Retorna os dois ultimos numeros do ano. Para o 2010 retorna 10.

- getFullYear() Retorna o ano com todos os digitos. Funciona com datas posteriores ao ano 2000.

******************************

- setDate(d) Atualiza o dia do mes (integer between 1 and 31 that represents day of the current month)

- setHours(h) Atualiza a hora.

- setMinutes(m) Muda os minutos.

- setMonth(m) Muda o mes (atencao ao mes que comeca por 0).

- setSeconds(s) Muda os segundos.

- setTime(t) Atualiza a data completa. Recebe um numero de segundos desde 1 de janeiro de 1970.

- setYear(y) Obsoleto. Muda o ano, recebe um numero, ao que lhe soma 1900 antes de coloca-lo como ano data. Se receber 95 colocara o ano 1995. 

- setFullYear() Muda o ano (4 digitos) da data ao numero que recebe por parametro. 

- getimezoneOffse() Devolva a diferenca (em minutos) entre a hora local e a hora GMT (Greenwich, UK Mean Time).

- toGMTString() Devolva o valor do atual em hora GMT (Greenwich Mean Time) 

// ***************************

ALiBS.dt.ts: function(p) // get time stamp = similar -->> date.toJSON()

a = new Date()
	Wed Jul 15 2015 13:35:55 GMT-0300 (BRT)
a.toJSON()
	"2015-07-15T16:35:55.388Z" (3 horas a mais)
a.setUTCHours(a.getHours());
	1436967355388
a.toJSON()
	"2015-07-15T13:35:55.388Z" (horario correto)
a.toJSON().replace(/[:T.-]/gi,"")
	"20150715133555388Z" (timestamp do horario correto)

*******

Get date in local time

	(1)	a = new Date()
		a.setUTCHours(a.getHours());
		a.setUTCMinutes(a.getMinutes());
		a.toJSON().replace("T"," ");

			a = new Date()
				Wed Jul 15 2015 13:35:55 GMT-0300 (BRT)
			a.toJSON()
				"2015-07-15T16:35:55.388Z" (3 horas a mais)
			a.setUTCHours(a.getHours());
				1436967355388
			a.toJSON()
				"2015-07-15T13:35:55.388Z" (horario correto)
			a.toJSON().replace(/[:T.-]/gi,"")
				"20150715133555388Z" (timestamp do horario correto)

	(2)	new Date((new Date()).toString()+" UTC"); // may not work in I.E.

	(3)	(new Date()).toString().substring(0,24)+" UTC"; // may not work in I.E.

	(4)	a = new Date();
		b = a - (a.getTimezoneOffset()*60*1000); // offset in minutes (Brasil=180)(UTC-0300)(GMT-0300)
		c = new Date(b)
		c.toJSON().replace("T"," ");

	(5)	a = new Date()-3*60*60*1000;
		b = new Date(a);
		b.toJSON().replace("T"," ");

// ***************************

- date to number 
	(new Date()) -1+1
	- result in milliseconds

- number to date
	new Date(number)
	- number is milliseconds

// ***************************

Sum / Substract <string> Dates

pct = "20140814"; // pwdChangedTime
a = ALiBS.dt.s2j(pct); // string to JSON
b = ALiBS.dt.sd2(30,{y:a.y,m:a.m,dy:a.d}); // sum 30 days
c = ""+b.y+b.sm+b.sd; // string

// ***************************

http://en.wikipedia.org/wiki/Year_2000_problem#Leap_years
Ordinarily, a year is a leap year if it is divisible by 4. 
A year divisible by 100, however, is not a leap year unless it is also divisible by 400.

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.dt) ALiBS.gv.dt = {}; // global values
//
if(!ALiBS.gv.dt.le) ALiBS.gv.dt.le = 0; // local log status
//
ALiBS.gv.dt.nn = 0; // x times loaded
//
ALiBS.gv.dt.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.dt.rl = "14.0814.0001";
//
// *************************************************************************
//
ALiBS.dt = // Date and Time
{
	gs: function(p) // get seconds
	// p = (optional)(JSON)parameters {d:dt,y:2010} 
	// 	d = (optional)(date)date
	//	y = (optional)(number)year to be substract (to generate a short number)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.gs",le:ALiBS.gv.dt.le});
		//
		var t = (p && p.d) || new Date();
		var a = (p && p.y) || 0;
		//
		var j = ALiBS.dt.gj({d:t});
		//
		var y = (j.y-a)*12*30*24*3600*1000;
		var m = (j.m)*30*24*3600*1000; 
		var d = (j.d)*24*3600*1000;
		//
		var h = (j.h)*3600*1000;
		var m = (j.n)*60*1000;
		var s = (j.s)*1000;
		var i = j.i;
		//
		return j.y+j.m+j.d+j.h+j.m+j.s+j.i;
	},
	// *****************************************************************
	ts: function(p) // get time stamp = similar -->> date.toJSON()
	// p = (optional)(JSON)parameters {d:dt,s0:"-",s1:"::",s2:":",s3:"."} 
	// 	d = (date)date
	//	s0 = (char)separador ano-mes-dia
	//	s1 = (char)separador data-hora
	//	s2 = (char)separador hora-minuto-segundo
	//	s3 = (char)separador milisegundo
	//	dt = (number)date and time (return type)
	//		0 = (DEFAULT) = date and time = 2012-06-13 10:15:42.047 (including miliseconds)
	//		1 = date = 2012-06-13
	//		2 = date and time + Z = 2012-06-13 10:15:42Z (no miliseconds)
	//		3 = date and time = 2012-06-13 10:15:42 (no miliseconds)
	//		4 = date and time = 2012-06-13 10:15:42.13 (miliseconds with 2 places/positions)
	//		5 = date and time = 12.0613.1015 (no miliseconds) (to validate ALiBS release)
	//	la = (number)run ALiBS.sy.la
	//		0 = DEFAULT = do NOT show 
	//		1 = show
	//
	// return String reverse Date yyyy/mm/dd-hh-mm-ss-mmm
	{
		//DO NOT USE ALiBS.sy.la(6+0) here!!!! To avoid looping!!!!
		//
		if(p && p.la && p.la==1) ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.ts",le:ALiBS.gv.dt.le});
		//
		var t = (p && p.d) || new Date();
		//
		var s0 = (p && p.s0) || "";
		var s1 = (p && p.s1) || "";
		var s2 = (p && p.s2) || "";
		var s3 = (p && p.s3) || "";
		//
		var dt = (p && p.dt) || 0;
		//
		var j = ALiBS.dt.gj({d:t,s:true});
		//
		if(dt==1) 	return j.y+s0+j.sm+s0+j.sd; // 1
		else if(dt==2) return j.y+s0+j.sm+s0+j.sd+s1+j.sh+s2+j.sn+s2+j.ss+"Z"; // 2
		else if(dt==3) return j.y+s0+j.sm+s0+j.sd+s1+j.sh+s2+j.sn+s2+j.ss; // 3
		else if(dt==4) return j.y+s0+j.sm+s0+j.sd+s1+j.sh+s2+j.sn+s2+j.ss+s3+(j.si).substring(0,2); // 4
		else if(dt==5) return ""+(j.y%2000)+"."+j.sm+j.sd+"."+j.sh+j.sn; // 5
		else 		return j.y+s0+j.sm+s0+j.sd+s1+j.sh+s2+j.sn+s2+j.ss+s3+j.si; // 0 
	},
	// *****************************************************************
	ts2: function(p) // Funciona dentro de Drivers com javascript 1.0 TODO(2015-07-16):adicionar parametros como em ts()
	{
		var a = new Date();
		a = new Date();
		a.setUTCHours(a.getHours());

		//
		//var b = a.toJSON().replace(/[T:.-]/gi,"");
		//var b = a.toISOString().replace(/[T:.-]/gi,"");
		//
		var b = a.toGMTString();
		//
		//"Wed, 15 Jul 2015 15:12:58
		//
		var v0 = "" + (a.getMonth()+1);
		if(v0.length==1)
		{
			v0 = "0" + v0;
		}
		//
		var c = b.split(" ");
		//
		var d = "" + c[3] + v0 + c[1] + c[4] + "00Z";
		//
		d = d.replace(/[:]/gi,"");
		//
		return d;
	},
	// *****************************************************************
	gj: function(p) // get JSON date
	// p = (optional)(JSON)parameters {d:dt} 
	// 	d = (optional)(date)date
	//	s = (optional)(boolean)also return (string) date
	//		false = DEFAULT
	// return = JSON date
	{
		if(p && p.la && p.la==1) ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.gj",le:ALiBS.gv.dt.le});
		//
		var t = (p && p.d) || new Date();
		var j = {}; // JSON date 
		//
		j.y = t.getFullYear();
		j.m = t.getMonth()+1; // [1-12]
		j.d = t.getDate();
		//
		j.h = t.getHours();
		j.n = t.getMinutes();
		j.s = t.getSeconds();
		j.i = t.getMilliseconds();
		//
		j.wd = t.getDay(); // 0=sunday,1=monday
		//
		//r.wd = t.getDayOfWeek(); // 1 = domingo, 2 = segunda 
		//r.wn = t.getDayOfWeek(); // Sunday, Monday
		//
		j.l = ALiBS.dt.ly({d:t}); // leap year = true
		j.ma = ALiBS.dt.ma({d:t}); // array of max days of month
		j.md = j.ma[j.m-1]; // max days of current/Date month 
		//
		if(p && p.s && p.s==true) // also return (string) date
		{
			if(j.m<10) j.sm = "0"+j.m;
			else j.sm = ""+j.m;
			//
			if(j.d<10) j.sd = "0"+j.d;
			else j.sd = ""+j.d;
			//
			if(j.h<10) j.sh = "0"+j.h;
			else j.sh = ""+j.h;
			//
			if(j.n<10) j.sn = "0"+j.n;
			else j.sn = ""+j.n;
			//
			if(j.s<10) j.ss = "0"+j.s;
			else j.ss = ""+j.s;
			//
			if(j.i<10) j.si = "00"+j.i;
			else if(j.i<100) j.si = "0"+j.i;
			else j.si = ""+j.i;
		}
		//
		return j;
	},
	// *****************************************************************
	ma: function(p) // month array of max days
	// p = (optional)(JSON)parameters {d:dt} 
	// 	d = (date)date
	//	y = (number)year
	// return array
	{
		if(p && p.la && p.la==1) ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.ma",le:ALiBS.gv.dt.le});
		//
		var t;
		var y = (p && p.y);
		//
		if(y==null)
		{
			t = (p && p.d) || new Date();
			y = t.getFullYear();
		}
		//
		var f = 28; // february max days
		//
		if(ALiBS.dt.ly({y:y})) f = 29; // true = leap year, february = 29 days
		//
		return [31, f, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; 
	},
	// *****************************************************************
	ly: function(p) // leap year - bissexto
	// p = (optional)(JSON)parameters {d:dt} 
	// 	d = (date)date
	//	y = (number)year
	// return true/false
	{
		if(p && p.la && p.la==1) ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.ly",le:ALiBS.gv.dt.le});
		//
		var t;
		var y = (p && p.y);
		//
		if(y==null)
		{
			t = (p && p.d) || new Date();
			y = t.getFullYear();
		}
		//
		if(y==0) return false; // year 0 is not a leap year
		//
		if((y%4)!=0) // not divisible by 4 = not a leap year
		{
			return false; 
		}
		else
		{
			if((y%100)!=0) // divisible by 4 but not divisible by 100
			{
				return true; 
			}
			else 
			{
				if((y%400)!=0) // divisible by 4, divisible by 100 but not divisible by 400
				{
					return false;
				}
				else // divisible by 4, 100, 400
				{
					return true;
				}
			}
		}
	},
	// *****************************************************************
	sd2: function(n,p) // sum/substract days
	// n = (required)(number)days to sum/substract
	// p = (optional)(JSON)parameters {d:dt} 
	// 	d = (date)date
	//	y = (number)year
	//	m = (number)month [0-11]
	//	dy = (number)day
	// return JSON date
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.sd2",le:ALiBS.gv.dt.le});
		//
		if(typeof(n)!="number") return null; 
		n = Math.round(n);
		//
		var t;
		var y = (p && p.y);
		var m = (p && p.m);
		var d = (p && p.dy);
		var ma;
		//
		if(y==null || m==null || d==null)
		{
			t = (p && p.d) || new Date();
			y = t.getFullYear();
			m = t.getMonth(); // [0-11]
			d = t.getDate();
		}
		//
		ma = ALiBS.dt.ma({y:y});
		//
		if(n>0)
		{
			for(var i=0;i<n;i++)
			{
				if(d<ma[m]) // d < 31 OR 30 OR 29 OR 28 (last day of month)
				{
					d++;
				}
				else if(m<11) // month < 12 AND d == 31 OR 30 OR 29 OR 28 (last day of month)
				{
					d = 1;
					m++;
				}
				else // 31 December
				{
					d = 1;
					m = 0;
					y++;
					ma = ALiBS.dt.ma({y:y});
				}
			}
		}
		else if(n<0)
		{
			for(var i=0;i<-n;i++)
			{
				if(d>1) // d > (first day of month)
				{
					d--;
				}
				else if(m>0) // month > 0 AND d == 1
				{
					m--;
					d = ma[m];
				}
				else // 31 December
				{
					
					d = 31;
					m = 11;
					y--;
					ma = ALiBS.dt.ma({y:y});
				}
			}
		}
		else // n==0
		{
			// nothing
		}
		//
/*		if(n==281)
		{
			ALiBS.el.av("iTaLog","y="+y+"\n");
			ALiBS.el.av("iTaLog","m="+m+"\n");
			ALiBS.el.av("iTaLog","d="+d+"\n");
			ALiBS.el.av("iTaLog","date="+""+y+m+d+"\n");
		}
*/		//
		var x = ALiBS.dt.i2j(y,m,d);
		//
		//return ""+x.sy+x.sm+x.sd;
		return x;
	},
	// *****************************************************************
	s2j: function(s,p) // String date TO JSON date
	// can find the correct input format if year.length == 4 AND year >= 1300 
	// if year.length == 2, will try ymd, dmy, mdy, respectively
	//
	// s = (required)(string)string date
	// p = (optional)(JSON)parameters {f:"dmy"}
	//	f = (string)input format = dmy , ymd , mdy
	//
	// return Array(y,m,d,y0,m0,d0) 
	//	m = month = [0-11] number
	//	m0= month = [01-12] String
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.s2j",le:ALiBS.gv.dt.le});
		//
		if(!s || typeof(s)!="string" || s == NaN)
		{
			// set error
			return null; 
		}
		//
		var f = (p && p.f); // input format
		//
		var a0 = "";
		var a1 = "";
		var a2 = "";
		var a3 = "";
		//
		var d = -1; // day
		var m = -1; // month
		var y = -100*1000; // year
		//
		s = ALiBS.sg.rs(s,".","/");
		s = ALiBS.sg.rs(s,"-","/");
		//
		if(!f && s.length==10)
		{
			if(s.indexOf("/")==4) f = "ymd";
			else if(s.indexOf("/")==5)
			{
				a0 = parseInt(s.substr(0,2),10); 
				a1 = parseInt(s.substr(2,2),10); 
				//
				if(a0<=31 && a1<=12) // dmy
				{
					f = "dmy";
				}
				else if(a0<=12 && a1<=31) // mdy
				{
					f = "mdy";
				}
			}
		}
		//
		s = ALiBS.sg.rs(s,"/","");
		//
		if(!s)
		{
			// set error
			return null; 
		}
		//
		if(s.length!=6 && s.length!=8)
		{
			// set error
			return null; 
		}
		//
		if(s == NaN)
		{
			// set error
			return null; 
		}
		//
		if(!f || (f!="dmy" && f!="ymd" && f!="mdy"))
		{
			if(s.length==6) 
			{
				a0 = parseInt(s.substr(0,2),10); 
				a1 = parseInt(s.substr(2,2),10); 
				a2 = parseInt(s.substr(4,2),10); 
				//
				if(a1<=12 && a2<=31) // ymd
				{
					y = a0; 
					m = a1-1;
					d = a2;
				}
				else if(a0<=31 && a1<=12) // dmy
				{
					d = a0;
					m = a1-1;
					y = a2; 
				}
				else if(a0<=12 && a1<=31) // mdy
				{
					m = a0-1;
					d = a1;
					y = a2; 
				}
				else
				{
					// set error
					return null; 
				}
				//
				if(y>50) y+=1900;
				else y+=2000;
			}
			else if(s.length==8) 
			{
				a0 = parseInt(s.substr(0,2),10); 
				a1 = parseInt(s.substr(2,2),10); 
				a2 = parseInt(s.substr(4,2),10); 
				a3 = parseInt(s.substr(6,2),10); 
				//
				if(a2<=12 && a3<=31) // yymd
				{
					y = a0*100+a1; 
					m = a2-1;
					d = a3;
				}
				else if(a0<=31 && a1<=12) // dmyy
				{
					d = a0;
					m = a1-1;
					y = a2*100+a3; 
				}
				else if(a0<=12 && a1<=31) // mdyy
				{
					m = a0-1;
					d = a1;
					y = a2*100+a3; 
				}
				else
				{
					// set error
					return null; 
				}
			}
		}
		else if(s.length==6) 
		{
			if(f == "dmy")
			{
				d = parseInt(s.substr(0,2),10);
				m = parseInt(s.substr(2,2),10)-1;
				y = parseInt(s.substr(4,2),10); 
			}
			else if(f == "ymd")
			{
				y = parseInt(s.substr(0,2),10);
				m = parseInt(s.substr(2,2),10)-1;
				d = parseInt(s.substr(4,2),10); 
			}
			else if(f == "mdy")
			{
				m = parseInt(s.substr(0,2),10)-1;
				d = parseInt(s.substr(2,2),10);
				y = parseInt(s.substr(4,2),10); 
			}
			//
			if(y>50) y+=1900;
			else y+=2000;
		}
		else if(s.length==8)
		{
			if(f == "dmy")
			{
				d = parseInt(s.substr(0,2),10);
				m = parseInt(s.substr(2,2),10)-1;
				y = parseInt(s.substr(4,4),10); 
			}
			else if(f == "ymd")
			{
				y = parseInt(s.substr(0,4),10);
				m = parseInt(s.substr(4,2),10)-1;
				d = parseInt(s.substr(6,2),10); 
			}
			else if(f == "mdy")
			{
				m = parseInt(s.substr(0,2),10)-1;
				d = parseInt(s.substr(2,2),10);
				y = parseInt(s.substr(4,4),10); 
			}
		}
		//
/*		//
		ALiBS.el.av("iTaLog","a0="+a0+"\n");
		ALiBS.el.av("iTaLog","a1="+a1+"\n");
		ALiBS.el.av("iTaLog","a2="+a2+"\n");
		ALiBS.el.av("iTaLog","a3="+a3+"\n");
		//
		ALiBS.el.av("iTaLog","s="+s+"\n");
		ALiBS.el.av("iTaLog","f="+f+"\n");
		ALiBS.el.av("iTaLog","y="+y+"\n");
		ALiBS.el.av("iTaLog","m="+m+"\n");
		ALiBS.el.av("iTaLog","d="+d+"\n");
*/		//
		if(d<1 || m<0 || y==-100*1000) 
		{
			// set error
			return null; 
		}
		else if(d>31 || m>11) // || y<1900 || y>2500)
		{
			// set error
			return null; 
		}
		else if(m==1 && d>28 && !ALiBS.dt.ly({y:y})) 
		{
			// set error
			return null; 
		}
		else
		{
			return ALiBS.dt.i2j(y,m,d);
		}
	},
	// *****************************************************************
	i2j: function(y,m,d) // Int to JSON date
	// y = (required)(number)year
	// m = (required)(number)month [0-11]
	// d = (required)(number)day
	//
	// return JSON date
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.i2j",le:ALiBS.gv.dt.le});
		//
		var j = {};
		//
		j.y = y;
		j.m = m;
		j.d = d;
		//
		if(m+1<10) j.sm = "0"+(m+1);
		else j.sm = "" +(m+1);
		//
		if(d<10) j.sd = "0"+d;
		else j.sd = "" +d;
		//
		if(y<10) j.sy = "000"+y;
		else if(y<100) j.sy = "00"+y;
		else if(y<1000) j.sy = "0"+y;
		else j.sy = "" +y;
		//
		j.f = {};
		j.f["ymd"] = j.sy+j.sm+j.sd; 
		j.f["dmy"] = j.sd+j.sm+j.sy;
		j.f["mdy"] = j.sm+j.sd+j.sy;
		//
		return j;
	},
	// *****************************************************************
	df: function(p) // difference of 2 dates
	// p = (optional)(JSON)parameters {y0:2010,f:"ymd"}
	//	s0 = (string)date
	//	s1 = (string)date
	//	y0 = (number)year
	//	m0 = (number)month
	//	d0 = (number)day
	//	y1 = (number)year
	//	m1 = (number)month
	//	d1 = (number)day
	//	f0 = (string)s0 format = dmy , ymd , mdy
	//	f1 = (string)s1 format = dmy , ymd , mdy
	//
	// return number of days = difference
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.df",le:ALiBS.gv.dt.le});
		//
		var y0 = (p && p.y0);
		var m0 = (p && p.m0);
		var d0 = (p && p.d0);
		//
		var y1 = (p && p.y1);
		var m1 = (p && p.m1);
		var d1 = (p && p.d1);
		//
		var s0 = (p && p.s0);
		var s1 = (p && p.s1);
		//
		var f0 = (p && p.f0);
		var f1 = (p && p.f1);
		//
		var x;
		var l;
		//
		if(y0==null || m0==null || d0==null)
		{
			if(s0)
			{
				x = new ALiBS.dt.s2j(s0,{f:f0});
				//
				y0 = x.y;
				m0 = x.m;
				d0 = x.d;
				//
				s0 = x.f["ymd"];
			}
			else
			{
				return -1;
			}
		}
		else
		{
			x = ALiBS.dt.i2j(y0,m0,d0);
			s0 = x.f["ymd"];
		}
		//
		if(y1==null || m1==null || d1==null)
		{
			if(s1)
			{
				x = new ALiBS.dt.s2j(s1,{f:f1});
				//
				y1 = x.y;
				m1 = x.m;
				d1 = x.d;
				//
				s1 = x.f["ymd"];
			}
			else
			{
				return -2;
			}
		}
		else
		{
			x = ALiBS.dt.i2j(y1,m1,d1);
			s1 = x.f["ymd"];
		}
		//
		if(s0<s1)
		{
			// nothing
		}
		else if(s1<s0)
		{
			x = y0;
			y0 = y1;
			y1 = x;
			//
			x = m0;
			m0 = m1;
			m1 = x;
			//
			x = d0;
			d0 = d1;
			d1 = x;
			//
			x = s0;
			s0 = s1;
			s1 = x;
		}
		else // s0 == s1
		{
			return 0;
		}
/*		//
		ALiBS.el.av("iTaLog","y0="+y0+"\n");
		ALiBS.el.av("iTaLog","m0="+m0+"\n");
		ALiBS.el.av("iTaLog","d0="+d0+"\n");
		ALiBS.el.av("iTaLog","y1="+y1+"\n");
		ALiBS.el.av("iTaLog","m1="+m1+"\n");
		ALiBS.el.av("iTaLog","d1="+d1+"\n");
*/		//
		var t0 = 0;
		//
		l = 370; // security limit = not to be achieved
		for(var i=0;i<l;i++) 
		{
			x = ALiBS.dt.sd2(i,{y:y0,m:0,dy:1});
			//
			if(x.f["ymd"]==s0)
			{
				t0-=i; 
				break;
			}
		}
		//
		l = 370; // security limit = not to be achieved
		for(var i=0;i<l;i++) 
		{
			x = ALiBS.dt.sd2(i,{y:y1,m:0,dy:1});
			//
			if(x.f["ymd"]==s1)
			{
				t0+=i; 
				break;
			}
		}
		//
		l = y1-y0; // security limit = not to be achieved
		for(var i=0;i<l;i++)
		{
			if(ALiBS.dt.ly({y:y0+i})) t0+=366;
			else t0+=365;
		}
		//
		return t0;
	},
	// *****************************************************************
	dw: function(p) // day of week
	// p = (optional)(JSON)parameters {y:2010,f:"ymd"}
	//	s = (string)date
	//	y = (number)year
	//	m = (number)month [0-11]
	//	d = (number)day
	//	f = (string)input format = dmy , ymd , mdy
	//
	// return day of week 0=Sunday, 1=Monday, 2=Tuesday, 3=Wednesday, 4=Thursday, 5=Friday, 6=Saturday
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.dw",le:ALiBS.gv.dt.le});
		//
		var y = (p && p.y);
		var m = (p && p.m);
		var d = (p && p.d);
		//
		var s = (p && p.s);
		//
		var f = (p && p.f);
		//
		var x;
		//
		if(y==null || m==null || d==null)
		{
			if(s)
			{
				x = ALiBS.dt.s2j(s,{f:f});
				//
				y = x.y;
				m = x.m;
				d = x.d;
				//
				s = x.f["ymd"];
			}
			else
			{
				return -1;
			}
		}
		else
		{
			x = ALiBS.dt.i2j(y,m,d);
			s = x.f["ymd"];
		}
		//
		x = ALiBS.dt.df({s0:s,s1:"20100103",f0:"ymd",f1:"ymd"});
		//
		if(s>="20100103")
		{
			return (x%7);  
		}
		else
		{
			if(x%7==0) return 0;
			else return 7-(x%7);
		}
	},
	// *****************************************************************
	dp: function(y) // day of Pascoa
	// y = (required)(number)year
	// return JSON dates "ymd"
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.dp",le:ALiBS.gv.dt.le});
		//
		if(typeof(y)!="number") return null;
		//
		var j = {};
		var d;
		var m;
		var w;
		var r; // real date
		//
		x = y%19; // +1, nao precisa pois Array começa em 0
		//
		var a = new Array();
		a[a.length] = new Array(1,14,4);
		a[a.length] = new Array(2,3,4);
		a[a.length] = new Array(3,23,3);
		a[a.length] = new Array(4,11,4);
		a[a.length] = new Array(5,31,3);
		a[a.length] = new Array(6,18,4);
		a[a.length] = new Array(7,8,4);
		a[a.length] = new Array(8,28,3);
		a[a.length] = new Array(9,16,4);
		a[a.length] = new Array(10,5,4);
		a[a.length] = new Array(11,25,3);
		a[a.length] = new Array(12,13,4);
		a[a.length] = new Array(13,2,4);
		a[a.length] = new Array(14,22,3);
		a[a.length] = new Array(15,10,4);
		a[a.length] = new Array(16,30,3);
		a[a.length] = new Array(17,17,4);
		a[a.length] = new Array(18,7,4);
		a[a.length] = new Array(19,27,3);
		//
		d = a[x][1];
		m = a[x][2];
		//
		x = ALiBS.dt.i2j(y,m-1,d); // reference date from table
		w = 7 - ALiBS.dt.dw({y:y,m:m-1,d:d}); // days to next sunday
		//
		r = ALiBS.dt.sd2(w,{y:y,m:m-1,dy:d}); // sum days
		j.pa = r.f["ymd"]; // Pascoa = Sunday
		//
		r = ALiBS.dt.sd2(w-47,{y:y,m:m-1,dy:d}); // sum days
		j.ca = r.f["ymd"]; // Carnival = Tuesday
		//
		r = ALiBS.dt.sd2(w+60,{y:y,m:m-1,dy:d}); // sum days
		j.cc = r.f["ymd"]; // Corpus Christi = Thursday
		//
		return j;
	},
	// *****************************************************************
	u2s: function(u,p) // Unix date to string
	// u = (required)(string)Unix date ("1346672955") seconds since 01/01/1970 - 0hs (ATTENTION: it's NOT milliseconds)
	// p = (optional)(JSON)parameters {}
	//	t = (number)type
	//		0 = local time = Default
	//		1 = GMT time
	//
	// return (string)Date String ("Mon Sep 03 2012 08:49:15 GMT-0300 (BRT)")
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.u2s",le:ALiBS.gv.dt.le});
		//
		var d;
		//
		var r;
		//
		if(u=='')
		{
			d = new Date();
			//u = parseInt(d.valueOf() / 1000);
		}
		else
		{
			d = new Date(parseInt(u,10)*1000);
		}
		//
		if(p && p.t) r = d.toGMTString();
		else r = d.toString();
		//
		return r;
	},
	// *****************************************************************
	u2ts: function(u,p) // Unix date to timestamp
	// u = (required)(string)Unix date ("1346672955") seconds since 01/01/1970 - 0hs (ATTENTION: it's NOT milliseconds)
	// p = (optional)(JSON)parameters {}
	//	ty = (number)type
	//		0 = DEFAULT = unix date without milliseconds = zero milliseconds
	//		1 = milliseconds date
	//
	//	s0 = (char)separador ano-mes-dia
	//	s1 = (char)separador data-hora
	//	s2 = (char)separador hora-minuto-segundo
	//	s3 = (char)separador milisegundo
	//	dt = (number)date and time
	//		0 = (DEFAULT) = date and time = 2012-06-13 10:15:42.047 (including miliseconds)
	//		1 = date = 2012-06-13
	//		2 = date and time + Z = 2012-06-13 10:15:42Z (no miliseconds)
	//		3 = date and time = 2012-06-13 10:15:42 (no miliseconds)
	//		4 = date and time = 2012-06-13 10:15:42.13 (miliseconds with 2 places/positions)
	//
	// return String reverse Date yyyy/mm/dd-hh-mm-ss-mmm
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.u2ts",le:ALiBS.gv.dt.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.ty)!="number") z.ty = 0; // 
			if(typeof(z.dt)!="number") z.dt = 0; // 
			if(typeof(z.s0)!="string") z.s0 = ""; // 
			if(typeof(z.s1)!="string") z.s1 = ""; // 
			if(typeof(z.s2)!="string") z.s2 = ""; // 
			if(typeof(z.s3)!="string") z.s3 = ""; // 
			//
			// local variables [BEST-PRACTICE]
			z.ec = -100; // 
			z.y0 = (z.ty==1? 1 : 1000);
//console.log("z.y0="+z.y0);
			//
			z.ec = -200; // 
			z.d0 = new Date(parseInt(u,10)*z.y0);
//console.log("z.d0="+z.d0);
			//
			z.ec = -201; // 
			r = ALiBS.dt.ts({d:z.d0,dt:z.dt,s0:z.s0,s1:z.s1,s2:z.s2,s3:z.s3});
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.dt.u2ts",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	s2s: function(s0,p) // string to string
	// s0 = (required)(string)string s0 "Fri Jun 28 09:37:20 BRT 2013"
	// p = (optional)(JSON)parameters {}
	//
	// return "Fri Jun 28 2013 09:37:20 GMT-300 (BRT)" (parameter for Date())
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.s2s",le:ALiBS.gv.dt.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.ty)!="number") z.ty = 0; // 
			//
			// local variables [BEST-PRACTICE]
			z.ec = -100; // 
			z.sx = s0.split(" ");
//console.log("z.sx="+z.sx);
			//
			z.ec = -200; // 
			r = z.sx[0]+" "+z.sx[1]+" "+z.sx[2]+" "+z.sx[5]+" "+z.sx[3]+" GMT-300 (BRT)";
//console.log("r="+r);
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.dt.s2s",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	s2u: function(s) // Date string to Unix date
	// s = (required)(string)Date String ("Mon Sep 03 2012 08:49:15 GMT-0300 (BRT)")
	//
	// return (string)Unix date ("1346672955") in seconds
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.s2u",le:ALiBS.gv.dt.le});
		//
		var d;
		//
		if(s=='')
		{
			d = new Date();
			//s = d.toString();
		}
		else
		{
			d = new Date(s);
		}
		//
		return parseInt(d.valueOf()/1000,10);
	},
	// *****************************************************************
	t2j: function(s,p) // String Timestamp to JSON Timestamp - TODO(2013-05-30): STRESS-TEST
	// s = (required)(string)String Timestamp 2013-06-02 08:00:00    or   2013-06-02 08:00:00.000
	// p = (optional)(JSON)parameters {}
	//	
	// return (JSON)Timestamp
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		//
		ALiBS.sy.la(5+0,"s="+s,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
		//
		// local varibles [BEST-PRACTICE]
		z.s = s.replace(/[-:\s.]/g,"");
		//
		var r = {}; // [BEST-PRACTICE]
		//
		if(z.s.length>=8)
		{
			r.y = z.s.substr(0,4); // 2013
			r.m = z.s.substr(4,2); // 05
			r.d = z.s.substr(6,2); // 30
		}
		//
		if(z.s.length>=12)
		{
			r.h = z.s.substr(8,2); // 08
			r.n = z.s.substr(10,2); // 00
		}
		//
		if(z.s.length>=14)
		{
			r.s = z.s.substr(12,2); // 00
		}
		//
		if(z.s.length>=17)
		{
			r.i = z.s.substr(14,3); // 000
		}
		//
		r.dd = new Date(r.y,r.m-1,r.d,r.h,r.n,r.s);
		//
		ALiBS.sy.la(5+0,"z.s="+z.s,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.y="+r.y,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.m="+r.m,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.d="+r.d,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.h="+r.h,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.n="+r.n,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.s="+r.s,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.i="+r.i,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.dd="+r.dd,{o:"ALiBS.dt.t2j",le:ALiBS.gv.dt.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	t2u: function(s,p) // String Timestamp to UNIX(number) Timestamp - TODO(2013-05-30): STRESS-TEST
	// s = (required)(string)String Timestamp 2013-06-02 08:00:00    or   2013-06-02 08:00:00.000
	// p = (optional)(JSON)parameters {}
	//	
	// return (UNIX)(number)Timestamp in milliseconds
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.t2u",le:ALiBS.gv.dt.le});
		//
		ALiBS.sy.la(5+0,"s="+s,{o:"ALiBS.dt.t2u",le:ALiBS.gv.dt.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
		//
		// local varibles [BEST-PRACTICE]
		z.s = (s+"00000000000000000000000").substr(0,23); // 2013-06-02 08:00:00.000
		z.j = ALiBS.dt.t2j(z.s);
		//
		r = new Date(z.j.y, parseInt(z.j.m,10)-1, z.j.d, z.j.h, z.j.n, z.j.s, z.j.i) -1 +1;
		//
		ALiBS.sy.la(5+0,"z.s="+z.s,{o:"ALiBS.dt.t2u",le:ALiBS.gv.dt.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	m2j: function(m,p) // Milliseconds to JSON
	// m = (required)(number)milliseconds
	// p = (optional)(JSON)parameters {}
	//	cm = (number)custom month = DEFAULT = 30 days
	//	cd = (number)custom day = DEFAULT = 24 hours
	//	rt = (string)custom return = DEFAULT = y (year) 
	//	
	// return (JSON)Timestamp 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		//
		ALiBS.sy.la(5+0,"m="+m,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		if(typeof(z.cm)!="number") z.cm = "30"; // custom month = DEFAULT = 30 days
		if(typeof(z.cd)!="number") z.cd = "24"; // custom day = DEFAULT = 24 hours
		if(typeof(z.rt)!="string") z.rt = "y"; // custom returny = DEFAULT = y (year)
		//
		// local varibles [BEST-PRACTICE]
		z.ds = 1000; // millisecond division
		z.dn = 60*z.ds; // second division
		z.dh = 60*z.dn; // minute division
		//
		z.dd = z.cd*z.dh; // hour division
		z.dm = 30*z.dd; // day division
		//
		z.dy = 12*z.dm; // month division
		//
		// ************************
		//
		var r = {}; // [BEST-PRACTICE]
		//
		z.re = m;
		//
		r.y = 0;
		r.m = 0;
		r.d = 0;
		r.h = 0;
		r.n = 0;
		r.s = 0;
		r.i = 0;
		//
		if("y".indexOf(z.rt)>=0)
		{
			r.y = Math.floor(z.re/z.dy);
			z.re = z.re%z.dy;
		}
		//
		if("ym".indexOf(z.rt)>=0)
		{
			r.m = Math.floor(z.re/z.dm);
			z.re = z.re%z.dm;
		}
		//
		if("ymd".indexOf(z.rt)>=0)
		{
			r.d = Math.floor(z.re/z.dd);
			z.re = z.re%z.dd;
		}
		//
		if("ymdh".indexOf(z.rt)>=0)
		{
			r.h = Math.floor(z.re/z.dh);
			z.re = z.re%z.dh;
		}
		//
		if("ymdhn".indexOf(z.rt)>=0)
		{
			r.n = Math.floor(z.re/z.dn);
			z.re = z.re%z.dn;
		}
		//
		if("ymdhns".indexOf(z.rt)>=0)
		{
			r.s = Math.floor(z.re/z.ds);
			z.re = z.re%z.ds;
		}
		//
		r.i = z.re;
		//
		// ************************
		//
		ALiBS.sy.la(5+0,"z.dy="+z.dy,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"z.dm="+z.dm,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"z.dd="+z.dd,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"z.dh="+z.dh,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"z.dn="+z.dn,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"z.ds="+z.ds,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		//
		ALiBS.sy.la(5+0,"r.y="+r.y,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.m="+r.m,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.d="+r.d,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.h="+r.h,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.n="+r.n,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.s="+r.s,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		ALiBS.sy.la(5+0,"r.i="+r.i,{o:"ALiBS.dt.m2j",le:ALiBS.gv.dt.le});
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
function ALiBS_dt_ts()
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_dt_ts",le:ALiBS.gv.dt.le});
	//
	return ALiBS.dt.ts({s0:"-",s1:" ",s2:":",s3:"."} ).substring(0,19);
};
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.dt("+(++ALiBS.gv.dt.nn)+"x)",{o:"ALiBS.dt"});
ALiBS.sy.la(0+0,"ALiBS.gv.dt.rl="+ALiBS.gv.dt.rl,{o:"ALiBS.dt"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.dt.le="+ALiBS.gv.dt.le+ALiBS.gv.sy.se,{o:"ALiBS.dt"});


/*

function OnCalcUnixToString()
{
	var oIn = document.getElementById("txtUnixTimestamp");
	var oDate;
	if (oIn.value == '')
	{
		oDate = new Date();
		oIn.value = parseInt(oDate.valueOf() / 1000);
	}
	else
		oDate = new Date(parseInt(oIn.value) * 1000);

	document.getElementById("divResult1").innerHTML = '<p>' + oDate.toString() + '</p>';
	document.getElementById("divResult2").innerHTML = '<p>' + oDate.toGMTString() + '</p>';
}

function OnCalcStringToUnix()
{
	var oIn = document.getElementById("txtString");
	var oDate;
	if (oIn.value == '')
	{
		oDate = new Date();
		oIn.value = oDate.toString();
	}
	else
		oDate = new Date(oIn.value);

	document.getElementById("divResult3").innerHTML = '<p>' + parseInt(oDate.valueOf() / 1000) + '</p>';
}


*/

