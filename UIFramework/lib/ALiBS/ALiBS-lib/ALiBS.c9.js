/* *************************************************************************
// INFO:

var base10 = parseInt(string,baseFrom[2-16])

string = value to be converted
radix = baseFrom used in the string
return = (number)(base10)ALWAYS

******************************

If the radix parameter is omitted:
16 if string begins with "0x"
8 if string begins with "0"
10 if string begins with any other value

******************************

parseInt COMPARE

toBase(string,baseFrom[2-100],baseTo[2-100])

parseInt(string,baseFrom) = toBase(string,baseFrom,baseTo:10)

******************************

baseTo[2-16] = baseFrom[2-16].toString(baseTo[2-16])

baseFrom = (number)(base[2-16])

******************************
******************************
******************************
******************************

r = parseInt("10101010101010010110001010101110101000101011101010",2); // 750577927228138 (base10)
r = r.toString(36); // 7e22qmiuwa (base36)
//
//
r = parseInt("7e22qmiuwa",36); // 750577927228138 (base10)
r.toString(2); // 10101010101010010110001010101110101000101011101010 (base2)

******************************

r = ALiBS.c9.sb("10101010101010010110001010101110101000101011101010",2); // 750577927228138 (base10)
r = ALiBS.c9.nb(r,36); // 7E22QMIUWA (base36)
//
//
r = ALiBS.c9.sb("7E22QMIUWA",36); // 750577927228138 (base10)
r = ALiBS.c9.nb(r,2); // 10101010101010010110001010101110101000101011101010 (base2)

******************************

r = ALiBS.c9.sb("10101010101010010110001010101110101000101011101010",2); // 750577927228138 (base10)
r = ALiBS.c9.nb(r,113); // 3Lwe#MUP#F (base113)
//
//
r = ALiBS.c9.sb("3Lwe#MUP#F",113); //  (base10)
r = ALiBS.c9.nb(r,2); // 10101010101010010110001010101110101000101011101010 (base2)

******************************

r = ALiBS.c9.sbb("10101010101010010110001010101110101000101011101010",2,113); // 3Lwe#MUP#F
r = ALiBS.c9.sbb(r,113,2); // 10101010101010010110001010101110101000101011101010

******************************

c = s.charAt(i);
c = s.charCodeAt(i);
String.fromCharCode(n);

******************************

(i18n/special/text)String to (base16) String = (1 byte TO 2 bytes)
(i18n/special/text)String to (base64) String = (3 bytes TO 4 bytes)
Array of (numbers) = string decomposta em bytes = parseInt(array[i],base)
Array of(Strings) = string decomposta em chars ????

OR

(number) to (baseTo) String 
(numeric string) to (baseTo) number 

******************************

# Base64: 
- final size = original *4/3
- uses A-Z, a-z, 0-9, / and +
- case sensitive
- shorter representations because it uses a 64 character set instead of hexadecimal's 16. 

many implementations follow IETF PEM RFC 1421
which specifies A-Z, a-z, 0-9, / and + 
while using = for padding. 

Because + and / need to be URI escaped, 
Wikipedia mentions a variant 
that uses * and - in place of + and / respectively 
and removes line breaks as well as padding. 

* cannot be used in DOM ids, so, use _- instead.

******************************

# Hexadecimal 
- final size = original *2
- uses 0-9 and A-F 
- case insensitive
- the representations are longer due to the smaller character set
- case insensitivy can be advantageous in certain situations (RACF)

ver sites http://ostermiller.org/calc/encode.html
e http://decodebase64.com/

// *************************************************************************
// TODO:

Problemas com numeros grandes?
ALiBS.c9.rx(6559918375484030283637382912942,64)

TODO(2010-09-14):ALiBS.sg.ih() vai ficar em c9?
TODO(2010-09-14):ALiBS.sg.i64() vai ficar em c9?

*/
//
// *************************************************************************
//
if(!ALiBS.gv.c9) ALiBS.gv.c9 = {}; // global values
//
if(!ALiBS.gv.c9.le) ALiBS.gv.c9.le = 0; // local log status
//
ALiBS.gv.c9.nn = 0; // x times loaded
//
ALiBS.gv.c9.rl = "15.0803.0936";
//
// *************************************************************************
//
ALiBS.c9 = // c9 = base conversion (2 to 100...n)
{
	_s0: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-+$%&?@(*)[/]<=>{|}:;,.^~`'", // [0-89] = 90 chars = base90
	_s1: new Array("0#","1#","2#","3#","4#","5#","6#","7#","8#","9#","A#","B#","C#","D#","E#","F#","G#","H#","I#","J#","K#","L#","M#"), // [90-113] = 23 chars (reverse)
	// - = used to represent negative numbers <= base63 
	// ! = used to represent negative numbers > base64
	// # = used to represent chars for base90 or greater
	//	#0 = 90, #1 = 91
	// ## = not used = free for use
	// \ = not used = avoid using
	// " = not used = avoid using
	// TODO(2011-11-16): represent space, tab, \r, \n 
	// *****************************************************************
	ns: function(n0,b0,p) // [2-100] = number TO base string = toString(baseTo)[2-16] TODO(2010-09-15):substituir todos os n.toString()
	/** 
	Method <static> <string> ALiBS.c9.ns(n0,b0,p)

		Description: <number> base10 to <string> baseNN

		Parameters:
			n0 = (required)(number)number base10
			b0 = (required)(number)baseNN (2 to 100...n)

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> Numeric String coded in baseNN

		Example(s):
			var ok = ALiBS.c9.ns();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.ns",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(n0)!="number") throw "ALiBS.c9.ns.Exception: Error: Invalid Parameter Type: (number) n0 passed as "+typeof(n0); // 
			if(typeof(b0)!="number") throw "ALiBS.c9.ns.Exception: Error: Invalid Parameter Type: (number) b0 passed as "+typeof(b0); // 
			//
			ALiBS.sy.la(5+0,"n0="+n0,{o:"ALiBS.c9.ns",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"b0="+b0,{o:"ALiBS.c9.ns",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_s0 = new Array();
			z.v_q0 = Math.floor(Math.abs(n0));
			z.v_r0 = null;
			//
			// ************************
			//
			while(true) 
			{
				z.v_r0 = z.v_q0%b0;
				//
				if(z.v_r0<ALiBS.c9._s0.length) z.v_s0[z.v_s0.length] = ALiBS.c9._s0.charAt(z.v_r0);
				else z.v_s0[z.v_s0.length] = ALiBS.c9._s1[z.v_r0-ALiBS.c9._s0.length];
				//
				z.v_q0 = (z.v_q0-z.v_r0)/b0;
				// 
				if(z.v_q0==0) break;
			}
			//
			if(n0<0) 
			{
				if(b0<64) z.v_s0[z.v_s0.length] = "-";
				else z.v_s0[z.v_s0.length] = "!"; // b>=64
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + ALiBS.sg.rv(z.v_s0.join(""));
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.ns",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.ns",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.ns",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	sn: function(s0,b0,p) // [2-100] = string (FROM base) (TO base 10) = parseInt(s,baseFrom)[2-16] TODO(2010-09-15):substituir todos os parseInt()
	/** 
	Method <static> <number> ALiBS.c9.sn(s0,b0,p)

		Description: <string> baseNN to <number> base10

		Parameters:
			s = (required)(string)string coded in baseFrom
			b = (required)(number)baseFrom (2 to 100...n)

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number> number base10

		Example(s):
			var ok = ALiBS.c9.sn();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.sn",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.c9.sn.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(b0)!="number") throw "ALiBS.c9.sn.Exception: Error: Invalid Parameter Type: (number) b0 passed as "+typeof(b0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.c9.sn",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"b0="+b0,{o:"ALiBS.c9.sn",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_n0 = 0;
			z.v_c0;
			z.v_v0;
			z.v_ii = 0;
			z.v_e0; // exponent
			//
			// ************************
			//
			z.ec = -1200; // 
			if(b0<64)
			{
				z.ec = -1210; // 
				if(s0.charAt(0)=="-") z.v_ii = 1;
			}
			else // b>=64
			{
				z.ec = -1220; // 
				if(s0.charAt(0)=="!") z.v_ii = 1;
			}
			//
			z.ec = -1300; // 
			if(b0<=36) s0 = s0.toUpperCase();
			//
			//e = s.length-1-ii; // do not count negative signal
			z.ec = -1400; // 
			z.v_e0 = s0.length-1; 
			//
			z.ec = -1500; // 
			for(var i=0;i<s0.length;i++)
			{
				z.ec = -1510; // 
				z.v_c0 = s0.charAt(i);
				//
				z.ec = -1520; // 
				if(z.v_c0==String.fromCharCode(35)) z.v_e0--; // # correct string length
			}
			//ALiBS.sy.la(5,"e="+e,{o:"ALiBS.c9",le:true});
			//
			z.ec = -1600; // 
			for(var i=z.v_ii;i<s0.length;i++)
			{
				z.ec = -1610; // 
				z.v_c0 = s0.charAt(i);
				//ALiBS.sy.la(5,"z.v_c0="+z.v_c0,{o:"ALiBS.c9",le:true});
				//
				z.ec = -1620; // 
				if(z.v_c0==String.fromCharCode(35)) // #
				{
					z.ec = -1630; // 
					i++;
					z.v_e0++;
					z.v_c0 = s0.charAt(i);
					z.v_v0 = ALiBS.c9._s0.indexOf(z.v_c0);
					//
					//ALiBS.sy.la(5,"i(#)="+i,{o:"ALiBS.c9",le:true});
					//ALiBS.sy.la(5,"z.v_c0(#)="+z.v_c0,{o:"ALiBS.c9",le:true});
					//ALiBS.sy.la(5,"z.v_v0(#)="+z.v_v0,{o:"ALiBS.c9",le:true});
					//ALiBS.sy.la(5,"z.v_e0-i(#)="+(z.v_e0-i),{o:"ALiBS.c9",le:true});
					//
					z.ec = -1640; // 
					if(z.v_v0<0)
					{
						z.v_n0 = null;
						break;
						//return null;
					}
					//
					z.ec = -1650; // 
					z.v_v0 += ALiBS.c9._s0.length;
					//z.v_n0 = z.v_n0 + z.v_v0*Math.pow(b0,e-i);
				}
				else
				{
					z.ec = -1710; // 
					z.v_v0 = ALiBS.c9._s0.indexOf(z.v_c0);
					//
					//ALiBS.sy.la(5,"i()="+i,{o:"ALiBS.c9",le:true});
					//ALiBS.sy.la(5,"z.v_c0()="+z.v_c0,{o:"ALiBS.c9",le:true});
					//ALiBS.sy.la(5,"z.v_v0()="+z.v_v0,{o:"ALiBS.c9",le:true});
					//
					z.ec = -1720; // 
					if(z.v_v0<0)
					{
						z.v_n0 = null;
						break;
						//return null;
					}
					//z.v_n0 = z.v_n0 + z.v_v0*Math.pow(b0,e-i);
				}
				//
				//z.v_n0 = z.v_n0 + z.v_v0*Math.pow(b0,(s0.length-i-1));
				z.ec = -1800; // 
				z.v_n0 = z.v_n0 + z.v_v0*Math.pow(b0,z.v_e0-i);
			}
			//
			z.ec = -1900; // 
			if(z.v_n0 && z.v_ii==1) z.v_n0 = -z.v_n0;
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_n0; 
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.sn",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.sn",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.sn",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	ss: function(s0,bf,bt,p) // 
	/** 
	Method <static> <number> ALiBS.c9.ss(s0,bf,bt,p)

		Description: String (FROM base) (TO base) 

		Parameters:
			s0 = (required)(string) string coded in baseFrom
			bf = (required)(number) baseFrom
			bt = (required)(number) baseTo

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> string coded in baseNN

		Example(s):
			var ok = ALiBS.c9.ss();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.ss",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.c9.ss.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(bf)!="number") throw "ALiBS.c9.ss.Exception: Error: Invalid Parameter Type: (number) bf passed as "+typeof(bf); // 
			if(typeof(bt)!="number") throw "ALiBS.c9.ss.Exception: Error: Invalid Parameter Type: (number) bt passed as "+typeof(bt); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.c9.ss",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"bf="+bf,{o:"ALiBS.c9.ss",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"bt="+bt,{o:"ALiBS.c9.ss",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = ALiBS.c9.sn(s0,bf); // (string)baseFrom to (number)base10
			//ALiBS.sy.la(5,"r(c9-sb)="+r,{o:"ALiBS.c9",le:true});
			r = ALiBS.c9.ns(r,bt); // (number)base10 to (string)baseTo
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.ss",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.ss",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.ss",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	sh: function(s0,p) // (string OR array) TO hexa
	/** 
	Method <static> <number> ALiBS.c9.sh(s0,p)

		Description: String OR Array TO Hexa

		Parameters:
			s = (required)(string) string 
				OR 
			    (Array) strings 
				OR 
			    (Array) numbers to convert

			p = (optional)(JSON)parameter {}
				px = prefix 
					"" = Default
					0x
				x = OLD Compatibility

		Observations:
	
		Return Value:
			<static> <string> string in hexa

		Example(s):
			var ok = ALiBS.c9.sh();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.sh",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string" && ALiBS.sy.ty(s0)!="ob"+"ject-A"+"rray") throw "ALiBS.c9.sh.Exception: Error: Invalid Parameter Type: (string/ob"+"ject-A"+"rray) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.c9.sh",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_px = z.px || z.x; // Parameter Pattern and/or OLD Parameter Compatibility

			if(typeof(z.p_px)!="string" || z.p_px!="0x") z.p_px = ""; // TODO(2015-06-29): NOT used
			//
			ALiBS.sy.la(5+0,"z.p_px="+z.p_px,{o:"ALiBS.c9.sh",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_v0;
			z.v_m0;
			z.v_t0;
			z.v_a0 = new Array();
			//
			// ************************
			//
			z.ec = -1200; // 
			for(var i=0;i<s0.length;i++)
			{
				z.ec = -1300; // 
				if(typeof(s0)=="obj"+"ect") // Array of numbers // TODO(2012-06-27): substituir typeof()=="object"
				{
					z.ec = -1310; // 
					if(typeof(s0[i])=="number")
					{
						z.ec = -1320; // 
						z.v_v0 = s0[i];
					}
					else if(typeof(s0[i])=="string") // Array of Strings with numbers, letters and specials
					{
						z.ec = -1350; // 
						if(s0[i]!="") // 
						{
							z.ec = -1360; // 
							z.v_v0 = s0[i].charCodeAt(0);
						}
						else // empty position in array s0
						{
							z.ec = -1370; // 
							z.v_a0 = null;
							break;
						}
					}
					else // other
					{
						z.ec = -1380; // 
						z.v_a0 = null;
						break;
					}
				}
				else if(typeof(s0)=="string") // String with numbers, letters and specials
				{
					z.ec = -1600; // 
					z.v_v0 = s0.charCodeAt(i);
				}
				else // other
				{
					z.ec = -1700; // 
					z.v_a0 = null;
					break;
				}
				//
				z.ec = -2200; // 
				z.v_m0 = Math.floor(z.v_v0/16); // multiplicador
				z.ec = -2300; // 
				z.v_t0 = z.v_v0-z.v_m0*16; // resto	
				//
				z.ec = -2400; // 
				z.v_a0[z.v_a0.length] = ALiBS.c9._s0.charAt(z.v_m0);
				z.ec = -2500; // 
				z.v_a0[z.v_a0.length] = ALiBS.c9._s0.charAt(z.v_t0);
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			if(z.v_a0==null) r = null;
			else r = "" + z.v_a0.join(""); // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.sh",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.sh",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.sh",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	hs: function(h0,p) // hexa TO (string OR array)
	/** 
	Method <static> <string> / <reference> <array> ALiBS.c9.hs(h0,p)

		Description: aaaa

		Parameters:
			h = (required)(string)hex string

			p = (optional)(JSON)parameter {}
				rt = return type 
					s = String = DEFAULT
					a = Array
				t = return type (OLD Compatibility)

		Observations:
	
		Return Value:
			<static> <string> string
				OR
			<reference> <array> array

		Example(s):
			var ok = ALiBS.c9.hs(h0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.hs",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(h0)!="string") throw "ALiBS.c9.hs.Exception: Error: Invalid Parameter Type: (string) h0 passed as "+typeof(h0); // 
			//
			ALiBS.sy.la(5+0,"h0="+h0,{o:"ALiBS.c9.hs",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_rt = z.rt || z.t; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_rt)!="string") z.p_rt = "s"; // 
			//
			ALiBS.sy.la(5+0,"z.p_rt="+z.p_rt,{o:"ALiBS.c9.hs",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_l0 = h0.length;
			//
			z.ec = -1300; // 
			h0 = h0.toUpperCase();
			z.ec = -1400; // 
			z.v_a0 = ALiBS.c9._s0.substr(0,16);
			//
			z.ec = -1500; // 
			z.v_l1 = z.v_a0.length;
			z.v_ok = false;
			//
			z.ec = -1600; // 
			z.v_n0 = new Array();
			z.v_c0;
			//
			z.ec = -1700; // 
			z.v_x0 = h0.substr(0,2);
			//
			// ************************
			//
			z.ec = -2000; // 
			for(var i=0;i<z.v_l0;i++) // // TODO: testar ALiBS.c9.ih() 
			{
				z.ec = -2100; // 
				for(var j=0;j<z.v_l1;j++)
				{
					z.ec = -2200; // 
					if(h0.charAt(i)==z.v_a0.charAt(j))
					{
						z.ec = -2300; // 
						z.v_ok = true;
						break;
					}
				}
				//
				z.ec = -2400; // 
				if(!z.v_ok)
				{
					z.ec = -2500; // 
					z.v_n0 = null;
					break;
				}
				else 
				{
					z.ec = -2600; // 
					z.v_ok = false;
				}
			}
			//
			//for(var i=((h.substr(0,2)=="0x")?2:0);i<h.length;i+=2)
			//
			if(z.v_n0!=null) // null inside for
			{
				z.ec = -5000; // 
				if(z.v_x0=="0x")
				{
					z.ec = -5100; // 
					z.v_x0 = 2;
				}
				else
				{
					z.ec = -5200; // 
					z.v_x0 = 0;
				}
				//
				z.ec = -6000; // 
				for(var i=z.v_x0;i<h0.length;i+=2)  
				{
					z.ec = -6100; // 
					z.v_c0 = ALiBS.c9.sn(h0.substr(i,2),16);
					//
					z.ec = -6200; // 
					if(z.p_rt=="a") // a = array
					{
						z.ec = -6300; // 
						z.v_n0[z.v_n0.length] = z.v_c0; // Array
					}
					else // s = string
					{
						z.ec = -6400; // 
						z.v_n0[z.v_n0.length] = String.fromCharCode(z.v_c0); // String
					}
				}
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			if(z.v_n0==null) r = null;
			else if(z.p_rt=="a") r = z.v_n0; // array
			else r = z.v_n0.join(""); // default = string
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.hs",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.hs",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.hs",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	ih: function(s0,p) // isHexa TODO(2010-09-14): colocar em stress test TODO(2010-09-14):ALiBS.sg.ih() vai ficar em c9?
	/** 
	Method <static> <boolean> ALiBS.c9.ih(s0,p)

		Description: isHexa

		Parameters:
			s0 = (required)(string)string

		Observations:
	
		Return Value:
			<static> <boolean> true/false

		Example(s):
			var ok = ALiBS.c9.ih(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.ih",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = true; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.c9.ih.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.c9.ih",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_l0 = s.length;
			z.v_c0;
			//
			// ************************
			//
			z.ec = -2000; // 
			for(var i=0;i<z.v_l0;i++)
			{
				z.ec = -2100; // 
				z.v_c0 = s0.charAt(i);
				//
				z.ec = -2200; // 
				if(ALiBS.sg.is(z.v_c0))
				{
					z.ec = -2300; // 
					r = false; // not special = alpha OR num
					break;
				}
				//
				z.ec = -2400; // 
				if((z.v_c0<"0") || (z.v_c0>"f"))
				{
					z.ec = -2500; // 
					r = false;
					break;
				}
				//
				z.ec = -2600; // 
				if((z.v_c0>"F") && (z.v_c0<"a"))
				{
					z.ec = -2700; // 
					r =  false;
					break;
				}
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			// r = r;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.ih",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.ih",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.ih",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	_gka: 
	{
		// To Generate new shuffle
		// var r = ALiBS.c2.sa("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-");
		//
		// r.sort(function(a,b){return ALiBS.sg.rd(0,2)-1});
		// ALiBS.c2.as(r);
		//
		rfc: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", // 64+1 chars = IETF PEM RFC 1421 = must be escaped (URI)
		"rfc+/": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", // 64 chars = must be escaped (URI)
		//
		uri: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.", // 64+1 chars = no need to escape (URI)
		"!uri": "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-.", // 64+1 chars reverse = no need to escape (URI)
		//
		s0: "_YzkbtHMnVXRwKBG6IvTU3WA14s0crepOCiSy5l7gPqu.oxjmhf8dN9FZLQ-a2EDJ", // 64+1 chars  = uri shuffle 0
		s1: "B1T8Lq56rwUF-Y2kb9vaH3.WAM4s0NnepOCidy_XjgoQxhZEJcRlGKzDmIfPtS7Vu", // 64+1 chars = uri shuffle 1
		sa: "kViv_zxhgN2nKCdeM8IjGFtXAZyJwO6fQRS59lm.HopuWscDUbaE0PT3-1Y47qBLr", // 64+1 chars = uri shuffle sa
		//
		o0: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,+", // 64+1 chars (other special chars)
		"!o0": "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,+" // 64+1 chars reverse (other special chars)
	},
	// *****************************************************************
	e4: function(s0,p) // encode = String TO String base64
	/** 
	Method <static> <number> ALiBS.c9.e4(s0,p)

		Description: aaaa

		Parameters:
			s0 = (required)(string) string text/i18n to convert

			p = (optional)(JSON)parameter {}
				n0 = (string)name
					"rfc" = DEFAULT
				n = OLD Compatibility
			
				u0 = (boolean) utf8 true/false 
					true = (DEFAULT)(textos = true) (utf8=true gera strings > 4/3)
					false = (binarios = false) 
				u = OLD Compatibility

		Observations:
	
		Return Value:
			<static> <string> string base64

		Example(s):
			var ok = ALiBS.c9.e4(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.c9.e4.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_n0 = z.n0 || z.n; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_u0 = z.u0 || z.u; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_n0)!="string") z.p_n0 = "rfc"; // 
			if(typeof(z.p_u0)!="boolean") z.p_u0 = true; // 
			//
			ALiBS.sy.la(5+0,"z.p_n0="+z.p_n0,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"z.p_u0="+z.p_u0,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.ec = -2000; // 
			z.v_k0 = ALiBS.c9._gka[z.p_n0]; // TODO(2010-09-16):passar string shuffle como parametro, nesse caso, nao buscar com _gk()
			ALiBS.sy.la(5+0,"z.v_k0="+z.v_k0,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			//
			z.ec = -2100; // 
			z.v_r0 = new Array();
			z.v_i0 = 0;
			//
			z.v_c1;
			z.v_c2; 
			z.v_c3;
			//
			z.v_e1, 
			z.v_e2, 
			z.v_e3, 
			z.v_e4;
			//
			// ************************
			//
			z.ec = -3000; // 
			if(z.p_u0) // use UTF-8
			{
				s0 = ALiBS.c6.en(s0); // utf8 = default
			}
			//
	//console.log("(1)s="+ALiBS.c2.sa(s));
			ALiBS.sy.la(5+0,"(ini)s0="+ALiBS.c2.sa(s0),{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -4000; // 
			while(z.v_i0<s0.length) 
			{
				z.ec = -4100; // 
				z.v_c1 = s0.charCodeAt(z.v_i0++);
				//
				z.v_c2 = s0.charCodeAt(z.v_i0++);
				//
				z.v_c3 = s0.charCodeAt(z.v_i0++);
				//
	//ALiBS.sy.la(0,"c1="+c1,{o:"ALiBS.c9.e4",le:1});
	//ALiBS.sy.la(0,"c2="+c2,{o:"ALiBS.c9.e4",le:1});
	//ALiBS.sy.la(0,"c3="+c3,{o:"ALiBS.c9.e4",le:1});
				//
				z.ec = -4200; // 
				z.v_e1 = z.v_c1 >> 2;
				z.v_e2 = ((z.v_c1 & 3) << 4) | (z.v_c2 >> 4);
				z.v_e3 = ((z.v_c2 & 15) << 2) | (z.v_c3 >> 6);
				z.v_e4 = z.v_c3 & 63;
				//
	/*
	console.log("\n\n");
	console.log("c9.e4");
	console.log("e1="+e1);
	console.log("e2="+e2);
	console.log("e3="+e3);
	console.log("e4="+e4);
	//
	console.log("\n\nc1="+c1);
	console.log("c2="+c2);
	console.log("c3="+c3);
	*/
				//
				z.ec = -4300; // 
				if(isNaN(z.v_c2)) 
				{
					z.ec = -4400; // 
					z.v_e3 = z.v_e4 = 64;
				} 
				else if(isNaN(z.v_c3)) 
				{
					z.ec = -4500; // 
					z.v_e4 = 64;
				}
				//
				z.v_r0[z.v_r0.length] = z.v_k0.charAt(z.v_e1) + z.v_k0.charAt(z.v_e2) + z.v_k0.charAt(z.v_e3) + z.v_k0.charAt(z.v_e4);
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0.join(""); // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.e4",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
		//
		return r; 
	},
	// *****************************************************************
	d4: function(s0,p) // decode = String(base64) TO String
	/** 
	Method <static> <number> ALiBS.c9.d4(s0,p)

		Description: aaaa

		Parameters:
			s0 = (required)(string) base64 coded string

			p = (optional)(JSON)parameter {}
				n0 = (string)name
				n = OLD Compatibility

				u0 = (boolean) utf8 true/false 
					true = (DEFAULT)(textos = true) (utf8=true gera strings > 4/3)
					false = (binarios = false) 
				u = OLD Compatibility

		Observations:
	
		Return Value:
			<static> <string> text

		Example(s):
			var ok = ALiBS.c9.d4(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -901;
			s0 = "" + s0; // js Server is misunderstanding "s0" as object
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.c9.d4.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_n0 = z.n0 || z.n; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_u0 = z.u0 || z.u; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_n0)!="string") z.p_n0 = "rfc"; // 
			if(typeof(z.p_u0)!="boolean") z.p_u0 = true; // 
			//
			ALiBS.sy.la(5+0,"z.p_n0="+z.p_n0,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"z.p_u0="+z.p_u0,{o:"ALiBS.c9.e4",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_r1 = new Array();
			z.v_r2 = new Array();
			z.v_i0 = 0;
			z.v_r0;
			//
			z.v_c1;
			z.v_c2; 
			z.v_c3;
			//
			z.v_e1; 
			z.v_e2; 
			z.v_e3; 
			z.v_e4;
			//
			// ************************
			//
			z.ec = -2000; // 
	 		z.v_k0 = ALiBS.c9._gka[z.p_n0];
			//
			ALiBS.sy.la(5+0,"z.v_k0="+z.v_k0,{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
			// 
			ALiBS.sy.la(5+0,"(ini)s0="+ALiBS.c2.sa(s0),{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
			//
			// *********************************
			//
			z.ec = -3000; // 
			while(z.v_i0<s0.length)
			{
//console.log("\n\ns["+i+"]="+s.charAt(i));
//console.log("s["+i+"]="+s.charCodeAt(i));
//
				z.ec = -3100; // 
	 			z.v_e1 = z.v_k0.indexOf(s0.charAt(z.v_i0++));
				z.v_e2 = z.v_k0.indexOf(s0.charAt(z.v_i0++));
				z.v_e3 = z.v_k0.indexOf(s0.charAt(z.v_i0++));
				z.v_e4 = z.v_k0.indexOf(s0.charAt(z.v_i0++));
	 			//
				z.ec = -3200; // 
				z.v_c1 = (z.v_e1 << 2) | (z.v_e2 >> 4);
				z.v_c2 = ((z.v_e2 & 15) << 4) | (z.v_e3 >> 2);
				z.v_c3 = ((z.v_e3 & 3) << 6) | z.v_e4;
				//
/*
console.log("\n\n");
console.log("c9.d4");
console.log("e1="+e1);
console.log("e2="+e2);
console.log("e3="+e3);
console.log("e4="+e4);
//
console.log("\n\nc1="+c1);
console.log("c2="+c2);
console.log("c3="+c3);
//
*/
				// ************************
				//
				z.ec = -3300; // 
				z.v_r1[z.v_r1.length] = String.fromCharCode(z.v_c1);
				//
/*if(c1==0)
{
	r[r.length] = String.fromCharCode(0); // adicionar marcador ,0,0,
	r[r.length] = String.fromCharCode(1); 
}*/
				//
				// ************************
				//
				z.ec = -3400; // 
				if(z.v_e3!=64) 
				{
					z.ec = -3500; // 
					z.v_r1[z.v_r1.length] = String.fromCharCode(z.v_c2);
					//
/*if(c2==0)
{
	r[r.length] = String.fromCharCode(0); // adicionar marcador ,0,0,
	r[r.length] = String.fromCharCode(2); 
}*/
				}
				else
				{
//console.log("passou e3 (if/else)");
//console.log("e3=="+e3);
//console.log("r.length="+r.length);
				}
				//
				// ************************
				//
				z.ec = -4000; // 
				if(z.v_e4!=64) // modified from original
				{
					z.ec = -4100; // 
					if(z.v_e4!=0) // modified from original
					{
						z.ec = -4200; // 
						z.v_r1[z.v_r1.length] = String.fromCharCode(z.v_c3);
						//
/*if(c3==0)
{
	r[r.length] = String.fromCharCode(0); // adicionar marcador ,0,0,
	r[r.length] = String.fromCharCode(3); 
}*/
					}
					else // e4==0 - modified from original
					{
						z.ec = -4300; // 
						if(z.v_c3==0)
						{
							z.ec = -4310; // 
							z.v_r1[z.v_r1.length] = String.fromCharCode(0); // novo, não da problema com byte 0
						}
						else if(z.v_c3==64)
						{
							z.ec = -4320; // 
							z.v_r1[z.v_r1.length] = String.fromCharCode(64); // novo, não da problema com byte 0
						}
						else if(z.v_c3==128)
						{
							z.ec = -4330; // 
							z.v_r1[z.v_r1.length] = String.fromCharCode(0); // novo, não da problema com byte 0
							//z.v_r1[z.v_r1.length] = String.fromCharCode(128); // novo, não da problema com byte 0
						}
						else if(z.v_c3==192)
						{
//console.log("passou e4==128)");
							//z.v_r1[z.v_r1.length] = String.fromCharCode(128); // novo, não da problema com byte 0
						}
						else if(z.v_c3==256)
						{
//console.log("passou e4==256)");
							//z.v_r1[z.v_r1.length] = String.fromCharCode(128); // novo, não da problema com byte 0
						}
						else
						{
//console.log("passou e4 (if/else/else)");
//console.log("e4="+e4);
//console.log("r.length="+r.length);
						}
						//
//r[r.length] = String.fromCharCode(0); // adicionar marcador ,0,0,
//r[r.length] = String.fromCharCode(4); 
//console.log("c3="+c3); 
					}
				}
				else // e4==64
				{
//console.log("passou e4 (if/else)");
//console.log("e4="+e4);
//console.log("r.length="+r.length);
				}
			}
			//
			// *********************************
			//
			z.ec = -5000; // 
			if(z.v_k0.length==64) // no "PAD" = "rfc+/" 
			{
				z.ec = -5100; // 
				z.v_nn = z.v_r1.length%8; 
				//
				z.ec = -5200; // 
				if(z.v_nn!=0) z.v_r1.length = z.v_r1.length-z.v_nn; // not divisible by 8 (there is rest) = remove extra (zeros)
				//
				// ************
				//
				z.ec = -5300; // 
				for(var i=0;i<z.v_r1.length;i++)
				{
					z.ec = -5400; // 
					if((i%8)==0) z.v_nn = i; // save i position
					//
					z.ec = -5500; // 
					if((i%8)==7 && z.v_r1[i].charCodeAt(0)==0) // last byte position = 0
					{
						z.v_r2[z.v_r2.length] = z.v_r1[i];
						//
						z.v_c1 = z.v_r2[z.v_nn+7];
						//
						z.v_r2[z.v_nn+7] = z.v_r2[z.v_nn+6];
						z.v_r2[z.v_nn+6] = z.v_r2[z.v_nn+5];
						z.v_r2[z.v_nn+5] = z.v_r2[z.v_nn+4];
						z.v_r2[z.v_nn+4] = z.v_r2[z.v_nn+3];
						z.v_r2[z.v_nn+3] = z.v_r2[z.v_nn+2];
						z.v_r2[z.v_nn+2] = z.v_r2[z.v_nn+1];
						z.v_r2[z.v_nn+1] = z.v_r2[z.v_nn];
						//
						z.v_r2[z.v_nn] = z.v_c1;
					}
					else
					{
						z.ec = -5600; // 
						z.v_r2[z.v_r2.length] = z.v_r1[i];
					}
					//ALiBS.sy.la(5,"z.v_r1["+i+"]="+z.v_r1[i].charCodeAt(0),{o:"ALiBS.c9",le:true});
				}
				//
				z.ec = -6000; // 
				z.v_r1 = z.v_r2;
				//ALiBS.sy.la(5,"z.v_r2="+ALiBS.c2.sa(z.v_r2),{o:"ALiBS.c9",le:true});
				//ALiBS.sy.la(5,"z.v_r1="+ALiBS.c2.sa(z.v_r1),{o:"ALiBS.c9",le:true});
			}
			//
			// ************************
			//
			z.ec = -7000; // 
			if(z.p_u0) // use UTF-8
			{
				z.ec = -7100; // 
				z.v_r0 = ALiBS.c6.de(z.v_r1.join("")); // utf8 = default
			}
			else
			{
				z.ec = -7200; // 
				z.v_r0 = z.v_r1.join("");
			}
			//
			//ALiBS.sy.la(5+0,"(fim)z.v_r0="+ALiBS.c2.sa(z.v_r0),{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
			ALiBS.sy.la(5+0,"(fim)z.v_r0="+z.v_r0,{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_r0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c9.d4",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c9.d4",le:ALiBS.gv.c9.le});
		//
		return r; 
	}
	// *****************************************************************
/*	i4: function(s) // isBase64 TODO(2010-09-14): colocar em stress test 
	// s = (required)(string)string
	// return = (boolean)true/false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c9.i4",le:ALiBS.gv.c9.le});
		//
		if(typeof(s)!="string" || s==null || s=="") 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.id:typeof(s)!=string||s==null||s==''"; 
			return false;
		}
		//
		var l = s.length;
		var c;
		//
		for(var i=0;i<l;i++)
		{
			c = s.charAt(i);
			if(c=="_" || c=="-") continue; // allowed
			if(ALiBS.sg.is(c)) return false; // not special = alpha OR num
			if((c<"0") || (c>"z")) return false;
			if((c>"Z") && (c<"a")) return false;
		}
		return true;
	}*/
	// *****************************************************************
};
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_c9_e4")>=0)
		{
			/*ALiBS_c9_e4 = function(cc,rq,rp,p) // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_c9_e4",le:ALiBS.gv.c9.le});
				//
				return ALiBS.c9.e4(s0,p);
			};*/
			//
			ALiBS_c9_e4 = ALiBS.c9.e4; // TODO(2015-07-29): Testar
		}
		//
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_c9_d4")>=0)
		{
			/*ALiBS_c9_d4 = function(cc,rq,rp,p) // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_c9_d4",le:ALiBS.gv.c9.le});
				//
				return ALiBS.c9.d4(s0,p);
			};*/
			//
			ALiBS_c9_d4 = ALiBS.c9.d4; // TODO(2015-07-29): Testar
		}
	}
}
//
// *************************************************************************
ALiBS.sy.la(0+0,"loaded module ALiBS.c9("+(++ALiBS.gv.c9.nn)+"x)",{o:"ALiBS.c9"});
ALiBS.sy.la(0+0,"ALiBS.gv.c9.rl="+ALiBS.gv.c9.rl,{o:"ALiBS.c9"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.c9.le="+ALiBS.gv.c9.le+ALiBS.gv.sy.se,{o:"ALiBS.c9"});

