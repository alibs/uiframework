/* *************************************************************************
// INFO:


// *************************************************************************
// TODO:


*/
//
// *************************************************************************
//
if(!ALiBS.gv.ui) ALiBS.gv.ui = {}; // global values
//
if(!ALiBS.gv.ui.le) ALiBS.gv.ui.le = 0; // local log status
//
ALiBS.gv.ui.nn = 0; // x times loaded
//
ALiBS.gv.ui.rl = "15.0731.1139";
//
// *************************************************************************
//
ALiBS.gv.ui.dth = "a"; // default theme
//
// *************************************************************************
//
ALiBS.ui = // User Interface 
{
	en: function(s0,p) // TODO(2015-09-04): REPLACE this function
	/** 
	Method <static> <string> ALiBS.ui.en(p)

		Description: Encode, zip, cryto, b4 on text for HTTP / HTTPS transmition

		Parameters:

			s0 = (required)(string) text 
			p = (optional)(JSON)parameter {} 

				DEPRECATED ky = (number) FIRST cryptography (or ONLY)
					0 = DEFAULT = do NOT crypto 
					1 = DES / 3DES
					2 = AES
					3 = Blowfish

				kyx = (number-array) each value represents a cryptography algorithm
					[] = DEFAULT = do NOT crypto
					other options = [1,2] , [2,3,4] , [1,4,2,3]

					values = (number) cryptography algorithm
						0 = DEFAULT = do NOT crypto 
						1 = DES / 3DES
						2 = AES
						3 = Blowfish

				ui = (number) number of bits for AES
					256 = DEFAULT
					128
					192

				pw = (String) password
					DES = 8 bytes = 64 bits
					3DES = 9+ bytes = 72+ bits
					AES = ?? bytes = ?? bits
					Blowfish = ?? byte = ?? bits

				zp = (number) zip
					0 = DEFAULT = do NOT zip = ALiBS.sg.zp
					1 = zip after (FIRST/OLD WAY)
					2 = zip before
					3 = zip before+after

				b64 = (number)
					0 = DEFAULT = do NOT use b64 = ALiBS.c9.en
					1 = use b64
					TODO(2015-04-02):
						64 = base64
						62 = base62
						113 = base113

				b64n = (String)name
					rfc = DEFAULT
					others: rfc,rfc+/,uri,!uri,s0

				b64u = (boolean) utf8
					true = DEFAULT = use UTF8
					false = do NOT use UTF8

		Observations:
	
		Return Value:
			<static> <string> encrypted text

		Example(s):
			var en = ALiBS.ui.en("abcdefgh",{b64:1,zp:1,ky:1,pw:"12345678",b64n:"uri",b64u:true});

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.ui.en.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_ky   = z.ky; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_kyx  = z.kyx; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_ui   = z.ui; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_pw   = z.pw; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_zp   = z.zp; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_b64  = z.b64; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_b64n = z.b64n; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_b64u = z.b64u; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(ALiBS.sy.ty(z.p_kyx)=="object-Array")
			{
				// ok to use
			}
			else // use z.p_ky0 = OLD Parameter Compatibility
			{
				if(typeof(z.p_ky0)!="number" || z.p_ky0<0 || z.p_ky0>1)
				{
					z.p_kyx = [];
				}
				else
				{
					z.p_kyx = new Array();
					//
					z.p_kyx[0] = z.p_ky0; // ONLY 1 value
				}
			}
			//
			if(typeof(z.p_ui)!="number" || (!(z.p_ui==128 || z.p_ui==192 || z.p_ui==256)) ) z.p_ui = 256;
			if(typeof(z.p_zp)!="number" || !(""+z.p_zp).match(/[0-3]/)) z.p_zp = 1;
			if(typeof(z.p_b64)!="number" || !(""+z.p_b64).match(/[0-1]/)) z.p_b64 = 0;
			if(typeof(z.p_b64n)!="string") z.p_b64n = "rfc";
			if(typeof(z.p_b64u)!="boolean") z.p_b64u = true;
			//
//console.log("(en)z.p_ky0="+z.p_ky0);
//console.log("(en)z.p_kyx="+z.p_kyx);
			//
			ALiBS.sy.la(5+0,"z.p_ky0="+z.p_ky0,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_kyx="+z.p_kyx,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_ui="+z.p_ui,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_zp="+z.p_zp,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_b64="+z.p_b64,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_b64n="+z.p_b64n,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_b64u="+z.p_b64u,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			//
			ALiBS.sy.la(5+0,"z.p_pw="+z.p_pw,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_r0 = s0; // return value
			//
			//
			// ************************
			//
			z.ec = -1200; // 
			if(z.p_zp==3 || z.p_zp==2) // 2 = zip before = unzip after
			{
				z.v_r0 = ALiBS.sg.zp(z.v_r0);
				//
				z.v_r0 = ALiBS.c2.as(z.v_r0);
				//
				z.v_r0 = ALiBS.c6.en(z.v_r0);
			}
			//
			// ************************
			//
			z.ec = -1300; // 
//console.log("typeof(z.p_kyx)="+typeof(z.p_kyx));
//console.log("z.p_kyx.length="+z.p_kyx.length);
//console.log("z.p_kyx="+z.p_kyx);
			//
			for(var i=0;i<z.p_kyx.length;i++)
			{
//console.log("(z.ec="+z.ec+")(z.p_kyx["+i+"]="+z.p_kyx[i]);
				if(z.p_kyx[i]==1) // cryptography DES / 3DES
				{
					z.ec = -1310; // 
					z.v_r0 = ALiBS.c3.en(z.p_pw,z.v_r0);
//console.log("(z.ec="+z.ec+")(i="+i+")z.v_r0.lenght="+z.v_r0.length);
				}
				else if(z.p_kyx[i]==2) // cryptography AES
				{
//if(i==0) console.log("z.p_ui="+z.p_ui);
					z.ec = -1320; // 
					z.v_r0 = ALiBS.c0.en(z.p_pw,z.v_r0,{ui:z.p_ui}); // AES decrypt
//console.log("(z.ec="+z.ec+")(i="+i+")z.v_r0.lenght="+z.v_r0.length);
				}
				else if(z.p_kyx[i]==3) // cryptography Blowfish
				{
					z.ec = -1330; // 
					z.v_r0 = ALiBS.c5.en(z.p_pw,z.v_r0); // Blowfish
					//
//console.log("(z.ec="+z.ec+")(i="+i+")z.v_r0.lenght="+z.v_r0.length);
				}
			}
			//
			// ************************
			//
			z.ec = -1400; // 
			if(z.p_zp==3 || z.p_zp==1) // 1 = zip after = unzip before
			{
				z.v_r0 = ALiBS.sg.zp(z.v_r0);
				//
				z.v_r0 = ALiBS.c2.as(z.v_r0);
				//
				z.v_r0 = ALiBS.c6.en(z.v_r0);
			}
			//
			// ************************
			//
			z.ec = -1500; // 
			if(z.p_b64) // 1
			{
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
//console.log("(z.ec="+z.ec+")z.v_r0="+ALiBS.c2.sa(z.v_r0));
				z.v_r0 = ALiBS.c9.e4(z.v_r0,{n:z.p_b64n,u:z.p_b64u});
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0; 
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.ui.en",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.ui.en",le:ALiBS.gv.ui.le});
		//
		return r; 
	},
	// *****************************************************************
	de: function(d0,p) // TODO(2015-09-04): REPLACE this function
	/** 
	Method <static> <string> ALiBS.ui.de(p)

		Description: Decode, unzip, decryto, deb4 on text for HTTP / HTTPS transmition

		Parameters:

			d0 = (required)(string) Encrypted Text 
			p = (optional)(JSON)parameter {} 

				DEPRECATED ky = (number) FIRST cryptography (or ONLY)
					0 = DEFAULT = do NOT crypto 
					1 = DES / 3DES
					2 = AES
					3 = Blowfish

				kyx = (number-array) each value represents a cryptography algorithm
					[] = DEFAULT = do NOT crypto
					other options = [1,2] , [2,3,4] , [1,4,2,3]

					values = (number) cryptography algorithm
						0 = DEFAULT = do NOT crypto 
						1 = DES / 3DES
						2 = AES
						3 = Blowfish

				ui = (number) number of bits for AES
					256 = DEFAULT
					128
					192

				pw = (String) password
					DES = 8 bytes = 64 bits
					3DES = 9+ bytes = 72+ bits
					AES = ?? bytes = ?? bits
					Blowfish = ?? byte = ?? bits

				zp = (number) zip
					0 = DEFAULT = do NOT zip = ALiBS.sg.zp
					1 = zip after (FIRST/OLD WAY)
					2 = zip before
					3 = zip before+after

				b64 = (number)
					0 = DEFAULT = do NOT use b64 = ALiBS.c9.de
					1 = use b64
					TODO(2015-04-02):
						64 = base64
						62 = base62
						113 = base113

				b64n = (String)name
					rfc = DEFAULT
					others: rfc,rfc+/,uri,!uri,s0

				b64u = (boolean) utf8
					true = DEFAULT = use UTF8do NOT disclose
					false = do NOT use UTF8

		Observations:
	
		Return Value:
			<static> <string> decrypted text

		Example(s):
			var en = ALiBS.ui.de("xyzxyzxyz",{b64:1,zp:1,ky:1,pw:"12345678",b64n:"uri",b64u:true});

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(d0)!="string") throw "ALiBS.ui.de.Exception: Error: Invalid Parameter Type: (string) d0 passed as "+typeof(d0); // 
			//
			ALiBS.sy.la(5+0,"d0="+d0,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_ky0  = z.ky0; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_kyx  = z.kyx; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_ui   = z.ui; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_pw   = z.pw; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_zp   = z.zp; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_b64  = z.b64; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_b64n = z.b64n; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_b64u = z.b64u; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(ALiBS.sy.ty(z.p_kyx)=="object-Array")
			{
				// ok to use
			}
			else // use z.p_ky0 = COMPATIBILITY
			{
				if(typeof(z.p_ky0)!="number" || z.p_ky0<0 || z.p_ky0>1)
				{
					z.p_kyx = [];
				}
				else
				{
					z.p_kyx = new Array();
					//
					z.p_kyx[0] = z.p_ky0; // ONLY 1 value
				}
			}
			//
//console.log("(de)z.p_ky0="+z.p_ky0);
//console.log("(de)z.p_kyx="+z.p_kyx);
			//
			if(typeof(z.p_ui)!="number" || (!(z.p_ui==128 || z.p_ui==192 || z.p_ui==256)) ) z.p_ui = 256;
			if(typeof(z.p_zp)!="number" || !(""+z.p_zp).match(/[0-3]/)) z.p_zp = 1;
			if(typeof(z.p_b64)!="number" || !(""+z.p_b64).match(/[0-1]/)) z.p_b64 = 0;
			if(typeof(z.p_b64n)!="string") z.p_b64n = "rfc";
			if(typeof(z.p_b64u)!="boolean") z.p_b64u = true;
			//
			ALiBS.sy.la(5+0,"z.p_ky0="+z.p_ky0,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_kyx="+z.p_kyx,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_ui="+z.p_ui,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_zp="+z.p_zp,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_b64="+z.p_b64,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_b64n="+z.p_b64n,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			ALiBS.sy.la(5+0,"z.p_b64u="+z.p_b64u,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_r0 = d0; // return value
			//
			// ************************
			//
			z.ec = -1200; // 
			if(z.p_b64) // 1
			{
//console.log("(z.ec="+z.ec+")z.v_r0="+z.v_r0);
				z.v_r0 = ALiBS.c9.d4(z.v_r0,{n:z.p_b64n,u:z.p_b64u}); // base64
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
//console.log("(z.ec="+z.ec+")z.v_r0="+ALiBS.c2.sa(z.v_r0));
			}
			//
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
			// ************************
			//
			z.ec = -1300; // 
			if(z.p_zp==3 || z.p_zp==1) // 1 = zip after = unzip before
			{
				z.v_r0 = ALiBS.c6.de(z.v_r0);
				//
				z.v_r0 = ALiBS.c2.sa(z.v_r0); 
				//
				z.v_r0 = ALiBS.sg.uz(z.v_r0);
			}
			//
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
			// ************************
			//
			z.ec = -1400; // 
//console.log("(z.ec="+z.ec+")z.p_kyx="+z.p_kyx);
//console.log("typeof(z.p_kyx)="+typeof(z.p_kyx));
//console.log("z.p_kyx.length="+z.p_kyx.length);
//console.log("z.p_kyx="+z.p_kyx);
			//
			for(var i=z.p_kyx.length-1;i>=0;i--) // Reverse loop
			{
//console.log("(z.ec="+z.ec+")(z.p_kyx["+i+"]="+z.p_kyx[i]);
				if(z.p_kyx[i]==1) // cryptography DES / 3DES
				{
					z.ec = -1410; // 
					z.v_r0 = ALiBS.c3.de(z.p_pw,z.v_r0);
//console.log("(z.ec="+z.ec+")(i="+i+")z.v_r0.lenght="+z.v_r0.length);
				}
				else if(z.p_kyx[i]==2) // cryptography AES
				{
//if(0) console.log("z.p_ui="+z.p_ui);
					z.ec = -1420; // 
					z.v_r0 = ALiBS.c0.de(z.p_pw,z.v_r0,{ui:z.p_ui}); // AES decrypt
//console.log("(z.ec="+z.ec+")(i="+i+")z.v_r0.lenght="+z.v_r0.length);
				}
				else if(z.p_kyx[i]==3) // cryptography Blowfish
				{
					z.ec = -1430; // 
					z.v_r0 = ALiBS.c5.de(z.p_pw,z.v_r0); // Blowfish
					//
//console.log("(z.ec="+z.ec+")(i="+i+")z.v_r0.lenght="+z.v_r0.length);
				}
			}
			//
			// ************************
			//
			z.ec = -1500; // 
			if(z.p_zp==3 || z.p_zp==2) // 2 = zip before = unzip after
			{
				z.ec = -1510; // 
				z.v_r0 = ALiBS.c6.de(z.v_r0);
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);

				//z.v_r0 = ALiBS.c9.d4(z.v_r0,{n:z.p_b64n,u:z.p_b64u}); // base64
				//
				z.ec = -1520; // 
				z.v_r0 = ALiBS.c2.sa(z.v_r0); 
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
				//
				z.ec = -1530; // 
				z.v_r0 = ALiBS.sg.uz(z.v_r0);
			}
//console.log("(z.ec="+z.ec+")z.v_r0.lenght="+z.v_r0.length);
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0; 
//console.log("(r)r.lenght="+r.length);
//console.log("\n\n");
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.ui.de",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.ui.de",le:ALiBS.gv.ui.le});
		//
//console.log("(end)r.lenght="+r.length);

		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.ui("+(++ALiBS.gv.ui.nn)+"x)",{o:"ALiBS.ui"});
ALiBS.sy.la(0+0,"ALiBS.gv.ui.rl="+ALiBS.gv.ui.rl,{o:"ALiBS.ui"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.ui.le="+ALiBS.gv.ui.le+ALiBS.gv.sy.se,{o:"ALiBS.ui"});
 
