/* *************************************************************************
// INFO:

Data storage and transmition (session/data persistance)
----------------------------

Server: Data is stored in session AND/OR databases AND/OR javascript variables
Client: Data is stored in javascript variables AND/OR cookies AND/OR localdatabases

data sent from client to server: sent in parameter[x]
data sent from server to client: sent in variable x, using ALiBS.ax / ALiBS.ag and call back function

****

- When opening a new browser tab, it should be a child of calling window and use parent.ALiBS so it can use session/javascript data

- Caso use javascript variables para manter os dados da sessao no cliente,
	todas as novas tabs abertas no browser devem ser filhas da tab original,
	para que as novas tabs tenham acesso as variaveis javascript da tab original.

// *************************************************************************
// TODO():

- eventualmente trocar todos os random base64???? ou adicionar novos valores?
- trocar pode gerar problemas para hashs que estao armazenados e que precisem ser decodificado com base64, portanto, neste caso, este base64 especifico deveria ser fixo.


http://msdn.microsoft.com/en-us/library/ff648647.aspx

Use the Correct Algorithm and Correct Key Size

It is important to make sure you choose the right algorithm for the right job and to make sure you use a key size that provides a sufficient degree of security. 
Larger key sizes generally increase security. 
The following list summarizes the major algorithms together with the key sizes that each uses:

Data Encryption Standard (DES) 64-bit key (8 bytes)
TripleDES 128-bit key or 192-bit key (16 or 24 bytes)
Rijndael 128256 bit keys (1632 bytes)
RSA 38416,384 bit keys (482,048 bytes)

For large data encryption, use the TripleDES symmetric encryption algorithm. 
For slower and stronger encryption of large data, use Rijndael. 
To encrypt data that is to be stored for short periods of time, you can consider using a faster but weaker algorithm such as DES. 
For digital signatures, use Rivest, Shamir, and Adleman (RSA) or Digital Signature Algorithm (DSA). 
For hashing, use the Secure Hash Algorithm (SHA)1.0. 
For keyed hashes, use the Hash-based Message Authentication Code (HMAC) SHA1.0.

******************************

- A cada request o cliente deve enviar SOMENTE o parameter[x]
- O algoritmo abaixo deve ser validado em ALiBS.wu.d0

	x = base64("s0=xpto;s1=xpto;r0=xpto;r1=xpto;r2=xpto;")

		- do not send session data because server already know it
		- r0...r? (request data)(usar criptografia?)
		- v0 = base64( superHash(s0...s? + r0...r? + k0...k?) ) (HMAC of session/request/control data)
		- d0...d? (user/application data)(usar criptografia?)
		- v1 = base64( superHash(d0...d? + k0...k?) ) (HMAC of user/application data)

*/
//
// *************************************************************************
//
if(!ALiBS.gv.wu) ALiBS.gv.wu = {}; // global values
//
if(!ALiBS.gv.wu.le) ALiBS.gv.wu.le = 0; // local log status
//
ALiBS.gv.wu.nn = 0; // x times loaded
//
ALiBS.gv.wu.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wu.rl = "15.0622.1415";
//
/*

ALiBS.gv.wu.k0 = ""; // Session Key (senha da sessao)(usada para criptografia simetrica)(obtida com diffie-hellman)
ALiBS.gv.wu.k1 = ""; // User Password Super Hash (a new and different hash is create for each session)(change n times superHash(text,n) runs)
//
ALiBS.gv.wu.s0 = ""; // Session ID
ALiBS.gv.wu.s1 = ""; // Session Timestamp = Login timestamp
ALiBS.gv.wu.s3 = ""; // Session User Agent On Login (read from client after successfull login and should last during entire session) to send to server at each request
//
ALiBS.gv.wu.r0 = ""; // Request Challenge/Token
ALiBS.gv.wu.r1 = ""; // Request Timestamp (o server nao aceita requests com timestamp passado, somente maior que o valor armazenado no server)
			// (para garantir que o hash gerado a cada request seja diferente)
ALiBS.gv.wu.r2 = ""; // Request Random (length=??bytes)(para garantir que o hash gerado a cada request seja diferente)
ALiBS.gv.wu.r3 = ""; // Request Random [1000-3000] (numero de vezes que o superHash vai ser executado internamente)
ALiBS.gv.wu.r4 = ""; // Request Random [0-9999] (variação de base64)(no inicio, usar somente [0-99])(outra opcao é o server mandar a string de base64 pra ser usada)
ALiBS.gv.wu.r5 = ""; // Request base64 string [09azAZ_-] shuffled to be used in request/session - vou usar isso???? 
//
ALiBS.gv.wu.d0 = ""; // Data 0

*/
// *************************************************************************
//
ALiBS.wu = // User (Client) Authentication AND Request Validation
{
	// *****************************************************************
	lp: function(a,p) // (Runs on Server) Client Login Page
	// a = (required)(String)????
	// p = (optional)(JSON)parameters {}
	//
	// return HTML Login Page
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.lp",le:ALiBS.gv.wu.le});
		//
		var a = new Array();
		//
		a[a.length] = '<form name="form1">';
		//
		a[a.length] = '<div id="divErro_x_EID">';
		a[a.length] = 		'<span></span>';
		a[a.length] = 		'</div>';
		//
		a[a.length] = '<div id="divUser_x_EID">';
		a[a.length] = 		'<br/><span id="userTxt_x_EID">Usuário:</span>';
		a[a.length] = 		'<br/><input type=text id="user_x_EID"></input>';
		a[a.length] = 		'<br/><span id="passTxt_x_EID">Senha:</span>';
		a[a.length] = 		'<br/><input type=password id="pass_x_EID"></input>';
		a[a.length] = 		'<br/><input id="but_x_EID" type=button value="Login" onclick="ALiBS.wu.la()">';
		a[a.length] = '</div>';
		//
		a[a.length] = '</form>';
		//
		return a.join("");
	},
	// *****************************************************************
	la: function() // (Runs on Client) Login Action TODO
	{
	//console.log("passou app login_x()");

		//alert("Executando o Login Padrão");
/*		//
		var user_x_EID = ALiBS.el.ge("user_x_EID");
		var pass_x_EID = ALiBS.el.ge("pass_x_EID");
		//
	console.log("login_x()-user_x_EID="+user_x_EID.value);
	console.log("login_x()-pass_x_EID="+pass_x_EID.value);
		//
	console.log("login_x()-ALiBS.gv.tk.user0="+ALiBS.gv.tk.user0);
		//
		if(ALiBS.sg.co(user_x_EID.value,["A7892","I500334","B978741","u0000001","u0000002","u0000003","u0000004","admin","volneidds"])>=0)
		{
			if(ALiBS.sg.co(pass_x_EID.value,["N0v3ll","novell","zaq1xsw2","10203040"])>=0)
			{
				ALiBS.el.ge("divErro_x_EID").innerHTML = "<span>Usuário Logado</span>";
				//
				ALiBS.st.st("divErro_x_EID","show");
				ALiBS.st.st("divUser_x_EID","hide");
			}
			else
			{
				ALiBS.el.ge("divErro_x_EID").innerHTML = "<span>Senha Inválida (HTML)</span>";
			}
		}
		else
		{
			ALiBS.el.ge("divErro_x_EID").innerHTML = "<span>Usuário Inválido (HTML)</span>";
		}
*/
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	xxxx_ao: function(ho,us,pw,p) // Generic Authentication Object
	/** 
	Method <instance> <ALiBS.wu.ao> ALiBS.wu.ao(ho,us,pw,p)

		Description: Generic Authentication Object

		Parameters:

			ho = (required)(string) Host (url)

			us = (required)(string) UserID (principal)

			pw = (required)(string) Password (secret)

			p = (optional)(JSON)parameters {}

				uDN = (string) userDN (principal)
				uID = (string) userID (principal)

				cp = (number) Use Connection Pool 
					0 = No connection pool
					1 = DEFAULT = Use connection pool

				to = (number) Connection Timeout (milliseconds)
					10*1000 = DEFAULT = 10.000 milliseconds = 10 seconds

				mxrs = (number) Max Results
					10*1000 = DEFAULT

				ssl = (required)(number) SSL
					0 = DEFAULT = NO SSL
					1 = Use the SSL Socket Property = ( Context.SECURITY_PROTOCOL=ssl ) + ( URL=ldap://localhost:636/o=JNDITutorial )
					2 = Use the LDAPS URL = (URL=ldaps://localhost:636/o=JNDITutorial)

		Observations:
	
		Return Value:
			Authentication Object with 
				(cc) connection data (javax.naming.directory.DirContext)
				(cc) connection data (other)

		Example:
			var x = ALiBS.wu.ao(host,user,pass);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = {}; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 

			//TODO:
			// r.d0 = {}; // Data Object 0 (could create others manually)
			// r.v0 = {}; // Custom Variables 0 (could create others manually)

			r.sdc = {}; // authentication object / session data connection
			r.va = {}; // custom variables/values
			//
			z.ec = -950; // 
			if(typeof(ho)!="string") throw "alibs.wu.ao.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			if(typeof(us)!="string") throw "alibs.wu.ao.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			if(typeof(pw)!="string") throw "alibs.wu.ao.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(8+0,"z.pw="+z.pw,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			if(typeof(z.to)!="number") z.to = 10*1000; // 
			if(typeof(z.mxrs)!="number") z.mxrs = 10*1000; // 
			if(typeof(z.uID)!="string") z.uID = ""; // 
			if(typeof(z.uDN)!="string") z.uDN = ""; // 
			//
			ALiBS.sy.la(5+0,"z.cp="+z.cp,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(5+0,"z.ssl="+z.ssl,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(5+0,"z.to="+z.to,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(5+0,"z.mxrs="+z.mxrs,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(5+0,"z.uID="+z.uID,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			ALiBS.sy.la(5+0,"z.uDN="+z.uDN,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
			//
			//
			// local variables [BEST-PRACTICE]
			//z.a = new Array();
			//
			r.sdc.ho = ho;
			r.sdc.us = us;
			r.sdc.pw = pw;
			//
			r.sdc.cp = z.cp;
			r.sdc.ssl = z.ssl;
			r.sdc.to = z.to;
			r.sdc.mxrs = z.mxrs;
			//
			r.sdc.uID = z.uID;
			r.sdc.uDN = z.uDN;
			//
			r.sdc.cc0 = r.sdc.cc = null; // javax.naming.directory.DirContext
			r.sdc.cc1 = null; // connection information from application (iManager/Tomcat/javax/etc)
			r.sdc.cc2 = null; // connection information from application (iManager/Tomcat/javax/etc)
			r.sdc.cc3 = null; // connection information from application (iManager/Tomcat/javax/etc)
			r.sdc.cc4 = null; // connection information from application (iManager/Tomcat/javax/etc)
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wu.ao",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
		//
		return r; // Authentication Object
	},
	// *****************************************************************
	xxxx_sao: function(ao,p) // Save/Set Authentication Object to session
	/** 
	Method <static> <number> ALiBS.wu.sao(ao,p)

		Description: Save/Set Authentication Object to session

		Parameters:

			ao = (required)(ALiBS.wu.ao) Authentication Object

			p = (optional)(JSON)parameters {}


		Observations:
	
		Return Value:
			Status:
				1 = Success
				-1 = Error

		Example:
			var x = ALiBS.wu.sao(ao);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.sao",le:ALiBS.gv.wu.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = -1; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			//
			z.ec = -950; // 
			//if(typeof(ho)!="string") throw "ALiBS.wu.sao.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wu.sao",le:ALiBS.gv.wu.le});
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			if(typeof(ao.sdc.uID)!="string") ao.sdc.uID = ""; // 
			//
			session.setAttribute("ALiBS.wu.ao.instance",ao); // user (HTTPServer) session
			//
			r = 1;
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wu.sao",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wu.sao",le:ALiBS.gv.wu.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wu.sao",le:ALiBS.gv.wu.le});
		//
		return r; // number
	},
	// *****************************************************************
	xxxx_gao: function(p) // Save/Set Authentication Object to session
	/** 
	Method <instance> <ALiBS.wu.ao> ALiBS.wu.gao(p)

		Description: Save/Set Authentication Object to session

		Parameters:

			p = (optional)(JSON)parameters {}


		Observations:
	
		Return Value:
			Status:
				1 = Success
				-1 = Error

		Example:
			var x = ALiBS.wu.gao(ao);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.gao",le:ALiBS.gv.wu.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = null; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			//
			z.ec = -950; // 
			//if(typeof(ho)!="string") throw "ALiBS.wu.gao.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			//
			//ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wu.gao",le:ALiBS.gv.wu.le});
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//
			//ALiBS.sy.la(5+0,"z.cp="+z.cp,{o:"ALiBS.wu.gao",le:ALiBS.gv.wu.le});
			//
			// local variables [BEST-PRACTICE]
			//if(typeof(ao.sdc.uID)!="string") ao.sdc.uID = ""; // 
			//
			r = session.getAttribute("ALiBS.wu.ao.instance",ao); // user (HTTPServer) session
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wu.gao",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wu.gao",le:ALiBS.gv.wu.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wu.gao",le:ALiBS.gv.wu.le});
		//
		return r; // Authentication Object
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	r0: function(r,p) // request structure validation 
	// r = (required)(JSON)request
	// p = (optional)(JSON)parameters {m:"GET",o:"http",d:"www.meusite.com",p:"port",u:"/uri2"}
	//	m = (String)request method (P=POST(default), G=GET)
	//	o = (String)protocol (http/https) 
	//	d = (String)domain name
	//	p = (String)port 
	//	u = (String)uri/servlet
	//
	// return = (number) error code
	//	r = 1 = OK - Valid Request
	//	r = -100 = Invalid request[method] = should be POST
	//	r = -101 = Invalid request[method] = should be GET
	//	r = -102 = Invalid request[servletPath]
	//	r = -103 = Invalid request[requestURI]
	//	r = -104 = Invalid request[serverPort]
	//	r = -105 = Invalid request[serverName] == domain name
	//	r = -106 = Invalid request[headers][Host]
	//	r = -107 = Invalid request[requestURL]
	//	r = -108 = Invalid request[headers][Origin]
	//
	//	r = -199 = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.r0",le:ALiBS.gv.wu.le});
		//
		var m = "P"; // POST = default 
		var o = "http"; // protocol
		var d = ""; // domain name
		var p = ""; // port
		var u = ""; // uri
		//
		if(p)
		{
			if(typeof(p.m)=="string" && p.m=="G") m = "G";
			if(typeof(p.o)=="string") o = p.o;
			if(typeof(p.d)=="string") d = p.d;
			if(typeof(p.p)=="string") p = p.p;
			if(typeof(p.u)=="string") u = p.u;
		}
		//
		//
		if(m=="P") // ONLY accept POST
		{
			if(r["method"]!="POST")
			{
				// save to Authentication DBase
				return -100; // should be POST
			}
			else
			{
				// TODO(2011-04-13): queryString=null (validar se é null)

			}
		}
		else if(m=="G") // ONLY accept GET
		{
			if(r["method"]!="GET")
			{
				// save to Authentication DBase
				return -101; // should be GET
			}
			else
			{
				//
			}
		}
		//
		//
		if(u!=r["servletPath"]) // /uri2
		{
			// save to Authentication DBase
			return -102; // 
		}
		//
		if(u!=r["requestURI"]) // /uri2
		{
			// save to Authentication DBase
			return -103; // 
		}
		//
		if(p!=r["serverPort"]) // 7777
		{
			// save to Authentication DBase
			return -104; // 
		}
		//
		if(d!=r["serverName"]) // 127.0.0.1 OR www.meusite.com
		{
			// save to Authentication DBase
			return -105; // 
		}
		//
		if(r["headers"]["Host"]!=(d+":"+p)) // 127.0.0.1:7777 OR www.meusite.com:7777
		{
			// save to Authentication DBase
			return -106; // 
		}
		//
		if(r["requestURL"]!=(o+":/"+"/"+d+":"+p+u)) // http://127.0.0.1:7777/uri2 OR http://www.meusite.com:7777/uri2
		{
			// save to Authentication DBase
			return -107; // 
		}
		//
		if(r["headers"]["Origin"])
		{
			if(r["headers"]["Origin"]!=(o+":/"+"/"+d+":"+p+u)) // http://127.0.0.1:7777/uri2 OR http://www.meusite.com:7777/uri2
			{
				// save to Authentication DBase
				return -108; // 
			}
		}
		//

		// TODO(2011-04-13): vou aceitar somente 1.0 e 1.1 ?
		//protocol=HTTP/1.1

		// TODO(2011-04-13): remoteAddr=remoteHost (validar se é igual)(IP do cliente)

		// TODO(2011-04-13): parametro unico enviado em ALiBS.ax
		// request[parameters][x]=ABCD_abcs-1234

		// TODO(2011-04-13): eu envio o contentType de ALiBS.ax e deve ser igual
		// contentType=application/x-www-form-urlencoded

		// TODO(2011-04-13): eu envio o contentLength de ALiBS.ax 
		//contentLength (validar somente se existe)

		// TODO(2011-04-13): pode mudar?
		// request[headers][Connection]=keep-alive

		// TODO(2011-04-13): validar aqui?
		//pathInfo=null
		//contextPath=
		//pathTranslated=null
		//characterEncoding=null
		//remoteUser=null
		//authType=null
		//body=
		//
		return 1; // Validation OK
	}, 
	// *****************************************************************
	s0: function(r,p) // session validation
	// r = (required)(JSON)request
	// p = (optional)(JSON)parameters {}
	//
	// return = (number) error code
	//	r = 2 = OK - Valid Session
	//	r = -200 =  
	//	r = -299 = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.s0",le:ALiBS.gv.wu.le});
		//

		// TODO(2011-04-13): salvar valores em tabela sqlite de autenticacao

		// TODO(2011-04-13): Nao podem ter os valores alterados durante a sessao
		// remoteAddr (IP do cliente)
		// remoteHost (IP do cliente)

		// TODO(2011-04-13): validar dados do user-agent
		// request[headers][User-Agent]=Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.151 Safari/534.16
		//locale=en_US
		//request[locales][en]=1
		//request[locales][en_US]=0
		//request[headers][Accept-Language]=en-US,en;q=0.8


		// TODO(2011-04-13): devem ser iguais
		// requestedSessionId=18oqnnkgyc7p
		// request[headers][Cookie]=JSESSIONID=18oqnnkgyc7p

		// TODO(2011-04-13): deve existir somente o cookie JSESSIONID ????
		// request[headers][Cookie]=JSESSIONID=18oqnnkgyc7p

		// TODO(2011-04-13): validar????
		//request[headers][Accept-Charset]=ISO-8859-1,utf-8;q=0.7,*;q=0.3
		//request[headers][Content-Type]=application/x-www-form-urlencoded
		//request[headers][Accept-Encoding]=gzip,deflate,sdch
		//request[headers][Accept]=*/*

	},
	// *****************************************************************
	sid: function(r,p) // session request - servlet HttpSession
	/** 
	Method <static> <string> ALiBS.wu.sid(p)

		Description: aaaa

		Parameters:
			r = (required)(JSON)request

			p = (optional)(JSON)parameter {}

				c = (boolean)create session if it not exists (default = false)

		Observations:
	
		Return Value:
			<static> <string> string

		Example(s):
			var ssid = ALiBS.wu.sid();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - NOT to be disclosed
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.sid",le:ALiBS.gv.wu.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// Aumentar z.ec de 100/100
			//
			// ************************
			//
			z.ec = -950; // 
			if(typeof(z.ho)!="string") throw "ALiBS.wu.sid.Exception: Error: Invalid Parameter Type (ho="+typeof(ho)+")"; // 
			if(typeof(z.us)!="string") throw "ALiBS.wu.sid.Exception: Error: Invalid Parameter Type (us="+typeof(us)+")"; // 
			if(typeof(z.pw)!="string") throw "ALiBS.wu.sid.Exception: Error: Invalid Parameter Type (pw="+typeof(pw)+")"; // 
			//
			ALiBS.sy.la(5+0,"z.ho="+z.ho,{o:"ALiBS.wu.sid",le:ALiBS.gv.wu.le});
			//
			// ************************
			//
			z.ec = -1000; // 
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.cs)!="string") z.cs = "hg_css0a"; // DEFAULT - to remove css use cs:""
			//if(typeof(z.cp)!="number" || !(""+z.cp).match(/[0-1]/)) z.cp = 0; // 
			//if(typeof(z.ssl)!="number" || !(""+z.ssl).match(/[0-2]/)) z.ssl = 0; // 
			//
			if(typeof(z.fd)!="string" || z.fd=="*")
			{
				z.fd = ALiBS.to.fdn(t0); // get ALL defined field names = DEFAULT = "*"
			}
			//
			if(typeof(z.fc)!="string")
			{
				z.ec = -1002; // 
				//t0.tb.fc = "(f00:=*)"; // DEFAULT = *
				z.ALL = 1; // faster
			}
			else
			{
				z.ec = -1003; // 
				t0.tb.fc = z.fc; 
			}
			//
			//ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.wu.sid",le:ALiBS.gv.wu.le});
			//
			// ************************
			//
			z.ec = -1050; // 
			// table variables
			//if(typeof(t.ff.hg.fc)=="undefined") t.ff.hg.fc = ALiBS.hg._hgfc; // pointer = DEFAULT - "field filter" header click
			//
			// ************************
			//
			z.ec = -1100; // 
			// local variables [BEST-PRACTICE]
			//z.a0 = new Array();
			//
			// ************************
			//
			z.ec = -9000; // -9902, -9903, etc
			if(i>1000) throw "ALiBS.wu.sid.Exception: Error: Infinite-Loop (i="+i+")"; // Debug
			//
			// onde se aplicar, depois de usar a variável
			//
			// ************************
			//
			z.ec = -9800; // 
			z.t0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			z.ret = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			z.Tk = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			// ************************
			//
			z.ec = -9900; // 
			//throw "ALiBS.wu.sid.Exception: Error: Force Error (msg="+msg+")"; // FORCE ERROR
			//
			//r = z.a.join(""); // [BEST-PRACTICE]
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1="";break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wu.sid",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wu.sid",le:ALiBS.gv.wu.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.wu.sid",le:ALiBS.gv.wu.le});
		//
		return r; 




		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.sr",le:ALiBS.gv.wu.le});
		//
		var c = false; // default
		//
		if(p)
		{
			if(typeof(p.c)=="boolean" && c==true) c = true;
		}
		//
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.sr",le:ALiBS.gv.wu.le});
		//
		var s = r.getSession(c);
		return s.getId();
	},
	// *****************************************************************
	u0: function(r,p) // user validation
	// r = (required)(JSON)request
	// p = (optional)(JSON)parameters {}
	//
	// return = (number) error code
	//	r = 3 = OK - Valid User
	//	r = -300 =  
	//	r = -399 = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.u0",le:ALiBS.gv.wu.le});
		//
		// TODO(2011-04-13): validar acessos, bloqueios, etc
	},
	// *****************************************************************
	d0: function(r,p) // data validation
	// r = (required)(JSON)request
	// p = (optional)(JSON)parameters {}
	//
	// return = (number) error code
	//	r = 4 = OK - Valid Data
	//	r = -400 =  
	//	r = -499 = 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.d0",le:ALiBS.gv.wu.le});
		//
		// TODO(2011-04-13): eu envio o contentLength de ALiBS.ax e deve ser igual
		//contentLength=47 (validar com o tamanho do parameter x.length)

		// TODO(2011-04-13): nao sei se vai ser util, mas tem que ter a pagina que chamou ou a ultima pagina?
		// request[headers][Referer]=http://127.0.0.1:7777/html/t2.html
		// request[headers][Referer]=http://www.meusite.com:7777/html/t2.html

		// TODO(2011-04-13): validar conteudo de x (parameter[x])
	},
	// *****************************************************************
	wsc: function(cc,rq,rp,p) // ws cmd 
	// cc = (required)(connection) Connection Control / Session (javax.naming.directory.DirContext)
	// rq = (required)(HttpServletRequest) page request
	// rp = (required)(HttpServletResponse) page response
	//
	//requestHttp TODO(2014-06-03): needed?
	//responseHttp TODO(2014-06-03): needed?
	//
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	// return = (string) status control
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wu.wsc",le:ALiBS.gv.wu.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = z.cmd + " Registered but NOT found in ALiBS.wu.wsc()"; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			for(var i in z)
			{
				ALiBS.sy.la(7+0,"z["+i+"]="+z[i],{o:"ALiBS.wu.wsc",le:ALiBS.gv.wu.le});
			}
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1001; 
			z.cmd0 = z.cmd.replace("ALiBS.wu.",""); // remains p8, t8, o8, etc
			//
			z.ec = -1050; 
			if(z.cmd0 && ALiBS.wu[z.cmd0])
			{
				z.ec = -1070; 
				r = ALiBS.wu[z.cmd0](cc,rq,rp,p);
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("wsc.if.lp")>=0) // 
			{
				ALiBS.sy.la(5+0,"z.cmd="+z.cmd,{o:"ALiBS.wu.wsc",le:ALiBS.gv.wu.le});
				//
				z.ec = -2000; // 
				z.ssid = ALiBS.wu.sr(rq);
				ALiBS.sy.la(5+0,"z.ssid="+z.ssid,{o:"ALiBS.wu.ao",le:ALiBS.gv.wu.le});
				//
				z.ec = -2100; // 
				r = ALiBS.wu.lp();
			}
			//
			// *************************************************
			//
			else 
			{
				//
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wu.wsc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wu.wsc",le:ALiBS.gv.wu.le});
		//
		return r; 
	}
	// *****************************************************************
};
//
// *************************************************************************
//
if(ALiBS.gv.ws) // add urls to ws cache
{
	ALiBS.gv.ws.uc["ALiBS.wu.wsc.if.lp"] = ALiBS.wu.wsc; 
	//http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wu.wsc.if.lp&p1=xxxx
	//
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wu("+(++ALiBS.gv.wu.nn)+"x)",{o:"ALiBS.wu"});
ALiBS.sy.la(0+0,"ALiBS.gv.wu.rl="+ALiBS.gv.wu.rl,{o:"ALiBS.wu"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.wu.le="+ALiBS.gv.wu.le+ALiBS.gv.sy.se,{o:"ALiBS.wu"});

