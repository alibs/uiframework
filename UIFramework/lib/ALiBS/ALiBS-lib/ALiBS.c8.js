/* *************************************************************************
// INFO:

// *************************************************************************
TODO(2011-02-22): 

*/

//
// *************************************************************************
//
if(!ALiBS.gv.c8) ALiBS.gv.c8 = {}; // global values
//
if(!ALiBS.gv.c8.le) ALiBS.gv.c8.le = 0; // local log status
//
ALiBS.gv.c8.nn = 0; // x times loaded
//
ALiBS.gv.c8.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.c8 = // md5
{
	m5: function(s) // MD5 Hash encode
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8.m5",le:ALiBS.gv.c8.le});
		//
		return ALiBS.c8.b2s(ALiBS.c8._m5m4(ALiBS.c8.s2b(s), s.length * 8));
	},
	// *****************************************************************
	h5: function(k,d) // HMAC-MD5
	// k = (required)(String) secret key = password
	// d = (required)(String) data = message
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8.h5",le:ALiBS.gv.c8.le});
		//
		var bk = ALiBS.c8.s2b(k);
		if(bk.length>16) bk = ALiBS.c8._m5m4(bk,k.length*8); // TODO(2011-02-22): blocksize should be 64? http://en.wikipedia.org/wiki/HMAC
		//
		var ip = Array(16); // ipad // TODO: blocksize should be a variable to be tested var bs = 16|64|128
		var op = Array(16); // opad
		//
		for(var i=0;i<16;i++)
		{
			ip[i] = bk[i] ^ 0x36363636;
			op[i] = bk[i] ^ 0x5C5C5C5C;
		}
		//
		var h = ALiBS.c8._m5m4(ip.concat(ALiBS.c8.s2b(d)),512+d.length*8); // hash
		return ALiBS.c8.b2s(ALiBS.c8._m5m4(op.concat(h),512+128));
	},
	// *****************************************************************
	sh: function(n,s,k,d) // Super Hash = HMAC-MD5
	// n = (required)(number) times to run
	// s = (required)(String) salt
	// k = (required)(String) secret key = password
	// d = (required)(String) data = message
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8.sh",le:ALiBS.gv.c8.le});
		//
		var v = d;
		//
		for(var i=0;i<n;i++)
		{
			v = ALiBS.c8.h5(k+s,v+s);
		}
		//
		return v;
	},
	// *****************************************************************
	s2b: function(a) // string to binary // Convert a raw string to an array of little-endian words Characters >255 have their high-byte silently ignored
						//Endianness refers to the ordering of individually addressable sub-components within a longer data item as stored in external memory 
						//(or, sometimes, as sent on a serial connection). 
						//The usual contrast is whether the most-significant (big-endian) or least-significant (little-endian) byte is ordered first
						// (at the smallest address) within the larger data item.
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8.s2b",le:ALiBS.gv.c8.le});
		//
		var b = Array(a.length >> 2);
		for(var i = 0; i < b.length; i++) b[i] = 0;
		for(var i = 0; i < a.length * 8; i += 8) b[i>>5] |= (a.charCodeAt(i / 8) & 0xFF) << (i%32);
		return b;
	},
	// *****************************************************************
	b2s: function(a) // binary to string // Convert an array of little-endian words to a string
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8.b2s",le:ALiBS.gv.c8.le});
		//
		var b = "";
		for(var i = 0; i < a.length * 32; i += 8) b += String.fromCharCode((a[i>>5] >>> (i % 32)) & 0xFF);
		return b;
	},
	// *****************************************************************
	// *****************************************************************
	_m5m0: function(q,a,b,x,s,t) // md5
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8._m5m0",le:ALiBS.gv.c8.le});
		//
		var z0 = ALiBS.c8._m5m2(x,t);
		var z1 = ALiBS.c8._m5m2(a,q);
		//
		z0 = ALiBS.c8._m5m2(z1,z0);
		z0 = ALiBS.c8._m5m3(z0,s);
		z0 = ALiBS.c8._m5m2(z0,b);
		//	
		return z0;
	},
	// *****************************************************************
	_m5m1: function(n,a,b,c,d,x,s,t) // md5
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8._m5m1",le:ALiBS.gv.c8.le});
		//
		if(n==1) return ALiBS.c8._m5m0((b & c) | ((~b) & d), a, b, x, s, t); // ff
		else if(n==2) return ALiBS.c8._m5m0((b & d) | (c & (~d)), a, b, x, s, t); // gg
		else if(n==3) return ALiBS.c8._m5m0(b ^ c ^ d, a, b, x, s, t); // hh
		else if(n==4) return ALiBS.c8._m5m0(c ^ (b | (~d)), a, b, x, s, t); // ii
	},
	// *****************************************************************
	_m5m2: function(x,y) // md5 = Add integers, wrapping at 2^32. This uses 16-bit operations internally to work around bugs in some JS interpreters
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8._m5m2",le:ALiBS.gv.c8.le});
		//
		var a = (x & 0xFFFF) + (y & 0xFFFF);
		var b = (x >> 16) + (y >> 16) + (a >> 16);
		return (b << 16) | (a & 0xFFFF);
	},
	// *****************************************************************
	_m5m3: function(a,b) // md5 = Bitwise rotate a 32-bit number to the left
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8._m5m3",le:ALiBS.gv.c8.le});
		//
		return (a << b) | (a >>> (32 - b));
	},
	// *****************************************************************
	_m5m4: function(x,l) // md5 = Calculate the MD5 of an array of little-endian words, and a bit length
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8._m5m4",le:ALiBS.gv.c8.le});
		//
		/* append padding */
		x[l >> 5] |= 0x80 << ((l) % 32);
		x[(((l + 64) >>> 9) << 4) + 14] = l;
		//
		var a =  1732584193;
		var b = -271733879;
		var c = -1732584194;
		var d =  271733878;
		//
		for(var i = 0; i < x.length; i += 16)
		{
			var olda = a;
			var oldb = b;
			var oldc = c;
			var oldd = d;
			//
			a = ALiBS.c8._m5m1(1,a, b, c, d, x[i+ 0], 7 , -680876936);
			d = ALiBS.c8._m5m1(1,d, a, b, c, x[i+ 1], 12, -389564586);
			c = ALiBS.c8._m5m1(1,c, d, a, b, x[i+ 2], 17,  606105819);
			b = ALiBS.c8._m5m1(1,b, c, d, a, x[i+ 3], 22, -1044525330);
			a = ALiBS.c8._m5m1(1,a, b, c, d, x[i+ 4], 7 , -176418897);
			d = ALiBS.c8._m5m1(1,d, a, b, c, x[i+ 5], 12,  1200080426);
			c = ALiBS.c8._m5m1(1,c, d, a, b, x[i+ 6], 17, -1473231341);
			b = ALiBS.c8._m5m1(1,b, c, d, a, x[i+ 7], 22, -45705983);
			a = ALiBS.c8._m5m1(1,a, b, c, d, x[i+ 8], 7 ,  1770035416);
			d = ALiBS.c8._m5m1(1,d, a, b, c, x[i+ 9], 12, -1958414417);
			c = ALiBS.c8._m5m1(1,c, d, a, b, x[i+10], 17, -42063);
			b = ALiBS.c8._m5m1(1,b, c, d, a, x[i+11], 22, -1990404162);
			a = ALiBS.c8._m5m1(1,a, b, c, d, x[i+12], 7 ,  1804603682);
			d = ALiBS.c8._m5m1(1,d, a, b, c, x[i+13], 12, -40341101);
			c = ALiBS.c8._m5m1(1,c, d, a, b, x[i+14], 17, -1502002290);
			b = ALiBS.c8._m5m1(1,b, c, d, a, x[i+15], 22,  1236535329);
			//
			a = ALiBS.c8._m5m1(2,a, b, c, d, x[i+ 1], 5 , -165796510);
			d = ALiBS.c8._m5m1(2,d, a, b, c, x[i+ 6], 9 , -1069501632);
			c = ALiBS.c8._m5m1(2,c, d, a, b, x[i+11], 14,  643717713);
			b = ALiBS.c8._m5m1(2,b, c, d, a, x[i+ 0], 20, -373897302);
			a = ALiBS.c8._m5m1(2,a, b, c, d, x[i+ 5], 5 , -701558691);
			d = ALiBS.c8._m5m1(2,d, a, b, c, x[i+10], 9 ,  38016083);
			c = ALiBS.c8._m5m1(2,c, d, a, b, x[i+15], 14, -660478335);
			b = ALiBS.c8._m5m1(2,b, c, d, a, x[i+ 4], 20, -405537848);
			a = ALiBS.c8._m5m1(2,a, b, c, d, x[i+ 9], 5 ,  568446438);
			d = ALiBS.c8._m5m1(2,d, a, b, c, x[i+14], 9 , -1019803690);
			c = ALiBS.c8._m5m1(2,c, d, a, b, x[i+ 3], 14, -187363961);
			b = ALiBS.c8._m5m1(2,b, c, d, a, x[i+ 8], 20,  1163531501);
			a = ALiBS.c8._m5m1(2,a, b, c, d, x[i+13], 5 , -1444681467);
			d = ALiBS.c8._m5m1(2,d, a, b, c, x[i+ 2], 9 , -51403784);
			c = ALiBS.c8._m5m1(2,c, d, a, b, x[i+ 7], 14,  1735328473);
			b = ALiBS.c8._m5m1(2,b, c, d, a, x[i+12], 20, -1926607734);
			//
			a = ALiBS.c8._m5m1(3,a, b, c, d, x[i+ 5], 4 , -378558);
			d = ALiBS.c8._m5m1(3,d, a, b, c, x[i+ 8], 11, -2022574463);
			c = ALiBS.c8._m5m1(3,c, d, a, b, x[i+11], 16,  1839030562);
			b = ALiBS.c8._m5m1(3,b, c, d, a, x[i+14], 23, -35309556);
			a = ALiBS.c8._m5m1(3,a, b, c, d, x[i+ 1], 4 , -1530992060);
			d = ALiBS.c8._m5m1(3,d, a, b, c, x[i+ 4], 11,  1272893353);
			c = ALiBS.c8._m5m1(3,c, d, a, b, x[i+ 7], 16, -155497632);
			b = ALiBS.c8._m5m1(3,b, c, d, a, x[i+10], 23, -1094730640);
			a = ALiBS.c8._m5m1(3,a, b, c, d, x[i+13], 4 ,  681279174);
			d = ALiBS.c8._m5m1(3,d, a, b, c, x[i+ 0], 11, -358537222);
			c = ALiBS.c8._m5m1(3,c, d, a, b, x[i+ 3], 16, -722521979);
			b = ALiBS.c8._m5m1(3,b, c, d, a, x[i+ 6], 23,  76029189);
			a = ALiBS.c8._m5m1(3,a, b, c, d, x[i+ 9], 4 , -640364487);
			d = ALiBS.c8._m5m1(3,d, a, b, c, x[i+12], 11, -421815835);
			c = ALiBS.c8._m5m1(3,c, d, a, b, x[i+15], 16,  530742520);
			b = ALiBS.c8._m5m1(3,b, c, d, a, x[i+ 2], 23, -995338651);
			//
			a = ALiBS.c8._m5m1(4,a, b, c, d, x[i+ 0], 6 , -198630844);
			d = ALiBS.c8._m5m1(4,d, a, b, c, x[i+ 7], 10,  1126891415);
			c = ALiBS.c8._m5m1(4,c, d, a, b, x[i+14], 15, -1416354905);
			b = ALiBS.c8._m5m1(4,b, c, d, a, x[i+ 5], 21, -57434055);
			a = ALiBS.c8._m5m1(4,a, b, c, d, x[i+12], 6 ,  1700485571);
			d = ALiBS.c8._m5m1(4,d, a, b, c, x[i+ 3], 10, -1894986606);
			c = ALiBS.c8._m5m1(4,c, d, a, b, x[i+10], 15, -1051523);
			b = ALiBS.c8._m5m1(4,b, c, d, a, x[i+ 1], 21, -2054922799);
			a = ALiBS.c8._m5m1(4,a, b, c, d, x[i+ 8], 6 ,  1873313359);
			d = ALiBS.c8._m5m1(4,d, a, b, c, x[i+15], 10, -30611744);
			c = ALiBS.c8._m5m1(4,c, d, a, b, x[i+ 6], 15, -1560198380);
			b = ALiBS.c8._m5m1(4,b, c, d, a, x[i+13], 21,  1309151649);
			a = ALiBS.c8._m5m1(4,a, b, c, d, x[i+ 4], 6 , -145523070);
			d = ALiBS.c8._m5m1(4,d, a, b, c, x[i+11], 10, -1120210379);
			c = ALiBS.c8._m5m1(4,c, d, a, b, x[i+ 2], 15,  718787259);
			b = ALiBS.c8._m5m1(4,b, c, d, a, x[i+ 9], 21, -343485551);
			//
			a = ALiBS.c8._m5m2(a, olda);
			b = ALiBS.c8._m5m2(b, oldb);
			c = ALiBS.c8._m5m2(c, oldc);
			d = ALiBS.c8._m5m2(d, oldd);
		}
		//
		return Array(a, b, c, d);
	},
	// *****************************************************************
	xx: function(s) //
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c8.xx",le:ALiBS.gv.c8.le});
		//
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c8("+(++ALiBS.gv.c8.nn)+"x)",{o:"ALiBS.c8"});
ALiBS.sy.la(0+0,"ALiBS.gv.c8.le="+ALiBS.gv.c8.le+ALiBS.gv.sy.se,{o:"ALiBS.c8"});

