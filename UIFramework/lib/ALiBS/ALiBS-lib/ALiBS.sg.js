/* *************************************************************************
// INFO:

javascript tem2 metodos para substring:

s.substr(iniPos,len)

s.substring(iniPos,fimPos) fim incluido 
	- usar s.substring(iniPos,s.length)

// ***************************

// *************************************************************************
// TODO:

- corrigir nomes de parametros

*/
//
// *************************************************************************
//
if(!ALiBS.gv.sg) ALiBS.gv.sg = {}; // global values
//
if(!ALiBS.gv.sg.le) ALiBS.gv.sg.le = 0; // local log status
//
ALiBS.gv.sg.nn = 0; // x times loaded
//
ALiBS.gv.sg.rl = "15.0731.1454";
//
// *************************************************************************
//
ALiBS.sg = // string util
{
	ca: function(a0,p) // copy array - used by ALiBS.sy.ga()
	/** 
	Method <reference> <array> ALiBS.sg.ca(a0,p)

		Description: Copy array

		Parameters:
			t0 = (required)(array) array to be copied

			p = (optional)(JSON)parameter {}

				rv = (boolean)reverse array
					false = DEFAULT
				r = OLD Compatibility

		Observations:
	
		Return Value:
			<reference> <array> copied array

		Example(s):
			var ok = ALiBS.sg.ca(a0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ca",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(a0)!="object-Array") throw "ALiBS.sg.ca.Exception: Error: Invalid Parameter Type: (object-Array) a0 passed as "+ALiBS.sy.ty(a0); // 
			//
			ALiBS.sy.la(5+0,"a0="+a0,{o:"ALiBS.sg.ca",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_rv = z.rv || z.r; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_rv)!="boolean") z.p_rv = false; 
			//
			ALiBS.sy.la(5+0,"z.p_rv="+z.p_rv,{o:"ALiBS.sg.ca",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_b0 = new Array();
			//
			if(z.p_rv) // reverse
			{
				for(var i=0;i<a0.length;i++)
				{
					z.v_b0[z.v_b0.length] = a0[a0.length-i-1];
				}
			}
			else
			{
				for(var i=0;i<a0.length;i++)
				{
					z.v_b0[z.v_b0.length] = a0[i];
				}
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = (z.v_b0); // <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.ca",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.ca",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.ca",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	si: function(s0,a0,b0,p) // string inside 
	/** 
	Method <static> <string> ALiBS.sg.si(s0,a0,b0,p)

		Description: Substring Inside

		Parameters:

			s0 = (required)(string)string
			a0 = (required)(string)substring after = end
			b0 = (required)(string)substring before = start

			p = (optional)(JSON)parameter {}

				ad = (boolean)(after)include substring after
					false = DEFAULT

				ac = (boolean)(after)match case string after
					false = DEFAULT
			
				bd = (boolean)(before)include substring before
					false = DEFAULT

				bc = (boolean)(before)match case string before
					false = DEFAULT

		Observations:
	
		Return Value:
			substring inside OR ""

		Example:
			var x = ALiBS.sg.si("abcdX1234","abcd","1234");

	--------------------------------------------------------------------
	*/
	/*
		//
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.si.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(a0)!="string") throw "ALiBS.sg.si.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(b0)!="string") throw "ALiBS.sg.si.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"a0="+a0,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"b0="+b0,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_ad = z.ad; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_ac = z.ac; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_bd = z.bd; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_bc = z.bc; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_ad)!="boolean") z.p_ad = false; // DEFAULT
			if(typeof(z.p_ac)!="boolean") z.p_ac = false; // DEFAULT
			if(typeof(z.p_bd)!="boolean") z.p_bd = false; // DEFAULT
			if(typeof(z.p_bc)!="boolean") z.p_bc = false; // DEFAULT
			//
			ALiBS.sy.la(5+0,"z.p_ad="+z.p_ad,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_ac="+z.p_ac,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_bd="+z.p_bd,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_bc="+z.p_bc,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.ec = -2000; // 
			if(!s0)
			{
				r = ""; // <string>, NOT <reference> object
			}
			else
			{
				z.ec = -2100; // 
				z.v_s1 = ALiBS.sg.as(s0,a0,{d:z.p_ad,c:z.p_ac});
				ALiBS.sy.la(5+0,"z.v_s1="+z.v_s1,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
				//
				z.ec = -2200; // 
				z.v_s2 = ALiBS.sg.bs(z.v_s1,b0,{d:z.p_bd,c:z.p_bc});
				ALiBS.sy.la(5+0,"z.v_s2="+z.v_s2,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
				//
				r = "" + z.v_s2; // <string>, NOT <reference> object
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.si",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.si",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	as: function(s0,x0,p) // substring after string
	/** 
	Method <static> <string> ALiBS.sg.as(s0,x0,p)

		Description: Substring After

		Parameters:
			s0 = (required)(string) string
			x0 = (required)(string) substring to start

			p = (optional)(JSON)parameter {}

				d0 = (boolean)include substring
					false = DEFAULT
				d = OLD Compatibility

				c0 = (boolean)match case
					false = DEFAULT
				c = OLD Compatibility

		Observations:
	
		Return Value:
			<static> <string> substring after

		Example(s):
			var ok = ALiBS.sg.as("abcd1234","abcd");

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.as.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(x0)!="string") throw "ALiBS.sg.as.Exception: Error: Invalid Parameter Type: (string) x0 passed as "+typeof(x0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"x0="+x0,{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_d0 = z.d0 || z.d; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_c0 = z.c0 || z.c; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_d0)!="boolean") z.p_d0 = false; 
			if(typeof(z.p_c0)!="boolean") z.p_c0 = false; 
			//
			ALiBS.sy.la(5+0,"z.p_d0="+z.p_d0,{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_c0="+z.p_c0,{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_i0 = -1;
			//
			// ************************
			//
			if(z.p_c0) // match case
			{
				z.v_i0 = s0.indexOf(x0);
			}
			else // ignore case
			{
				z.v_i0 = s0.toLowerCase().indexOf(x0.toLowerCase());
			}
			//
			if(z.v_i0>=0)
			{
				if(z.p_d0) // include x
				{
					r = "" + s0.substring(z.v_i0,s0.length); // <string>, NOT <reference> object
				}
				else // exclude x
				{
					r = "" + s0.substring(z.v_i0+x0.length,s0.length); // <string>, NOT <reference> object
				}
			}
			else
			{
				r = "" + s0; // <string>, NOT <reference> object
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.as",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.as",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	bs: function(s0,x0,p) // substring before string
	/** 
	Method <static> <string> ALiBS.sg.bs(s0,x0,p)

		Description: Substring Before

		Parameters:
			s0 = (required)(string) string
			x0 = (required)(string) substring to start

			p = (optional)(JSON)parameter {}

				d0 = (boolean)include substring
					false = DEFAULT
				d = OLD Compatibility

				c0 = (boolean)match case
					false = DEFAULT
				c = OLD Compatibility

		Observations:
	
		Return Value:
			<static> <string> substring before

		Example(s):
			var ok = ALiBS.sg.bs("abcd1234","1234");

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.bs.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(x0)!="string") throw "ALiBS.sg.bs.Exception: Error: Invalid Parameter Type: (string) x0 passed as "+typeof(x0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"x0="+x0,{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_d0 = z.d0 || z.d; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_c0 = z.c0 || z.c; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_d0)!="boolean") z.p_d0 = false; 
			if(typeof(z.p_c0)!="boolean") z.p_c0 = false; 
			//
			ALiBS.sy.la(5+0,"z.p_d0="+z.p_d0,{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_c0="+z.p_c0,{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_i0 = -1;
			//
			// ************************
			//
			if(z.p_c0) // match case
			{
				z.v_i0 = s0.indexOf(x0);
			}
			else // ignore case
			{
				z.v_i0 = s0.toLowerCase().indexOf(x0.toLowerCase());
			}
			//
			if(z.v_i0>=0)
			{
				if(z.p_d0) // include x
				{
					r = "" + s0.substring(0,z.v_i0+x0.length); // <string>, NOT <reference> object
				}
				else // exclude x
				{
					r = "" + s0.substring(0,z.v_i0); // <string>, NOT <reference> object
				}
			}
			else
			{
				r = "" + s0; // <string>, NOT <reference> object
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//r = r;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.bs",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.bs",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	ce: function(s,a,p) // string equals any item of list (array) (exact item)
	// s = (required)(string) string
	// a = (required)(array) array (not regular expressions)
	// implementar p (JSON) p.n = return occurence n
	//
	// return (number)position in Array OR -1
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ce",le:ALiBS.gv.sg.le});
		//
		for(var i=0;i<a.length;i++)
		{
			if( s == a[i] ) return i; // encontrou
		}
		//
		return -1;
	},
	// *****************************************************************
	co: function(s,a,p) // string contains any item of list (array) (match item = not necessarily exact item) = like indexOf() for many items
	// s = (required)(String)string
	// a = (required)(Array)array of regular expressions
	// implementar p (JSON) p.n = return occurence n
	//
	// return (number)position in Array OR -1
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.co",le:ALiBS.gv.sg.le});
		//
		for(var i=0;i<a.length;i++)
		{
			var m = s.match(a[i]);
			if(m!=null) return i; // encontrou
		}
		//
		return -1;
	},
	// *****************************************************************
	no: function(s,v) // number of occurances of substring
	// s = (required)(String)string/text
	// v = (required)(String)substring (to be counted)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.no",le:ALiBS.gv.sg.le});
		//
		return s.split(v).length-1;
	},
	// *****************************************************************
	nox: function(s,v,p) // number of occurances of substring outside ( " '
	// s = (required)(String)string/text
	// v = (required)(String)substring (to be counted)
	// p = (optional)(JSON)parameters 
	//	x = characters that open/close 
	// return 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.nox",le:ALiBS.gv.sg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.n = 0;
		z.t = {}; // status
		//
		if(p)
		{
			if(typeof(p.x)=="string") z.x = p.x;
		}
		//
		if(typeof(z.x)=="undefined") z.x = "/()'" + '[]"'; // DEFAULT 
		//
		for(var i = 0;i<s.length;i++)
		{
			z.c = s.charAt(i);
			//
			if(z.x.indexOf(z.c)>=0)
			{
				if(z.c=='"' || z.c=="'" || z.c=="/") // opening or closing
				{
					if(typeof(z.t[z.c])=="boolean" && z.t[z.c]==true) // is open
					{
						z.t[z.c] = false; // close
					}
					else // is closed
					{
						z.t[z.c] = true; // open
					}
				}
				else if(z.c=="(") // opening
				{
					if(typeof(z.t["("])=="number") 
					{
						z.t["("]++; 
					}
					else 
					{
						z.t["("] = 1; 
					}
				}
				else if(z.c==")") // closing
				{
					if(typeof(z.t["("])=="number") 
					{
						z.t["("]--; 
					}
					else 
					{
						z.t["("] = 0; 
					}
				}
				else if(z.c=="[") // opening
				{
					if(typeof(z.t["["])=="number") 
					{
						z.t["["]++; 
					}
					else 
					{
						z.t["["] = 1; 
					}
				}
				else if(z.c=="]") // closing
				{
					if(typeof(z.t["["])=="number") 
					{
						z.t["["]--; 
					}
					else 
					{
						z.t["["] = 0; 
					}
				}
			}
			else if(z.c==v)
			{
				z.nn = 0;
				//
				for(var j in z.t)
				{
					if(typeof(z.t[j])=="boolean" && z.t[j]==true || typeof(z.t[j])=="number" && z.t[j]>0 )
					{
						z.nn++; // at least one is open
						break;
					}
				}
				//
				if(z.nn==0)
				{
					z.n++; // count occurrance if is outside
				}
			}
		}
		//
		var r = 0 + z.n; // [BEST-PRACTICE] sum 0 to return a number, NOT an OBJECT reference
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	cp: function(s,v) // Capitalization
	// s = (required)(String)string
	// a = (required)(Array)array (ponto, virgula, espaço, especiais)
	//
	// return (String)Capitalized string
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.cp",le:ALiBS.gv.sg.le});
		//
		var o = false;
		var a = new Array();
		if(!v) var v = [/ /];
		var c;
		var t = s.toLowerCase();
		//
		for(var i=0;i<s.length;i++)
		{
			c = new String(t.charAt(i));
			//
			if(ALiBS.sg.ce(c,v)>-1)
			{
				o = true;
			}
			else if(o)
			{
				c = c.toUpperCase();
				o = false;
			}
			else if(i==0)
			{
				c = c.toUpperCase();
				o = false;
			}
			//
			a[a.length]=c;
		}
		//
		return a.join("");
	},
	// *****************************************************************
	cr: function(s,n,p) // crop to n chars
	// s = (required)(String)string
	// n = (required)(number)number of chars to return
	// p = (optional)(JSON)parameters {}
	//	o = (number)orientation
	//		1 = crop from left = remove left = DEFAULT
	//		2 = crop from right = remove right
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.cr",le:ALiBS.gv.sg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		var r;
		//
		if(p)
		{
			if(typeof(p.o)=="number" && p.o==2) z.o = 2; // rigth
		}
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.o)=="undefined" || !z.o) z.o = 1; // DEFAULT = left
		//
		if(z.o==1) // 1 = left
		{
			r = "" + s.substring(s.length-n,s.length); // <string>, NOT <reference> object
		}
		else // 2 = right
		{
			r = "" + s.substring(0,n); // <string>, NOT <reference> object
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	id: function(s,p) // isDigit 
	// s = (required)(String)string
	// p = (optional)(JSON)parameters {m:100,n:10,o:["+","-","."]}
	//	o = (Array)outros (virgula, ponto, $)(valid values, considered as part of number)
	//	m = (number)max length
	//	n = (number)min length
	// return (boolean)true/false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.id",le:ALiBS.gv.sg.le});
		//
		if(typeof(s)!="string" || s==null || s=="") 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.id:typeof(s)!=string||s==null||s==''"; 
			return false;
		}
		//
		var v = null;
		var m = 1000000; // 1 Mega
		var n = 0;
		//
		var l = s.length;
		var c;
		//
		if(p)
		{
			if(typeof(p.o)=="obj"+"ect") v = p.o;
			//
			if(typeof(p.m)=="number") m = p.m;
			//
			if(typeof(p.n)=="number") n = p.n;
		}
		//
		if(s.length<n || s.length>m) 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.id:s.length<n||s.length>m"; 
			return false;
		}
		//
		for(var i=0;i<l;i++)
		{
			c = s.charAt(i);
			if((c<"0") || (c>"9"))
			{
				var y = false;
				if(v && v.length>0)
				{
					for(var j=0;j<v.length;j++)
					{
						if( c==v[j] )
						{
							y=true; // encontrou
							break;
						}			 	
					}
				}
				if(!y) 
				{
					ALiBS.gv.er.em=":ERROR:ALiBS.sg.id:s is not a digit String"; 
					return false;
				}
			}
		}
		return true;
	},
	// *****************************************************************
	ie: function(s) // isEmpty
	// s = (required)(String)string
	//
	// return (boolean)true/false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ie",le:ALiBS.gv.sg.le});
		//
		if((typeof s=="undefined") || (s == null) || (s.length == 0)) return true;
		else return false;
	},
	// *****************************************************************
	ic: function(s) // isChar 
	// s = (required)(String)string
	// return (boolean)true/false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ic",le:ALiBS.gv.sg.le});
		//
		if(typeof(s)!="string" || s==null || s=="") 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.id:typeof(s)!=string||s==null||s==''"; 
			return false;
		}
		//
		var l = s.length;
		var c;
		//
		for(var i=0;i<l;i++)
		{
			c = s.charAt(i);
			if((c<"A") || (c>"z")) return false;
			if((c>"Z") && (c<"a")) return false;
		}
		return true;
	},
	// *****************************************************************
	is: function(s) // isSpecial TODO(2010-09-14): colocar em stress test
	// s = (required)(String)string
	// return (boolean)true/false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.is",le:ALiBS.gv.sg.le});
		//
		if(typeof(s)!="string" || s==null || s=="") 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.id:typeof(s)!=string||s==null||s==''"; 
			return false;
		}
		//
		var l = s.length;
		var c;
		//
		for(var i=0;i<l;i++)
		{
			c = s.charAt(i);
			if(ALiBS.sg.ic(c)) return false;
			if(ALiBS.sg.id(c)) return false;
		}
		return true;
	},
	// *****************************************************************
	/*
	 * @return found position | o | -1
	 * 
	 */
	fv: function(s,o) // firstValid - refazer conforme ltrim/rtrim
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.fv",le:ALiBS.gv.sg.le});
		//
		var tmp = s.search(/[^\n\t\s]/);
		//
		if(tmp==-1 && arguments.length==2)
		{
			tmp = o;
		}
		//
		return tmp;
	},
	// *****************************************************************
	lv: function(s,o) // lastValid - refazer conforme ltrim/rtrim
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.lv",le:ALiBS.gv.sg.le});
		//
		var tmp2 = this.reverse(s);
		var tmp = tmp2.search(/[^\n\t\s]/);
		//
		if(tmp==-1 && arguments.length==2)
		{
			tmp = o;
		}
		else
		{
			tmp = tmp2.length-tmp-1;
		}
		//
		return tmp;
	},
	// *****************************************************************
	rc: function(s,oldChar,newChar,start,end) // replaceChar = 1 char with 1 char/String
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rc",le:ALiBS.gv.sg.le});
		//
		var tmp = "";
		for(var i=0;i<s.length;i++)
		{
			var tmp2 = s.charAt(i);
			if(i>=start && i<=end && tmp2==oldChar)
			{
				tmp = tmp.concat(newChar); 
			}
			else
			{ 
				tmp = tmp.concat(tmp2); 
			}
		}
		//
		return tmp;
	},	
	// *****************************************************************
	pw: function(p) // random password
	// p = (optional)(JSON)parameters {}
	//
	//	clw = (string)lower case characters
	//		DEFAULT = [a-z]
	//
	//	cup = (string)upper case characters
	//		DEFAULT = [A-Z]
	//
	//	cnu = (string)number characters
	//		DEFAULT = [0-9]
	//
	//	csp = (string)special characters
	//		DEFAULT = !@#$%&-_+?/
	//
	//	le = (number)length to return
	//		-1 = DEFAULT = ALL characters = 73
	//
	// return = ramdom password
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.pw",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			// Aumentar z.ec de 100/100
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.clw)!="string") z.clw = "abcdefghijklmnopqrstuvwxyz"; // 26
			if(typeof(z.cup)!="string") z.cup = "ABCDEFGHIJKLMNOPQRSTUWVXYZ"; // 26
			if(typeof(z.cnu)!="string") z.cnu = "0123456789"; // 10
			if(typeof(z.csp)!="string") z.csp = "!@$%&-_+?/#"; // 11
			if(typeof(z.le)!="number" || z.le<1) z.le = -1;
			//
			// local variables [BEST-PRACTICE]
			z.ec = -1101; // 
			z.s0 = z.clw + z.cup + z.cnu + z.csp;
			z.ec = -1102; // 
			z.v_s1 = ALiBS.c2.sa(z.s0);
			//
			z.ec = -1108; // 
			for(var i=0;i<10;i++)
			{
				z.v_s1.sort(function(a,b){return ALiBS.sg.rd(0,2)-1}); // sort array using random
			}
			z.ec = -1110; // 
			if(z.le==-1) // ALL characters = 73 = z.clw + z.cup + z.cnu + z.csp
			{
				z.ec = -1111; // 
				r = ALiBS.c2.as(z.v_s1);
//console.log("(-1)z.le="+z.le);
			}
			else // defined length
			{
				z.v_v0 = z.v_s1.length;
				//
				if(z.le>z.v_v0) // z.le > 73
				{
//console.log("(1)z.le="+z.le);
					z.v1 = z.le-z.v_v0; // size difference
					//
					for(var i=0;i<z.v1;i++)
					{
						z.v2 = ALiBS.sg.rd(0,72); // ramdom 
						//
						z.v_s1[z.v_s1.length] = z.v_s1[z.v2];
					}
					//
					r = ALiBS.sg.u2s( ALiBS.c2.as(z.v_s1) );
				}
				else 
				{
//console.log("(2)z.le="+z.le);
					z.ec = -1120; // 
					r = ALiBS.sg.u2s( ALiBS.c2.as(z.v_s1) ).substr(0,z.le);
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.pw",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	rd: function(mn,mx,p) // random number
	/** 
	Method <static> <number> ALiBS.sg.rd(mn,mx,p)

		Description: Random Number

		Parameters:

			mn = (required)(number)min number (0<=min)
			mx = (required)(number)max number (0<=min<=max)

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number> randon number

		Example(s):
			var ok = ALiBS.sg.rd(1,100);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rd",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			//if(!s0 && s0!=0) s0 = ""; // Only if parameter s0 is allowed to be [EMPTY]
			//
			if(typeof(mn)!="number") throw "ALiBS.sg.rd.Exception: Error: Invalid Parameter Type: (number) mn passed as "+typeof(mn); // 
			if(typeof(mx)!="number") throw "ALiBS.sg.rd.Exception: Error: Invalid Parameter Type: (number) mx passed as "+typeof(mx); // 
			//
			if(mn>=mx) throw "ALiBS.sg.rd.Exception: Error: Invalid Parameter Value: mn("+mn+") >= mx("+mx+")"; // 
			//
			ALiBS.sy.la(5+0,"mn="+mn,{o:"ALiBS.sg.rd",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"mx="+mx,{o:"ALiBS.sg.rd",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			r = Math.floor(Math.random()*(mx-mn+1) + mn);
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = (+r); // <number>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rd",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rd",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rd",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	rg: function(le,p) // random string
	/** 
	Method <static> <string> ALiBS.sg.rg(le,p)

		Description: Random String

		Parameters:

			le = (required)(number)string length

			p = (optional)(JSON)parameter {}

				s0 | s = (string)string with valid chars (s = OLD Compatibility)
					0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ = DEFAULT 

		Observations:
	
		Return Value:
			<static> <string> random string

		Example(s):
			var ok = ALiBS.sg.rg(10);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rg",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			//if(!s0 && s0!=0) s0 = ""; // Only if parameter s0 is allowed to be [EMPTY]
			//
			if(typeof(le)!="number") throw "ALiBS.sg.rg.Exception: Error: Invalid Parameter Type: (number) le passed as "+typeof(le); // 
			//
			if(le==0) throw "ALiBS.sg.rd.Exception: Error: Invalid Parameter Value: le("+le+")"; // 
			//
			ALiBS.sy.la(5+0,"le="+le,{o:"ALiBS.sg.rg",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_s0 = z.s0 || z.s; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_s0)!="string") z.p_s0 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			//
			ALiBS.sy.la(5+0,"z.p_s0="+z.p_s0,{o:"ALiBS.sg.rg",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_v0 = new Array();
			//
			// ************************
			//
			for(var i=0;i<le;i++)
			{
				z.v_v0[i] = "" + z.p_s0.charAt(ALiBS.sg.rd(0,z.p_s0.length-1));
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_v0.join(""); // <string>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rg",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rg",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rg",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	rs: function(s0,fr,to,p) // replace string 
	/** 
	Method <static> <string> ALiBS.sg.rs(p)

		Description: Replace String (each <char> like "fr" is replaced with <string> "to")

		Parameters:
			s0 = (required)(string) string
			fr = (required)(string) from
			to = (required)(string) to

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> string replaced

		Example(s):
			var ok = ALiBS.sg.rs("aabbccdd","ab","x");

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rs",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.rs.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(fr)!="string") throw "ALiBS.sg.rs.Exception: Error: Invalid Parameter Type: (string) fr passed as "+typeof(fr); // 
			if(typeof(to)!="string") throw "ALiBS.sg.rs.Exception: Error: Invalid Parameter Type: (string) to passed as "+typeof(to); // 
			//
			if(!s0) throw "ALiBS.sg.rs.Exception: Error: Invalid Parameter Value: <EMPTY|NULL> s0"; // 
			if(!fr) throw "ALiBS.sg.rs.Exception: Error: Invalid Parameter Value: <EMPTY|NULL> fr"; // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.rs",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"fr="+fr,{o:"ALiBS.sg.rs",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"to="+to,{o:"ALiBS.sg.rs",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_a0 = new Array();
			//
			// ************************
			//
			z.v_ex = new RegExp("["+fr+"]","gi");
			//
			z.v_r0 = "" + s0.replace(z.v_ex,to); // <string>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0; // <string>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rs",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rs",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rs",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	rse: function(s0,fr,to,p) // replace string using RegExp (the occurance of expression is replaced)
	/** 
	Method <static> <string> ALiBS.sg.rse(s0,fr,to,p)

		Description: Replace String Using RegExp

		Parameters:

			s0 = (required)(string) string
			fr = (required)(string) from
			to = (required)(string) to

			p = (optional)(JSON)parameter {}

				op | o = (string) options (o = OLD Compatibility)
					"gi" = DEFAULT

		Observations:
	
		Return Value:
			<static> <string> string replaced

		Example(s):
			var ok = ALiBS.sg.rse(s0,fr,to);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.rse.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(typeof(fr)!="string") throw "ALiBS.sg.rse.Exception: Error: Invalid Parameter Type: (string) fr passed as "+typeof(fr); // 
			if(typeof(to)!="string") throw "ALiBS.sg.rse.Exception: Error: Invalid Parameter Type: (string) to passed as "+typeof(to); // 
			//
			if(!s0) throw "ALiBS.sg.rse.Exception: Error: Invalid Parameter Value: <EMPTY|NULL> s0"; // 
			if(!fr) throw "ALiBS.sg.rse.Exception: Error: Invalid Parameter Value: <EMPTY|NULL> fr"; // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"fr="+fr,{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"to="+to,{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_op = z.op || z.o; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_op)!="string") z.p_op = "gi"; 
			//
			ALiBS.sy.la(5+0,"z.p_op="+z.p_op,{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.v_r1 = new RegExp(fr,z.p_op);
			//
			z.v_r0 = s0.replace(z.v_r1,to);
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0; // <string>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rse",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rse",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	re: function(s,n) // repeat string
	// s = (required)(String)string to repeat
	// n = (required)(number)times to repeat
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.re",le:ALiBS.gv.sg.le});
		//
		if(typeof(s)!="string" || !s || s=="") 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.re:typeof(s)!=string||!s||s==''"; 
			return "";
		}
		//
		if(typeof(n)!="number" || !n || n<=0) 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.re:typeof(n)!=number||!n||n<=0"; 
			return "";
		}
		//
		var a = new Array();
		//
		for(var i=0;i<n;i++)
		{
			a[a.length] = s;
		}
		//
		return a.join("");
	},
	// *****************************************************************
	rv: function(s0,p) // reverse
	/** 
	Method <static> <string> ALiBS.sg.rv(s0,p)

		Description: Reverse String

		Parameters:
			s0 = (required)(string) string

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> reversed string

		Example(s):
			var ok = ALiBS.sg.rv("abcd");

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rv",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(!s0) s0 = "";
			//
			if(typeof(s0)!="string") throw "ALiBS.sg.rv.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.rv",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			if(!s0)
			{
				r = ""; // <string>, NOT <reference> object
			}
			else
			{
				r = "" + s0.split("").reverse().join(""); // <string>, NOT <reference> object
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//r = r;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rv",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rv",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rv",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	lt: function(s0,p) // ltrim
	/** 
	Method <static> <string> ALiBS.sg.lt(p)

		Description: String Left Trim

		Parameters:
			s0 = (required)(string) string

			p = (optional)(JSON)parameter {}

				rc | c = (string)chars to remove (c = OLD Compatibility)
					s,t,n,r,0,xB = DEFAULT
						z = zero char "0"
						s = space
						t = \t tab
						n = \n new line
						r = \r car return
						0 = byte 0
						xB = byte 11

				oc | o = (string)real/other chars to remove (o = OLD Compatibility)
					"" = DEFAULT 
						- NOT the chars in symbol list 
						- DO NOT use "0",space,tab,new line,car return, byte 0, byte 11, 
						- DO NOT use separator: "," or other separator, join all chars without separator = "#$@&"

				cm = (number)command
					1 = left trim = DEFAULT
					2 = rigth trim
					3 = trim = both = left + rigth

		Observations:
	
		Return Value:
			<static> <string> string without removed chars

		Example(s):
			var ok = ALiBS.sg.lt(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(!s0)
			{
				r = "";
			}
			else
			{
				if(typeof(s0)!="string") throw "ALiBS.sg.lt.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
				//
				ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				//
				// ************************
				//
				z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
				if(p)
				{
					if(typeof(p)=="string") // p = "z,s,t,n,r,0,xB" = old style
					{
						z.p_rc = p; // p = "z,s,t,n,r,0,xB" = old style
					}
					else // JSON // p = {c:"z,s,t,n,r,0,xB"} = new style
					{
						z.p_rc = (p.cr||p.c); // Parameter Pattern and/or OLD Parameter Compatibility
						z.p_oc = (p.oc||p.o); // Parameter Pattern and/or OLD Parameter Compatibility
						z.p_cm = p.cm; // Parameter Pattern and/or OLD Parameter Compatibility
					}
				}
				//
				// ************************
				//
				z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
				if(typeof(z.p_rc)!="string" || z.p_rc=="") z.p_rc = "s,t,n,r,0,xB"; // Default Symbols ([z]ero is NOT default)
				if(typeof(z.p_oc)!="string") z.p_oc = ""; // Default Real chars = {o:"*,#"} = new style
				if(typeof(z.p_cm)!="number") z.p_cm = 1; // Default = left trim
				//
				ALiBS.sy.la(5+0,"z.p_rc="+z.p_rc,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				ALiBS.sy.la(5+0,"z.p_oc="+z.p_oc,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				ALiBS.sy.la(5+0,"z.p_cm="+z.p_cm,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				//
				// ************************
				//
				z.ec = -1050; // [BEST-PRACTICE] table variables
				//
				// ************************
				//
				z.ec = -1100; // [BEST-PRACTICE] local variables
				z.v_c0 = new Array();
				z.v_b0 = new Array();
				//
				// ************************
				//
				for(var i=0;i<z.p_rc.length;i++)
				{
					if(z.p_rc.charAt(i)=="z") z.v_c0[z.v_c0.length] = String.fromCharCode(48); // "0" = string zero
					else if(z.p_rc.charAt(i)=="s") z.v_c0[z.v_c0.length] = String.fromCharCode(32); // space
					else if(z.p_rc.charAt(i)=="t") z.v_c0[z.v_c0.length] = String.fromCharCode(9); // tab
					else if(z.p_rc.charAt(i)=="n") z.v_c0[z.v_c0.length] = String.fromCharCode(10); // new line
					else if(z.p_rc.charAt(i)=="r") z.v_c0[z.v_c0.length] = String.fromCharCode(13); // car return
					else if(z.p_rc.charAt(i)=="0") z.v_c0[z.v_c0.length] = String.fromCharCode(0); // byte 0 = \0
				}
				//
				if(z.p_rc.indexOf("xB")>=0) z.v_c0[z.v_c0.length] = String.fromCharCode(11); 
				//
				//
				for(var i=0;i<z.p_oc.length;i++)
				{
					z.v_c0[z.v_c0.length] = z.p_oc.charAt(i); // other REAL chars
				}
				//
				ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				ALiBS.sy.la(5+0,"z.p_rc="+z.p_rc,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				ALiBS.sy.la(5+0,"z.p_oc="+z.p_oc,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				ALiBS.sy.la(5+0,"z.v_c0="+z.v_c0,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
				//
				for(var i=0;i<s0.length;i++)
				{
					z.v_b0[z.v_b0.length] = s0.charAt(i);
				}
				//
				if(z.p_cm==1 || z.p_cm==3) // left trim OR trim=both
				{
					z.v_f0 = true;
					//
					while(z.v_f0)
					{
						z.v_f0 = false;
						//
						for(var j=0;j<z.v_c0.length;j++) // loop c0
						{
							if(z.v_b0[0]==z.v_c0[j])
							{
								z.v_b0.shift(); // remove first item
								z.v_f0 = true; 
								break;
							}
						}
					}
				}
				//
				if(z.p_cm==2 || z.p_cm==3) // rigth trim OR trim=both
				{
					z.v_f0 = true;
					//
					while(z.v_f0)
					{
						z.v_f0 = false;
						//
						for(var j=0;j<z.v_c0.length;j++) // loop c0
						{
							if(z.v_b0[z.v_b0.length-1]==z.v_c0[j])
							{
								z.v_b0.pop(); // remove last item
								z.v_f0 = true; 
								break;
							}
						}
					}
				}
				//
				r = "" + z.v_b0.join(""); // <string>, NOT <reference> object
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//r = r;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.lt",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.lt",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	rt: function(s0,p) // right trim
	/** 
	Method <static> <string> ALiBS.sg.rt(s0,p)

		Description: String Right Trim

		Parameters:
			s0 = (required)(string) string

			p = (optional)(JSON)parameter {}

				cr | c = (string)chars to remove (c = OLD Compatibility)
					s,t,n,r,0,xB = DEFAULT

				oc | o = (string)other chars to remove (o = OLD Compatibility)
					"" = DEFAULT 

		Observations:
	
		Return Value:
			<static> <string> string without removed chars

		Example(s):
			var ok = ALiBS.sg.rt(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rt",le:ALiBS.gv.sg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(!s0)
			{
				r = "";
			}
			else
			{
				if(typeof(s0)!="string") throw "ALiBS.sg.rt.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
				//
				ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.rt",le:ALiBS.gv.sg.le});
				//
				// ************************
				//
				z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
				if(p)
				{
					if(typeof(p)=="string") // p = "z,s,t,n,r,0,xB" = old style
					{
						r = "" + ALiBS.sg.lt(s0,{cr:p,oc:"",cm:2}); // <string>, NOT <reference> object
					}
					else // JSON // p = {c:"z,s,t,n,r,0,xB"} = new style
					{
						r = "" + ALiBS.sg.lt(s0,{cr:(p.cr||p.c),oc:(p.oc||p.o),cm:2}); // <string>, NOT <reference> object
					}
				}
				else
				{
					r = "" + ALiBS.sg.lt(s0,{cm:2}); // <string>, NOT <reference> object
				}
				//
				// ************************
				//
				z.ec = -1050; // [BEST-PRACTICE] table variables
				//
				// ************************
				//
				z.ec = -1100; // [BEST-PRACTICE] local variables
				//
				// ************************
				//
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//r = r;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rt",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rt",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rt",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	tr: function(s0,p) // trim
	/** 
	Method <static> <string> ALiBS.sg.tr(s0,p)

		Description: String Trim

		Parameters:
			s0 = (required)(string) string

			p = (optional)(JSON)parameter {}

				cr | c = (string)chars to remove (c = OLD Compatibility)
					s,t,n,r,0,xB = DEFAULT

				oc | o = (string)other chars to remove (o = OLD Compatibility)
					"" = DEFAULT 

		Observations:
	
		Return Value:
			<static> <string> string without removed chars

		Example(s):
			var ok = ALiBS.sg.tr(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.tr",le:ALiBS.gv.sg.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(!s0)
			{
				r = "";
			}
			else
			{
				if(typeof(s0)!="string") throw "ALiBS.sg.tr.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
				//
				ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.tr",le:ALiBS.gv.sg.le});
				//
				// ************************
				//
				z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
				if(p)
				{
					if(typeof(p)=="string") // p = "z,s,t,n,r,0,xB" = old style
					{
						r = "" + ALiBS.sg.lt(s0,{cr:p,oc:"",cm:3}); // <string>, NOT <reference> object
					}
					else // JSON // p = {c:"z,s,t,n,r,0,xB"} = new style
					{
						r = "" + ALiBS.sg.lt(s0,{cr:(p.cr||p.c),oc:(p.oc||p.o),cm:3}); // <string>, NOT <reference> object
					}
				}
				else
				{
					r = "" + ALiBS.sg.lt(s0,{cm:3}); // <string>, NOT <reference> object
				}
				//
				// ************************
				//
				z.ec = -1050; // [BEST-PRACTICE] table variables
				//
				// ************************
				//
				z.ec = -1100; // [BEST-PRACTICE] local variables
				//
				// ************************
				//
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//r = r;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.tr",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.tr",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.tr",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	fi: function(s,c,m,p) // fill with char in left/rigth
	// s = (required)(String)string to fill 
	// c = (required)(char)char to fill
	// m = (required)(number)max final length
	// p = (optional)(JSON)parameters {d:"l"} 
	//	d = (string) direction to fill (l=left=default, r=rigth)
	//
	// return (String)string filled
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.fi",le:ALiBS.gv.sg.le});
		//
		if(typeof(s)!="string" || s==null) 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.sg.fi:typeof(s)!=string||s==null"; 
			return null;
		}
		//
		if(typeof(c)!="string" || c==null || c=="") 
		{
			ALiBS.gv.er.em=":WARNING:ALiBS.sg.fi:typeof(c)!=string||c==null||c==''"; 
			return s;
		}
		//
		if(typeof(m)!="number" || m==null || m==0) 
		{
			ALiBS.gv.er.em=":WARNING:ALiBS.sg.fi:typeof(m)!=number||m==null||m==0"; 
			return s;
		}
		//
		var l = s.length;
		if(l>=m) return s; // current length is bigger than final length
		var n = m-l;
		//
		var d = (p && p.d) || "l";
		if(d!="l" && d!="r") d = "l";
		var r;
		//
		if(d=="l")
		{
			r = "" + ALiBS.sg.re(c,n) + s; // <string>, NOT <reference> object
		}
		else if(d=="r")
		{
			r = "" + s + ALiBS.sg.re(c,n); // <string>, NOT <reference> object
		}
		else
		{
			//r = r; // <string>, NOT <reference> object
		}
		//
		return r;
	},
	// *****************************************************************
	zp: function(s0,p) // compress text = compact = ZIP 
	/** 
	Method <reference> <number:array> ALiBS.sg.zp(s0)

		Description: Text Compress (ZIP)

		Parameters:
			s0 = (required)(string) string

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<reference> <number:array> ALiBS.sg.zp(s0)

		Example(s):
			var ok = ALiBS.sg.zp(s0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

		LZW Compression/Decompression for Strings
		Compress to about 16% of original size.
		http://rosettacode.org/wiki/LZW_compression#JavaScript

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.zp",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = []; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			//
			if(typeof(s0)!="string") throw "ALiBS.sg.zp.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.zp",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_dc = {};
			z.v_dz = 256; // z.v_dc size
			z.v_wc;
			z.v_w0 = "";
			z.v_c0;
			//
			// ************************
			//
			for(var i=0;i<z.v_dz;i++) 
			{
				z.v_dc[String.fromCharCode(i)] = i;
			}
			//
			for(var i=0;i<s0.length;i++) 
			{
				z.v_c0 = s0.charAt(i);
				//
				z.v_wc = z.v_w0 + z.v_c0;
				//
				if(z.v_dc[z.v_wc]) 
				{
					z.v_w0 = z.v_wc;
				}
				else 
				{
					r.push(z.v_dc[z.v_w0]);
					z.v_dc[z.v_wc] = z.v_dz++; // Add z.v_wc to the z.v_dc
					z.v_w0 = String(z.v_c0);
				}
			}
			//
			if(z.v_w0!=="") 
			{
				r.push(z.v_dc[z.v_w0]);
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = (r); // <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.zp",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.zp",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.zp",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	uz: function(a0,p) // uncompress text = UNZIP
	/** 
	Method <static> <string> ALiBS.sg.uz(a0,p)

		Description: Text Uncompress (unZIP)

		Parameters:
			a0 = (required)(string) compressed string

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <string> string/text

		Example(s):
			var ok = ALiBS.sg.uz(a0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.uz",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			//
			if(ALiBS.sy.ty(a0)!="object-Array") throw "ALiBS.sg.uz.Exception: Error: Invalid Parameter Type: (object-Array) a0 passed as "+ALiBS.sy.ty(a0); // 
			//
			ALiBS.sy.la(5+0,"a0="+a0,{o:"ALiBS.sg.uz",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_dc = [];
			z.v_dz = 256; // z.v_dc size
			z.v_w0;
			z.v_k0;
			z.v_e0 = "";
			z.v_r0 = new Array();
			//
			// ************************
			//
			for(var i=0;i<z.v_dz;i++) 
			{
				z.v_dc[i] = String.fromCharCode(i);
			}
	 		//
			z.v_w0 = String.fromCharCode(a0[0]);
			z.v_r0[z.v_r0.length] = z.v_w0;
			//
			for(var i=1;i<a0.length;i++) 
			{
				z.v_k0 = a0[i];
				//
				if(z.v_dc[z.v_k0]) 
				{
					z.v_e0 = z.v_dc[z.v_k0];
				} 
				else 
				{
					if(z.v_k0===z.v_dz) 
					{
						z.v_e0 = z.v_w0 + z.v_w0.charAt(0);
					} 
					else 
					{
						return null;
					}
				}
	 			//
				z.v_r0[z.v_r0.length] = z.v_e0;
				//
				z.v_dc[z.v_dz++] = z.v_w0 + z.v_e0.charAt(0); // Add z.v_w0+z.v_e0[0] to z.v_dc
				//
				z.v_w0 = z.v_e0;
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0.join(""); // <string>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.uz",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.uz",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.uz",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	rpc: function(s0,a0,p) // replace strings (array)
	/** 
	Method <static> <string> ALiBS.sg.rpc(s0,a0,p)

		Description: Replace String

		Parameters:

			s0 = (required)(string)string
			a0 = (required)(array-multi)strings    [[string-from,string-to],[string-from,string-to]]

			p = (optional)(JSON)parameter {}

				op = (string)REGEX options
					"gi" = DEFAULT

		Observations:
	
		Return Value:
			<static> <string> replaced text

		Example:
			var x = ALiBS.sg.rpc("abcdxpto1234",[["a",x],["1","9"]]);

	--------------------------------------------------------------------
	*/
	/*
		ALiBS.sg.rpc("alfredo luiz brasil da silva".toUpperCase(),[["O","0"],["I","1"],["Z","2"],["E","3"],["A","4"],["S","5"],["G","6"],["T","7"],["B","8"],["P","9"]]); 

		ALiBS.sg.rpc("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",[["O","0"],["I","1"],["Z","2"],["E","3"],["A","4"],["S","5"],["G","6"],["T","7"],["B","8"],["P","9"]]); 
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.rpc.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(ALiBS.sy.ty(a0)!="object-Array") throw "ALiBS.sg.rpc.Exception: Error: Invalid Parameter Type: (object-Array) a0 passed as "+ALiBS.sy.ty(a0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"a0="+a0,{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_op = z.op; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_op)!="string") z.p_op = "gi"; // DEFAULT
			//
			ALiBS.sy.la(5+0,"z.p_op="+z.p_op,{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			z.ec = -1201; // 
			if(!s0) // ALiBS.sg.u2s() // undefined
			{
				z.ec = -1201; // 
				if(typeof(s0)=="number")
				{
					z.ec = -1202; // 
					z.v_r0 = "" + s0;
				}
				else
				{
					z.ec = -1203; // 
					z.v_r0 = "";
				}
			}
			else
			{
				z.ec = -1204; // 
				z.v_r0 = "" + s0;
			}
			//
			ALiBS.sy.la(5+0,"z.v_r0="+z.v_r0,{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -2000; // 
			if(!z.v_r0)
			{
				z.ec = -2100; // 
				r = ""; // <string>, NOT <reference> object
			}
			else
			{
				z.ec = -2200; // 
				z.v_r2 = s0;
				//
				z.ec = -2300; // 
				for(var i=0;i<a0.length;i++) // 
				{
					z.ec = -2400; // 
					z.r1 = new RegExp(a0[i][0],z.p_op); // from = a0[i][0]
					ALiBS.sy.la(7+0,"a0["+i+"][0]="+a0[i][0],{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
					//
					z.ec = -2500; // 
					z.v_r2 = z.v_r2.replace(z.r1,a0[i][1]); // to = a0[i][1]
					ALiBS.sy.la(7+0,"a0["+i+"][1]="+a0[i][1],{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
					ALiBS.sy.la(7+0,"z.v_r2="+z.v_r2,{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
				}
				//
				r = "" + z.v_r2; // <string>, NOT <reference> object
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE]
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.rpc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.sg.rpc",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	u2s: function(u0,p) // undefined/null/empty to string
	// u0 = (required)(number)string (undefined/null/empty/0)
	// p = (optional)(JSON)parameters {} 
	//	?
	//
	// return (string)string "" Or ""+u0
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.u2s",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			if(!u0)
			{
				z.ec = -1101; // 
				if(typeof(u0)=="number")
				{
					z.ec = -1102; // 
					r = "" + u0; // <string>, NOT <reference> object
				}
				else
				{
					z.ec = -1103; // 
					r = ""; // <string>, NOT <reference> object
				}
			}
			else
			{
				z.ec = -1104; // 
				r = "" + u0; // <string>, NOT <reference> object
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.u2s",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	ro: function(n,d,p) // round
	// n = (required)(number)number to round
	// d = (required)(number)decimal positions
	// p = (optional)(JSON) {}
	//	y = (number)type
	//		0 = no rounding = DEFAULT
	//		1 = round up
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ro",le:ALiBS.gv.sg.le});
		//
		var z = p || {};
		//
		if(typeof(z.y)!="number") z.y = 0; // type = 0 = DEFAULT
		//
		var r = {}; // JSON object to return
		//
		var d0 = Math.pow(10,d);
		//
		if(z.y==0) // no rounding
		{
			z.x = parseInt(n*d0,10); // TODO(2013-05-02): should use Math.floor() ????
			//
			r.no = (+(z.x/d0)); // <number>, NOT <reference> object
			r.s = "" + r.no; // <string>, NOT <reference> object
			r.r0 = 0 + r.no; // <number>, NOT <reference> object
		}
		else if(z.y==1) // round up
		{
			z.x = Math.round(n*d0);
			//
			r.rd = (+(z.x/d0)); // <number>, NOT <reference> object
			r.s = "" + r.rd; // <string>, NOT <reference> object
			r.r0 = 0 + r.rd; // <number>, NOT <reference> object
		}
		//
		//z.l = (z.x+"").length;
		//
		//j.s = (z.x+"").substring(0,z.l-2) + "." + (z.x+"").substring(z.l-2,z.l);
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	cb: function(s0,p) // combinação / arranjo / permuta
	// s0 = (required)(string)items to combine ("a,b,c,d")
	// p = (optional)(JSON) {}
	//	re = (number)repeat strings in result
	//		0 = DEFAULT = do NOT repeat
	//
	//	sp0 = (string)separator for s0 
	//		"," = DEFAULT
	//
	//	sp1a = (string)separator for values in result strings
	//		"," = DEFAULT
	//	
	//	sp1b = (string)separator for lines in result strings
	//		";" = DEFAULT
	//
	//	rt = (number)return type
	//		0 = DEFAULT = Array of strings
	//		1 = Array.join()
	//		99 = Array.length
	//
	//	z.mx = (number)max number of combinations
	//		1000*1000 = DEFAULT
	//
	//	z.elx = (number)max elements in each line
	//		1000*1000 = DEFAULT
	//
	//	z.eln = (number)min elements in each line
	//		1 = DEFAULT
	//
	// return = (Array) of strings
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.cb",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.re)!="number") z.re = 0; // DEFAULT 
			if(typeof(z.rt)!="number") z.rt = 0; // DEFAULT 
			//
			if(typeof(z.mx)!="number") z.mx = 1000*1000; // DEFAULT 
			if(typeof(z.elx)!="number") z.elx = 1000*1000; // DEFAULT 
			if(typeof(z.eln)!="number") z.eln = 0; // DEFAULT 
			//
			if(typeof(z.sp0)!="string") z.sp0 = ","; // DEFAULT 
			if(typeof(z.sp1a)!="string") z.sp1a = ","; // DEFAULT 
			if(typeof(z.sp1b)!="string") z.sp1b = ";"; // DEFAULT 
			//
			// local variables [BEST-PRACTICE]
			z.ec = -2000; // 
			z.v_a0 = ("xpto"+z.sp0+s0).split(z.sp0);
			//
			z.ec = -2001; // 
			z.v_a1 = new Array();
			//
			z.v_r0 = new Array();
			//
			z.v_cbad = 0;
			z.v_cok = 0;
			z.v_ok = true;
			//
			z.ec = -2100; // 
			for(var i=0;i<z.mx;i++) // 256 na base 4 == 10000 -->> passou de 4 digitos
			{
				z.ec = -2150; // 
				z.v_v0 = ALiBS.c9.ns(i,z.v_a0.length); // z.v_a0.length = base (base4,base5,etc)
				//
				z.v_ok = true;
				//
				// ********
				//
				z.ec = -2250; // 
				if(z.v_v0.length>z.v_a0.length-1) // limit exceeded 
				{
//console.log("(bad)z.v_v0="+z.v_v0);
					z.v_cbad++;
					z.v_ok = false;
					break;
				}
				//
				// ********
				//
				z.ec = -2260; // 
				if(z.v_v0.length>z.elx) // max elements each line
				{
//console.log("(bad)z.v_v0="+z.v_v0);
					z.v_cbad++;
					z.v_ok = false;
				}
				//
				// ********
				//
				z.ec = -2270; // 
				if(z.v_v0.length<z.eln) // min elements each line
				{
//console.log("(bad)z.v_v0="+z.v_v0);
					z.v_cbad++;
					z.v_ok = false;
				}
				//
				// ********
				//
				z.ec = -2200; // 
				if(z.re==0) // do NOT repeat
				{
					z.ec = -2210; // 
					for(var j=0;j<z.v_a0.length;j++)
					{
						z.ec = -2220; // 
						if( ALiBS.sg.no(z.v_v0,""+j)>1 ) // repeated values
						{
//console.log("(bad)z.v_v0="+z.v_v0);
							z.v_cbad++;
							z.v_ok = false;
							break;
						}
					}
				}
				//
				// ********
				//
				if(z.v_ok)
				{
//console.log("(OK)z.v_v0="+z.v_v0);
					z.ec = -2300; // 
					for(var j=0;j<z.v_v0.length;j++)
					{
						z.v_a1[z.v_a1.length] = z.v_a0[(+z.v_v0.charAt(j))];
					}
					//
					z.ec = -2400; // 
					z.a2 = z.v_a1.join(z.sp1a);
					//
					z.ec = -2410; // 
					if(z.a2.indexOf("xpto")>=0)
					{
//console.log("(bad2)z.v_v0="+z.v_v0);
						z.v_cbad++;
					}
					else
					{
						z.ec = -2420; // 
						z.v_r0[z.v_r0.length] = z.a2;
					}
					//
					z.v_a1.length = 0; // reset Array
					//
					z.v_cok++
				}
			}
//console.log("z.v_cbad="+z.v_cbad);
//console.log("z.v_cok="+z.v_cok);
			//
			z.ec = -2500; // 
			if(z.rt==1) // Array.join()
			{
				z.ec = -2510; // 
				r = z.v_r0.join(z.sp1b);
			}
			else if(z.rt==99)
			{
				z.ec = -2520; // 
				r = z.v_r0.length;
			}
			else // DEFAULT = (z.rt==0) OR another value
			{
				r = z.v_r0; // Array
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.cb",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	gn: function(s0,p) // get name = first, middle, last name
	// s0 = (required)(string)string 
	// p = (optional)(JSON)parameters {} 
	//	ty = (number) name type
	//		0 = DEFAULT = first name
	//		1 = middle name
	//		2 = last name
	//
	//	sp = (string) separator
	//		" " = DEFAULT = space
	//
	// return (string)first, middle, last name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.gn",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			if(typeof(z.ty)!="number") z.ty = 0; // DEFAULT 
			if(typeof(z.sp)!="string") z.sp = " "; // DEFAULT 
			//
			z.v_s1 = s0.split(z.sp);
			//
			if(z.ty==1) // middle name
			{
				if(z.v_s1.length>=3)
				{
					z.v_s1.length = z.v_s1.length-1;
					z.v_s1.reverse();
					z.v_s1.length = z.v_s1.length-1;
					z.v_s1.reverse();
					r = z.v_s1.join(" ");
				}
				else
				{
					r = "";
				}
			}
			else if(z.ty==2) // last name
			{
				if(z.v_s1.length>=2)
				{
					r = z.v_s1[z.v_s1.length-1];
				}
				else
				{
					r = "";
				}
			}
			else // 0 or other = first name
			{
				if(z.v_s1.length>=1)
				{
					r = z.v_s1[0];
				}
				else
				{
					r = "";
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.gn",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	t2Dx: function(x0,p) // txt to DOMxml // ALiBStxt2Doc(xmlTxt)
	// x0 = (required)(string)text xml
	// p = (optional)(JSON)parameters {} 
	//
	// return (DOMxml)DOMxml object
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.t2Dx",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			z.ec = -1101; // 
			r = (new DOMParser()).parseFromString(x0, "text/xml");
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.t2Dx",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	Dxev: function(x0,xp,p) // evaluate xpath // ALiBSxpath(xmlDoc,xpath,p)
	// x0 = (required)(DOMxml)DOMxml object (gererated with ALiBS.sg.t2xd)
	// xp = (required)(string)text xpath
	// p = (optional)(JSON)parameters {} 
	//
	//	so = (string)separator
	//		"" or "\n" = \n = DEFAULT
	//		"<br/>" = <br/>
	//
	//	cx = (string)context
	//		xmlDOM.documentElement.getElementsByTagName("modify")
	//
	// return (XML/DOM)dom xml
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.Dxev",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		var r = ""; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		try
		{
			z.ec = -1001; // 
			if(typeof(z.so)!="string") z.so = ""; // DEFAULT 
			//
			z.ec = -1002; // 
			if(typeof(z.cx)!="string") z.cx = ""; // DEFAULT 
			//
			if(!z.cx) // context = ROOT
			{
				z.ec = -1101; // 
				z.nodes = x0.evaluate(xp,x0,null,XPathResult.ANY_TYPE,null);
			}
			else // selected context
			{
				z.ec = -1102; // 
				z.nodes = x0.evaluate(xp,x0.documentElement.getElementsByTagName(z.cx)[0],null,XPathResult.ANY_TYPE,null);
			}
			//
			z.ec = -1110; // 
			z.nr = true; // used to enter in while // result of z.nodes.iterateNext(); // z.nr can be [value] or [false]
			//
			z.ec = -1150; // 
			z.v_r0 = new Array();
			//
			while(z.nr)
			{
				z.ec = -1200; // 
				z.nr = z.nodes.iterateNext();
				//
				if(z.nr)
				{
					z.ec = -1300; // 
					z.r1 = z.nr.childNodes[0].nodeValue;
					//
					if(z.r1)
					{
						z.v_r0[z.v_r0.length] = z.r1;
					}
				}
			}
			//
			z.ec = -2000; // 
			r = z.v_r0.join(z.so);
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.Dxev",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	md0: function(s0,p) // CPF OR digitos do CPF
	/** 
	Method <static> <string> ALiBS.sg.md0(s,p)

		Description: Cálculo de CPF, RG, Título de Eleitor, ECT

		Parameters:

			s0 = (required)(string)CPF

			p = (optional)(JSON)parameter {}

				rt = (number)return type

					0 = DEFAULT = (string) CPF + sp0 + 2 Digitos
					1 = (number) CPF First Digit Only 
					2 = (number) CPF Second Digit Only
					3 = (string) CPF 2 Digits 

					10 = (string) RG + sp0 + 1 Digito
					11 = (number) RG First Digit Only 

					20 = (string) Titulo Eleitor + sp0 + 1 Digito
					21 = (number) Titulo Eleitor First Digit Only 
					22 = (number) Titulo Eleitor Second Digit Only

					30 = (string) Encomendas ECT + sp0 + 1 Digito
					31 = (number) Encomendas ECT First Digit Only 

				sp0 = (string)separator for digits
					"" = DEFAULT

		Observations:
	
		Return Value:
			<static> <string> Cálculo de CPF, RG, Título de Eleitor, ECT

		Example:
			var x = ALiBS.sg.md0("068582188",{rt:0});

	--------------------------------------------------------------------
	*/
	/*
		http://pt.wikipedia.org/wiki/D%C3%ADgito_verificador

			modulo 10 TODO

		http://www.jalucrei.com.br/calculo_dv_cpf_cgc.htm

			CPF: começa com 9 até 0, reverso
				TODO: return false/invalid para sequencias de digitos iguais
			CNPJ: TODO
			TÍTULO ELEITORAL: TODO


			Conta Corrente TODO
			Agência TODO

		http://www.ngmatematica.com/2014/02/como-determinar-o-digito-verificador-do.html

			RG: OK

		http://www.codigodebarrasean.com/calculadora_do_digito_verificador.php

			Calculadora do dígito verificador do seu código de barra UPC TODO
			Calculadora do dígito verificador do seu código de barra EAN TODO
			Calculadora do dígito verificador do seu código SCC-14/DUN-14 TODO

		http://www.guj.com.br/java/27594-calculo-de-digito-verificador---declaracao-de-importacao

			Declaração de importação TODO

		http://pt.wikipedia.org/wiki/EAN-13

			Código de barras EAN-13

		http://ghiorzi.org/DVnew.htm

			VARIOS CALCULOS
			CODIGO RASTREAMENTO CORREIO: TODO
			CARTÃO DE CRÉDITO: TODO

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(!s0 && s0!=0) s0 = ""; // Only if parameter s0 is allowed to be [EMPTY]
			//
			if(typeof(s0)!="string") throw "ALiBS.sg.rt.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.rt",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_sp0 = z.sp0; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_rt = z.rt; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(typeof(z.p_sp0)!="string") z.p_sp0 = ""; // DEFAULT
			if(typeof(z.p_rt)!="number") z.p_rt = 0; // DEFAULT
			//
			ALiBS.sy.la(5+0,"z.p_sp0="+z.p_sp0,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_rt="+z.p_rt,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			//
			// ************************
			//
			if(z.p_rt<9) // CPF = 2 digits
			{
				z.ec = -10001; // 
				z.v_d1 = ALiBS.sg.mod(s0,{ra0:[[10,0]]});
				ALiBS.sy.la(5+0,"z.v_d1="+z.v_d1,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				z.ec = -10002; // 
				z.v_d2 = ALiBS.sg.mod(s0+z.v_d1,{ra0:[[10,0]]});
				ALiBS.sy.la(5+0,"z.v_d2="+z.v_d2,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				if(z.p_rt==1) // 1 = CPF First Digit Only
				{
					r = "" + z.v_d1; // <string>, NOT <reference> object
				}
				else if(z.p_rt==2) // 2 = CPF Second Digit Only
				{
					r = "" + z.v_d2; // <string>, NOT <reference> object
				}
				else if(z.p_rt==3) // 3 = CPF 2 Digits 
				{
					r = "" + z.v_d1 + z.v_d2; // <string>, NOT <reference> object
				}
				else // if(z.p_rt==0) // 0 = DEFAULT = CPF + sp0 + 2 Digitos
				{
					r = "" + s0 + z.p_sp0 + z.v_d1 + z.v_d2; // <string>, NOT <reference> object
				}
			}
			else if(z.p_rt==10 || z.p_rt==11) // RG = 1 digit
			{
				z.ec = -10010; // 
				z.v_d1 = ALiBS.sg.mod(s0,{ra0:[[10,"X"],[11,0]],sb0:11});
				ALiBS.sy.la(5+0,"z.v_d1="+z.v_d1,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				if(z.p_rt==10) // 10 = RG + sp0 + 1 Digito
				{
					r = "" + s0 + z.p_sp0 + z.v_d1; // <string>, NOT <reference> object
				}
				else if(z.p_rt==11) // 11 = RG First Digit Only 
				{
					r = "" + z.v_d1; // <string>, NOT <reference> object
				}
			}
			else if(z.p_rt==20 || z.p_rt==21  || z.p_rt==22) // Titulo Eleitor = 2 digits
			{
				z.ec = -10020; // 
				z.v_d1 = ALiBS.sg.mod(s0.substring(0,s0.length-2),{ra0:[[10,0]]});
				ALiBS.sy.la(5+0,"z.v_d1="+z.v_d1,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				z.ec = -10021; // 
				z.v_d2 = ALiBS.sg.mod(s0.substring(s0.length-2,s0.length)+z.v_d1,{ra0:[[10,0]]});
				ALiBS.sy.la(5+0,"z.v_d2="+z.v_d2,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				if(z.p_rt==20) // 20 = Titulo Eleitor + sp0 + 2 Digitos
				{
					r = "" + s0 + z.p_sp0 + z.v_d1 + z.v_d2; // <string>, NOT <reference> object
				}
				else if(z.p_rt==21) // 21 = Titulo Eleitor First Digit Only 
				{
					r = "" + z.v_d1; // <string>, NOT <reference> object
				}
				else if(z.p_rt==22) // 22 = Titulo Eleitor Second Digit Only 
				{
					r = "" + z.v_d2; // <string>, NOT <reference> object
				}
			}
			else if(z.p_rt==30 || z.p_rt==31) // Encomendas ECT = 1 digit
			{
				z.ec = -10030; // 
				//z.v_d1 = ALiBS.sg.mod(s0,{m00:[8,6,4,2,3,5,9,7],ra0:[[0,5],[1,0],[11,5],[10,0]],sb0:11});
				z.v_d1 = ALiBS.sg.mod(s0,{m00:[8,6,4,2,3,5,9,7],ra0:[[11,5],[10,0]],sb0:11});
				ALiBS.sy.la(5+0,"z.v_d1="+z.v_d1,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				if(z.p_rt==30) // 10 = Encomendas ECT + sp0 + 1 Digito
				{
					r = "" + s0 + z.p_sp0 + z.v_d1; // <string>, NOT <reference> object
				}
				else if(z.p_rt==31) // 11 = Encomendas ECT First Digit Only 
				{
					r = "" + z.v_d1; // <string>, NOT <reference> object
				}
			}
			else if(z.p_rt==100 || z.p_rt==101) // C/C = 1 digit = BB
			{
				z.ec = -10100; // 
				z.v_d1 = ALiBS.sg.mod(s0,{ra0:[[10,"X"],[11,0]]});
				ALiBS.sy.la(5+0,"z.v_d1="+z.v_d1,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
//console.log("z.v_d1="+z.v_d1);
				//
				if(z.p_rt==100) // 100 = C/C + sp0 + 1 Digito
				{
					r = "" + s0 + z.p_sp0 + z.v_d1; // <string>, NOT <reference> object
				}
				else if(z.p_rt==101) // 101 = C/C First Digit Only 
				{
					r = "" + z.v_d1; // <string>, NOT <reference> object
				}
			}
			else if(z.p_rt==200 || z.p_rt==201) // Agência = 1 digit = BB
			{
				z.ec = -10200; // 
				z.v_d1 = ALiBS.sg.mod(s0,{ra0:[[10,"X"],[11,0]]});
				ALiBS.sy.la(5+0,"z.v_d1="+z.v_d1,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
				//
				if(z.p_rt==200) // 100 = Agência + sp0 + 1 Digito
				{
					r = "" + s0 + z.p_sp0 + z.v_d1; // <string>, NOT <reference> object
				}
				else if(z.p_rt==201) // 101 = Agência First Digit Only
				{
					r = "" + z.v_d1; // <string>, NOT <reference> object
				}
			}
			else // 
			{
				//
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.md0",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.md0",le:ALiBS.gv.sg.le});
		//
		return r; 
	},
	// *****************************************************************
	mod: function(s0,p) // 
	/** 
	Method <static> <string> ALiBS.sg.mod(s0,p)

		Description: Digit From Module 10/11 Calculation

		Parameters:

			s0 = (required)(string) digits (cpf, cnpj, etc)

			p = (optional)(JSON)parameter {}
				m00 = (array) multiplicator table
					[0,1,2,3,4,5,6,7,8,9] = DEFAULT

				sb0 = (number) subtraction value
					null = DEFAULT

				mdn = (number) module number
					11 = DEFAULT

				ra0 = (array multi) values to return // [[10,0],[11,1]] = 10 return 0, 11 return 1
					null = DEFAULT

		Observations:
	
		Return Value:
			<static> <string> digit from module 10/11

		Example:
			RG: 
				var x = ALiBS.sg.mod("18798102",{sb0:11});

			CPF:
				var x = ALiBS.sg.mod("068582188",{ra0:[[10,0]]});

	--------------------------------------------------------------------
	*/
	/*
		RG: ALiBS.sg.mod("18798102",{sb0:11})
		cpf: começa com 9 até 0, reverso
		cnpj: 
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(s0)!="string") throw "ALiBS.sg.mod.Exception: Error: Invalid Parameter Type: (string) s0 passed as "+typeof(s0); // 
			if(!ALiBS.sg.id(s0)) throw "ALiBS.sg.mod.Exception: Error: Invalid Parameter Value: s0 is NOT digit"; // 
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			z.p_m00 = z.m00; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_ra0 = z.ra0; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_sb0 = z.sb0; // Parameter Pattern and/or OLD Parameter Compatibility
			z.p_mdn = z.mdn; // Parameter Pattern and/or OLD Parameter Compatibility
			//
			if(ALiBS.sy.ty(z.p_m00)!="object-Array")  z.p_m00 = [0,1,2,3,4,5,6,7,8,9]; 
			if(ALiBS.sy.ty(z.p_ra0)!="object-Array") z.p_ra0 = null; 
			if(typeof(z.p_sb0)!="number") z.p_sb0 = 0; 
			if(typeof(z.p_mdn)!="number") z.p_mdn = 11; 
			//
			ALiBS.sy.la(5+0,"z.p_m00="+z.p_m00,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_ra0="+z.p_ra0,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_sb0="+z.p_sb0,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.p_mdn="+z.p_mdn,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_v0 = s0.split("");
			z.v_sm = 0;
			z.v_a0 = new Array();
			z.v_mp = z.p_m00.length-1; // multiplicator position
			//
			// ************************
			//
			z.ec = -1150; // 
			for(var i=z.v_v0.length-1;i>=0;i--)
			{
				z.ec = -1200; // 
				z.v_s1 = z.v_v0[i]*z.p_m00[z.v_mp];
				ALiBS.sy.la(7+0,"z.v_s1="+z.v_s1,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
				//
				z.v_a0[z.v_a0.length] = "" + z.p_m00[z.v_mp];
				//
				z.ec = -1210; // 
				z.v_sm += z.v_s1;
				ALiBS.sy.la(7+0,"z.v_sm="+z.v_sm+ALiBS.gv.sy.se,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
				//
				z.ec = -1220; // 
				z.v_mp--;
				//
				z.ec = -1230; // 
				if(z.v_mp<0)
				{
					z.v_mp = z.p_m00.length-1; // multiplicator position
				}
			}
			//
			ALiBS.sy.la(5+0,"s0="+s0,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			ALiBS.sy.la(5+0,"z.v_a0="+z.v_a0.reverse().join("")+ALiBS.gv.sy.se,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			//
			ALiBS.sy.la(5+0,"z.v_sm="+z.v_sm,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
			//
			z.ec = -1300; // 
			//
			if(z.p_sb0)
			{
				z.v_r0 = z.p_sb0 - (z.v_sm % z.p_mdn);
			}
			else
			{
				z.v_r0 = z.v_sm % z.p_mdn;
			}
			//
			if(z.p_ra0)
			{
				for(var i =0;i<z.p_ra0.length;i++) // [[10,0],[11,1]]
				{
					if(z.v_r0==z.p_ra0[i][0])
					{
						z.v_r0 = z.p_ra0[i][1];
						break;
					}
					else
					{
						// nothing
					}
				}
			}
			else
			{
				// nothing
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = "" + z.v_r0; // <string>, NOT <reference> object
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.sg.mod",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.sg.mod",le:ALiBS.gv.sg.le});
		//
		return r; 
	}
	// *****************************************************************
	// DEPRECATED: 2015-08-05
	/*li: function(s0,li) // listInside // string contains any item of Array (exact item)
	// s0 = (required)(String)string text
	// li = (required)(String)list = "abcd,1234,xpto"
	// implementar p (JSON) p.n = return occurence n
	//
	// return (number)position in Array OR -1
	{
		var z = {};
		//
		z.v_ll = li.split(",");
		//
		z.v_ok = false; // NOT FOUND
		//
		var r;
		//
		for(var i=0;i<z.v_ll.length;i++)
		{
			if(s0.indexOf(z.v_ll[i])>=0)
			{
				z.v_ok = true; // FOUND
				break;
			}
		}
		//
		r = z.v_ok;
		//
		z = null;
		delete z;
		//
		return r;
	}*/
	// *****************************************************************
	// DEPRECATED: 2015-08-05
	/*wc: function(s) // writeChars // TODO(2010-09-08): sy.hr ainda existe? (Para listar os caracteres, usar ALiBS.c2.sa)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
		//
		for(var i=0;i<s.length;i++)
		{
			if(s.charAt(i)=='\t')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/t"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else if(s.charAt(i)=='\n')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/n"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else if(s.charAt(i)=='\r')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/r"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else if(s.charAt(i)=='a')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/a"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else if(s.charAt(i)=='b')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/b"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else if(s.charAt(i)=='c')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/c"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else if(s.charAt(i)==' ')
			{
				ALiBS.sy.la(5+0,"char["+i+"]=/s"+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
			else 
			{
				ALiBS.sy.la(5+0,"char["+i+"]="+s.charAt(i)+""+ALiBS.gv.sy.se,{o:"ALiBS.sg.wc",le:ALiBS.gv.sg.le});
			}
		}		
	}*/
	// *****************************************************************
};
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_sg_as")>=0)
		{
			ALiBS_sg_as = function(s,x,p)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ALiBS_sg_as",le:ALiBS.gv.sg.le});
				//
				return ALiBS.sg.as(s,x,p);
			};
		}
		//
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_sg_bs")>=0)
		{
			ALiBS_sg_bs = function(s,x,p) // called by novell driver // claypool shell  (do NOT use var)
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sg.ALiBS_sg_bs",le:ALiBS.gv.sg.le});
				//
				return ALiBS.sg.bs(s,x,p);
			};
		}
	}
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.sg("+(++ALiBS.gv.sg.nn)+"x)",{o:"ALiBS.sg"});
ALiBS.sy.la(0+0,"ALiBS.gv.sg.rl="+ALiBS.gv.sg.rl,{o:"ALiBS.sg"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.sg.le="+ALiBS.gv.sg.le+ALiBS.gv.sy.se,{o:"ALiBS.sg"});

