/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

TODO: não executa de dentro de um for(), somente usando ALiBS.ag.tc

TODO: ldifGenerator no firefox vai até 99% e nao 100%

*/
//
// *************************************************************************
//
if(!ALiBS.gv.gg) ALiBS.gv.gg = {}; // global values
//
if(!ALiBS.gv.gg.le) ALiBS.gv.gg.le = 0; // local log status
//
ALiBS.gv.gg.nn = 0; // x times loaded
//
ALiBS.gv.gg.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.gg = // gauge
{
	g0: function(v,p) // 
	// v = (required)(number)current ocurrence number (if v>100 then pass {m:max})
	// p = (optional)(JSON)parameters {c0:".cGauge01a",c1:".cGauge01b",m:200}
	//	c0 = (string)class c0 (done)
	//	c1 = (string)class c1 (to-do)
	//	m = (number)max occurences (if v!=100)
	// return (String)html gauge
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.gg.g0",le:ALiBS.gv.gg.le});
		//
		var c0 = ""; 
		var c1 = "";
		var m = 100;
		var s = new Array();
		//
		if(p) // TODO: fazer assim em todas as classes, igual ao ALiBS.to
		{
			if(p.c0 && typeof(p.c0)=="string") c0 = p.c0;
			if(p.c1 && typeof(p.c1)=="string") c1 = p.c1;
			if(p.m && typeof(p.m)=="number") m = p.m;
		}
		//
		v = Math.floor(v*100/m);
		//
		s[s.length] = '<span class="' + c1 + '">' + v + '% </span>';
		//
		if(v<=100)
		{
			s[s.length] = '<span class="' + c0 + '">';
			//
			for(var i=1;i<=v;i++)
			{
				s[s.length] = "_";
			}
			//
			s[s.length] =  (i-1) + '%';
			//
			s[s.length] = '</span>';
		}
		//
		if(v<100)
		{
			s[s.length] = '<span class="' +c1+ '">';
			//
			for(var i=v+1;i<100;i++)
			{
				s[s.length] = "_";
			}
			//
			s[s.length] = '</span>';
		}
		//
		return s.join("");
	},
	// *****************************************************************
	g1: function(v,p) // 
	// v = (required)(number)current ocurrence number (if v>100 then pass {m:max})
	// p = (optional)(JSON)parameters {c0:".cGauge01a",c1:".cGauge01b",m:200}
	//	c0 = (string)class c0 (done)
	//	c1 = (string)class c1 (to-do)
	//	m = (number)max occurences (if v!=100)
	// return (String)html gauge
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.gg.g1",le:ALiBS.gv.gg.le});
		//
		var c0 = ""; 
		var c1 = "";
		var m = 100;
		var s = new Array();
		//
		if(p) // TODO: fazer assim em todas as classes, igual ao ALiBS.to
		{
			if(p.c0 && typeof(p.c0)=="string") c0 = p.c0;
			if(p.c1 && typeof(p.c1)=="string") c1 = p.c1;
			if(p.m && typeof(p.m)=="number") m = p.m;
		}
		//
		v = Math.floor(v*100/m);
		//
		s[s.length] = '<span class="' + c1 + '">' + v + '% </span>';
		//
		if(v<=100)
		{
			s[s.length] = '<span class="' + c0 + '">';
			//
			for(var i=0;i<=v;i++)
			{
				s[s.length] = "" + i%10;
			}
			//
			s[s.length] = '</span>';
		}
		//
		if(v<100)
		{
			s[s.length] = '<span class="' +c1+ '">';
			//
			for(var i=v+1;i<=100;i++)
			{
				s[s.length] = "_";
			}
			//
			s[s.length] = '</span>';
		}
		//
		return s.join("");
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.gg("+(++ALiBS.gv.gg.nn)+"x)",{o:"ALiBS.gg"});
ALiBS.sy.la(0+0,"ALiBS.gv.gg.le="+ALiBS.gv.gg.le+ALiBS.gv.sy.se,{o:"ALiBS.gg"});

