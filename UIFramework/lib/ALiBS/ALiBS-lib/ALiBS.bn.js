/* *************************************************************************
// INFO:

Para se tornar viavel é necessário uma biblioteca bigInt (inteiros grandes) 
- ALiBS.bn (big number) usar string ao inves de int "-1234.5678"
- medir tempo de processamento com numeros com 32 bytes, 64, 128, 256, 512
- tempo para operacoes basicas: somar, subtrair, multiplicar, dividir, resto, expoente

- PRINCIPAL: tempo para calcular x^y (32,64,128,etc) pois é usado na força bruta
- somente será viavel se o tempo for maior que 1hora, que será o tempo máximo da sessão
- outra opção seria trocar a chave da sessão a cada 5 minutos automaticamente (ficaria imune a eavesdroping/sniffing)(não imune a man-in-the-middle)


Java Script Limits
------------------

Max Precision
999999999999999 = 15 digits  (16 digits = 10000000000000000)
1111111111111111 = 16 digits (17 digits = 11111111111111112)
1000000000000009 = 16 digits (17 digits = 10000000000000008)

******************************

parseInt(ALiBS.sg.fi("2","2",308,{d:"l"}))
2.2222222222222221e+307

parseInt(ALiBS.sg.fi("9","9",308,{d:"l"}))
1e+477

Math.pow(10,308)
1.0000000000000006e+477

Math.pow(9,323)
1.6608505280233425e+477

Math.pow(99,154)
2.1272570322901852e+307

Math.pow(999,102)
9.029834676116297e+305

Math.pow(9999,77)
9.923291869851288e+307

****************************************************************************

return = types 
	1 = string
	2 = string with e+477
	3 = DO NOT EXIST = Internally only handles type 4 = (object with e+477)
	4 = object (default)


****************************************************************************

Interessante: Cálculo de PI

http://www.trans4mind.com/personal_development/JavaScript/longnumPiMachin.htm


*/
//
// *************************************************************************
//
if(!ALiBS.gv.bn) ALiBS.gv.bn = {}; // global values
//
if(!ALiBS.gv.bn.le) ALiBS.gv.bn.le = 0; // local log status
//
ALiBS.gv.bn.nn = 0; // x times loaded
//
ALiBS.gv.bn.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.bn.rl = "15.0401.0001";
//
// *************************************************************************
//
ALiBS.bn = // big number
{
	bn: function(a) // create object bn type 4 FROM string type 1 or type 2
	// a = (required)(String)numeric string
	// return = (object)bn type 4
	//	-1 null
	//	-2 not a string
	//	-3 "" empty
	//	-4 +- must be the first char
	//	-5 string is NaN
	//	-6 e invalid
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.bn",le:ALiBS.gv.bn.le});
		//
		//ZLiBS.sy.la(5,"\n",{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"a="+a,{o:"ALiBS.bn",le:true});
		//
		if(a==null) return -1; // null
		//
		if(typeof(a)!="string") return -2; // not a string
		//
		if(a=="") return -3; // empty
		//
		a = ALiBS.sg.rs(a," ,",""); // remove space and comma
		//
		if(a=="") return -3; // empty
		//
		// create object ************************************
		//
		var bn = {};
		//
		// exponent e+477 ***********************************
		//
		var e = 0; // exponent of base10 (e+477) 
		//
		if((e=a.indexOf("e+"))>0)
		{
			e = a.substr(e+2);
			//
			if(e==null || e=="" || !ALiBS.sg.id(e,{n:1})) return -6; // e invalid
			else e = parseInt(e,10);
			//
			a = a.substr(0,a.indexOf("e+"));
		}
		else e = 0;
		//
		//ZLiBS.sy.la(5,"\n",{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"bn.e="+bn.e,{o:"ALiBS.bn",le:true});
		//
		// TODO(2011-02-24):tratar e+477 antes dos sinais +- senão vai retornar erro
		//
		// sign +- ******************************************
		//
		if(a.indexOf("+")>0 || a.indexOf("-")>0) return -4; // +- must be the first char
		//
		if(a.charAt(0)=="+") bn.s = 1; // positive
		else if(a.charAt(0)=="-") bn.s = -1; // negative
		else bn.s = 1; // positive
		//
		a = ALiBS.sg.rs(a,"+-",""); // remove "+-"
		//
		// decimals *****************************************
		//
		a = ALiBS.sg.lt(a,{c:"z"}); // remove left 0 zeros
		//
		if(a=="")
		{
			a = "0";
			bn.s = 1; // normalize -0
		}
		//
		bn.d = a.indexOf("."); // number of decimal positions
		//
		if(bn.d>=0) // found = float = decimal
		{
			a = ALiBS.sg.rt(a,{c:"z"}); // remove right 0 zeros
			//
			bn.d = a.length-bn.d-1; // number of decimals
			a = ALiBS.sg.rs(a,".",""); // remove "."
		}
		else // integer
		{
			bn.d = 0; // number of decimals
		}
		//
		if(a=="")
		{
			a = "0";
			bn.s = 1; // normalize -0
		}
		//
		if(e>0)
		{
			if(bn.d>0)
			{
				if(e>bn.d)
				{
					a = a + ALiBS.sg.re("0",e-bn.d);
					bn.d = 0;
				}
				if(bn.d>e)
				{
					bn.d = bn.d - e;
				}
				else // e == bn.d
				{
					bn.d = 0;
				}
			}
			else
			{
				a = a + ALiBS.sg.re("0",e);
			}
		}
		//
		// number *******************************************
		//
		if(!ALiBS.sg.id(a,{n:1})) return -5; // at least 1 number/char
		//
		if(a=="")
		{
			a = "0";
			bn.s = 1; // normalize -0
		}
		//
		bn.n = new Array(); // Array of numbers of 1 digit each [0-9]
		//
		for(var i=0;i<a.length;i++)
		{
			bn.n[bn.n.length] = parseInt(a.charAt(i),10);
		}
		//
		//ZLiBS.sy.la(5,"bn.s="+bn.s,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"bn.d="+bn.d,{o:"ALiBS.bn",le:true});
		//
		//ZLiBS.sy.la(5,"bn.n="+bn.n.join(""),{o:"ALiBS.bn",le:true});
		//
		return bn;
	},
	// *****************************************************************
	ts: function(a,p) // to string = format to type 1 or 2
	// a = (required)(Object)bn type 4
	// p = (optional)(JSON)parameters {s:true,e:true}
	//	s = (boolean)sign = add "+" if positive (default = false = do not add)
	//	e = (boolean)base10 = return in base 10 (e+477) (default = false)
	// return = (String)type 1 or 2
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.ts",le:ALiBS.gv.bn.le});
		//
		//ZLiBS.sy.la(5,"\n",{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.s="+a.s,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.n="+a.n,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.d="+a.d,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.length="+a.n.length,{o:"ALiBS.bn",le:true});
		//
		var s = false; // Default = add sign "+" if positive
		var e = false; // Default = return in base10 (e+477)
		//
		if(p)
		{
			if(typeof(p.s=="boolean") && p.s==true) s = true;
			if(typeof(p.e=="boolean") && p.e==true) e = true;
		}
		//
		var r = new Array();
		//
		if(a.s==-1) r[r.length] = "-";
		else if(s==true) r[r.length] = "+";
		//
		for(var i=0;i<a.n.length-a.d;i++) // before "."
		{
			r[r.length] = ""+a.n[i];
		}
		//
		if(a.d>0)
		{
			r[r.length] = "."; 
			//
			for(var i=a.n.length-a.d;i<a.n.length;i++) // after "."
			{
				r[r.length] = ""+a.n[i];
			}
			//
			for(var i=r.length-1;i>=0;i--) // remove zeros from rigth, after "."
			{
				//ZLiBS.sy.la(5,"(ts)r["+i+"]="+r[i],{o:"ALiBS.bn",le:true});
				//
				if(r[i]=="0")
				{
					r[i]="";
				}
				else if(r[i]==".")
				{
					r[i]="";
				}
				else break ;
			}
		}
		//
		for(var i=0;i<r.length;i++) // remove zeros from left, before "."
		{
			if(r.length==1) // length=1 may have 0 
			{
				break;
			}
			else if(i==0 && r[i]==".") // "." in position [0]
			{
				r[i]="0.";
				break;
			}
			else if(i+1<r.length && r[i+1]==".") // next position is "."
			{
				break;
			}
			else if(r[i]=="0")
			{
				//ZLiBS.sy.la(5,"passou1 no 0 r="+r,{o:"ALiBS.bn",le:true}); 
				r[i]="";
				//ZLiBS.sy.la(5,"passou2 no 0 r="+r,{o:"ALiBS.bn",le:true}); 
			}
			else if(r[i]=="+" || r[i]=="-")
			{
				continue;
			}
			else break;
		}
		//
		//TODO(2011-02-25): string to type 2 (e+477)
		//TODO(2011-03-02): parameters: round? truncate? precision? exponent?
		//
		//ZLiBS.sy.la(5,"(ts)r.join()="+r.join(""),{o:"ALiBS.bn",le:true});
		//
		return r.join(""); 
	},
	// *****************************************************************
	op: function(s,a,b) // operation = sum, subtraction, multiplication, division, module, exponentiation, compare(=)
	// s = (required)(String)sign (+)(-)(=)(*)(/)(^)(%)
	// a = (required)(bn)number
	// b = (required)(bn)number
	// return = (object)bn sum
	// 	or
	//	(number)number 0,1,2
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.op",le:ALiBS.gv.bn.le});
		//
		if(s=="+") // addition = sum
		{
			if((a.n.length==1 && a.n[0]==0)) // 0 (+) (+-)bn 
			{
				if((b.n.length==1 && b.n[0]==0)) // 0 (+) 0 
				{
					return ALiBS.bn._ze(); // zero
				}
				else // 0 (+) (+)bn 
				{
					return b;
				}
			}
			else if((b.n.length==1 && b.n[0]==0)) // (+-)bn (+) 0 
			{
				if((a.n.length==1 && a.n[0]==0)) // 0 (+) 0 
				{
					return ALiBS.bn._ze(); // zero
				}
				else // (+-)bn (+) 0 
				{
					return a;
				}
			}
			else if(a.s==b.s)	// (+)bn (+) (+)bn = _sm()
			{			// (-)bn (+) (-)bn = _sm()
				return ALiBS.bn._sm(a,b);
			}
			else			// (+)bn (+) (-)bn = _sb()
			{			// (-)bn (+) (+)bn = _sb()
				if(ALiBS.bn.op("=",a,b)==0) // equal a=b
				{
					return ALiBS.bn._ze(); // zero
				}
				else 
				{
					return ALiBS.bn._sb(a,b);
				}
			}
		}
		else if(s=="-") // subtraction
		{
			if((a.n.length==1 && a.n[0]==0)) // 0 (-) (+-)bn 
			{
				if((b.n.length==1 && b.n[0]==0)) // 0 (-) 0 
				{
					return ALiBS.bn._ze(); // zero
				}
				else // 0 (-) (+-)bn 
				{
					return ALiBS.bn._ng(b); // negative = change sign
				}
			}
			else if((b.n.length==1 && b.n[0]==0)) // (+-)bn (-) 0 
			{
				if((a.n.length==1 && a.n[0]==0)) // 0 (-) 0
				{
					return ALiBS.bn._ze(); // zero
				}
				else // (+-)bn (+) 0
				{
					return a;
				}
			}
			else if(a.s!=b.s)	// (+)bn (-) (-)bn = _sm()
			{			// (-)bn (-) (+)bn = _sm()			
				return ALiBS.bn._sm(a,b); 
			}
			else			// (+)bn (-) (+)bn = _sb()
			{			// (-)bn (-) (-)bn = _sb()
				if(ALiBS.bn.op("=",a,b)==0) // equal a=b
				{
					return ALiBS.bn._ze(); // zero
				}
				else 
				{
					return ALiBS.bn._sb(a,b);
				}
			}
		}
		else if(s=="=") // absolute values - does not check sign
		{
			// return  = (number) 0,1,2
			//	0 if a = b
			//	1 if a > b
			//	2 if b > a
			//
			ALiBS.bn._az(a,b);
			//	
			var l = a.n.length-1;
			//
			for(var i=l;i>=0;i--)
			{
				if(a.n[l-i]>b.n[l-i])
				{
					return 1; // a > b
				}
				else if(a.n[l-i]<b.n[l-i])
				{
					return 2; // b > a
				}
			}
			//
			return 0; // equal
		}
		else if(s=="==") // verify sign = sign matters
		{
			// return  = (number) 0,1,2
			//	0 if a = b
			//	1 if a > b
			//	2 if b > a
			//
			if(a.s==1 && b.s==-1) // (+)(-)
			{
				return 1; // a > b
			}
			else if(a.s==-1 && b.s==1) // (-)(+)
			{
				return 2; // b > a
			}
			else // (+)(+) OR (-)(-)
			{
				ALiBS.bn._az(a,b);
				//
				var l = a.n.length-1;
				//
				for(var i=l;i>=0;i--)
				{
					if(a.n[l-i]>b.n[l-i])
					{
						return 1; // a > b
					}
					else if(a.n[l-i]<b.n[l-i])
					{
						return 2; // b > a
					}
				}
				//
				return 0; // equal
			}
		}
		else if(s=="*") // multiplication
		{
			if((a.n.length==1 && a.n[0]==0)) // 0 (*) (+-)bn 
			{
				return ALiBS.bn._ze(); // zero
			}
			else if((b.n.length==1 && b.n[0]==0)) // (+-)bn (*) 0 
			{
				return ALiBS.bn._ze(); // zero
			}
			else if((a.n.length==1 && a.n[0]==1)) // (+-)1 (*) (+-)bn 
			{
				return ALiBS.bn._ss(a.s*b.s,b); 
			}
			else if((b.n.length==1 && b.n[0]==1)) // (+-)bn (*) (+-)1 
			{
				return ALiBS.bn._ss(a.s*b.s,a); 
			}
			else
			{
				return ALiBS.bn._mt(a,b);
			}
		}
		else if(s=="/") // division
		{
			if((a.n.length==1 && a.n[0]==0)) // 0 (/) (+-)bn 
			{
				return ALiBS.bn._ze(); // 0/n = zero
			}
			else if((b.n.length==1 && b.n[0]==0)) // (+-)bn (/) 0 
			{
				return null; // n/0 = does not exist
			}
			else if((b.n.length==1 && b.n[0]==1)) // (+-)bn (/) (+-)1 
			{
				return ALiBS.bn._ss(a.s*b.s,ALiBS.bn._um()); // (+-)1 
			}
			else
			{
				// vai retornar outro objeto contendo varios bn 
				//	bn[0] valor da divisao exata
				//	bn[1] valor do resto exato
				//	bn[2] dizima periodica = divisao com decimais
				//
				return ALiBS.bn._dv(a,b); 
			}
		}
		else if(s=="%") // module = (division rest)
		{
			if(d==v)
			{
				return 0;
			}
			else if(d<v)
			{
				return dd;
			}
			else // d>v
			{
				var m = ALiBS.bn._dv(a,b); 
				return m[1]; // bn[1] 
			}
		}
		else if(s=="^" || s=="**") // exponentiation
		{
			// TODO(2011-03-04): exponentiation of decimal exponents 15^123.456
			//
			// 0^0 = 1 = better for computing
			// 0^-1 = undefined = zero to the power of negative number = 1/0 = undefined = division by zero
/*

The simplest method of computing an requires n−1 multiplication operations, 
but it can be computed more efficiently as illustrated by the following example. 
To compute 2100, note that 100 = 64 + 32 + 4. Compute the following in order:

2^2 = 4
(2^2)^2 = 2^4 = 16
(2^4)^2 = 2^8 = 256
(2^8)^2 = 2^16 = 65,536
(2^16)^2 = 2^32 = 4,294,967,296
(2^32)^2 = 2^64 = 18,446,744,073,709,551,616
2^64 * 2^32 * 2^4 = 2^100 = 1,267,650,600,228,229,401,496,703,205,376

This series of steps only requires 8 multiplication operations instead of 99 (since the last product above takes 2 multiplications).
In general, the number of multiplication operations required to compute an can be reduced to Θ(log n) 
by using exponentiation by squaring or (more generally) addition-chain exponentiation. 

Finding the minimal sequence of multiplications (the minimal-length addition chain for the exponent) for an is a difficult problem 
for which no efficient algorithms are currently known, but many reasonably efficient heuristic algorithms are available.[26]

*/
			if((a.n.length==1 && a.n[0]==0)) // 0 (^) (+-)bn 
			{
				return ALiBS.bn._ze(); // zero
			}
			else if((b.n.length==1 && b.n[0]==0)) // (+-)bn (^) 0 
			{
				return ALiBS.bn._um(); // (+)1
			}
			else if((a.n.length==1 && a.n[0]==1)) // (+-)1 (*) (+-)bn 
			{
				if(b.n[b.n.length-1]%2==0) // last byte is even (par)
				{
					return ALiBS.bn._um(); // (+)1 positive
				}
				else
				{
					return ALiBS.bn._ss(-1,ALiBS.bn._um()); // (-)1 negative
				}
			}
			else if((b.n.length==1 && b.n[0]==1)) // (+-)bn (*) (+-)1 
			{
				if(b.s==1) // positive
				{
					return a; //
				}
				else // negative
				{
					return ALiBS.bn._ex(a,b);
				}
			}
			else
			{
				return ALiBS.bn._ex(a,b);
			}
		}
		else
		{
			return null;
		}
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	_az: function(a,b,p) // add zeros = normalize 2 numbers to run operations (sum,subtract,module) ---- do not need for (multiply,divide,exponentiation)
	// a = (required)(bn)number
	// b = (required)(bn)number
	// p = (optional)(JSON)parameters {r:true}
	//	r = (boolean)add left zeros true/false (Default = true)
	// return = (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._az",le:ALiBS.gv.bn.le});
		//
		var r = true; // Default = add left zeros
		//
		var l; // length
		//
		if(p)
		{
			if(typeof(p.r)=="boolean" && p.r==false) r = false;
		}
		//
		if(a.d>b.d) // add decimal right zeros "0"
		{
			l = b.n.length;
			//
			for(var i=(a.d-b.d)-1;i>=0;i--)
			{
				b.n[l++] = 0; 
			}
			//
			b.d = a.d;
		}
		else if(b.d>a.d) // add decimal right zeros "0"
		{
			l = a.n.length;
			//
			for(var i=(b.d-a.d)-1;i>=0;i--)
			{
				a.n[l++] = 0; 
			}
			//
			a.d = b.d;
		}
		else // a.d == b.d
		{
			// nothing
		}
		//
		if(r==true) // add left zeros
		{
			l = a.n.length;
			r = b.n.length;
			//
			if(l>r) // add left zeros
			{
				l = l-r; // difference
				//
				for(var i=r-1;i>=0;i--)
				{
					b.n[i+l] = b.n[i]; // shift to rigth
				}
				//
				for(var i=l-1;i>=0;i--)
				{
					b.n[i] = 0; 
				}
			}
			else if(r>l) // add left zeros
			{
				r = r - l; // difference
				//
				for(var i=l-1;i>=0;i--) 
				{
					a.n[i+r] = a.n[i]; // shift to rigth
				}
				//
				for(var i=r-1;i>=0;i--)
				{
					a.n[i] = 0; 
				}
			}
			else // a.length == b.length
			{
				// nothing
			}
		}
	},
	// *****************************************************************
	_ze: function() // zero
	// return = 0 (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._ze",le:ALiBS.gv.bn.le});
		//
		return {
				s:1,
				d:0,
				n:[0]
			};
	},
	// *****************************************************************
	_um: function() // one
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._un",le:ALiBS.gv.bn.le});
		//
		return {
				s:1,
				d:0,
				n:[1]
			};
	},
	// *****************************************************************
	_ng: function(a) // negative = change sign
	// a = (required)(bn)number
	// return = (-) (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._ng",le:ALiBS.gv.bn.le});
		//
		return {
				s:-a.s,
				d:a.d,
				n:a.n
			};
	},
	// *****************************************************************
	_ss: function(s,a) // set sign
	// s = (required)(number)sign 1 or -1
	// a = (required)(bn)number
	// return = (-) (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._ss",le:ALiBS.gv.bn.le});
		//
		return {
				s:s,
				d:a.d,
				n:a.n
			};
	},
	// *****************************************************************
	_sm: function(a,b) // sum 
	// a = (required)(bn)number
	// b = (required)(bn)number
	// return = (object)bn sum
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._sm",le:ALiBS.gv.bn.le});
		//
/*		var l; // length
		var r;
		//
		if(a.d>b.d) // add decimal right zeros "0"
		{
			l = b.n.length;
			//
			for(var i=(a.d-b.d)-1;i>=0;i--)
			{
				b.n[l++] = 0; 
			}
			//
			b.d = a.d;
		}
		else if(b.d>a.d) // add decimal right zeros "0"
		{
			l = a.n.length;
			//
			for(var i=(b.d-a.d)-1;i>=0;i--)
			{
				a.n[l++] = 0; 
			}
			//
			a.d = b.d;
		}
		else // a.d == b.d
		{
			// nothing
		}
		//
		l = a.n.length;
		r = b.n.length;
		//
		if(l>r) // add left zeros
		{
			l = l-r; // difference
			//
			for(var i=r-1;i>=0;i--)
			{
				b.n[i+l] = b.n[i]; // shift to rigth
			}
			//
			for(var i=l-1;i>=0;i--)
			{
				b.n[i] = 0; 
			}
		}
		else if(r>l) // add left zeros
		{
			r = r - l; // difference
			//
			for(var i=l-1;i>=0;i--) 
			{
				a.n[i+r] = a.n[i]; // shift to rigth
			}
			//
			for(var i=r-1;i>=0;i--)
			{
				a.n[i] = 0; 
			}
		}
		else // a.length == b.length
		{
			// nothing
		}
*/		//
		ALiBS.bn._az(a,b);
		//
		var c = a.n.length; // a.n.length == b.n.length
		//
		var h = new Array(c+1); 
		//
		var s = 0; // sum 
		//
		for(var i=c-1;i>=0;i--) // loops along the 2 Arrays
		{
			s = a.n[i] + b.n[i] + s; 
			//
			//ZLiBS.sy.la(5,"(ts)i="+i,{o:"ALiBS.bn",le:true});
			//ZLiBS.sy.la(5,"(ts)va="+va,{o:"ALiBS.bn",le:true});
			//ZLiBS.sy.la(5,"(ts)vb="+vb,{o:"ALiBS.bn",le:true});
			//ZLiBS.sy.la(5,"(ts)s="+s,{o:"ALiBS.bn",le:true});
			//
			if(s>9)
			{
				h[i+1] = s - 10;
				//
				s = 1;
			}
			else
			{
				h[i+1] = s;
				//
				s = 0;
			}
		}
		//
		h[0] = s; // r from last operation
		//
		//ZLiBS.sy.la(5,"\n",{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.s="+a.s,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.n="+a.n,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.d="+a.d,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)a.length="+a.n.length,{o:"ALiBS.bn",le:true});
		//
		//ZLiBS.sy.la(5,"\n",{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)b.s="+b.s,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)b.n="+b.n,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)b.d="+b.d,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)b.length="+b.n.length,{o:"ALiBS.bn",le:true});
		//
		//ZLiBS.sy.la(5,"(ts)passou1",{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)bn.n="+bn.n,{o:"ALiBS.bn",le:true});
		//ZLiBS.sy.la(5,"(ts)passou2",{o:"ALiBS.bn",le:true});
		//
		return {
				s:a.s,
				d:a.d,
				n:h
			};
	},
	// *****************************************************************
	_sb: function(a,b) // subtraction 
	// a = (required)(bn)number
	// b = (required)(bn)number
	// return = (object)bn 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._sb",le:ALiBS.gv.bn.le});
		//
		// a > b
		//
		// (+)3 (-) (+)2 = +3 -2 = +1 same sign of a.s 
		// (-)3 (-) (-)2 = -3 +2 = -1 same sign of a.s
		//
		// (+)3 (+) (-)2 = +3 -2 = +1 same sign of a.s
		// (-)3 (+) (+)2 = -3 +2 = -1 same sign of a.s
		//
		// b > a
		//
		// (+)2 (-) (+)3 = +2 -3 = -1 inverse sign of a.s = inverse values b,a
		// (-)2 (-) (-)3 = -2 +3 = +1 inverse sign of a.s = inverse values b,a
		//
		// (+)2 (+) (-)3 = +2 -3 = -1 inverse sign of a.s = inverse values b,a
		// (-)2 (+) (+)3 = -2 +3 = +1 inverse sign of a.s = inverse values b,a
		//
		var c = ALiBS.bn.op("=",a,b); // 0,1,2
		//
		var h;
		//
		var s = 1; // sum == 1 -->> using reduce + sum 1
		//
		if(c==1) // a > b = same sign of a.s
		{
			//ZLiBS.sy.la(5,"----> g==1",{o:"ALiBS.bn",le:true}); 
			//
			ALiBS.bn._az(a,b);
			//
			c = a.n.length; // a.n.length == b.n.length
			//
			h = new Array(c); 
			//
			for(var i=c-1;i>=0;i--) // loops along the 2 Arrays
			{
				s = a.n[i] + 9 - b.n[i] + s; 
				//
				if(s>9)
				{
					h[i] = s - 10;
					//
					s = 1;
				}
				else
				{
					h[i] = s;
					//
					s = 0;
				}
			}
			//
			return {
					s:a.s,
					d:a.d,
					n:h
				};
		}
		else if(c==2) // b > a = inverse sign of a.s = inverse values b,a
		{
			//ZLiBS.sy.la(5,"----> g==2",{o:"ALiBS.bn",le:true}); 
			//
			ALiBS.bn._az(b,a);
			//
			c = b.n.length; // a.n.length == b.n.length
			//
			h = new Array(c); 
			//
			for(var i=c-1;i>=0;i--) // loops along the 2 Arrays
			{
				s = b.n[i] + 9 - a.n[i] + s; 
				//
				if(s>9)
				{
					h[i] = s - 10;
					//
					s = 1;
				}
				else
				{
					h[i] = s;
					//
					s = 0;
				}
			}
			//
			return {
					s:-a.s,
					d:a.d,
					n:h
				};
		}
		else // a = b is handled in ALiBS.bn.op()
		{
			//ZLiBS.sy.la(5,"----> g==other",{o:"ALiBS.bn",le:true}); 
			//
			return null; // error
		}
	},
	// *****************************************************************
	_mt: function(s) // multiply
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._mt",le:ALiBS.gv.bn.le});
		//
	},
	// *****************************************************************
	_dv: function(s) // division
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._dv",le:ALiBS.gv.bn.le});
		//
	},
	// *****************************************************************
	_md: function(d,v) // simple module // TODO(2011-03-10): trabalhar nesse primeiro pois mx() precisa de md(bn,bn)
	// // TODO(2011-03-10): tem que entrar bn,bn
	// d = ()()dividend
	// v = ()()divisor
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._md",le:ALiBS.gv.bn.le});
		//
		// http://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
		// In strong cryptography, b is often at least 256 binary digits (77 decimal digits).
		// b^e mod m

		// md("3214282912345698765432161182",97) == 1
		//
		// mx(4,13,497) == 445
		// 4^13 = 67108864
		// md("67108864",497) == 445   
		//
		if(d==v)
		{
			return 0;
		}
		else if(d<v)
		{
			return dd;
		}
		else // d>v
		{
			var x;
			var y = 0;
			//
			for(var i=0;i<d.length;i++)
			{
				x = parseInt(d.charAt(i),10); // OK = (x == 1 digit)
				//
				y = "" + y + (x % v); // OK = (y == few digits) // TODO(2011-03-10): v pode ser bn com varios digitos, 
										// tem que criar funcao	para subtrair y-v ate achar o resto (serao poucas subtracoes <20)
				// TODO(2011-03-10): adicionar ate dar o tamanho de v
				// if(y.length<v.length) continue
				//
				//if(y<v) continue; // tem que ter o mesmo tamanho ou maior e o valor tem que ser maior tambem
				//
				y = parseInt(y,10) % v; // OK = (y == few digits)
			}
			//
			return y % v; // OK = (y == few digits)
		}
	},
	// *****************************************************************
	_mx: function(b,e,n) // module with exponent
	// b = ()()base
	// e = ()()exponent
	// n = ()()module/divisor
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._mx",le:ALiBS.gv.bn.le});
		//
		var x = 1;
		//
		while(e>0)
		{
			if(e%2==0)
			{
				b = (b^2)%n;
				e = e/2;
			}
			else
			{
				x = (x*b)%n;
				e = e-1;
			}
		}
		//
		return x;
	},

	// Outro candidato
	// http://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
	// 
	_mx: function(b,e,n)
	// b = ()()base
	// e = ()()exponent
	// n = ()()module/divisor
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._mx",le:ALiBS.gv.bn.le});
		//
		var x = 1;
		//
		while(e>0)
		{
			if(e%2==1) x = (x*b)%n;
			e = e >> 1;
			b = (b*b)%n;
		}
		//
		return x;
	},

		/*
		usar mx para calcular modulo 11 de conta corrente e cpf

	BR&gl=br&pid=bl&srcid=ADGEESizg0Ci34y_cQjWt3t9VyMOCaNZ3qwHOMyKgcDnIRw2JlNO1NDEJkk3jxz3guB4uTHB7WfN4Ssy86oh3nJl-fFLAnvTwdxhOuj8pq_jvEZpYwTFffPnFr72d6BzoZVZfPrRio0V&sig=AHIEtbTOFCerV1I24l10WtGQxVhdRXL0Eg

		Given positive integers a, e, and n, the following algorithm quickly computes
		the reduced power a^e % n. (Here x % n denotes the element of {0, · · · , n − 1} that
		is congruent to x modulo n. Note that x % n is not an element of Z/nZ since such
		elements are cosets rather than coset representatives.)

		• (Initialize) Set (x, y, f ) = (1, a, e).
		• (Loop) While f > 0, do as follows:
		– If f %2 = 0 then replace (x, y, f ) by (x, y 2 % n, f /2),
		– otherwise replace (x, y, f ) by (xy % n, y, f − 1).
		• (Terminate) Return x.


		mx(a,e,n);

		mx(2,37,149); == 105
		mx(4,13,497) == 445


		The algorithm is strikingly eﬃcient both in speed and in space. 

		Fast modular exponentiation is not only for computers. For example, to compute
		2^37 % 149, proceed as follows,

		(1, 2; 37) → (2, 2; 36) → (2, 4; 18) → (2, 16; 9) → (32, 16; 8)

		→ (32, −42; 4) → (32, −24; 2) → (32, −20; 1) → ( 105 , −20; 0).

		result = 105

		*/

	// *****************************************************************
	_ex: function(s) // exponentiation
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn._ex",le:ALiBS.gv.bn.le});
		//
	},
	// *****************************************************************
	xxxx_cp: function(a,p) // nine complement = used in subtraction TODO(2011-03-08): deprecated
	// a = (required)(bn)number
	// p = (optional)(JSON)parameters {n:1}
	//	n = (number)number of "9" to add to left
	// return = (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.xxxx_cp",le:ALiBS.gv.bn.le});
		//
		var x = new Array();
		//
		var l;
		var l2;
		//
		if(p)
		{
			if(typeof(p.n=="number") && p.n>0)
			{
				l = p.n-1;
				//
				for(var i=l;i>=0;i--)
				{
					x[l-i] = 9;
				}
			}
		}
		//
		l = a.n.length - 1;
		l2 = x.length;
		//
		for(var i=l;i>=0;i--)
		{
			x[l2++] = 9-a.n[l-i];
		}
		//
		return {
				s:1,
				d:a.d,
				n:x
			};
	},
	// *****************************************************************
	xxxx_rd: function(a,p) // reduce = used in subtraction TODO(2011-03-08): deprecated
	// a = (required)(bn)number
	// p = (optional)(JSON)parameters {r:true}
	//	r = (boolean)remove index [0] = (Default = false)
	// return = (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.xxxx_rd",le:ALiBS.gv.bn.le});
		//
		var x = new Array();
		//
		var al = a.n.length-1;
		var l = 0;
		//
		var r = true; // Default = true = remove index [0] = first byte
		//
		if(p)
		{
			if(typeof(p.r)=="boolean" && p.r==false) r = false;
		}
		//
		if(r==true) // remove first byte index [0]
		{
			// nothing = remove value
		}
		else // false = do not remove = user in subtraction a < b
		{
			x[l++] = a.n[0];
		}
		//
		for(var i=al-1;i>0;i--) // copy bytes
		{
			x[l++] = a.n[al-i];
		}
		//
		if(a.n[al]+1<=9) // always sum +1 to last byte
		{
			x[l++] = a.n[al] + 1;
			//
			return {
					s:a.s,
					d:a.d,
					n:x
				}; // bn;
		}
		else
		{
			x[l++] = a.n[al];
			//
			if(a.d==0)
			{
				return ALiBS.bn.op("+",{s:a.s,d:a.d,n:x},ALiBS.bn._um()); // +1 = integer
			}
			else // bn has decimals
			{
				if(x[x.length-1]<9)
				{
					x[x.length-1] = x[x.length-1] + 1;
					//
					return {
							s:a.s,
							d:a.d,
							n:x
						}; 
				}
				else // ==9
				{
					if(x[x.length-2]<9)
					{
						x[x.length-1] = 0;
						x[x.length-2] = x[x.length-2] + 1;
						//
						return {
								s:a.s,
								d:a.d,
								n:x
							}; 
					}
					else
					{
						//var v = "0." + ALiBS.sg.re("0",a.d-1) + "1"; // create decimal 0.00000001
						//v = ALiBS.bn.bn(v);
						//
						var y = new Array();
						//
						for(var i=a.d-1;i>=0;i--)
						{
							y[a.d-1-i] = 0;
						}
						//
						y[y.length] = 1;
						//
						//bn = ALiBS.bn.op("+",{s:1,d:a.d,n:x},v); 
						bn = ALiBS.bn.op("+",{s:1,d:a.d,n:x},{s:1,d:a.d,n:y}); 
						bn.s = a.s;
						//
						return bn;
					}
				}
			}
		}
	},
	// *****************************************************************
	xxxx_ty: function(a,p) // check type // TODO(2011-03-02): needed?
	// a = (required)(String|Object)number
	// p = (optional)(JSON)parameters {} necessary????
	//	? = 
	// return = types 
	//	1 = string
	//	2 = string with e+477
	//	3 = DO NOT EXIST = Internally only handles type 4 = (object with e+477)
	//	4 = object (default)
	//	-1 = not a bn
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.xxxx_ty",le:ALiBS.gv.bn.le});
		//
	},
	// *****************************************************************
	xxxx_rz: function(a,p) // remove zeros = normalize 1 bn // TODO(2011-03-02): needed?
	// a = (required)(Object)bn type 4
	// p = (optional)(JSON)parameters {r:true,l:true}
	//	r = (boolean)remove rigth zeros true/false (Default = true)
	//	l = (boolean)remove left zeros true/false (Default = true)
	// return = (object)bn type 4
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.bn.xxxx_rz",le:ALiBS.gv.bn.le});
		//
		// TODO(2011-02-24): remove left zeros
		// TODO(2011-02-24): remove right zeros if decimal

	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.bn("+(++ALiBS.gv.bn.nn)+"x)",{o:"ALiBS.bn"});
ALiBS.sy.la(0+0,"ALiBS.gv.bn.le="+ALiBS.gv.bn.le+ALiBS.gv.sy.se,{o:"ALiBS.bn"});

