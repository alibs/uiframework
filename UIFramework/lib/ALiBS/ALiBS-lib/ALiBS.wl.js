/* *************************************************************************
// INFO:

Used by:

Rhino command line
jswebjetty
jswebserver
novell drivers

http://www.novell.com/documentation/developer/jldap/jldapenu/api/com/novell/ldap/LDAPConnection.html
http://docs.oracle.com/javase/1.5.0/docs/api/javax/naming/directory/InitialDirContext.html
http://docs.oracle.com/javase/1.5.0/docs/api/javax/naming/InitialContext.html#getEnvironment()

http://docs.oracle.com/javase/tutorial/jndi/ldap/operations.html
http://docs.oracle.com/javase/tutorial/jndi/ops/bindattr.html
http://docs.oracle.com/javase/tutorial/jndi/ops/bind.html

/* *************************************************************************

http://da2i.univ-lille1.fr/doc/tutorial-java/jndi/ldap/ssl.html

SSL and Custom Sockets
In addition to SASL authentication, most LDAP servers allow their services to be accessed through SSL. 
SSL is especially useful for LDAP v2 servers because the v2 protocol does not support SASL authentication.
An SSL-enabled server often supports SSL in two ways. 
	- In the most basic way, the server supports SSL ports in addition to normal (unprotected) ports. 
	- The other way in which a server supports SSL is via the use of the Start TLS Extension (RFC 2830). 
		This option is available only to LDAP v3 servers and is described in detail in the section.

/* *************************************************************************

Using the SSL Socket Property

By default, Sun's LDAP service provider uses plain sockets when communicating with the LDAP server. 
To request that SSL sockets be use, set the Context.SECURITY_PROTOCOL property to "ssl".

In the following example, the LDAP server is offering SSL at port 636. 
To run this program, you must enable SSL on port 636 on your LDAP server. 

Server Requirements: 
	- The LDAP server must be set up with an X.509 SSL server certificate and have SSL enabled. 
	- Typically, you must first obtain a signed certificate for the server from a certificate authority (CA). 
		Then, follow the instructions from your directory vendor on how to enable SSL. 
			Different vendors have different tools for doing this.

For the Sun Java Directory Server, v5.2, use the Manage Certificates tool in the Administration Console to generate a Certificate Signing Request (CSR). 
Submit the CSR to a CA to obtain an X.509 SSL server certificate. 
Using the Administration Console, add the certificate to the server's list of certificates. 
Also install the CA's certificate if it is not already in the server's list of trusted CAs. 
Enable SSL by using the Configuration tab in the Administration Console. 
	Select the server in the left pane. 
	Select the Encryption tab in the right pane. 
	Click the checkboxes for "Enable SSL for this server" and "Use this cipher family: RSA", 
		ensuring that the server certificate you have added is in the list of certificates.

Client Requirements: 
	- You need to ensure that the client trusts the LDAP server that you'll be using. 
	- You must install the server's certificate (or its CA's certificate) in your JRE's database of trusted certificates. 

	Here is an example.

	- (certificate did NOT work) Export cert.der certificate from server object 
		- server object in IDM Novell 

	- (NOT tested) Firefox: Add Exception -> Get Certificat -> View -> Details -> Export... (NOT tested)

	- (NOT tested) KeyMan (http://www.alphaworks.ibm.com/tech/keyman) You can get SSL cert directly from the File -> Import menu

	- (NOT tested) InstallCert (Code by Andreas Sterbenz, now available on code.google.com)
		java InstallCert [host]:[port] 
			output.cert

	- (certificate worked OK) Export cert-ldap-browser-VM.der certificate from Ldap Browser 
		- test connection using SSL + 636 + JNDI (instead of Apache LDAP Client API)
		- in page Network Parameter / link Certificate Validation
			- export

	http://www.sslshopper.com/article-most-common-java-keytool-keystore-commands.html

	- Find JVM / cacerts
		cd /opt
		find . -iname "cacerts"

	- Find JVM / cacerts
		https://192.168.9.80:8443/nps/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cmd=1003
		https://192.168.9.10:8443/nps/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cmd=1003

		java.home=/home/dados2/myCopiedPrograms/Java-JDK-JRE/x64-jdk-1.7.0_45/jre

		cd JAVA_HOME/lib/security

	- List certificates
		cd JAVA_HOME/lib/security
		cd /opt/novell/eDirectory/lib/nds-modules/jre/lib/security (novell drivers)

		https://192.168.9.80:8443/nps/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cmd=1003
			java.home=/home/dados2/myCopiedPrograms/Java-JDK-JRE/x64-jdk-1.7.0_45/jre
		cd JAVA_HOME/lib/security

		../../bin/keytool -list -keystore cacerts
			password: changeit (DEFAULT password)
		../../bin/keytool -list -v -keystore cacerts > cacerts-list.txt

	- Import certificate 
		../../bin/keytool -import -alias IDM-VM-192-168-9-80 -file ./certificados/IDM-VM-192-168-9-80.der -keystore cacerts
		../../bin/keytool -import -alias cert.der -file /var/Relatorios/cert.der -keystore cacerts
		../../bin/keytool -import -alias cert-ca-root.der -file /var/Relatorios/cert-ca-root.der -keystore cacerts
		../../bin/keytool -import -alias cert-self-signed.der -file /var/Relatorios/cert-self-signed.der -keystore cacerts
		../../bin/keytool -import -alias cert-ldap-browser.der -file /var/Relatorios/cert-ldap-browser.der -keystore cacerts


	- Delete certificate
		../../bin/keytool -delete -alias IDM-VM-192-168-9-80 -keystore cacerts
	
	- Others
		keytool -exportcert -keystore jssecacerts -storepass changeit -file output.cert
		keytool -importcert -keystore [DESTINATION_KEYSTORE] -file output.cert

Sample Code:

	Hashtable env = new Hashtable();

	env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

	env.put(Context.PROVIDER_URL,"ldap://localhost:636/o=JNDITutorial");

	env.put(Context.SECURITY_PROTOCOL, "ssl"); // Specify SSL

	env.put(Context.SECURITY_AUTHENTICATION, "simple");

	env.put(Context.SECURITY_PRINCIPAL, "cn=S. User, ou=NewHires, o=JNDITutorial");

	env.put(Context.SECURITY_CREDENTIALS, "mysecret");

	DirContext ctx = new InitialDirContext(env);

Note: If you use SSL to connect to a server on a port that is not using SSL, then your program will hang. 
Similarly, if you use a plain socket to connect to a server's SSL socket, then your application will hang. 
This is a characteristic of the SSL protocol.

/* *************************************************************************

Using the LDAPS URL

Instead of requesting the use of SSL via the use of the Context.SECURITY_PROTOCOL property, 
you can also request the use of SSL via the use of LDAPS URLs. 

Sample Code:

	Hashtable env = new Hashtable();

	env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

	env.put(Context.PROVIDER_URL, "ldaps://localhost:636/o=JNDITutorial"); // Specify LDAPS URL

	env.put(Context.SECURITY_AUTHENTICATION, "simple");

	env.put(Context.SECURITY_PRINCIPAL, "cn=S. User, ou=NewHires, o=JNDITutorial");

	env.put(Context.SECURITY_CREDENTIALS, "mysecret");

	DirContext ctx = new InitialDirContext(env);

LDAPS URLs are accepted anywhere LDAP URLs are accepted. 
Check out the JNDI Tutorial for details on LDAP and LDAPS URLs.
Client Authentication: Using SSL with the External SASL Mechanism

/* *************************************************************************

SSL provides authentication and other security services at a lower layer than the LDAP. 
If authentication has already been done at the SSL, the LDAP layer can use that authentication information from SSL by using the External SASL mechanism.
The following example is like the previous SSL example, except that instead of using simple authentication, it uses the External SASL authentication. 
By using External, you do not need to supply any principal or password information, because they get picked up from the SSL.

Server Requirements: 
	- This example requires the LDAP server to allow certificate-based client authentication. 
	In addition, the LDAP server must trust (the CAs of) the client certificates that it receives, 
		and must be able to map the owner distinguished names in the client certificates to principals that it knows about. 
	Follow the instructions from your directory vendor on how to perform these tasks.

Client Requirements: 
	- This example requires the client to have an X.509 SSL client certificate. 
	Moreover, the certificate must be stored as the first key entry in a keystore file. 
	If this entry is password-protected, it must have the same password as the keystore. 
	For more information about JSSE keystores, see the JSSE Reference Guide.

	Hashtable env = new Hashtable(11);

	env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

	env.put(Context.PROVIDER_URL, "ldap://localhost:636/o=JNDITutorial");

	env.put(Context.SECURITY_AUTHENTICATION, "EXTERNAL"); // Principal and credentials will be obtained from the connection

	env.put(Context.SECURITY_PROTOCOL, "ssl"); // Specify SSL

	DirContext ctx = new InitialDirContext(env);

To run this program so that the client's certificate is used for authentication, 
	you must provide (as system properties) the location and password of the keystore containing the client's certificate. 
	Here is an example of how to run the program.

	java -Djavax.net.ssl.keyStore=MyKeystoreFile \
	    -Djavax.net.ssl.keyStorePassword=mysecret \
	    External

If you do not supply a keystore, the program will run using anonymous authentication because no client credential exists at the SSL.

This example shows the most basic way to accomplish certificate-based client authentication. More advanced ways can be accomplished by writing and using a custom socket factory that accesses the client certificate in a more flexible manner, perhaps by using an LDAP directory. The next section shows how to use a custom socket factory with your JNDI application.

/* *************************************************************************

Using Custom Sockets

When using SSL, the LDAP provider will, by default, use the socket factory, javax.net.ssl.SSLSocketFactory , for creating an SSL socket to communicate with the server, using the default JSSE configuration. The JSSE can be customized in a variety of ways, as detailed in the JSSE Reference Guide. However, there are times when those customizations are not sufficient and you need to have more control over the SSL sockets, or sockets in general, used by the LDAP service provider. For example, you might need sockets that can bypass firewalls, or JSSE sockets that use nondefault caching/retrieval policies for its trust and key stores. To set the socket factory implementation used by the LDAP service provider, set the "java.naming.ldap.factory.socket" property to the fully qualified class name of the socket factory. This class must implement the javax.net.SocketFactory abstract class and provide an implementation of the getDefault() method that returns an instance of the socket factory. See the JSSE Reference Guide for details.
Here is an example of a custom socket factory that produces plain sockets.

public class CustomSocketFactory extends SocketFactory {
    public static SocketFactory getDefault() {

	System.out.println("[acquiring the default socket factory]");
	return new CustomSocketFactory();
    }
	...
}
Note that this example creates a new instance of CustomSocketFactory each time a new LDAP connection is created. This might be appropriate for some applications and socket factories. If you want to reuse the same socket factory, getDefault() should return a singleton.
To use this custom socket factory with a JNDI program, set the "java.naming.ldap.factory.socket" property, as shown in the following example.

	Hashtable env = new Hashtable();

	env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

	env.put(Context.PROVIDER_URL, "ldap://localhost:389/o=JNDITutorial");

	env.put("java.naming.ldap.factory.socket", "CustomSocketFactory"); // Specify the socket factory

	DirContext ctx = new InitialDirContext(env);

The "java.naming.ldap.factory.socket" property is useful for setting the socket factory on a per context basis. 
Another way to control the sockets used by the LDAP service provider is to set the socket factory for all sockets used in the entire program, 
by using java.net.Socket.setSocketImplFactory(). 
Use of this method is less flexible because it affects all socket connections, not just LDAP connections and therefore, should be used with care.



// *************************************************************************

[OK] jc: function() // JNDI Connection
[OK] jx: function() // JNDI Connection Close

[OK] jda: function() // JNDI Attribute Definition
TODO:jdc: function() // JNDI Class Definition

[OK] js: function() // JNDI Search Object
[OK] ja: function() // JNDI Attribute ADD/REMOVE/REPLACE
TODO:jo: function() // JNDI Object ADD/DELETE

TODO:ji:

- Vou precisar ????
	nc // Novell Connection
	ns // Novell Objects Search
	na // Novell Atributes
	no // Novell Objetcs/Entries (ADD/MODIFY/DELETE)

// *************************************************************************

- js
	- ATENCAO: 
		- Usar ALiBS.wl.js() Para pesquisar "Entry" com "scope=0" (objeto específico)

		- NÃO USAR ALiBS.wl.jp pois vai pesquisar usando "scope=2" e retorno será ERRADO

		usar js 
			z.atts = "ou,brdTokenGeral,brdImanagerGeral,brdPortalGeral,modifyTimestamp";
			z.base = "ou=Usuarios,o=Bradesco";
			z.filt = "(objectClass=*)"; // 
			z.scop = 0; // entry
			z.mxrs = 1; // return 1 entry

			z.atts = "cn,brdTokenHabilitado,brdTokenAssociado";
			z.base = dd.user; // "cn=U0000004,ou=Funcionarios,ou=Usuarios,o=Bradesco";
			z.filt = "(cn=" + z.us + ")"; // (cn=U0000004)
			z.scop = 0; // entry
			z.mxrs = 1; // return 1 entry

			z.T0 = ALiBS.wl.js(dd.cc,z.base,z.filt,z.scop,z.mxrs,z.atts,{rt:1});

	- Método Antigo - Usado sozinho ou em conjunto com jp
	- Entrada: fitro string, string splitted, Array, table (wl.js)
	- Retorno: tabela com dados
	//
	- Não é paginado, somente em conjunto com jp
	- Não remove repetidos de filtro Array - usar ALiBS.to.rd() 

// ***************************

- jk
	- Método Novo - Usado em conjunto com jp
	- Entrada: filtro string (NOT splittable) (call wl.js)
	- Retorno: tabela com f00 para ser paginada
	//
	- Não é paginado
	- Não remove repetidos de filtro Array pois este filtro não se aplica

- jp
	- Método Novo - Usado sozinho, em conjunto com jk ou em conjunto com js
	- Entrada: fitro string splitted, Array, table (call wl.js)
	- Retorno: tabela com dados
	//
	- Paginado
	- Não remove repetidos de filtro Array - usar ALiBS.to.rd()

// *************************************************************************

- TODO(2013-07-08): URGENTE
	- 

- TODO(2013-07-08): Sem Urgência
	- wl.jp ou wl.js
		- criar ff2() para executar a cada 100 registros
			- zipar registros
			- salvar em arquivo
*/
//
// *************************************************************************
//
if(!ALiBS.gv.wl) ALiBS.gv.wl = {}; // global values
//
if(!ALiBS.gv.wl.le) ALiBS.gv.wl.le = 0; // local log status
// 
ALiBS.gv.wl.nn = 0; // x times loaded
//
ALiBS.gv.wl.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wl.rl = "14.0822.0001";
//
// *************************************************************************
//
ALiBS.gv.wl.mxp = 100; // max records per each paged search
//
ALiBS.gv.wl.mxx = 1000*1000; // max records in main search
//
ALiBS.gv.wl.sac = {}; // schema attribute type cache
//
//ALiBS.gv.wl.cc = {}; // connection cache - to replace dd, because dd is only for one user and server is multiuser
//
//ALiBS.gv.wl.jdann = 0; // count TEMP
//
ALiBS.gv.wl.jx = 1; // ALiBS.wl.jx behavior = Control for closing connections
	//	1 = DEFAULT = close connections
	//	0 = do NOT close connections
//
// *************************************************************************
//
ALiBS.wl = // server ldap
{
	// *****************************************************************
	jc: function(h,u,w,p) // JNDI InitialDirContext (connect)
	// h = (required)(String)host (url)
	// u = (required)(String)userDN (principal)
	// w = (required)(String)password (secret)
	// p = (optional)(JSON){}
	//	cp = (number)connection pool 
	//		0 = No connection pool
	//		1 = DEFAULT = Use connection pool
	//	to = (number)connection timeout
	//		DEFAULT = 10 s
	//
	//	ssl = (number)ssl type
	//		0 = DEFAULT = NO SSL
	//		1 = Use the SSL Socket Property = ( Context.SECURITY_PROTOCOL=ssl ) + ( URL=ldap://localhost:636/o=JNDITutorial )
	//		2 = Use the LDAPS URL = (URL=ldaps://localhost:636/o=JNDITutorial)
	//
	// return = <javax.naming.directory.DirContext> Connection Data
	//
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			ALiBS.sy.la(5+0,"h="+h,{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"u="+u,{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(8+0,"w="+w,{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
			//
			z.ec = -200; // 
			z.e = new java.util.Hashtable();
			//
	/*		if(p)
			{
				if(typeof(p.cp)=="boolean") z.cp = p.cp;
				if(typeof(p.to)=="number") z.to = p.to;
			}*/
			//  DEFAULTS [BEST-PRACTICE]
			if(typeof(z.cp)!="number") z.cp = 1; // DEFAULT 
			if(typeof(z.to)!="number") z.to = 10*1000; // DEFAULT = 10 s
			//
			ALiBS.sy.la(5+0,"z.cp="+z.cp,{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.to="+z.to,{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.ssl="+z.ssl,{o:"ALiBS.wl.jc",le:ALiBS.gv.wl.le});
			//
			z.ec = -400; // 
			if(z.cp) 
			{
				z.e.put("com.sun.jndi.ldap.connect.pool","true");
			} 
			else 
			{
				z.e.put("com.sun.jndi.ldap.connect.pool", "false");
			}
			//
			z.ec = -401; // 
			z.e.put(Packages.javax.naming.Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			z.ec = -403; // 
			z.e.put(Packages.javax.naming.Context.SECURITY_PRINCIPAL,u);
			z.ec = -404; // 
			z.e.put(Packages.javax.naming.Context.SECURITY_CREDENTIALS,w);
			z.ec = -405; // 
			z.e.put("com.sun.jndi.ldap.connect.timeout",""+z.to);
			//
			if(typeof(z.ssl)!="number" || z.ssl==0) // NO SSL = "ldap://localhost:389/o=JNDITutorial"
			{
				z.ec = -420; // 
				z.e.put(Packages.javax.naming.Context.PROVIDER_URL,h); // "ldap://localhost:389/o=JNDITutorial"
			}
			else if(z.ssl==1) // SSL Socket Property = "ldap://localhost:636/o=JNDITutorial"
			{
				z.ec = -430; // 
				z.e.put(Packages.javax.naming.Context.PROVIDER_URL,h); // "ldap://localhost:636/o=JNDITutorial"
				z.ec = -431; // 
				z.e.put(Packages.javax.naming.Context.SECURITY_PROTOCOL,"ssl"); // Specify SSL
				z.ec = -432; // 
				z.e.put(Packages.javax.naming.Context.SECURITY_AUTHENTICATION,"simple");
			}
			else if(z.ssl==2) // LDAPS URL = "ldaps://localhost:636/o=JNDITutorial"
			{
				z.ec = -440; // 
				z.e.put(Packages.javax.naming.Context.PROVIDER_URL,h); // "ldaps://localhost:636/o=JNDITutorial"
				z.ec = -441; // 
				z.e.put(Packages.javax.naming.Context.SECURITY_AUTHENTICATION, "simple");
			}
			//
			z.ec = -500; // 
			r = new Packages.javax.naming.directory.InitialDirContext(z.e);
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				case -500:z.m0="Connection Error";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; 
	},
	// *****************************************************************
	jt: function(dd,pw,p) // test connection AND open connection
	/** 
	Method <static> <number> ALiBS.wl.jt(s,p)

		Description: Test Connection AND/OR Open Connection

		Parameters:
			dd = (required)(javax.naming.directory.DirContext) Connection Data
			pw = (required)(string) Password

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			0 = [NOK] Bad Connection / Bad Credentials
			1 = [OK] Using New Connection
			2 = [OK] Using Old Connection

		Example:
			var ok = ALiBS.wl.jt(conn,pass);

	--------------------------------------------------------------------
	*/
	/*

	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			z.ok = 0;
			//
			ALiBS.sy.la(8+0,"pw="+pw,{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
			//
			z.ec = -3; // 
			if(typeof(dd.cc)=="undefined" || !dd.cc || !dd.cc.getEnvironment())
			{
				if(!dd.pass) // empty/null
				{
					if(!pw) // empty/null
					{
						ALiBS.sy.la(5+0,"(auth)(!dd.pass)(!pw)pw is Empty/null",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok2 = 0;
					}
					else
					{
						dd.pass = pw;
						//
						ALiBS.sy.la(5+0,"(auth)(!dd.pass)(SET)dd.pass=pw",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok2 = 1;
					}
				}
				else // dd.pass is NOT empty
				{
					if(!pw) // empty/null
					{
						ALiBS.sy.la(5+0,"(auth)(!pw)using old dd.pass",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok2 = 1;
					}
					else if(dd.pass!=pw) // new password
					{
						dd.pass = pw;
						//
						ALiBS.sy.la(5+0,"(auth)(dd.pass!=pw)(SET)dd.pass=pw",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok2 = 1;
					}
					else // dd.pass = pw;
					{
						ALiBS.sy.la(5+0,"(auth)(dd.pass=pw)using old dd.pass",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok2 = 1;
					}
				}
				//
				// ***********
				//
				z.ec = -4; // 
				if(z.ok2)
				{
					ALiBS.sy.la(8+0,"(auth)pw="+pw,{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le}); // confidencial
					//
					z.ec = -5; // 
					dd.cc = ALiBS.wl.jc(dd.host,dd.user,dd.pass,{cp:dd.cp,to:dd.to,ssl:dd.ssl}); // connect
					//
					z.ec = -6; // 
					if(typeof(dd.cc)=="undefined" || !dd.cc || !dd.cc.getEnvironment())
					{
						ALiBS.sy.la(5+0,"(auth)cc NOT OK (new)",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok = 0;
					}
					else
					{
						ALiBS.sy.la(5+0,"(auth)cc OK (new)",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
						//
						z.ok = 1;
					}
				}
				else
				{
					// z.ok = 0; // initial value
				}
			}
			else
			{
				ALiBS.sy.la(5+0,"(auth)cc OK (old)",{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
				//
				z.ok = 2;
			}
			//
			ALiBS.sy.la(5+0,"(auth)z.ok="+z.ok,{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"(auth)z.ok2="+z.ok2,{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le});
			//
			z.ec = -7; // 
			if(z.ok)
			{
				// 8 = confidencial = (java.naming.security.credentials=N0v3ll)
				ALiBS.sy.la(8+0,"(auth)dd.cc.getEnvironment="+dd.cc.getEnvironment(),{o:"ALiBS.wl.jt",le:ALiBS.gv.wl.le,ll:1000}); 
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jt",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		r = z.ok; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *************************************************************************
	jx: function(dd,p) // close connection
	// dd = (required)(object)connection data
	// p = (optional)(JSON){}
	//	
	// return = ""
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jx",le:ALiBS.gv.wl.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = ""; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			try // do NOT throw Exception
			{
				dd.cc.close(); // close server connection
				dd.cc = null; // invalidate data
			}
			catch(e)
			{
				ALiBS.sy.la(5+0,"[HANDLED][EXCEPTION]:closing connection",{o:"ALiBS.wl.jx",le:ALiBS.gv.wl.le});
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jx",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *************************************************************************
	scc: function(c) // show connection information
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.scc",le:ALiBS.gv.wl.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		//var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			z.t = new ALiBS.to.t();
			//
			z.ec = -3; // 
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getEnvironment]=", // Retrieves the environment in effect for this context. See class description for more details on environment properties
					f01:c.getEnvironment()
				};
			//
			z.ec = -4; // 
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getNameInNamespace]=", // Retrieves the full name of this context within its own namespace
					f01:c.getNameInNamespace()
				};

	/*		//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[isConnected]=", // Indicates whether the connection represented by this object is open at this time
					f01:c.isConnected()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[isConnectionAlive]=", // Checks whether the connection represented by this object is still alive or not
					f01:c.isConnectionAlive()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[isTLS]=", // Indicatates if the connection is protected by TLS
					f01:c.isTLS()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getAuthenticationDN]=", // Returns the distinguished name (DN) used for as the bind name during the last successful bind operation
					f01:c.getAuthenticationDN()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getAuthenticationMethod]=", // Returns the method used to authenticate the connection
					f01:c.getAuthenticationMethod()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getHost]=", // Returns the host name of the LDAP server to which the object is or was last connected, in the format originally specified
					f01:c.getHost()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getPort]=", // Returns the port number of the LDAP server to which the object is or was last connected
					f01:c.getPort()
				};
			//
			z.t.rs[z.t.rs.length] = 
				{
					f00:"[getProtocolVersion]=", // Returns the protocol version uses to authenticate
					f01:c.getProtocolVersion()
				};
	*/		//
			// ************************
			//
			z.ec = -5; // 
			ALiBS.to.o(z.t,0);
			//
			z.ec = -6; // 
			for(var i=0;i<z.t.rs.length;i++) // confidencial = (java.naming.security.credentials=N0v3ll)
			{
				ALiBS.sy.la(8+0,""+z.t.rs[i].f00+z.t.rs[i].f01,{o:"ALiBS.wl.scc",le:ALiBS.gv.wl.le,ll:1000}); // confidencial
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.scc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return;
	},
	// *****************************************************************
	jda: function(c,b,a) // JNDI Attribute Definition - Schema
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	// b = (required)(String)baseDN
	// a = (required)(String)attribute
	// return = (Object)Attribute Schema Definition
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			ALiBS.sy.la(5+0,"b: "+b,{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"a: "+a,{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
			//
			z.ec = -2; // 
			z.x1 = c.getAttributes(b,a.split(","));
			ALiBS.sy.la(5+0,"z.x1: "+z.x1,{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
			//
			z.ec = -3; // 
			z.x2 = z.x1.get(a);
			ALiBS.sy.la(5+0,"z.x2: "+z.x2,{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
			//
			z.ec = -4; // 
			z.x3 = z.x2.getAttributeDefinition();
			//
			r = {}; // [BEST-PRACTICE]
			//
			z.ec = -5; // 
			r.s = ""+z.x3.getAttributes(""); // (String) Attribute Schema Definition
			//
			z.ec = -6; // 
			if(r.s.indexOf("single-value")>0)
			{
				r.t = 1; // (number) type
			}
			else // multi-value
			{
				r.t = 2; // (number) type
			}
			//
			ALiBS.sy.la(5+0,"r.s: "+r.s,{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"r.t: "+r.t,{o:"ALiBS.wl.jda",le:ALiBS.gv.wl.le});
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jda",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	jk: function(c,b,f,p) // JNDI Paged Search Control Table
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	// b = (required)(string)baseDN
	// f = (required)(string/array/ALiBS.to.rs.f00)filter (LDAP search filter according to RFC2254 (see {@link #ldapCount(DirContext, String, String)})
	// p = (optional)(JSON){}
	//	mxx = (number) // max records in main search
	//		DEFAULT = ALiBS.gv.wl.mxx = 1000*1000
	//
	// return = ALWAYS return object ALiBS.to.t
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jk",le:ALiBS.gv.wl.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.mxx)!="number") z.mxx = ALiBS.gv.wl.mxx; // DEFAULT = return ALiBS.to.ts();
			//
			ALiBS.sy.la(5+0,"z.mxx="+z.mxx,{o:"ALiBS.wl.jk",le:ALiBS.gv.wl.le});
			//
			// local varibles [BEST-PRACTICE]
			//
			// ************************
			//
			// Parameters z/p 
			z.rt = 1; // ALWAYS return object ALiBS.to.t
			z.al = ""; 
			z.n1 = ""; // just to prevent
			z.fc = "";
			//
			z.mv0 = "";		
			z.mv1 = "";		
			z.mv2 = "";		
			z.mv3 = "";		
			z.mv4 = "";		
			z.mv5 = "";		
			z.mv6 = "";		
			z.mv7 = "";		
			z.mv8 = "";		
			z.mv9 = "";		
			//
			z.mf0 = "";
			z.mf1 = "";
			z.mf2 = "";
			z.mf3 = "";
			z.mf4 = "";
			z.mf5 = "";
			z.mf6 = "";
			z.mf7 = "";
			z.mf8 = "";
			z.mf9 = "";
			//
			//	fp = (string)prefix to be appended at the begining of the Array filter (ex: "cn=")
			//	fs = (string)sufix to be appended at the end of the Array filter (ex: ",o=Org")
			//	fo = (string)logical operator for Array (| &)
			//	fi = (string)case table, fi = field to copy to array to create new filter
			//	fx = filter extended to be appended to Array filter -->> (sn=abcd)(loginDisabled=true) (uses operator fo2)
			//
			z.ec = -3; // 
			z.ff0 = function(t0,ri) // table, record number/index
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jk.ff0",le:ALiBS.gv.wl.le});
				//
				var z = {}; // [BEST-PRACTICE]
				//
				z.ec = -1; // ec = start
				//
				//var r; // [BEST-PRACTICE]
				//
				try
				{
					z.ec = -2; // 
					z.DN = t0.rs[ri].f00;
					//
					z.ec = -3; // 
					z.DNcn = ALiBS.sg.bs(z.DN,",");
					//
					z.ec = -4; // 
					t0.rs[ri].f00 = z.DNcn;
				}
				catch(e)
				{
					switch(z.ec)
					{
						case -1:z.m0="Generic Error";z.m1="";break;
						//case -2:z.m0="Parameter x must be a number";z.m1="";break;
						default:z.m0="Generic Default Error";z.m1="";break;
					}
					//
					r = ALiBS.sy.em(z.ec,"ALiBS.wl.jk.ff0",{m0:z.m0,m1:z.m1,m2:e}).m2;
					//
					z = null; // [BEST-PRACTICE]
					delete z; // [BEST-PRACTICE]
					//
					throw r;
				}
				//
				z = null; // [BEST-PRACTICE]
				delete z; // [BEST-PRACTICE]
				//
				return;
			};
			// ************************
			//
			z.ec = -6; // 
			z.tm = ALiBS.wl.js(c,b,f,2,z.mxx,"",z); // table main, do NOT set attrs, return only f00=DNcn
			//
			z.ec = -7; // 
			ALiBS.to.lt(z.tm,{o:"ALiBS.wl.jk",le:ALiBS.gv.wl.le,ll:500,id:"(wl.jk)(A)"});
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jk",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		r = z.tm; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r; // pointer
	},
	// *****************************************************************
	jp: function(c,b,f,a,p) // JNDI Paged Search Result Table
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	// b = (required)(string)baseDN
	// f = (required)(string/array/ALiBS.to.rs.f00)filter (LDAP search filter according to RFC2254 (see {@link #ldapCount(DirContext, String, String)})
	// a = (required)(String)attrList (comma separated list of attributes to return)
	// p = (optional)(JSON){}
	//	mxp = (number) // max records per each paged search
	//		DEFAULT = ALiBS.gv.wl.mxp = 100
	//
	// return = ALWAYS return object ALiBS.to.t
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// local varibles [BEST-PRACTICE]
			r = {}; // to save local variable and NOT merge to z/p in wl.js
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.mxp)!="number") r.mxp = ALiBS.gv.wl.mxp; // DEFAULT = 
			else r.mxp = z.mxp; // ATTENTION - this function works differently - not using z/p
			//
			ALiBS.sy.la(5+0,"r.mxp="+r.mxp,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le});
			//
			z.ec = -3; // 
			//
			// local varibles [BEST-PRACTICE]
			r.len = (f.length)/(r.mxp);
			r.len = Math.floor(r.len)+1;
			ALiBS.sy.la(5+0,"r.len="+r.len,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le});
			//
			// ************************
			// Parameters z/p 
			z.rt = 1; // ALWAYS return object ALiBS.to.t
			//
			// ************************
			//
			z.ec = -4; // 
			for(var i=0;i<r.len;i++)
			{
				ALiBS.sy.la(5+0,"(wl.jp)i="+i,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le});
				//
				z.jpi = i*r.mxp; // first record in page
				z.jpf = z.jpi+r.mxp-1; // last record in page
				//
				if(i==0)
				{
					z.ec = -5; // 
					r.t0 = ALiBS.wl.js(c,b,f,2,ALiBS.gv.wl.mxx,a,z);
					//
					z.ec = -6; // 
					ALiBS.to.lt(r.t0,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le,ll:500,id:"(wl.jp)(A)(t0)"});
				}
				else
				{
					z.ec = -7; // 
					r.t1 = ALiBS.wl.js(c,b,f,2,ALiBS.gv.wl.mxx,a,z);
					//
					z.ec = -8; // 
					ALiBS.to.lt(r.t1,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le,ll:500,id:"(wl.jp)("+i+")(t1)"});
					//
					z.ec = -9; // 
					ALiBS.to.at(r.t0,r.t1); // append tables to final table
					//
					r.t1 = null; // to avoid process again below
				}
			}
			//
			if(r.t1!=null) // r.t1 has values NOT appended
			{
				z.ec = -10; // 
				ALiBS.to.lt(r.t1,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le,ll:500,id:"(wl.jp)(B)(t1)"});
				//
				z.ec = -11; // 
				ALiBS.to.at(r.t0,r.t1); // append tables to final table
				//
				r.t1 = null; // to free memory because is var r
			}
			//
			z.ec = -11; // 
			ALiBS.to.lt(r.t0,{o:"ALiBS.wl.jp",le:ALiBS.gv.wl.le,ll:500,id:"(wl.jp)(B)(t0)"});
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jp",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		//r = r.t0; // [BEST-PRACTICE]
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r.t0; // pointer
	},
	// *****************************************************************
	js: function(c,b,f,s,m,a,p) // JNDI Objects Search
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	// b = (required)(string)baseDN
	// f = (required)(string/array/ALiBS.to.rs.f00)filter (LDAP search filter according to RFC2254 (see {@link #ldapCount(DirContext, String, String)})
	// s = (required)(string)scope (0,1,2)(object,onelevel,subtree)
	// m = (required)(string)maxResultSet Max Number of Objects to Return (Default=????)
	// a = (required)(string)attrList (comma separated list of attributes to return)
	//	"" = return only f00 = DN
	//
	// p = (optional)(JSON){}
	//
	// ********************************
	// when using js
	//
	//	rt = (number)return type
	//		0 = (String) ALiBS.to.ts() = DEFAULT
	//		1 = (ALiBS.to) = table
	//
	// ********************************
	// when using js,jp
	//
	//	al = (string)custom attributes list in table object (g00,g01,g02)
	//
	//	n1 = (string)multivalue field to create nx1 table (f00)
	//		"" = DEFAULT
	//
	//	ff0 = (function)function(t0,ri) ALWAYS runs
	//	ff1 = (function)function(t0,ri) runs if(z.t0.rs[ri]!=null && f01,f02,etc exists)
	//
	//	fc = (string)record complex filter (z.ok = match = keep record)
	//		"" = NO FILTER = ALL records = DEFAULT
	//
	//	mv0 to mv9 = (string)multivalue attr complex filter
	//		"" = NO FILTER = DEFAULT
	//
	//	mf0 to mf9 = (string)multivalue attr formatting
	//		"" = NO FORMAT = DEFAULT
	//			- works for siglevalue attr 
	//			- to copy fields, use (f01#f02,f03#f04)
	//
	//	md0 to md9 = (number)multivalue delete attr (only runs if mv0...mv9 fails) (ATTENTION: it's NOT a filter)
	//		1 = delete = DEFAULT
	//		0 = do NOT delete
	//
	// ********************************
	// when using js,jk,jp
	//
	//	fp = (string)prefix to be appended at the begining of the Array filter (ex: "cn=")
	//		"" = DEFAULT
	//
	//	fs = (string)sufix to be appended at the end of the Array filter (ex: ",o=Org")
	//		"" = DEFAULT
	//
	//	fo = (string)logical operator for Array (| &)
	//		| = DEFAULT
	//
	//	fi = (string)case fi==ALiBS.to.t (table), fi = field to copy to array to create new filter
	//		f00 = DEFAULT
	//
	//	fx = (string)filter extended to be appended to Array filter -->> (sn=abcd)(loginDisabled=true) (uses operator fo2)
	//		"" = DEFAULT 
	//
	// ********************************
	// when using jp
	//
	//	jpi = (number)first record in page
	//		0 = DEFAULT 
	//
	//	jpf = (number)last record in page
	//		ALiBS.gv.wl.mxx = DEFAULT 
	//
	// return = (object)(string) ALiBS.to.t
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -1001; // 
			//
			ALiBS.sy.la(5+0,"b=["+typeof(b)+"]="+b,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"s=["+typeof(s)+"]="+s,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"m=["+typeof(m)+"]="+m,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"a=["+typeof(a)+"]="+a,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			z.ec = -1002; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.rt)!="number") z.rt = 0; // DEFAULT = return ALiBS.to.ts();
			if(typeof(z.fc)!="string") z.fc = ""; // DEFAULT = record complex filter
			if(typeof(z.fp)!="string") z.fp = ""; // DEFAULT = prefix str to be appended to Array filter
			if(typeof(z.fs)!="string") z.fs = ""; // DEFAULT = sufix str to be appended to Array filter
			if(typeof(z.fi)!="string") z.fi = "f00"; // DEFAULT = if typeof(f) == table, fi = field to copy to array to create new filter
			if(typeof(z.n1)!="string") z.n1 = ""; // DEFAULT = multivalue field to create nx1 table
			if(typeof(z.fx)!="string") z.fx = ""; // DEFAULT = filter extended
			//
			z.ec = -1003; // 
			if(ALiBS.sy.ty(z.ff0)!="func"+"tion") z.ff0 = null; // ff0 = before split
			if(ALiBS.sy.ty(z.ff1)!="func"+"tion") z.ff1 = null; // ff1 = after split
			//
			//if(typeof(z.jp)!="number") z.jp = 0; // DEFAULT = 0 = NOT called by jp
			if(typeof(z.jpi)!="number") z.jpi = 0; // DEFAULT = 0 = first record in page
			if(typeof(z.jpf)!="number") z.jpf = ALiBS.gv.wl.mxx; // DEFAULT = ALiBS.gv.wl.mxx = last record in page
			//
			ALiBS.sy.la(5+0,"z.fc="+z.fc,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			ALiBS.sy.la(5+0,"z.fp="+z.fp,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.fs="+z.fs,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.fx="+z.fx,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			ALiBS.sy.la(5+0,"z.fo="+z.fo,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.fi="+z.fi,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			//ALiBS.sy.la(5+0,"z.jp="+z.jp,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.jpi="+z.jpi,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"z.jpf="+z.jpf,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			z.ec = -1101; // 
			if(typeof(z.fo)!="string")
			{
				z.fo = "|"; // DEFAULT = logical operator |
				z.fo2 = "&"; // DEFAULT = logical operator &
			}
			else
			{
				if(z.fo=="&") // reverse
				{
					z.fo2 = "|"; // reverse
				}
				else
				{
					z.fo = "|"; // DEFAULT = logical operator |
					z.fo2 = "&"; // DEFAULT = logical operator &
				}
			}
			//
			z.ec = -1102; // 
			for(var i=0;i<=9;i++) // mv
			{
				if(typeof(z["mv"+i])!="string") z["mv"+i] = ""; // DEFAULT = multivalue attr complex filter
				//
				ALiBS.sy.la(7+0,"z[mv"+i+"]="+z["mv"+i],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			}
			//
			z.ec = -1103; // 
			for(var i=0;i<=9;i++) // mf
			{
				if(ALiBS.sy.ty(z["mf"+i])!="obj"+"ect-RegExp" && ALiBS.sy.ty(z["mf"+i])!="string") z["mf"+i] = ""; // DEFAULT = multivalue attr complex filter
				//
				ALiBS.sy.la(7+0,"z[mf"+i+"]="+z["mf"+i],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			}
			//
			z.ec = -1104; // 
			for(var i=0;i<=9;i++) // md
			{
				if(ALiBS.sy.ty(z["md"+i])!="number") z["md"+i] = 1; // DEFAULT = delete = multivalue delete attr
				//
				ALiBS.sy.la(7+0,"z[md"+i+"]="+z["md"+i],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			}
			//
			z.ec = -1201; // 
			// local varibles [BEST-PRACTICE]
			z.v0 = a.split(","); // attribute names in filter
			//
			ALiBS.sy.la(5+0,"(1)z.v0="+z.v0,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			ALiBS.sy.la(5+0,"(1)z.v0.length="+z.v0.length,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			z.ec = -1202; // 
			z.a0 = "DN," + a; // real name (DN,cn,sn)
			z.a0n = z.a0.split(",");
			//
			z.ala = new Array(); // custom fields array (f00,f01,etc)
			z.ala[z.ala.length] = "f00"; // always f00
			//
			z.ec = -1203; // 
			if(typeof(z.al)!="string" || z.al.length<3) // custom attributes list in table object (g00,g01,g02)
			{
				for(var i=0;i<z.v0.length;i++)
				{
					if(z.v0[i])
					{
						if(i<10) z.ii = "0" + (i+1);
						else	 z.ii = "" + (i+1);
						//
						z.ala[z.ala.length] = "f"+z.ii; // f01,f02,f03,f04,etc
					}
				}
			}
			else
			{
				z.al2 = z.al.split(",");
				//
				for(var i=0;i<z.al2.length;i++)
				{
					if(z.al2[i])
					{
						z.ala[z.ala.length] = z.al2[i]; // g01,g02,g03,g04,etc
					}
				}
			}
			//
			ALiBS.sy.la(5+0,"(1)z.ala="+z.ala,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			//
			z.ec = -1301; // 
			//
			// local varibles [BEST-PRACTICE]
			z.nu = 0; // deleted records/objects
			z.ty = ALiBS.sy.ty(f);
			//
			z.ec = -1302; // 
			if(z.ty=="obj"+"ect-Array")
			{
				ALiBS.sy.la(5+0,"(typeof)f=obj"+"ect-Array",{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				//
				z.ar0 = new Array(); // new Array from [jpi-jpf]
				//
				for(var i=z.jpi;(i<=z.jpf && i<f.length);i++)
				{
					if(f[i]) z.ar0[z.ar0.length] = "(" + z.fp + f[i] + z.fs + ")"; // should NEVER be "",null,undefined -->> LDAP filter ERROR
				}
				//
				f = "(" + z.fo + z.ar0.join("") + ")";
			}
			else if(z.ty=="string")
			{
				ALiBS.sy.la(5+0,"(typeof)f=string",{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			}
			else
			{
				ALiBS.sy.la(5+0,"(typeof)f=????",{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			}
			//
			z.ec = -1401; // 
			z.ar0 = null; // [BEST-PRACTICE] Free Memory - Garbage Collection
			//
			if(z.fx)
			{
				f = "(" + z.fo2 + z.fx + f + ")";
			}
			//
			ALiBS.sy.la(5+0,"(final)filter="+f,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le,ll:10*1000});
			//
			z.ec = -1410; // 
			z.sc = new Packages.javax.naming.directory.SearchControls();
			//
			z.ec = -1411; // 
			z.sc.setSearchScope(parseInt(s,10));
			//
			z.ec = -1412; // 
			z.sc.setCountLimit(parseInt(m,10));
			//
			z.ec = -1413; // 
			z.sc.setReturningAttributes(a.split(",")); 
			//
			z.ec = -1414; // 
			z.t0 = new ALiBS.to.t(); // [BEST-PRACTICE]
			//
			z.ec = -1501; // 
			z.va0 = z.a0.split(","); // attribute names in filter
			//
			for(var i=0;i<z.ala.length;i++)
			{
				z.t0.nm[z.ala[i]] = z.va0[i]; // field names
				z.t0.cp[z.ala[i]] = z.va0[i]; // field captions
				//
				ALiBS.sy.la(7+0,"z.t0.nm["+z.ala[i]+"]="+z.t0.nm[z.ala[i]],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				ALiBS.sy.la(7+0,"z.t0.cp["+z.ala[i]+"]="+z.t0.cp[z.ala[i]],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
			}
			//
			// ****************
			//
//ALiBS.sy.la(1,"",{so:"print"});
			//z.ec = -20; // 
			//
			if(f=="(|)") // to prevent EMPTY filter = (|) = eDir crash
			{
				ALiBS.sy.la(5+0,"(Empty Filter)f="+f,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				//
				z.ok_sch = 0;
			}
			else
			{
				try 
				{
					z.n0 = c.search(b,f,z.sc); 
					//
					z.ok_sch = 1;
				}
				catch(e)
				{
					ALiBS.sy.la(5+0,"Exception e="+e,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					//
					z.ok_sch = 0;
				}
			}
			//
			// ****************
			//
			z.ec = -1601; // 
			if(  !z.ok_sch || !(z.n0.hasMoreElements())  ) // to prevent EMPTY filter = (|)
			{
				z.ec = -23; // 
				if(z.rt==1) // table object
				{
					r = z.t0; // [BEST-PRACTICE]
					//
					ALiBS.sy.la(5+0,"(z.rt:1)r.rs.length="+r.rs.length,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				}
				else // string to.ts()
				{
					r = "[ZERO-RECORDS]";
					//
					ALiBS.sy.la(5+0,"(z.rt:"+z.rt+")r="+r,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				}
			} 
			else // ok
			{
				z.ec = -24; // 
				ALiBS.sy.la(5+0,"attributes="+a,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				//
				z.v1; // 
				z.v2 = new Array(z.v0.length); // (1) single-value OR (2) multi-value
				z.v3; // (1) single-value OR (2) multi-value 
				//
				z.t0.tb.fc = z.fc; // record complex filter
				//
				z.ec = -25; // 
		                while(z.n0.hasMoreElements()) // objects / records
		                {
					//cn=A0014: null:null:{logindisabled=loginDisabled: FALSE, givenname=givenName: OTAVIO, cn=cn: A0014}
					//
					//  rs_0_f00:"abcd"|rs_0_f01:"efgh"|
					//  rs_1_f00:"xpto"|rs_1_f01:"pqrs"';
					//
					z.ec = -25; // 
					z.tn = "" + z.n0.nextElement().toString();
					//
					ALiBS.sy.la(7+0,"(0)z.tn="+z.tn,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					//
					z.len = z.t0.rs.length;
					z.t0.rs[z.len] = {};
					//
					z.ec = -26; // 
					for(var i=0;i<z.ala.length;i++) // initialize Table fields - number of fields = number of attributes + 1 (DN)
					{
						z.t0.rs[z.len][z.ala[i]] = "";
					}
					//
					if(z.tn.indexOf("No attributes")>=0 && z.tn.indexOf("description")<0) // case "No attributes" inside description
					{
						z.ec = -27; // 
						z.tn = ALiBS.sg.u2s(z.tn).replace(/No attributes/gi,"");
						//
						z.ec = -28; // 
						z.tn = ALiBS.sg.bs(z.tn,":");
						//
						if(!z.tn) 
						{
							z.DN = b;
						}
						else 
						{
							z.DN = z.tn + "," + b;
						}
						//
						z.t0.rs[z.len].f00 = z.DN; // DN
						//
						for(var i=1;i<z.ala.length;i++) // set value "" to all attr
						{
							z.t0.rs[z.len][z.ala[i]] = "";
						}
						//
						ALiBS.sy.la(7+0,"z.DN="+z.DN,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
						ALiBS.sy.la(7+0,"z.t0.rs["+z.len+"].f00="+z.t0.rs[z.len].f00,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
						//
						// *********************************
						//
						z.ec = -29; // 
						if(z.ff0) z.ff0(z.t0,z.len);
					}
					else
					{
						z.ec = -30; // 
						if(z.tn.indexOf("|")>=0) // to not conflict with separator
						{
							z.tn = ALiBS.sg.u2s(z.tn).replace(/\|/gi,"-O_t-"); // temp separator
						}
						//
						z.ec = -31; // 
						z.tn = z.tn.substring(0,z.tn.length-1);
						//
						z.ec = -32; // 
						for(var i=0;i<z.v0.length;i++) // remove "givenName=givenname: "
						{
							z.tn = ALiBS.sg.rse(z.tn,z.v0[i]+"="+z.v0[i]+": ","|"+z.v0[i]+':');
						}
						//
						z.ec = -33; // 
						z.tn = ALiBS.sg.u2s(z.tn).replace(/\,\ \|/gi,"|");
						//
						z.tn = "DN:" + z.tn;
						//
						ALiBS.sy.la(7+0,"(1)z.tn="+z.tn,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
						//
						z.ec = -34; // 
						z.v1 = z.tn.split("|"); // this is a temp separator. It's not used outside this function
						//
						z.ec = -35; // 
						for(var j=0;j<z.v1.length;j++) // records
						{
							z.ec = -36; // 
							z.x = ALiBS.sg.u2s(z.v1[j]).replace(/\-O_t-/gi,"|"); // return "|" changed to -O_t-
							//
							z.ec = -37; // 
							if(z.v1[j].indexOf("DN:")==0)
							{
								z.ec = -38; // 
								z.x = ALiBS.sg.bs( ALiBS.sg.as(z.x,"DN:") ,":");
								//
								if(!z.x) 
								{
									z.DN = b;
								}
								else 
								{
									z.DN = z.x + "," + b;
								}
								//
								z.t0.rs[z.len].f00 = z.DN; // DN
								//
								ALiBS.sy.la(7+0,"z.DN="+z.DN,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
								ALiBS.sy.la(7+0,"z.t0.rs["+z.len+"].f00="+z.t0.rs[z.len].f00,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
							}
							else
							{
								z.ec = -39; // 
								for(var i=0;i<z.v0.length;i++) // attributes names in filter
								{
									z.ec = -40; // 
									if(z.x.indexOf(z.v0[i])==0)
									{
										z.ec = -41; // 
										if(!ALiBS.gv.wl.sac[z.v0[i]] && !z.v2[i])
										{
											z.ec = -4200; // 
											z.v3 = new ALiBS.wl.jda(c,z.DN,z.v0[i]);
											z.ec = -4210; // 
											z.v2[i] = z.v3.t; // (number) type
											//
											z.ec = -43; // 
											ALiBS.gv.wl.sac[z.v0[i]] = z.v2[i];
											//ALiBS.gv.wl.jdann++;
										}
										//
										z.ec = -44; // 
										if(ALiBS.gv.wl.sac[z.v0[i]]==1 || z.v2[i]==1) // single-value
										{
											z.ec = -45; // 
											z.t0.rs[z.len][z.ala[i+1]] = ALiBS.sg.as(z.x,": ").replace(/^\ /,"");
										}
										else // multi-value
										{
											z.ec = -46; // 
											z.v3 = ALiBS.sg.as(z.x,": "); // z.v3 used as temp
											//
											z.ec = -47; // 
											z.t0.rs[z.len][z.ala[i+1]] = ALiBS.sg.u2s(z.v3).replace(/\,\ /gi,"!pIpE!").replace(/^\ /,""); // !pIpE! = multi-value separator
										}
										//
										break;
									}
								}
							}
						}
						//
						// *********************************
						//
						z.ec = -48; // 
						if(z.ff0) z.ff0(z.t0,z.len);
					} // mudei aqui
					//
					//
					// *********************************
					//
					ALiBS.sy.la(7+0,"(a)rs["+z.len+"]="+z.t0.rs[z.len].f00,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					//
					ALiBS.sy.la(7+0,"z.fc="+z.fc,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					ALiBS.sy.la(7+0,"z.t0.tb.fc="+z.t0.tb.fc,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					//
					ALiBS.sy.la(7+0,""+ALiBS.gv.sy.se,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					//
					z.ec = -49; // 
					if(z.t0.tb.fc) // record/object complex filter
					{
						z.ec = -50; // 
						z.ok = z.t0.ft.fc(z.t0,z.len); // "complex" filter
						//
						ALiBS.sy.la(7+0,"(fc)z.ok="+z.ok,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
						//
						z.ec = -51; // 
						if(z.ok)
						{
							ALiBS.sy.la(7+0,"(keep record)rs["+z.len+"]="+z.t0.rs[z.len].f00,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
						}
						else
						{
							ALiBS.sy.la(7+0,"(null record)rs["+z.len+"]="+z.t0.rs[z.len].f00,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
							//
							z.t0.rs[z.len] = null;
							//
							z.nu++;				
						}
					}
					//
					ALiBS.sy.la(7+0,""+ALiBS.gv.sy.se,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
					//
					// *********************************
					//
					z.ec = -52; // 
					if(z.t0.rs[z.len]) // NOT null
					{
						z.ec = -53; // 
						if(z.ff1) z.ff1(z.t0,z.len);
						//
						// *************************
						//
						for(var l=0;l<=9;l++)
						{
							if(z["mv"+l]) // z.t0.rs[z.len] NOT null
							{
								ALiBS.sy.la(7+0,"z.mv"+l+"="+z["mv"+l],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
								ALiBS.sy.la(7+0,"z.mf"+l+"="+z["mf"+l],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
								//
								z.ec = -54; // 
								z.x1 = z["mv"+l].match(/[><:!=]{1,}/); // (  == >= <= != :=  )
								//z.x1 = z["mv"+l].indexOf("="); // matches second =   (  == >= <= != :=  )
								//
								ALiBS.sy.la(7+0,"z.x1="+z.x1,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
								//
								//if(z.x1>0)
								if(z.x1)
								{
									z.ec = -55; // 
									z.x1a = z["mv"+l].split(z.x1); // ["f00","xpto"] 
									z.x2 = z.x1a[0].substring(1,4); // (f00 -->> f00
									//z.x4 = z.x1a[1]; // xpto
									//
									//z.x2 = z["mv"+l].substring(1,z.x1-1); // field f00 or other
									z.x4 = z.t0.rs[z.len][z.x2]; // target attribute string 
									//
									z.ec = -1551; // 
									z.x4b = ALiBS.sy.ty(z["mf"+l]);
									//
									ALiBS.sy.la(7+0,"z.x2="+z.x2,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
									ALiBS.sy.la(7+0,"z.x4="+z.x4,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
									//
									if(z.x4)
									{
										z.ec = -56; // 
										z.x5 = z.x4.split("!pIpE!"); // many values
										//
										ALiBS.sy.la(7+0,"z.x5="+z.x5,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
										//
										z.ec = -57; // 
										z.x0 = new ALiBS.to.t(); // table with target attribute
										//
										z.ec = -58; // 
										z.x0.tb.fc = z["mv"+l]; // multivalue attr complex filter
										z.x3 = new Array(); // used for multivalue attr
										//
										for(var k=0;k<z.x5.length;k++) // for each attr splitted "!pIpE!"
										{
											z.ec = -59; // 
											z.x0.rs[k] = {};
											z.x0.rs[k][z.x2] = z.x5[k];
											//
											z.ec = -60; // 
											z.ok = z.x0.ft.fc(z.x0,k); // "complex" filter
											//
											ALiBS.sy.la(7+0,"(mv)z.ok="+z.ok,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
											//
											ALiBS.sy.la(7+0,"z.x5["+k+"]="+z.x5[k],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
											//
											if(z.ok) // validate mv filter
											{
												ALiBS.sy.la(7+0,"(keep attr)x0.rs["+k+"]["+z.x2+"]="+z.x0.rs[k][z.x2],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
												if(z.x4b=="obj"+"ect-RegExp")
												{
													ALiBS.sy.la(7+0,"mf=obj"+"ect-RegExp",{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
													//
													z.ec = -61; // 
													z.x5b = z.x5[k].match(z["mf"+l]);
													if(z.x5b) z.x3[z.x3.length] = "" + z.x5b;
												}
												else
												{
													z.ec = -62; // 
													ALiBS.sy.la(7+0,"mf=else",{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
													//
													z.x3[z.x3.length] = z.x5[k];
												}
											}
											else // if mv filter fails
											{
												z.ec = -63; // 
												if(z["md"+l]==1) // delete
												{
													// do NOT add
													ALiBS.sy.la(7+0,"(null attr)x0.rs["+k+"]["+z.x2+"]="+z.x0.rs[k][z.x2],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
												}
												else // 0
												{
													z.x3[z.x3.length] = z.x5[k];
													ALiBS.sy.la(7+0,"(NOT delete attr)x0.rs["+k+"]["+z.x2+"]="+z.x0.rs[k][z.x2],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
												}
											}
										}
										//
										z.ec = -64; // 
										if(z.x3.length>0) z.t0.rs[z.len][z.x2] = z.x3.join("!pIpE!");
										//
										ALiBS.sy.la(7+0,"(a)z.t0.rs["+z.len+"]["+z.x2+"]="+z.t0.rs[z.len][z.x2],{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
									}
								}
							}
							else
							{
								break; // to not go through [0-9]
							}
							//
							ALiBS.sy.la(7+0,""+ALiBS.gv.sy.se,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
						}
					}
					//} // else
		                } // while
				//
				// ****************
				//
				z.ec = -65; // 
				ALiBS.sy.la(5+0,"(a)rs.length="+z.t0.rs.length,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				//
				z.t0.rs.sort();
				//
				z.t0.rs.length = z.t0.rs.length - z.nu;
				//
				ALiBS.sy.la(5+0,"(b)rs.length="+z.t0.rs.length,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				//
				// ****************
				//
				z.ec = -66; // 
				ALiBS.to.un(z.t0);
				//
				if(z.n1)
				{
					z.ec = -67; // 
					ALiBS.to.n1(z.t0,z.n1);
				}
				//
				z.t0rs = null; // to speed up Garbage Collection
				//
				if(z.rt==1) // table object
				{
					r = z.t0; // [BEST-PRACTICE]
				}
				else // string to.ts()
				{
					z.ec = -68; // 
					r = ALiBS.to.ts(z.t0);
					//
					ALiBS.sy.la(5+0,"(ALiBS.wl.js)z.t0.length="+z.t0.length,{o:"ALiBS.wl.js",le:ALiBS.gv.wl.le});
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.js",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	ja: function(c,x,b,a,v,p) // JNDI ADD/REMOVE/REPLACE Attributes
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	// x = (required)(String)action ADD/REMOVE/REPLACE /REMOVEALL
	// b = (required)(String)baseDN
	// a = (required)(String)attrList (comma separated list of attributes to return)
	// v = (required)(String)value to ADD/REMOVE/REPLACE/REPLACEALL
	// p = (optional)(JSON){}
	//	
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.ja",le:ALiBS.gv.wl.le});
		//
		// ADD_ATTRIBUTE=1
		// REPLACE_ATTRIBUTE = 2
		// REMOVE_ATTRIBUTE = 3 
		//	EMPTY = remove all values
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.rt)!="number") 
			//
			// local varibles [BEST-PRACTICE]
			z.x0;
			z.mu = new Array; // = new Packages.javax.naming.directory.ModificationItem[1];
			//
			r = -9; // [BEST-PRACTICE]
			if(x!="GETATT")
			{
				if(x=="ADD") // ADD VALUES to multivalued attributes (if attribute does not exist, create the attribute)
				{
					z.ec = -21; // 
					z.x0 = Packages.javax.naming.directory.DirContext.ADD_ATTRIBUTE;
					z.mu[0] = new Packages.javax.naming.directory.ModificationItem(z.x0,new Packages.javax.naming.directory.BasicAttribute(a,v));
				}
				else if(x=="REMOVE") // REMOVE VALUES of multivalued attributes (if last value is removed, the attribute is removed)
				{
					z.ec = -22; // 
					z.x0 = Packages.javax.naming.directory.DirContext.REMOVE_ATTRIBUTE;
					z.mu[0] = new Packages.javax.naming.directory.ModificationItem(z.x0,new Packages.javax.naming.directory.BasicAttribute(a,v));
				}
				else if(x=="REPLACE") // REPLACE ALL VALUES of single/multivalued atributes (if attribute does not exist, create the attribute)
				{
					z.ec = -23; // 
					z.x0 = Packages.javax.naming.directory.DirContext.REPLACE_ATTRIBUTE;
					z.mu[0] = new Packages.javax.naming.directory.ModificationItem(z.x0,new Packages.javax.naming.directory.BasicAttribute(a,v));
				}
				else if(x=="REMOVEALL") // Does not use "v" = value (if no values are specified, deletes the attribute)
				{
					z.ec = -24; // 
					z.x0 = Packages.javax.naming.directory.DirContext.REMOVE_ATTRIBUTE;
					z.mu[0] = new Packages.javax.naming.directory.ModificationItem(z.x0,new Packages.javax.naming.directory.BasicAttribute(a));
				}
				//
				z.ec = -25; // 
				try
				{
					c.modifyAttributes(b,z.mu); 
					r = 1;
				}
				catch(e) 
				{
					ALiBS.sy.la(5+0,"[HANDLED][EXCEPTION]:"+e,{o:"ALiBS.wl.ja",le:ALiBS.gv.wl.le});
					r = -1;
				}
			}
			else // GETATT
			{
				z.ec = -31; // 
				z.idx = b.indexOf(",");
				//
				z.ec = -32; // 
				z.f0 = b.substring(0,z.idx); // filter
				//
				z.ec = -33; // 
				z.b0 = b.substring(z.idx+1,b.length); // base
				//
				z.ec = -34; // 
				r = ALiBS.wl.js(c,z.b0,z.f0,"1","1",a); // JNDI Objects Search
				//
				ALiBS.sy.la(5+0,"z.b0="+z.b0,{o:"ALiBS.wl.ja",le:ALiBS.gv.wl.le});
				ALiBS.sy.la(5+0,"z.f0="+z.f0,{o:"ALiBS.wl.ja",le:ALiBS.gv.wl.le});
				//
				// Did not implement the code below because return of ALiBS.wl.js is ALREADY ALiBS.to.t Object
				//
				//Attributes matchAttrs = lctx.getAttributes(p1,p2.split("\\,")); // p1=baseDN, p2=attrs
				//
				//ret = matchAttrs.toString();
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.ja",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	jo: function(c,x,b,p) // JNDI Object ADD/DELETE
	// c = (required)(connection)Connection Control / Session (javax.naming.directory.DirContext)
	// x = (required)(String)action ADD/DELETE
	// b = (required)(String)baseDN
	// p = (optional)(JSON){}
	//	
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.jo",le:ALiBS.gv.wl.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.rt)!="number") 
			//
			// local varibles [BEST-PRACTICE]
			z.x0;
			z.mu = new Array; // = new Packages.javax.naming.directory.ModificationItem[1];
			//
			r = -100; // [BEST-PRACTICE]
			if(x=="ADD") // ADD object
			{
				z.ec = -101; // 
				//c.bind(b);
				//c.modifyAttributes(b,z.mu); 
				//r = 1;
			}
			else if(x=="DELETE") // DELETE object
			{
				z.ec = -201; // 
				//
				c.cc.unbind(b); // Only in javax.naming.InitialContext, Not in javax.naming.directory.InitialDirContext
				//				
				r = 1;
				//
				// usar lookup para validar se o objeto foi apagado
				//Object lookup(String name)
				//              throws NamingException
			}
			else if(x=="RENAME") // RENAME object
			{
				//c.rename(b,b2);
				//void rename(String oldName,
				//	  String newName)
				//	    throws NamingException

				// usar lookup para validar se o objeto foi renomeado
				//Object lookup(String name)
				//              throws NamingException
			}
			else
			{
				r = -2;
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.jo",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	df: function(i,o,p) // DN format
	//TODO:(2011-07-11) "/" "/admin/services/bradesco" - Precisa?
	/**
		o = (Required)(String)output type
			"," "cn=admin,ou=services,o=bradesco"
			"." ".admin.services.bradesco"
			"\" "\admin\services\bradesco"
	*/
	// i = (required)(String)input 
	// o = (required)(String)output type "," "." "\"
	// p = (optional)(JSON)parameter {oo:"Embraer,Bradesco",oou:"Sistema,Servers,IDM,Grupos"}
	//	oo = (String)Object (O)rganization = FORCE o
	//	oou = (String)Object (O)rganization (U)nit = FORCE ou
	//	ocn = (String)Object (C)omum (N)ame = FORCE cn
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{

			z.ec = -101; 
			ALiBS.sy.la(5+0,"i="+i,{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
			z.ec = -102; 
			ALiBS.sy.la(5+0,"o="+o,{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
			//
			z.ec = -2; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			//if(typeof(z.rt)!="number") 
			if(typeof(z.oo)!="string") z.oo = "";
			if(typeof(z.oou)!="string") z.oou = "";
			if(typeof(z.ocn)!="string") z.ocn = "";
			//
			z.ec = -111; 
			ALiBS.sy.la(5+0,"z.oo="+z.oo,{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
			z.ec = -112; 
			ALiBS.sy.la(5+0,"z.oou="+z.oou,{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
			z.ec = -113; 
			ALiBS.sy.la(5+0,"z.ocn="+z.ocn,{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
			//
			// local varibles [BEST-PRACTICE]
			z.ty; // input type
			//
			// ************************
			//
			if(i.indexOf(",")>=0)
			{
				z.ty = ",";
			}
			else if(i.indexOf(".")>=0)
			{
				z.ty = ".";
			}
			else if(i.indexOf("\\")>=0)
			{
				z.ty = "\\";
			}
			else if(i.indexOf("=")>=0)
			{
				z.ty = ",";
			}
			else
			{
				z.ty = ".";
				i = "." + i; // (Bradesco --> .Bradesco)
			}
			//
			z.ec = -3; // 
			if(z.ty==o) 
			{
				if(i.charAt(0)=="." || i.charAt(0)=="," || i.charAt(0)=="\\" ) i = i.substring(1,i.length);
				if(i.charAt(i.length-1)=="." || i.charAt(i.length-1)=="," || i.charAt(i.length-1)=="\\" ) i = i.substring(0,i.length-1);
				//
				r = i; // input type == output type
			}
			else
			{
				z.ec = -4; // 
				z.a = i.split(z.ty);
				z.b = new Array();
				//
				z.ec = -5; // 
				if(z.ty==",") // remove cn= ou= o=
				{
					z.ec = -6; // 
					for(var j=0;j<z.a.length;j++)
					{
						n = z.a[j].indexOf("=");
						//
						z.a[j] = z.a[j].substr(n+1,z.a[j].length);
					}
				}
				else if(z.ty=="\\") // reverse elementsb = final order = 0=[admin] 1=[services] 2=[brd]
				{
					z.ec = -7; // 
					for(var j=0;j<z.a.length;j++)
					{
						z.b[j] = z.a[(z.a.length-j-1)];
					}
					//
					z.ec = -8; // 
					for(var j=0;j<z.a.length;j++)
					{
						z.a[j] = z.b[j];
						z.b[j] = "";
					}
				}
				//
				// *********************************************************
				//
				z.ec = -9; // 
				z.b.length = 0;
				//
				if(o==".")
				{
					z.ec = -10; // 
					for(var j=0;j<z.a.length;j++)
					{
						z.v = ALiBS.sg.tr(z.a[j]);
						//
						if(z.v!="")
						{
							if(z.b.length==0) z.b[j] = z.v;
							else z.b[j] = "." + z.v;
						}
					}
					//
					z.ec = -11; // 
					z.v = z.b.join("");
					//
					z.ec = -12; // 
					if(z.v.charAt(0)==".") z.v =z.v.substring(1,z.v.length);
					//
					z.ec = -13; // 
					if(z.v.charAt(z.v.length-1)==".") z.v = z.v.substring(0,z.v.length-1);
					//
					r = z.v; // 
				}
				else if(o=="\\")
				{
					z.ec = -14; // 
					for(var j=0;j<z.a.length;j++)
					{
						z.v = ALiBS.sg.tr(z.a[(z.a.length-j-1)]);
						//
						if(z.v!="")
						{
							if(z.b.length==0) z.b[j] = z.v;
							else z.b[j] = "\\" + z.v;
						}
					}
					//
					z.ec = -15; // 
					z.v = z.b.join("");
					//
					z.ec = -16; // 
					if(z.v.charAt(0)=="\\") z.v = z.v.substring(1,z.v.length);
					//
					z.ec = -17; // 
					if(z.v.charAt(z.v.length-1)=="\\") z.v = z.v.substring(0,z.v.length-1);
					//
					r = z.v; // 
				}
				else if(o==",")
				{
					z.ec = -18; // 
					for(var j=0;j<z.a.length;j++)
					{
						z.ec = -19; // 
						ALiBS.sy.la(5+0,"z.a["+j+"]="+z.a[j],{o:"ALiBS.wl.df",le:ALiBS.gv.wl.le});
						z.v = ALiBS.sg.tr(z.a[j]);
						//
						if(z.v!="")
						{
							z.ec = -20; // 
							z.xv = z.v.toLowerCase();
							z.xoo = z.oo.toLowerCase();
							z.xoou = z.oou.toLowerCase();
							z.xocn = z.ocn.toLowerCase();
							//
							z.ec = -21; // 
							if(z.oo!="" && ALiBS.sg.ce(z.xv,z.xoo.split(","))>=0)
							{
								z.v = "o=" + z.v; // FORCE o
							}
							else if(z.oou!="" && ALiBS.sg.ce(z.xv,z.xoou.split(","))>=0)
							{
								z.v = "ou=" + z.v; // FORCE ou
							}
							else if(z.ocn!="" && ALiBS.sg.ce(z.xv,z.xocn.split(","))>=0)
							{
								z.v = "cn=" + z.v; // FORCE cn
							}
							else if(i.toLowerCase().indexOf("appconfig")>=0)
							{
								z.ec = -23; // 
								if(j==z.a.length-1) z.v = "o=" + z.v; // ADD o to last object if not listed in oo
								else z.v = "cn=" + z.v; // user application - ADD cn to all objects except those listed in oo,oou,ocn
							}
							else
							{
								z.ec = -24; // 
								if(j==z.a.length-1) z.v = "o=" +z.v; // ADD o to last object if not listed in oo
								else if(z.b.length==0) z.v = "cn=" + z.v; // ADD cn to first object if not listed in ocn
								else z.v = "ou=" + z.v; // ADD ou to all objects not listed in oo,oou,ocn
							}
							//
							z.b[z.b.length] = z.v + ",";
						}
					}
					//
					z.ec = -25; // 
					z.v = z.b.join("");
					//
					z.ec = -26; // 
					if(z.v.charAt(0)==",") z.v = z.v.substring(1,z.v.length);
					//
					z.ec = -27; // 
					if(z.v.charAt(z.v.length-1)==",") z.v = z.v.substring(0,z.v.length-1);
					//
					r =  z.v; // 
				}
				else
				{
					r = i;
				}
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wl.df",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	}
};
// *************************************************************************
//
if(ALiBS.gv.sy.ev=="Server")
{
	if(typeof(ALiBS.gv.sy.reference)!="undefined")
	{
		if(ALiBS.gv.sy.reference.indexOf("ALiBS_wl_df")>=0)
		{
			ALiBS_wl_df = function(i,o,ou) // called by novell driver // claypool shell  (do NOT use var)
			// ou = (String) = NOT JSON
			{
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS_wl_df",le:ALiBS.gv.wl.le});
				//
				return ALiBS.wl.df(i,o,{oou:ou});
			};
		}
	}
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wl("+(++ALiBS.gv.wl.nn)+"x)",{o:"ALiBS.wl"});
ALiBS.sy.la(0+0,"ALiBS.gv.wl.rl="+ALiBS.gv.wl.rl,{o:"ALiBS.wl"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.wl.le="+ALiBS.gv.wl.le+ALiBS.gv.sy.se,{o:"ALiBS.wl"});
//
// *************************************************************************
//
//	SAMPLE OF FUNCTIONS TO BE OVERWRITTEN / EXTENDED
/*

*/
//
// *************************************************************************
/*
function ALiBS_wl_se(host,port,user,password,base,scope,filter,attrList,maxResultSet) // called by novell driver
{
	ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.ALiBS_wl_se",le:ALiBS.gv.wl.le});
	//
	return ALiBS.wl.se(host,port,user,password,base,scope,filter,attrList,maxResultSet);
};
//




	cc: function(host,port,user,password) // connect
	// s = (required)(String)s
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.cc",le:ALiBS.gv.wl.le});
		//
		var z0; // cp nao pega 
		var r0;
		var zr;
		var rz;
		//
		var c = new Packages.com.novell.ldap.LDAPConnection();
		//
		c.connect(host,port); // connect and bind to the server
		//
		var pwd = new java.lang.String(password).getBytes("UTF8");
		//
		c.bind(Packages.com.novell.ldap.LDAPConnection.LDAP_V3,user,pwd); // TODO(2011-01-12):colocar a autenticação em outra funcao
		//
		return c;
	},
	// *****************************************************************
	cx: function(s) // close conection
	// s = (required)(String)s
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.cx",le:ALiBS.gv.wl.le});
		//
	},
	// *****************************************************************
	cm: function(s) // command ???? add, modify, delete
	// s = (required)(String)s
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.cm",le:ALiBS.gv.wl.le});
		//
	},
	// *****************************************************************
	//se: function(host,port,user,password,base,scope,filter,attrList,maxResultSet) // search
	se: function(lc,base,scope,filter,attrList,maxResultSet) // search
*/
	/**
	*  ldapSearch
	*
	* @param (String} host 		LDAP Server, either DNS or IP-Address
	* @param (Number} port     		LDAP listening port
	* @param (String} user     		user account, full distinguished name, LDAP syntax
	* @param (String} password 		the cleartext LDAP userpassword
	* @param (String} base			search base 
	* @param (String} scope		(base | one | sub)
	* @param (String} filter     		LDAP search filter according to RFC2254 (see {@link #ldapCount(DirContext, String, String)}
	* @param (String} attrList		comma separated list of attributes to return
	* @param (Number} maxResultSet		maximum result set size (0=unlimited)
	* @type Nodeset		   
	*
	* @return NodeSet containing instances from search result, or status element with error message
	*/

	//{


	/*	var nodeSet = new Packages.com.novell.xml.xpath.NodeSet();
		var document = Packages.com.novell.xml.dom.DocumentFactory.newDocument();
		var ndsElement   = document.createElement("nds");
		document.appendChild(ndsElement);
		ndsElement.setAttributeNS(null, "dtdversion", "3.5");	
		var outputElement = document.createElement("output");
		ndsElement.appendChild(outputElement);
	*/	//
/*		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.se",le:ALiBS.gv.wl.le});
		//
		var searchScope = Packages.com.novell.ldap.LDAPConnection.SCOPE_ONE;
		if(scope=="base")
		{
			searchScope = Packages.com.novell.ldap.LDAPConnection.SCOPE_BASE;
		} 
		else if(scope=="sub")
		{
			searchScope = Packages.com.novell.ldap.LDAPConnection.SCOPE_SUB;
		}
		//
		var attrSplit = attrList.split(',');
		var attrArray = java.lang.reflect.Array.newInstance(java.lang.String, attrSplit.length);
		//
		for(var attrIndex in attrSplit)
		{
			attrArray[attrIndex] = attrSplit[attrIndex];
		}
		//	
		//var lc = new Packages.com.novell.ldap.LDAPConnection();
		//var lc = ALiBS.wl.cc(host,port,user,password);
		//
		// set search result set size limit
		var searchConstraints = new Packages.com.novell.ldap.LDAPSearchConstraints(lc.getSearchConstraints());
		searchConstraints.setMaxResults(maxResultSet);
		lc.setConstraints(searchConstraints);
		//
		var errorMessage = "";
	    	var r = new Array();
		//
		try
		{
			//lc.connect(host,port); // connect and bind to the server
			//
			try
			{
				//var pwd = new java.lang.String(password).getBytes("UTF8");
				//
				//lc.bind(Packages.com.novell.ldap.LDAPConnection.LDAP_V3,user,pwd); // TODO(2011-01-12):colocar a autenticação em outra funcao
				//
				try
				{
					var searchResults = lc.search(base,searchScope,filter,
									attrArray,         // return all attributes
									false);       // return attrs and values
					//
					// process the results
					//   -- The first while loop goes through all the entries
					//   -- The second while loop goes through all the attributes
					//   -- The third while loop goes through all the attribute values
					//
					//
					var n = 0; // object number
					//
					while(searchResults.hasMore())
					{
						var nextEntry = searchResults.next();
						//
						r[n] = {};
						r[n]["DN"] = nextEntry.getDN();
						//
						// create the instance node
						//var instanceElement = document.createElement("instance");
						//instanceElement.setAttributeNS(null,"src-dn", nextEntry.getDN());
						//
						var attributeSet = nextEntry.getAttributeSet();
						var allAttributes = attributeSet.iterator();
						//
						// create the attr nodes
						while(allAttributes.hasNext())
						{
							var attribute = allAttributes.next();
							var attname = attribute.getName();
							//		
							//var attrElement = document.createElement("attr");
							//attrElement.setAttributeNS(null, "attr-name", attribute.getName());

							var allValues = attribute.getStringValues();
							//r[n][attname] = allValues;
				
							// create the value nodes

							if( allValues != null)
							{
								var at = "";
								//
								while(allValues.hasMoreElements())
								{
									//var valueElement = document.createElement("value");
									//valueElement.appendChild(document.createTextNode(allValues.nextElement()));
									//attrElement.appendChild(valueElement);
									//
									at = at + ";"+ allValues.nextElement();
								}
								r[n][attname] = at;
								//instanceElement.appendChild(attrElement);
							}

						}
						//outputElement.appendChild(instanceElement);
						//nodeSet.add(instanceElement);
						//
						n++;
					}
				}
				catch(e)
				{
					errorMessage = errorMessage + "<br>Search-bind:"+e.toString();
				}
			}
			catch(e)
			{
				errorMessage = errorMessage + "<br>Error-bind:"+e.toString();
			}
		}
		catch(e)
		{
			errorMessage = errorMessage + "<br>Error-connect:"+e.toString();
		}
		//
		try
		{
			lc.disconnect(); // disconnect with the server
		}
		catch(e)
		{
			errorMessage = errorMessage + "<br>Error-disconnect:"+e.toString();
		}
		//
		if(errorMessage)
		{
			var statusElement = document.createElement("status");
			statusElement.setAttributeNS(null, "level", "error");
			statusElement.appendChild(document.createTextNode(errorMessage));
			outputElement.appendChild(statusElement);
			nodeSet.add(statusElement);
		}
		//
		return r; //nodeSet;
	},
	// *****************************************************************
	aa: function(lc,dn,att,value) // Atribute ADD TODO(2011-02-18): não esta funcionando
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.aa",le:ALiBS.gv.wl.le});
		//
		lc.modify(dn,new Packages.com.novell.ldap.LDAPModification(Packages.com.novell.ldap.LDAPModification.ADD,new Packages.com.novell.ldap.LDAPAttribute(att,value)));
	},
	// *****************************************************************
	ar: function(lc,dn,att,value) // Atribute REPLACE TODO(2011-02-18): não esta funcionando
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wl.ar",le:ALiBS.gv.wl.le});
		//
		lc.modify(dn,new Packages.com.novell.ldap.LDAPModification(Packages.com.novell.ldap.LDAPModification.REPLACE,new Packages.com.novell.ldap.LDAPAttribute(att,value)));
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************

*/
