/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.sb) ALiBS.gv.sb = {}; // global values
//
if(!ALiBS.gv.sb.le) ALiBS.gv.sb.le = 0; // local log status
//
ALiBS.gv.sb.nn = 0; // x times loaded
//
ALiBS.gv.sb.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.sb0 = ALiBS.ob.gc();
//
// *************************************************************************
//
ALiBS.sb0.prototype = 
{
	vr: function() // variables
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.vr",le:ALiBS.gv.sb.le});
		//
		this.sbf = new Array();
	},
	// *****************************************************************
	ap: function(s,p) // append
	// s = (required)(string)string 
	// p = (optional)(JSON)parameters {s:""} 
	// 	s=(string)sufix  - once if string OR for each array item
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.ap",le:ALiBS.gv.sb.le});
		//
		var t = (p && p.s) || "";
		//
		if(typeof s == "string")
		{
			this.sbf[this.sbf.length] = s+t;
		}
		else if(typeof s == "obj"+"ect")
		{
			var l = s.length;
			for(var i=0;i<l;i++)
			{
				this.sbf[this.sbf.length] = s[i]+t;
			}
		}
	},
	// *****************************************************************
	lg: function() // length of string, not array
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.lg",le:ALiBS.gv.sb.le});
		//
		return this.ts().length;
	},
	// *****************************************************************
	rp: function(i,f,s) // replace 
	// i = (required)(number)inicio 
	// f = (required)(number)fim 
	// s = (required)(string)string 
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.rp",le:ALiBS.gv.sb.le});
		//
   		var tmp = this.ts();
		var tmp1 = "";
		var tmp2 = "";
		var tmp3 = "";
		//
		if(i<=0) 
		{ 
			tmp1 = ""; 
		}
		else 
		{ 
			tmp1 = tmp.substr(0,i); 
		}
		//
		if(f>=tmp.length) 
		{ 
			tmp2 = ""; 
		}
		else 
		{ 
			tmp2 = tmp.substr(f+1,tmp.length); 
		}
		//		
		tmp3 = new ALiBS.sb0();
		tmp3.ap(tmp1); 
		tmp3.ap(s); 
		tmp3.ap(tmp2); 
		//
		this.sbf.length=1;
		this.sbf[0]=tmp3.ts();
	},
	// *****************************************************************
	ta: function() // to array
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.ta",le:ALiBS.gv.sb.le});
		//
		return this.sbf;
	},
	// *****************************************************************
	ts: function() // to string
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.ts",le:ALiBS.gv.sb.le});
		//
		return this.sbf.join("");
	}
	// *****************************************************************
};
// *************************************************************************
ALiBS.sb = ALiBS.ob.gc();
//
ALiBS.gv.sb.tmp0 = 
{
	vr: function(s) // variables
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.vr",le:ALiBS.gv.sb.le});
		//
		this.rs();
		if(arguments.length==1)
		{
			this.sbf[0] = s; // recebe Array de sb0 como uma string
		}
	},
	// *********************************************************************
	cc: function(s) // concat
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.cc",le:ALiBS.gv.sb.le});
		//
		this.ap(s);
	},
	// *********************************************************************
	rs: function() // reset
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.sb.rs",le:ALiBS.gv.sb.le});
		//
		for(var i=0;i<this.sbf.length;i++)
		{
			this.sbf[i] = "";
		}
	}
	// *********************************************************************
};
//
ALiBS.sb.prototype = ALiBS.ob.ex(new ALiBS.sb0(),ALiBS.gv.sb.tmp0);
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.sb("+(++ALiBS.gv.sb.nn)+"x)",{o:"ALiBS.sb"});
ALiBS.sy.la(0+0,"ALiBS.gv.sb.le="+ALiBS.gv.sb.le+ALiBS.gv.sy.se,{o:"ALiBS.sb"});

