/* *************************************************************************
// INFO:

menus devem estar em tables independentes para nao conflitar o recordDN
	- portanto nao precisa 
		- ft:filtro
		- f00

// ****************************************

ALiBS.to - Menu m0:

- Tree Menu
- Simple Menu

	f00: [FREE][NOTUSED]
	f01: (String-Normalized) recordDN [CALCULATED]
	f02: (String) text
	f03:	(String) url/javascript
	f08: left background image for option


- Tree Menu with [CALCULATED] f01 recordDN

	f04: (String) status [FIX] text:2=entry/leaf [CALCULATED] containers/folders/title:1=open subtree,0=closed subtree)
	f05: (String) parentID
	f06: (String) recordIDX - order
	f07: (String) recordID
	x00: (String-Normalized) index for f05:parentID-normalized

// *************************************************************************
// TODO:



*/
//
// *************************************************************************
//
if(!ALiBS.gv.hm) ALiBS.gv.hm = {}; // global values
//
if(!ALiBS.gv.hm.le) ALiBS.gv.hm.le = 0; // local log status
//
ALiBS.gv.hm.nn = 0; // x times loaded
//
ALiBS.gv.hm.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.hm.m0_img0 = ALiBS.gv.sy.sp + "ALiBS/hm.m0.img0.png"; // closed = plus = status
ALiBS.gv.hm.m0_img1 = ALiBS.gv.sy.sp + "ALiBS/hm.m0.img1.png"; // open = minus = status
ALiBS.gv.hm.m0_img2 = ALiBS.gv.sy.sp + "ALiBS/hm.m0.img2.png"; // leaf = page
//
ALiBS.gv.hm.m0_imgx = ALiBS.gv.sy.sp + "home.gif"; // left
ALiBS.gv.hm.m0_imgy = ALiBS.gv.sy.sp + "arrow2.gif"; // right
//
// *************************************************************************
//
ALiBS.hm = // tree menu
{
	m0: function(t,p) // add ul/li
	// t = (required)(ALiBS.to)table
	// p = (optional)(JSON)parameters {} 
	//	f01 = (String)recordDN
	//	f02 = (String)text
	//	f03 = (String)url
	//	f04 = (String)(ONLY for menu +-)status [FIX] text:2=leaf [CALCULATED] containers/folders/title:1=open,0=closed)
	//
	//	f00 = [FREE][NOTUSED]
	//	ft = (String)filter name
	//
	//	c0 = (String)first ul className
	//	i0 = (String)first ul id = MENU NAME
	//	ai = (String)tags inside "<a>" - ini
	//	af = (String)tags inside "<a>" - fim
	//	sc = (String)span class
	//
	//	hm0 = (String)onclick function name to hide/show/save items (NOT function pointer/reference)
	//	sh = (number)show recordID in text
	//		0 = do NOT show = DEFAULT
	//		1 = show
	// return (String)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hm.m0",le:ALiBS.gv.hm.le});
		//
		//ALiBS.hm._ur(D,ALiBS.gv.ck.ct,"ck.menuT.wbs0.teste","menuT.wbs0.teste"); // update menu with cookie state TODO(2012-08-11):passar por parametro ck.menuT...
		//
		var z = {}; // [BEST-PRACTICE]
		//
		z.a = new Array(); // result
		z.lt = t.rs.length;
		//
		z.nc = 0; // counter
		z.cl = null; // split = current level
		z.nl = null; // split = next level
		//
		if(p)
		{
			if(typeof(p.f01)=="string") z.f01 = p.f01;
			if(typeof(p.f02)=="string") z.f02 = p.f02;
			if(typeof(p.f03)=="string") z.f03 = p.f03;
			if(typeof(p.f04)=="string") z.f04 = p.f04;
			//
			if(typeof(p.sc)=="string")
			{
				z.sc = "class='"+p.sc+"'";
				//
				z.si0 = 'style="background-image:url('+ALiBS.gv.hm.m0_img0+')"';
				z.si1 = 'style="background-image:url('+ALiBS.gv.hm.m0_img1+')"';
				z.si2 = 'style="background-image:url('+ALiBS.gv.hm.m0_img2+')"';
			}
			//
			if(typeof(p.sh)=="number") z.sh = p.sh;
			//
			if(typeof(p.hm0)=="string") z.hm0 = 'onclick="' + p.hm0 + '(this);"';
			//
			//if(typeof(p.ft)=="string") z.ft = p.ft;
			//
			if(typeof(p.ai)=="string") z.ai = p.ai;
			//
			if(typeof(p.af)=="string") z.af = p.af;
			//
			if(typeof(p.i0)=="string") z.i0 = 'id="i_hmm0_ul0_'+p.i0+'"';
			//
			if(typeof(p.c0)=="string")
			{
				z.c0 = "class='"+p.c0+"'";
				z.c0l = "class='"+p.c0+"_ls'"; // span left img
				z.c0r = "class='"+p.c0+"_rs'"; // span right img
			}
		}
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.f01)=="undefined") z.f01 = "f01"; // DEFAULT 
		if(typeof(z.f02)=="undefined") z.f02 = "f02"; // DEFAULT 
		if(typeof(z.f03)=="undefined") z.f03 = "f03"; // DEFAULT 
		if(typeof(z.f04)=="undefined") z.f04 = "f04"; // DEFAULT 
		if(typeof(z.f05)=="undefined") z.f05 = "f05"; // DEFAULT 
		if(typeof(z.f08)=="undefined") z.f08 = "f08"; // DEFAULT 
		//
		if(typeof(z.sc)=="undefined")
		{
			z.sc = ""; // DEFAULT 
			//
			z.si0 = ''; // DEFAULT 
			z.si1 = ''; // DEFAULT 
			z.si2 = ''; // DEFAULT 
		}
		//
		if(typeof(z.sh)=="undefined") z.sh = 0; // DEFAULT = do NOT show recordID
		//if(typeof(z.ft)=="undefined") z.ft = ""; // DEFAULT 
		if(typeof(z.ai)=="undefined") z.ai = ""; // DEFAULT 
		if(typeof(z.af)=="undefined") z.af = ""; // DEFAULT 
		if(typeof(z.i0)=="undefined") z.i0 = ""; // DEFAULT 
		if(typeof(z.hm0)=="undefined") z.hm0 = 'onclick="ALiBS.hm._m0h('+t.tb.rf+',this,'+"'"+p.i0+"'"+',{f04:'+"'"+z.f04+"'"+'});"';
		//
//console.log("z.hm0="+z.hm0)
		if(typeof(z.c0)=="undefined")
		{
			z.c0 = ""; // DEFAULT 
		}
		//
		if(z.c0!="" || z.i0!="") z.a[z.a.length] = "<ul "+z.i0+" "+z.c0+">";
		//
		for(var j=0;j<z.lt;j++) // records
		{
			//if(z.ft!="" && t.ft[z.ft] && !t.ft[z.ft](j)) continue;
			//
			z.a[z.a.length] = '<li id="i_hmm0_li_'+t.rs[j][z.f01]+'_'+p.i0+'">';
			//
			if(z.sc || z.si0)
			{
				if(!t.rs[j][z.f04] || t.rs[j][z.f04]=="0")
				{
					z.a[z.a.length] = '<span id="i_hmm0_span_'+t.rs[j][z.f01]+'_'+p.i0+'" '+z.sc+' '+z.si0+' '+z.hm0+'></span>';
				}
				else if(t.rs[j][z.f04]=="1")
				{
					z.a[z.a.length] = '<span id="i_hmm0_span_'+t.rs[j][z.f01]+'_'+p.i0+'" '+z.sc+' '+z.si1+' '+z.hm0+'></span>';
				}
				else // page
				{
					z.a[z.a.length] = '<span id="i_hmm0_span_'+t.rs[j][z.f01]+'_'+p.i0+'" '+z.sc+' '+z.si2+'></span>';
				}
			}
			//
			if(z.sh==1) // show recordID
			{
				z.a[z.a.length] = '<a href="'+t.rs[j][z.f03]+'">'+z.ai+t.rs[j][z.f02]+z.af+' ('+t.rs[j][z.f01]+')';
				//z.a[z.a.length] = '</a>';
			}
			else // do NOT show recordID
			{
				z.a[z.a.length] = '<a href="'+t.rs[j][z.f03]+'">'+z.ai+t.rs[j][z.f02]+z.af;
				//z.a[z.a.length] = '</a>';
			}
			//
			if(!z.sc)
			{
				if(t.rs[j][z.f08]) 
				{
					z.a[z.a.length] = '<span ' + z.c0l + ' style="background:url(' + ALiBS.gv.sy.sp + t.rs[j][z.f08] + ');';
					z.a[z.a.length] = '"></span>';
				}
				else if(ALiBS.gv.hm.m0_imgx)
				{
					z.a[z.a.length] = '<span ' + z.c0l + ' style="background:url(' + ALiBS.gv.hm.m0_imgx + ');';
					z.a[z.a.length] = '"></span>';
				}
				else
				{
					// nothing
				}
				//
				if(t.rs[j][z.f04]!="2" && ALiBS.gv.hm.m0_imgy)
				{
					z.a[z.a.length] = '<span ' + z.c0r + ' style="background:url(' + ALiBS.gv.hm.m0_imgy + ');';
					z.a[z.a.length] = '"></span>';
				}
			}
			//
			z.a[z.a.length] = '</a>';
			//
			if(j+1<z.lt)
			{
				if(t.rs[j+1][z.f01].indexOf(t.rs[j][z.f01])!=0) // closing
				{
					z.cl = t.rs[j][z.f01].split(".").length; // current level
					z.nl = t.rs[j+1][z.f01].split(".").length; // next level
					//
					while(z.cl-z.nl-z.nc>=0)
					{
						if(z.nc>0) z.a[z.a.length] = '</ul>';
						z.a[z.a.length] = '</li>';
						z.nc++;
					}
					//
					z.nc = 0;
				}
				else
				{
					if(z.sc || z.si0)
					{
						if(!t.rs[j][z.f04] || t.rs[j][z.f04]=="0")
						{
							z.a[z.a.length] = "<ul id='i_hmm0_ul_"+t.rs[j][z.f01]+'_'+p.i0+"' style='display:none'>";
						}
						else if(t.rs[j][z.f04]=="1")
						{
							z.a[z.a.length] = "<ul id='i_hmm0_ul_"+t.rs[j][z.f01]+'_'+p.i0+"' style='display:block'>";
						}
					}
					else
					{
						z.a[z.a.length] = "<ul id='i_hmm0_ul_"+t.rs[j][z.f01]+"'>";
					}
				}
			}
		}	
		//
		if(z.c0!="" || z.i0!="") z.a[z.a.length] = '</ul>';
		//
		var r = z.a.join("");
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	_m0h: function(t,s,i0,p) // show/hide/save cookie
	// t = (required)(ALiBS.to)table
	// s = (required)(element)span
	// i0 = (required)(String)menu name
	// p = (optional)(JSON)parameters {} 
	//	f04 = (String)(ONLY for menu +-)status [FIX] text:2=leaf [CALCULATED] containers/folders/title:1=open,0=closed)
	// return = NONE
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hm.m0h",le:ALiBS.gv.hm.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		//z.b // [INFO]
		//z.j // [INFO]
		//z.id = s.id.substring("iSpan".length,s.id.length);
		//z.eu = ALiBS.el.ge("iUl"+z.id);
		
		z.id = s.id.replace("i_hmm0_span_","");
		z.id = z.id.replace("_"+i0,"");

//console.log("s.id="+s.id)
//console.log("z.id="+z.id)

		//z.eu = ALiBS.el.ge(s.id.replace("i_hmm0_span_","i_hmm0_ul_")+i0);

		z.eu = "i_hmm0_ul_"+z.id+"_"+i0;

//console.log("z.eu="+z.eu)

		z.eu = ALiBS.el.ge(z.eu);


		//
		if(p)
		{
			if(typeof(p.f04)=="string") z.f04 = p.f04;
		}
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.f04)=="undefined") z.f04 = "f04"; // DEFAULT 
		//
		if(z.eu)
		{
			z.b = (z.eu.style.display=="block");
			z.eu.style.display=(z.b)?"none":"block";			
			s.style.backgroundImage = (!z.b)?"url("+ALiBS.gv.hm.m0_img1+")":"url("+ALiBS.gv.hm.m0_img0+")";
			//
			z.j = ALiBS.to.of(t,z.id); // must be sorted by f01 = recordDN
			//
/*
console.log("t.so.y="+t.so.y)
console.log("t.so.g="+t.so.g)
console.log("z.id="+z.id)
console.log("z.j="+z.j)
*/
			if(z.b) t.rs[z.j][z.f04] = "0";
			else t.rs[z.j][z.f04] = "1"; 
			//
			//ALiBS.hm._us(D,ALiBS.gv.ck.ct,"ck.menuT.wbs0.agendaTarefas","menuT.wbs0.agendaTarefas"); // TODO(2012-08-11):passar por parametro ck.menuT...
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
	},
	// *****************************************************************
	_m0r: function(mt,ct,cr,m) // read cookie reference in cookie table and set state in menu table
	// mt = (required)(ALiBS.to)menu table
	// ct = (required)(ALiBS.to)cookie table
	// cr = (required)(String)cookie reference
	// m = (required)(String)menu name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hm.m0r",le:ALiBS.gv.hm.le});
		//
		var n = ALiBS.to.of(ct,cr);
		//
		var a;
		var j=0;
		//
		if(n>=0) // record found
		{
			a = ct.rs[n].f01;
			//
			if(a)
			{
				for(var i=0;i<a.length;i++)
				{
					if(mt.ft[m] && !mt.ft[m](i)) continue;
					//
					mt.rs[i].f04 = a.substring(j,j+1);
					j++;
				}
			}
		}
	},
	// *****************************************************************
	_m0s: function(mt,ct,cr,m) // save cookie reference in cookie table
	// mt = (required)(ALiBS.to)menu table
	// ct = (required)(ALiBS.to)cookie table
	// cr = (required)(String)cookie reference
	// m = (required)(String)menu name
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hm._m0s",le:ALiBS.gv.hm.le});
		//
		var n = ALiBS.to.of(ct,cr);
		//
		var a = new Array();
		//
		for(var i=0;i<mt.rs.length;i++)
		{
			if(mt.ft[m] && !mt.ft[m](i)) continue;
			//
			if(!mt.rs[i].f04 || mt.rs[i].f04 == "0") a[a.length] = "0";
			else a[a.length] = mt.rs[i].f04;
		}
		//
		if(n>=0) ct.rs[n].f01 = a.join("");
		else
		{
			ct.rs[ct.rs.length] = 
			{
				f00:cr,
				f01:a.join("")
			};
		}
	},
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	// *****************************************************************
	m1: function(t,c0,p) // 
	// t = (required)(ALiBS.to)table
	// c0 = (required)(string)class c0 (div)
	// p = (optional)(JSON)parameters {ft:"menu1"} 
	//	ft = (String)filter name
	// return (Array) 	<div class=cMenu02a><a class=cMenu02a_a href="#nogo">Option1</a>
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hm.m1",le:ALiBS.gv.hm.le});
		//
		var z = {}; // [BEST-PRACTICE]
		//
		// local varibles [BEST-PRACTICE]
		z.a = new Array();
		//
		if(p)
		{
			if(typeof(p.ft)=="string") z.ft = p.ft;
		}
		//  DEFAULTS [BEST-PRACTICE]
		if(typeof(z.ft)=="undefined") z.ft = ""; // DEFAULT
		//
		z.a[z.a.length] = '<div class="'+c0+'">';
		//
		for(var j=0;j<t.rs.length;j++) // records
		{
			if(t.ft[z.ft] && !t.ft[z.ft](j)) break;
			//
			z.a[z.a.length] = '<a class="'+c0+'a'+(j%2)+'" href="'+t.rs[j].f03+'"><em></em><b>'+t.rs[j].f02+'</b></a>';
		}
		//
		z.a[z.a.length] = '</div>';
		//
		var r = z.a.join("");
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	},
	// *****************************************************************
	m2: function(t,p) // 
	// t = (required)(ALiBS.to)table
	// p = (optional)(JSON)parameters {} 
	//	
	// return (Array) Html
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.hm.m2",le:ALiBS.gv.hm.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		// DEFAULTS [BEST-PRACTICE]
		//if(typeof(z.h0)!="number") z.h0 = 1; // show field header = DEFAULT
		//
		// local varibles [BEST-PRACTICE]
		z.a = new Array();
		z.m; // match
		//
		for(var j=0;j<t.rs.length;j++) // records
		{
			//if(t.ft[z.ft] && !t.ft[z.ft](j)) break;
			//
			z.m = t.rs[j].f01.match(/\./gi);
			//
			if(!z.m) // Menu = no "." = [0]
			{
				if(j>0)
				{
					z.a[z.a.length] = '</dl>';
					z.a[z.a.length] = "</div></li>";
				}
				//
				z.a[z.a.length] = '<li><a href="' + t.rs[j].f03 +'">' + t.rs[j].f02 + "</a>";
				z.a[z.a.length] = '<div>';
			}
			else if(z.m.length==1) // Submenu = "." = [0.0]
			{
				if(j>1)
				{
					z.a[z.a.length] = '</dl>';
				}
				//
				z.a[z.a.length] = '<dl>';
				z.a[z.a.length] = '<dt><a href="' + t.rs[j].f03 + '">' + t.rs[j].f02 + '</a></dt>';
			}
			else // Option > 1 "." = [0.0.0]
			{
				z.a[z.a.length] = '<dd><a href="' + t.rs[j].f03 + '">' + t.rs[j].f02 + '</a></dd>';
			}
		}
		//
		z.a[z.a.length] = '</dl>';
		z.a[z.a.length] = "</div></li>";
		//
		var r = z.a.join("");
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		return r;
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.hm("+(++ALiBS.gv.hm.nn)+"x)",{o:"ALiBS.hm"});
ALiBS.sy.la(0+0,"ALiBS.gv.hm.le="+ALiBS.gv.hm.le+ALiBS.gv.sy.se,{o:"ALiBS.hm"});

