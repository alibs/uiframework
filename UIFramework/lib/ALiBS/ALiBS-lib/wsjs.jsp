<%@  page pageEncoding="utf-8" 
contentType="text/html;charset=utf-8" 
import="java.util.*,claypool.server.*" %>

<%
	String ret = "NOT-INITIALIZED";
	//
	try
	{
		System.out.println("\n\n\n\n(wsjs.jsp)version 15.0617.1638");
		//
		// *********************************************************
		//
		//int lctx = 0; // change to get connection
		//
		Shell shell = (Shell) getServletConfig().getServletContext().getAttribute("shell"); // loads 1 time, for server
		//
		if(shell==null)
		{
			System.out.println("(wsjs.jsp)Loading WSJS Engine");
			//
			java.util.Properties prop = new java.util.Properties();
			prop.load(getServletContext().getResourceAsStream("/WEB-INF/wsjs.properties"));
			//
			String apcx = prop.getProperty("apcx");
			String aprt = prop.getProperty("aprt");
			String apap = prop.getProperty("apap");
			//
			System.out.println("(wsjs.jsp)apcx="+apcx);
			System.out.println("(wsjs.jsp)aprt="+aprt);
			System.out.println("(wsjs.jsp)apap="+apap);
			//
			shell = new Shell(apcx,aprt,apap+"/ALiBS-load.js","true","1");
			getServletConfig().getServletContext().setAttribute("shell",shell);
			//
			System.out.println("(wsjs.jsp)WSJS Engine Started\n\n\n\n");
		}
		else
		{
			System.out.println("(wsjs.jsp)WSJS Engine Already Loaded\n\n\n\n");
		}
		//
		// *********************************************************
		//
		Object[] pargs = {"za,zb,zc",1,true,"cccc"};
		Object p = shell.callGlobalFunction("ALiBS_sy_no",pargs);
		//
		//
		session.setAttribute("test0","testing0"); // test for passing session to javascript
		// do NOT remove - see http://alibs.local:8099/WEB/alibs/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.ws.wsc.if.session.getAttribute&p0=test0
		//
		//Object[] args = {lctx,request,response,p};
		Object[] args = {session,request,response,p};
		ret = "" + shell.callGlobalFunction("ALiBS_ws_js",args);
	}
	catch(Exception e) 
	{
		ret = "(wsjs.jsp)EXCEPTION=" + e.toString();
	}
	//
	System.out.println("(wsjs.jsp)length="+ret.length());
	//System.out.println("(wsjs.jsp)ret="+ret);
	//
	out.println(ret); // == Return (trocado por causa do retorno de ALiBS.to)
	//
	//out.println(cb+"(" + "'" + ret + "'" + ");"); // == Return (trocado por causa do retorno de ALiBS.to)
%>

