/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

*/
//
// *************************************************************************
//
if(!ALiBS.gv.op) ALiBS.gv.op = {}; // global values
//
if(!ALiBS.gv.op.le) ALiBS.gv.op.le = 0; // local log status
//
ALiBS.gv.op.nn = 0; // x times loaded
//
ALiBS.gv.op.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.op = 
{
	co: function(t,n) // contains option
	// t=(required)(number)total options
	// n=(required)(number)option
	// return=(boolean)true|false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.co",le:ALiBS.gv.op.le});
		//
		var a = ALiBS.op.go(t);
		for(var i=0;i<a.length;i++)
		{
			//document.write("a["+i+"]="+a[i]+"<br>");
			if(a[i]==n) return true;
		}
		//
		return false;
	},
	// *****************************************************************
	c2: function(a,n) // contains option
	// a = (required)(array)options
	// n=(required)(number)option
	// return=(boolean)true|false
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.c2",le:ALiBS.gv.op.le});
		//
		for(var i=0;i<a.length;i++)
		{
			if(a[i]==n) return true;
		}
		//
		return false;
	},
	// *****************************************************************
	go: function(t) // get options
	// t=(required)(number)total options
	// return=(array)options
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.go",le:ALiBS.gv.op.le});
		//
		var a = new Array();
		for(var i=0;(a[i]=Math.pow(2,i))<t;i++)
		{
		} // potencias: 1,2,4,8,16,32
		var s=0; // soma
		//
		for(var i=(a.length-1);i>=0;i--)
		{
			if(a[i]<t)
			{
				if(s+a[i]<t) 
				{
					s+=a[i];
				}
				else if(s+a[i]==t) 
				{
					s+=a[i];
				}
				else if(s+a[i]>t) a[i]=-2;
			}
			//else if(a[i]==t) return a[i];
			else if(a[i]==t) s+=a[i];
			else if(a[i]>t) a[i]=-1;
		}
		//
		//document.write("a="+a.join()+"<br>");
		//document.write("a.length="+a.length+"<br>");
		var b = new Array();
		for(var i=0;i<a.length;i++)
		{
			if(a[i]>0) b[b.length]=i;
		}
		//
		//document.write("b="+b.join()+"<br>");
		return b; // .join();
	},
	// *****************************************************************
	no: function(t) // number of options 
	// t=(required)(number)total options
	// return=(number)number of options
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.no",le:ALiBS.gv.op.le});
		//
		var a = ALiBS.op.go(t);
		//document.write("a="+a+"<br>");
		return a.length; 
	},
	// *****************************************************************
	so: function(a) // set options - generally initial call
	// a = (required)(array)options
	// return=(number)total options
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.so",le:ALiBS.gv.op.le});
		//
		var t=0;
		for(var i=0;i<a.length;i++)
		{
			t+=Math.pow(2,a[i]);
		}
		//
		return t;
	},
	// *****************************************************************
	ao: function(t,n) // add option
	// t=(required)(number)total options
	// n=(required)(number)option
	// return=(number) novo total
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.ao",le:ALiBS.gv.op.le});
		//
		var c = ALiBS.op.co(t,n);
		//
		if(c) return t; // already contains, do not add
		else return t+Math.pow(2,n); // add
	},
	// *****************************************************************
	ro: function(t,n) // remove option
	// t=(required)(number)total options
	// n=(required)(number)option
	// return=(number) novo total
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.op.ro",le:ALiBS.gv.op.le});
		//
		var c = ALiBS.op.co(t,n);
		//
		if(c) return t-Math.pow(2,n); // contains, remove
		else return t; // do not contains
	}
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.op("+(++ALiBS.gv.op.nn)+"x)",{o:"ALiBS.op"});
ALiBS.sy.la(0+0,"ALiBS.gv.op.le="+ALiBS.gv.op.le+ALiBS.gv.sy.se,{o:"ALiBS.op"});

