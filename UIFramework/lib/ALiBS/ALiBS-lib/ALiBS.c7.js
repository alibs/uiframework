/* *************************************************************************
// INFO:

Hash (message digest): http://pt.wikipedia.org/wiki/Hash

 - hash = seqüencia de bits geradas por um algoritmo de dispersão;
 - O processo é unidirecional e impossibilita descobrir o conteúdo original a partir do hash;
 - O valor de conferência ("check-sum") muda se um único bit for alterado, acrescentado ou retirado da mensagem;
 - em geral representada em base hexadecimal;
 - usado para identificar um arquivo, informação ou senha unicamente (resultado (quase) exclusivo);
 - existem colisões (seqüências iguais para dados diferentes);
 - o ideal seria gerar identificadores únicos para os valores de entrada;
 - mais usados: 
	- md5 (16 bytes = 128 bits)  RSA Data Security, Inc. (RFC 1321) muito utilizado por softwares com protocolo P2P - Peer-to-Peer;
	- SHA-1 (20 bytes = 160 bits) (Secure Hash Algorithm) (NIST e NSA). Já foram exploradas falhas no SHA;
	- SHA-512 (64 bytes = 512 bits)
	- WHIRLPOOL: (Vincent Rijmen (co-autor do AES) e Paulo S. L. M. Barreto) recomendada pelo projeto NESSIE (Europeu), 
		adotado pelo ISO e IEC como parte do padrão internacional ISO 10118-3.

	- SuperHash
		- Hash mantem o tamanho
		- com b64 o tamanho dobra

****************************************************************************

Hash (message digest) MD5: http://pt.wikipedia.org/wiki/MD5    http://pajhome.org.uk/crypt/md5/

- Vulnerabilidade:
	- Como o MD5 faz apenas uma passagem sobre os dados, se dois prefixos com o mesmo hash forem construídos, 
	- Um sufixo comum pode ser adicionado a ambos para tornar uma colisão mais provável. 
	- Deste modo é possível que duas strings diferentes produzam o mesmo hash.

******************************

Hash (message digest) SHA-1: http://pajhome.org.uk/crypt/md5/    http://pt.wikipedia.org/wiki/SHA-1    http://pajhome.org.uk/crypt/md5/scripts.html (jshash-2.2.zip)

 - usada numa grande variedade de aplicações e protocolos de segurança, incluindo TLS, SSL, PGP, SSH, S/MIME e IPSec;
 - SHA-1 foi considerado o sucessor do MD5. 
 - Ambos têm vulnerabilidades comprovadas;
 - Em algumas correntes, é sugerido que o SHA-256 ou superior seja usado para tecnologia crítica;
 - Os algoritmos SHA foram desenhados pela National Security Agency (NSA) e publicados como um padrão do governo Norte-Americano.
 - variantes: SHA-224, SHA-256, SHA-384, e SHA-512 — por vezes chamadas de SHA-2.
 - Ainda não foram reportados ataques às variantes SHA-2;

****************************************************************************

Hash based key strengthening: http://en.wikipedia.org/wiki/Key_strengthening

function superHash(n,pwd,salt)
// n = (required)(number)times to run
// pwd = (required)(String)password
// salt = (required)(String)salt to be added to strengh hash
{
	var h = pwd;
	for(var i=0;i<n;i++)
	{
		if(i%2==0) 	h = MD5(""+h+salt); // concatenation
		else 		h = SHA1(""+salt+h);
	}
	return h;
}

Strength and time

For these examples assume that the slowest personal computers in use today (2010) can do about 65000 SHA-1 hashes in one second using compiled code. 
Thus a program that uses key strengthening can use 65000 rounds of hashes and delay the user for at most one second.

Testing a trial password or passphrase typically requires one hash operation. 
But if key strengthening was used, the attacker must compute strengthened key for each key they test, meaning there are 65000 hashes to compute per test. 
This increases the attacker's workload by a factor of 65000, approximately 216 operations, which means the enhanced key is "worth" about an additional 16 bits in key strength.

The commonly accepted Moore's law states that computer speed doubles about every 1.5 years. 
Under this assumption, every 1.5 years one more bit of key strength is plausibly brute-forcible. 
This implies that 16 extra bits of strength is worth about 16×1.5 = 24 years later cracking, 
but it also means that the number of key strengthening rounds a system uses 
should be doubled about every 1.5 years to maintain the same level of security. 

(Since most keys are more secure than necessary, systems that require consistent deterministic key generation will likely not update the number of iterations used in key strengthening. 
In such a case, the designer should take into consideration how long they wish for the key derivation system to go unaltered and should choose an appropriate number of hashes for the lifespan of the system.)

Modern password-based key derivation functions, such as PBKDF2 (specified in RFC 2898), 
use a cryptographic hash, such as MD5 or SHA1, more salt (e.g. 64 bits) and a high iteration count (often 1000 or more). 

****************************************************************************

http://en.wikipedia.org/wiki/Salt_%28cryptography%29

Assume a user’s (encrypted) secret key is stolen and he is known to use one of 200,000 English words as his password. 

The system uses a 32-bit salt (4bytes)

The salted key is now the original password appended to this random 32-bit salt. 
Because of this salt, the attacker’s pre-calculated hashes are of no value. 
He must calculate the hash of each word with each of 2^32 (4,294,967,296) possible salts appended until a match is found. 
The total number of possible inputs can be obtained by multiplying the number of words in the dictionary with the number of possible salts:

2^{32} X 200.000 = 8.58993459 X 10^{14}

To complete a brute-force attack, the attacker must now compute about 800 trillion hashes, instead of only 200,000. 
Even though the password itself is known to be simple, the secret salt makes breaking the password increasingly difficult.

2^32 = 4,294,967,296 of possibilities

Total = 200.000 X 2^32 = 200.000 X 4,294,967,296 = more than 800.000.000.000.000 = more than 800 trillion possibilities

****************************************************************************

800.000.000.000.000
80.000 per second
= 10.000.000.000 seconds
= +- 3.000.000 horas
= 125.000 dias
= 342 anos

****************************************************************************

DESAFIO = Hash-based Message Authentication Code

http://en.wikipedia.org/wiki/HMAC
http://en.wikipedia.org/wiki/Message_authentication_code

Definition (from RFC 2104)
SHA-1 HMAC Generation.

Let:

    * H(·) be a cryptographic hash function
    * K be a secret key padded to the right with extra zeros to the block size of the hash function
    * m be the message to be authenticated
    * ∥ denote concatenation
    * ⊕ denote exclusive or (XOR)
    * opad be the outer padding (0x5c5c5c…5c5c, one-block-long hexadecimal constant)
    * ipad be the inner padding (0x363636…3636, one-block-long hexadecimal constant)

Then HMAC(K,m) is mathematically defined by

    HMAC(K,m) = H((K ⊕ opad) ∥ H((K ⊕ ipad) ∥ m)).

******************************

Implementation

The following pseudocode demonstrates how HMAC may be implemented.

function hmac (key, message)
    if (length(key) > blocksize) then
        key = hash(key) // keys longer than blocksize are shortened
    else if (length(key) < blocksize) then
        key = key ∥ zeroes(blocksize - length(key)) // keys shorter than blocksize are zero-padded
    end if
    
    opad = [0x5c * blocksize] ⊕ key // Where blocksize is that of the underlying hash function
    ipad = [0x36 * blocksize] ⊕ key // Where ⊕ is exclusive or (XOR)
    
    return hash(opad ∥ hash(ipad ∥ message)) // Where ∥ is concatenation
end function

****************************************************************************

HMAC usage

- The cryptographic strength of the HMAC depends upon the size of the secret key that is used;

1) To authenticate a message (password needed)(Assinatura)(verify both the data integrity and the authenticity of a message)
	- userA sends the message and the HMAC-A(pass,msg) to user B
	- userB calculates HMAC-B(pass,msg) and verify HMAC-A == HMAC-B

2) To authenticate the client (password needed)(challenge)
	- server sends HMAC-S(pass,msg) (msg = random string) (login page)
	- client sends HMAC-C(pass,HMAC-S)
	- server calculates HMAC-S2(pass,HMAC-S) and verify HMAC-C == HMAC-S2

// *************************************************************************

TODO(2011-02-22): MUITO IMPORTANTE: ESTUDAR : http://c2.com/cgi/wiki?HmacUserAuthentication

http://en.wikipedia.org/wiki/Mutual_authentication
Mutual authentication or two-way authentication

******************************

TODO(2011-02-22): Ideia boa: http://en.wikipedia.org/wiki/Two-factor_authentication

Mobile Phones

VeriSign Identity Protection Access for Mobile Credential
There is presently only limited discussion on using wired phones for authentication, most applications focus on use of mobile phones instead.
A new category of T-FA tools transforms the PC user's mobile phone into a token device using SMS messaging, an interactive telephone call, or via downloadable application to a smartphone. 
Since the user now communicates over two channels, the mobile phone becomes a two-factor, two-channel authentication mechanism.

******************************

p = 23
base g = 5

Facil de calcular "a" por força bruta 
A = 5^a mod 23 = 8
for(a=1;a<9999;a++){ if(Math.pow(5,a)%23==8) return a;}

Para se tornar viavel é necessário uma biblioteca bigInt (inteiros grandes) 
- ALiBS.bn (big number) usar string ao inves de int "-1234.5678"
- medir tempo de processamento com numeros com 32 bytes, 64, 128, 256, 512
- tempo para operacoes basicas: somar, subtrair, multiplicar, dividir, resto, expoente
- PRINCIPAL: tempo para calcular x^y (32,64,128,etc) pois é usado na força bruta
- somente será viavel se o tempo for maior que 1hora, que será o tempo máximo da sessão
- outra opção seria trocar a chave da sessão a cada 5 minutos automaticamente (ficaria imune a eavesdroping/sniffing)(não imune a man-in-the-middle)

http://blog.thatscaptaintoyou.com/introducing-big-js-arbitrary-precision-math-for-javascript/ (https://github.com/whatgoodisaroad/Big-js/downloads)
http://ohdave.com/rsa/BigInt.js
http://www-cs-students.stanford.edu/~tjw/jsbn/


Facil de implementar
http://en.wikipedia.org/wiki/Diffie-Hellman_key_exchange
- susceptivel a man-in-the-middle
- Safe primes p: (p-1)/2 is also prime

Implementar tambem SPEKE, onde g é uma password (integer) conhecidos pelo servidor e pelo cliente e nao sao trocados/transmitidos via link
- NÃO susceptivel (imune) a man-in-the-middle
- Pode ser uma opcao para criar a session-key (identificador / password da sessão)

Eavesdropping (computadores) é uma técnica de hacking que se baseia na violação da confidencialidade. 
Uma analogia bastante razoável seria a ação de grampear um telefone. 
É uma leitura não autorizada de mensagens.

/ **************************************************************************

ALiBS.c9.e4(ALiBS.c7.sh(1000,"salt","abcd","1234"),{n:"uri"})
"woQ6w6LCqAXDvcO_wrZ0bcK3eDFxO2LDmhDDocKkw60ZUybCiEVTw6YDHg-ChmAAwocswqfCgsKtQ8O1wrnCpMKQwrPCgEnDsV-DoRTDulDCnjLDtxc9TxfDtnZMw6U."

string: "abcd"

	http://hash.online-convert.com/sha512-generator

		D8022F2060AD6EFD297AB73DCC5355C9B214054B0D1776A136A669D26A7D3B14F73AA0D0EBFF19EE333368F0164B6419A96DA49E3E481753E7E96B716BDCCB6F

		2AIvIGCtbv0perc9zFNVybIUBUsNF3ahNqZp0mp9OxT3OqDQ6/8Z7jMzaPAWS2QZqW2knj5IF1Pn6Wtxa9zLbw==

	http://www.convertstring.com/Hash/SHA512

		D8022F2060AD6EFD297AB73DCC5355C9B214054B0D1776A136A669D26A7D3B14F73AA0D0EBFF19EE333368F0164B6419A96DA49E3E481753E7E96B716BDCCB6F

	http://www.miniwebtool.com/sha512-hash-generator/

		d8022f2060ad6efd297ab73dcc5355c9b214054b0d1776a136a669d26a7d3b14f73aa0d0ebff19ee333368f0164b6419a96da49e3e481753e7e96b716bdccb6f

	b = ALiBS.c9.e4(  ALiBS.c7.s5("abcd")  )	
			TODO(2015-07-03):PROBLEMA: meu base64 retorna um valor diferente do site
					Para textos em ASCII, retorna correto aparentemente
		w5gCLyBgwq1uw70pesK3PcOMU1XDicKyFAVLDRd2wqE2wqZpw5JqfTsUw7c6wqDDkMOrw78Zw64zM2jDsBZLZBnCqW3CpMKePkgXU8Onw6lrcWvDnMOLbw==

	b = ALiBS.c9.sh(  ALiBS.c7.s5("abcd")  )	o retorno em hexa esta igual, o que significa que o SHA-512 esta correto
		D8022F2060AD6EFD297AB73DCC5355C9B214054B0D1776A136A669D26A7D3B14F73AA0D0EBFF19EE333368F0164B6419A96DA49E3E481753E7E96B716BDCCB6F





	https://www.base64encode.org/


		ALiBS.c7.s5("abcd") to base 64
		w5gCLyBgwq1uw70pesK3PcOMU1XDicKyFAVLDQoXdsKhNsKmacOSan07FMO3OsKgw5DDq8O/GcOuMzNow7AWS2QZwqltwqTCnj5IF1PDp8Opa3Frw5zDi28=


	http://www.motobit.com/util/base64-decoder-encoder.asp


		ALiBS.c7.s5("abcd") to base 64
		2AIvIGCtbv0perc9zFNVybIUBUsNChd2oTamadJqfTsU9zqg0Ov/Ge4zM2jwFktkGaltpCYjMTU4Oz5IF1Pn6Wtxa9zLbw==

	http://base64decode.net/

		ALiBS.c7.s5("abcd") to base 64
		IsOYAi8gYMKtbsO9KXrCtz3DjFNVw4nCshQFSw0KF3bCoTbCpmnDkmp9OxTDtzrCoMOQw6vDvxnDrjMzaMOwFktkGcKpbcKkwp4+SBdTw6fDqWtxa8Ocw4tvIg==


	https://paulschou.com/tools/xlate/

		d8 02 2f 20 60 ad 6e fd 29 7a b7 3d cc 53 55 c9 b2 14 05 4b 0d 0a 17 76 a1 36 a6 69 d2 6a 7d 3b 14 f7 3a a0 d0 eb ff 19 ee 33 33 68 f0 16 4b 64 19 a9 6d a4 26 23 31 35 38 3b 3e 48 17 53 e7 e9 6b 71 6b dc cb 6f 
		2AIvIGCtbv0perc9zFNVybIUBUsNChd2oTamadJqfTsU9zqg0Ov/Ge4zM2jwFktkGaltpCYjMTU4Oz5IF1Pn6Wtxa9zLbw==


// *************************************************************************

	b = ALiBS.c9.e4(  ALiBS.c9.sh(  ALiBS.c7.s5("abcd")  )  )
		RDgwMjJGMjA2MEFENkVGRDI5N0FCNzNEQ0M1MzU1QzlCMjE0MDU0QjBEMTc3NkExMzZBNjY5RDI2QTdEM0IxNEY3M0FBMEQwRUJGRjE5RUUzMzMzNjhGMDE2NEI2NDE5QTk2REE0OUUzRTQ4MTc1M0U3RTk2QjcxNkJEQ0NCNkY=

	c = ALiBS.c9.sh(  ALiBS.c7.s5("abcd")  )
		D8022F2060AD6EFD297AB73DCC5355C9B214054B0D1776A136A669D26A7D3B14F73AA0D0EBFF19EE333368F0164B6419A96DA49E3E481753E7E96B716BDCCB6F



	https://paulschou.com/tools/xlate/


		d = ALiBS.c7.s5("abcd")

		a = "d8 02 2f 20 60 ad 6e fd 29 7a b7 3d cc 53 55 c9 b2 14 05 4b 0d 0a 17 76 a1 36 a6 69 d2 6a 7d 3b 14 f7 3a a0 d0 eb ff 19 ee 33 33 68 f0 16 4b 64 19 a9 6d a4 26 23 31 35 38 3b 3e 48 17 53 e7 e9 6b 71 6b dc cb 6f ".replace(/\s/gi,"").toUpperCase()

		D8022F2060AD6EFD297AB73DCC5355C9B214054B0D0A1776A136A669D26A7D3B14F73AA0D0EBFF19EE333368F0164B6419A96DA426233135383B3E481753E7E96B716BDCCB6F"

d8 02 2f 20 60 ad 6e fd 29 7a b7 3d cc 53 55 c9 b2 14 05 4b 0d 0a 17 76 a1 36 a6 69 d2 6a 7d 3b 14 f7 3a a0 d0 eb ff 19 ee 33 33 68 f0 16 4b 64 19 a9 6d a4 26 23 31 35 38 3b 3e 48 17 53 e7 e9 6b 71 6b dc cb 6f 
		RDgwMjJGMjA2MEFENkVGRDI5N0FCNzNEQ0M1MzU1QzlCMjE0MDU0QjBEMTc3NkExMzZBNjY5RDI2QTdEM0IxNEY3M0FBMEQwRUJGRjE5RUUzMzMzNjhGMDE2NEI2NDE5QTk2REE0OUUzRTQ4MTc1M0U3RTk2QjcxNkJEQ0NCNkY=


// *************************************************************************

d = ALiBS.c2.sa(ALiBS.c7.s5("abcd")).replace(/, /gi,",")
[216, 2, 47, 32, 96, 173, 110, 253, 41, 122, 183, 61, 204, 83, 85, 201, 178, 20, 5, 75, 13, 23, 118, 161, 54, 166, 105, 210, 106, 125, 59, 20, 247, 58, 160, 208, 235, 255, 25, 238, 51, 51, 104, 240, 22, 75, 100, 25, 169, 109, 164, 158, 62, 72, 23, 83, 231, 233, 107, 113, 107, 220, 203, 111]

a = "216, 2, 47, 32, 96, 173, 110, 253, 41, 122, 183, 61, 204, 83, 85, 201, 178, 20, 5, 75, 13, 23, 118, 161, 54, 166, 105, 210, 106, 125, 59, 20, 247, 58, 160, 208, 235, 255, 25, 238, 51, 51, 104, 240, 22, 75, 100, 25, 169, 109, 164, 158, 62, 72, 23, 83, 231, 233, 107, 113, 107, 220, 203, 111"

a.replace(/, /gi,",")
"216,2,47,32,96,173,110,253,41,122,183,61,204,83,85,201,178,20,5,75,13,XX,23,118,161,54,166,105,210,106,125,59,20,247,58,160,208,235,255,25,238,51,51,104,240,22,75,100,25,169,109,164,158,62,72,23,83,231,233,107,113,107,220,203,111"



https://paulschou.com/tools/xlate/

216 2 47 32 96 173 110 253 41 122 183 61 204 83 85 201 178 20 5 75 13 10 23 118 161 54 166 105 210 106 125 59 20 247 58 160 208 235 255 25 238 51 51 104 240 22 75 100 25 169 109 164 38 35 49 53 56 59 62 72 23 83 231 233 107 113 107 220 203 111 

a = "216 2 47 32 96 173 110 253 41 122 183 61 204 83 85 201 178 20 5 75 13 10 23 118 161 54 166 105 210 106 125 59 20 247 58 160 208 235 255 25 238 51 51 104 240 22 75 100 25 169 109 164 38 35 49 53 56 59 62 72 23 83 231 233 107 113 107 220 203 111 "

a.replace(/ /gi,",")
"216,2,47,32,96,173,110,253,41,122,183,61,204,83,85,201,178,20,5,75,13,10,23,118,161,54,166,105,210,106,125,59,20,247,58,160,208,235,255,25,238,51,51,104,240,22,75,100,25,169,109,164,38,35,49,53,56,59,62,72,23,83,231,233,107,113,107,220,203,111,"


TODO(2015-07-08): Pesquisar UNICODE - UTF-8 - UTF-16 - UTF-32 (Salvei alguns links em Chrome/myProjects)

*/
//
// *************************************************************************
//
if(!ALiBS.gv.c7) ALiBS.gv.c7 = {}; // global values
//
if(!ALiBS.gv.c7.le) ALiBS.gv.c7.le = 0; // local log status
//
ALiBS.gv.c7.nn = 0; // x times loaded
//
ALiBS.gv.c7.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.c7 = // sha512
{
	s5: function(s) // SHA512 Hash encode
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.s5",le:ALiBS.gv.c7.le});
		//
		return ALiBS.c7.b2s(ALiBS.c7._s5m0(ALiBS.c7.s2b(s), s.length * 8));
	},
	// *****************************************************************
	h5: function(key,data) // HMAC-SHA512
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.h5",le:ALiBS.gv.c7.le});
		//
		var bkey = ALiBS.c7.s2b(key);
		if(bkey.length > 32) bkey = ALiBS.c7._s5m0(bkey, key.length * 8);
		//
		var ipad = Array(32), opad = Array(32);
		for(var i = 0; i < 32; i++)
		{
			ipad[i] = bkey[i] ^ 0x36363636;
			opad[i] = bkey[i] ^ 0x5C5C5C5C;
		}
		//
		var hash = ALiBS.c7._s5m0(ipad.concat(ALiBS.c7.s2b(data)), 1024 + data.length * 8);
		return ALiBS.c7.b2s(ALiBS.c7._s5m0(opad.concat(hash), 1024 + 512));
	},
	// *****************************************************************
	sh: function(n,s,k,d) // Super Hash = HMAC-SHA512
	// n = (required)(number) times to run
	// s = (required)(String) salt
	// k = (required)(String) secret key = password
	// d = (required)(String) data = message
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.sh",le:ALiBS.gv.c7.le});
		//
		var v = d;
		//
		for(var i=0;i<n;i++)
		{
			v = ALiBS.c7.h5(k+s,v+s);
		}
		//
		return v;
	},
	// *****************************************************************
	s2b: function(a) // string to binary // Convert a raw string to an array of little-endian words Characters >255 have their high-byte silently ignored
						//Endianness refers to the ordering of individually addressable sub-components within a longer data item as stored in external memory 
						//(or, sometimes, as sent on a serial connection). 
						//The usual contrast is whether the most-significant (big-endian) or least-significant (little-endian) byte is ordered first
						// (at the smallest address) within the larger data item.
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.s2b",le:ALiBS.gv.c7.le});
		//
		var b = Array(a.length >> 2);
		for(var i = 0; i < b.length; i++) b[i] = 0;
		for(var i = 0; i < a.length * 8; i += 8) b[i>>5] |= (a.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
		return b;
	},
	// *****************************************************************
	b2s: function(a) // binary to string // Convert an array of little-endian words to a string
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.b2s",le:ALiBS.gv.c7.le});
		//
		var b = "";
		for(var i = 0; i < a.length * 32; i += 8) b += String.fromCharCode((a[i>>5] >>> (24 - i % 32)) & 0xFF);
		return b;
	},
	// *****************************************************************
	// *****************************************************************
	_s5m0: function(x,len)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7._s5m0",le:ALiBS.gv.c7.le});
		//
		//Initial hash values
		var H = new Array();
		//
		H[H.length] = new ALiBS.c7.i64(0x6a09e667, -205731576);
		H[H.length] = new ALiBS.c7.i64(-1150833019, -2067093701);
		H[H.length] = new ALiBS.c7.i64(0x3c6ef372, -23791573);
		H[H.length] = new ALiBS.c7.i64(-1521486534, 0x5f1d36f1);
		H[H.length] = new ALiBS.c7.i64(0x510e527f, -1377402159);
		H[H.length] = new ALiBS.c7.i64(-1694144372, 0x2b3e6c1f);
		H[H.length] = new ALiBS.c7.i64(0x1f83d9ab, -79577749);
		H[H.length] = new ALiBS.c7.i64(0x5be0cd19, 0x137e2179);
		//
		var T1 = new ALiBS.c7.i64(0, 0);
		var T2 = new ALiBS.c7.i64(0, 0);
		var a = new ALiBS.c7.i64(0,0);
		var b = new ALiBS.c7.i64(0,0);
		var c = new ALiBS.c7.i64(0,0);
		var d = new ALiBS.c7.i64(0,0);
		var e = new ALiBS.c7.i64(0,0);
		var f = new ALiBS.c7.i64(0,0);
		var g = new ALiBS.c7.i64(0,0);
		var h = new ALiBS.c7.i64(0,0);
		//
		//Temporary variables not specified by the document
		var s0 = new ALiBS.c7.i64(0, 0);
		var s1 = new ALiBS.c7.i64(0, 0);
		var Ch = new ALiBS.c7.i64(0, 0);
		var Maj = new ALiBS.c7.i64(0, 0);
		var r1 = new ALiBS.c7.i64(0, 0);
		var r2 = new ALiBS.c7.i64(0, 0);
		var r3 = new ALiBS.c7.i64(0, 0);
		//
		var j;
		var i;
		var W = new Array(80);
		//
		for(var i=0;i<80;i++) W[i] = new ALiBS.c7.i64(0, 0);
		//
		// append padding to the source string. The format is described in the FIPS.
		x[len >> 5] |= 0x80 << (24 - (len & 0x1f));
		x[((len + 128 >> 10)<< 5) + 31] = len;
		//
		for(var i=0;i<x.length;i+=32) //32 dwords is the block size
		{
			ALiBS.c7.i64c(a, H[0]);
			ALiBS.c7.i64c(b, H[1]);
			ALiBS.c7.i64c(c, H[2]);
			ALiBS.c7.i64c(d, H[3]);
			ALiBS.c7.i64c(e, H[4]);
			ALiBS.c7.i64c(f, H[5]);
			ALiBS.c7.i64c(g, H[6]);
			ALiBS.c7.i64c(h, H[7]);
			//
			for(var j=0;j<16;j++)
			{
				W[j].h = x[i + 2*j];
				W[j].l = x[i + 2*j + 1];
			}
			//
			for(var j=16;j<80;j++)
			{
				//sigma1
				ALiBS.c7.i64r(r1, W[j-2], 19);
				ALiBS.c7.i64v(r2, W[j-2], 29);
				ALiBS.c7.i64s(r3, W[j-2], 6);
				s1.l = r1.l ^ r2.l ^ r3.l;
				s1.h = r1.h ^ r2.h ^ r3.h;
				//sigma0
				ALiBS.c7.i64r(r1, W[j-15], 1);
				ALiBS.c7.i64r(r2, W[j-15], 8);
				ALiBS.c7.i64s(r3, W[j-15], 7);
				s0.l = r1.l ^ r2.l ^ r3.l;
				s0.h = r1.h ^ r2.h ^ r3.h;
				//
				ALiBS.c7.i644(W[j], s1, W[j-7], s0, W[j-16]);
			}
			//
			for(var j=0;j<80;j++)
			{
				//Ch
				Ch.l = (e.l & f.l) ^ (~e.l & g.l);
				Ch.h = (e.h & f.h) ^ (~e.h & g.h);
				//
				//Sigma1
				ALiBS.c7.i64r(r1, e, 14);
				ALiBS.c7.i64r(r2, e, 18);
				ALiBS.c7.i64v(r3, e, 9);
				//
				s1.l = r1.l ^ r2.l ^ r3.l;
				s1.h = r1.h ^ r2.h ^ r3.h;
				//
				//Sigma0
				ALiBS.c7.i64r(r1, a, 28);
				ALiBS.c7.i64v(r2, a, 2);
				ALiBS.c7.i64v(r3, a, 7);
				//
				s0.l = r1.l ^ r2.l ^ r3.l;
				s0.h = r1.h ^ r2.h ^ r3.h;
				//
				//Maj
				Maj.l = (a.l & b.l) ^ (a.l & c.l) ^ (b.l & c.l);
				Maj.h = (a.h & b.h) ^ (a.h & c.h) ^ (b.h & c.h);
				//
				ALiBS.c7.i645(T1, h, s1, Ch, ALiBS.c7._s5k[j], W[j]);
				ALiBS.c7.i642(T2, s0, Maj);
				//
				ALiBS.c7.i64c(h, g);
				ALiBS.c7.i64c(g, f);
				ALiBS.c7.i64c(f, e);
				ALiBS.c7.i642(e, d, T1);
				//
				ALiBS.c7.i64c(d, c);
				ALiBS.c7.i64c(c, b);
				ALiBS.c7.i64c(b, a);
				ALiBS.c7.i642(a, T1, T2);
			}
			//
			ALiBS.c7.i642(H[0], H[0], a);
			ALiBS.c7.i642(H[1], H[1], b);
			ALiBS.c7.i642(H[2], H[2], c);
			ALiBS.c7.i642(H[3], H[3], d);
			ALiBS.c7.i642(H[4], H[4], e);
			ALiBS.c7.i642(H[5], H[5], f);
			ALiBS.c7.i642(H[6], H[6], g);
			ALiBS.c7.i642(H[7], H[7], h);
		}
		//
		//represent the hash as an array of 32-bit dwords
		var hash = new Array(16);
		//
		for(var i=0; i<8; i++)
		{
			hash[2*i] = H[i].h;
			hash[2*i + 1] = H[i].l;
		}
		//
		return hash;
	},
	// *****************************************************************
	i64: function(h, l) //A constructor for 64-bit numbers
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i64",le:ALiBS.gv.c7.le});
		//
		this.h = h;
		this.l = l;
		//this.toString = int64toString;
	},
	// *****************************************************************
	i64c: function(dst, src) //Copies src into dst, assuming both are 64-bit numbers
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i64c",le:ALiBS.gv.c7.le});
		//
		dst.h = src.h;
		dst.l = src.l;
	},
	// *****************************************************************
	//Right-rotates a 64-bit number by shift
	//Won't handle cases of shift>=32
	//The function revrrot() is for that
	i64r: function(dst, x, shift)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i64r",le:ALiBS.gv.c7.le});
		//
		dst.l = (x.l >>> shift) | (x.h << (32-shift));
		dst.h = (x.h >>> shift) | (x.l << (32-shift));
	},
	// *****************************************************************
	//Reverses the dwords of the source and then rotates right by shift.
	//This is equivalent to rotation by 32+shift
	i64v: function(dst, x, shift)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i64v",le:ALiBS.gv.c7.le});
		//
		dst.l = (x.h >>> shift) | (x.l << (32-shift));
		dst.h = (x.l >>> shift) | (x.h << (32-shift));
	},
	// *****************************************************************
	//Bitwise-shifts right a 64-bit number by shift
	//Won't handle shift>=32, but it's never needed in SHA512
	i64s: function(dst, x, shift)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i64s",le:ALiBS.gv.c7.le});
		//
		dst.l = (x.l >>> shift) | (x.h << (32-shift));
		dst.h = (x.h >>> shift);
	},
	// *****************************************************************
	//Adds two 64-bit numbers
	//Like the original implementation, does not rely on 32-bit operations
	i642: function(dst, x, y)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i642",le:ALiBS.gv.c7.le});
		//
		var w0 = (x.l & 0xffff) + (y.l & 0xffff);
		var w1 = (x.l >>> 16) + (y.l >>> 16) + (w0 >>> 16);
		var w2 = (x.h & 0xffff) + (y.h & 0xffff) + (w1 >>> 16);
		var w3 = (x.h >>> 16) + (y.h >>> 16) + (w2 >>> 16);
		dst.l = (w0 & 0xffff) | (w1 << 16);
		dst.h = (w2 & 0xffff) | (w3 << 16);
	},
	// *****************************************************************
	i644: function(dst, a, b, c, d) //Same, except with 4 addends. Works faster than adding them one by one.
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i644",le:ALiBS.gv.c7.le});
		//
		var w0 = (a.l & 0xffff) + (b.l & 0xffff) + (c.l & 0xffff) + (d.l & 0xffff);
		var w1 = (a.l >>> 16) + (b.l >>> 16) + (c.l >>> 16) + (d.l >>> 16) + (w0 >>> 16);
		var w2 = (a.h & 0xffff) + (b.h & 0xffff) + (c.h & 0xffff) + (d.h & 0xffff) + (w1 >>> 16);
		var w3 = (a.h >>> 16) + (b.h >>> 16) + (c.h >>> 16) + (d.h >>> 16) + (w2 >>> 16);
		dst.l = (w0 & 0xffff) | (w1 << 16);
		dst.h = (w2 & 0xffff) | (w3 << 16);
	},
	// *****************************************************************
	i645: function(dst, a, b, c, d, e) //Same, except with 5 addends
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c7.i645",le:ALiBS.gv.c7.le});
		//
		var w0 = (a.l & 0xffff) + (b.l & 0xffff) + (c.l & 0xffff) + (d.l & 0xffff) + (e.l & 0xffff);
		var w1 = (a.l >>> 16) + (b.l >>> 16) + (c.l >>> 16) + (d.l >>> 16) + (e.l >>> 16) + (w0 >>> 16);
		var w2 = (a.h & 0xffff) + (b.h & 0xffff) + (c.h & 0xffff) + (d.h & 0xffff) + (e.h & 0xffff) + (w1 >>> 16);
		var w3 = (a.h >>> 16) + (b.h >>> 16) + (c.h >>> 16) + (d.h >>> 16) + (e.h >>> 16) + (w2 >>> 16);
		dst.l = (w0 & 0xffff) | (w1 << 16);
		dst.h = (w2 & 0xffff) | (w3 << 16);
	}
	// *****************************************************************
};
//
ALiBS.c7._s5k = new Array();
//
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x428a2f98, -685199838); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x71374491, 0x23ef65cd);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1245643825, -330482897); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-373957723, -2121671748);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x3956c25b, -213338824); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x59f111f1, -1241133031);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1841331548, -1357295717); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1424204075, -630357736);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-670586216, -1560083902); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x12835b01, 0x45706fbe);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x243185be, 0x4ee4b28c); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x550c7dc3, -704662302);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x72be5d74, -226784913); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-2132889090, 0x3b1696b1);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1680079193, 0x25c71235); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1046744716, -815192428);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-459576895, -1628353838); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-272742522, 0x384f25e3);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0xfc19dc6, -1953704523); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x240ca1cc, 0x77ac9c65);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x2de92c6f, 0x592b0275); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x4a7484aa, 0x6ea6e483);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x5cb0a9dc, -1119749164); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x76f988da, -2096016459);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1740746414, -295247957); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1473132947, 0x2db43210);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1341970488, -1728372417); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1084653625, -1091629340);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-958395405, 0x3da88fc2); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-710438585, -1828018395);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x6ca6351, -536640913); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x14292967, 0xa0e6e70);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x27b70a85, 0x46d22ffc); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x2e1b2138, 0x5c26c926);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x4d2c6dfc, 0x5ac42aed); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x53380d13, -1651133473);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x650a7354, -1951439906); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x766a0abb, 0x3c77b2a8);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-2117940946, 0x47edaee6); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1838011259, 0x1482353b);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1564481375, 0x4cf10364); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1474664885, -1136513023);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1035236496, -789014639); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-949202525, 0x654be30);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-778901479, -688958952); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-694614492, 0x5565a910);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-200395387, 0x5771202a); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x106aa070, 0x32bbd1b8);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x19a4c116, -1194143544); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x1e376c08, 0x5141ab53);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x2748774c, -544281703); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x34b0bcb5, -509917016);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x391c0cb3, -976659869); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x4ed8aa4a, -482243893);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x5b9cca4f, 0x7763e373); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x682e6ff3, -692930397);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x748f82ee, 0x5defb2fc); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x78a5636f, 0x43172f60);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-2067236844, -1578062990); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1933114872, 0x1a6439ec);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1866530822, 0x23631e28); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1538233109, -561857047);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-1090935817, -1295615723); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-965641998, -479046869);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-903397682, -366583396); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-779700025, 0x21c0c207);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-354779690, -840897762); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(-176337025, -294727304);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x6f067aa, 0x72176fba); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0xa637dc5, -1563912026);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x113f9804, -1090974290); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x1b710b35, 0x131c471b);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x28db77f5, 0x23047d84); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x32caab7b, 0x40c72493);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x3c9ebe0a, 0x15c9bebc); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x431d67c4, -1676669620);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x4cc5d4be, -885112138); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x597f299c, -60457430);
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x5fcb6fab, 0x3ad6faec); 
ALiBS.c7._s5k[ALiBS.c7._s5k.length] = new ALiBS.c7.i64(0x6c44198c, 0x4a475817);
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c7("+(++ALiBS.gv.c7.nn)+"x)",{o:"ALiBS.c7"});
ALiBS.sy.la(0+0,"ALiBS.gv.c7.le="+ALiBS.gv.c7.le+ALiBS.gv.sy.se,{o:"ALiBS.c7"});
//
// *************************************************************************

/*

// Calculate the SHA-512 of a raw string
function rstr_sha512(s)
{
	return ALiBS.c7.b2s(ALiBS.c7._s5m0(ALiBS.c7.s2b(s), s.length * 8));
};

// Calculate the HMAC-SHA-512 of a key and some data (raw strings)
function rstr_hmac_sha512(key, data)
{
	var bkey = ALiBS.c7.s2b(key);
	if(bkey.length > 32) bkey = ALiBS.c7._s5m0(bkey, key.length * 8);

	var ipad = Array(32), opad = Array(32);
	for(var i = 0; i < 32; i++)
	{
		ipad[i] = bkey[i] ^ 0x36363636;
		opad[i] = bkey[i] ^ 0x5C5C5C5C;
	}

	var hash = ALiBS.c7._s5m0(ipad.concat(ALiBS.c7.s2b(data)), 1024 + data.length * 8);
	return ALiBS.c7.b2s(ALiBS.c7._s5m0(opad.concat(hash), 1024 + 512));
};

*/

// *************************************************************************

/*
 * A JavaScript implementation of the Secure Hash Algorithm, SHA-512, as defined
 * in FIPS 180-2
 * Version 2.2 Copyright Anonymous Contributor, Paul Johnston 2000 - 2009.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for details.
 */

/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
//var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
//var b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
/*
function hex_sha512(s)    
{ 
	return rstr2hex(rstr_sha512(str2rstr_utf8(s))); 
};

function b64_sha512(s)    
{ 
	return rstr2b64(rstr_sha512(str2rstr_utf8(s))); 
};

function any_sha512(s, e) 
{ 
	return rstr2any(rstr_sha512(str2rstr_utf8(s)), e);
};

function hex_hmac_sha512(k, d)
{ 
	return rstr2hex(rstr_hmac_sha512(str2rstr_utf8(k), str2rstr_utf8(d))); 
};

function b64_hmac_sha512(k, d)
{ 
	return rstr2b64(rstr_hmac_sha512(str2rstr_utf8(k), str2rstr_utf8(d))); 
};

function any_hmac_sha512(k, d, e)
{ 
	return rstr2any(rstr_hmac_sha512(str2rstr_utf8(k), str2rstr_utf8(d)), e);
};
*/

/*
 * Perform a simple self-test to see if the VM is working
 */
/*
function sha512_vm_test()
{
	return hex_sha512("abc").toLowerCase() ==
		"ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a" +
		"2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f";
};
*/



/*
 * Convert a raw string to a hex string
 */
/*
function rstr2hex(input)
{
	try { hexcase } catch(e) { hexcase=0; }
	var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
	var output = "";
	var x;
	for(var i = 0; i < input.length; i++)
	{
		x = input.charCodeAt(i);
		output += hex_tab.charAt((x >>> 4) & 0x0F)
		   +  hex_tab.charAt( x        & 0x0F);
	}
	return output;
};
*/

/*
 * Convert a raw string to a base-64 string
 */
/*
function rstr2b64(input)
{
	try { b64pad } catch(e) { b64pad=''; }
	var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	var output = "";
	var len = input.length;
	for(var i = 0; i < len; i += 3)
	{
		var triplet = (input.charCodeAt(i) << 16)
			| (i + 1 < len ? input.charCodeAt(i+1) << 8 : 0)
			| (i + 2 < len ? input.charCodeAt(i+2)      : 0);

		for(var j = 0; j < 4; j++)
		{
			if(i * 8 + j * 6 > input.length * 8) output += b64pad;
			else output += tab.charAt((triplet >>> 6*(3-j)) & 0x3F);
		}
	}
	return output;
};
*/

/*
 * Convert a raw string to an arbitrary string encoding
 */
/*
function rstr2any(input, encoding)
{
	var divisor = encoding.length;
	var i, j, q, x, quotient;

	// Convert to an array of 16-bit big-endian values, forming the dividend 
	var dividend = Array(Math.ceil(input.length / 2));
	for(i = 0; i < dividend.length; i++)
	{
		dividend[i] = (input.charCodeAt(i * 2) << 8) | input.charCodeAt(i * 2 + 1);
	}

	// Repeatedly perform a long division. The binary array forms the dividend,
	// the length of the encoding is the divisor. Once computed, the quotient
	// forms the dividend for the next step. All remainders are stored for later
	// use.

	var full_length = Math.ceil(input.length * 8 /
		                    (Math.log(encoding.length) / Math.log(2)));
	var remainders = Array(full_length);
	for(j = 0; j < full_length; j++)
	{
		quotient = Array();
		x = 0;
		for(i = 0; i < dividend.length; i++)
		{
			x = (x << 16) + dividend[i];
			q = Math.floor(x / divisor);
			x -= q * divisor;
			if(quotient.length > 0 || q > 0)
			quotient[quotient.length] = q;
		}
		remainders[j] = x;
		dividend = quotient;
	}

	// Convert the remainders to the output string 
	var output = "";

	for(i = remainders.length - 1; i >= 0; i--)
		output += encoding.charAt(remainders[i]);

	return output;
};
*/

/*
 * Encode a string as utf-8.
 * For efficiency, this assumes the input is valid utf-16.
 */
/*
function str2rstr_utf8(input)
{
	var output = "";
	var i = -1;
	var x, y;

	while(++i < input.length)
	{
		// Decode utf-16 surrogate pairs 
		x = input.charCodeAt(i);
		y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
		if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF)
		{
		x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
		i++;
		}

		// Encode output as utf-8 
		if(x <= 0x7F)
			output += String.fromCharCode(x);
		else if(x <= 0x7FF)
			output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
						    0x80 | ( x         & 0x3F));
		else if(x <= 0xFFFF)
			output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
						    0x80 | ((x >>> 6 ) & 0x3F),
						    0x80 | ( x         & 0x3F));
		else if(x <= 0x1FFFFF)
			output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
						    0x80 | ((x >>> 12) & 0x3F),
						    0x80 | ((x >>> 6 ) & 0x3F),
						    0x80 | ( x         & 0x3F));
	}
	return output;
};
*/

/*
 * Encode a string as utf-16
 */
/*
function str2rstr_utf16le(input)
{
	var output = "";

	for(var i = 0; i < input.length; i++)
		output += String.fromCharCode( input.charCodeAt(i)        & 0xFF,
				          (input.charCodeAt(i) >>> 8) & 0xFF);
	return output;
};

function str2rstr_utf16be(input)
{
	var output = "";

	for(var i = 0; i < input.length; i++)
		output += String.fromCharCode((input.charCodeAt(i) >>> 8) & 0xFF,
				           input.charCodeAt(i)        & 0xFF);
	return output;
};
*/

/*
 * Convert a raw string to an array of big-endian words
 * Characters >255 have their high-byte silently ignored.
 */
/*
function s2b(input)
{
	var output = Array(input.length >> 2);

	for(var i = 0; i < output.length; i++)
		output[i] = 0;
	for(var i = 0; i < input.length * 8; i += 8)
		output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
	return output;
};
*/

/*
 * Convert an array of big-endian words to a string
 */
/*
function b2s(input)
{
	var output = "";
	for(var i = 0; i < input.length * 32; i += 8)
		output += String.fromCharCode((input[i>>5] >>> (24 - i % 32)) & 0xFF);
	return output;
};
*/


/*
 * Calculate the SHA-512 of an array of big-endian dwords, and a bit length
 */
//var sha512_k;
//function binb_sha512(x, len)
//{
	/*
	if(sha512_k == undefined)
	{
		//SHA512 constants
		sha512_k = new Array(
			new int64(0x428a2f98, -685199838), new int64(0x71374491, 0x23ef65cd),
			new int64(-1245643825, -330482897), new int64(-373957723, -2121671748),
			new int64(0x3956c25b, -213338824), new int64(0x59f111f1, -1241133031),
			new int64(-1841331548, -1357295717), new int64(-1424204075, -630357736),
			new int64(-670586216, -1560083902), new int64(0x12835b01, 0x45706fbe),
			new int64(0x243185be, 0x4ee4b28c), new int64(0x550c7dc3, -704662302),
			new int64(0x72be5d74, -226784913), new int64(-2132889090, 0x3b1696b1),
			new int64(-1680079193, 0x25c71235), new int64(-1046744716, -815192428),
			new int64(-459576895, -1628353838), new int64(-272742522, 0x384f25e3),
			new int64(0xfc19dc6, -1953704523), new int64(0x240ca1cc, 0x77ac9c65),
			new int64(0x2de92c6f, 0x592b0275), new int64(0x4a7484aa, 0x6ea6e483),
			new int64(0x5cb0a9dc, -1119749164), new int64(0x76f988da, -2096016459),
			new int64(-1740746414, -295247957), new int64(-1473132947, 0x2db43210),
			new int64(-1341970488, -1728372417), new int64(-1084653625, -1091629340),
			new int64(-958395405, 0x3da88fc2), new int64(-710438585, -1828018395),
			new int64(0x6ca6351, -536640913), new int64(0x14292967, 0xa0e6e70),
			new int64(0x27b70a85, 0x46d22ffc), new int64(0x2e1b2138, 0x5c26c926),
			new int64(0x4d2c6dfc, 0x5ac42aed), new int64(0x53380d13, -1651133473),
			new int64(0x650a7354, -1951439906), new int64(0x766a0abb, 0x3c77b2a8),
			new int64(-2117940946, 0x47edaee6), new int64(-1838011259, 0x1482353b),
			new int64(-1564481375, 0x4cf10364), new int64(-1474664885, -1136513023),
			new int64(-1035236496, -789014639), new int64(-949202525, 0x654be30),
			new int64(-778901479, -688958952), new int64(-694614492, 0x5565a910),
			new int64(-200395387, 0x5771202a), new int64(0x106aa070, 0x32bbd1b8),
			new int64(0x19a4c116, -1194143544), new int64(0x1e376c08, 0x5141ab53),
			new int64(0x2748774c, -544281703), new int64(0x34b0bcb5, -509917016),
			new int64(0x391c0cb3, -976659869), new int64(0x4ed8aa4a, -482243893),
			new int64(0x5b9cca4f, 0x7763e373), new int64(0x682e6ff3, -692930397),
			new int64(0x748f82ee, 0x5defb2fc), new int64(0x78a5636f, 0x43172f60),
			new int64(-2067236844, -1578062990), new int64(-1933114872, 0x1a6439ec),
			new int64(-1866530822, 0x23631e28), new int64(-1538233109, -561857047),
			new int64(-1090935817, -1295615723), new int64(-965641998, -479046869),
			new int64(-903397682, -366583396), new int64(-779700025, 0x21c0c207),
			new int64(-354779690, -840897762), new int64(-176337025, -294727304),
			new int64(0x6f067aa, 0x72176fba), new int64(0xa637dc5, -1563912026),
			new int64(0x113f9804, -1090974290), new int64(0x1b710b35, 0x131c471b),
			new int64(0x28db77f5, 0x23047d84), new int64(0x32caab7b, 0x40c72493),
			new int64(0x3c9ebe0a, 0x15c9bebc), new int64(0x431d67c4, -1676669620),
			new int64(0x4cc5d4be, -885112138), new int64(0x597f299c, -60457430),
			new int64(0x5fcb6fab, 0x3ad6faec), new int64(0x6c44198c, 0x4a475817));
	}
	*/
/*
	//Initial hash values
	var H = new Array(
		new int64(0x6a09e667, -205731576),
		new int64(-1150833019, -2067093701),
		new int64(0x3c6ef372, -23791573),
		new int64(-1521486534, 0x5f1d36f1),
		new int64(0x510e527f, -1377402159),
		new int64(-1694144372, 0x2b3e6c1f),
		new int64(0x1f83d9ab, -79577749),
		new int64(0x5be0cd19, 0x137e2179));

	var T1 = new int64(0, 0),
		T2 = new int64(0, 0),
		a = new int64(0,0),
		b = new int64(0,0),
		c = new int64(0,0),
		d = new int64(0,0),
		e = new int64(0,0),
		f = new int64(0,0),
		g = new int64(0,0),
		h = new int64(0,0),
		//Temporary variables not specified by the document
		s0 = new int64(0, 0),
		s1 = new int64(0, 0),
		Ch = new int64(0, 0),
		Maj = new int64(0, 0),
		r1 = new int64(0, 0),
		r2 = new int64(0, 0),
		r3 = new int64(0, 0);

	var j, i;
	var W = new Array(80);

	for(i=0; i<80; i++)
		W[i] = new int64(0, 0);

	// append padding to the source string. The format is described in the FIPS.
	x[len >> 5] |= 0x80 << (24 - (len & 0x1f));
	x[((len + 128 >> 10)<< 5) + 31] = len;

	for(i = 0; i<x.length; i+=32) //32 dwords is the block size
	{
		int64copy(a, H[0]);
		int64copy(b, H[1]);
		int64copy(c, H[2]);
		int64copy(d, H[3]);
		int64copy(e, H[4]);
		int64copy(f, H[5]);
		int64copy(g, H[6]);
		int64copy(h, H[7]);

		for(j=0; j<16; j++)
		{
			W[j].h = x[i + 2*j];
			W[j].l = x[i + 2*j + 1];
		}

		for(j=16; j<80; j++)
		{
			//sigma1
			int64rrot(r1, W[j-2], 19);
			int64revrrot(r2, W[j-2], 29);
			int64shr(r3, W[j-2], 6);
			s1.l = r1.l ^ r2.l ^ r3.l;
			s1.h = r1.h ^ r2.h ^ r3.h;
			//sigma0
			int64rrot(r1, W[j-15], 1);
			int64rrot(r2, W[j-15], 8);
			int64shr(r3, W[j-15], 7);
			s0.l = r1.l ^ r2.l ^ r3.l;
			s0.h = r1.h ^ r2.h ^ r3.h;

			int64add4(W[j], s1, W[j-7], s0, W[j-16]);
		}

		for(j = 0; j < 80; j++)
		{
			//Ch
			Ch.l = (e.l & f.l) ^ (~e.l & g.l);
			Ch.h = (e.h & f.h) ^ (~e.h & g.h);

			//Sigma1
			int64rrot(r1, e, 14);
			int64rrot(r2, e, 18);
			int64revrrot(r3, e, 9);
			s1.l = r1.l ^ r2.l ^ r3.l;
			s1.h = r1.h ^ r2.h ^ r3.h;

			//Sigma0
			int64rrot(r1, a, 28);
			int64revrrot(r2, a, 2);
			int64revrrot(r3, a, 7);
			s0.l = r1.l ^ r2.l ^ r3.l;
			s0.h = r1.h ^ r2.h ^ r3.h;

			//Maj
			Maj.l = (a.l & b.l) ^ (a.l & c.l) ^ (b.l & c.l);
			Maj.h = (a.h & b.h) ^ (a.h & c.h) ^ (b.h & c.h);

			int64add5(T1, h, s1, Ch, sha512_k[j], W[j]);
			int64add(T2, s0, Maj);

			int64copy(h, g);
			int64copy(g, f);
			int64copy(f, e);
			int64add(e, d, T1);
			int64copy(d, c);
			int64copy(c, b);
			int64copy(b, a);
			int64add(a, T1, T2);
		}

		int64add(H[0], H[0], a);
		int64add(H[1], H[1], b);
		int64add(H[2], H[2], c);
		int64add(H[3], H[3], d);
		int64add(H[4], H[4], e);
		int64add(H[5], H[5], f);
		int64add(H[6], H[6], g);
		int64add(H[7], H[7], h);
	}

	//represent the hash as an array of 32-bit dwords
	var hash = new Array(16);

	for(i=0; i<8; i++)
	{
		hash[2*i] = H[i].h;
		hash[2*i + 1] = H[i].l;
	}
	return hash;
};
*/



