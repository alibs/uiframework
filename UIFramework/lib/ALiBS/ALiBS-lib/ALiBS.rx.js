/* *************************************************************************
// INFO:

var a = /\d{7}/

var a = new RegExp("\\d{7}", "g")

// ***************************

- não trata emails que ultrapassam 1 linha TODO(2010-09-14): arrumar na aplicação e não em mx()
alf@a9.
com.br,alf@a10
.com.br alf@a11.
com

- extrair emails 
- extrair emails e status de envio
- extrair mensagens de erro de log
- extrair pedaços dos log (xml)	

// ***************************

http://www.regular-expressions.info/examples.html
http://www.regular-expressions.info/dates.html
http://www.regular-expressions.info/email.html
http://www.regular-expressions.info/numericranges.html

http://regexlib.com/

http://regexadvice.com/

http://www.javascriptkit.com/jsref/regexp.shtml

// ***************************

Used to specify types of characters in a search (a sample)‏

\d OR [0-9] 		Matches any decimal digit
\D OR [^0-9] 		Matches any non-digit character
\s OR [ \t\n\r\f\v] 	Matches any whitespace character
\S OR [^ \t\n\r\f\v] 	Matches any non-whitespace character
\w OR [a-zA-Z0-9_] 	Matches any alphanumeric character
\W OR [^a-zA-Z0-9_] 	Matches any non-alphanumeric character

// ***************************

Characters with special meanings (metacharacters)(should be escaped)(\+)(\*)(\\): 

01) .		(REAL CHAR) Matches any char, ONE REQUIRED time if NOT with * OR {} (the dot)
		ab.	matches (aba,abb,abc,abd,etc)(does NOT match ab) 
		a..b	matches (aaab,aabb,abcb,axyb,etc)(does NOT match ab,abb,axb) 

		.*	[any char]{zero OR more times}
		[a-zA-Z0-9_]{0,} [any char]{zero OR more times}

		ab.*	matches (ab,aba,abab,abxyzd,etc)


02) * OR {0,}	(QUANTITY) Matches LAST char, {zero OR more times} (the asterisk or star)
		ab*	matches (a,ab,abb,abbb,etc)
		ab{0,}	matches (a,ab,abb,abbb,etc)

		abc*..		matches (abxy,abcxy,abccxy,abcccxy,etc)
		abc{0,}..	matches (abxy,abcxy,abccxy,abcccxy,etc)


03) + OR {1,}	(QUANTITY) Matches LAST char, {one OR more times}, at least one time (the plus sign)
		abc+	matches (abc,abcc,abccc,etc)
		abc{1,} matches (abc,abcc,abccc,etc)

		[abc]+		matches (a,b,c,aa,ab,ac,bb,bc,cc,abc,aaa,bbb,ccc,aaaa,bbbb,cccc,aabb,aacc,bbcc,etc)
		[abc]{1,}	matches (a,b,c,aa,ab,ac,bb,bc,cc,abc,aaa,bbb,ccc,aaaa,bbbb,cccc,aabb,aacc,bbcc,etc)

04) {}		(QUANTITY)
		a{4}	matches (a,aa,aaa,aaaa)
		a{2,5}	matches (aa,aaa,aaaa,aaaaa)

06) ^		(DENY OR START POSITION)(the caret) 
		[^ab] 		(in) DENY [c-zA-Z0-9_] are permitted (a,b,ab are NOT permitted)
		^[ab] 		(out) START POSITION (a,b) at the start (does NOT match ab at the start)

07) $		(END POSITION)(the dollar sign) 
		[ab]$ 		(a,b) at the end (does NOT match ab at the end)

04) [	the opening square bracket [, 
		[abc]		[any char a,b, OR c]
		[ab$] 		[any char a,b, OR $]

10) ?	the question mark ?, 
	do? 		will match zero or one (optional), same as do{0,1}

05) \ 	the backslash \, 

08) ,	the period ,

09) |	logical OR


11) ()	(the opening round bracket and the closing round bracket)


// ***************************

g = global search = search all the string/text, not only the first occurance
i = case insensitive

// ***************************

Regular Expression consists of 1 or more expressions. 
Any two regular expressions A and B can be concatenated. 
The result is a regular expression which matches a string 

if A matches some amount of the beginning of that string and B matches the rest of the string. 

For example:

You can use regular expressions in the Replace All or Replace First verbs in Argument Builder to match a pattern and then replace it. 
The example in the slide shows the pattern to match a phone number 
defined as (401) 444-4444 and using the grouping variables ($1, $2, $3) 
and write it back out as 401-444-4444

^\((d\d\d)\)\s*(\d\d\d)-(\d\d\d\d)$

// ***************************

UK Post code example
Outward postcode is 1 or 2 digit letters, followed by 1 or 2 digits
Inward  postcode is 1 digit, followed by 2 letters
Ex: 	PO4 1AX
	NW13 7AB
[a-z][a-z]?[0-9][0-9]?[0-9][a-z]{2} 
or 
\w{1,2}\d{1,2}\d\w\w

// ***************************

Phone Number Match  .*[89]00.*[0-9]{3}.*[0-9]{4}
 1-800-123-4567  or   800-1234567  or  9001234567  or  (800) 123-4567  or  18001234567
NOT 1-801-1234567  or   (901) 900-4567 or  170011234567

// *************************************************************************
// TODO:

validar ip
var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;

dentro de [] não é necessário usar \. 
ver exemplo de email1

******************************

/\([0-9]{2}\/[0-9]{2}\/[0-9]{4}/i; // dd-mm-aaaa

******************************

Não usar inicio (^) e fim ($) para fazer match pois a string passada em mx() não tem somente o email
e sim uma linha de texto com os emails misturados

Somente usar inicio (^) e fim ($) quando estiver testando um valor exato, exemplo: conteudo de um campo ou tag html.
Obs: na verdade, não é necessário pois te() faz esta validação

email0 faz match() = extrai o email de dentro de um texto/string
emailF faz test() = testa se a string(completa) é um email (Gerando erro:vide em te())

*/
//
// *************************************************************************
//
if(!ALiBS.gv.rx) ALiBS.gv.rx = {}; // global values
//
if(!ALiBS.gv.rx.le) ALiBS.gv.rx.le = 0; // local log status
//
ALiBS.gv.rx.nn = 0; // x times loaded
//
ALiBS.gv.rx.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.rx.rc = {}; // regular expressions cache
//
// *************************************************************************
//
// Mapping of Regular Expressions
ALiBS.gv.rx.rc["email0"] = /[a-z0-9]{1,}[a-z0-9._-]{1,}@[a-z0-9_-]{1,}[a-z0-9_-]{1,}\.[a-z]{2,4}[.]{0,1}[a-z]{0,2}/gi; // all occurances, insensitive
ALiBS.gv.rx.rc["emailF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.emailF",le:ALiBS.gv.rx.le});
				//
				return !ALiBS.sg.is(s.charAt(0));
			}; // must exist, returns true/false TODO(2010-09-14):melhorar teste
//
ALiBS.gv.rx.rc["cpf0"] = /^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$/g; // all occurances TODO(2010-09-14): validar ^ inicio e $ fim = Stress test
ALiBS.gv.rx.rc["cpf1"] = /^[0-9]{3}$/g; // all occurances TODO(2010-09-14): validar ^ inicio e $ fim = Stress test
ALiBS.gv.rx.rc["cpfF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.cpfF",le:ALiBS.gv.rx.le});
				//
				return true; 
			}; // must exist, returns true/false TODO(2010-09-14):melhorar teste
//
ALiBS.gv.rx.rc["tel0"] = /^\([0-9]{2}\)[0-9]{4}\-[0-9]{4}$/g; // all occurances TODO(2010-09-14): validar ^ inicio e $ fim = Stress test
ALiBS.gv.rx.rc["telF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.telF",le:ALiBS.gv.rx.le});
				//
				return true; 
			}; // must exist, returns true/false TODO(2010-09-14):melhorar teste
//
/*
ALiBS.gv.rx.rc["ALiBS.vs0"] = ALiBS.gv.sy.ALiBSvs; // first occurance, sensitive
ALiBS.gv.rx.rc["ALiBS.vsF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.vsF",le:ALiBS.gv.rx.le});
				//
				return true; 
			}; // must exist, returns true/false TODO(2010-09-14):melhorar teste
*/
//
ALiBS.gv.rx.rc["id0"] = /[0-9]{1,}/g; // all occurances, only numbers
ALiBS.gv.rx.rc["idF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.idF",le:ALiBS.gv.rx.le});
				//
				return ALiBS.sg.id(s);
			}; // must exist, returns true/false
//
ALiBS.gv.rx.rc["ic0"] = /[a-z]{1,}/gi; // all occurances, insensitive
ALiBS.gv.rx.rc["icF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.icF",le:ALiBS.gv.rx.le});
				//
				return ALiBS.sg.ic(s);
			}; // must exist, returns true/false
//
ALiBS.gv.rx.rc["is0"] = /[^a-z0-9]{1,}/gi; // all occurances, insensitive
ALiBS.gv.rx.rc["isF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.isF",le:ALiBS.gv.rx.le});
				//
				return ALiBS.sg.is(s);
			}; // must exist, returns true/false
//
ALiBS.gv.rx.rc["ih0"] = /[a-f0-9]{1,}/gi; // all occurances, insensitive 
ALiBS.gv.rx.rc["ihF"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.ihF",le:ALiBS.gv.rx.le});
				//
				return ALiBS.c9.ih(s);
			}; // must exist, returns true/false
//
ALiBS.gv.rx.rc["i40"] = /[a-z0-9_-]{1,}/gi; // all occurances, insensitive 
ALiBS.gv.rx.rc["i4F"] = function f(s)
			{ 
				ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.i4F",le:ALiBS.gv.rx.le});
				//
				return ALiBS.c9.i4(s);
			}; // must exist, returns true/false
//
//trim TODO(2013-11-10):implementar
//String.prototype.trim = function() {
//    return this.replace(/^\s+|\s+$/g, "");
//};
//
// *************************************************************************
//
ALiBS.rx = // properties
{
	is: function(e) // TODO(2010-09-14): vou usar pra que????
	// e = (required)(Object/String)error
	// return (boolean) true/false	
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.is",le:ALiBS.gv.rx.le});
		//
	},
	// *****************************************************************
	mx: function(s,x) // match expression
	// s = (required)(String)text to search
	// x = (required)(String)regex id in ALiBS.gv.rx.rc[x] (email,cpf,tel,etc)
	// return = (Array)matches
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.mx",le:ALiBS.gv.rx.le});
		//
		if(!s || !x) 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.rx.mx:!s||!x"; 
			return null;
		} // TODO(2010-09-14): retorno de erro com ALiBS.sy.er()
		//
		var r = new Array();
		var l;
		//
		var f = ALiBS.gv.rx.rc[x+"F"]; // must exist
		var e0 = ALiBS.gv.rx.rc[x+"0"]; // may not exist
		var e1 = ALiBS.gv.rx.rc[x+"1"]; // may not exist
		//
		var rf = null; // function test return
		var re0 = null; // expression0 match return
		var re1 = null; // expression1 match return
		//
		if(e0) re0 = s.match(e0); // return Array
		if(e1) re1 = s.match(e1); // return Array
		//
		//ALiBS.sy.la(5,"x="+x,{o:"ALiBS.rx",le:true});
		//ALiBS.sy.la(5,"x+F="+x+"F",{o:"ALiBS.rx",le:true});
		//ALiBS.sy.la(5,"f="+f,{o:"ALiBS.rx",le:true});
		//
		if(re0)
		{
			l = re0.length;
			for(var i=0;i<l;i++)
			{
				//ALiBS.sy.la(5,"re0["+i+"]="+re0[i],{o:"ALiBS.rx",le:true});
				//ALiBS.sy.la(5,"f(re0[i])="+f(re0[i]),{o:"ALiBS.rx",le:true});
				if(f(re0[i])) r[r.length] = re0[i]; // add to Array if f() returns true
			}
		}
		//
		if(re1)
		{
			l = re1.length;
			for(var i=0;i<l;i++)
			{
				//ALiBS.sy.la(5,"re1["+i+"]="+re1[i],{o:"ALiBS.rx",le:true});
				//ALiBS.sy.la(5,"f(re1[i])="+f(re1[i]),{o:"ALiBS.rx",le:true});
				if(f(re1[i])) r[r.length] = re1[i]; // add to Array if f() returns true
			}
		}
		//
		//ALiBS.sy.la(5,""+ALiBS.gv.sy.se,{o:"ALiBS.rx",le:true});
		return r; // Array 
	},
	// *****************************************************************
	mm: function(s,m,p) // match message
	// s = (required)(String)text to search
	// m = (required)(Array||table||rs) messages Array
	// p = (optional)(JSON)parameters {af:0,bf:1} 
	// 	af = (number)field number of A(table OR ALiBS.to.rs)
	// return = (Array)position = matched messages????
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.mm",le:ALiBS.gv.rx.le});
		//
		if(!s || !m) 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.rx.mm:!s||!m"; 
			return null;
		} // TODO(2010-09-14): retorno de erro com ALiBS.sy.er()
		//
		var af = null;
		if(p && typeof p.af != "undefined" && p.af != null) af = p.af;
		//
		var r = null; // match return
		var v = null;
		var len = 0;
		//
		if(typeof af == "undefined" || af == null) len = m.length; // Array
		else if(typeof m.fn == "undefined") len = m.length; // ALiBS.to.rs
		else len = m.rs.length; // table
		//
		for(var i=0;i<len;i++)
		{
			v = ALiBS.to.f2(m,i,{af:af});
			r = s.match(v);
			if(r) return i; // Array position
		}
		//
		return null;
	},
	// *****************************************************************
	te: function(s,x) // test expression 
	// s = (required)(String)to test
	// x = (required)(String)regex id in ALiBS.gv.rx.rc[x]
	// return = (boolean)true/false	
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.rx.te",le:ALiBS.gv.rx.le});
		//
		//ALiBS.sy.la(5,"s="+s,{o:"ALiBS.rx",le:true});
		//ALiBS.sy.la(5,"x="+x,{o:"ALiBS.rx",le:true});
		//
		if(!s || !x) 
		{
			ALiBS.gv.er.em=":ERROR:ALiBS.rx.te:!s||!x"; 
			return null;
		} // TODO(2010-09-14): retorno de erro com ALiBS.sy.er() - retornar ERRO ou FALSE????
		//
		var f = ALiBS.gv.rx.rc[x+"F"]; // must exist
		var e0 = ALiBS.gv.rx.rc[x+"0"];
		var e1 = ALiBS.gv.rx.rc[x+"1"];
		//
		var rf = null; // function test return
		var re0 = null; // expression0 test return
		var re1 = null; // expression1 test return
		//
		if(f) rf = f(s);  // must exist
		//
		//if(rf) ALiBS.sy.la(5,"rf="+rf,{o:"ALiBS.rx",le:true});
		//else ALiBS.sy.la(5,"rf="+rf+ALiBS.gv.sy.se,{o:"ALiBS.rx",le:true});
		//
		if(rf==true)  // f() and at least one expression must return true
		{
			//if(e0) re0 = e0.test(s); // problem with $ - . = it does not test correctly
			//if(e1) re1 = e1.test(s);
			//
			if(e0) re0 = s.match(e0); // return Array
			if(e1) re1 = s.match(e1); // return Array
			//
			if(s==re0) re0 = true;
			if(s==re1) re1 = true;
			//
			//ALiBS.sy.la(5,"re0="+re0,{o:"ALiBS.rx",le:true});
			//ALiBS.sy.la(5,"re1="+re1+ALiBS.gv.sy.se,{o:"ALiBS.rx",le:true});
			//
			if(re0==true || re1==true) return true; // f() and at least one expression must return true
			else return false;
		} 
		else return false; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.rx("+(++ALiBS.gv.rx.nn)+"x)",{o:"ALiBS.rx"});
ALiBS.sy.la(0+0,"ALiBS.gv.rx.le="+ALiBS.gv.rx.le+ALiBS.gv.sy.se,{o:"ALiBS.rx"});

