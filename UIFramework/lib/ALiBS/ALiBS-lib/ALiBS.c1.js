/* *************************************************************************
// INFO:

// *************************************************************************
// TODO:

1) change var r to string buffer or Array

*/
//
// *************************************************************************
//
if(!ALiBS.gv.c1) ALiBS.gv.c1 = {}; // global values
//
if(!ALiBS.gv.c1.le) ALiBS.gv.c1.le = 0; // local log status
//
ALiBS.gv.c1.nn = 0; // x times loaded
//
ALiBS.gv.c1.rl = "15.0731.1524";
//
// *************************************************************************
//
ALiBS.c1 = // conversion string - ebcdic
{
	__a0:
	[
		0,1,2,3,55,45,46,47,22,5,// 0-9
		37,11,12,13,14,15,16,17,18,19,// 10-19
		60,61,50,38,24,25,63,39,28,29,// 20-29
		30,31,64,90,127,123,91,108,80,125,//30-39
		77,93,92,78,107,96,75,97,240,241,// 40-49
		242,243,244,245,246,247,248,249,122,94,// 50-59
		76,126,110,111,124,193,194,195,196,197,// 60-69
		198,199,200,201,209,210,211,212,213,214,// 70-79
		215,216,217,226,227,228,229,230,231,232,// 80-89
		233,173,224,189,154,109,121,129,130,131,// 90-99
		132,133,134,135,136,137,145,146,147,148,// 100-109
		149,150,151,152,153,162,163,164,165,166,// 110-119
		167,168,169,192,79,208,95,7,32,33,// 120-129
		34,35,36,21,6,23,40,41,42,43,// 130-139
		44,9,10,27,48,49,26,51,52,53,// 140-149
		54,8,56,57,58,59,4,20,62,225,// 150-159
		65,66,67,68,69,70,71,72,73,81,// 160-169
		82,83,84,85,86,87,88,89,98,99,// 170-179
		100,101,102,103,104,105,112,113,114,115,// 180-189
		116,117,118,119,120,128,138,139,140,141,// 190-199
		142,143,144,106,155,156,157,158,159,160,// 200-209
		170,171,172,74,174,175,176,177,178,179,// 210-219
		180,181,182,183,184,185,186,187,188,161,// 220-229
		190,191,202,203,204,205,206,207,218,219,// 230-239
		220,221,222,223,234,235,236,237,238,239,// 240-249
		250,251,252,253,254,255 // 250-255
	],
	// *****************************************************************
	__ta: function(n0,t0,p) // table ascii to ebcdic
	/** 
	Method <static> <number:array> ALiBS.c1.ae(p)

		Description: table ascii to ebcdic

		Parameters:

			a0 = (required)(number:array) array (ascii / ebcedic)
			t0 = (required)(string) value type

		Observations:
	
		Return Value:
			<static> <number:array> array

		Example(s):
			var ok = ALiBS.c1.__ta();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c1.__ta",le:ALiBS.gv.c1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(typeof(n0)!="number") throw "ALiBS.c1.__ta.Exception: Error: Invalid Parameter Type: (number) n0 passed as "+typeof(n0); // 
			if(typeof(t0)!="string") throw "ALiBS.c1.__ta.Exception: Error: Invalid Parameter Type: (string) t0 passed as "+typeof(t0); // 
			//
			ALiBS.sy.la(5+0,"n0="+n0,{o:"ALiBS.c1.__ta",le:ALiBS.gv.c1.le});
			ALiBS.sy.la(5+0,"t0="+t0,{o:"ALiBS.c1.__ta",le:ALiBS.gv.c1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_r0 = null;
			//
			if(t0=="e") // return ebcdic
			{
				z.v_r0 = ALiBS.c1.__a0[n0]; // content = ebcdic
			}
			else // return ascii
			{
				for(var i=0;i<ALiBS.c1.__a0.length;i++)
				{
					if(n0==ALiBS.c1.__a0[i])
					{
						z.v_r0 = i; // position = ascii 
						break;
					}
				}
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_r0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c1.__ta",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c1.__ta",le:ALiBS.gv.c1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c1.__ta",le:ALiBS.gv.c1.le});
		//
		return r; 
	},
	// *****************************************************************
	ae: function(a0,p) // ascii to ebcdic
	/** 
	Method <static> <number:array> ALiBS.c1.ae(p)

		Description: ascii to ebcdic

		Parameters:

			a0 = (required)(number:array) array (ascii)

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number:array> array

		Example(s):
			var ok = ALiBS.c1.ae();

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c1.ae",le:ALiBS.gv.c1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(a0)!="object-Array") throw "ALiBS.c1.ae.Exception: Error: Invalid Parameter Type: (object-Array) a0 passed as "+ALiBS.sy.ty(a0); // 
			//
			ALiBS.sy.la(5+0,"a0="+a0,{o:"ALiBS.c1.ae",le:ALiBS.gv.c1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_e0 = new Array();
			//
			// ************************
			//
			z.ec = -2000; //
			for(var i=0;i<a0.length;i++)
			{
				z.v_e0[i] = ALiBS.c1.__ta(a0[i],"e");
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_e0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c1.ae",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c1.ea",le:ALiBS.gv.c1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c1.ea",le:ALiBS.gv.c1.le});
		//
		return r; 
	},
	// *****************************************************************
	ea: function(e0,p) // ebcdic to ascii
	/** 
	Method <static> <number:array> ALiBS.c1.ea(p)

		Description: ebcdic to ascii

		Parameters:

			e0 = (required)(number:array) array (ebcdic)

			p = (optional)(JSON)parameter {}

		Observations:
	
		Return Value:
			<static> <number:array> array

		Example(s):
			var ok = ALiBS.c1.ea(e0);

	--------------------------------------------------------------------
	*/
	/*
		Confidential Info - do NOT disclose
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.c1.ea",le:ALiBS.gv.c1.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // [BEST-PRACTICE] first command = raise 100/100
			//
			// ************************
			//
			z.ec = -950; // [BEST-PRACTICE] parameters validation
			if(ALiBS.sy.ty(e0)!="object-Array") throw "ALiBS.c1.ea.Exception: Error: Invalid Parameter Type: (object-Array) e0 passed as "+ALiBS.sy.ty(e0); // 
			//
			ALiBS.sy.la(5+0,"e0="+e0,{o:"ALiBS.c1.ea",le:ALiBS.gv.c1.le});
			//
			// ************************
			//
			z.ec = -1000; // [BEST-PRACTICE] optional parameters default values
			//
			// ************************
			//
			z.ec = -1050; // [BEST-PRACTICE] table variables
			//
			// ************************
			//
			z.ec = -1100; // [BEST-PRACTICE] local variables
			z.v_a0 = new Array();
			//
			// ************************
			//
			z.ec = -2000; //
			for(var i=0;i<e0.length;i++)
			{
				z.v_a0[i] = ALiBS.c1.__ta(e0[i],"a");
			}
			//
			// ************************
			//
			z.ec = -9900; // [BEST-PRACTICE] assign return value
			r = z.v_a0;
			//
			// ************************
			//
			z.ec = -9999; // [BEST-PRACTICE] last command = ERROR happened AFTER here
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				case -950:z.m0="Invalid Parameter Type";z.m1=""+e;break;  // -950 = Invalid Parameter Type
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.c1.ea",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.c1.ea",le:ALiBS.gv.c1.le});
		//
		ALiBS.sy.la(6+0,"Method Execution Finished...",{o:"ALiBS.c1.ea",le:ALiBS.gv.c1.le});
		//
		return r; 
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.c1("+(++ALiBS.gv.c1.nn)+"x)",{o:"ALiBS.c1"});
ALiBS.sy.la(0+0,"ALiBS.gv.c1.rl="+ALiBS.gv.c1.rl,{o:"ALiBS.c1"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.c1.le="+ALiBS.gv.c1.le+ALiBS.gv.sy.se,{o:"ALiBS.c1"});

