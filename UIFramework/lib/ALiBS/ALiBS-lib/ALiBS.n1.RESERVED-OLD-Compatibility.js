/* *************************************************************************
// INFO:

// *************************************************************************
// TODO

*/
//
// *************************************************************************
/** 
	Class ALiBS.n1

		Description: 
			- Required Class for All Others;
			- Creates ALiBS Namespace;
			- Modules Name Cache;
			- Log Management;

	--------------------------------------------------------------------
*/
//
if(!ALiBS.gv.n1) ALiBS.gv.n1 = {}; // global values
//
if(!ALiBS.gv.n1.le) ALiBS.gv.n1.le = 0; // local log status
//
ALiBS.gv.n1.nn = 0; // x times loaded
//
ALiBS.gv.n1.cs = "nasp,todo,7777,csst,lela,load";
//
// *************************************************************************
//
ALiBS.n1 = // Novell - iManager
{
	as0: function(id,go,ar,p) // XML advancedSelectionXML
	/** 
	Method <static> ALiBS.n1.ch(p)

		Description: Create XML advancedSelectionXML (advancedSelectionXML.value)

		Parameters:
			id = (required)(String)id
			go = (required)(String)group operation
			ar = (required)(String)row Array  
				var ar = new Array();
				ar[ar.length] = {a:"nrfLocalizedDescrs",o:"AS.Op.isEqual",v:"*",ro:"and"};
					a = (String)attribute
					o = (String)operation
					v = (String)value
					ro = (String)row operation
			p = (optional)(JSON) parameters {d:""} 
				d = (String)
				al = (String) attribute list
				cx = (number) 
					0 - Open and Close
					1 - Only Opens
					2 - Only Closes

		Return Value:
			NO Return

	--------------------------------------------------------------------
	*/
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.n1.ga",le:ALiBS.gv.n1.le});
		//
		var x = new Array();
		//
		var cx = 0; // Default
		//
		if(p)
		{
			if(typeof(p.cx)=="number" && p.cx>0) cx = p.cx;
		}
		//
		if(cx<2) // 0 or 1 = Opens
		{
			x[x.length] = "<selection-criterion>";
			//
			x[x.length] = "<types><id>" + id + "</id></types>";
			//
			if(p)
			{
				if(typeof(p.al)=="string") x[x.length] = "<att-list>" + p.al + "</att-list>";
			}
		}
		//
		x[x.length] = "<group>";
		//
		for(var i=0;i<ar.length;i++)
		{
			x[x.length] = "<row>";
			//
			x[x.length] = "<attribute>" + ar[i].a + "</attribute>";
			x[x.length] = "<operation>" + ar[i].o + "</operation>";
			x[x.length] = "<value>" + ar[i].v + "</value>";
			x[x.length] = "<row-op>" + ar[i].ro + "</row-op>";
			//
			x[x.length] = "</row>";
		}
		//
		x[x.length] = "<group-op>" + go + "</group-op>";
		x[x.length] = "</group>";
		//
		if(cx!=1) // Closes
		{
			x[x.length] = "</selection-criterion>";
		}
		//
		return x.join("");
	}
	// *****************************************************************
};
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.n1("+(++ALiBS.gv.n1.nn)+"x)",{o:"ALiBS.n1"});
ALiBS.sy.la(0+0,"ALiBS.gv.n1.le="+ALiBS.gv.n1.le+ALiBS.gv.sy.se,{o:"ALiBS.n1"});

