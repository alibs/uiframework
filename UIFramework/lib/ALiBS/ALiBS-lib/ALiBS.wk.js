/* *************************************************************************
// INFO:

// *************************************************************************
// TODO(2010-09-08):

TODO(2010-09-08): 

*/
//
// *************************************************************************
//
if(!ALiBS.gv.wk) ALiBS.gv.wk = {}; // global values
//
if(!ALiBS.gv.wk.le) ALiBS.gv.wk.le = 0; // local log status
//
ALiBS.gv.wk.nn = 0; // x times loaded
//
ALiBS.gv.wk.cs = "nasp,todo,7777,csst,lela,load";
//
ALiBS.gv.wk.rl = "14.0822.0001";
//
// *************************************************************************
//
ALiBS.gv.wk.sts = // TODO(2014-05-30):complete with other strings/codes
{
	OKPASS:"[OK][PASS]",
	ERRORPASSP:"[ERROR][PASS][P]",
	ERRORPASSU:"[ERROR][PASS][U]"
};
//
// *************************************************************************
//
ALiBS.wk = // (server) token
{
	// *****************************************************************
	p1: function(cc,rq,rp,p) // password validation + systems controls
	// cc = (required)(connection) Connection Control / Session (javax.naming.directory.DirContext)
	// rq = (required)(HttpServletRequest) page request
	// rp = (required)(HttpServletResponse) page response
	//
	// p = (optional)(JSON)parameters {} 
	//	us = (string) user
	//	pa = (string) pass 
	//	apa = (string) application Authentication data
	//
	// return = (string)
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wk.p1",le:ALiBS.gv.wk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -900; // 
			//
			// DEFAULTS [BEST-PRACTICE]
			if(typeof(z.apa)!="string") z.apa = ""; 
			//
			ALiBS.sy.la(5+0,"z.us="+z.us,{o:"ALiBS.wk.p1",le:ALiBS.gv.wk.le});
			ALiBS.sy.la(8+0,"z.pa="+z.pa,{o:"ALiBS.wk.p1",le:ALiBS.gv.wk.le});
			ALiBS.sy.la(5+0,"z.apa="+z.apa,{o:"ALiBS.wk.p1",le:ALiBS.gv.wk.le});
			//
			z.ec = -1001; // 
			z.T0 = ALiBS.wk.c1(cc,rq,rp,p);
			//
			//if(typeof(z.T0)!="number") 
			if(ALiBS.sy.ty(z.T0)=="object-ALiBS.to") 
			{
				z.ec = -1201; // 
				if(z.T0.rs.length>0 && z.T0.rs[0].f01!="") // modifyTimestamp != "" means search OK
				{
					z.ec = -1301; // 
					if(!z.apa) // no application info
					{
						r = "[SYSTEM-OFF][NO-APP-INFO]"; 
					}
					else if(z.T0.rs[0].f03!="1") // f03 = brdiManagerGeral/brdPortalGeral != 1 = EM MANUTENCAO 
					{
						r = "[SYSTEM-OFF][APP-OUT-OF-SERVICE]"; 
					}
					else
					{
						z.ec = -1401; // 
						if(z.T0.rs[0].f02=="1") // f02 = brdTokenGeral = ligado = 1
						{
							//r = "[TOKEN-GERAL-LIGADO]"; 
							//
							z.ec = -1500; // 
							z.T1 = ALiBS.wk.u1(cc,rq,rp,p);
							//
							z.ec = -1651; // 
							ALiBS.to.lt(z.T1,{o:"ALiBS.wk.p0",le:ALiBS.gv.wk.le,ll:500,id:"(TKlist)"});
							//
							z.ec = -1671; // 
							//if(typeof(z.T1)!="number") 
							if(ALiBS.sy.ty(z.T1)=="object-ALiBS.to") 
							{
								z.ec = -1701; // 
								if(z.T1.rs.length>0 && z.T1.rs[0].f01!="") // modifyTimestamp != "" means search OK
								{
									z.ec = -1800; // 
									if(z.T1.rs[0].f02!="1") // brdTokenHabilitado != 1 = Desabilitado
									{
										r = "[TOKEN-OFF][U][H]";
									}
									else if(z.T1.rs[0].f03!="1") // brdTokenAssociado != 1 = Desassociado
									{
										r = "[TOKEN-OFF][U][A]";
									}
									else
									{
										r = "[TOKEN-ON]";
									}
								}
								else
								{
									z.ec = -1900; // 
									if(z.T1.rs[0] && z.T1.rs[0].f01=="") // modifyTimestamp == "" means NO ACCESS
									{
										r = "[ERROR][NO-ACCESS][USER]";
									}
									else
									{
										r = "[ERROR][NO-INFO][USER]";
									}
								}
							}
							else if(typeof(z.T1)=="string")
							{
								r = "[ERROR]"+z.T0; // CONNECTION ERROR = 
							}
							else
							{
								r = "[ERROR][CONNECTION][USER]"; // CONNECTION ERROR = user or pass
							}
						}
						else
						{
							r = "[TOKEN-OFF][S]"; // Token Off System
						}
					}
				}
				else
				{
					if(z.T0.rs[0].f01=="") // modifyTimestamp == "" means NO ACCESS
					{
						r = "[ERROR][NO-ACCESS][OU]";
					}
					else
					{
						r = "[ERROR][NO-INFO][OU]";
					}
				}
			}
			else if(typeof(z.T0)=="string")
			{
				r = "[ERROR]"+z.T0; // CONNECTION ERROR = 
			}
			else
			{
				r = "[ERROR][CONNECTION][OU]"; // CONNECTION ERROR = control data
			}
			//
			ALiBS.wk.x1(cc,rq,rp,p); // x1 should use try/catch and NOT throw Exception
		}
		catch(e)
		{
			ALiBS.wk.x1(cc,rq,rp,p); // x1 should use try/catch and NOT throw Exception
			//
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wk.p1",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wk.p1",le:ALiBS.gv.wk.le});
		//
		return r; 
	},
	// *****************************************************************
	wsc: function(cc,rq,rp,p) // ws cmd 
	// cc = (required)(connection) Connection Control / Session (javax.naming.directory.DirContext)
	// rq = (required)(HttpServletRequest) page request
	// rp = (required)(HttpServletResponse) page response
	//
	//requestHttp TODO(2014-06-03): needed?
	//responseHttp TODO(2014-06-03): needed?
	//
	// p = (optional)(JSON) parameters {} - URL parameters
	//
	// return = (string) status control
	{
		ALiBS.sy.la(6+0,"Method Execution Started...",{o:"ALiBS.wk.wsc",le:ALiBS.gv.wk.le});
		//
		var z = p || {}; // [BEST-PRACTICE]
		//
		z.ec = -1; // ec = start
		//
		var r = z.cmd + " Registered but NOT found in ALiBS.wk.wsc()"; // [BEST-PRACTICE]
		//
		try
		{
			z.ec = -2; // 
			//
			for(var i in z)
			{
				ALiBS.sy.la(7+0,"z["+i+"]="+z[i],{o:"ALiBS.wk.wsc",le:ALiBS.gv.wk.le});
			}
			//
			// DEFAULTS [BEST-PRACTICE]
			//
			// table variables
			//
			// local variables [BEST-PRACTICE]
			//
			z.ec = -1001; 
			z.cmd0 = z.cmd.replace("ALiBS.wk.",""); // remains p8, t8, o8, etc
			//
			z.ec = -1050; 
			if(z.cmd0 && ALiBS.wk[z.cmd0])
			{
				z.ec = -1070; 
				r = ALiBS.wk[z.cmd0](cc,rq,rp,p);
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("p0")>=0) // check password - local test
			{
				//ALiBS.sy.la(5+0,"z.cmd="+z.cmd,{o:"ALiBS.wk.wsc",le:ALiBS.gv.wk.le});
				//
				z.ec = -1000; // 
				if(ALiBS.sg.co(z.p1,["A7892","I500334","B978741","u0000001","u0000002","u0000003","u0000004","admin","volneidds"])>=0)
				{
					z.ec = -1100; // 
					if(ALiBS.sg.co(z.p1,["N0v3ll","novell","zaq1xsw2","10203040"])>=0)
					{
						z.ec = -1200; // 
						r = "[TOKEN-ON]"; // password OK + token required
						//r = ALiBS.gv.wk.sts.OKPASS;
					}
					else
					{
						z.ec = -1300; // 
						r = ALiBS.gv.wk.sts.ERRORPASSP;
					}
				}
				else
				{
					z.ec = -1400; // 
					r = ALiBS.gv.wk.sts.ERRORPASSU;
				}
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("p1")>=0) // check password - IDM
			{
				//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"jsws:2000a",le:1});
				//
				z.ec = -11000; // 
				ALiBS.wk.ca(cc,rq,rp,{apa:z.apa}); // connetion Authentication data settings
				//
				z.ec = -11001; // 
				r = ALiBS.wk.p1(cc,rq,rp,{us:z.p1,pa:z.p2,apa:z.apa});
			}
			// *************************************************
			// *************************************************
			// *************************************************
			// *************************************************
			//
			else if(z.cmd.indexOf("t0")>=0) // check token - local test
			{
				//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"jsws:2001",le:1});
				//
				z.ec = -2000; // 
				if(z.p1.indexOf("u0000004")>=0 || z.p1.indexOf("admin")>=0)
				{
					z.ec = -2100; // 
					r = "[OK][TOKEN][000-0]"; 
				}
				else
				{
					z.ec = -2200; // 
					r = "[ERROR][TOKEN]";
				}
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("t1")>=0) // check token - local test
			{
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("t9")>=0) // check token - local test - token bradesco
			{
				//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"jsws:2001",le:1});
				//
				z.ec = -2000; // 
				if(z.p1.indexOf("u0000004z")>=0)
				{
					z.ec = -2100; // 
					r = "[XXX-USUARIO-NAO-TEM-TOKEN]"; // Token Bradesco teste: 33-2416007-3
				}
				else if(z.p1.indexOf("u0000004")>=0 || z.p1.indexOf("admin")>=0)
				{
					z.ec = -2200; // 
					r = "[OK][TOKEN][007-3]"; // Token Bradesco teste: 33-2416007-3
				}
				else
				{
					z.ec = -2300; // 
					r = "[ERROR][TOKEN]";
				}
			}
			//
			// *************************************************
			// *************************************************
			// *************************************************
			// *************************************************
			//
			else if(z.cmd.indexOf("o0")>=0) // check otp - local test
			{
				//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"jsws:2002",le:1});
				//
				z.ec = -3000; // 
				if(z.p1.indexOf("u0000004")>=0 || z.p1.indexOf("admin")>=0)
				{
					z.ec = -3100; // 
					if(z.p2=="000000") //
					{
						z.ec = -3200; // 
						r = "[OK][OTP]";
					}
					else
					{
						z.ec = -3300; // 
						r = "[ERROR][OTP][O]";
					}
				}
				else
				{
					z.ec = -3400; // 
					r = "[ERROR][OTP][U]";
				}
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("o1")>=0) // check otp
			{
			}
			//
			// *************************************************
			//
			else if(z.cmd.indexOf("o9")>=0) // check otp - local test - token bradesco
			{
				//ALiBS.sy.la(6+0,"Method Execution Started...",{o:"jsws:2002",le:1});
				//
				// u0000004z não vai chegar aqui pois vai dar erro no token number
				//
				z.ec = -3000; // 
				if(z.p1.indexOf("u0000004")>=0)
				{
					z.ec = -3100; // 
					if(z.p2.length==6 && ALiBS.sg.id(z.p2,{m:6,n:6}) && z.p2=="123456")
					{
						z.ec = -3200; // 
						r = "[OK][OTP]";
					}
					else
					{
						z.ec = -3300; // 
						r = "[XXX-OTP-INVALIDA]";
					}
				}
				else if(z.p1.indexOf("admin")>=0)
				{
					z.ec = -3100; // 
					if(z.p2.length==6 && ALiBS.sg.id(z.p2,{m:6,n:6}) && z.p2=="123456")
					{
						z.ec = -3400; // 
						r = "[OK][OTP]";
					}
					else
					{
						z.ec = -3500; // 
						r = "[ERROR][OTP][O]";
					}
				}
				else
				{
					z.ec = -3400; // 
					r = "[ERROR][OTP][U]";
				}
			}
			//
			// *************************************************
			// *************************************************
			// *************************************************
			// *************************************************
			//
			else 
			{
				//
			}
		}
		catch(e)
		{
			switch(z.ec)
			{
				case -1:z.m0="Generic Error";z.m1="";break;
				//case -2:z.m0="Parameter x must be a number";z.m1="";break;
				default:z.m0="Generic Default Error";z.m1="";break;
			}
			//
			r = ALiBS.sy.em(z.ec,"ALiBS.wk.wsc",{m0:z.m0,m1:z.m1,m2:e}).m2;
			//
			z = null; // [BEST-PRACTICE]
			delete z; // [BEST-PRACTICE]
			//
			throw r;
		}
		//
		z = null; // [BEST-PRACTICE]
		delete z; // [BEST-PRACTICE]
		//
		ALiBS.sy.la(5+0,"r="+r,{o:"ALiBS.wk.wsc",le:ALiBS.gv.wk.le});
		//
		return r; 
	}
	// *****************************************************************
};
//
// *************************************************************************
//
if(ALiBS.gv.ws) // add urls to ws cache
{
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.p0"] = ALiBS.wk.wsc; 
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.p0&p1=admin&p2=N0v3ll&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.p1"] = ALiBS.wk.wsc; 
	//Precisa da VM
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.p1&p1=admin&p2=N0v3ll&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.t0"] = ALiBS.wk.wsc; 
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.t0&p1=admin&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.t1"] = ALiBS.wk.wsc; 
	//Nao tem codigo no if
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.t1&p1=admin&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.t9"] = ALiBS.wk.wsc; 
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.t9&p1=admin&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.o0"] = ALiBS.wk.wsc; 
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.o0&p1=admin&p2=0000&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.o1"] = ALiBS.wk.wsc; 
	//Nao tem codigo no if
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.o1&p1=admin&p2=123456&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.wsc.if.o9"] = ALiBS.wk.wsc; 
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.o9&p1=admin&p2=123456&apa=t1-[A:local]-[T:local]
	//
	ALiBS.gv.ws.uc["ALiBS.wk.p1"] = ALiBS.wk.wsc; 
	//Precisa da VM
	//http://token.local:8080/WEB/token/ALiBS/ALiBS-lib-c/wsjs-c.jsp?cb=cb&cmd=ALiBS.wk.wsc.if.p1&p1=admin&p2=N0v3ll&apa=t1-[A:local]-[T:local]
	//
}
//
// *************************************************************************
//
ALiBS.sy.la(0+0,"loaded module ALiBS.wk("+(++ALiBS.gv.wk.nn)+"x)",{o:"ALiBS.wk"});
ALiBS.sy.la(0+0,"ALiBS.gv.wk.rl="+ALiBS.gv.wk.rl,{o:"ALiBS.wk"}); 
ALiBS.sy.la(0+0,"ALiBS.gv.wk.le="+ALiBS.gv.wk.le+ALiBS.gv.sy.se,{o:"ALiBS.wk"});
//
// *************************************************************************
//
//	SAMPLE OF FUNCTIONS TO BE OVERWRITTEN / EXTENDED
/*

*/
//
// *************************************************************************

